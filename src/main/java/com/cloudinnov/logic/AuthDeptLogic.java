package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.AuthDept;
import com.cloudinnov.model.AuthDeptUser;

/**
 * Created by chengning on 2016/11/11.
 */
public interface AuthDeptLogic extends IBaseLogic<AuthDept>{
	
	public List<AuthDept> tableList(String[] codes, String language);
	
	public int saveOtherLanguage(AuthDept authDept);
	
	public int updateOtherLanguage(AuthDept authDept);
	
	public List<AuthDeptUser> selectDeptUserList(AuthDeptUser authDeptUser);
}
