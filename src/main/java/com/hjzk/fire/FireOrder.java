package com.hjzk.fire;

import java.util.ArrayList;

import com.hjzk.serialport.exception.NoSuchPort;
import com.hjzk.serialport.exception.NotASerialPort;
import com.hjzk.serialport.exception.PortInUse;
import com.hjzk.serialport.exception.SendDataToSerialPortFailure;
import com.hjzk.serialport.exception.SerialPortOutputStreamCloseFailure;
import com.hjzk.serialport.exception.SerialPortParameterFailure;

import gnu.io.SerialPort;

public class FireOrder {

	static SerialPortManager manager = new SerialPortManager();
	
	public static void main(String[] args) throws SendDataToSerialPortFailure, SerialPortOutputStreamCloseFailure, SerialPortParameterFailure, NotASerialPort, NoSuchPort, PortInUse, InterruptedException {
		startAlarm();
	}
	
	public static void   startAlarm() throws SendDataToSerialPortFailure, SerialPortOutputStreamCloseFailure, SerialPortParameterFailure, NotASerialPort, NoSuchPort, PortInUse, InterruptedException{
 
		ArrayList<String> findPort = manager.findPort();
		for (int i = 0; i < findPort.size(); i++) {
			System.err.println(findPort.get(i));
		}
		SerialPort openPort = null;
		try {
			openPort = manager.openPort("COM3", 9600);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			openPort.close();
		}
		manager.sendToPort(openPort, startLightOrder());
		Thread.sleep(5000);
		manager.sendToPort(openPort, stopOrder());
		openPort.close();
	}
	
	public static  byte[] startVoiceOrder(){
		
		byte[] order=new byte[9];
		order[0]=(byte) hex2Low(0x01);
		order[1]=(byte) hex2Low(0x10);
		order[2]=(byte) hex2Low(0x00);
		order[3]=(byte) hex2Low(0x1A);
		order[4]=(byte) hex2Low(0x00);
		order[5]=(byte) hex2Low(0x01);
		order[6]=(byte) hex2Low(0x01);
		order[7]=(byte) hex2Low(0xCE);
		order[8]=(byte) hex2Low(0x18);
		
		return order;
	}
	
	public static byte[] startLightOrder(){
		byte[] order=new byte[9];
		order[0]=(byte) hex2Low(0x01);
		order[1]=(byte) hex2Low(0x10);
		order[2]=(byte) hex2Low(0x00);
		order[3]=(byte) hex2Low(0x1A);
		order[4]=(byte) hex2Low(0x00);
		order[5]=(byte) hex2Low(0x01);
		order[6]=(byte) hex2Low(0x02);
		order[7]=(byte) hex2Low(0x8E);
		order[8]=(byte) hex2Low(0x19);
		
		return order;
	}
	
	public static byte[] stopOrder(){
		
		byte[] order=new byte[9];
		order[0]=(byte) hex2Low(0x01);
		order[1]=(byte) hex2Low(0x10);
		order[2]=(byte) hex2Low(0x00);
		order[3]=(byte) hex2Low(0x1A);
		order[4]=(byte) hex2Low(0x00);
		order[5]=(byte) hex2Low(0x01);
		order[6]=(byte) hex2Low(0x00);
		order[7]=(byte) hex2Low(0x0F);
		order[8]=(byte) hex2Low(0xD8);
		return order;
	}
	
	public static int hex2Low(int num){
		
		byte a=(byte) num;
		int i=a;
		i = a&0xff;  
 
		return i;
	}
	
}
