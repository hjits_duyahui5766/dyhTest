package com.cloudinnov.test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.utils.support.spring.SpringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class WanPingFireUtils2 {

	static final Logger LOG = Logger.getLogger(WanPingFireUtils2.class);
	public static final String TOTAL_CAR = "total:car";
	public static final String CAL_NAME = "car:deviceId:";
	public static final String CAL_VALUE = ":value";
	private static final int RECENT_LIST_COUNT = 10000;
	public static final String SPLITTER_LEVEL0 = ",";
	private CarDetectorLogic carDetectorLogic = SpringUtils.getBean("carDetectorLogic");
	private JedisPool jedisPool = SpringUtils.getBean("jedisPool");

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static boolean isConnect = false;
	private static Socket socket;
	private static String ip;
	private static int port;

	public WanPingFireUtils2(String ip, int port) {
		// TODO Auto-generated constructor stub
		this.ip = ip;
		this.port = port;
	
	}
	public static void main(String[] args) {
		ip="10.141.181.111";
		port=5000;
		startConnectService();
	}

	public static void startConnectService() {

		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			// TODO: handle exception
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			LOG.info("重新建立 连接成功");
		} else {
			connectAgain();

		}

	}

	private static void connectAgain() {
		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			// TODO: handle exception
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			LOG.info("重新建立连接成功");
		} else {
			connectAgain();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private static class ThreadReader extends Thread {
		Socket socket;

		public ThreadReader(Socket socket) {
			this.socket = socket;
			this.setName("ThreadReader");
		}

		@Override
		public void run() {
			try {
				while (true) {

					InputStream is = socket.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					String s = reader.readLine();
					System.err.println(s);
					
					if (s.length() > 10) {
						parseCarData(s);
						System.err.println();
					}

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				 LOG.error("正在尝试重新建立连接");

				connectAgain();

			}

		}
	}

	private class ThreadWriter extends Thread {
		Socket socket;

		public ThreadWriter(Socket socket) {
			this.socket = socket;
			this.setName("ThreadWriter");
		}

		@Override
		public void run() {
			try {
				while (true) {
					socket.sendUrgentData(0xff);
					Thread.sleep(2000);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.err.println("网络断线");
			}
		}
	}

	private static void parseCarData(String result) {

		try {
			 
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("数据解析失败"+e.toString());
		}
	}
}
