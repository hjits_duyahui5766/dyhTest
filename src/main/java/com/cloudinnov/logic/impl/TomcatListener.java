package com.cloudinnov.logic.impl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
*
 * @author chengning
 * @date 2017年9月12日下午1:56:04
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public class TomcatListener implements ServletContextListener {
    private static final Logger LOG = LoggerFactory.getLogger(TomcatListener.class);
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
 
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        /*
         * 销毁所有后台线程
         * */
        if(GlobalInitLogicImpl.BACKGROUND_THREADS != null) {
            GlobalInitLogicImpl.BACKGROUND_THREADS.get(GlobalInitLogicImpl.EMERGECY_PHONE_THREAD_ALIAS).interrupt();
            GlobalInitLogicImpl.BACKGROUND_THREADS.get(GlobalInitLogicImpl.FIRE_CRE_THREAD_ALIAS).interrupt();
            GlobalInitLogicImpl.BACKGROUND_THREADS.get(GlobalInitLogicImpl.CAR_TCP_THREAD_ALIAS).interrupt();
            GlobalInitLogicImpl.BACKGROUND_THREADS.get(GlobalInitLogicImpl.INIT_ALLSCREEN_THREAD_ALIAS).interrupt();
            GlobalInitLogicImpl.BACKGROUND_THREADS.get(GlobalInitLogicImpl.INIT_DATABASE_THREAD_ALIAS).interrupt();
        }      
        LOG.error("全局销毁成功");
        
    }
}
