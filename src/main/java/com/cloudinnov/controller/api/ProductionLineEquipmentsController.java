package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.ProductionLineEquipmentsLogic;
import com.cloudinnov.model.Equipments;

/**
 * @author
 * @date 2016年2月23日下午6:10:25
 * @email
 * @remark 产物配置 controller
 * @version
 */
@Controller
@RequestMapping("/webapi/productLineEquipments")
public class ProductionLineEquipmentsController extends BaseController {

	@Autowired
	private ProductionLineEquipmentsLogic productionLineEquipmentsLogic;

	/** 查询产线下的设备
	* listByQuery 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @param request
	* @param @param response
	* @param @throws JsonGenerationException
	* @param @throws JsonMappingException
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping(value = "/listbyquery")
	@ResponseBody
	@SystemLog(module = "生产线", methods = "产线下的设备")
	public void listByQuery(String codes,String language,
			HttpServletRequest request, HttpServletResponse response) throws JsonGenerationException,
			JsonMappingException, IOException {
		List<Equipments> equipments = productionLineEquipmentsLogic.selectEquCodeNameByProLineCode(codes,language);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipments != null) {
			map.put("list", equipments);
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, "");
			response.getWriter().println(returnData);
		}
	}
}
