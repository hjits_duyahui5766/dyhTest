package com.cloudinnov.dao;

import com.cloudinnov.model.BomCategory;

public interface BomCategoryDao extends IBaseDao<BomCategory>{
    int deleteByPrimaryKey(Integer id);

    int insert(BomCategory record);

    int insertSelective(BomCategory record);

    BomCategory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BomCategory record);

    int updateByPrimaryKey(BomCategory record);
}