package com.cloudinnov.model;


public class EquipmentFaultChannel extends BaseModel{
	
    private String equipmentCode;

    private String libraryCode;

    private String name;

    private String comment;   
    
    private Equipments equipment;
    
    private FaultTranslationLibraries faultLib;

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

	public Equipments getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}

	public FaultTranslationLibraries getFaultLib() {
		return faultLib;
	}

	public void setFaultLib(FaultTranslationLibraries faultLib) {
		this.faultLib = faultLib;
	}
    
    
}