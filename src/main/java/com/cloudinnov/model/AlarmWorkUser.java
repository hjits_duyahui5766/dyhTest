package com.cloudinnov.model;

public class AlarmWorkUser extends BaseModel{

    private String userCode;

    private Integer role;
    

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

}