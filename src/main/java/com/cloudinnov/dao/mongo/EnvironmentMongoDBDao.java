package com.cloudinnov.dao.mongo;

import org.springframework.stereotype.Repository;

import com.cloudinnov.model.Environment;

@Repository("environmentMongoDBDao")
public class EnvironmentMongoDBDao extends BaseMongoDAOImpl<Environment>{
	
}
