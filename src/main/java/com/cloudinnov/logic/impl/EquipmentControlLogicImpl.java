package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.EquipmentPointsDao;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.mongo.EquipmentControlLogMongoDBDao;
import com.cloudinnov.logic.CameraControlLogic;
import com.cloudinnov.logic.EquipmentControlLogic;
import com.cloudinnov.model.CameraModel;
import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionConfig;
import com.cloudinnov.model.ControlSolutionConfig.Config;
import com.cloudinnov.model.ControlSolutionConfig.ConfigText;
import com.cloudinnov.model.EquControlModel;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.RemoteControllerUtil;
import com.cloudinnov.utils.control.ControlModel;
import com.cloudinnov.utils.control.EquipmentControlLog;
import com.cloudinnov.utils.control.EquipmentControlLog.Data;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月13日上午11:31:52
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class EquipmentControlLogicImpl implements EquipmentControlLogic {
	@Autowired
	private EquipmentPointsDao equipmentPointsDao;
	@Autowired
	private EquipmentsAttrDao equipmentsAttrDao;
	@Autowired
	private EquipmentControlLogMongoDBDao equipmentControlLogMongoDBDao;
	@Autowired
	private CameraControlLogic cameraControlLogic;

	@Override
	public int sendContrlCommands(String command) {
		int returnCode = 0;
		if (CommonUtils.isNotEmpty(command)) {
			List<EquControlModel> list = JSON.parseArray(command, EquControlModel.class);
			if (JudgeNullUtil.iList(list)) {
				List<ControlModel> data = new ArrayList<>();
				List<Data> logs = new ArrayList<>();
				List<EquipmentPoints> points = null;
				ControlModel contrl = null;
				EquipmentsAttr equAttrModel = null;
				EquipmentControlLog.Data log = null;
				for (EquControlModel model : list) {// 遍历得到单个设备
					if (model != null) {
						if (model.getCategoryCode().indexOf("SXT") != -1) {// 需要判断分类编码如果为摄像头则找到config中的屏号进行投屏操作
							equAttrModel = new EquipmentsAttr();
							equAttrModel.setEquipmentCode(model.getEquCode());
							CameraModel cameraModel = null;
							List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(equAttrModel);
							if (configList != null && configList.size() > 0) {
								cameraModel = new CameraModel();
								for (EquipmentsAttr attr : configList) {
									if (attr.getCustomTag() != null) {
										if (attr.getCustomTag().equals(CameraModel.INDEX_CODE)) {// 如果摄像头设备配置cameraId则进行投屏
											cameraModel.setIndexCode(attr.getValue());
											break;
										}
									}
								}
							}
							log = new EquipmentControlLog.Data();
							if (cameraModel != null && JudgeNullUtil.iList(model.getConfig())) {
								cameraModel.setScreenId(Integer.valueOf((String) model.getConfig().get(0).getValue()));
								String status = cameraControlLogic.switchCamera(cameraModel);
								System.out.println(status);
								log.setXid(String.valueOf(cameraModel.getScreenId()));
								log.setValue(cameraModel.getIndexCode());
								if (status.equals(CommonUtils.CAREMA_OK)) {
									log.setResult(true);
									log.setCode(1);
								} else {
									log.setResult(false);
								}
							} else {
								log.setResult(false);
								log.setCode(CommonUtils.PARAMETER_EXCEPTION_CODE);
							}
							logs.add(log);
						} else {
							points = equipmentPointsDao.selectPointsByEquipCode(model.getEquCode());// 通过设备编码查询设备下的点位以及指标类型
							if (JudgeNullUtil.iList(points) && JudgeNullUtil.iList(model.getConfig())) {// 如果设备下有点位并且设备控制中包含控制指标类型
								for (EquipmentPoints point : points) {
									for (EquControlModel.Config config : model.getConfig()) {
										if (CommonUtils.isNotEmpty(config.getIndexType())
												&& CommonUtils.isNotEmpty(point.getIndexType())
												&& config.getIndexType().equals(point.getIndexType())) {// 判断指标类型是否相等,相等则加入到控制集合中
											// 照明灯
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												contrl = new ControlModel();
												for (EquipmentPoints equipmentPoints : points) {
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
												}
												if (contrl.getXid() != null) {
													data.add(contrl);
												}
											} else if (config.getIndexType()
													.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
												contrl = new ControlModel();
												for (EquipmentPoints equipmentPoints : points) {
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_OF);
													}
												}
												if (contrl.getXid() != null) {
													data.add(contrl);
												}
											}
											// 车道指示器
											// 正面
											if (config.getType().equals(CommonUtils.EQUIPMENTS_JUST)) {
												if (config.getIndexType()
														.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)// 如果正绿为1，说明原来的是正红
														&& Integer.valueOf((Integer) config
																.getValue()) == CommonUtils.INDEX_TYPE_OF) {
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();
														if (equipmentPoints.getIndexType()
																.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(CommonUtils.INDEX_TYPE_ON);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														}
														else if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															int index = 0;
															for (ControlModel dat : data) {
																if(dat.getXid() == equipmentPoints.getXid()) {
																	index = 1;
																	break;
																}
															}
															if(index == 0) {
																contrl.setXid(equipmentPoints.getXid());
																contrl.setValue(0);
																if (contrl.getXid() != null) {
																	data.add(contrl);
																}
															}
														}
													}
													contrl = new ControlModel();
													contrl.setXid(point.getXid());
													contrl.setValue(0);
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
												if (config.getIndexType()
														.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)// 正红
														&& Integer.valueOf((Integer) config
																.getValue()) == CommonUtils.INDEX_TYPE_OF) {
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();
														if (equipmentPoints.getIndexType()
																.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(CommonUtils.INDEX_TYPE_OF);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														}
														else if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															int index = 0;
															for (ControlModel dat : data) {
																if(dat.getXid() == equipmentPoints.getXid()) {
																	index = 1;
																	break;
																}
															}
															if(index == 0) {
																contrl.setXid(equipmentPoints.getXid());
																contrl.setValue(0);
																if (contrl.getXid() != null) {
																	data.add(contrl);
																}
															}
														}
													}
												}
												//正面左转
												if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)){
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();													
														if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(1);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														}
													}
												}
											}
											// 反面
											if (config.getType().equals(CommonUtils.EQUIPMENTS_BACK)) {
												if (config.getIndexType()
														.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)// 反红
														&& Integer.valueOf((Integer) config
																.getValue()) == CommonUtils.INDEX_TYPE_OF) {
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();
														if (equipmentPoints.getIndexType()
																.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(CommonUtils.INDEX_TYPE_ON);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														} 
														else if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															int index = 0;
															for (ControlModel dat : data) {
																if(dat.getXid() == equipmentPoints.getXid()) {
																	index = 1;
																	break;
																}
															}
															if(index == 0) {
																contrl.setXid(equipmentPoints.getXid());
																contrl.setValue(0);
																if (contrl.getXid() != null) {
																	data.add(contrl);
																}
															}
														}
													}
													contrl = new ControlModel();
													contrl.setXid(point.getXid());
													contrl.setValue(0);
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
												if (config.getIndexType()
														.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)// 反绿
														&& Integer.valueOf((Integer) config
																.getValue()) == CommonUtils.INDEX_TYPE_OF) {
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();
														if (equipmentPoints.getIndexType()
																.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(CommonUtils.INDEX_TYPE_OF);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														} 
														else if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															int index = 0;
															for (ControlModel dat : data) {
																if(dat.getXid() == equipmentPoints.getXid()) {
																	index = 1;
																	break;
																}
															}
															if(index == 0) {
																contrl.setXid(equipmentPoints.getXid());
																contrl.setValue(0);
																if (contrl.getXid() != null) {
																	data.add(contrl);
																}
															}
														}
													}
												}
												if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {								
													for (EquipmentPoints equipmentPoints : points) {
														contrl = new ControlModel();
														if(equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
															contrl.setXid(equipmentPoints.getXid());
															contrl.setValue(1);
															if (contrl.getXid() != null) {
																data.add(contrl);
															}
														}
														
													}
							
												}
												
											}
											// 交通信号灯
											// 黄灯
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
															&& !equipmentPoints.getIndexType()
																	.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
													/*// 左转
													contrl = new ControlModel();
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
														contrl.setXid(equipmentPoints.getXid());
														for (EquControlModel.Config con : model.getConfig()) {
															if (con.getIndexType()
																	.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
																contrl.setValue(con.getValue());
															}
														}
													}
													if (contrl.getXid() != null && model.getValue() != null) {
														data.add(contrl);
													}*/
												}
											}
											// 红灯
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 绿灯
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 左转
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													} else {	//由于下面的代码会屏蔽掉左转的控制，所以在此添加左转的控制
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_OF);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 隧道风机
											// 正转
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 反转
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 停止
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 横洞卷帘门
											// 上升
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 下降
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (!equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											// 横洞指示器
											// 正面
											if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)// 开
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_OF);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
													contrl = new ControlModel();
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)
															|| equipmentPoints.getIndexType().equals(
																	CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
													contrl = new ControlModel();
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {// 红灯打开
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											} else if (config.getIndexType()
													.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)
													&& Integer.valueOf(
															(Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
												for (EquipmentPoints equipmentPoints : points) {
													contrl = new ControlModel();
													if (equipmentPoints.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
															|| equipmentPoints.getIndexType().equals(
																	CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
														contrl.setXid(equipmentPoints.getXid());
														contrl.setValue(CommonUtils.INDEX_TYPE_ON);
													}
													if (contrl.getXid() != null) {
														data.add(contrl);
													}
												}
											}
											if (!config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)
													&& !config.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
													&& !config.getIndexType()
															.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
												//判断设备控制命令值是否为空
												if(config.getValue() == null) {
												    continue;
												}else {
												    contrl = new ControlModel();
	                                                contrl.setXid(point.getXid());
	                                                /*
	                                                 * 因为加入了设备的3种前端状态
    	                                               1.设备在线开机 value为1 
    	                                               2.设备在线关机 0
    	                                               3.设备离线-1
    	                                                                                                                                             所以发送控制命令要对-1进行判断如果为-1需要转为0
	                                                */
	                                                int commandValue = (int) config.getValue();
	                                                contrl.setValue(commandValue == -1 ? 0 : commandValue);
	                                                if (contrl.getXid() != null) {
	                                                    data.add(contrl);
	                                                }
												}
												
											}
										}
									}
								}
							}
						}
					}
				}
				if (JudgeNullUtil.iList(data)) {// 把除摄像头之外的与bolomi控制的日志存储到数据库
					EquipmentControlLog result = RemoteControllerUtil.controller(data);// 发送控制指令到bolomi
					returnCode = result.getCode();
					equipmentControlLogMongoDBDao.save(result);
				}
				if (JudgeNullUtil.iList(logs)) {// 单独存储摄像头控制
					EquipmentControlLog result = new EquipmentControlLog();
					result.setData(logs);
					equipmentControlLogMongoDBDao.save(result);
				}
			}
		}
		return returnCode;
	}
	
	@Override
	public List<ControlModel> generateControllerCommond(ControlSolution controlSolution) {
		List<ControlModel> data = new ArrayList<ControlModel>();
		List<Config> configs = JSON.parseArray(controlSolution.getConfig(), ControlSolutionConfig.Config.class);// 读取方案中的实际勾选的控制点位
		List<EquipmentPoints> points = equipmentPointsDao.selectPointsByEquipCode(controlSolution.getEquipmentCode());// 查询设备下的所有点位
		if (JudgeNullUtil.iList(configs)) {
			EquipmentsAttr equAttrModel = null;
			EquipmentControlLog.Data log = null;
			List<Data> logs = new ArrayList<>();
			for (Config config : configs) {
				if (config.getCategoryCode().indexOf("SXT") != -1) {// 需要判断分类编码如果为摄像头则找到config中的屏号进行投屏操作
					equAttrModel = new EquipmentsAttr();
					equAttrModel.setEquipmentCode(controlSolution.getEquipmentCode());
					CameraModel cameraModel = null;
					List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(equAttrModel);
					if (configList != null && configList.size() > 0) {
						cameraModel = new CameraModel();
						for (EquipmentsAttr attr : configList) {
							if (attr.getCustomTag() != null) {
								if (attr.getCustomTag().equals(CameraModel.INDEX_CODE)) {// 如果摄像头设备配置cameraId则进行投屏
									cameraModel.setIndexCode(attr.getValue());
									break;
								}
							}
						}
					}
					log = new EquipmentControlLog.Data();
					if (cameraModel != null && config != null) {
						for (ConfigText con : configs.get(0).getTexts()) {
							cameraModel.setScreenId(Integer.valueOf((String) con.getValue()));
						}
						String status = cameraControlLogic.switchCamera(cameraModel);
						System.out.println(status);
						log.setXid(String.valueOf(cameraModel.getScreenId()));
						log.setValue(cameraModel.getIndexCode());
						if (status.equals(CommonUtils.CAREMA_OK)) {
							log.setResult(true);
							log.setCode(1);
						} else {
							log.setResult(false);
						}
					} else {
						log.setResult(false);
						log.setCode(CommonUtils.PARAMETER_EXCEPTION_CODE);
					}
					logs.add(log);
					if (JudgeNullUtil.iList(logs)) {// 单独存储摄像头控制
						EquipmentControlLog result = new EquipmentControlLog();
						result.setData(logs);
						equipmentControlLogMongoDBDao.save(result);
					}
				}
			}
		}
		if (JudgeNullUtil.iList(configs) && JudgeNullUtil.iList(points)) {
			ControlModel model = null;
			for (ConfigText config : configs.get(0).getTexts()) {// 对比群控中的点位和设备下的实际点位指标类型是否存在
				for (EquipmentPoints point : points) {
					if (CommonUtils.isNotEmpty(config.getIndexType()) && CommonUtils.isNotEmpty(point.getIndexType())
							&& config.getIndexType().equals(point.getIndexType())) {
						// 照明灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							for (EquipmentPoints equipmentPoints : points) {
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
							}
							if (model.getXid() != null) {
								data.add(model);
							}
						} else if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							for (EquipmentPoints equipmentPoints : points) {
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_OF);
								}
							}
							if (model.getXid() != null) {
								data.add(model);
							}
						}
						// 车道指示器
						// 正面
						if (config.getType().equals(CommonUtils.EQUIPMENTS_JUST)) {
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)// 正绿
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_ON);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								model.setXid(point.getXid());
								model.setValue(0);
								if (model.getXid() != null) {
									data.add(model);
								}
							}
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)// 正红
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_OF);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
							}
						}
						// 反面
						if (config.getType().equals(CommonUtils.EQUIPMENTS_BACK)) {
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)// 反红
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_ON);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								model.setXid(point.getXid());
								model.setValue(0);
								if (model.getXid() != null) {
									data.add(model);
								}
							}
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)// 反绿
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_OF);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
							}
						}
						// 交通信号灯
						// 黄灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
										&& !equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								// 左转
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									for (ConfigText con : configs.get(0).getTexts()) {
										if (con.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
											model.setValue(con.getValue());
										}
									}
								}
								if (model.getXid() != null && model.getValue() != null) {
									data.add(model);
								}
							}
						}
						// 红灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)
										&& !equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								// 左转
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									for (ConfigText con : configs.get(0).getTexts()) {
										if (con.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
											model.setValue(con.getValue());
										}
									}
								}
								if (model.getXid() != null && model.getValue() != null) {
									data.add(model);
								}
							}
						}
						// 绿灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 隧道风机
						// 正转
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 反转
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 停止
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 横洞卷帘门
						// 上升
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 下降
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 横洞指示器
						// 正面
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)// 开
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType()
										.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_OF);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)
										|| equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						} else if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
										|| equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						if (!config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)
								&& !config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
								&& !config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							model.setXid(point.getXid());
							model.setValue(config.getValue());
							if (model.getXid() != null) {
								data.add(model);
							}
						}
					}
				}
			}
		}
		return data;
	}
	
	@Override
	public List<ControlModel> generateControllerCommond(Equipments equipment) {

		List<ControlModel> data = new ArrayList<ControlModel>();
		List<Config> configs = JSON.parseArray(equipment.getCommand(), ControlSolutionConfig.Config.class);// 读取方案中的实际勾选的控制点位
		
		List<EquipmentPoints> points = equipmentPointsDao.selectPointsByEquipCode(equipment.getCode());// 查询设备下的所有点位
		if (JudgeNullUtil.iList(configs)) {
			EquipmentsAttr equAttrModel = null;
			EquipmentControlLog.Data log = null;
			List<Data> logs = new ArrayList<>();
			for (Config config : configs) {
				if (config.getCategoryCode().indexOf("SXT") != -1) {// 需要判断分类编码如果为摄像头则找到config中的屏号进行投屏操作
					equAttrModel = new EquipmentsAttr();
					equAttrModel.setEquipmentCode(equipment.getCode());
					CameraModel cameraModel = null;
					List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(equAttrModel);
					if (configList != null && configList.size() > 0) {
						cameraModel = new CameraModel();
						for (EquipmentsAttr attr : configList) {
							if (attr.getCustomTag() != null) {
								if (attr.getCustomTag().equals(CameraModel.INDEX_CODE)) {// 如果摄像头设备配置cameraId则进行投屏
									cameraModel.setIndexCode(attr.getValue());
									break;
								}
							}
						}
					}
					log = new EquipmentControlLog.Data();
					if (cameraModel != null && config != null) {
						for (ConfigText con : configs.get(0).getTexts()) {
							cameraModel.setScreenId(Integer.valueOf((String) con.getValue()));
						}
						String status = cameraControlLogic.switchCamera(cameraModel);
						System.out.println(status);
						log.setXid(String.valueOf(cameraModel.getScreenId()));
						log.setValue(cameraModel.getIndexCode());
						if (status.equals(CommonUtils.CAREMA_OK)) {
							log.setResult(true);
							log.setCode(1);
						} else {
							log.setResult(false);
						}
					} else {
						log.setResult(false);
						log.setCode(CommonUtils.PARAMETER_EXCEPTION_CODE);
					}
					logs.add(log);
					if (JudgeNullUtil.iList(logs)) {// 单独存储摄像头控制
						EquipmentControlLog result = new EquipmentControlLog();
						result.setData(logs);
						equipmentControlLogMongoDBDao.save(result);
					}
				}
			}
		}
		if (JudgeNullUtil.iList(configs) && JudgeNullUtil.iList(points)) {
			ControlModel model = null;
			for (ConfigText config : configs.get(0).getTexts()) {// 对比群控中的点位和设备下的实际点位指标类型是否存在
				for (EquipmentPoints point : points) {
					if (CommonUtils.isNotEmpty(config.getIndexType()) && CommonUtils.isNotEmpty(point.getIndexType())
							&& config.getIndexType().equals(point.getIndexType())) {
						// 照明灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							for (EquipmentPoints equipmentPoints : points) {
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
							}
							if (model.getXid() != null) {
								data.add(model);
							}
						} else if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							for (EquipmentPoints equipmentPoints : points) {
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_ON)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_OF);
								}
							}
							if (model.getXid() != null) {
								data.add(model);
							}
						}
						// 车道指示器
						// 正面
						if (config.getType().equals(CommonUtils.EQUIPMENTS_JUST)) {
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)// 正绿
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_ON);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								model.setXid(point.getXid());
								model.setValue(0);
								if (model.getXid() != null) {
									data.add(model);
								}
							}
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)// 正红
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_OF);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
							}
						}
						// 反面
						if (config.getType().equals(CommonUtils.EQUIPMENTS_BACK)) {
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)// 反红
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_ON);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								model.setXid(point.getXid());
								model.setValue(0);
								if (model.getXid() != null) {
									data.add(model);
								}
							}
							if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)// 反绿
									&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
								for (EquipmentPoints equipmentPoints : points) {
									model = new ControlModel();
									model.setDelayTime(configs.get(0).getDelayTime());
									if (equipmentPoints.getIndexType()
											.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {
										model.setXid(equipmentPoints.getXid());
										model.setValue(CommonUtils.INDEX_TYPE_OF);
										if (model.getXid() != null) {
											data.add(model);
										}
									}
								}
							}
						}
						// 交通信号灯
						// 黄灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HL)
										&& !equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								// 左转
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									for (ConfigText con : configs.get(0).getTexts()) {
										if (con.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
											model.setValue(con.getValue());
										}
									}
								}
								if (model.getXid() != null && model.getValue() != null) {
									data.add(model);
								}
							}
						}
						// 红灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HR)
										&& !equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								// 左转
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
									model.setXid(equipmentPoints.getXid());
									for (ConfigText con : configs.get(0).getTexts()) {
										if (con.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)) {
											model.setValue(con.getValue());
										}
									}
								}
								if (model.getXid() != null && model.getValue() != null) {
									data.add(model);
								}
							}
						}
						// 绿灯
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HG)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 隧道风机
						// 正转
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_FORWARD)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 反转
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_REVERSAL)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 停止
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_STOP)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 横洞卷帘门
						// 上升
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_RISE)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 下降
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (!equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_DECLINE)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						// 横洞指示器
						// 正面
						if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)// 开
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_OF) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType()
										.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_OF);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_GREEN)
										|| equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_RED)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						} else if (config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_HD_ONOFF)
								&& Integer.valueOf((Integer) config.getValue()) == CommonUtils.INDEX_TYPE_ON) {
							for (EquipmentPoints equipmentPoints : points) {
								model = new ControlModel();
								model.setDelayTime(configs.get(0).getDelayTime());
								if (equipmentPoints.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
										|| equipmentPoints.getIndexType()
												.equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
									model.setXid(equipmentPoints.getXid());
									model.setValue(CommonUtils.INDEX_TYPE_ON);
								}
								if (model.getXid() != null) {
									data.add(model);
								}
							}
						}
						if (!config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_LEFT)
								&& !config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_BACK_RED)
								&& !config.getIndexType().equals(CommonUtils.EQUIPMENTS_INDEXTYPE_JUST_GREEN)) {
							model = new ControlModel();
							model.setDelayTime(configs.get(0).getDelayTime());
							model.setXid(point.getXid());
							model.setValue(config.getValue());
							if (model.getXid() != null) {
								data.add(model);
							}
						}
					}
				}
			}
		}
		return data;
	}
}
