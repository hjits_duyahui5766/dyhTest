package com.cloudinnov.model;

public class SparepartPreventionCondition extends BaseModel{
    private Integer id;

    private Integer configId;

    private String formula;

    private Integer conditionWarningValue;

    private Integer status;

    private String createTime;

    private String lastUpdateTime;

    private String conditionWarningContent;
    
    private String stringConditionWarningValue;
    
    private int alarmConditionItemListLength;
    
    private int isHadWorkOrder;
    
    

   	public int getIsHadWorkOrder() {
   		return isHadWorkOrder;
   	}

   	public void setIsHadWorkOrder(int isHadWorkOrder) {
   		this.isHadWorkOrder = isHadWorkOrder;
   	}

    public int getAlarmConditionItemListLength() {
		return alarmConditionItemListLength;
	}

	public void setAlarmConditionItemListLength(int alarmConditionItemListLength) {
		this.alarmConditionItemListLength = alarmConditionItemListLength;
	}

	public String getStringConditionWarningValue() {
		return stringConditionWarningValue;
	}

	public void setStringConditionWarningValue(String stringConditionWarningValue) {
		this.stringConditionWarningValue = stringConditionWarningValue;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

	public Integer getConditionWarningValue() {
		return conditionWarningValue;
	}

	public void setConditionWarningValue(Integer conditionWarningValue) {
		this.conditionWarningValue = conditionWarningValue;
	}

	public String getConditionWarningContent() {
		return conditionWarningContent;
	}

	public void setConditionWarningContent(String conditionWarningContent) {
		this.conditionWarningContent = conditionWarningContent;
	}

}