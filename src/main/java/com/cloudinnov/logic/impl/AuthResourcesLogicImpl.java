package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthResourceDao;
import com.cloudinnov.dao.AuthRoleUserDao;
import com.cloudinnov.logic.AuthResourcesLogic;
import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthRoleUser;
import com.cloudinnov.model.AuthUsers;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月18日上午9:26:18
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class AuthResourcesLogicImpl extends BaseLogicImpl<AuthResource> implements AuthResourcesLogic {
	private int digit = 5;

	@Autowired
	private AuthResourceDao authResourcesDao;
	
	@Autowired
	private AuthRoleUserDao authRoleUserDao;

	@Override
	public int save(AuthResource res) {
		res.setCode(CodeUtil.resCode(digit));
		int returnCode = authResourcesDao.insert(res);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = authResourcesDao.insertInfo(res);
		} else {
			returnCode = 0;
		}
		return returnCode;
	}

	@Override
	public int saveOtherLanguage(AuthResource authResource) {
		return authResourcesDao.insertInfo(authResource);
	}

	@Override
	public List<AuthResource> selectResourceByUser(AuthUsers au) {
		AuthRoleUser roleUser = new AuthRoleUser();
		roleUser.setUserCode(au.getCode());
		roleUser.setCompanyCode(au.getCompanyCode());
		roleUser.setLanguage(au.getLanguage());
		List<AuthRoleUser> roleUsers = authRoleUserDao.selectListByCondition(roleUser);
		List<AuthResource> list = new ArrayList<AuthResource>();
		for(AuthRoleUser roleCode : roleUsers){
			au.setRoleCode(roleCode.getRoleCode());
			List<AuthResource> roles = authResourcesDao.selectResourceByUser(au);
			if(roles!=null && roles.size()>0){
				list.addAll(roles);
			}			
		}	
		List<AuthResource> resources = new ArrayList<AuthResource>();
		Set<Object> resourcesId = new HashSet<Object>();
		for (AuthResource resource : list) {
			 if(resourcesId.contains(resource.getSelId())){//资源ID存在则继续下次循环
				continue;
			}else{
				resources.add(resource);
			}
			resourcesId.add(resource.getSelId());//把资源ID存入set中
			resourcesId.add(resource.getUrl());//把资源ID存入set中
		}
		return resources;
	}

	@Override
	public List<AuthResource> selectTreeList(String roleCode, String language) {
		return authResourcesDao.selectTreeList(roleCode, language);
	}

	@Override
	public List<AuthResource> tableList(String[] codes, String language) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("codes", codes);
		return authResourcesDao.tableList(map);
	}

	@Override
	public List<AuthResource> selectSencondResourceByUser(Map<String, Object> map) {
		AuthRoleUser roleUser = new AuthRoleUser();
		roleUser.setUserCode(String.valueOf(map.get("code")));
		roleUser.setCompanyCode(String.valueOf(map.get("companyCode")));
		roleUser = authRoleUserDao.selectEntityByCondition(roleUser);
		map.put("roleCode", roleUser.getRoleCode());
		return authResourcesDao.selectSencondResourceByUser(map);
	}

	@Override
	public int updateOtherLanguage(AuthResource authResource) {
		return authResourcesDao.updateInfoByCondition(authResource);
	}

	@Override
	public int update(AuthResource res) {
		int returnCode = authResourcesDao.updateByCondition(res);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = authResourcesDao.updateInfoByCondition(res);
		} else {
			returnCode = 0;
		}
		return returnCode;
	}

	@Override
	public List<AuthResource> selectCommonsResource(AuthUsers au) {
		AuthRoleUser roleUser = new AuthRoleUser();
		roleUser.setUserCode(au.getCode());
		roleUser.setCompanyCode(au.getCompanyCode());
		roleUser.setLanguage(au.getLanguage());
		List<AuthRoleUser> roleUsers = authRoleUserDao.selectListByCondition(roleUser);
		List<AuthResource> list = new ArrayList<AuthResource>();
		for(AuthRoleUser roleCode : roleUsers){
			au.setRoleCode(roleCode.getRoleCode());
			au.setIsCommon(1);
			List<AuthResource> roles = authResourcesDao.selectResourceByUser(au);
			if(roles!=null && roles.size()>0){
				list.addAll(roles);
			}			
		}	
		List<AuthResource> resources = new ArrayList<AuthResource>();
		Set<Object> resourcesId = new HashSet<Object>();
		for (AuthResource resource : list) {
			if(resource.getParentId()==0){
				continue;
			} 
			if(resourcesId.contains(resource.getSelId())){//资源ID存在则继续下次循环
				continue;
			}else{
				resources.add(resource);
			}
			resourcesId.add(resource.getSelId());//把资源ID存入set中
			resourcesId.add(resource.getUrl());//把资源ID存入set中
		}
		return resources;
	}

	@Override
	public List<AuthResource> selectParentsResource(AuthUsers au) {
		AuthRoleUser roleUser = new AuthRoleUser();
		roleUser.setUserCode(au.getCode());
		roleUser.setCompanyCode(au.getCompanyCode());
		roleUser.setLanguage(au.getLanguage());
		List<AuthRoleUser> roleUsers = authRoleUserDao.selectListByCondition(roleUser);
		List<AuthResource> list = new ArrayList<AuthResource>();
		for(AuthRoleUser roleCode : roleUsers){
			au.setRoleCode(roleCode.getRoleCode());
			List<AuthResource> roles = authResourcesDao.selectResourceByUser(au);
			if(roles!=null && roles.size()>0){
				list.addAll(roles);
			}			
		}	
		List<AuthResource> resources = new ArrayList<AuthResource>();
		Set<Object> resourcesId = new HashSet<Object>();
		for (AuthResource resource : list) {
			if(resource.getParentId()!=0){
				continue;
			} 
			if(resourcesId.contains(resource.getSelId())){//资源ID存在则继续下次循环
				continue;
			}else{
				resources.add(resource);
			}
			resourcesId.add(resource.getSelId());//把资源ID存入set中
			resourcesId.add(resource.getUrl());//把资源ID存入set中
		}
		return resources;
	}

	@Override
	public List<AuthResource> selectChildrensResource(AuthUsers au, int parentId) {
		Map<String, Object> map = new HashMap<>();
		map.put("companyCode", au.getCustomerCode());
		map.put("parentId", parentId);
		map.put("code", au.getCode());
		map.put("language", au.getLanguage());
		AuthRoleUser roleUser = new AuthRoleUser();
		roleUser.setUserCode(au.getCode());
		roleUser.setCompanyCode(au.getCustomerCode());
		roleUser.setLanguage(au.getLanguage());
		List<AuthRoleUser> roleUsers = authRoleUserDao.selectListByCondition(roleUser);
		List<AuthResource> list = new ArrayList<AuthResource>();
		for(AuthRoleUser roleCode : roleUsers){
			map.put("roleCode", roleCode.getRoleCode());
			List<AuthResource> roles = authResourcesDao.selectSencondResourceByUser(map);
			if(roles!=null && roles.size()>0){
				list.addAll(roles);
			}			
		}	
		List<AuthResource> resources = new ArrayList<AuthResource>();
		Set<Object> resourcesId = new HashSet<Object>();
		for (AuthResource resource : list) {
			if(resource.getParentId()==0){
				continue;
			} 
			if(resourcesId.contains(resource.getSelId())){//资源ID存在则继续下次循环
				continue;
			}else{
				resources.add(resource);
			}
			resourcesId.add(resource.getSelId());//把资源ID存入set中
			resourcesId.add(resource.getUrl());//把资源ID存入set中
		}
		return resources;
	}
}
