package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.Trigger;
import com.cloudinnov.task.model.ScheduleJob;

/**
 * 触发器业务类
 * @ClassName: TiggerLogic
 * @Description: TODO
 * @author: ningmeng
 * @date: 2016年12月1日 下午3:04:37
 */
public interface TriggerLogic extends IBaseLogic<Trigger> {
	/**
	 * 根据触发器查询情报板群控方案,并且根据设备查询到情报板的配置信息,进行数据发送
	 * @Title: sendTextToInformationBoard
	 * @Description: TODO
	 * @param tiggerCode 触发器编码
	 * @param ScheduleJob 当前任务对象
	 * @return
	 * @return: int
	 */
	public int controlBoardSolutionByTiggercode(String tiggerCode, ScheduleJob model);
	/**
	 * 根据故障码查询触发器，并根据触发器查询到所涉及的情报板群控方案
	 * @param faultCode
	 * @param model
	 * @return
	 */
	public int controlBoardSolutionByFaultcode(String faultCode);
	public int updateOnOff(Trigger trigger);
	/**
	 * 根据触发器控制设备
	 * @param tigger
	 * @param code
	 * @return
	 */
	public int triggerControllerEquipments(Trigger tigger);
	public List<Trigger> selectTriggerByFaultCode();
}
