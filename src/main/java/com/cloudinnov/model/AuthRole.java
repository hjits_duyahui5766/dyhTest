package com.cloudinnov.model;

public class AuthRole extends BaseModel {

	private String companyCode;

	private Integer parentId;

	private Integer grade;

	private Integer orderId;
	
	private String name;

	private String comment;

	private String resourceCodesArray;
	
	private Integer roleType;
	
	private Integer deleteStatus;
	
	private Integer portal;
	
	private String rightCodesArray;

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public String getResourceCodesArray() {
		return resourceCodesArray;
	}

	public void setResourceCodesArray(String resourceCodesArray) {
		this.resourceCodesArray = resourceCodesArray;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Integer getPortal() {
		return portal;
	}

	public void setPortal(Integer portal) {
		this.portal = portal;
	}

	public String getRightCodesArray() {
		return rightCodesArray;
	}

	public void setRightCodesArray(String rightCodesArray) {
		this.rightCodesArray = rightCodesArray;
	}
	
	
}