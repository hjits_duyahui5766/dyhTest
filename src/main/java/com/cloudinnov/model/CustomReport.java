package com.cloudinnov.model;

public class CustomReport extends BaseModel {

	private String reportType;
	
	private String indexTypes;
	
	private String customers;
	
	private String equipments;
	
	private String areas;
	
	private String industries;

	private String time;
	
	private String accessRight;
	
	
	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getCustomers() {
		return customers;
	}

	public void setCustomers(String customers) {
		this.customers = customers;
	}

	public String getEquipments() {
		return equipments;
	}

	public void setEquipments(String equipments) {
		this.equipments = equipments;
	}

	public String getAreas() {
		return areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public String getIndustries() {
		return industries;
	}

	public void setIndustries(String industries) {
		this.industries = industries;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAccessRight() {
		return accessRight;
	}

	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}

	public String getIndexTypes() {
		return indexTypes;
	}

	public void setIndexTypes(String indexTypes) {
		this.indexTypes = indexTypes;
	}
	
	
}