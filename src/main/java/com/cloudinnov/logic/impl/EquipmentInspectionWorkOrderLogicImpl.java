package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentInspectionWorkOrderDao;
import com.cloudinnov.logic.EquipmentInspectionWorkOrderLogic;
import com.cloudinnov.model.EquipmentInspectionWorkOrder;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service("equipmentInspectionWorkOrderLogic")
public class EquipmentInspectionWorkOrderLogicImpl extends BaseLogicImpl<EquipmentInspectionWorkOrder>
		implements EquipmentInspectionWorkOrderLogic {
	
	@Autowired
	private EquipmentInspectionWorkOrderDao inspectionWorkOrderDao;
	
	public int save(EquipmentInspectionWorkOrder entity){
		String inspectionWordOrderCode = CodeUtil.inspectionWorkOrderCode(5);
		entity.setCode(inspectionWordOrderCode);
		entity.setStatus(CommonUtils.STATUS_NORMAL);
		entity.setOrderStatus(CommonUtils.STATUS_NORMAL);
		return inspectionWorkOrderDao.insert(entity);
	}
	
	public Page<EquipmentInspectionWorkOrder> list(int index, int size,EquipmentInspectionWorkOrder entity){
		PageHelper.startPage(index, size);
		Page<EquipmentInspectionWorkOrder> list = (Page<EquipmentInspectionWorkOrder>) inspectionWorkOrderDao.selectListByCondition(entity);
		return list;
	}
	
	public int update(EquipmentInspectionWorkOrder entity){
		entity.setOrderStatus(CommonUtils.ORDER_STATUS_HANDLE);
		return inspectionWorkOrderDao.updateByCondition(entity);
	}

	@Override
	public Page<EquipmentInspectionWorkOrder> search(int index, int size, EquipmentInspectionWorkOrder eiwo,
			String key) {
		PageHelper.startPage(index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		if(eiwo.getCustomerCode()!=null && !eiwo.getCustomerCode().equals(""))
		{
			map.put("customerCode", eiwo.getCustomerCode());
			map.put("customerCodes", eiwo.getCustomerCode().split(","));
		}
		if(eiwo.getProLineCode()!=null && !eiwo.getProLineCode().equals(""))
		{
			map.put("proLineCode", eiwo.getProLineCode());
			map.put("proLineCodes", eiwo.getProLineCode().split(","));
		}
		if(eiwo.getEquipmentCode()!=null && !eiwo.getEquipmentCode().equals(""))
		{
			map.put("equipmentCode", eiwo.getEquipmentCode());
			map.put("equipmentCodes", eiwo.getEquipmentCode().split(","));
		}
		
		map.put("keyword", key);
		map.put("orderstatus", eiwo.getOrderStatus());
		map.put("language", eiwo.getLanguage());
		Page<EquipmentInspectionWorkOrder> list = (Page<EquipmentInspectionWorkOrder>) inspectionWorkOrderDao.search(map);
		
		return list;
	}
}
