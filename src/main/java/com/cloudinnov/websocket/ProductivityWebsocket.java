package com.cloudinnov.websocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudinnov.utils.CommonUtils;

/**
 * 产能推送 webSocket
 * @author chengning
 * @date 2016年4月11日上午11:13:03
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@ServerEndpoint("/product/{customer}/{language}")
public class ProductivityWebsocket {
	private static Logger logger = LoggerFactory.getLogger(ProductivityWebsocket.class);
	private static final String FAULT_REDIS_SPILT = "_";
	// 静态变量，用来记录当前在线连接数。
	private static int onlineCount = 0;
	private String random;
	// 用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
	public static ConcurrentMap<String, ProductivityWebsocket> webSocketMap = new ConcurrentHashMap<String, ProductivityWebsocket>();
	// 用来发送数据
	private Session session;

	/**
	 * 打开连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnOpen
	public void onOpen(@PathParam("customer") String customer, @PathParam("language") String language,
			Session session) {
		this.session = session;
		this.random = CommonUtils.getUUID();
		webSocketMap.put(customer + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + random, this);
		// 连接人数加1
		addOnlineCount();
		logger.debug("产能推送开始连接:" + customer + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + random);
	}
	@OnMessage
	public void onMessage(@PathParam("customer") String customer, @PathParam("language") String language,
			String message) {
	}
	/**
	 * 异常时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnError
	public void onError(@PathParam("customer") String customer, @PathParam("language") String language,
			Throwable throwable, Session session) {
		logger.error("产能推送异常:" + customer + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + random);
		logger.error(throwable.getMessage(), throwable);
	}
	/**
	 * 关闭连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnClose
	public void onClose(@PathParam("customer") String customer, @PathParam("language") String language,
			Session session) {
		webSocketMap.remove(customer + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + random);
		// 在线数减1
		subOnlineCount();
		logger.debug("产能推送连接关闭:" + customer + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + random);
	}
	/**
	 * 推送消息
	 * @param message
	 * @throws IOException
	 */
	public synchronized void sendMessage(String message) throws IOException {
		this.session.getBasicRemote().sendText(message);
	}
	public static synchronized int getOnlineCount() {
		return onlineCount;
	}
	public static synchronized void addOnlineCount() {
		ProductivityWebsocket.onlineCount++;
	}
	public static synchronized void subOnlineCount() {
		ProductivityWebsocket.onlineCount--;
	}
}
