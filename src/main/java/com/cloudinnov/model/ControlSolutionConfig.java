package com.cloudinnov.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ControlSolutionConfig extends BaseModel {
	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String solutionCode;
	private String equipmentCode;
	private String equipmentClassify;// 设备分类
	@JsonIgnore
	private String sendContent;
	private Integer status;
	private boolean selectTrue;
	private String comment;
	private String createTime;
	private String lastUpdateTime;
	private String name;
	private String code;
	private Integer rowCount;
	private String color;
	private Integer displayType;
	private String categoryCode;
	private Integer solutionType;
	private String separator;
	@JsonIgnore
	private String config;
	@JsonIgnore
	private List<Config> textConfig;
	private Integer isRecover;
	private String tiggerCode;
	private Map<String, Object> configs;
	
	public boolean isSelectTrue() {
		return selectTrue;
	}
	public void setSelectTrue(boolean selectTrue) {
		this.selectTrue = selectTrue;
	}
	public Integer getIsRecover() {
		return isRecover;
	}
	public void setIsRecover(Integer isRecover) {
		this.isRecover = isRecover;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public Map<String, Object> getConfigs() {
		return configs;
	}
	public void setConfigs(Map<String, Object> configs) {
		this.configs = configs;
	}
	public Integer getRowCount() {
		return rowCount;
	}
	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Integer getDisplayType() {
		return displayType;
	}
	public void setDisplayType(Integer displayType) {
		this.displayType = displayType;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSolutionCode() {
		return solutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getEquipmentClassify() {
		return equipmentClassify;
	}
	public void setEquipmentClassify(String equipmentClassify) {
		this.equipmentClassify = equipmentClassify;
	}
	public String getSendContent() {
		return sendContent;
	}
	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getTiggerCode() {
		return tiggerCode;
	}
	public void setTiggerCode(String tiggerCode) {
		this.tiggerCode = tiggerCode;
	}
	public Integer getSolutionType() {
		return solutionType;
	}
	public void setSolutionType(Integer solutionType) {
		this.solutionType = solutionType;
	}
	public String getSeparator() {
		return separator;
	}
	public void setSeparator(String separator) {
		this.separator = separator;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	@JsonProperty("config")
	public List<Config> getTextConfig() {
		return textConfig;
	}
	public void setTextConfig(List<Config> textConfig) {
		this.textConfig = textConfig;
	}

	
	public static class Config {
		private String categoryCode;
		private String sendConent;
		private Integer delayTime;
		private List<ConfigText> texts;

		public String getCategoryCode() {
			return categoryCode;
		}
		public void setCategoryCode(String categoryCode) {
			this.categoryCode = categoryCode;
		}
		public String getSendConent() {
			return sendConent;
		}
		public void setSendConent(String sendConent) {
			this.sendConent = sendConent;
		}
		public List<ConfigText> getTexts() {
			return texts;
		}
		public void setTexts(List<ConfigText> texts) {
			this.texts = texts;
		}
		public Integer getDelayTime() {
			return delayTime;
		}
		public void setDelayTime(Integer delayTime) {
			this.delayTime = delayTime;
		}
	}

	public static class ConfigText {
		private String name;
		private Object value;
		private String type;
		private Object valueColor;
		private String indexType;

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Object getValueColor() {
			return valueColor;
		}
		public void setValueColor(Object valueColor) {
			this.valueColor = valueColor;
		}
		public String getIndexType() {
			return indexType;
		}
		public void setIndexType(String indexType) {
			this.indexType = indexType;
		}
	}

	@Override
	public String toString() {
		return "ControlSolutionConfig [id=" + id + ", solutionCode=" + solutionCode + ", equipmentCode=" + equipmentCode
				+ ", equipmentClassify=" + equipmentClassify + ", sendContent=" + sendContent + ", status=" + status
				+ ", comment=" + comment + ", createTime=" + createTime + ", lastUpdateTime=" + lastUpdateTime
				+ ", name=" + name + ", code=" + code + ", rowCount=" + rowCount + ", color=" + color + ", displayType="
				+ displayType + ", categoryCode=" + categoryCode + ", solutionType=" + solutionType + ", separator="
				+ separator + ", config=" + config + ", configs=" + configs + ", tiggerCode=" + tiggerCode + "]";
	}
}
