package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.EquipmentsCategoriesAttrLogic;
import com.cloudinnov.model.EquipmentsCategoryAttr;

@Controller
@RequestMapping("/webapi/equcategoryAttr")
public class EquipmentsCategoriesAttrController extends BaseController {

	@Autowired
	private EquipmentsCategoriesAttrLogic equipmentsCategoriesAttrLogic;

	@RequestMapping(value = "/selectByCategoryCode")
	@ResponseBody
	public void selectByCateCode(EquipmentsCategoryAttr equipmentsCategoryAttr, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<EquipmentsCategoryAttr> result = equipmentsCategoriesAttrLogic
				.selectByCategoryCode(equipmentsCategoryAttr);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}
}
