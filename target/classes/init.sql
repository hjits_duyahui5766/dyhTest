/*
Navicat MySQL Data Transfer

Source Server         : innov
Source Server Version : 50627
Source Host           : iotdev.cs37vcv27nht.rds.cn-north-1.amazonaws.com.cn:3306
Source Database       : cqew

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2017-01-05 21:38:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accident_work_order
-- ----------------------------
DROP TABLE IF EXISTS `accident_work_order`;
CREATE TABLE `accident_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `fault_code` varchar(20) NOT NULL,
  `description` text,
  `handing_suggestion` text,
  `first_time` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事故工单详情';

-- ----------------------------
-- Records of accident_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for alarm_log
-- ----------------------------
DROP TABLE IF EXISTS `alarm_log`;
CREATE TABLE `alarm_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `channel_code` varchar(20) NOT NULL COMMENT '故障通道编码',
  `fault_code` varchar(20) NOT NULL COMMENT '故障码',
  `first_time` varchar(20) NOT NULL COMMENT '发生时间',
  `work_order_code` varchar(20) DEFAULT NULL,
  `recovery_time` varchar(20) DEFAULT NULL COMMENT '恢复时间',
  `count` int(11) NOT NULL COMMENT '发生次数',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='设备告警日志表';

-- ----------------------------
-- Records of alarm_log
-- ----------------------------
INSERT INTO `alarm_log` VALUES ('1', 'EQB7HII', 'FCSEQHT', '10000', '1480067886732', 'FWTC77I', null, '5', '1', '1480067889000');
INSERT INTO `alarm_log` VALUES ('2', 'EQRUWQ1', 'FCG81H4', '10000', '1480476412772', 'FWGQ5ZF', null, '2', '1', '1480476420000');
INSERT INTO `alarm_log` VALUES ('3', 'EQRUWQ1', 'FCG81H4', '10001', '1480496993074', 'FWDXJAJ', null, '1', '1', '1480497000000');
INSERT INTO `alarm_log` VALUES ('4', 'EQRUWQ1', 'FCG81H4', '10002', '1480497014395', 'FWAVY45', null, '1', '1', '1480497022000');
INSERT INTO `alarm_log` VALUES ('5', 'EQRUWQ1', 'FCG81H4', '10003', '1480497304353', 'FWG21AT', null, '1', '1', '1480497312000');
INSERT INTO `alarm_log` VALUES ('6', 'EQRUWQ1', 'FCG81H4', '10004', '1480497309721', 'FWRRLX8', null, '1', '1', '1480497317000');
INSERT INTO `alarm_log` VALUES ('7', 'EQRUWQ1', 'FCG81H4', '10005', '1480497313678', 'FWSIHTH', null, '1', '1', '1480497321000');
INSERT INTO `alarm_log` VALUES ('8', 'EQRUWQ1', 'FCG81H4', '10006', '1480497317986', 'FWLGE9L', null, '1', '1', '1480497325000');
INSERT INTO `alarm_log` VALUES ('9', 'EQRUWQ1', 'FCG81H4', '10007', '1480497320596', 'FWK3S8F', null, '1', '1', '1480497328000');

-- ----------------------------
-- Table structure for alarm_rule
-- ----------------------------
DROP TABLE IF EXISTS `alarm_rule`;
CREATE TABLE `alarm_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `oem_code` varchar(20) NOT NULL COMMENT '设备制造商编码',
  `library_code` varchar(500) DEFAULT NULL COMMENT '对应故障码',
  `fault_code` varchar(500) NOT NULL COMMENT '对应故障码',
  `condition` varchar(500) DEFAULT NULL COMMENT '选择监控点位，可选择多个，格式为 point1,点1,>,50|point2,点2,=,1',
  `effect_time` varchar(20) DEFAULT NULL COMMENT '规则生效时间',
  `expire_time` varchar(20) DEFAULT NULL COMMENT '规则失效时间',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常 2-未启用 9-删除 ',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `on_off` int(11) NOT NULL,
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='告警规则表';

-- ----------------------------
-- Records of alarm_rule
-- ----------------------------
INSERT INTO `alarm_rule` VALUES ('1', 'ARXP97P', '吼吼', 'CUXM9D8', 'TL944B8', '告警故障码2', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":12},{\"are\":\"=\",\"omg\":12},{\"are\":\">\",\"omg\":3}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":2},{\"are\":\">\",\"omg\":3}]}]', '1306977900000', '1433294700000', '1', '你你刷复活了时间的浪费空间了凯撒发动机阿拉丁师傅将垃圾卢卡斯的浪费卡的', '0', '1480475672000', null);
INSERT INTO `alarm_rule` VALUES ('2', 'ARZ4GRS', '啦啦', 'CUXM9D8', 'TL944B8', '告警故障码2', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":3},{\"are\":\"<\",\"omg\":5}]}]', '1330560000000', '1467331200000', '9', '2134565', '0', '1480475679000', null);
INSERT INTO `alarm_rule` VALUES ('3', 'ARKAIAH', '4567', 'CUXM9D8', 'TLA4NJU', '10006', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":3}]}]', '1464714300000', '1469255400000', '9', 'wesrdfghbjm,./', '0', '1480582752000', null);
INSERT INTO `alarm_rule` VALUES ('4', 'ARIBRJZ', '1', 'CUXM9D8', 'TL944B8', '告警故障码2', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":1}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":2}]}]', '1296594300000', '1433395500000', '1', '1', '0', '1480744113000', null);
INSERT INTO `alarm_rule` VALUES ('5', 'AR5LQID', '请问请问', 'CUXM9D8', 'TL944B8', '告警故障码2', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":1}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":0}]}]', '1467340200000', '1464744300000', '1', '1', '0', '1480744805000', null);
INSERT INTO `alarm_rule` VALUES ('6', 'ARUJY56', '23', 'CUXM9D8', 'TLA4NJU', '10006', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":2}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":2}]}]', '1296680700000', '1433381100000', '9', '111', '0', '1480744836000', null);
INSERT INTO `alarm_rule` VALUES ('7', 'AR846A4', '23', 'CUXM9D8', 'TLA4NJU', '10004', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"<\",\"omg\":22333}]}]', '1330560000000', '1433116800000', '9', '1', '0', '1480744862000', null);
INSERT INTO `alarm_rule` VALUES ('8', 'ARABSJE', '触发火灾故障1111', 'CUXM9D8', 'TLA4NJU', '10006', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":10}]}]', '1480550400000', '1480550400000', '1', null, '0', '1480917173000', null);
INSERT INTO `alarm_rule` VALUES ('9', 'AR8RPEP', '1111', 'CUXM9D8', 'TL944B8', '10001', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":111}]}]', 'NaN', 'NaN', '9', null, '0', '1481860793000', null);
INSERT INTO `alarm_rule` VALUES ('10', 'ARDQYJ3', '123', 'CUXM9D8', 'undefined', '', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":22}]}]', '1481164200000', 'NaN', '1', null, '0', '1481868284000', null);
INSERT INTO `alarm_rule` VALUES ('11', 'ARD5MP6', '看看时间21211', 'CUXM9D8', 'undefined', '10007', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":11}]}]', '1481941500000', '1482580500000', '9', '看看时间是什么格式', '0', '1481870508000', null);
INSERT INTO `alarm_rule` VALUES ('12', 'AR1EZN8', '123123123', 'CUXM9D8', 'undefined', '10001', '[{\"code\":\"Dwa3ru5h\",\"name\":\"测试\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":3}]}]', '1481178600000', '', '9', null, '0', '1481881456000', null);
INSERT INTO `alarm_rule` VALUES ('13', 'AR4C722', '123123123', 'CUXM9D8', 'undefined', 'FANXL2B', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":22}]}]', '1481160300000', '', '1', '1111', '0', '1482130800000', null);
INSERT INTO `alarm_rule` VALUES ('14', 'ARIGC4N', '123', 'CUXM9D8', 'undefined', 'FAE3L1G', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":111}]}]', '', '', '9', '1111', '0', '1482131038000', null);
INSERT INTO `alarm_rule` VALUES ('15', 'ARABEPI', '规则32232332', 'CUXM9D8', '', '10006', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":12}]}]', '1481148600000', '1481869800000', '1', '玩儿玩儿', '0', '1482134368000', null);
INSERT INTO `alarm_rule` VALUES ('16', 'ARJZWL7', '23423423423423', 'CUXM9D8', '', 'FA9483K', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":33}]}]', '1481160600000', '1483199700000', '1', '234', '0', '1482134512000', null);
INSERT INTO `alarm_rule` VALUES ('17', 'ARUJFF1', '111', 'CUXM9D8', '', 'FA9483K', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":222}]}]', '', '', '9', '23', '0', '1482134575000', null);
INSERT INTO `alarm_rule` VALUES ('18', 'AR44519', '123', 'CUXM9D8', '', 'FAPLQJQ', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":33}]}]', '', '', '9', '认为二位', '0', '1482135251000', null);
INSERT INTO `alarm_rule` VALUES ('19', 'ARRJFB1', '规则', 'CUXM9D8', '', 'FANXL2B', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":12}]}]', '', '', '9', '1111', '0', '1482136529000', null);
INSERT INTO `alarm_rule` VALUES ('20', 'ARRJ3FM', '武器武器额外企鹅企鹅王wqqweqwewq', 'CUXM9D8', '', 'FAE3L1G', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":22}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":333}]}]', '1481178600000', '', '9', '111111', '0', '1482136975000', null);
INSERT INTO `alarm_rule` VALUES ('21', 'AREJIQQ', '啦啦啦请问去温泉', 'CUXM9D8', null, '11', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":11}]}]', '1481164200000', '1482403800000', '1', '111', '0', '1482138802000', null);
INSERT INTO `alarm_rule` VALUES ('22', 'ARP8HGN', '名称', 'CUXM9D8', null, '11', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":12}]}]', '', '', '1', '11111', '0', '1482139073000', null);
INSERT INTO `alarm_rule` VALUES ('23', 'ARPKBT3', '1111', 'CUXM9D8', null, 'FANXL2B', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":2222}]}]', '', '', '1', '11111', '0', '1482205805000', null);
INSERT INTO `alarm_rule` VALUES ('24', 'ARNZSQZ', '方案开关', 'CUXM9D8', null, 'FARZ9XL', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":12}]}]', '1481783400000', '1481783400000', '1', '1111', '0', '1482308503000', null);
INSERT INTO `alarm_rule` VALUES ('25', 'ARS7VDR', '1111', 'CUXM9D8', null, 'FAPLQJQ', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\"<\",\"omg\":333}]}]', '', '', '1', '对对对', '0', '1482308560000', null);
INSERT INTO `alarm_rule` VALUES ('26', 'ARKFU4S', 'ss', 'CUXM9D8', null, 'FANXL2B', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":34}]}]', '1481164200000', '1480736100000', '9', 'fasdfasasfasfdas', '1', '1482314257000', null);
INSERT INTO `alarm_rule` VALUES ('27', 'ARMZ54F', 'dd', 'CUXM9D8', null, 'FAPLQJQ', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"=\",\"omg\":4}]}]', '', '', '9', 'asdsasadsadsadas', '1', '1482315100000', null);
INSERT INTO `alarm_rule` VALUES ('28', 'ARBEXRE', '123123', 'CUXM9D8', null, 'FANXL2B', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":12}]}]', '', '', '9', '123123', '1', '1482316924000', null);
INSERT INTO `alarm_rule` VALUES ('29', 'ARDGQ5P', '123', 'CUXM9D8', null, 'FANXL2B', '[{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\">\",\"omg\":3}]}]', '', '', '9', 'asadasdasdsa', '0', '1482325746000', null);
INSERT INTO `alarm_rule` VALUES ('30', 'ARHWA7G', '111', 'CUXM9D8', null, 'FAE3L1G', '[{\"code\":\"Ahe66ang\",\"name\":\"测试点位\",\"rulesAddList\":[{\"are\":\">\",\"omg\":22}]},{\"code\":\"Dybfrx9y\",\"name\":\"测试单位1\",\"rulesAddList\":[{\"are\":\"<\",\"omg\":222}]}]', '', '', '1', null, '1', '1482326703000', null);

-- ----------------------------
-- Table structure for alarm_rule_point
-- ----------------------------
DROP TABLE IF EXISTS `alarm_rule_point`;
CREATE TABLE `alarm_rule_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `point_code` varchar(20) NOT NULL COMMENT '点位编码',
  `condition` varchar(500) NOT NULL COMMENT '选择监控点位，可选择多个，格式为>,50|=,1',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='告警规则点位配置表';

-- ----------------------------
-- Records of alarm_rule_point
-- ----------------------------
INSERT INTO `alarm_rule_point` VALUES ('1', 'ARXP97P', '测试点位', 'Ahe66ang', '>,12|=,12|>,3|', '1', '你你刷复活了时间的浪费空间了凯撒发动机阿拉丁师傅将垃圾卢卡斯的浪费卡的', '1480475673000', null);
INSERT INTO `alarm_rule_point` VALUES ('2', 'ARXP97P', '测试单位1', 'Dybfrx9y', '=,2|>,3|', '1', '你你刷复活了时间的浪费空间了凯撒发动机阿拉丁师傅将垃圾卢卡斯的浪费卡的', '1480475673000', null);
INSERT INTO `alarm_rule_point` VALUES ('6', 'ARIBRJZ', '测试点位', 'Ahe66ang', '=,1|', '1', '1', '1480744113000', null);
INSERT INTO `alarm_rule_point` VALUES ('7', 'ARIBRJZ', '测试单位1', 'Dybfrx9y', '>,2|', '1', '1', '1480744113000', null);
INSERT INTO `alarm_rule_point` VALUES ('8', 'AR5LQID', '测试点位', 'Ahe66ang', '=,1|', '1', '1', '1480744805000', null);
INSERT INTO `alarm_rule_point` VALUES ('9', 'AR5LQID', '测试单位1', 'Dybfrx9y', '>,0|', '1', '1', '1480744805000', null);
INSERT INTO `alarm_rule_point` VALUES ('26', 'ARDQYJ3', '测试单位1', 'Dybfrx9y', '=,22|', '1', null, '1482130766000', null);
INSERT INTO `alarm_rule_point` VALUES ('35', 'ARJZWL7', '测试点位', 'Ahe66ang', '>,33|', '1', '234', '1482134512000', null);
INSERT INTO `alarm_rule_point` VALUES ('39', 'AR4C722', '测试点位', 'Ahe66ang', '>,22|', '1', '1111', '1482136293000', null);
INSERT INTO `alarm_rule_point` VALUES ('42', 'ARABSJE', '测试点位', 'Ahe66ang', '>,10|', '1', null, '1482136732000', null);
INSERT INTO `alarm_rule_point` VALUES ('45', 'AREJIQQ', '测试点位', 'Ahe66ang', '>,11|', '1', '111', '1482138944000', null);
INSERT INTO `alarm_rule_point` VALUES ('46', 'ARP8HGN', '测试单位1', 'Dybfrx9y', '=,12|', '1', '11111', '1482139073000', null);
INSERT INTO `alarm_rule_point` VALUES ('47', 'ARABEPI', '测试点位', 'Ahe66ang', '=,12|', '1', '玩儿玩儿', '1482139937000', null);
INSERT INTO `alarm_rule_point` VALUES ('52', 'ARPKBT3', '测试点位', 'Ahe66ang', '>,2222|', '1', '11111', '1482205806000', null);
INSERT INTO `alarm_rule_point` VALUES ('53', 'ARNZSQZ', '测试点位', 'Ahe66ang', '=,12|', '1', '1111', '1482308503000', null);
INSERT INTO `alarm_rule_point` VALUES ('56', 'ARS7VDR', '测试点位', 'Ahe66ang', '<,333|', '1', '对对对', '1482309603000', null);
INSERT INTO `alarm_rule_point` VALUES ('58', 'ARKFU4S', '测试点位', 'Ahe66ang', '>,34|', '1', 'fasdfasasfasfdas', '1482314290000', null);
INSERT INTO `alarm_rule_point` VALUES ('64', 'ARHWA7G', '测试点位', 'Ahe66ang', '>,22|', '1', null, '1482326703000', null);
INSERT INTO `alarm_rule_point` VALUES ('65', 'ARHWA7G', '测试单位1', 'Dybfrx9y', '<,222|', '1', null, '1482326703000', null);

-- ----------------------------
-- Table structure for alarm_work_order
-- ----------------------------
DROP TABLE IF EXISTS `alarm_work_order`;
CREATE TABLE `alarm_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `section_code` varchar(20) NOT NULL,
  `code` varchar(50) NOT NULL,
  `customer_code` varchar(20) NOT NULL COMMENT '路段编码',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `fault_channel_code` varchar(20) NOT NULL COMMENT '公司编码',
  `fault_code` varchar(20) NOT NULL,
  `level` int(11) DEFAULT NULL COMMENT '告警级别1.严重2紧急3一般',
  `first_time` varchar(32) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `order_status` int(11) NOT NULL COMMENT '0 未接单 1处理中 2已处理',
  `padding_by` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '工单类型 1.设备工单 2.求助工单',
  `em_group_id` varchar(50) DEFAULT NULL COMMENT '环信群组Id',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of alarm_work_order
-- ----------------------------
INSERT INTO `alarm_work_order` VALUES ('1', 'CUP38LZ', 'FWTC77I', 'CUXM9D8', 'EQB7HII', 'FCSEQHT', '10000', '1', '1480067885473', null, '0', 'USDH71Y', null, null, '1', '1480067890000', null);
INSERT INTO `alarm_work_order` VALUES ('2', 'CUP38LZ', 'FW4D6C8', 'CUXM9D8', 'EQB7HII', 'FCSEQHT', '10000', '2', '1480067893568', null, '0', 'USDH71Y', null, null, '1', '1480067896000', null);
INSERT INTO `alarm_work_order` VALUES ('3', 'CUP38LZ', 'FW1SNG5', 'CUXM9D8', 'EQB7HII', 'FCSEQHT', '10000', '3', '1480067895023', null, '1', 'USDH71Y', null, null, '1', '1480067897000', null);
INSERT INTO `alarm_work_order` VALUES ('4', 'CUP38LZ', 'FWKW38C', 'CUXM9D8', 'EQB7HII', 'FCSEQHT', '10000', '1', '1480067895976', null, '2', 'USDH71Y', null, null, '1', '1480067898000', null);
INSERT INTO `alarm_work_order` VALUES ('5', 'CUP38LZ', 'FWZVE1X', 'CUXM9D8', 'EQB7HII', 'FCSEQHT', '10000', '1', '1480067896936', null, '1', 'USDH71Y', null, null, '1', '1480067899000', null);
INSERT INTO `alarm_work_order` VALUES ('6', 'CUXYEC3', 'FWGQ5ZF', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10000', '2', '1480476412730', null, '1', 'USDH71Y', null, null, '1', '1480476420000', null);
INSERT INTO `alarm_work_order` VALUES ('7', 'CUXYEC3', 'FW2KDNX', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10000', '1', '1480476454197', null, '2', 'USDH71Y', null, null, '1', '1480476461000', null);
INSERT INTO `alarm_work_order` VALUES ('8', 'CUXYEC3', 'FWDXJAJ', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10001', '2', '1480496993035', null, '0', 'USDH71Y', null, null, '1', '1480497000000', '1481689626409');
INSERT INTO `alarm_work_order` VALUES ('9', 'CUXYEC3', 'FWAVY45', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10002', '2', '1480497014212', null, '0', 'USDH71Y', null, null, '1', '1480497023000', null);
INSERT INTO `alarm_work_order` VALUES ('10', 'CUXYEC3', 'FWG21AT', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10003', '1', '1480497304335', null, '0', 'USDH71Y', null, null, '1', '1480497312000', null);
INSERT INTO `alarm_work_order` VALUES ('11', 'CUXYEC3', 'FWRRLX8', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10004', '2', '1480497309707', null, '1', 'USDH71Y', null, null, '1', '1480497317000', '1482131536849');
INSERT INTO `alarm_work_order` VALUES ('12', 'CUXYEC3', 'FWSIHTH', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10005', '2', '1480497313659', null, '1', 'USDH71Y', null, null, '1', '1480497321000', '1481689630956');
INSERT INTO `alarm_work_order` VALUES ('13', 'CUXYEC3', 'FWLGE9L', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10006', '1', '1480497317973', null, '2', 'USDH71Y', null, null, '1', '1480497325000', null);
INSERT INTO `alarm_work_order` VALUES ('14', 'CUXYEC3', 'FWK3S8F', 'CUXM9D8', 'EQRUWQ1', 'FCG81H4', '10007', '1', '1480497320579', null, '0', 'USDH71Y', null, null, '1', '1480497328000', null);

-- ----------------------------
-- Table structure for alarm_work_order_info
-- ----------------------------
DROP TABLE IF EXISTS `alarm_work_order_info`;
CREATE TABLE `alarm_work_order_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `description` text,
  `handing_suggestion` text,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of alarm_work_order_info
-- ----------------------------
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FW1SNG5', '雨天路滑，注意安全', '减速慢行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FW2KDNX', '向左滑动500米', '向右滑动', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FW4D6C8', '雨天路滑，注意安全', '减速慢行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWAVY45', '湿度多高', '除湿', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWDXJAJ', '温度过高', '降低温度', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWG21AT', '雨天路滑', '请减速慢行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWGQ5ZF', '向左滑动500米', '向右滑动', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWK3S8F', '照明故障', '请检查线路', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWKW38C', '雨天路滑，注意安全', '减速慢行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWLGE9L', '空调故障', '请检查线路', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWRRLX8', '前方道路塌方', '请绕道而行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWSIHTH', '风机故障', '请检查电路', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWTC77I', '雨天路滑，注意安全', '减速慢行', '未处理');
INSERT INTO `alarm_work_order_info` VALUES ('cn', 'FWZVE1X', '雨天路滑，注意安全', '减速慢行', '未处理');

-- ----------------------------
-- Table structure for alarm_work_user
-- ----------------------------
DROP TABLE IF EXISTS `alarm_work_user`;
CREATE TABLE `alarm_work_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(50) NOT NULL,
  `user_code` varchar(20) NOT NULL,
  `role` int(11) NOT NULL COMMENT '参与角色：1-责任人，2-参与者',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of alarm_work_user
-- ----------------------------
INSERT INTO `alarm_work_user` VALUES ('1', 'FWTC77I', 'USDH71Y', '2', '1', '1480067889000');
INSERT INTO `alarm_work_user` VALUES ('2', 'FW4D6C8', 'USDH71Y', '2', '1', '1480067896000');
INSERT INTO `alarm_work_user` VALUES ('3', 'FW1SNG5', 'USDH71Y', '2', '1', '1480067897000');
INSERT INTO `alarm_work_user` VALUES ('4', 'FWKW38C', 'USDH71Y', '2', '1', '1480067898000');
INSERT INTO `alarm_work_user` VALUES ('5', 'FWZVE1X', 'USDH71Y', '2', '1', '1480067899000');
INSERT INTO `alarm_work_user` VALUES ('6', 'FWGQ5ZF', 'USDH71Y', '2', '1', '1480476420000');
INSERT INTO `alarm_work_user` VALUES ('7', 'FW2KDNX', 'USDH71Y', '2', '1', '1480476461000');
INSERT INTO `alarm_work_user` VALUES ('8', 'FWDXJAJ', 'USDH71Y', '2', '1', '1480497000000');
INSERT INTO `alarm_work_user` VALUES ('9', 'FWAVY45', 'USDH71Y', '2', '1', '1480497022000');
INSERT INTO `alarm_work_user` VALUES ('10', 'FWG21AT', 'USDH71Y', '2', '1', '1480497312000');
INSERT INTO `alarm_work_user` VALUES ('11', 'FWRRLX8', 'USDH71Y', '2', '1', '1480497317000');
INSERT INTO `alarm_work_user` VALUES ('12', 'FWSIHTH', 'USDH71Y', '2', '1', '1480497321000');
INSERT INTO `alarm_work_user` VALUES ('13', 'FWLGE9L', 'USDH71Y', '2', '1', '1480497325000');
INSERT INTO `alarm_work_user` VALUES ('14', 'FWK3S8F', 'USDH71Y', '2', '1', '1480497328000');

-- ----------------------------
-- Table structure for attach
-- ----------------------------
DROP TABLE IF EXISTS `attach`;
CREATE TABLE `attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` varchar(20) NOT NULL COMMENT '附件类型：user-用户图片，equipment-设备图片，worderorder-工单处理图片',
  `object_id` int(11) DEFAULT NULL COMMENT '对象编号',
  `object_code` varchar(20) DEFAULT NULL COMMENT '对象编码',
  `name` varchar(50) DEFAULT NULL COMMENT '附件名称',
  `file_type` varchar(20) DEFAULT NULL COMMENT '文件类型',
  `size` varchar(20) DEFAULT NULL COMMENT '附件大小',
  `url` varchar(200) DEFAULT NULL,
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of attach
-- ----------------------------
INSERT INTO `attach` VALUES ('1', 'equipment', null, 'EQFSX1R', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/c043a30f70f84de093424fa839f3ca06.png', '1479461061000');
INSERT INTO `attach` VALUES ('2', 'equipment', null, 'EQ6VVK4', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/dcf662c657094778a4d3680f3e1c1be7.png', '1479461118000');
INSERT INTO `attach` VALUES ('3', 'equipment', null, 'EQFR8KN', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/3138410c87434bd2a6e994beacb90e04.png', '1479461271000');
INSERT INTO `attach` VALUES ('4', 'equipment', null, 'EQJ7UZ1', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/b705d735c5c14f2581f45f2f7ae72e29.png', '1479463441000');
INSERT INTO `attach` VALUES ('5', 'equipment', null, 'EQ5K3GU', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/239f3d761b954e27ad7c22c1b55c31c3.png', '1479786362000');
INSERT INTO `attach` VALUES ('6', 'equipment', null, 'EQTDYSW', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/a73782ab51f54f718fb26263ed27ee67.png', '1479797310000');
INSERT INTO `attach` VALUES ('7', 'equipment', null, 'EQ1PI8L', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/12374ad782824fa69bfcac450629a24c.png', '1479879934000');
INSERT INTO `attach` VALUES ('8', 'equipment', null, 'EQUBZ2M', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/e08441e42f3d4046826f8c74ea403482.png', '1479880198000');
INSERT INTO `attach` VALUES ('9', 'equipment', null, 'EQH498U', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/eae7d10973a4408a8744a4b0290c0b9f.png', '1479880255000');
INSERT INTO `attach` VALUES ('10', 'equipment', null, 'EQ8T43G', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/1d3e4966575d4782a588d07f3577fc50.png', '1479880503000');
INSERT INTO `attach` VALUES ('11', 'equipment', null, 'EQFCHNW', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/378d0f9921c74425b3777b2ee0f1328e.png', '1479880602000');
INSERT INTO `attach` VALUES ('12', 'equipment', null, 'EQ4ZQ16', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/6f30ae863b414d44b4d8e2a4113b77f7.png', '1479880876000');
INSERT INTO `attach` VALUES ('13', 'equipment', null, 'EQRD7BX', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/063c2d0398564049ad2d034b809dba5f.png', '1479890282000');
INSERT INTO `attach` VALUES ('14', 'equipment', null, 'EQHQ3B2', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/9f857976aad44f26b00bf229bae26ea0.png', '1479901620000');
INSERT INTO `attach` VALUES ('15', 'equipment', null, 'EQVG9RB', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/c784f322d43445a0815961d338a76a8e.png', '1479953981000');
INSERT INTO `attach` VALUES ('16', 'equipment', null, 'EQD5CDT', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/b7f439588d4c4dff9e8df5fda22d5561.png', '1479954491000');
INSERT INTO `attach` VALUES ('17', 'equipment', null, 'EQBUCAP', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/73e2ea7f1be94ddd86dfad1d3fe97733.png', '1479960673000');
INSERT INTO `attach` VALUES ('18', 'equipment', null, 'EQSBPJF', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/5874cedbe56d41a28fb6466d1f5c56dd.png', '1480041391000');
INSERT INTO `attach` VALUES ('19', 'equipment', null, 'EQYUV4R', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/cb3f19b65db0460f8aa8173f31ee88a7.png', '1480046051000');
INSERT INTO `attach` VALUES ('20', 'equipment', null, 'EQ4EJ95', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/1536f3e202ce49f0ba8876fd76893b21.png', '1480046133000');
INSERT INTO `attach` VALUES ('21', 'equipment', null, 'EQQ49HI', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/57bc550a9551455a8ee494dc484eab6b.png', '1480046437000');
INSERT INTO `attach` VALUES ('22', 'equipment', null, 'EQGBCLY', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/796d0a592a5840479e5b0e39be550cb2.png', '1480046830000');
INSERT INTO `attach` VALUES ('23', 'equipment', null, 'EQZ9MD8', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/a02cbe989f3e429892d0087e5d04ff27.png', '1480048733000');
INSERT INTO `attach` VALUES ('24', 'equipment', null, 'EQB7HII', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/f4d7a3081aa44e7eb6834beba69a319c.png', '1480061665000');
INSERT INTO `attach` VALUES ('25', 'equipment', null, 'EQYI15E', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/91f02ab949d7404aa9502ba874cb7f56.png', '1480061692000');
INSERT INTO `attach` VALUES ('26', 'equipment', null, 'EQZC3QK', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/2e063690127b42038741db6de7964c3d.png', '1480065571000');
INSERT INTO `attach` VALUES ('27', 'equipment', null, 'EQN6RXX', null, 'image', null, 'http://54.222.236.3:8000/upload/201611/5761bfb22aa2430990d69d5497a1aa6c.png', '1480069834000');
INSERT INTO `attach` VALUES ('28', 'equipment', null, 'EQ48PER', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/bd63d3d6cfdb4d369db9b2e95085acb9.png', '1480135616000');
INSERT INTO `attach` VALUES ('29', 'equipment', null, 'EQMN9LC', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/92223aebeff147389bba9e65d3eef7c6.png', '1480135645000');
INSERT INTO `attach` VALUES ('30', 'equipment', null, 'EQHRLZQ', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/8ec7a750617b4cbaa48f9904675d356d.png', '1480135757000');
INSERT INTO `attach` VALUES ('31', 'equipment', null, 'EQ1CU8E', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/4d0218c0ad1740e8b48eb04d23165a00.png', '1480135879000');
INSERT INTO `attach` VALUES ('32', 'equipment', null, 'EQDFJM9', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/563c91202e0147b7817b28585101ed36.png', '1480136003000');
INSERT INTO `attach` VALUES ('33', 'equipment', null, 'EQG4I6Y', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/7d4d5596fd3f472da261e1dc3bfd7bde.png', '1480136608000');
INSERT INTO `attach` VALUES ('34', 'equipment', null, 'EQBLR3R', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/cefee6789ff741a0982838b3de7c5e6c.png', '1480136624000');
INSERT INTO `attach` VALUES ('35', 'equipment', null, 'EQ7YSCS', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/b934a7769adb4a7e9ec8fa1eddfb9f97.png', '1480136642000');
INSERT INTO `attach` VALUES ('36', 'equipment', null, 'EQN14NT', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/2dd6d32f5da642a4bc3f4ef4e68116c1.png', '1480136657000');
INSERT INTO `attach` VALUES ('37', 'equipment', null, 'EQ8DZNN', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/838d3219c9614e599fd4e14f548ca80c.png', '1480136677000');
INSERT INTO `attach` VALUES ('38', 'equipment', null, 'EQBUGSG', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/f7edb9d3ce524c43b8405b75b1d77753.png', '1480136695000');
INSERT INTO `attach` VALUES ('39', 'equipment', null, 'EQT8QXS', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/202c61e41d634c888af0198834f2ec2a.png', '1480136712000');
INSERT INTO `attach` VALUES ('40', 'equipment', null, 'EQHXREK', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/6727ac480dc04499b6fd14a61c834025.png', '1480136731000');
INSERT INTO `attach` VALUES ('41', 'equipment', null, 'EQYA7SZ', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/6173ac8f762547fa90495bc901b2b30d.png', '1480136748000');
INSERT INTO `attach` VALUES ('42', 'equipment', null, 'EQ677AP', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/a771e9ea85b04516ac56c7c56ff29d7c.png', '1480137235000');
INSERT INTO `attach` VALUES ('43', 'equipment', null, 'EQ4U992', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/3c8b93257b9f4ebd9a3397899b3c8c81.png', '1480139093000');
INSERT INTO `attach` VALUES ('44', 'equipment', null, 'EQDQ4CC', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/ea9bf7df614448e1a661e694126aa5c4.png', '1480139109000');
INSERT INTO `attach` VALUES ('45', 'equipment', null, 'EQDW9GR', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/f2775c2311504b559e1d6c7a24edebb8.png', '1480139221000');
INSERT INTO `attach` VALUES ('46', 'equipment', null, 'EQUJGI8', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/ef50cfce1dc548cebed682d5e0ad7c78.png', '1480139240000');
INSERT INTO `attach` VALUES ('47', 'equipment', null, 'EQHE76P', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/960a364f88e6406c9dec44c892956919.png', '1480139256000');
INSERT INTO `attach` VALUES ('48', 'equipment', null, 'EQCPY4G', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/58517ce2b8654f84aded78b64dee345d.png', '1480139277000');
INSERT INTO `attach` VALUES ('49', 'equipment', null, 'EQQ735V', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/68021ab2175d4752b8a3d3dd1ddd1039.png', '1480139294000');
INSERT INTO `attach` VALUES ('50', 'equipment', null, 'EQHZPYC', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/0bc4df8dc15c4ce5bd557efd9939e506.png', '1480139314000');
INSERT INTO `attach` VALUES ('51', 'equipment', null, 'EQZMGKD', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/6c98e24174c44fcfbf0f6f2655140704.png', '1480139333000');
INSERT INTO `attach` VALUES ('52', 'equipment', null, 'EQGWGG8', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/2061ca25d51346bda77dde04b11d8289.png', '1480139352000');
INSERT INTO `attach` VALUES ('53', 'equipment', null, 'EQFAZUN', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/6142df32f4ff40cfb974f986651eb4ca.png', '1480139375000');
INSERT INTO `attach` VALUES ('54', 'equipment', null, 'EQFYDBF', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/bf90a7411a6a4bd084ae77ae72924b1a.png', '1480139391000');
INSERT INTO `attach` VALUES ('55', 'equipment', null, 'EQY7A5F', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/4f89d6a7ae1042eebeb260adf478c493.png', '1480139441000');
INSERT INTO `attach` VALUES ('56', 'equipment', null, 'EQRUWQ1', null, 'image', null, 'http://54.222.236.3:8002/upload/201611/89203c54b34949aaa8f5a485815c9786.png', '1480139457000');
INSERT INTO `attach` VALUES ('57', 'equipment', null, 'EQQ8L2R', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/eb8f9cc048ac4ff3b24f49ce2c1d86a6.png', '1480927665000');
INSERT INTO `attach` VALUES ('58', 'equipment', null, 'EQ5Z91M', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/075246e69ef84064aaa968c7470def68.png', '1481007948000');
INSERT INTO `attach` VALUES ('59', 'equipment', null, 'EQARHFX', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/3a7f742be8414979856fb0849724bda0.png', '1481008506000');
INSERT INTO `attach` VALUES ('60', 'equipment', null, 'EQRER2C', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/c932404e75094b4e907c3d85c30cc469.png', '1481009187000');
INSERT INTO `attach` VALUES ('61', 'equipment', null, 'EQWBK8A', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/45787516667e4e42a134a7eaf19ecc4e.png', '1481011584000');
INSERT INTO `attach` VALUES ('62', 'equipment', null, 'EQX7KTS', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/6eaa61e186804ca9bc525e9554824a54.png', '1481103885000');
INSERT INTO `attach` VALUES ('63', 'equipment', null, 'EQMM13V', null, 'image', null, 'http://54.222.236.3:8002/upload/201612/fbecddb9f8ac41379975a9e9e1003e01.png', '1481186263000');
INSERT INTO `attach` VALUES ('64', 'equipment', null, 'EQQQ2SD', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/0346c76123ed47cd96b5094c0e1b0342.png', '1481708741000');
INSERT INTO `attach` VALUES ('65', 'equipment', null, 'EQMVBF4', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/fc2b2179e5cd4ae480c1ff142fb8a814.png', '1481875133000');
INSERT INTO `attach` VALUES ('66', 'equipment', null, 'EQ6R6PJ', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/57d1f4ef74ed432a89c8c4e3526a1b70.png', '1481877943000');
INSERT INTO `attach` VALUES ('67', 'equipment', null, 'EQM1FNL', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/1892f8f9b57246aba7e1ce2c67df5b41.png', '1482111982000');
INSERT INTO `attach` VALUES ('68', 'equipment', null, 'EQ976WZ', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/c0480b6246b64b44825e91f6c45242be.png', '1482114939000');
INSERT INTO `attach` VALUES ('69', 'equipment', null, 'EQ3NG9Z', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/30c12cf5f46849b69aa5c95dd1df78ab.png', '1482116146000');
INSERT INTO `attach` VALUES ('70', 'equipment', null, 'EQI1JYC', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/5e99ee052d424f3b81b113fddf55f6f4.png', '1482204180000');
INSERT INTO `attach` VALUES ('71', 'equipment', null, 'EQPNVFC', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/d7f470d7efd441a59e49a58cb07f39ee.png', '1482209244000');
INSERT INTO `attach` VALUES ('72', 'equipment', null, 'EQFNFIU', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/7df1e09dc8814f87aea6944255517be1.png', '1482215086000');
INSERT INTO `attach` VALUES ('73', 'equipment', null, 'EQSAJ1J', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/778549f67ef44ad49c13521052da4794.png', '1482219083000');
INSERT INTO `attach` VALUES ('74', 'equipment', null, 'EQKB6L7', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/bbfaa62b4e3b40c7af02df279015696d.png', '1482223931000');
INSERT INTO `attach` VALUES ('75', 'equipment', null, 'EQHIPKF', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/b4c74c414a764b1dbc75c3d803ae9d22.png', '1482228391000');
INSERT INTO `attach` VALUES ('76', 'equipment', null, 'EQ2V8EG', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/6e1a89d8c6614fef8995d690a9df0ae8.png', '1482300586000');
INSERT INTO `attach` VALUES ('77', 'equipment', null, 'EQGIC49', null, 'image', null, 'http://54.222.236.3:8000/upload/201612/7bb2ab6959734b3e8f98701f9b1890b9.png', '1482301566000');
INSERT INTO `attach` VALUES ('78', 'equipment', null, 'EQR5IM2', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/2b276dabd7f4430eab5b113f3d97c4f2.png', '1482377031000');
INSERT INTO `attach` VALUES ('79', 'equipment', null, 'EQASVAS', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/469d32ca8ddc49c581ada13fe05f9bc4.png', '1482396206000');
INSERT INTO `attach` VALUES ('80', 'equipment', null, 'EQZZ7YY', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/44eda8c94ecc4052af044b34425ee55b.png', '1482990094000');
INSERT INTO `attach` VALUES ('81', 'equipment', null, 'EQKI57Q', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/7414b022f8bf4257bba68b8b641f18c9.png', '1482990395000');
INSERT INTO `attach` VALUES ('82', 'equipment', null, 'EQCJPPC', null, 'image', null, 'http://54.222.236.3:8003/upload/201612/c455d131673441989eb593da5186cbeb.png', '1483006928000');

-- ----------------------------
-- Table structure for auth_config
-- ----------------------------
DROP TABLE IF EXISTS `auth_config`;
CREATE TABLE `auth_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accesskey` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  `validity_period` datetime DEFAULT NULL,
  `oem_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_config
-- ----------------------------
INSERT INTO `auth_config` VALUES ('1', 'wwzgtest', 'wwzgtest', '2016-10-29 16:58:41', 'wwzg');
INSERT INTO `auth_config` VALUES ('2', 'ytyedzkj', 'ytyedzkj', '2017-10-29 16:58:41', 'ytye');
INSERT INTO `auth_config` VALUES ('3', 'nbhmkdzkj', 'nbhmkdzkj', '2017-10-29 16:58:41', 'hmk');

-- ----------------------------
-- Table structure for auth_dept
-- ----------------------------
DROP TABLE IF EXISTS `auth_dept`;
CREATE TABLE `auth_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `portal` int(11) DEFAULT NULL COMMENT '所属平台：1-管理门户，2-OEM门户，3-代理商门户，4-用户门户',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `code` varchar(20) NOT NULL,
  `parent_id` varchar(20) NOT NULL COMMENT '上级编号',
  `grade` int(11) NOT NULL COMMENT '级别',
  `order_id` int(11) DEFAULT NULL COMMENT '序号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of auth_dept
-- ----------------------------
INSERT INTO `auth_dept` VALUES ('1', '1', 'CUXM9D8', 'ADP7TBWS', '0', '1', null, '1', '1479090631000', '1480153677000');
INSERT INTO `auth_dept` VALUES ('2', '1', 'CUXM9D8', 'ADPDSMNA', '1', '2', null, '9', '1479090664000', null);
INSERT INTO `auth_dept` VALUES ('3', '1', 'CUXM9D8', 'ADP5A1B4', '1', '2', null, '1', '1479090859000', '1480153686000');
INSERT INTO `auth_dept` VALUES ('4', '1', 'CUXM9D8', 'ADPIWH9P', '1', '2', null, '9', '1479090949000', null);
INSERT INTO `auth_dept` VALUES ('5', '1', 'CUXM9D8', 'ADPU2IJU', '0', '1', null, '1', '1479091838000', '1480153731000');
INSERT INTO `auth_dept` VALUES ('6', '1', 'CUXM9D8', 'ADPBJ926', '0', '1', null, '9', '1479093122000', null);
INSERT INTO `auth_dept` VALUES ('7', '1', 'CUXM9D8', 'ADP32GXJ', '3', '3', null, '9', '1479093369000', '1479093718000');
INSERT INTO `auth_dept` VALUES ('8', '1', 'CUXM9D8', 'ADPYNZCK', '3', '3', null, '9', '1479093789000', '1479096995000');
INSERT INTO `auth_dept` VALUES ('9', '1', 'CUXM9D8', 'ADPSVTUV', '8', '4', null, '9', '1479094430000', null);
INSERT INTO `auth_dept` VALUES ('10', '1', 'CUXM9D8', 'ADP5794D', '9', '5', null, '1', '1479095468000', null);
INSERT INTO `auth_dept` VALUES ('11', '1', 'CUXM9D8', 'ADPRPJSJ', '0', '1', null, '9', '1479095477000', null);
INSERT INTO `auth_dept` VALUES ('12', '1', 'CUXM9D8', 'ADPRST9R', '0', '1', null, '9', '1479096233000', null);
INSERT INTO `auth_dept` VALUES ('13', '1', 'CUXM9D8', 'ADPK4ULB', '8', '4', null, '1', '1479096265000', null);
INSERT INTO `auth_dept` VALUES ('14', '1', 'CUXM9D8', 'ADPTS2SC', '8', '4', null, '1', '1479096305000', null);
INSERT INTO `auth_dept` VALUES ('15', '1', 'CUXM9D8', 'ADP25CFE', '0', '1', null, '9', '1479102963000', null);
INSERT INTO `auth_dept` VALUES ('16', '1', 'CUXM9D8', 'ADPHC3SQ', '8', '4', null, '1', '1479102973000', null);
INSERT INTO `auth_dept` VALUES ('17', '1', 'CUXM9D8', 'ADPG88F3', '16', '5', null, '1', '1479102983000', null);
INSERT INTO `auth_dept` VALUES ('18', '1', 'CUXM9D8', 'ADPISHV4', '7', '4', null, '9', '1479103003000', null);
INSERT INTO `auth_dept` VALUES ('19', '1', 'CUXM9D8', 'ADP8458R', '3', '3', null, '1', '1479112710000', '1480153715000');
INSERT INTO `auth_dept` VALUES ('20', '1', 'CUXM9D8', 'ADPFYVDX', '1', '2', null, '1', '1480153695000', null);
INSERT INTO `auth_dept` VALUES ('21', '1', 'CUXM9D8', 'ADP2JJRZ', '5', '4', null, '1', '1480153746000', '1480474578000');
INSERT INTO `auth_dept` VALUES ('22', '1', 'CUXM9D8', 'ADPRQNCX', '0', '1', null, '9', '1480906982000', null);

-- ----------------------------
-- Table structure for auth_dept_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_dept_info`;
CREATE TABLE `auth_dept_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门信息表';

-- ----------------------------
-- Records of auth_dept_info
-- ----------------------------
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP25CFE', '23', '23');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP2JJRZ', '管理一部', '管理一部');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP32GXJ', '啊实打实大', '阿斯达');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP5794D', '撒撒大声地', '啊实打实大师');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP5A1B4', '运营一部', '运营一部');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP7TBWS', '运营部', '运营部');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADP8458R', '技术一组', '技术一组');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPBJ926', '123', '123');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPDSMNA', '请问', '请问');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPFYVDX', '运营二部', '运营二部');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPG88F3', 'qweqwe', 'qweqweq');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPHC3SQ', 'qweqw', 'qweqwe');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPISHV4', 'fghfgh', 'fghfghfgh');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPIWH9P', '123', '123');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPK4ULB', '去问问企鹅全文', '请问请问');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPRPJSJ', '请问请问', '请问请问');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPRQNCX', '123123', '123123123');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPRST9R', '请问请问', '请问请问');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPSVTUV', '略略略', '略略略');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPTS2SC', '嘿嘿嘿', '嘿嘿嘿');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPU2IJU', '管理部', '管理部');
INSERT INTO `auth_dept_info` VALUES ('cn', 'ADPYNZCK', '周星驰', '啦啦啦');

-- ----------------------------
-- Table structure for auth_dept_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_dept_user`;
CREATE TABLE `auth_dept_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `dept_code` varchar(20) NOT NULL COMMENT '部门编号',
  `user_code` varchar(20) NOT NULL COMMENT '资源编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='部门用户关系表';

-- ----------------------------
-- Records of auth_dept_user
-- ----------------------------
INSERT INTO `auth_dept_user` VALUES ('1', 'CUXM9D8', 'ADP25CFE', 'US2V6NL', '9', '1479113338000');
INSERT INTO `auth_dept_user` VALUES ('2', 'CUXM9D8', 'ADPRST9R', 'US2V6NL', '9', '1479113338000');
INSERT INTO `auth_dept_user` VALUES ('3', 'CUXM9D8', 'ADP25CFE', 'US2V6NL', '1', '1479113352000');
INSERT INTO `auth_dept_user` VALUES ('4', 'CUXM9D8', 'ADP7TBWS', 'USX6GHJ', '9', '1481602956000');
INSERT INTO `auth_dept_user` VALUES ('5', 'CUXM9D8', 'ADP8458R', 'USX6GHJ', '9', '1481602956000');
INSERT INTO `auth_dept_user` VALUES ('6', 'CUXM9D8', 'ADP5A1B4', 'USX6GHJ', '9', '1481602956000');
INSERT INTO `auth_dept_user` VALUES ('7', 'CUXM9D8', 'ADPFYVDX', 'USX6GHJ', '9', '1481602956000');
INSERT INTO `auth_dept_user` VALUES ('8', 'CUXM9D8', 'ADPFYVDX', 'USX6GHJ', '1', '1481603144000');
INSERT INTO `auth_dept_user` VALUES ('9', 'CUXM9D8', 'ADP8458R', 'USX6GHJ', '1', '1481603144000');
INSERT INTO `auth_dept_user` VALUES ('10', 'CUXM9D8', 'ADP5A1B4', 'USX6GHJ', '1', '1481603144000');
INSERT INTO `auth_dept_user` VALUES ('11', 'CUXM9D8', 'ADP7TBWS', 'USX6GHJ', '1', '1481603144000');
INSERT INTO `auth_dept_user` VALUES ('12', 'CUXM9D8', 'ADP7TBWS', 'USYZAP4', '9', '1481603186000');
INSERT INTO `auth_dept_user` VALUES ('13', 'CUXM9D8', 'ADP8458R', 'USYZAP4', '9', '1481603186000');
INSERT INTO `auth_dept_user` VALUES ('14', 'CUXM9D8', 'ADP5A1B4', 'USYZAP4', '9', '1481603186000');
INSERT INTO `auth_dept_user` VALUES ('15', 'CUXM9D8', 'ADPFYVDX', 'USYZAP4', '9', '1481603186000');
INSERT INTO `auth_dept_user` VALUES ('16', 'CUXM9D8', 'ADPFYVDX', 'USYZAP4', '9', '1481603205000');
INSERT INTO `auth_dept_user` VALUES ('17', 'CUXM9D8', 'ADP8458R', 'USYZAP4', '9', '1481603205000');
INSERT INTO `auth_dept_user` VALUES ('18', 'CUXM9D8', 'ADP5A1B4', 'USYZAP4', '9', '1481603205000');
INSERT INTO `auth_dept_user` VALUES ('19', 'CUXM9D8', 'ADP7TBWS', 'USYZAP4', '9', '1481603205000');
INSERT INTO `auth_dept_user` VALUES ('20', 'CUXM9D8', 'ADPFYVDX', 'USYZAP4', '1', '1481603699000');
INSERT INTO `auth_dept_user` VALUES ('21', 'CUXM9D8', 'ADP8458R', 'USYZAP4', '1', '1481603699000');
INSERT INTO `auth_dept_user` VALUES ('22', 'CUXM9D8', 'ADP5A1B4', 'USYZAP4', '1', '1481603699000');
INSERT INTO `auth_dept_user` VALUES ('23', 'CUXM9D8', 'ADP7TBWS', 'USYZAP4', '1', '1481603699000');
INSERT INTO `auth_dept_user` VALUES ('24', 'CUXM9D8', 'ADP7TBWS', 'US2UUA9', '1', '1481603778000');
INSERT INTO `auth_dept_user` VALUES ('25', 'CUXM9D8', 'ADP8458R', 'US2UUA9', '1', '1481603778000');
INSERT INTO `auth_dept_user` VALUES ('26', 'CUXM9D8', 'ADP5A1B4', 'US2UUA9', '1', '1481603778000');
INSERT INTO `auth_dept_user` VALUES ('27', 'CUXM9D8', 'ADPFYVDX', 'US2UUA9', '1', '1481603778000');
INSERT INTO `auth_dept_user` VALUES ('28', 'CUXM9D8', 'ADPFYVDX', 'USU3XBV', '9', '1481604514000');
INSERT INTO `auth_dept_user` VALUES ('29', 'CUXM9D8', 'ADP8458R', 'USU3XBV', '9', '1481604514000');
INSERT INTO `auth_dept_user` VALUES ('30', 'CUXM9D8', 'ADP5A1B4', 'USU3XBV', '9', '1481604514000');
INSERT INTO `auth_dept_user` VALUES ('31', 'CUXM9D8', 'ADP7TBWS', 'USU3XBV', '9', '1481604514000');
INSERT INTO `auth_dept_user` VALUES ('32', 'CUXM9D8', 'ADPFYVDX', 'USU3XBV', '1', '1481604846000');
INSERT INTO `auth_dept_user` VALUES ('33', 'CUXM9D8', 'ADP8458R', 'USU3XBV', '1', '1481604846000');
INSERT INTO `auth_dept_user` VALUES ('34', 'CUXM9D8', 'ADP5A1B4', 'USU3XBV', '1', '1481604846000');
INSERT INTO `auth_dept_user` VALUES ('35', 'CUXM9D8', 'ADP7TBWS', 'USU3XBV', '1', '1481604846000');
INSERT INTO `auth_dept_user` VALUES ('36', 'CUXM9D8', 'ADPFYVDX', 'USDH71Y', '1', '1481706434000');
INSERT INTO `auth_dept_user` VALUES ('37', 'CUXM9D8', 'ADP8458R', 'USDH71Y', '1', '1481706434000');
INSERT INTO `auth_dept_user` VALUES ('38', 'CUXM9D8', 'ADP5A1B4', 'USDH71Y', '1', '1481706434000');
INSERT INTO `auth_dept_user` VALUES ('39', 'CUXM9D8', 'ADP7TBWS', 'USDH71Y', '1', '1481706434000');

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_info`;
CREATE TABLE `auth_group_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组信息';

-- ----------------------------
-- Records of auth_group_info
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_user`;
CREATE TABLE `auth_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `group_code` varchar(20) NOT NULL COMMENT '部门编号',
  `user_code` varchar(20) NOT NULL COMMENT '资源编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组用户关系表';

-- ----------------------------
-- Records of auth_group_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_module
-- ----------------------------
DROP TABLE IF EXISTS `auth_module`;
CREATE TABLE `auth_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `portal` int(11) DEFAULT NULL COMMENT '所属平台：1-管理门户，2-OEM门户，3-代理商门户，4-用户门户',
  `code` varchar(20) DEFAULT NULL COMMENT '编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='模块表';

-- ----------------------------
-- Records of auth_module
-- ----------------------------
INSERT INTO `auth_module` VALUES ('1', '1', 'AMU6MLBM', '1', '1480154211000', null);
INSERT INTO `auth_module` VALUES ('2', '1', 'AMUGP1T1', '1', '1480155130000', null);

-- ----------------------------
-- Table structure for auth_module_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_module_info`;
CREATE TABLE `auth_module_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模块信息表';

-- ----------------------------
-- Records of auth_module_info
-- ----------------------------
INSERT INTO `auth_module_info` VALUES ('cn', 'AMU6MLBM', '路段管理', '路段管理');
INSERT INTO `auth_module_info` VALUES ('cn', 'AMUGP1T1', '设备管理', '设备管理');

-- ----------------------------
-- Table structure for auth_resource
-- ----------------------------
DROP TABLE IF EXISTS `auth_resource`;
CREATE TABLE `auth_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `portal` int(11) DEFAULT NULL COMMENT '''所属平台：1-管理门户 2-OEM门户 3-代理商门户 4-用户门户 5-集成商门户''',
  `type` varchar(10) NOT NULL COMMENT '类型：admin-管理门户，user-用户门户',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `parent_id` int(11) NOT NULL COMMENT '上级编号',
  `grade` int(11) NOT NULL COMMENT '级别',
  `url` varchar(100) DEFAULT NULL COMMENT '链接',
  `order_id` int(11) DEFAULT NULL COMMENT '序号',
  `is_common` int(11) DEFAULT NULL COMMENT '是否常用菜单：0-否，1-是',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_resource
-- ----------------------------
INSERT INTO `auth_resource` VALUES ('1', 'REF3CR2', '1', 'admin', 'icon-screen-desktop', '0', '1', '#/page/monitor/center', '1', '0', '9', '1460614866000', '1471606175000');
INSERT INTO `auth_resource` VALUES ('2', 'RE3GD1Y', '1', 'admin', 'icon-bell', '0', '1', '#/page/workorder/list', '2', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('3', 'REHXDPW', '1', 'admin', 'icon-users', '0', '1', '#/page/agent/list', '3', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('4', 'REQJ7F4', '1', 'admin', 'icon-users', '0', '1', '#/page/customer/list', '4', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('5', 'REFSWCL', '1', 'admin', 'icon-speedometer', '0', '1', '#/page/equipment/list', '5', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('6', 'REVN2B9', '1', 'admin', 'icon-share', '0', '1', '#/page/productionline/list', '6', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('7', 'REN1AJ6', '1', 'admin', 'icon-bar-chart', '0', '1', '#/page/compare/list', '7', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('8', 'REW1R11', '1', 'admin', 'icon-pie-chart', '0', '1', '#/page/stat/list', '8', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('9', 'RE2TML4', '1', 'admin', 'icon-settings', '0', '1', '#/page/system/list', '10', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('10', 'RES3Y2A', '1', 'admin', 'fa fa-briefcase', '9', '2', '#/page/system/faulTranslateLibrary/list', '1', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('11', 'READLSH', '1', 'admin', 'fa fa-folder', '9', '2', '#/page/system/equcategory/list', '2', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('12', 'RE3Z9JM', '1', 'admin', 'fa fa-database', '9', '2', '#/page/system/resource/list', '3', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('13', 'REPL3QC', '1', 'admin', 'fa fa-user', '9', '2', '#/page/system/role/list', '4', '1', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('14', 'RE6C3LH', '1', 'admin', 'fa fa-user', '9', '2', '#/page/system/user/list', '5', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('15', 'REJR63X', '1', 'admin', 'fa fa-globe', '9', '2', '#/page/system/area/list', '6', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('16', 'RE8EAF8', '1', 'admin', 'fa fa-shopping-cart', '9', '2', '#/page/system/material/list', '7', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('17', 'RE6U41F', '1', 'admin', 'fa fa-shopping-cart', '9', '2', '#/page/system/costconfig/list', '8', '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('18', 'RES7JRY', '1', 'admin', 'fa fa-list', '9', '2', '#/page/system/systemParameter/list', '9', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('19', 'RE64T8E', '1', 'admin', 'fa fa-info-circle', '9', '2', '#/page/system/systemlog/list', '10', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('20', 'RE6ILBJ', '4', 'user', 'icon-wrench', '0', '1', '#/page/workbench/list', '1', '0', '1', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('21', 'REU68BA', '4', 'user', 'icon-share', '0', '1', '#/page/monitor/list', null, '0', '9', '1460614866000', null);
INSERT INTO `auth_resource` VALUES ('22', 'RE3H19G', '4', 'user', 'icon-bell', '0', '1', '#/page/workorder/list', '2', '0', '1', '1460614866000', '1472032851000');
INSERT INTO `auth_resource` VALUES ('23', 'REC5WYR', '4', 'user', 'icon-speedometer', '0', '1', '#/page/equipment/list', '3', '0', '1', '1460614866000', '1472032858000');
INSERT INTO `auth_resource` VALUES ('24', 'REDPI5E', '4', 'user', 'icon-bar-chart', '0', '1', '#/page/productionline/flow', '4', '0', '1', '1460614866000', '1472032864000');
INSERT INTO `auth_resource` VALUES ('25', 'REYPFP2', '4', 'user', 'icon-settings', '0', '1', '#/page/system/list', '10', '0', '1', '1460614866000', '1472032894000');
INSERT INTO `auth_resource` VALUES ('26', 'REU9NJL', '4', 'user', 'icon-pie-chart', '0', '1', '#/page/stat/list', '5', '0', '1', '1460874950000', null);
INSERT INTO `auth_resource` VALUES ('27', 'REHTIA2', '4', 'user', 'fa fa-user', '25', '2', '#/page/system/role/list', '1', '1', '1', '1460876158000', '1472032911000');
INSERT INTO `auth_resource` VALUES ('28', 'REQTSH6', '4', 'user', 'fa fa-user', '25', '2', '#/page/system/user/list', '2', '0', '1', '1460876181000', '1472032919000');
INSERT INTO `auth_resource` VALUES ('29', 'REBCMII', '4', 'user', 'fa fa-shopping-cart', '25', '2', '#/page/system/product/list', '3', '0', '9', '1460876197000', '1472032928000');
INSERT INTO `auth_resource` VALUES ('30', 'REH2K9X', '1', 'admin', 'fa fa-list-alt fa-file-text-o', '0', '1', '#/page/kb/list', '9', '0', '9', '1460891640000', '1461222630000');
INSERT INTO `auth_resource` VALUES ('31', 'RE21B7D', '4', 'user', 'fa fa-list-alt fa-file-text-o', '0', '1', '#/page/kb/list', '6', '0', '9', '1460891893000', null);
INSERT INTO `auth_resource` VALUES ('32', 'RE1CPBM', '4', 'user', 'fa fa-shopping-cart', '25', '2', '#/page/system/cost/list', '4', '0', '9', '1461228122000', '1472032935000');
INSERT INTO `auth_resource` VALUES ('33', 'RE31S2J', '4', 'user', 'fa fa-shopping-cart', '25', '2', '#/page/system/productionSchedule/list', '5', '0', '9', '1461228157000', '1472032939000');
INSERT INTO `auth_resource` VALUES ('34', 'RETZVYA', '1', 'admin', 'icon-users', '0', '1', '#/page/oem/list', null, '0', '1', '1463039915000', null);
INSERT INTO `auth_resource` VALUES ('35', 'REGEYUS', '1', 'admin', 'icon-users', '0', '1', '#/page/integrator/list', null, '0', '1', '1464933622000', null);
INSERT INTO `auth_resource` VALUES ('36', 'REDGENF', '1', 'admin', 'fa fa-wrench', '0', '1', '#/page/inspection/list', '5', '0', '9', '1466146811000', null);
INSERT INTO `auth_resource` VALUES ('37', 'RE8GI6X', '1', 'admin', 'fa fa-wrench', '36', '2', '#/page/inspection/config', '1', '0', '1', '1466146842000', null);
INSERT INTO `auth_resource` VALUES ('38', 'RE71K8J', '1', 'admin', 'fa fa-wrench', '36', '2', '#/page/inspection/job', '2', '0', '1', '1466146867000', null);
INSERT INTO `auth_resource` VALUES ('39', 'REF8J7L', '1', 'admin', 'fa fa-delicious', '9', '2', '#/page/system/sim/list', '11', '0', '1', '1466566522000', '1466566983000');
INSERT INTO `auth_resource` VALUES ('40', 'REDXWVT', '1', 'admin', 'fa fa-child', '9', '2', '#/page/system/bom/list', '12', '0', '9', '1467688857000', null);
INSERT INTO `auth_resource` VALUES ('41', 'RET98UK', '4', 'user', 'icon-bar-chart', '0', '4', '#/page/productionline/flow', '7', '0', '9', '1468663649000', '1472032874000');
INSERT INTO `auth_resource` VALUES ('42', 'REQ51ZS', '4', 'user', 'icon-speedometer', '0', '1', '#/page/equipment/list', '8', '0', '9', '1468663676000', '1472032881000');
INSERT INTO `auth_resource` VALUES ('43', 'REH8AFA', '4', 'user', 'icon-bell', '0', '1', '#/page/workorder/list', '9', '0', '9', '1468663685000', '1472032886000');
INSERT INTO `auth_resource` VALUES ('64', 'RENENHW', '2', 'oem', 'icon-screen-desktop', '0', '1', '#/page/monitor/center', '1', '0', '9', '1471339697000', '1472021161000');
INSERT INTO `auth_resource` VALUES ('65', 'REYD8LQ', '5', 'integrator', null, '0', '1', null, '1', '0', '1', '1471339728000', '1472033010000');
INSERT INTO `auth_resource` VALUES ('66', 'REMYC5F', '3', 'agent', null, '0', '1', null, '1', '0', '1', '1471339746000', '1472032994000');
INSERT INTO `auth_resource` VALUES ('67', 'RE3CQF9', '3', 'agent', null, '0', '1', null, '2', '0', '1', '1471973504000', '1472033000000');
INSERT INTO `auth_resource` VALUES ('68', 'REWESEE', '5', 'integrator', null, '0', '1', null, '2', '0', '1', '1471973684000', '1472033014000');
INSERT INTO `auth_resource` VALUES ('69', 'RERMXJR', '2', 'oem', 'icon-bell', '0', '1', '#/page/workOrderMain/list', '2', '0', '9', '1472007522000', '1474959715000');
INSERT INTO `auth_resource` VALUES ('70', 'RE8RTH8', '2', 'oem', 'icon-users', '0', '1', '#/page/agent/list', '3', '0', '9', '1472019614000', '1472021369000');
INSERT INTO `auth_resource` VALUES ('71', 'RE6FIJB', '2', 'oem', 'icon-users', '0', '1', '#/page/customer/list', '4', '0', '9', '1472019625000', '1472021396000');
INSERT INTO `auth_resource` VALUES ('72', 'RE5QINV', '2', 'oem', 'icon-speedometer', '0', '1', '#/page/equipment/list', '5', '0', '9', '1472019638000', '1472021417000');
INSERT INTO `auth_resource` VALUES ('73', 'REYH3JV', '2', 'oem', 'icon-users', '0', '1', '#/page/inspection/list', '6', '0', '9', '1472019654000', '1475203355000');
INSERT INTO `auth_resource` VALUES ('74', 'REQ8NT1', '2', 'oem', 'icon-share', '0', '1', '#/page/productionline/list', '7', '0', '9', '1472019668000', '1472021944000');
INSERT INTO `auth_resource` VALUES ('75', 'REXVYQ2', '2', 'oem', 'icon-bar-chart', '0', '1', '#/page/compare/list', '8', '0', '9', '1472019682000', '1472021990000');
INSERT INTO `auth_resource` VALUES ('76', 'RE7ZQ8A', '2', 'oem', 'icon-pie-chart', '0', '1', '#/page/stat/list', '9', '0', '9', '1472019690000', '1472021997000');
INSERT INTO `auth_resource` VALUES ('77', 'REM3YSL', '2', 'oem', 'fa fa-list-alt fa-file-text-o', '0', '1', '#/page/kb/list', '10', '0', '9', '1472019697000', '1472022003000');
INSERT INTO `auth_resource` VALUES ('78', 'REQ4U3D', '2', 'oem', 'icon-settings', '0', '1', '#/page/system/list', '11', '0', '9', '1472019709000', '1472022008000');
INSERT INTO `auth_resource` VALUES ('79', 'REXC74E', '2', 'oem', 'fa fa-briefcase', '78', '2', '#/page/system/faulTranslateLibrary/list', '1', '0', '9', '1472022428000', null);
INSERT INTO `auth_resource` VALUES ('80', 'REIC9XI', '2', 'oem', 'fa fa-folder', '78', '2', '#/page/system/equcategory/list', '2', '0', '9', '1472022566000', null);
INSERT INTO `auth_resource` VALUES ('81', 'REMCRYZ', '2', 'oem', 'fa fa-user', '78', '2', '#/page/system/user/list', '3', '0', '9', '1472022614000', null);
INSERT INTO `auth_resource` VALUES ('82', 'RE2A8K9', '2', 'oem', 'fa fa-globe', '78', '2', '#/page/system/area/list', '4', '0', '9', '1472022669000', null);
INSERT INTO `auth_resource` VALUES ('83', 'REQ6ZNE', '2', 'oem', 'fa fa-shopping-cart', '78', '2', '#/page/system/material/list', '5', '0', '9', '1472022710000', null);
INSERT INTO `auth_resource` VALUES ('84', 'REG6AK8', '2', 'oem', 'fa fa-shopping-cart', '78', '2', '#/page/system/costconfig/list', '6', '0', '9', '1472022740000', '1472022755000');
INSERT INTO `auth_resource` VALUES ('85', 'RE25G1T', '2', 'oem', 'fa fa-list', '78', '2', '#/page/system/systemParameter/list', '7', '0', '9', '1472022798000', null);
INSERT INTO `auth_resource` VALUES ('86', 'REABDKE', '2', 'oem', 'fa fa-info-circle', '78', '2', '#/page/system/systemlog/list', '8', '0', '9', '1472022825000', '1472022833000');
INSERT INTO `auth_resource` VALUES ('87', 'RE9G65K', '2', 'oem', 'fa fa-wrench', '73', '2', '#/page/inspection/config', '1', '0', '9', '1472023046000', '1472023052000');
INSERT INTO `auth_resource` VALUES ('88', 'REABM68', '2', 'oem', 'fa fa-wrench', '73', '2', '#/page/inspection/job', '2', '0', '9', '1472023073000', null);
INSERT INTO `auth_resource` VALUES ('89', 'REIFNAG', '2', 'oem', 'fa fa-wrench', '78', '2', '#/page/system/equModel/list', '3', '0', '9', '1473132775000', null);
INSERT INTO `auth_resource` VALUES ('90', 'RE3YK1B', '2', 'oem', 'fa fa-wrench', '78', '2', '#/page/system/equModel/list', '3', '0', '9', '1473132831000', null);
INSERT INTO `auth_resource` VALUES ('91', 'REVY5UA', '2', 'oem', 'fa fa-child', '78', '2', '#/page/system/bom/list', '5', '0', '9', '1473404738000', '1474882213000');
INSERT INTO `auth_resource` VALUES ('92', 'RE33PQQ', '5', 'integrator', '1', '0', '1', '12', '21', '0', '1', '1474609654000', null);
INSERT INTO `auth_resource` VALUES ('93', 'RE4A66S', '2', 'oem', '3', '64', '2', '2', '4', '0', '9', '1474611104000', null);
INSERT INTO `auth_resource` VALUES ('94', 'REJJJRK', '2', 'oem', '3', '64', '2', '2', '4', '0', '9', '1474616810000', null);
INSERT INTO `auth_resource` VALUES ('95', 'REVGC3P', '5', 'integrator', '21212', '65', '2', '2121212', '1111', '0', '1', '1474703418000', null);
INSERT INTO `auth_resource` VALUES ('96', 'REU3BAM', '1', 'admin', 'fa fa-shopping-cart', '9', '2', '#/page/system/indicators/list', '9', '0', '1', '1474857688000', '1474857964000');
INSERT INTO `auth_resource` VALUES ('97', 'REZT69Q', '1', 'admin', 'fa fa-child', '9', '2', '#/page/system/lndustry/list', '10', '0', '1', '1474881958000', '1474881984000');
INSERT INTO `auth_resource` VALUES ('98', 'RECPALD', '2', 'oem', 'icon-users', '69', '2', '#/page/workOrderMain/workorder/list', '1', '0', '1', '1474954215000', '1475044871000');
INSERT INTO `auth_resource` VALUES ('99', 'RE25MRN', '2', 'oem', 'fa fa-wrench', '69', '2', '#/page/workOrderMain/routInspection/list', '2', '0', '1', '1474955823000', '1475048869000');
INSERT INTO `auth_resource` VALUES ('100', 'REB712G', '2', 'oem', 'icon-bell', '69', '2', '#/page/workOrderMain/maintain/list', '3', '0', '1', '1474955883000', '1475044905000');
INSERT INTO `auth_resource` VALUES ('101', 'RE9IAF5', '2', 'oem', 'icon-eye', '0', '1', null, '1', '0', '1', '1476340197000', '1476352559000');
INSERT INTO `auth_resource` VALUES ('102', 'REB22VF', '2', 'oem', 'icon-direction', '101', '2', '#/page/monitor/center', '2', '1', '1', '1476340217000', '1482109208000');
INSERT INTO `auth_resource` VALUES ('103', 'REUDHKN', '2', 'oem', 'icon-users', '0', '1', null, '9', '0', '9', '1476340231000', '1479997402000');
INSERT INTO `auth_resource` VALUES ('104', 'RE83QZE', '2', 'oem', 'icon-users', '103', '2', '', '1', '0', '9', '1476340250000', '1476352607000');
INSERT INTO `auth_resource` VALUES ('105', 'RE8KXT7', '2', 'oem', 'icon-user-follow', '104', '3', '#/page/customer/add', '1', '0', '1', '1476340268000', '1476353934000');
INSERT INTO `auth_resource` VALUES ('106', 'RELA7LX', '2', 'oem', 'icon-users', '104', '3', '#/page/customer/list', '2', '0', '1', '1476340287000', '1476353950000');
INSERT INTO `auth_resource` VALUES ('107', 'REIJMXJ', '2', 'oem', 'icon-screen-desktop', '103', '2', null, '2', '0', '1', '1476340299000', '1476352618000');
INSERT INTO `auth_resource` VALUES ('108', 'REPYVUJ', '2', 'oem', 'icon-plus', '107', '3', '#/page/equipment/add', '1', '0', '1', '1476340370000', '1479043890000');
INSERT INTO `auth_resource` VALUES ('109', 'RE5RC73', '2', 'oem', 'icon-list', '107', '3', '#/page/equipment/list', '2', '0', '1', '1476340385000', '1476354011000');
INSERT INTO `auth_resource` VALUES ('110', 'REUSQ38', '2', 'oem', 'icon-grid', '107', '3', '#/page/system/equcategory/list', '3', '0', '1', '1476340401000', '1476354122000');
INSERT INTO `auth_resource` VALUES ('111', 'RE13IH7', '2', 'oem', 'icon-layers', '107', '3', '#/page/system/equModel/list', '4', '0', '1', '1476340417000', '1476354353000');
INSERT INTO `auth_resource` VALUES ('112', 'REQ1RI5', '2', 'oem', 'icon-puzzle', '107', '3', '#/page/system/bom/list', '5', '0', '1', '1476340486000', '1476354211000');
INSERT INTO `auth_resource` VALUES ('113', 'RERTSV7', '2', 'oem', 'icon-link', '103', '2', null, '3', '0', '1', '1476340504000', '1476352681000');
INSERT INTO `auth_resource` VALUES ('114', 'REGDLBL', '2', 'oem', 'icon-plus', '113', '3', '#/page/productionline/add', '1', '0', '1', '1476340520000', '1476354225000');
INSERT INTO `auth_resource` VALUES ('115', 'RESMGQQ', '2', 'oem', 'icon-list', '113', '3', '#/page/productionline/list', '2', '0', '1', '1476340536000', '1476354238000');
INSERT INTO `auth_resource` VALUES ('116', 'REJDMR4', '2', 'oem', 'icon-wrench', '0', '1', null, '3', '0', '1', '1476340546000', '1481853463000');
INSERT INTO `auth_resource` VALUES ('117', 'RECE2F1', '2', 'oem', 'icon-list', '116', '2', '#/page/workOrderMain/workorder/list', '3', '0', '1', '1476340563000', '1479997489000');
INSERT INTO `auth_resource` VALUES ('118', 'RE9KHQI', '2', 'oem', 'icon-calendar', '116', '2', null, '5', '0', '1', '1476340574000', '1479997521000');
INSERT INTO `auth_resource` VALUES ('119', 'REXUJEN', '2', 'oem', 'icon-plus', '118', '3', '#/page/inspection/config/add', '1', '0', '1', '1476340586000', '1479192439000');
INSERT INTO `auth_resource` VALUES ('120', 'REX9DCR', '2', 'oem', 'icon-list', '118', '3', '#/page/inspection/list', '2', '0', '1', '1476340599000', '1476354692000');
INSERT INTO `auth_resource` VALUES ('121', 'RE858N3', '2', 'oem', 'icon-wrench', '116', '2', null, '4', '0', '1', '1476340618000', '1479997531000');
INSERT INTO `auth_resource` VALUES ('122', 'REDV46J', '2', 'oem', 'icon-bar-chart', '0', '1', null, '5', '0', '1', '1476340627000', '1479997770000');
INSERT INTO `auth_resource` VALUES ('123', 'RE1GADU', '2', 'oem', 'icon-bar-chart', '122', '2', '#/page/compare/list', '8', '0', '9', '1476340645000', '1479997599000');
INSERT INTO `auth_resource` VALUES ('124', 'REWJ61S', '2', 'oem', 'icon-graph', '122', '2', '#/page/stat/capacity/year', '2', '0', '9', '1476340662000', '1476354583000');
INSERT INTO `auth_resource` VALUES ('125', 'REZP81H', '2', 'oem', 'icon-pie-chart', '122', '2', '#/page/stat/fault/year', '3', '0', '9', '1476340681000', '1476354545000');
INSERT INTO `auth_resource` VALUES ('126', 'REHSF6Y', '2', 'oem', 'icon-setting', '0', '1', null, '5', '0', '1', '1476340693000', '1480908116000');
INSERT INTO `auth_resource` VALUES ('127', 'RE1CFT6', '2', 'oem', 'icon-users', '126', '2', '#/page/system/role/list', '1', '0', '1', '1476340706000', '1480908144000');
INSERT INTO `auth_resource` VALUES ('128', 'REQ8PTJ', '2', 'oem', 'icon-users', '126', '2', '#/page/system/user/list', '2', '0', '1', '1476340719000', '1480908150000');
INSERT INTO `auth_resource` VALUES ('129', 'RERJ4ND', '2', 'oem', 'icon-notebook', '107', '3', '#/page/system/faulTranslateLibrary/list', '6', '0', '1', '1476787874000', '1476788260000');
INSERT INTO `auth_resource` VALUES ('130', 'REM62IE', '2', 'oem', 'fa fa-bell-o', '121', '3', '#page/preventive/alarm', '1', '0', '1', '1476944615000', '1476945096000');
INSERT INTO `auth_resource` VALUES ('131', 'RE8PN65', '2', 'oem', '123', '0', '1', '123', '123', '0', '9', '1476959610000', null);
INSERT INTO `auth_resource` VALUES ('132', 'RELZTGI', '2', 'oem', 'icon-list', '121', '3', '#/page/preventive/list', '2', '0', '9', '1477299643000', '1477299667000');
INSERT INTO `auth_resource` VALUES ('133', 'RENI1E4', '2', 'oem', 'icon-graph', '122', '2', '#/page/stat/list', '10', '0', '9', '1477901150000', '1479997756000');
INSERT INTO `auth_resource` VALUES ('134', 'RETS4ED', '2', 'oem', 'icon-eye', '0', '1', '#/page/monitor/center/Process', '3', '0', '9', '1477907650000', null);
INSERT INTO `auth_resource` VALUES ('136', 'REUTAFZ', '2', 'oem', 'icon-eye', '102', '3', '#/page/monitor/center/Process', '2', '0', '9', '1477907874000', '1478055862000');
INSERT INTO `auth_resource` VALUES ('137', 'RED6N8Z', '2', 'oem', 'icon-calendar', '122', '2', '#/page/report/list', '9', '0', '9', '1477998788000', '1479997604000');
INSERT INTO `auth_resource` VALUES ('138', 'REBMD71', '2', 'oem', 'icon-list', '121', '3', '#/page/preventive/list', '2', '0', '1', '1478000049000', null);
INSERT INTO `auth_resource` VALUES ('139', 'REZU2RG', '2', 'oem', 'icon-vector', '101', '2', '#/page/monitor/tunnel', '3', '1', '1', '1478056652000', '1482109265000');
INSERT INTO `auth_resource` VALUES ('140', 'REQG61F', '5', 'integrator', null, '0', '1', '#/page/ceshi', null, '0', '1', '1478836292000', null);
INSERT INTO `auth_resource` VALUES ('141', 'REP3J6X', '5', 'integrator', null, '0', '2', '#/page', null, '0', '1', '1478836474000', null);
INSERT INTO `auth_resource` VALUES ('142', 'RE2EY9G', '5', 'integrator', null, '0', '1', null, null, '0', '1', '1478836578000', null);
INSERT INTO `auth_resource` VALUES ('143', 'REI7Y5P', '5', 'integrator', null, '0', '1', null, null, '0', '1', '1478837268000', null);
INSERT INTO `auth_resource` VALUES ('144', 'REAAM3A', '2', 'oem', 'fa fa-database', '126', '2', '#/page/system/resource/list', null, '0', '1', '1479044831000', '1479998139000');
INSERT INTO `auth_resource` VALUES ('145', 'REU4R1M', '2', 'oem', 'icon-users', '126', '2', '#/page/system/user/list', null, '0', '9', '1479046060000', null);
INSERT INTO `auth_resource` VALUES ('146', 'RENVTJ7', '2', 'oem', null, '0', '1', null, null, '0', '9', '1479087248000', null);
INSERT INTO `auth_resource` VALUES ('147', 'REMED2C', '2', 'oem', 'dswd', '126', '2', 'dwd', null, '0', '9', '1479098277000', null);
INSERT INTO `auth_resource` VALUES ('148', 'RE61PVW', '2', 'oem', 'dswd', '126', '3', 'dwd', null, '0', '9', '1479098847000', null);
INSERT INTO `auth_resource` VALUES ('149', 'REK6ERY', '2', 'oem', 'ce', '0', '2', 'cew', null, '0', '9', '1479098869000', null);
INSERT INTO `auth_resource` VALUES ('150', 'REEK48U', '2', 'oem', 'icon-grid', '126', '2', '#/page/system/sector/list', null, '0', '1', '1479103122000', '1480908137000');
INSERT INTO `auth_resource` VALUES ('151', 'REAHB1B', '2', 'oem', 'icon-social-dropbox', '126', '2', '#/page/system/modular/list', null, '0', '1', '1479176789000', '1480908134000');
INSERT INTO `auth_resource` VALUES ('152', 'RE67PSB', '2', 'oem', 'icon-flag', '103', '2', '#/page/section', null, '0', '1', '1479549105000', '1479549351000');
INSERT INTO `auth_resource` VALUES ('154', 'REVIDNI', '2', 'oem', 'icon-flag', '152', '3', '#/page/section/list', null, '0', '9', '1479693295000', null);
INSERT INTO `auth_resource` VALUES ('155', 'REG6IRK', '2', 'oem', 'icon-plus', '152', '3', '#/page/section/add', null, '0', '1', '1479693369000', null);
INSERT INTO `auth_resource` VALUES ('156', 'RE495NV', '2', 'oem', 'icon-fdsa', '154', '4', '#/page/section/list', null, '0', '1', '1479693425000', null);
INSERT INTO `auth_resource` VALUES ('157', 'REH9V6H', '2', 'oem', 'icon-list', '152', '3', '#/page/section/list', null, '0', '1', '1479693460000', '1479693479000');
INSERT INTO `auth_resource` VALUES ('158', 'REJYQKX', '2', 'oem', null, '103', '2', '', null, '0', '1', '1479693845000', null);
INSERT INTO `auth_resource` VALUES ('159', 'REPSJ91', '2', 'oem', 'icon-user', '158', '3', '#/page/customer/add', null, '0', '1', '1479693868000', null);
INSERT INTO `auth_resource` VALUES ('160', 'RE83ECS', '2', 'oem', 'icon-users', '158', '3', '#/page/customer/list', null, '0', '1', '1479693906000', null);
INSERT INTO `auth_resource` VALUES ('161', 'REQ4FZW', '2', 'oem', 'icon-bell', '0', '1', null, null, '0', '1', '1479802322000', null);
INSERT INTO `auth_resource` VALUES ('162', 'REKJZI9', '2', 'oem', 'icon-plus', '0', '1', '#/page/trigger/add', null, '0', '9', '1479802918000', null);
INSERT INTO `auth_resource` VALUES ('163', 'RECJFAQ', '2', 'oem', 'icon-plus', '161', '2', '#/page/trigger/add', null, '0', '1', '1479802956000', null);
INSERT INTO `auth_resource` VALUES ('164', 'RELIAZ3', '2', 'oem', 'fa fa-newspaper-o', '107', '3', '#/page/equipment/board', null, '0', '1', '1479891175000', '1479891457000');
INSERT INTO `auth_resource` VALUES ('165', 'RET1GFG', '2', 'oem', '1212', '0', '1', '121', null, '0', '9', '1479994255000', null);
INSERT INTO `auth_resource` VALUES ('166', 'RE99ZI7', '2', 'oem', '1', '0', '1', '1', '1', '0', '9', '1479996465000', null);
INSERT INTO `auth_resource` VALUES ('167', 'RELBBT5', '2', 'null', 'null', '101', '2', '#/page/command', '1', '0', '1', '1479997294000', '1480150171000');
INSERT INTO `auth_resource` VALUES ('168', 'RENMDZF', '2', 'oem', null, '0', '1', null, '2', '0', '1', '1479997320000', null);
INSERT INTO `auth_resource` VALUES ('169', 'RE4N241', '2', 'oem', 'icon-fire', '168', '2', 'weatherReport/list', '1', '0', '1', '1479997349000', '1482109283000');
INSERT INTO `auth_resource` VALUES ('170', 'REMD8RW', '2', 'oem', 'icon-shuffle', '168', '2', null, '2', '0', '1', '1479997369000', '1482109310000');
INSERT INTO `auth_resource` VALUES ('171', 'RE57Z7N', '2', 'oem', 'icon-link', '168', '2', '#/page/informationScheme/equipment', '3', '0', '1', '1479997382000', '1482109331000');
INSERT INTO `auth_resource` VALUES ('172', 'REKUDG1', '2', 'oem', 'icon-bell', '116', '2', null, '1', '0', '1', '1479997443000', '1482108825000');
INSERT INTO `auth_resource` VALUES ('173', 'REV7K7Y', '2', 'oem', 'icon-plus', '172', '3', '#/page/rules/add', '1', '0', '1', '1479997463000', '1480127519000');
INSERT INTO `auth_resource` VALUES ('174', 'REWXT24', '2', 'oem', 'icon-list', '172', '3', '#/page/rules/list', '2', '0', '1', '1479997477000', '1480127548000');
INSERT INTO `auth_resource` VALUES ('175', 'REEDRHT', '2', 'oem', 'icon-list', '116', '2', '#/page/alarmHistory/list', '2', '0', '1', '1479997507000', '1482392839000');
INSERT INTO `auth_resource` VALUES ('176', 'RE2XDKB', '2', 'oem', null, '0', '1', null, '7', '0', '9', '1479997573000', null);
INSERT INTO `auth_resource` VALUES ('177', 'REB1DMT', '2', 'oem', 'fa fa-pie-chart', '122', '2', '#/page/report/trafficFlow', '1', '0', '1', '1479997590000', '1480750565000');
INSERT INTO `auth_resource` VALUES ('178', 'REXKBTL', '2', 'oem', 'fa fa-line-chart', '122', '2', '#/page/weatherReport/list', '2', '0', '1', '1479997620000', '1480670694000');
INSERT INTO `auth_resource` VALUES ('179', 'RESD9RR', '2', 'oem', 'fa fa-bar-chart', '122', '2', null, '3', '0', '1', '1479997631000', '1480308913000');
INSERT INTO `auth_resource` VALUES ('180', 'REXH4NH', '2', 'oem', 'fa fa-bar-chart', '122', '2', null, '4', '0', '1', '1479997649000', '1480308927000');
INSERT INTO `auth_resource` VALUES ('181', 'RE2E2HM', '2', 'oem', 'fa fa-area-chart', '122', '2', null, '5', '0', '1', '1479997663000', '1480308945000');
INSERT INTO `auth_resource` VALUES ('182', 'REQ4KSL', '2', 'oem', 'fa fa-pie-chart', '122', '2', '#/page/stat/list', '6', '0', '1', '1479997690000', '1480663187000');
INSERT INTO `auth_resource` VALUES ('183', 'REH7PUY', '2', 'oem', 'icon-calendar', '122', '2', '#/page/system/systemlog/list', '7', '0', '1', '1479997708000', '1481608329000');
INSERT INTO `auth_resource` VALUES ('184', 'REXEF3G', '2', 'oem', null, '0', '1', null, '4', '0', '1', '1479997796000', null);
INSERT INTO `auth_resource` VALUES ('185', 'RETCMXN', '2', 'oem', 'icon-flag', '184', '2', '#/page/section', '1', '0', '1', '1479997817000', '1482108868000');
INSERT INTO `auth_resource` VALUES ('186', 'REYZ61A', '2', 'oem', 'icon-direction', '184', '2', null, '2', '0', '1', '1479998094000', '1482108935000');
INSERT INTO `auth_resource` VALUES ('187', 'RE63T1F', '2', 'oem', 'icon-fire', '184', '2', null, '3', '0', '1', '1479998104000', '1482108953000');
INSERT INTO `auth_resource` VALUES ('188', 'REZX1KJ', '2', 'oem', 'icon-support', '184', '2', '', '4', '0', '1', '1479998119000', '1482109085000');
INSERT INTO `auth_resource` VALUES ('189', 'REYN9T2', '2', 'oem', 'icon-plus', '187', '3', '#/page/trigger/add', '1', '0', '1', '1479999009000', '1481853495000');
INSERT INTO `auth_resource` VALUES ('190', 'REDXNCQ', '2', 'oem', 'icon-list', '185', '3', '#/page/section/list', '2', '0', '1', '1479999052000', null);
INSERT INTO `auth_resource` VALUES ('191', 'RE1M5QQ', '2', 'oem', 'icon-plus', '185', '3', '#/page/section/add', '1', '0', '1', '1479999120000', '1479999137000');
INSERT INTO `auth_resource` VALUES ('192', 'RETV3ZD', '2', 'oem', 'fa fa-newspaper-o', '186', '3', '#/page/equipment/board', '3', '0', '9', '1479999210000', '1479999560000');
INSERT INTO `auth_resource` VALUES ('193', 'REK7116', '2', 'oem', 'icon-plus', '186', '3', '#/page/equipment/add', '1', '0', '1', '1479999238000', '1479999288000');
INSERT INTO `auth_resource` VALUES ('194', 'RE4DCP3', '2', 'oem', 'icon-list', '186', '3', '#/page/equipment/list', '2', '0', '1', '1479999278000', null);
INSERT INTO `auth_resource` VALUES ('195', 'RE1IJSN', '2', 'oem', null, '190', '4', '#/page/system/equcategory/list', '4', '0', '1', '1479999336000', null);
INSERT INTO `auth_resource` VALUES ('196', 'REGQEPX', '2', 'oem', 'icon-grid', '186', '3', '#/page/system/equcategory/list', '4', '0', '1', '1479999369000', '1479999533000');
INSERT INTO `auth_resource` VALUES ('197', 'REXHJDD', '2', 'oem', '', '186', '3', '', '5', '0', '9', '1479999436000', '1482216078000');
INSERT INTO `auth_resource` VALUES ('198', 'REGDFE2', '2', 'oem', 'icon-notebook', '186', '3', '#/page/system/faulTranslateLibrary/list', '5', '0', '1', '1479999473000', '1482216096000');
INSERT INTO `auth_resource` VALUES ('199', 'RESACSB', '2', 'oem', 'icon-list', '187', '3', '#/page/trigger/list', '2', '0', '1', '1480040058000', '1481853503000');
INSERT INTO `auth_resource` VALUES ('200', 'RESU2H4', '2', 'oem', 'icon-plus', '188', '3', '#/page/groupControl/add', '1', '0', '1', '1480040152000', '1480040809000');
INSERT INTO `auth_resource` VALUES ('201', 'RE84EWN', '2', 'oem', 'icon-list', '188', '3', '#/page/groupControl/list', '2', '0', '1', '1480040181000', '1480040777000');
INSERT INTO `auth_resource` VALUES ('202', 'REVRPJN', '2', 'oem', 'fa fa-newspaper-o', '184', '2', null, '5', '0', '9', '1480482374000', null);
INSERT INTO `auth_resource` VALUES ('203', 'RE5EYC4', '2', 'oem', 'icon-plus', '202', '3', '#/page/information/add', '1', '0', '1', '1480483744000', '1480487154000');
INSERT INTO `auth_resource` VALUES ('204', 'RE78PVX', '2', 'oem', 'icon-list', '202', '3', '#/page/information/list', '2', '0', '1', '1480483810000', '1480487159000');
INSERT INTO `auth_resource` VALUES ('205', 'REXRGJW', '2', 'oem', 'icon-screen-desktop', '101', '2', '#/bigscreen/command', '1', '1', '1', '1480558411000', '1482109231000');
INSERT INTO `auth_resource` VALUES ('206', 'RENI4P5', '2', 'oem', 'icon-plus', '188', '3', '#/page/information/add', '3', '0', '1', '1480563642000', '1480573479000');
INSERT INTO `auth_resource` VALUES ('207', 'REMA2HU', '2', 'oem', 'icon-list', '188', '3', '#/page/information/list', '4', '0', '1', '1480564564000', '1480573484000');
INSERT INTO `auth_resource` VALUES ('208', 'REWTGTU', '2', 'oem', 'icon-picture', '168', '2', '#/page/informationScheme/information', '4', '0', '1', '1480566501000', '1482109440000');

-- ----------------------------
-- Table structure for auth_resource_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_resource_info`;
CREATE TABLE `auth_resource_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_resource_info
-- ----------------------------
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE13IH7', '设备型号', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE1CFT6', '角色', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE1CPBM', '成本管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE1GADU', '对比分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE1IJSN', '设备分类', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE1M5QQ', '路段添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE21B7D', '知识库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE25G1T', '系统参数', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE25MRN', '巡检工单', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE2A8K9', '区域管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE2E2HM', '设备报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE2EY9G', 'ces', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE2TML4', '系统管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE2XDKB', '系统', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE31S2J', '生产安排', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE33PQQ', '123', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE3CQF9', '代理商门户-测试2', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE3GD1Y', '工单管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE3H19G', '工单管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE3YK1B', '设备型号', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE3Z9JM', '资源管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE495NV', '路段列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE4A66S', '1', '5');
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE4DCP3', '设备列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE4N241', '事故处理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE57Z7N', '设备控制', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE5EYC4', '情报板添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE5QINV', '设备管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE5RC73', '设备列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE61PVW', 'hshu1222', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE63T1F', '事件配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE64T8E', '系统日志', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE67PSB', '路段管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE6C3LH', '用户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE6FIJB', '客户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE6ILBJ', '工作台', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE6U41F', '成本管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE71K8J', '巡检工单', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE78PVX', '情报板列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE7ZQ8A', '统计分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE83ECS', '客户列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE83QZE', '客户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE84EWN', '群控列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE858N3', '预防性维护配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE8EAF8', '原料管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE8GI6X', '巡检配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE8KXT7', '添加客户', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE8PN65', '123123', '123');
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE8RTH8', '代理商管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE99ZI7', '123', '1');
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE9G65K', '巡检配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE9IAF5', '监控', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RE9KHQI', '设备巡检配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REAAM3A', '菜单', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REABDKE', '系统日志', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REABM68', '巡检工单', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'READLSH', '设备分类', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REAHB1B', '权限', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REB1DMT', '交通报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REB22VF', '路段监控', '监控中心');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REB712G', '维护工单', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REBCMII', '产物配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REBMD71', '预防性维护任务列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REC5WYR', '设备管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RECE2F1', '工单列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RECJFAQ', '触发器添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RECPALD', '设备工单', '');
INSERT INTO `auth_resource_info` VALUES ('cn', 'RED6N8Z', '自定义报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REDGENF', '设备巡检管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REDPI5E', '产线监控', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REDV46J', '统计分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REDXNCQ', '路段列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REDXWVT', '原件管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REEDRHT', '告警记录', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REEK48U', '部门', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REF3CR2', '监控中心', '');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REF8J7L', 'SIM卡管理', '云中控联通3G SIM卡管理');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REFSWCL', '设备管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REG6AK8', '成本管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REG6IRK', '添加路段', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REGDFE2', '故障翻译库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REGDLBL', '添加产线', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REGEYUS', '集成商管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REGQEPX', '设备分类', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REH2K9X', '知识库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REH7PUY', '系统日志', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REH8AFA', '故障信息', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REH9V6H', '路段列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REHSF6Y', '权限管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REHTIA2', '角色管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REHXDPW', '代理商管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REI7Y5P', 'cd', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REIC9XI', '设备分类', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REIFNAG', '设备型号', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REIJMXJ', '设备管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REJDMR4', '设备运维', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REJJJRK', '1', '5');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REJR63X', '区域管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REJYQKX', '客户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REK6ERY', 'cewnuh', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REK7116', '添加设备', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REKJZI9', '触发器添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REKUDG1', '告警规则', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RELA7LX', '客户列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RELBBT5', '大屏监控页', 'null');
INSERT INTO `auth_resource_info` VALUES ('cn', 'RELIAZ3', '情报板', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RELZTGI', '预防性维护任务列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REM3YSL', '知识库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REM62IE', '添加告警任务', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REMA2HU', '情报板方案列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REMCRYZ', '用户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REMD8RW', '交通管制', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REMED2C', 'hshu', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REMYC5F', '代理商门户-测试1', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REN1AJ6', '对比分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RENENHW', '监控中心', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RENI1E4', '报表中心', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RENI4P5', '情报板方案添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RENMDZF', '调度中心', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RENVTJ7', 'qwe', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REP3J6X', 'ces', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REPL3QC', '角色管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REPSJ91', '客户添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REPYVUJ', '添加设备', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ1RI5', '元件管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ4FZW', '触发器', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ4KSL', '故障报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ4U3D', '系统管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ51ZS', '履历信息', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ6ZNE', '原料管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ8NT1', '产线管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQ8PTJ', '用户', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQG61F', '测试', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQJ7F4', '客户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REQTSH6', '用户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RERJ4ND', '故障翻译库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RERMXJR', '工单管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RERTSV7', '产线管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RES3Y2A', '故障翻译库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RES7JRY', '系统参数', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RESACSB', '事件列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RESD9RR', '事故报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RESMGQQ', '产线列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RESU2H4', '群控方案添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RET1GFG', '12', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RET98UK', '状态检测', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RETCMXN', '路段管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RETS4ED', '过程画面', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RETV3ZD', '情报板', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'RETZVYA', '设备制造商管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REU3BAM', '指标类型', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REU4R1M', '用户管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REU68BA', '监控中心', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REU9NJL', '统计分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REUDHKN', '客户', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REUSQ38', '设备分类', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REUTAFZ', '产线监控', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REV7K7Y', '规则配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REVGC3P', '2121', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REVIDNI', '路段列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REVN2B9', '产线管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REVRPJN', '情报板方案', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REVY5UA', '元件管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REW1R11', '统计分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REWESEE', '集成商门户-测试2', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REWJ61S', '产能报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REWTGTU', '情报板控制', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REWXT24', '规则列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REX9DCR', '巡检任务列表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXC74E', '故障翻译库', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXEF3G', '基础信息维护', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXH4NH', '发布事故命令报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXHJDD', '', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXKBTL', '气象报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXRGJW', '监控主页', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXUJEN', '添加巡检任务', '添加巡检任务');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REXVYQ2', '对比分析', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REYD8LQ', '集成商门户-测试1', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REYH3JV', '设备巡检配置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REYN9T2', '事件添加', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REYPFP2', '系统设置', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REYZ61A', '设备管理', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REZP81H', '故障报表', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REZT69Q', '行业模板', null);
INSERT INTO `auth_resource_info` VALUES ('cn', 'REZU2RG', '隧道监控', '111');
INSERT INTO `auth_resource_info` VALUES ('cn', 'REZX1KJ', '场景预案', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE1CPBM', 'Cost control', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE21B7D', 'Knowledge', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE25MRN', 'Inspection W/O', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE2TML4', 'System Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE31S2J', 'Production arrangement', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE3GD1Y', 'Workorder Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE3H19G', 'Workorder Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE3Z9JM', 'Resource Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE5QINV', 'Euipment MGMT', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE64T8E', 'Systemlog', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE6C3LH', 'User Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE6FIJB', 'Customer Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE6ILBJ', 'Workbench', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE6U41F', 'Costconfig Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE7ZQ8A', 'Statistic', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE8EAF8', 'Material Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RE9G65K', 'Inspction Plan', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REABM68', 'Inspection W/O', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'READLSH', 'Equipments Classification', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REB712G', 'Maintenance work order', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REBCMII', 'Product configuration', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REC5WYR', 'Equipment Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RECPALD', 'Work Order', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REDPI5E', 'Produtionline Monitoring', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REF3CR2', 'Monitoring Center', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REFSWCL', 'Equipment Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REH2K9X', 'Knowledge', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REHTIA2', 'Role management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REHXDPW', 'Agent Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REIC9XI', 'Categary', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REIFNAG', 'Equipment Model', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REJR63X', 'Area Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REMCRYZ', 'systemusers', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REN1AJ6', 'Contrastive Analysis', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RENENHW', 'Monitoring Center', 'wqw');
INSERT INTO `auth_resource_info` VALUES ('en', 'REPL3QC', 'Role Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REQ4U3D', 'System Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REQ8NT1', 'Production Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REQJ7F4', 'Customer Management', '');
INSERT INTO `auth_resource_info` VALUES ('en', 'REQTSH6', 'User management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RERMXJR', 'W/O List MGMT', '00');
INSERT INTO `auth_resource_info` VALUES ('en', 'RES3Y2A', 'FaulTranslateLibrary', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'RES7JRY', 'SystemParameter', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REU68BA', 'Monitoring Center', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REU9NJL', 'Statistical', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REVN2B9', 'Productionline Management', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REVY5UA', 'element', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REW1R11', 'Statistic Analysis', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REXC74E', 'Error Library', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REXVYQ2', 'Comparison Analysis', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REYH3JV', 'Manage Inspction Plan', null);
INSERT INTO `auth_resource_info` VALUES ('en', 'REYPFP2', 'System Setting', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE21B7D', 'Base de Conocimiento', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE2TML4', 'Sistema', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE3GD1Y', 'Lista de Obras', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE3H19G', 'Lista de Obras', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE3Z9JM', 'Gestión de Recursos', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE64T8E', 'Log del Sistema', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE6C3LH', 'wqw', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE6ILBJ', 'Plataforma de Trabajo', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE6U41F', 'Costos', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RE8EAF8', 'Lista de Materiales', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'READLSH', 'Categoría de Equipamiento', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REC5WYR', 'Lista de Equipamiento', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REDPI5E', 'Monitor de Planta', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REF3CR2', 'Centro de Monitor', 'Centro de Monitor');
INSERT INTO `auth_resource_info` VALUES ('es', 'REFSWCL', 'Lista de Equipamiento', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REH2K9X', 'Base de Conocimiento', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REHTIA2', 'Gestión de Papel', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REHXDPW', 'Administración de Agentes', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REJR63X', 'Area', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REN1AJ6', 'Lista de Comparación', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REPL3QC', 'Gestión de Papel', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REQJ7F4', 'Administración de Clientes', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RES3Y2A', 'Memoria de Traducción de Errores', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'RES7JRY', 'Parámetros de Sistema', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REU68BA', 'Monitor', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REU9NJL', 'Análisis Estadístico', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REVN2B9', 'Lista de Línea de Producción', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REW1R11', 'Análisis Estadístico', null);
INSERT INTO `auth_resource_info` VALUES ('es', 'REYPFP2', 'Sistema', null);

-- ----------------------------
-- Table structure for auth_right
-- ----------------------------
DROP TABLE IF EXISTS `auth_right`;
CREATE TABLE `auth_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `portal` int(11) DEFAULT NULL COMMENT '所属平台：1-管理门户，2-OEM门户，3-代理商门户，4-用户门户',
  `module` varchar(20) NOT NULL COMMENT '模块',
  `code` varchar(20) DEFAULT NULL COMMENT '编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of auth_right
-- ----------------------------
INSERT INTO `auth_right` VALUES ('1', '1', 'AMU6MLBM', 'AMU6MLBM_ADD', '1', '1480154227000', null);
INSERT INTO `auth_right` VALUES ('2', '1', 'AMU6MLBM', 'AMU6MLBM_EDIT', '1', '1480155005000', null);
INSERT INTO `auth_right` VALUES ('3', '1', 'AMU6MLBM', 'AMU6MLBM_DELETE', '1', '1480155121000', null);
INSERT INTO `auth_right` VALUES ('4', '1', 'AMUGP1T1', 'AMUGP1T1_ADD', '1', '1480155141000', null);
INSERT INTO `auth_right` VALUES ('5', '1', 'AMUGP1T1', 'AMUGP1T1_EDIT', '1', '1480155159000', null);
INSERT INTO `auth_right` VALUES ('6', '1', 'AMUGP1T1', 'AMUGP1T1_DELETE', '9', '1480155185000', null);
INSERT INTO `auth_right` VALUES ('7', '1', 'AMUGP1T1', 'AMUGP1T1_qweqwe', '9', '1480907086000', null);

-- ----------------------------
-- Table structure for auth_right_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_right_info`;
CREATE TABLE `auth_right_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限信息表';

-- ----------------------------
-- Records of auth_right_info
-- ----------------------------
INSERT INTO `auth_right_info` VALUES ('cn', 'AMU6MLBM_ADD', '添加', '添加');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMU6MLBM_DELETE', '删除', '删除');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMU6MLBM_EDIT', '修改', '修改');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMUGP1T1_ADD', '添加', '添加');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMUGP1T1_DELETE', '删除', '删除');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMUGP1T1_EDIT', '修改', '修改');
INSERT INTO `auth_right_info` VALUES ('cn', 'AMUGP1T1_qweqwe', 'qweqwe', 'qweqweqwe');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `code` varchar(20) NOT NULL,
  `portal` int(11) DEFAULT NULL COMMENT '''所属平台：1-管理门户 2-OEM门户 3-代理商门户 4-用户门户''5-集成商门户',
  `role_type` int(11) DEFAULT NULL COMMENT '角色类型：1-系统角色，2-自定义角色',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级编号',
  `grade` int(11) DEFAULT NULL COMMENT '级别',
  `order_id` int(11) DEFAULT NULL COMMENT '序号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(50) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES ('1', 'CUXM9D8', 'RODQCCR', null, '1', null, null, null, '1', '1460614867000', '1476345954000');
INSERT INTO `auth_role` VALUES ('2', 'CULTGG4', 'RO8Y1C4', null, '2', null, null, null, '1', '1460774993000', '1461030748000');
INSERT INTO `auth_role` VALUES ('3', 'CUXVVBJ', 'ROTT6V7', null, '2', null, null, null, '9', '1460793700000', null);
INSERT INTO `auth_role` VALUES ('4', 'CUXVVBJ', 'ROTIFZ1', null, '2', null, null, null, '1', '1460793746000', null);
INSERT INTO `auth_role` VALUES ('5', 'CUBFQ7L', 'RO6KBE7', null, '2', null, null, null, '9', '1460804554000', null);
INSERT INTO `auth_role` VALUES ('6', 'CUBFQ7L', 'ROMLREE', null, '2', null, null, null, '1', '1460876092000', null);
INSERT INTO `auth_role` VALUES ('7', 'CUTMYL7', 'ROG1LNR', null, '2', null, null, null, '1', '1460876386000', '1461074369000');
INSERT INTO `auth_role` VALUES ('8', 'wwzg', 'wwzg-role-1', null, '1', '0', '1', '1', '1', '1424456541', '1459485469000');
INSERT INTO `auth_role` VALUES ('9', 'CU2CWZ5', 'ROVHW6S', null, '2', null, null, null, '1', '1460434111000', null);
INSERT INTO `auth_role` VALUES ('10', 'wwzg-cu-1', 'ROISSQB', null, '2', null, null, null, '1', '1460522597000', null);
INSERT INTO `auth_role` VALUES ('11', 'CURJBDI', 'ROY2IUU', null, '2', null, null, null, '1', '1460705924000', '1462965145000');
INSERT INTO `auth_role` VALUES ('12', 'wwzg', 'ROZLIDC', null, '2', null, null, null, '1', '1461126373000', '1461126394000');
INSERT INTO `auth_role` VALUES ('13', 'CUXM9D8', 'ROEUFPE', null, '2', null, null, null, '1', '1462286917000', '1473650330000');
INSERT INTO `auth_role` VALUES ('14', 'CUDT48I', 'RO9ATWD', null, '2', null, null, null, '1', '1462287598000', '1462287731000');
INSERT INTO `auth_role` VALUES ('15', 'CU6HHX7', 'ROTI6RK', null, '2', null, null, null, '9', '1462378331000', '1462452646000');
INSERT INTO `auth_role` VALUES ('16', 'CUXM9D8', 'ROXWX9R', null, '2', null, null, null, '9', '1462452859000', null);
INSERT INTO `auth_role` VALUES ('17', 'CU6HHX7', 'ROEHD9D', null, '2', null, null, null, '1', '1462453111000', '1466409079000');
INSERT INTO `auth_role` VALUES ('18', 'CU6HHX7', 'ROZMJQP', null, '2', null, null, null, '1', '1462499263000', '1462598840000');
INSERT INTO `auth_role` VALUES ('19', 'CUCDKWQ', 'RONXLZS', null, '2', null, null, null, '1', '1462940291000', '1465885224000');
INSERT INTO `auth_role` VALUES ('20', 'CUYBUV5', 'ROGWDSQ', null, '2', null, null, null, '1', '1462969050000', '1468393625000');
INSERT INTO `auth_role` VALUES ('21', 'wwzg-cu-1', 'ROBRNPF', null, '2', null, null, null, '1', '1463545951000', null);
INSERT INTO `auth_role` VALUES ('22', 'CUXM9D8', 'ROV46CM', null, '2', null, null, null, '1', '1463647628000', '1476973585000');
INSERT INTO `auth_role` VALUES ('23', 'CU2PXKW', 'ROLM3NH', null, '2', null, null, null, '1', '1463728459000', '1463985089000');
INSERT INTO `auth_role` VALUES ('24', 'CUAVGM2', 'ROP1Y3L', null, '2', null, null, null, '1', '1463982568000', '1463982773000');
INSERT INTO `auth_role` VALUES ('25', 'CUPM8XR', 'ROLGLAW', null, '2', null, null, null, '1', '1463989696000', null);
INSERT INTO `auth_role` VALUES ('26', 'CU9LF9L', 'ROEV1T7', null, '2', null, null, null, '1', '1463989791000', null);
INSERT INTO `auth_role` VALUES ('27', 'CUPGFA4', 'ROWAKKA', null, '2', null, null, null, '1', '1464317270000', '1464318563000');
INSERT INTO `auth_role` VALUES ('28', 'CU21MDZ', 'RO5XX28', null, '2', null, null, null, '1', '1464599026000', '1472440415000');
INSERT INTO `auth_role` VALUES ('29', 'CUJR3CD', 'ROX2DFZ', null, '2', null, null, null, '1', '1464777019000', '1464778712000');
INSERT INTO `auth_role` VALUES ('30', 'CUJR3CD', 'ROB2Y7S', null, '2', null, null, null, '1', '1464777185000', '1464778733000');
INSERT INTO `auth_role` VALUES ('31', 'CUBV3BF', 'RO8EGUS', null, '2', null, null, null, '1', '1467017923000', null);
INSERT INTO `auth_role` VALUES ('32', 'CU66D23', 'RO2VFC2', null, '2', null, null, null, '1', '1467165680000', '1474271537000');
INSERT INTO `auth_role` VALUES ('33', 'CUNI8SX', 'ROQ26Y4', null, '2', null, null, null, '1', '1467342809000', '1467961064000');
INSERT INTO `auth_role` VALUES ('34', 'CUXM9D8', 'cloudinnov-role', '1', '2', '0', '0', '1', '1', '1463037660', '1474882051000');
INSERT INTO `auth_role` VALUES ('35', 'CUBLHI7', 'ROJZF6W', null, '2', null, null, null, '1', '1463124627000', '1467291145000');
INSERT INTO `auth_role` VALUES ('36', 'CUS9JPF', 'RO9Q5VN', null, '2', null, null, null, '1', '1463390335000', '1463392770000');
INSERT INTO `auth_role` VALUES ('37', 'CUH56LT', 'ROQWA45', null, '2', null, null, null, '1', '1463394300000', null);
INSERT INTO `auth_role` VALUES ('38', 'CUQT5RJ', 'ROWVVZ7', null, '2', null, null, null, '1', '1464688455000', '1464688563000');
INSERT INTO `auth_role` VALUES ('39', 'CUEPHVN', 'RO79ZT2', null, '2', null, null, null, '1', '1464780998000', '1464791822000');
INSERT INTO `auth_role` VALUES ('40', 'CUEPHVN', 'ROQHZZ6', null, '2', null, null, null, '9', '1464781020000', null);
INSERT INTO `auth_role` VALUES ('41', 'CU4HPEF', 'ROV2GEZ', null, '2', null, null, null, '1', '1464781912000', '1464792240000');
INSERT INTO `auth_role` VALUES ('42', 'CU97FXJ', 'ROBBXE4', null, '2', null, null, null, '1', '1464933701000', '1464933812000');
INSERT INTO `auth_role` VALUES ('43', 'CUNDAHS', 'ROKTEX3', null, '2', null, null, null, '1', '1467625145000', '1468289208000');
INSERT INTO `auth_role` VALUES ('44', 'CU1M9WR', 'ROLSNN7', null, '2', null, null, null, '1', '1467625466000', '1471319928000');
INSERT INTO `auth_role` VALUES ('45', 'CUNBBDH', 'ROWRYUP', null, '2', null, null, null, '1', '1467778028000', null);
INSERT INTO `auth_role` VALUES ('46', 'CUIYZFY', 'ROPNNG3', null, '2', null, null, null, '1', '1467778213000', null);
INSERT INTO `auth_role` VALUES ('47', 'CUNDAHS', 'RO628XP', null, '2', null, null, null, '1', '1467782254000', null);
INSERT INTO `auth_role` VALUES ('48', 'CUFT1YA', 'ROZPH7M', null, '2', null, null, null, '1', '1467943341000', null);
INSERT INTO `auth_role` VALUES ('49', 'CU2A1FN', 'ROQCBDA', null, '2', null, null, null, '1', '1468224085000', null);
INSERT INTO `auth_role` VALUES ('50', 'CUGC9WK', 'ROANH67', null, '2', null, null, null, '1', '1468292424000', '1468292435000');
INSERT INTO `auth_role` VALUES ('51', 'CUFGNSA', 'RO6ITC1', null, '2', null, null, null, '1', '1468392325000', null);
INSERT INTO `auth_role` VALUES ('52', 'CU5J2MG', 'ROFSBR6', null, '2', null, null, null, '1', '1468553794000', '1468555143000');
INSERT INTO `auth_role` VALUES ('53', 'CUISUX9', 'RO4YV13', null, '2', null, null, null, '1', '1468642443000', null);
INSERT INTO `auth_role` VALUES ('54', 'CU9EENT', 'ROFC9AW', null, '2', null, null, null, '1', '1468645511000', '1468655915000');
INSERT INTO `auth_role` VALUES ('55', 'CU5RWXY', 'ROWD31H', null, '2', null, null, null, '1', '1468651774000', '1468663961000');
INSERT INTO `auth_role` VALUES ('56', 'CUWIZR9', 'ROC2BDX', null, '2', null, null, null, '1', '1468654350000', '1468654493000');
INSERT INTO `auth_role` VALUES ('57', 'CUGA5NP', 'RO57S64', null, '2', null, null, null, '1', '1468654671000', null);
INSERT INTO `auth_role` VALUES ('58', 'CU8YFU5', 'ROWTG4Q', null, '2', null, null, null, '1', '1469065510000', '1471973803000');
INSERT INTO `auth_role` VALUES ('59', 'CU7NG95', 'ROMTU2Q', null, '2', null, null, null, '1', '1469087722000', null);
INSERT INTO `auth_role` VALUES ('60', 'CUNDAHS', 'ROHGICQ', null, '2', null, null, null, '1', '1469509918000', null);
INSERT INTO `auth_role` VALUES ('61', 'CUGC9WK', 'RO8BZD4', null, '2', null, null, null, '1', '1469510941000', '1469510976000');
INSERT INTO `auth_role` VALUES ('62', 'CUQ7UW8', 'ROVPJU4', null, '2', null, null, null, '1', '1469511478000', '1476869348000');
INSERT INTO `auth_role` VALUES ('63', 'CUQME9A', 'ROJ8TY7', null, '2', null, null, null, '1', '1469517950000', null);
INSERT INTO `auth_role` VALUES ('64', 'CUQ378M', 'RO5QDV7', null, '2', null, null, null, '1', '1470104523000', null);
INSERT INTO `auth_role` VALUES ('65', 'CU67HI2', 'RO9P59W', null, '2', null, null, null, '1', '1470108936000', null);
INSERT INTO `auth_role` VALUES ('66', 'CUA9JU6', 'RO8ACWF', null, '2', null, null, null, '1', '1470211383000', null);
INSERT INTO `auth_role` VALUES ('67', 'CUDX738', 'RO4XRK3', null, '2', null, null, null, '1', '1470279471000', '1472023367000');
INSERT INTO `auth_role` VALUES ('68', 'CULM169', 'ROGB4G9', null, '2', null, null, null, '1', '1470281286000', '1470282253000');
INSERT INTO `auth_role` VALUES ('69', 'CU8A9SV', 'RORX3P8', null, '2', null, null, null, '1', '1470294371000', '1470295205000');
INSERT INTO `auth_role` VALUES ('70', 'CUIDTYB', 'ROA4PB9', null, '2', null, null, null, '1', '1470297436000', null);
INSERT INTO `auth_role` VALUES ('71', 'CU2UGA8', 'ROCL4GV', null, '2', null, null, null, '1', '1470367750000', null);
INSERT INTO `auth_role` VALUES ('72', 'CUJ4T8H', 'ROVZTES', null, null, null, null, null, '9', '1470392629000', null);
INSERT INTO `auth_role` VALUES ('73', 'CUJ4T8H', 'RO978YG', null, null, null, null, null, '1', '1470393666000', null);
INSERT INTO `auth_role` VALUES ('75', 'CUPDDC1', 'RORSX3E', null, '2', null, null, null, '1', '1471575363000', null);
INSERT INTO `auth_role` VALUES ('76', 'CUXM9D8', 'ROQQCJB', null, '2', null, null, null, '1', '1471575397000', '1474959837000');
INSERT INTO `auth_role` VALUES ('77', 'CUXM9D8', 'ROEJVQC', null, '2', null, null, null, '9', '1471924239000', '1471962070000');
INSERT INTO `auth_role` VALUES ('78', 'CUCCYQ5', 'ROQT1IX', null, '2', null, null, null, '1', '1471973309000', '1472012218000');
INSERT INTO `auth_role` VALUES ('79', 'CU7HSDJ', 'RO1UXPV', null, null, null, null, null, '1', '1471973469000', '1471973551000');
INSERT INTO `auth_role` VALUES ('80', 'CUXM9D8', 'RO3JSPB', null, '2', null, null, null, '1', '1472007786000', '1472007816000');
INSERT INTO `auth_role` VALUES ('81', 'CUPB6CJ', 'ROCDTBM', null, null, null, null, null, '1', '1472012050000', '1472012055000');
INSERT INTO `auth_role` VALUES ('82', 'CU21MDZ', 'ROMLL2Y', null, '2', null, null, null, '1', '1472969213000', null);
INSERT INTO `auth_role` VALUES ('83', 'CU66D23', 'ROGNAQF', null, '2', null, null, null, '1', '1474260670000', null);
INSERT INTO `auth_role` VALUES ('84', 'CU66D23', 'ROL3UV5', null, '2', null, null, null, '1', '1474265323000', '1474271564000');
INSERT INTO `auth_role` VALUES ('85', '', 'ROUGGCS', '2', '1', null, null, null, '1', '1474617841000', '1477538108000');
INSERT INTO `auth_role` VALUES ('86', '', 'ROSHD1G', '2', '1', null, null, null, '1', '1474617855000', '1477538115000');
INSERT INTO `auth_role` VALUES ('87', '', 'RO9WIGK', '2', '1', null, null, null, '1', '1474617865000', '1477538138000');
INSERT INTO `auth_role` VALUES ('88', '', 'ROKJ4JY', '2', '1', null, null, null, '1', '1474617890000', '1477538128000');
INSERT INTO `auth_role` VALUES ('89', '', 'ROTAIJ7', '4', '1', null, null, null, '1', '1474702163000', null);
INSERT INTO `auth_role` VALUES ('90', '', 'ROK981P', '4', '1', null, null, null, '1', '1474702181000', null);
INSERT INTO `auth_role` VALUES ('91', '', 'ROV1V1P', '4', '1', null, null, null, '1', '1474702191000', null);
INSERT INTO `auth_role` VALUES ('92', '', 'ROECJ39', '4', '1', null, null, null, '1', '1474702205000', null);
INSERT INTO `auth_role` VALUES ('93', 'CUWHNJU', 'RO1NT9U', null, '2', null, null, null, '1', '1474702921000', '1474877908000');
INSERT INTO `auth_role` VALUES ('94', 'CUWHNJU', 'ROV3GXA', null, '2', null, null, null, '1', '1474877939000', '1475916780000');
INSERT INTO `auth_role` VALUES ('95', 'CUY4GJK', 'RO9DVNR', null, '2', null, null, null, '1', '1474882372000', null);
INSERT INTO `auth_role` VALUES ('96', 'CUXM9D8', 'RO2T76U', null, '2', null, null, null, '1', '1474962260000', '1476675059000');
INSERT INTO `auth_role` VALUES ('97', 'CUY4GJK', 'ROAQQFP', null, '2', null, null, null, '1', '1476340832000', null);
INSERT INTO `auth_role` VALUES ('98', 'CUXM9D8', 'ROKWXED', null, '2', null, null, null, '1', '1476346006000', '1480566579000');
INSERT INTO `auth_role` VALUES ('99', 'CU7BN8A', 'ROG1NPD', null, '2', null, null, null, '1', '1476770538000', null);
INSERT INTO `auth_role` VALUES ('100', 'CUV4SVC', 'RORV562', null, '2', null, null, null, '1', '1476785125000', '1478000490000');
INSERT INTO `auth_role` VALUES ('101', 'CUZQBD6', 'RO86134', null, '2', null, null, null, '1', '1476785497000', null);
INSERT INTO `auth_role` VALUES ('102', 'CUXM9D8', 'RO2Q6BQ', null, '1', null, null, null, '9', '1477475261000', '1477477122000');
INSERT INTO `auth_role` VALUES ('103', 'CUXM9D8', 'ROG4844', null, '1', null, null, null, '9', '1477475287000', null);
INSERT INTO `auth_role` VALUES ('104', 'CUXM9D8', 'ROU6DNB', null, '1', null, null, null, '9', '1477476308000', null);
INSERT INTO `auth_role` VALUES ('105', 'CUXM9D8', 'RO7GE6S', null, '1', null, null, null, '9', '1477476323000', null);
INSERT INTO `auth_role` VALUES ('106', 'CUXM9D8', 'ROLQZ5D', null, '1', null, null, null, '9', '1477476368000', '1477476372000');
INSERT INTO `auth_role` VALUES ('107', 'CUXM9D8', 'RO34HLI', null, '1', null, null, null, '9', '1477476970000', null);
INSERT INTO `auth_role` VALUES ('108', 'CUXM9D8', 'ROI8B52', null, '1', null, null, null, '9', '1477478473000', '1477478481000');
INSERT INTO `auth_role` VALUES ('109', 'CUXM9D8', 'ROMEC3Q', null, '1', null, null, null, '1', '1477547804000', '1479961482000');
INSERT INTO `auth_role` VALUES ('110', 'CUUDJBA', 'ROGVBHL', null, '2', null, null, null, '9', '1477635911000', '1477635920000');
INSERT INTO `auth_role` VALUES ('111', 'CUK47CG', 'ROHFH3Z', null, '2', null, null, null, '1', '1477706842000', null);
INSERT INTO `auth_role` VALUES ('112', 'CU6MH81', 'ROII8J4', null, '2', null, null, null, '1', '1477880177000', '1478056886000');
INSERT INTO `auth_role` VALUES ('113', 'CURGNML', 'ROZJ69Y', null, '2', null, null, null, '1', '1477893979000', null);
INSERT INTO `auth_role` VALUES ('114', 'CUH7BW7', 'ROJW9X9', null, '2', null, null, null, '1', '1478490824000', null);

-- ----------------------------
-- Table structure for auth_role_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_info`;
CREATE TABLE `auth_role_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_role_info
-- ----------------------------
INSERT INTO `auth_role_info` VALUES ('cn', 'cloudinnov-role', '云中控管理员（系统）', '1');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO1NT9U', '13', '1232');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO1UXPV', '测试角色（代理商）-guo', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO2Q6BQ', 'qweqwe', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO2T76U', '测试管理员2', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO2VFC2', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO34HLI', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO3JSPB', '测试角色（系统）-guo', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO4XRK3', '中科云创', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO4YV13', '青藏铁路', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO57S64', '1212', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO5QDV7', '的诉讼费的', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO5XX28', '技术工程师', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO628XP', '测试元件名称', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO6ITC1', 'oem', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO6KBE7', '213123', '123');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO79ZT2', '用户', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO7GE6S', '气温气温', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO86134', '发布会角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO8ACWF', '管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO8BZD4', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO8EGUS', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO8Y1C4', '角色', '客户');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO978YG', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO9ATWD', '中科云创（厦门）管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO9DVNR', '1221', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO9P59W', '管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RO9Q5VN', 'A-GM', '11');
INSERT INTO `auth_role_info` VALUES ('cn', 'RO9WIGK', '主管', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROA4PB9', 'GUEST', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROANH67', '客户公司2角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROAQQFP', '新版资源测试', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROB2Y7S', '用户', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROBBXE4', 'jcs', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROBRNPF', '王书平-DEMO', 'DEMO演示');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROC2BDX', '12', '测试');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROCDTBM', '测试角色（代理商）-guo', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROCL4GV', '你好', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RODQCCR', '系统管理员1', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROECJ39', '工程师', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROEHD9D', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROEJVQC', '测试角色-guo', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROEUFPE', '云中控管理员（自定义）', '2');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROEV1T7', '1', '1');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROFC9AW', '青藏铁路', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROFSBR6', 'ntjj管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROG1LNR', '角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROG1NPD', 'ios测试角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROG4844', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROGB4G9', '中科云创', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROGNAQF', '12', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROGVBHL', 'qwe', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROGWDSQ', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROHFH3Z', '测试', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROHGICQ', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROI8B52', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROII8J4', '污水处理', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROISSQB', '角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROJ8TY7', 'guest', 'fangke');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROJW9X9', '徐中建的新角色', '只能看工作台，工单管理。');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROJZF6W', '维沃重工管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROK981P', 'BOSS', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROKJ4JY', '工程师', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROKTEX3', '测试oem管理员', '123');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROKWXED', '新菜单测试', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROL3UV5', '123465', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROLGLAW', '1', '1');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROLM3NH', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROLQZ5D', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROLSNN7', '华鲁管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROMEC3Q', '12312312', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROMLL2Y', '管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROMLREE', '摩根管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROMTU2Q', '测试-user', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RONXLZS', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROP1Y3L', 'User', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROPNNG3', '1212', '212');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQ26Y4', '华恒测试角色', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQCBDA', '测试角色1', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQHZZ6', '访客', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQQCJB', '测试管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQT1IX', '测试角色（客户）-guo', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROQWA45', 'a=custr', '1');
INSERT INTO `auth_role_info` VALUES ('cn', 'RORSX3E', '测试', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RORV562', '发布会管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'RORX3P8', '管理员', 'ADMIN');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROSHD1G', 'BOSS', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROTAIJ7', '系统管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROTI6RK', '访客-YTYE', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROTIFZ1', '管理层', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROTT6V7', '王刚', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROU6DNB', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROUGGCS', '超级管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROV1V1P', '主管', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROV2GEZ', '访客', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROV3GXA', '345', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROV46CM', '游客', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROVHW6S', '人事', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROVPJU4', '用户', 'guest');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROVZTES', '123', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROWAKKA', '管理员', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROWD31H', '青藏铁路', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROWRYUP', '1212', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROWTG4Q', '测试1', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROWVVZ7', 'liucl-1', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROX2DFZ', '访客', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROXWX9R', 'guest', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROY2IUU', '访客', '访客');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROZJ69Y', '博世科污水处理', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'ROZLIDC', '访客权限', '权限2');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROZMJQP', 'user', '客户');
INSERT INTO `auth_role_info` VALUES ('cn', 'ROZPH7M', '公司1角色1', null);
INSERT INTO `auth_role_info` VALUES ('cn', 'wwzg-role-1', '管理权限', '权限1');
INSERT INTO `auth_role_info` VALUES ('en', 'RO5XX28', 'Engineer', null);
INSERT INTO `auth_role_info` VALUES ('en', 'RO8Y1C4', 'Role', 'Customer');
INSERT INTO `auth_role_info` VALUES ('en', 'RODQCCR', 'Admin', null);
INSERT INTO `auth_role_info` VALUES ('en', 'ROG1LNR', 'Role', null);
INSERT INTO `auth_role_info` VALUES ('en', 'ROMLREE', 'Morgant Administrator', null);
INSERT INTO `auth_role_info` VALUES ('en', 'ROTIFZ1', 'Management', null);
INSERT INTO `auth_role_info` VALUES ('en', 'ROV46CM', 'www', null);
INSERT INTO `auth_role_info` VALUES ('es', 'RO8Y1C4', 'Papel', 'Usuario');
INSERT INTO `auth_role_info` VALUES ('es', 'RODQCCR', 'Administrador', 'Administrador');

-- ----------------------------
-- Table structure for auth_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_resource`;
CREATE TABLE `auth_role_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `role_code` varchar(32) NOT NULL COMMENT '部门编号',
  `resource_code` varchar(32) NOT NULL COMMENT '资源编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4970 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_role_resource
-- ----------------------------
INSERT INTO `auth_role_resource` VALUES ('26', 'CUXVVBJ', 'ROTIFZ1', 'RE6ILBJ', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('27', 'CUXVVBJ', 'ROTIFZ1', 'REU68BA', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('28', 'CUXVVBJ', 'ROTIFZ1', 'RE3H19G', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('29', 'CUXVVBJ', 'ROTIFZ1', 'REC5WYR', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('30', 'CUXVVBJ', 'ROTIFZ1', 'REDPI5E', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('31', 'CUXVVBJ', 'ROTIFZ1', 'REYPFP2', '1', '1460793746608');
INSERT INTO `auth_role_resource` VALUES ('32', 'CUBFQ7L', 'RO6KBE7', 'RE6ILBJ', '1', '1460804554553');
INSERT INTO `auth_role_resource` VALUES ('49', 'CUBFQ7L', 'ROMLREE', 'RE6ILBJ', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('50', 'CUBFQ7L', 'ROMLREE', 'RE3H19G', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('51', 'CUBFQ7L', 'ROMLREE', 'REC5WYR', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('52', 'CUBFQ7L', 'ROMLREE', 'REDPI5E', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('53', 'CUBFQ7L', 'ROMLREE', 'REYPFP2', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('54', 'CUBFQ7L', 'ROMLREE', 'REU9NJL', '1', '1460876092706');
INSERT INTO `auth_role_resource` VALUES ('94', 'CULTGG4', 'RO8Y1C4', 'RE6ILBJ', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('95', 'CULTGG4', 'RO8Y1C4', 'RE3H19G', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('96', 'CULTGG4', 'RO8Y1C4', 'REC5WYR', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('97', 'CULTGG4', 'RO8Y1C4', 'REDPI5E', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('98', 'CULTGG4', 'RO8Y1C4', 'REHTIA2', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('99', 'CULTGG4', 'RO8Y1C4', 'REQTSH6', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('100', 'CULTGG4', 'RO8Y1C4', 'REBCMII', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('101', 'CULTGG4', 'RO8Y1C4', 'REYPFP2', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('102', 'CULTGG4', 'RO8Y1C4', 'REU9NJL', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('103', 'CULTGG4', 'RO8Y1C4', 'RE21B7D', '1', '1460774993000');
INSERT INTO `auth_role_resource` VALUES ('165', 'CUTMYL7', 'ROG1LNR', 'RE6ILBJ', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('166', 'CUTMYL7', 'ROG1LNR', 'RE3H19G', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('167', 'CUTMYL7', 'ROG1LNR', 'REC5WYR', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('168', 'CUTMYL7', 'ROG1LNR', 'REDPI5E', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('169', 'CUTMYL7', 'ROG1LNR', 'REHTIA2', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('170', 'CUTMYL7', 'ROG1LNR', 'REQTSH6', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('171', 'CUTMYL7', 'ROG1LNR', 'REBCMII', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('172', 'CUTMYL7', 'ROG1LNR', 'REYPFP2', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('173', 'CUTMYL7', 'ROG1LNR', 'REU9NJL', '1', '1460876386000');
INSERT INTO `auth_role_resource` VALUES ('235', 'CUDT48I', 'RO9ATWD', 'REF3CR2', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('236', 'CUDT48I', 'RO9ATWD', 'RE3GD1Y', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('237', 'CUDT48I', 'RO9ATWD', 'REHXDPW', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('238', 'CUDT48I', 'RO9ATWD', 'REQJ7F4', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('239', 'CUDT48I', 'RO9ATWD', 'REFSWCL', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('240', 'CUDT48I', 'RO9ATWD', 'REVN2B9', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('241', 'CUDT48I', 'RO9ATWD', 'REN1AJ6', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('242', 'CUDT48I', 'RO9ATWD', 'REW1R11', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('243', 'CUDT48I', 'RO9ATWD', 'RES3Y2A', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('244', 'CUDT48I', 'RO9ATWD', 'READLSH', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('245', 'CUDT48I', 'RO9ATWD', 'RE3Z9JM', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('246', 'CUDT48I', 'RO9ATWD', 'REPL3QC', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('247', 'CUDT48I', 'RO9ATWD', 'RE6C3LH', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('248', 'CUDT48I', 'RO9ATWD', 'REJR63X', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('249', 'CUDT48I', 'RO9ATWD', 'RE8EAF8', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('250', 'CUDT48I', 'RO9ATWD', 'RE6U41F', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('251', 'CUDT48I', 'RO9ATWD', 'RES7JRY', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('252', 'CUDT48I', 'RO9ATWD', 'RE64T8E', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('253', 'CUDT48I', 'RO9ATWD', 'RE2TML4', '1', '1462287598000');
INSERT INTO `auth_role_resource` VALUES ('255', 'CU6HHX7', 'ROTI6RK', 'REN1AJ6', '1', '1462378331000');
INSERT INTO `auth_role_resource` VALUES ('256', 'CUXM9D8', 'ROXWX9R', 'REN1AJ6', '1', '1462452859173');
INSERT INTO `auth_role_resource` VALUES ('275', 'CU6HHX7', 'ROZMJQP', 'REF3CR2', '1', '1462499263000');
INSERT INTO `auth_role_resource` VALUES ('276', 'CU6HHX7', 'ROZMJQP', 'REQJ7F4', '1', '1462499263000');
INSERT INTO `auth_role_resource` VALUES ('277', 'CU6HHX7', 'ROZMJQP', 'REFSWCL', '1', '1462499263000');
INSERT INTO `auth_role_resource` VALUES ('278', 'CU6HHX7', 'ROZMJQP', 'REN1AJ6', '1', '1462499263000');
INSERT INTO `auth_role_resource` VALUES ('280', 'CURJBDI', 'ROY2IUU', 'REF3CR2', '1', '1460705924000');
INSERT INTO `auth_role_resource` VALUES ('285', 'wwzg-cu-1', 'ROBRNPF', 'REF3CR2', '1', '1463545951539');
INSERT INTO `auth_role_resource` VALUES ('286', 'wwzg-cu-1', 'ROBRNPF', 'REQJ7F4', '1', '1463545951539');
INSERT INTO `auth_role_resource` VALUES ('287', 'wwzg-cu-1', 'ROBRNPF', 'REFSWCL', '1', '1463545951539');
INSERT INTO `auth_role_resource` VALUES ('288', 'wwzg-cu-1', 'ROBRNPF', 'REVN2B9', '1', '1463545951539');
INSERT INTO `auth_role_resource` VALUES ('289', 'wwzg-cu-1', 'ROBRNPF', 'REN1AJ6', '1', '1463545951539');
INSERT INTO `auth_role_resource` VALUES ('332', 'CUAVGM2', 'ROP1Y3L', 'REF3CR2', '1', '1463982568000');
INSERT INTO `auth_role_resource` VALUES ('333', 'CUAVGM2', 'ROP1Y3L', 'RE3GD1Y', '1', '1463982568000');
INSERT INTO `auth_role_resource` VALUES ('334', 'CUAVGM2', 'ROP1Y3L', 'REQJ7F4', '1', '1463982568000');
INSERT INTO `auth_role_resource` VALUES ('335', 'CUAVGM2', 'ROP1Y3L', 'REFSWCL', '1', '1463982568000');
INSERT INTO `auth_role_resource` VALUES ('336', 'CUAVGM2', 'ROP1Y3L', 'RE6T92Y', '1', '1463982568000');
INSERT INTO `auth_role_resource` VALUES ('337', 'CU2PXKW', 'ROLM3NH', 'RE6ILBJ', '1', '1463728459000');
INSERT INTO `auth_role_resource` VALUES ('338', 'CU2PXKW', 'ROLM3NH', 'RE3H19G', '1', '1463728459000');
INSERT INTO `auth_role_resource` VALUES ('339', 'CU2PXKW', 'ROLM3NH', 'REC5WYR', '1', '1463728459000');
INSERT INTO `auth_role_resource` VALUES ('340', 'CU2PXKW', 'ROLM3NH', 'REDPI5E', '1', '1463728459000');
INSERT INTO `auth_role_resource` VALUES ('341', 'CUPM8XR', 'ROLGLAW', 'REF3CR2', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('342', 'CUPM8XR', 'ROLGLAW', 'RE3GD1Y', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('343', 'CUPM8XR', 'ROLGLAW', 'REHXDPW', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('344', 'CUPM8XR', 'ROLGLAW', 'REQJ7F4', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('345', 'CUPM8XR', 'ROLGLAW', 'REFSWCL', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('346', 'CUPM8XR', 'ROLGLAW', 'REVN2B9', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('347', 'CUPM8XR', 'ROLGLAW', 'REN1AJ6', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('348', 'CUPM8XR', 'ROLGLAW', 'REW1R11', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('349', 'CUPM8XR', 'ROLGLAW', 'RE2TML4', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('350', 'CUPM8XR', 'ROLGLAW', 'RES3Y2A', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('351', 'CUPM8XR', 'ROLGLAW', 'READLSH', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('352', 'CUPM8XR', 'ROLGLAW', 'RE3Z9JM', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('353', 'CUPM8XR', 'ROLGLAW', 'REPL3QC', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('354', 'CUPM8XR', 'ROLGLAW', 'RE6C3LH', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('355', 'CUPM8XR', 'ROLGLAW', 'REJR63X', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('356', 'CUPM8XR', 'ROLGLAW', 'RE8EAF8', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('357', 'CUPM8XR', 'ROLGLAW', 'RE6U41F', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('358', 'CUPM8XR', 'ROLGLAW', 'RES7JRY', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('359', 'CUPM8XR', 'ROLGLAW', 'RE64T8E', '1', '1463989696416');
INSERT INTO `auth_role_resource` VALUES ('360', 'CU9LF9L', 'ROEV1T7', 'REF3CR2', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('361', 'CU9LF9L', 'ROEV1T7', 'RE3GD1Y', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('362', 'CU9LF9L', 'ROEV1T7', 'REHXDPW', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('363', 'CU9LF9L', 'ROEV1T7', 'REQJ7F4', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('364', 'CU9LF9L', 'ROEV1T7', 'REFSWCL', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('365', 'CU9LF9L', 'ROEV1T7', 'REVN2B9', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('366', 'CU9LF9L', 'ROEV1T7', 'REN1AJ6', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('367', 'CU9LF9L', 'ROEV1T7', 'REW1R11', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('368', 'CU9LF9L', 'ROEV1T7', 'RE2TML4', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('369', 'CU9LF9L', 'ROEV1T7', 'RES3Y2A', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('370', 'CU9LF9L', 'ROEV1T7', 'READLSH', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('371', 'CU9LF9L', 'ROEV1T7', 'RE3Z9JM', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('372', 'CU9LF9L', 'ROEV1T7', 'REPL3QC', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('373', 'CU9LF9L', 'ROEV1T7', 'RE6C3LH', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('374', 'CU9LF9L', 'ROEV1T7', 'REJR63X', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('375', 'CU9LF9L', 'ROEV1T7', 'RE8EAF8', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('376', 'CU9LF9L', 'ROEV1T7', 'RE6U41F', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('377', 'CU9LF9L', 'ROEV1T7', 'RES7JRY', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('378', 'CU9LF9L', 'ROEV1T7', 'RE64T8E', '1', '1463989791010');
INSERT INTO `auth_role_resource` VALUES ('391', 'CUPGFA4', 'ROWAKKA', 'RE3GD1Y', '1', '1464317270000');
INSERT INTO `auth_role_resource` VALUES ('392', 'CUPGFA4', 'ROWAKKA', 'REQJ7F4', '1', '1464317270000');
INSERT INTO `auth_role_resource` VALUES ('393', 'CUPGFA4', 'ROWAKKA', 'REFSWCL', '1', '1464317270000');
INSERT INTO `auth_role_resource` VALUES ('394', 'CUPGFA4', 'ROWAKKA', 'REN1AJ6', '1', '1464317270000');
INSERT INTO `auth_role_resource` VALUES ('395', 'CUPGFA4', 'ROWAKKA', 'REH2K9X', '1', '1464317270000');
INSERT INTO `auth_role_resource` VALUES ('410', 'CUJR3CD', 'ROX2DFZ', 'RE6ILBJ', '1', '1464777019000');
INSERT INTO `auth_role_resource` VALUES ('411', 'CUJR3CD', 'ROX2DFZ', 'REDPI5E', '1', '1464777019000');
INSERT INTO `auth_role_resource` VALUES ('412', 'CUJR3CD', 'ROB2Y7S', 'RE6ILBJ', '1', '1464777185000');
INSERT INTO `auth_role_resource` VALUES ('413', 'CUJR3CD', 'ROB2Y7S', 'RE3H19G', '1', '1464777185000');
INSERT INTO `auth_role_resource` VALUES ('414', 'CUJR3CD', 'ROB2Y7S', 'REC5WYR', '1', '1464777185000');
INSERT INTO `auth_role_resource` VALUES ('415', 'CUJR3CD', 'ROB2Y7S', 'REDPI5E', '1', '1464777185000');
INSERT INTO `auth_role_resource` VALUES ('416', 'CUJR3CD', 'ROB2Y7S', 'REU9NJL', '1', '1464777185000');
INSERT INTO `auth_role_resource` VALUES ('423', 'CUCDKWQ', 'RONXLZS', 'RE3H19G', '1', '1462940291000');
INSERT INTO `auth_role_resource` VALUES ('424', 'CUCDKWQ', 'RONXLZS', 'REC5WYR', '1', '1462940291000');
INSERT INTO `auth_role_resource` VALUES ('425', 'CUCDKWQ', 'RONXLZS', 'REU9NJL', '1', '1462940291000');
INSERT INTO `auth_role_resource` VALUES ('426', 'CU6HHX7', 'ROEHD9D', 'RE6ILBJ', '1', '1462453111000');
INSERT INTO `auth_role_resource` VALUES ('427', 'CU6HHX7', 'ROEHD9D', 'REC5WYR', '1', '1462453111000');
INSERT INTO `auth_role_resource` VALUES ('428', 'CUBV3BF', 'RO8EGUS', 'RE6ILBJ', '1', '1467017923737');
INSERT INTO `auth_role_resource` VALUES ('442', 'CUS9JPF', 'RO9Q5VN', 'REF3CR2', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('443', 'CUS9JPF', 'RO9Q5VN', 'RE3GD1Y', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('444', 'CUS9JPF', 'RO9Q5VN', 'REHXDPW', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('445', 'CUS9JPF', 'RO9Q5VN', 'REQJ7F4', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('446', 'CUS9JPF', 'RO9Q5VN', 'REFSWCL', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('447', 'CUS9JPF', 'RO9Q5VN', 'REVN2B9', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('448', 'CUS9JPF', 'RO9Q5VN', 'REN1AJ6', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('449', 'CUS9JPF', 'RO9Q5VN', 'REW1R11', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('450', 'CUS9JPF', 'RO9Q5VN', 'RE6C3LH', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('451', 'CUS9JPF', 'RO9Q5VN', 'RES3Y2A', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('452', 'CUS9JPF', 'RO9Q5VN', 'READLSH', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('453', 'CUS9JPF', 'RO9Q5VN', 'RE2TML4', '1', '1463390335000');
INSERT INTO `auth_role_resource` VALUES ('454', 'CUH56LT', 'ROQWA45', 'REU68BA', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('455', 'CUH56LT', 'ROQWA45', 'RE6ILBJ', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('456', 'CUH56LT', 'ROQWA45', 'RE3H19G', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('457', 'CUH56LT', 'ROQWA45', 'REC5WYR', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('458', 'CUH56LT', 'ROQWA45', 'REDPI5E', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('459', 'CUH56LT', 'ROQWA45', 'REU9NJL', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('460', 'CUH56LT', 'ROQWA45', 'REYPFP2', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('461', 'CUH56LT', 'ROQWA45', 'REHTIA2', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('462', 'CUH56LT', 'ROQWA45', 'REQTSH6', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('463', 'CUH56LT', 'ROQWA45', 'REBCMII', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('464', 'CUH56LT', 'ROQWA45', 'RE1CPBM', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('465', 'CUH56LT', 'ROQWA45', 'RE31S2J', '1', '1463394300379');
INSERT INTO `auth_role_resource` VALUES ('466', 'CUQT5RJ', 'ROWVVZ7', 'REHXDPW', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('467', 'CUQT5RJ', 'ROWVVZ7', 'REQJ7F4', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('468', 'CUQT5RJ', 'ROWVVZ7', 'REFSWCL', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('469', 'CUQT5RJ', 'ROWVVZ7', 'REW1R11', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('470', 'CUQT5RJ', 'ROWVVZ7', 'RES3Y2A', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('471', 'CUQT5RJ', 'ROWVVZ7', 'READLSH', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('472', 'CUQT5RJ', 'ROWVVZ7', 'RE3Z9JM', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('473', 'CUQT5RJ', 'ROWVVZ7', 'REPL3QC', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('474', 'CUQT5RJ', 'ROWVVZ7', 'RE6C3LH', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('475', 'CUQT5RJ', 'ROWVVZ7', 'REJR63X', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('476', 'CUQT5RJ', 'ROWVVZ7', 'RE8EAF8', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('477', 'CUQT5RJ', 'ROWVVZ7', 'RE6U41F', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('478', 'CUQT5RJ', 'ROWVVZ7', 'RES7JRY', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('479', 'CUQT5RJ', 'ROWVVZ7', 'RE64T8E', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('480', 'CUQT5RJ', 'ROWVVZ7', 'RE2TML4', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('481', 'CUQT5RJ', 'ROWVVZ7', 'REH2K9X', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('482', 'CUQT5RJ', 'ROWVVZ7', 'RETZVYA', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('483', 'CUQT5RJ', 'ROWVVZ7', 'REF3CR2', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('484', 'CUQT5RJ', 'ROWVVZ7', 'RE3GD1Y', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('485', 'CUQT5RJ', 'ROWVVZ7', 'REVN2B9', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('486', 'CUQT5RJ', 'ROWVVZ7', 'REN1AJ6', '1', '1464688455000');
INSERT INTO `auth_role_resource` VALUES ('487', 'CUEPHVN', 'RO79ZT2', 'REF3CR2', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('488', 'CUEPHVN', 'RO79ZT2', 'RE3GD1Y', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('489', 'CUEPHVN', 'RO79ZT2', 'REHXDPW', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('490', 'CUEPHVN', 'RO79ZT2', 'REQJ7F4', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('491', 'CUEPHVN', 'RO79ZT2', 'REFSWCL', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('492', 'CUEPHVN', 'RO79ZT2', 'REN1AJ6', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('493', 'CUEPHVN', 'RO79ZT2', 'REW1R11', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('494', 'CUEPHVN', 'RO79ZT2', 'REH2K9X', '1', '1464780998902');
INSERT INTO `auth_role_resource` VALUES ('495', 'CUEPHVN', 'ROQHZZ6', 'REF3CR2', '1', '1464781020048');
INSERT INTO `auth_role_resource` VALUES ('496', 'CUEPHVN', 'ROQHZZ6', 'REN1AJ6', '1', '1464781020048');
INSERT INTO `auth_role_resource` VALUES ('497', 'CU4HPEF', 'ROV2GEZ', 'RE3GD1Y', '1', '1464781912000');
INSERT INTO `auth_role_resource` VALUES ('498', 'CU4HPEF', 'ROV2GEZ', 'REN1AJ6', '1', '1464781912000');
INSERT INTO `auth_role_resource` VALUES ('499', 'CU4HPEF', 'ROV2GEZ', 'REW1R11', '1', '1464781912000');
INSERT INTO `auth_role_resource` VALUES ('500', 'CU4HPEF', 'ROV2GEZ', 'REVN2B9', '1', '1464781912000');
INSERT INTO `auth_role_resource` VALUES ('501', 'CU4HPEF', 'ROV2GEZ', 'REF3CR2', '1', '1464781912000');
INSERT INTO `auth_role_resource` VALUES ('502', 'CU97FXJ', 'ROBBXE4', 'REF3CR2', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('503', 'CU97FXJ', 'ROBBXE4', 'RE3GD1Y', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('504', 'CU97FXJ', 'ROBBXE4', 'REHXDPW', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('505', 'CU97FXJ', 'ROBBXE4', 'REQJ7F4', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('506', 'CU97FXJ', 'ROBBXE4', 'REFSWCL', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('507', 'CU97FXJ', 'ROBBXE4', 'REVN2B9', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('508', 'CU97FXJ', 'ROBBXE4', 'REN1AJ6', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('509', 'CU97FXJ', 'ROBBXE4', 'REW1R11', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('510', 'CU97FXJ', 'ROBBXE4', 'RES3Y2A', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('511', 'CU97FXJ', 'ROBBXE4', 'READLSH', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('512', 'CU97FXJ', 'ROBBXE4', 'REPL3QC', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('513', 'CU97FXJ', 'ROBBXE4', 'RE6C3LH', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('514', 'CU97FXJ', 'ROBBXE4', 'REJR63X', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('515', 'CU97FXJ', 'ROBBXE4', 'RETZVYA', '1', '1464933701000');
INSERT INTO `auth_role_resource` VALUES ('542', 'CUBLHI7', 'ROJZF6W', 'REF3CR2', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('543', 'CUBLHI7', 'ROJZF6W', 'RE3GD1Y', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('544', 'CUBLHI7', 'ROJZF6W', 'REHXDPW', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('545', 'CUBLHI7', 'ROJZF6W', 'REQJ7F4', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('546', 'CUBLHI7', 'ROJZF6W', 'REFSWCL', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('547', 'CUBLHI7', 'ROJZF6W', 'REVN2B9', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('548', 'CUBLHI7', 'ROJZF6W', 'REN1AJ6', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('549', 'CUBLHI7', 'ROJZF6W', 'REW1R11', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('550', 'CUBLHI7', 'ROJZF6W', 'RES3Y2A', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('551', 'CUBLHI7', 'ROJZF6W', 'READLSH', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('552', 'CUBLHI7', 'ROJZF6W', 'REPL3QC', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('553', 'CUBLHI7', 'ROJZF6W', 'RE6C3LH', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('554', 'CUBLHI7', 'ROJZF6W', 'REF8J7L', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('555', 'CUBLHI7', 'ROJZF6W', 'RE2TML4', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('556', 'CUBLHI7', 'ROJZF6W', 'RE3Z9JM', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('557', 'CUBLHI7', 'ROJZF6W', 'REJR63X', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('558', 'CUBLHI7', 'ROJZF6W', 'RE8EAF8', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('559', 'CUBLHI7', 'ROJZF6W', 'RE6U41F', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('560', 'CUBLHI7', 'ROJZF6W', 'RES7JRY', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('561', 'CUBLHI7', 'ROJZF6W', 'RE64T8E', '1', '1463124627000');
INSERT INTO `auth_role_resource` VALUES ('811', 'CUNBBDH', 'ROWRYUP', 'RE6ILBJ', '1', '1467778026493');
INSERT INTO `auth_role_resource` VALUES ('812', 'CUIYZFY', 'ROPNNG3', 'REU9NJL', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('813', 'CUIYZFY', 'ROPNNG3', 'REYPFP2', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('814', 'CUIYZFY', 'ROPNNG3', 'REHTIA2', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('815', 'CUIYZFY', 'ROPNNG3', 'REQTSH6', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('816', 'CUIYZFY', 'ROPNNG3', 'REBCMII', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('817', 'CUIYZFY', 'ROPNNG3', 'RE1CPBM', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('818', 'CUIYZFY', 'ROPNNG3', 'RE31S2J', '1', '1467778211226');
INSERT INTO `auth_role_resource` VALUES ('819', 'CUFT1YA', 'ROZPH7M', 'RE6ILBJ', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('820', 'CUFT1YA', 'ROZPH7M', 'REU68BA', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('821', 'CUFT1YA', 'ROZPH7M', 'RE3H19G', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('822', 'CUFT1YA', 'ROZPH7M', 'REC5WYR', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('823', 'CUFT1YA', 'ROZPH7M', 'REDPI5E', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('824', 'CUFT1YA', 'ROZPH7M', 'REYPFP2', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('825', 'CUFT1YA', 'ROZPH7M', 'REHTIA2', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('826', 'CUFT1YA', 'ROZPH7M', 'REQTSH6', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('827', 'CUFT1YA', 'ROZPH7M', 'REBCMII', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('828', 'CUFT1YA', 'ROZPH7M', 'RE1CPBM', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('829', 'CUFT1YA', 'ROZPH7M', 'RE31S2J', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('830', 'CUFT1YA', 'ROZPH7M', 'REU9NJL', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('831', 'CUFT1YA', 'ROZPH7M', 'RE21B7D', '1', '1467943320167');
INSERT INTO `auth_role_resource` VALUES ('844', 'CUNI8SX', 'ROQ26Y4', 'RE6ILBJ', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('845', 'CUNI8SX', 'ROQ26Y4', 'RE3H19G', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('846', 'CUNI8SX', 'ROQ26Y4', 'REC5WYR', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('847', 'CUNI8SX', 'ROQ26Y4', 'REDPI5E', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('848', 'CUNI8SX', 'ROQ26Y4', 'REHTIA2', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('849', 'CUNI8SX', 'ROQ26Y4', 'REQTSH6', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('850', 'CUNI8SX', 'ROQ26Y4', 'REBCMII', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('851', 'CUNI8SX', 'ROQ26Y4', 'RE1CPBM', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('852', 'CUNI8SX', 'ROQ26Y4', 'RE31S2J', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('853', 'CUNI8SX', 'ROQ26Y4', 'REYPFP2', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('854', 'CUNI8SX', 'ROQ26Y4', 'REU9NJL', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('855', 'CUNI8SX', 'ROQ26Y4', 'RE21B7D', '1', '1467342809000');
INSERT INTO `auth_role_resource` VALUES ('856', 'CU2A1FN', 'ROQCBDA', 'REDGENF', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('857', 'CU2A1FN', 'ROQCBDA', 'RE8GI6X', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('858', 'CU2A1FN', 'ROQCBDA', 'RE71K8J', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('859', 'CU2A1FN', 'ROQCBDA', 'REFSWCL', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('860', 'CU2A1FN', 'ROQCBDA', 'REQJ7F4', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('861', 'CU2A1FN', 'ROQCBDA', 'RETZVYA', '1', '1468224083867');
INSERT INTO `auth_role_resource` VALUES ('862', 'CUNDAHS', 'ROKTEX3', 'REF3CR2', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('863', 'CUNDAHS', 'ROKTEX3', 'RE3GD1Y', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('864', 'CUNDAHS', 'ROKTEX3', 'REHXDPW', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('865', 'CUNDAHS', 'ROKTEX3', 'REQJ7F4', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('866', 'CUNDAHS', 'ROKTEX3', 'REFSWCL', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('867', 'CUNDAHS', 'ROKTEX3', 'REVN2B9', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('868', 'CUNDAHS', 'ROKTEX3', 'REN1AJ6', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('869', 'CUNDAHS', 'ROKTEX3', 'REW1R11', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('870', 'CUNDAHS', 'ROKTEX3', 'RES3Y2A', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('871', 'CUNDAHS', 'ROKTEX3', 'READLSH', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('872', 'CUNDAHS', 'ROKTEX3', 'RE3Z9JM', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('873', 'CUNDAHS', 'ROKTEX3', 'REPL3QC', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('874', 'CUNDAHS', 'ROKTEX3', 'RE6C3LH', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('875', 'CUNDAHS', 'ROKTEX3', 'REJR63X', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('876', 'CUNDAHS', 'ROKTEX3', 'RE8EAF8', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('877', 'CUNDAHS', 'ROKTEX3', 'RE6U41F', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('878', 'CUNDAHS', 'ROKTEX3', 'RES7JRY', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('879', 'CUNDAHS', 'ROKTEX3', 'RE64T8E', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('880', 'CUNDAHS', 'ROKTEX3', 'REF8J7L', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('881', 'CUNDAHS', 'ROKTEX3', 'REDXWVT', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('882', 'CUNDAHS', 'ROKTEX3', 'RE2TML4', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('883', 'CUNDAHS', 'ROKTEX3', 'REDGENF', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('884', 'CUNDAHS', 'ROKTEX3', 'RE8GI6X', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('885', 'CUNDAHS', 'ROKTEX3', 'RE71K8J', '1', '1467625145000');
INSERT INTO `auth_role_resource` VALUES ('886', 'CUFGNSA', 'RO6ITC1', 'REF3CR2', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('887', 'CUFGNSA', 'RO6ITC1', 'RE3GD1Y', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('888', 'CUFGNSA', 'RO6ITC1', 'REHXDPW', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('889', 'CUFGNSA', 'RO6ITC1', 'REQJ7F4', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('890', 'CUFGNSA', 'RO6ITC1', 'REFSWCL', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('891', 'CUFGNSA', 'RO6ITC1', 'REVN2B9', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('892', 'CUFGNSA', 'RO6ITC1', 'REN1AJ6', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('893', 'CUFGNSA', 'RO6ITC1', 'REW1R11', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('894', 'CUFGNSA', 'RO6ITC1', 'RE2TML4', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('895', 'CUFGNSA', 'RO6ITC1', 'RES3Y2A', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('896', 'CUFGNSA', 'RO6ITC1', 'READLSH', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('897', 'CUFGNSA', 'RO6ITC1', 'RE3Z9JM', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('898', 'CUFGNSA', 'RO6ITC1', 'REPL3QC', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('899', 'CUFGNSA', 'RO6ITC1', 'RE6C3LH', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('900', 'CUFGNSA', 'RO6ITC1', 'REJR63X', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('901', 'CUFGNSA', 'RO6ITC1', 'RE8EAF8', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('902', 'CUFGNSA', 'RO6ITC1', 'RE6U41F', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('903', 'CUFGNSA', 'RO6ITC1', 'RES7JRY', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('904', 'CUFGNSA', 'RO6ITC1', 'RE64T8E', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('905', 'CUFGNSA', 'RO6ITC1', 'REF8J7L', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('906', 'CUFGNSA', 'RO6ITC1', 'REDXWVT', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('907', 'CUFGNSA', 'RO6ITC1', 'REH2K9X', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('908', 'CUFGNSA', 'RO6ITC1', 'REDGENF', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('909', 'CUFGNSA', 'RO6ITC1', 'RE8GI6X', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('910', 'CUFGNSA', 'RO6ITC1', 'RE71K8J', '1', '1468392320158');
INSERT INTO `auth_role_resource` VALUES ('911', 'CUYBUV5', 'ROGWDSQ', 'RE6ILBJ', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('912', 'CUYBUV5', 'ROGWDSQ', 'REU68BA', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('913', 'CUYBUV5', 'ROGWDSQ', 'RE3H19G', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('914', 'CUYBUV5', 'ROGWDSQ', 'REC5WYR', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('915', 'CUYBUV5', 'ROGWDSQ', 'REDPI5E', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('916', 'CUYBUV5', 'ROGWDSQ', 'REYPFP2', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('917', 'CUYBUV5', 'ROGWDSQ', 'REHTIA2', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('918', 'CUYBUV5', 'ROGWDSQ', 'REQTSH6', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('919', 'CUYBUV5', 'ROGWDSQ', 'REBCMII', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('920', 'CUYBUV5', 'ROGWDSQ', 'RE1CPBM', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('921', 'CUYBUV5', 'ROGWDSQ', 'RE31S2J', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('922', 'CUYBUV5', 'ROGWDSQ', 'REU9NJL', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('923', 'CUYBUV5', 'ROGWDSQ', 'RE21B7D', '1', '1462969050000');
INSERT INTO `auth_role_resource` VALUES ('1052', 'CU5J2MG', 'ROFSBR6', 'REF3CR2', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1053', 'CU5J2MG', 'ROFSBR6', 'RE3GD1Y', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1054', 'CU5J2MG', 'ROFSBR6', 'REQJ7F4', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1055', 'CU5J2MG', 'ROFSBR6', 'REFSWCL', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1056', 'CU5J2MG', 'ROFSBR6', 'REVN2B9', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1057', 'CU5J2MG', 'ROFSBR6', 'REN1AJ6', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1058', 'CU5J2MG', 'ROFSBR6', 'REW1R11', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1059', 'CU5J2MG', 'ROFSBR6', 'RES3Y2A', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1060', 'CU5J2MG', 'ROFSBR6', 'READLSH', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1061', 'CU5J2MG', 'ROFSBR6', 'RE3Z9JM', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1062', 'CU5J2MG', 'ROFSBR6', 'REPL3QC', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1063', 'CU5J2MG', 'ROFSBR6', 'RE6C3LH', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1064', 'CU5J2MG', 'ROFSBR6', 'REJR63X', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1065', 'CU5J2MG', 'ROFSBR6', 'RE8EAF8', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1066', 'CU5J2MG', 'ROFSBR6', 'RE6U41F', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1067', 'CU5J2MG', 'ROFSBR6', 'RES7JRY', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1068', 'CU5J2MG', 'ROFSBR6', 'RE64T8E', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1069', 'CU5J2MG', 'ROFSBR6', 'REF8J7L', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1070', 'CU5J2MG', 'ROFSBR6', 'REDXWVT', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1071', 'CU5J2MG', 'ROFSBR6', 'RE2TML4', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1072', 'CU5J2MG', 'ROFSBR6', 'REH2K9X', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1073', 'CU5J2MG', 'ROFSBR6', 'RE8GI6X', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1074', 'CU5J2MG', 'ROFSBR6', 'RE71K8J', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1075', 'CU5J2MG', 'ROFSBR6', 'REDGENF', '1', '1468553794000');
INSERT INTO `auth_role_resource` VALUES ('1076', 'CUISUX9', 'RO4YV13', 'REDGENF', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1077', 'CUISUX9', 'RO4YV13', 'RE8GI6X', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1078', 'CUISUX9', 'RO4YV13', 'RE71K8J', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1079', 'CUISUX9', 'RO4YV13', 'REH2K9X', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1080', 'CUISUX9', 'RO4YV13', 'RE2TML4', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1081', 'CUISUX9', 'RO4YV13', 'RES3Y2A', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1082', 'CUISUX9', 'RO4YV13', 'READLSH', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1083', 'CUISUX9', 'RO4YV13', 'RE3Z9JM', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1084', 'CUISUX9', 'RO4YV13', 'REPL3QC', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1085', 'CUISUX9', 'RO4YV13', 'RE6C3LH', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1086', 'CUISUX9', 'RO4YV13', 'REJR63X', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1087', 'CUISUX9', 'RO4YV13', 'RE8EAF8', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1088', 'CUISUX9', 'RO4YV13', 'RE6U41F', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1089', 'CUISUX9', 'RO4YV13', 'RES7JRY', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1090', 'CUISUX9', 'RO4YV13', 'RE64T8E', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1091', 'CUISUX9', 'RO4YV13', 'REF8J7L', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1092', 'CUISUX9', 'RO4YV13', 'REDXWVT', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1093', 'CUISUX9', 'RO4YV13', 'REW1R11', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1094', 'CUISUX9', 'RO4YV13', 'REN1AJ6', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1095', 'CUISUX9', 'RO4YV13', 'REVN2B9', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1096', 'CUISUX9', 'RO4YV13', 'REFSWCL', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1097', 'CUISUX9', 'RO4YV13', 'REQJ7F4', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1098', 'CUISUX9', 'RO4YV13', 'REHXDPW', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1099', 'CUISUX9', 'RO4YV13', 'RE3GD1Y', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1100', 'CUISUX9', 'RO4YV13', 'REF3CR2', '1', '1468642443183');
INSERT INTO `auth_role_resource` VALUES ('1118', 'CUWIZR9', 'ROC2BDX', 'REF3CR2', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1119', 'CUWIZR9', 'ROC2BDX', 'RE3GD1Y', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1120', 'CUWIZR9', 'ROC2BDX', 'REHXDPW', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1121', 'CUWIZR9', 'ROC2BDX', 'REQJ7F4', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1122', 'CUWIZR9', 'ROC2BDX', 'REFSWCL', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1123', 'CUWIZR9', 'ROC2BDX', 'REVN2B9', '1', '1468654350000');
INSERT INTO `auth_role_resource` VALUES ('1124', 'CUGA5NP', 'RO57S64', 'RE6ILBJ', '1', '1468654670731');
INSERT INTO `auth_role_resource` VALUES ('1125', 'CUGA5NP', 'RO57S64', 'REU68BA', '1', '1468654670731');
INSERT INTO `auth_role_resource` VALUES ('1126', 'CUGA5NP', 'RO57S64', 'RE3H19G', '1', '1468654670731');
INSERT INTO `auth_role_resource` VALUES ('1153', 'CU9EENT', 'ROFC9AW', 'RE6ILBJ', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1154', 'CU9EENT', 'ROFC9AW', 'RE3H19G', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1155', 'CU9EENT', 'ROFC9AW', 'REC5WYR', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1156', 'CU9EENT', 'ROFC9AW', 'REDPI5E', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1157', 'CU9EENT', 'ROFC9AW', 'REU9NJL', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1158', 'CU9EENT', 'ROFC9AW', 'RE21B7D', '1', '1468645511000');
INSERT INTO `auth_role_resource` VALUES ('1192', 'CU5RWXY', 'ROWD31H', 'RE6ILBJ', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1193', 'CU5RWXY', 'ROWD31H', 'REHTIA2', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1194', 'CU5RWXY', 'ROWD31H', 'REQTSH6', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1195', 'CU5RWXY', 'ROWD31H', 'REU9NJL', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1196', 'CU5RWXY', 'ROWD31H', 'RET98UK', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1197', 'CU5RWXY', 'ROWD31H', 'REQ51ZS', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1198', 'CU5RWXY', 'ROWD31H', 'REH8AFA', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1199', 'CU5RWXY', 'ROWD31H', 'REYPFP2', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1200', 'CU5RWXY', 'ROWD31H', 'REBCMII', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1201', 'CU5RWXY', 'ROWD31H', 'RE1CPBM', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1202', 'CU5RWXY', 'ROWD31H', 'RE31S2J', '1', '1468651774000');
INSERT INTO `auth_role_resource` VALUES ('1230', 'CU7NG95', 'ROMTU2Q', 'REC5WYR', '1', '1469087715621');
INSERT INTO `auth_role_resource` VALUES ('1231', 'CU7NG95', 'ROMTU2Q', 'RE3H19G', '1', '1469087715621');
INSERT INTO `auth_role_resource` VALUES ('1232', 'CU7NG95', 'ROMTU2Q', 'REDPI5E', '1', '1469087715621');
INSERT INTO `auth_role_resource` VALUES ('1233', 'CUNDAHS', 'ROHGICQ', 'REF3CR2', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1234', 'CUNDAHS', 'ROHGICQ', 'RE3GD1Y', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1235', 'CUNDAHS', 'ROHGICQ', 'REQJ7F4', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1236', 'CUNDAHS', 'ROHGICQ', 'REFSWCL', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1237', 'CUNDAHS', 'ROHGICQ', 'REVN2B9', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1238', 'CUNDAHS', 'ROHGICQ', 'REN1AJ6', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1239', 'CUNDAHS', 'ROHGICQ', 'REW1R11', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1240', 'CUNDAHS', 'ROHGICQ', 'RE2TML4', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1241', 'CUNDAHS', 'ROHGICQ', 'RES3Y2A', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1242', 'CUNDAHS', 'ROHGICQ', 'READLSH', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1243', 'CUNDAHS', 'ROHGICQ', 'RE3Z9JM', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1244', 'CUNDAHS', 'ROHGICQ', 'REPL3QC', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1245', 'CUNDAHS', 'ROHGICQ', 'RE6C3LH', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1246', 'CUNDAHS', 'ROHGICQ', 'REJR63X', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1247', 'CUNDAHS', 'ROHGICQ', 'RE8EAF8', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1248', 'CUNDAHS', 'ROHGICQ', 'RE6U41F', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1249', 'CUNDAHS', 'ROHGICQ', 'RES7JRY', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1250', 'CUNDAHS', 'ROHGICQ', 'RE64T8E', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1251', 'CUNDAHS', 'ROHGICQ', 'REF8J7L', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1252', 'CUNDAHS', 'ROHGICQ', 'REDXWVT', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1253', 'CUNDAHS', 'ROHGICQ', 'REDGENF', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1254', 'CUNDAHS', 'ROHGICQ', 'RE8GI6X', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1255', 'CUNDAHS', 'ROHGICQ', 'RE71K8J', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1256', 'CUNDAHS', 'ROHGICQ', 'REGEYUS', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1257', 'CUNDAHS', 'ROHGICQ', 'RETZVYA', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1258', 'CUNDAHS', 'ROHGICQ', 'REH2K9X', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1259', 'CUNDAHS', 'ROHGICQ', 'REHXDPW', '1', '1469509913634');
INSERT INTO `auth_role_resource` VALUES ('1275', 'CUGC9WK', 'RO8BZD4', 'RE6ILBJ', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1276', 'CUGC9WK', 'RO8BZD4', 'RE3H19G', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1277', 'CUGC9WK', 'RO8BZD4', 'REC5WYR', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1278', 'CUGC9WK', 'RO8BZD4', 'REDPI5E', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1279', 'CUGC9WK', 'RO8BZD4', 'REHTIA2', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1280', 'CUGC9WK', 'RO8BZD4', 'REQTSH6', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1281', 'CUGC9WK', 'RO8BZD4', 'REBCMII', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1282', 'CUGC9WK', 'RO8BZD4', 'RE1CPBM', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1283', 'CUGC9WK', 'RO8BZD4', 'RE31S2J', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1284', 'CUGC9WK', 'RO8BZD4', 'REYPFP2', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1285', 'CUGC9WK', 'RO8BZD4', 'REU9NJL', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1286', 'CUGC9WK', 'RO8BZD4', 'RE21B7D', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1287', 'CUGC9WK', 'RO8BZD4', 'RET98UK', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1288', 'CUGC9WK', 'RO8BZD4', 'REQ51ZS', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1289', 'CUGC9WK', 'RO8BZD4', 'REH8AFA', '1', '1469510941000');
INSERT INTO `auth_role_resource` VALUES ('1315', 'CUQME9A', 'ROJ8TY7', 'RE6ILBJ', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1316', 'CUQME9A', 'ROJ8TY7', 'RE3H19G', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1317', 'CUQME9A', 'ROJ8TY7', 'REC5WYR', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1318', 'CUQME9A', 'ROJ8TY7', 'REDPI5E', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1319', 'CUQME9A', 'ROJ8TY7', 'REYPFP2', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1320', 'CUQME9A', 'ROJ8TY7', 'REHTIA2', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1321', 'CUQME9A', 'ROJ8TY7', 'REQTSH6', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1322', 'CUQME9A', 'ROJ8TY7', 'REBCMII', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1323', 'CUQME9A', 'ROJ8TY7', 'RE1CPBM', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1324', 'CUQME9A', 'ROJ8TY7', 'RE31S2J', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1325', 'CUQME9A', 'ROJ8TY7', 'REU9NJL', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1326', 'CUQME9A', 'ROJ8TY7', 'RE21B7D', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1327', 'CUQME9A', 'ROJ8TY7', 'RET98UK', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1328', 'CUQME9A', 'ROJ8TY7', 'REQ51ZS', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1329', 'CUQME9A', 'ROJ8TY7', 'REH8AFA', '1', '1469517945264');
INSERT INTO `auth_role_resource` VALUES ('1453', 'CU67HI2', 'RO9P59W', 'REF3CR2', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1454', 'CU67HI2', 'RO9P59W', 'RE3GD1Y', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1455', 'CU67HI2', 'RO9P59W', 'REFSWCL', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1456', 'CU67HI2', 'RO9P59W', 'REVN2B9', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1457', 'CU67HI2', 'RO9P59W', 'REN1AJ6', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1458', 'CU67HI2', 'RO9P59W', 'REW1R11', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1459', 'CU67HI2', 'RO9P59W', 'REDGENF', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1460', 'CU67HI2', 'RO9P59W', 'RE8GI6X', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1461', 'CU67HI2', 'RO9P59W', 'RE71K8J', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1462', 'CU67HI2', 'RO9P59W', 'REHXDPW', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1463', 'CU67HI2', 'RO9P59W', 'REQJ7F4', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1464', 'CU67HI2', 'RO9P59W', 'REH2K9X', '1', '1470108930526');
INSERT INTO `auth_role_resource` VALUES ('1465', 'CUA9JU6', 'RO8ACWF', 'REF3CR2', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1466', 'CUA9JU6', 'RO8ACWF', 'RE3GD1Y', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1467', 'CUA9JU6', 'RO8ACWF', 'REQJ7F4', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1468', 'CUA9JU6', 'RO8ACWF', 'REFSWCL', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1469', 'CUA9JU6', 'RO8ACWF', 'REVN2B9', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1470', 'CUA9JU6', 'RO8ACWF', 'REN1AJ6', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1471', 'CUA9JU6', 'RO8ACWF', 'REW1R11', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1472', 'CUA9JU6', 'RO8ACWF', 'REDGENF', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1473', 'CUA9JU6', 'RO8ACWF', 'RE8GI6X', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1474', 'CUA9JU6', 'RO8ACWF', 'RE71K8J', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1475', 'CUA9JU6', 'RO8ACWF', 'REH2K9X', '1', '1470211375023');
INSERT INTO `auth_role_resource` VALUES ('1532', 'CULM169', 'ROGB4G9', 'RE6ILBJ', '1', '1470281286000');
INSERT INTO `auth_role_resource` VALUES ('1533', 'CULM169', 'ROGB4G9', 'RE3H19G', '1', '1470281286000');
INSERT INTO `auth_role_resource` VALUES ('1534', 'CULM169', 'ROGB4G9', 'REC5WYR', '1', '1470281286000');
INSERT INTO `auth_role_resource` VALUES ('1535', 'CULM169', 'ROGB4G9', 'REDPI5E', '1', '1470281286000');
INSERT INTO `auth_role_resource` VALUES ('1536', 'CULM169', 'ROGB4G9', 'REQ51ZS', '1', '1470281286000');
INSERT INTO `auth_role_resource` VALUES ('1537', 'CU8A9SV', 'RORX3P8', 'REF3CR2', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1538', 'CU8A9SV', 'RORX3P8', 'RE3GD1Y', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1539', 'CU8A9SV', 'RORX3P8', 'REQJ7F4', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1540', 'CU8A9SV', 'RORX3P8', 'REFSWCL', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1541', 'CU8A9SV', 'RORX3P8', 'REVN2B9', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1542', 'CU8A9SV', 'RORX3P8', 'REN1AJ6', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1543', 'CU8A9SV', 'RORX3P8', 'REW1R11', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1544', 'CU8A9SV', 'RORX3P8', 'REH2K9X', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1545', 'CU8A9SV', 'RORX3P8', 'REDGENF', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1546', 'CU8A9SV', 'RORX3P8', 'RE8GI6X', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1547', 'CU8A9SV', 'RORX3P8', 'RE71K8J', '1', '1470294360583');
INSERT INTO `auth_role_resource` VALUES ('1548', 'CUIDTYB', 'ROA4PB9', 'RE6ILBJ', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1549', 'CUIDTYB', 'ROA4PB9', 'RE3H19G', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1550', 'CUIDTYB', 'ROA4PB9', 'REU9NJL', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1551', 'CUIDTYB', 'ROA4PB9', 'RET98UK', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1552', 'CUIDTYB', 'ROA4PB9', 'REQ51ZS', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1553', 'CUIDTYB', 'ROA4PB9', 'REH8AFA', '1', '1470297425005');
INSERT INTO `auth_role_resource` VALUES ('1605', 'CU2UGA8', 'ROCL4GV', 'RE6ILBJ', '1', '1470367736764');
INSERT INTO `auth_role_resource` VALUES ('1606', 'CU2UGA8', 'ROCL4GV', 'REDPI5E', '1', '1470367736764');
INSERT INTO `auth_role_resource` VALUES ('1655', 'CUJ4T8H', 'ROVZTES', 'REHXDPW', '1', '1470392615067');
INSERT INTO `auth_role_resource` VALUES ('1656', 'CUJ4T8H', 'ROVZTES', 'REFSWCL', '1', '1470392615067');
INSERT INTO `auth_role_resource` VALUES ('1657', 'CUJ4T8H', 'RO978YG', 'REF3CR2', '1', '1470393651988');
INSERT INTO `auth_role_resource` VALUES ('1658', 'CUJ4T8H', 'RO978YG', 'RE3GD1Y', '1', '1470393651988');
INSERT INTO `auth_role_resource` VALUES ('1659', 'CUJ4T8H', 'RO978YG', 'REHXDPW', '1', '1470393651988');
INSERT INTO `auth_role_resource` VALUES ('1660', 'CU1M9WR', 'ROLSNN7', 'REF3CR2', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1661', 'CU1M9WR', 'ROLSNN7', 'RE3GD1Y', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1662', 'CU1M9WR', 'ROLSNN7', 'REQJ7F4', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1663', 'CU1M9WR', 'ROLSNN7', 'REFSWCL', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1664', 'CU1M9WR', 'ROLSNN7', 'REVN2B9', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1665', 'CU1M9WR', 'ROLSNN7', 'REW1R11', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1666', 'CU1M9WR', 'ROLSNN7', 'RES3Y2A', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1667', 'CU1M9WR', 'ROLSNN7', 'READLSH', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1668', 'CU1M9WR', 'ROLSNN7', 'RE3Z9JM', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1669', 'CU1M9WR', 'ROLSNN7', 'REPL3QC', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1670', 'CU1M9WR', 'ROLSNN7', 'RE6C3LH', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1671', 'CU1M9WR', 'ROLSNN7', 'REJR63X', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1672', 'CU1M9WR', 'ROLSNN7', 'RE8EAF8', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1673', 'CU1M9WR', 'ROLSNN7', 'RE6U41F', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1674', 'CU1M9WR', 'ROLSNN7', 'RES7JRY', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1675', 'CU1M9WR', 'ROLSNN7', 'RE64T8E', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1676', 'CU1M9WR', 'ROLSNN7', 'REF8J7L', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1677', 'CU1M9WR', 'ROLSNN7', 'RE2TML4', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1678', 'CU1M9WR', 'ROLSNN7', 'REDXWVT', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1679', 'CU1M9WR', 'ROLSNN7', 'RE2Q65K', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1680', 'CU1M9WR', 'ROLSNN7', 'RE33QTS', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1681', 'CU1M9WR', 'ROLSNN7', 'REDGENF', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1682', 'CU1M9WR', 'ROLSNN7', 'RE8GI6X', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1683', 'CU1M9WR', 'ROLSNN7', 'RE71K8J', '1', '1467625466000');
INSERT INTO `auth_role_resource` VALUES ('1772', 'CUPDDC1', 'RORSX3E', 'REDPI5E', '1', '1471575359153');
INSERT INTO `auth_role_resource` VALUES ('1773', 'CUPDDC1', 'RORSX3E', 'REU9NJL', '1', '1471575359153');
INSERT INTO `auth_role_resource` VALUES ('1774', 'CUPDDC1', 'RORSX3E', 'REBCMII', '1', '1471575359153');
INSERT INTO `auth_role_resource` VALUES ('1817', 'cloudinnov', 'ROEJVQC', 'REF3CR2', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1818', 'cloudinnov', 'ROEJVQC', 'RE3GD1Y', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1819', 'cloudinnov', 'ROEJVQC', 'REHXDPW', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1820', 'cloudinnov', 'ROEJVQC', 'REQJ7F4', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1821', 'cloudinnov', 'ROEJVQC', 'REFSWCL', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1822', 'cloudinnov', 'ROEJVQC', 'REVN2B9', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1823', 'cloudinnov', 'ROEJVQC', 'RES3Y2A', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1824', 'cloudinnov', 'ROEJVQC', 'READLSH', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1825', 'cloudinnov', 'ROEJVQC', 'RE3Z9JM', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1826', 'cloudinnov', 'ROEJVQC', 'REPL3QC', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1827', 'cloudinnov', 'ROEJVQC', 'RE6C3LH', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1828', 'cloudinnov', 'ROEJVQC', 'REJR63X', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1829', 'cloudinnov', 'ROEJVQC', 'RE8EAF8', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1830', 'cloudinnov', 'ROEJVQC', 'RE6U41F', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1831', 'cloudinnov', 'ROEJVQC', 'RES7JRY', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1832', 'cloudinnov', 'ROEJVQC', 'RE64T8E', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1833', 'cloudinnov', 'ROEJVQC', 'REF8J7L', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1834', 'cloudinnov', 'ROEJVQC', 'REDXWVT', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1835', 'cloudinnov', 'ROEJVQC', 'RE2TML4', '1', '1471924239000');
INSERT INTO `auth_role_resource` VALUES ('1883', 'CU7HSDJ', 'RO1UXPV', 'REMYC5F', '1', '1471973469000');
INSERT INTO `auth_role_resource` VALUES ('1884', 'CU7HSDJ', 'RO1UXPV', 'RE3CQF9', '1', '1471973469000');
INSERT INTO `auth_role_resource` VALUES ('1885', 'CU8YFU5', 'ROWTG4Q', 'REYD8LQ', '1', '1469065510000');
INSERT INTO `auth_role_resource` VALUES ('1887', 'CUXM9D8', 'RO3JSPB', 'RENENHW', '1', '1472007786000');
INSERT INTO `auth_role_resource` VALUES ('1888', 'CUXM9D8', 'RO3JSPB', 'RERMXJR', '1', '1472007786000');
INSERT INTO `auth_role_resource` VALUES ('1890', 'CUPB6CJ', 'ROCDTBM', 'REMYC5F', '1', '1472012050000');
INSERT INTO `auth_role_resource` VALUES ('1891', 'CUPB6CJ', 'ROCDTBM', 'RE3CQF9', '1', '1472012050000');
INSERT INTO `auth_role_resource` VALUES ('1892', 'CUCCYQ5', 'ROQT1IX', 'RE6ILBJ', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1893', 'CUCCYQ5', 'ROQT1IX', 'RE3H19G', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1894', 'CUCCYQ5', 'ROQT1IX', 'REC5WYR', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1895', 'CUCCYQ5', 'ROQT1IX', 'REHTIA2', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1896', 'CUCCYQ5', 'ROQT1IX', 'REQTSH6', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1897', 'CUCCYQ5', 'ROQT1IX', 'REBCMII', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1898', 'CUCCYQ5', 'ROQT1IX', 'RE1CPBM', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1899', 'CUCCYQ5', 'ROQT1IX', 'RE31S2J', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1900', 'CUCCYQ5', 'ROQT1IX', 'REYPFP2', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1901', 'CUCCYQ5', 'ROQT1IX', 'REU9NJL', '1', '1471973309000');
INSERT INTO `auth_role_resource` VALUES ('1912', 'CUDX738', 'RO4XRK3', 'RENENHW', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1913', 'CUDX738', 'RO4XRK3', 'RERMXJR', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1914', 'CUDX738', 'RO4XRK3', 'RE8RTH8', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1915', 'CUDX738', 'RO4XRK3', 'RE6FIJB', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1916', 'CUDX738', 'RO4XRK3', 'RE5QINV', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1917', 'CUDX738', 'RO4XRK3', 'REYH3JV', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1918', 'CUDX738', 'RO4XRK3', 'REQ8NT1', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1919', 'CUDX738', 'RO4XRK3', 'REXVYQ2', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1920', 'CUDX738', 'RO4XRK3', 'RE7ZQ8A', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1921', 'CUDX738', 'RO4XRK3', 'RE9G65K', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1922', 'CUDX738', 'RO4XRK3', 'REABM68', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1923', 'CUDX738', 'RO4XRK3', 'REQ4U3D', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1924', 'CUDX738', 'RO4XRK3', 'REXC74E', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1925', 'CUDX738', 'RO4XRK3', 'REIC9XI', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1926', 'CUDX738', 'RO4XRK3', 'REMCRYZ', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1927', 'CUDX738', 'RO4XRK3', 'RE2A8K9', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1928', 'CUDX738', 'RO4XRK3', 'REQ6ZNE', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1929', 'CUDX738', 'RO4XRK3', 'REG6AK8', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1930', 'CUDX738', 'RO4XRK3', 'RE25G1T', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1931', 'CUDX738', 'RO4XRK3', 'REABDKE', '1', '1470279471000');
INSERT INTO `auth_role_resource` VALUES ('1958', 'CU21MDZ', 'RO5XX28', 'RE6ILBJ', '1', '1464599026000');
INSERT INTO `auth_role_resource` VALUES ('1959', 'CU21MDZ', 'RO5XX28', 'RE3H19G', '1', '1464599026000');
INSERT INTO `auth_role_resource` VALUES ('1960', 'CU21MDZ', 'RO5XX28', 'REC5WYR', '1', '1464599026000');
INSERT INTO `auth_role_resource` VALUES ('1961', 'CU21MDZ', 'RO5XX28', 'REDPI5E', '1', '1464599026000');
INSERT INTO `auth_role_resource` VALUES ('2156', 'cloudinnov', 'ROEUFPE', 'REF3CR2', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2157', 'cloudinnov', 'ROEUFPE', 'RE3GD1Y', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2158', 'cloudinnov', 'ROEUFPE', 'REQJ7F4', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2159', 'cloudinnov', 'ROEUFPE', 'REFSWCL', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2160', 'cloudinnov', 'ROEUFPE', 'REVN2B9', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2161', 'cloudinnov', 'ROEUFPE', 'REN1AJ6', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2162', 'cloudinnov', 'ROEUFPE', 'REW1R11', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2163', 'cloudinnov', 'ROEUFPE', 'RES3Y2A', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2164', 'cloudinnov', 'ROEUFPE', 'READLSH', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2165', 'cloudinnov', 'ROEUFPE', 'RE3Z9JM', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2166', 'cloudinnov', 'ROEUFPE', 'REPL3QC', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2167', 'cloudinnov', 'ROEUFPE', 'RE6C3LH', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2168', 'cloudinnov', 'ROEUFPE', 'REJR63X', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2169', 'cloudinnov', 'ROEUFPE', 'RE8EAF8', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2170', 'cloudinnov', 'ROEUFPE', 'RE6U41F', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2171', 'cloudinnov', 'ROEUFPE', 'RES7JRY', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2172', 'cloudinnov', 'ROEUFPE', 'RE64T8E', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2173', 'cloudinnov', 'ROEUFPE', 'REF8J7L', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2174', 'cloudinnov', 'ROEUFPE', 'REDXWVT', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2175', 'cloudinnov', 'ROEUFPE', 'RE2TML4', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2176', 'cloudinnov', 'ROEUFPE', 'REH2K9X', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2177', 'cloudinnov', 'ROEUFPE', 'RETZVYA', '1', '1462286917000');
INSERT INTO `auth_role_resource` VALUES ('2186', 'cloudinnov', 'ROV46CM', 'REF3CR2', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2187', 'cloudinnov', 'ROV46CM', 'RE3GD1Y', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2188', 'cloudinnov', 'ROV46CM', 'REQJ7F4', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2189', 'cloudinnov', 'ROV46CM', 'REFSWCL', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2190', 'cloudinnov', 'ROV46CM', 'REVN2B9', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2191', 'cloudinnov', 'ROV46CM', 'REN1AJ6', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2192', 'cloudinnov', 'ROV46CM', 'REW1R11', '1', '1463647628000');
INSERT INTO `auth_role_resource` VALUES ('2270', 'CU66D23', 'ROGNAQF', 'RE6ILBJ', '1', '1474260666422');
INSERT INTO `auth_role_resource` VALUES ('2271', 'CU66D23', 'ROGNAQF', 'RE3H19G', '1', '1474260666422');
INSERT INTO `auth_role_resource` VALUES ('2273', 'CU66D23', 'RO2VFC2', 'RE6ILBJ', '1', '1467165680000');
INSERT INTO `auth_role_resource` VALUES ('2274', 'CU66D23', 'RO2VFC2', 'REC5WYR', '1', '1467165680000');
INSERT INTO `auth_role_resource` VALUES ('2275', 'CU66D23', 'RO2VFC2', 'REDPI5E', '1', '1467165680000');
INSERT INTO `auth_role_resource` VALUES ('2276', 'CU66D23', 'ROL3UV5', 'RE6ILBJ', '1', '1474265323000');
INSERT INTO `auth_role_resource` VALUES ('2318', 'CUWHNJU', 'ROTAIJ7', 'RE6ILBJ', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2319', 'CUWHNJU', 'ROTAIJ7', 'RE3H19G', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2320', 'CUWHNJU', 'ROTAIJ7', 'REC5WYR', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2321', 'CUWHNJU', 'ROTAIJ7', 'REDPI5E', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2322', 'CUWHNJU', 'ROTAIJ7', 'REYPFP2', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2323', 'CUWHNJU', 'ROTAIJ7', 'REHTIA2', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2324', 'CUWHNJU', 'ROTAIJ7', 'REQTSH6', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2325', 'CUWHNJU', 'ROTAIJ7', 'REU9NJL', '1', '1474702457710');
INSERT INTO `auth_role_resource` VALUES ('2326', 'CUWHNJU', 'ROK981P', 'RE6ILBJ', '1', '1474702475903');
INSERT INTO `auth_role_resource` VALUES ('2327', 'CUWHNJU', 'ROK981P', 'RE3H19G', '1', '1474702475903');
INSERT INTO `auth_role_resource` VALUES ('2328', 'CUWHNJU', 'ROK981P', 'REC5WYR', '1', '1474702475903');
INSERT INTO `auth_role_resource` VALUES ('2329', 'CUWHNJU', 'ROK981P', 'REDPI5E', '1', '1474702475903');
INSERT INTO `auth_role_resource` VALUES ('2330', 'CUWHNJU', 'ROK981P', 'REU9NJL', '1', '1474702475903');
INSERT INTO `auth_role_resource` VALUES ('2331', 'CUWHNJU', 'ROV1V1P', 'RE6ILBJ', '1', '1474702486245');
INSERT INTO `auth_role_resource` VALUES ('2332', 'CUWHNJU', 'ROV1V1P', 'RE3H19G', '1', '1474702486245');
INSERT INTO `auth_role_resource` VALUES ('2333', 'CUWHNJU', 'ROV1V1P', 'REC5WYR', '1', '1474702486245');
INSERT INTO `auth_role_resource` VALUES ('2334', 'CUWHNJU', 'ROV1V1P', 'REDPI5E', '1', '1474702486245');
INSERT INTO `auth_role_resource` VALUES ('2335', 'CUWHNJU', 'ROV1V1P', 'REU9NJL', '1', '1474702486245');
INSERT INTO `auth_role_resource` VALUES ('2336', 'CUWHNJU', 'ROECJ39', 'RE6ILBJ', '1', '1474702500264');
INSERT INTO `auth_role_resource` VALUES ('2337', 'CUWHNJU', 'ROECJ39', 'RE3H19G', '1', '1474702500264');
INSERT INTO `auth_role_resource` VALUES ('2338', 'CUWHNJU', 'ROECJ39', 'REDPI5E', '1', '1474702500264');
INSERT INTO `auth_role_resource` VALUES ('2369', 'CUWHNJU', 'RO1NT9U', 'RE6ILBJ', '1', '1474702921000');
INSERT INTO `auth_role_resource` VALUES ('2370', 'CUWHNJU', 'RO1NT9U', 'RE3H19G', '1', '1474702921000');
INSERT INTO `auth_role_resource` VALUES ('2372', 'cloudinnov', 'cloudinnov-role', 'REQJ7F4', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2373', 'cloudinnov', 'cloudinnov-role', 'REFSWCL', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2374', 'cloudinnov', 'cloudinnov-role', 'REVN2B9', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2375', 'cloudinnov', 'cloudinnov-role', 'RES3Y2A', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2376', 'cloudinnov', 'cloudinnov-role', 'RE3Z9JM', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2377', 'cloudinnov', 'cloudinnov-role', 'REPL3QC', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2378', 'cloudinnov', 'cloudinnov-role', 'RE6C3LH', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2379', 'cloudinnov', 'cloudinnov-role', 'REJR63X', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2380', 'cloudinnov', 'cloudinnov-role', 'RES7JRY', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2381', 'cloudinnov', 'cloudinnov-role', 'RE64T8E', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2382', 'cloudinnov', 'cloudinnov-role', 'REF8J7L', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2383', 'cloudinnov', 'cloudinnov-role', 'REU3BAM', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2384', 'cloudinnov', 'cloudinnov-role', 'RE2TML4', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2385', 'cloudinnov', 'cloudinnov-role', 'RETZVYA', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2386', 'cloudinnov', 'cloudinnov-role', 'REGEYUS', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2387', 'cloudinnov', 'cloudinnov-role', 'REZT69Q', '1', '1463037660');
INSERT INTO `auth_role_resource` VALUES ('2388', 'CUY4GJK', 'RO9DVNR', 'RE5QINV', '1', '1474882371475');
INSERT INTO `auth_role_resource` VALUES ('2406', 'CUXM9D8', 'ROQQCJB', 'RENENHW', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2407', 'CUXM9D8', 'ROQQCJB', 'RE6FIJB', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2408', 'CUXM9D8', 'ROQQCJB', 'RE5QINV', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2409', 'CUXM9D8', 'ROQQCJB', 'RE9G65K', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2410', 'CUXM9D8', 'ROQQCJB', 'REABM68', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2411', 'CUXM9D8', 'ROQQCJB', 'REYH3JV', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2412', 'CUXM9D8', 'ROQQCJB', 'REQ8NT1', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2413', 'CUXM9D8', 'ROQQCJB', 'REXVYQ2', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2414', 'CUXM9D8', 'ROQQCJB', 'RE7ZQ8A', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2415', 'CUXM9D8', 'ROQQCJB', 'REXC74E', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2416', 'CUXM9D8', 'ROQQCJB', 'REIC9XI', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2417', 'CUXM9D8', 'ROQQCJB', 'REMCRYZ', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2418', 'CUXM9D8', 'ROQQCJB', 'REIFNAG', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2419', 'CUXM9D8', 'ROQQCJB', 'REVY5UA', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2420', 'CUXM9D8', 'ROQQCJB', 'REQ4U3D', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2421', 'CUXM9D8', 'ROQQCJB', 'RERMXJR', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2422', 'CUXM9D8', 'ROQQCJB', 'RECPALD', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2423', 'CUXM9D8', 'ROQQCJB', 'RE25MRN', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2424', 'CUXM9D8', 'ROQQCJB', 'REB712G', '1', '1471575397000');
INSERT INTO `auth_role_resource` VALUES ('2446', 'CUWHNJU', 'ROV3GXA', 'RE6ILBJ', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2447', 'CUWHNJU', 'ROV3GXA', 'REC5WYR', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2448', 'CUWHNJU', 'ROV3GXA', 'RE3H19G', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2449', 'CUWHNJU', 'ROV3GXA', 'REDPI5E', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2450', 'CUWHNJU', 'ROV3GXA', 'REYPFP2', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2451', 'CUWHNJU', 'ROV3GXA', 'REHTIA2', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2452', 'CUWHNJU', 'ROV3GXA', 'REQTSH6', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2453', 'CUWHNJU', 'ROV3GXA', 'REU9NJL', '1', '1474877939000');
INSERT INTO `auth_role_resource` VALUES ('2454', 'CUY4GJK', 'ROAQQFP', 'RE9IAF5', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2455', 'CUY4GJK', 'ROAQQFP', 'REB22VF', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2456', 'CUY4GJK', 'ROAQQFP', 'REUDHKN', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2457', 'CUY4GJK', 'ROAQQFP', 'RE8KXT7', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2458', 'CUY4GJK', 'ROAQQFP', 'RELA7LX', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2459', 'CUY4GJK', 'ROAQQFP', 'REPYVUJ', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2460', 'CUY4GJK', 'ROAQQFP', 'RE5RC73', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2461', 'CUY4GJK', 'ROAQQFP', 'REUSQ38', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2462', 'CUY4GJK', 'ROAQQFP', 'RE13IH7', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2463', 'CUY4GJK', 'ROAQQFP', 'REQ1RI5', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2464', 'CUY4GJK', 'ROAQQFP', 'REGDLBL', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2465', 'CUY4GJK', 'ROAQQFP', 'RESMGQQ', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2466', 'CUY4GJK', 'ROAQQFP', 'RE83QZE', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2467', 'CUY4GJK', 'ROAQQFP', 'REIJMXJ', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2468', 'CUY4GJK', 'ROAQQFP', 'RERTSV7', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2469', 'CUY4GJK', 'ROAQQFP', 'REJDMR4', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2470', 'CUY4GJK', 'ROAQQFP', 'REXUJEN', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2471', 'CUY4GJK', 'ROAQQFP', 'REX9DCR', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2472', 'CUY4GJK', 'ROAQQFP', 'RECE2F1', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2473', 'CUY4GJK', 'ROAQQFP', 'RE9KHQI', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2474', 'CUY4GJK', 'ROAQQFP', 'RE858N3', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2475', 'CUY4GJK', 'ROAQQFP', 'REDV46J', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2476', 'CUY4GJK', 'ROAQQFP', 'RE1GADU', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2477', 'CUY4GJK', 'ROAQQFP', 'REWJ61S', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2478', 'CUY4GJK', 'ROAQQFP', 'REZP81H', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2479', 'CUY4GJK', 'ROAQQFP', 'REHSF6Y', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2480', 'CUY4GJK', 'ROAQQFP', 'RE1CFT6', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2481', 'CUY4GJK', 'ROAQQFP', 'REQ8PTJ', '1', '1476340831070');
INSERT INTO `auth_role_resource` VALUES ('2482', 'CUXM9D8', 'RODQCCR', 'RERMXJR', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2483', 'CUXM9D8', 'RODQCCR', 'RE6FIJB', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2484', 'CUXM9D8', 'RODQCCR', 'RE5QINV', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2485', 'CUXM9D8', 'RODQCCR', 'REYH3JV', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2486', 'CUXM9D8', 'RODQCCR', 'REQ8NT1', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2487', 'CUXM9D8', 'RODQCCR', 'REXVYQ2', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2488', 'CUXM9D8', 'RODQCCR', 'RE7ZQ8A', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2489', 'CUXM9D8', 'RODQCCR', 'REXC74E', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2490', 'CUXM9D8', 'RODQCCR', 'REIC9XI', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2491', 'CUXM9D8', 'RODQCCR', 'REMCRYZ', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2492', 'CUXM9D8', 'RODQCCR', 'REIFNAG', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2493', 'CUXM9D8', 'RODQCCR', 'REVY5UA', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2494', 'CUXM9D8', 'RODQCCR', 'REQ4U3D', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2495', 'CUXM9D8', 'RODQCCR', 'RECPALD', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2496', 'CUXM9D8', 'RODQCCR', 'RE25MRN', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2497', 'CUXM9D8', 'RODQCCR', 'REB712G', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2498', 'CUXM9D8', 'RODQCCR', 'RENENHW', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2499', 'CUXM9D8', 'RODQCCR', 'RE9IAF5', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2500', 'CUXM9D8', 'RODQCCR', 'REB22VF', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2501', 'CUXM9D8', 'RODQCCR', 'REUDHKN', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2502', 'CUXM9D8', 'RODQCCR', 'RE8KXT7', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2503', 'CUXM9D8', 'RODQCCR', 'RELA7LX', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2504', 'CUXM9D8', 'RODQCCR', 'REPYVUJ', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2505', 'CUXM9D8', 'RODQCCR', 'RE5RC73', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2506', 'CUXM9D8', 'RODQCCR', 'REUSQ38', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2507', 'CUXM9D8', 'RODQCCR', 'RE13IH7', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2508', 'CUXM9D8', 'RODQCCR', 'REQ1RI5', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2509', 'CUXM9D8', 'RODQCCR', 'REGDLBL', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2510', 'CUXM9D8', 'RODQCCR', 'RESMGQQ', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2511', 'CUXM9D8', 'RODQCCR', 'RE83QZE', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2512', 'CUXM9D8', 'RODQCCR', 'REIJMXJ', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2513', 'CUXM9D8', 'RODQCCR', 'RERTSV7', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2514', 'CUXM9D8', 'RODQCCR', 'REJDMR4', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2515', 'CUXM9D8', 'RODQCCR', 'REXUJEN', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2516', 'CUXM9D8', 'RODQCCR', 'REX9DCR', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2517', 'CUXM9D8', 'RODQCCR', 'RECE2F1', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2518', 'CUXM9D8', 'RODQCCR', 'RE9KHQI', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2519', 'CUXM9D8', 'RODQCCR', 'RE858N3', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2520', 'CUXM9D8', 'RODQCCR', 'REDV46J', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2521', 'CUXM9D8', 'RODQCCR', 'RE1GADU', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2522', 'CUXM9D8', 'RODQCCR', 'REWJ61S', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2523', 'CUXM9D8', 'RODQCCR', 'REZP81H', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2524', 'CUXM9D8', 'RODQCCR', 'REHSF6Y', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2525', 'CUXM9D8', 'RODQCCR', 'RE1CFT6', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2526', 'CUXM9D8', 'RODQCCR', 'REQ8PTJ', '1', '1460614867000');
INSERT INTO `auth_role_resource` VALUES ('2555', 'CUXM9D8', 'RO2T76U', 'RE9IAF5', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2556', 'CUXM9D8', 'RO2T76U', 'REB22VF', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2557', 'CUXM9D8', 'RO2T76U', 'REUDHKN', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2558', 'CUXM9D8', 'RO2T76U', 'RE8KXT7', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2559', 'CUXM9D8', 'RO2T76U', 'RELA7LX', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2560', 'CUXM9D8', 'RO2T76U', 'REPYVUJ', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2561', 'CUXM9D8', 'RO2T76U', 'RE5RC73', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2562', 'CUXM9D8', 'RO2T76U', 'REUSQ38', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2563', 'CUXM9D8', 'RO2T76U', 'RE13IH7', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2564', 'CUXM9D8', 'RO2T76U', 'REQ1RI5', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2565', 'CUXM9D8', 'RO2T76U', 'REGDLBL', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2566', 'CUXM9D8', 'RO2T76U', 'RESMGQQ', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2567', 'CUXM9D8', 'RO2T76U', 'RE83QZE', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2568', 'CUXM9D8', 'RO2T76U', 'REIJMXJ', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2569', 'CUXM9D8', 'RO2T76U', 'RERTSV7', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2570', 'CUXM9D8', 'RO2T76U', 'REJDMR4', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2571', 'CUXM9D8', 'RO2T76U', 'REXUJEN', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2572', 'CUXM9D8', 'RO2T76U', 'REX9DCR', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2573', 'CUXM9D8', 'RO2T76U', 'RECE2F1', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2574', 'CUXM9D8', 'RO2T76U', 'RE9KHQI', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2575', 'CUXM9D8', 'RO2T76U', 'RE858N3', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2576', 'CUXM9D8', 'RO2T76U', 'REDV46J', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2577', 'CUXM9D8', 'RO2T76U', 'RE1GADU', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2578', 'CUXM9D8', 'RO2T76U', 'REWJ61S', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2579', 'CUXM9D8', 'RO2T76U', 'REZP81H', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2580', 'CUXM9D8', 'RO2T76U', 'REHSF6Y', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2581', 'CUXM9D8', 'RO2T76U', 'RE1CFT6', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2582', 'CUXM9D8', 'RO2T76U', 'REQ8PTJ', '1', '1474962260000');
INSERT INTO `auth_role_resource` VALUES ('2583', 'CU7BN8A', 'ROG1NPD', 'RE6ILBJ', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2584', 'CU7BN8A', 'ROG1NPD', 'RE3H19G', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2585', 'CU7BN8A', 'ROG1NPD', 'REC5WYR', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2586', 'CU7BN8A', 'ROG1NPD', 'REDPI5E', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2587', 'CU7BN8A', 'ROG1NPD', 'REYPFP2', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2588', 'CU7BN8A', 'ROG1NPD', 'REHTIA2', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2589', 'CU7BN8A', 'ROG1NPD', 'REQTSH6', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2590', 'CU7BN8A', 'ROG1NPD', 'REU9NJL', '1', '1476770532218');
INSERT INTO `auth_role_resource` VALUES ('2619', 'CUZQBD6', 'RO86134', 'RE6ILBJ', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2620', 'CUZQBD6', 'RO86134', 'RE3H19G', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2621', 'CUZQBD6', 'RO86134', 'REC5WYR', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2622', 'CUZQBD6', 'RO86134', 'REYPFP2', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2623', 'CUZQBD6', 'RO86134', 'REHTIA2', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2624', 'CUZQBD6', 'RO86134', 'REQTSH6', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2625', 'CUZQBD6', 'RO86134', 'REDPI5E', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2626', 'CUZQBD6', 'RO86134', 'REU9NJL', '1', '1476785491183');
INSERT INTO `auth_role_resource` VALUES ('2657', 'CUQ7UW8', 'ROVPJU4', 'RE9IAF5', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2658', 'CUQ7UW8', 'ROVPJU4', 'REB22VF', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2659', 'CUQ7UW8', 'ROVPJU4', 'REUDHKN', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2660', 'CUQ7UW8', 'ROVPJU4', 'RE8KXT7', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2661', 'CUQ7UW8', 'ROVPJU4', 'RELA7LX', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2662', 'CUQ7UW8', 'ROVPJU4', 'REPYVUJ', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2663', 'CUQ7UW8', 'ROVPJU4', 'RE5RC73', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2664', 'CUQ7UW8', 'ROVPJU4', 'REUSQ38', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2665', 'CUQ7UW8', 'ROVPJU4', 'RE13IH7', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2666', 'CUQ7UW8', 'ROVPJU4', 'REQ1RI5', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2667', 'CUQ7UW8', 'ROVPJU4', 'RERJ4ND', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2668', 'CUQ7UW8', 'ROVPJU4', 'REGDLBL', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2669', 'CUQ7UW8', 'ROVPJU4', 'RESMGQQ', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2670', 'CUQ7UW8', 'ROVPJU4', 'RE83QZE', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2671', 'CUQ7UW8', 'ROVPJU4', 'REIJMXJ', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2672', 'CUQ7UW8', 'ROVPJU4', 'RERTSV7', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2673', 'CUQ7UW8', 'ROVPJU4', 'REJDMR4', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2674', 'CUQ7UW8', 'ROVPJU4', 'REXUJEN', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2675', 'CUQ7UW8', 'ROVPJU4', 'REX9DCR', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2676', 'CUQ7UW8', 'ROVPJU4', 'RECE2F1', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2677', 'CUQ7UW8', 'ROVPJU4', 'RE9KHQI', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2678', 'CUQ7UW8', 'ROVPJU4', 'RE858N3', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2679', 'CUQ7UW8', 'ROVPJU4', 'REDV46J', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2680', 'CUQ7UW8', 'ROVPJU4', 'RE1GADU', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2681', 'CUQ7UW8', 'ROVPJU4', 'REWJ61S', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2682', 'CUQ7UW8', 'ROVPJU4', 'REZP81H', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2683', 'CUQ7UW8', 'ROVPJU4', 'REHSF6Y', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2684', 'CUQ7UW8', 'ROVPJU4', 'RE1CFT6', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2685', 'CUQ7UW8', 'ROVPJU4', 'REQ8PTJ', '1', '1469511478000');
INSERT INTO `auth_role_resource` VALUES ('2752', 'CUXM9D8', 'ROLQZ5D', 'RENENHW', '1', '1477476368000');
INSERT INTO `auth_role_resource` VALUES ('2753', 'CUXM9D8', 'ROLQZ5D', 'RERMXJR', '1', '1477476368000');
INSERT INTO `auth_role_resource` VALUES ('2754', 'CUXM9D8', 'ROLQZ5D', 'RECPALD', '1', '1477476368000');
INSERT INTO `auth_role_resource` VALUES ('2755', 'CUXM9D8', 'ROLQZ5D', 'RE25MRN', '1', '1477476368000');
INSERT INTO `auth_role_resource` VALUES ('2756', 'CUXM9D8', 'ROLQZ5D', 'REB712G', '1', '1477476368000');
INSERT INTO `auth_role_resource` VALUES ('2757', 'CUXM9D8', 'RO34HLI', 'RENENHW', '1', '1477476961170');
INSERT INTO `auth_role_resource` VALUES ('2758', 'CUXM9D8', 'RO34HLI', 'RERMXJR', '1', '1477476961170');
INSERT INTO `auth_role_resource` VALUES ('2759', 'CUXM9D8', 'RO34HLI', 'RECPALD', '1', '1477476961170');
INSERT INTO `auth_role_resource` VALUES ('2760', 'CUXM9D8', 'RO34HLI', 'RE25MRN', '1', '1477476961170');
INSERT INTO `auth_role_resource` VALUES ('2761', 'CUXM9D8', 'RO34HLI', 'REB712G', '1', '1477476961170');
INSERT INTO `auth_role_resource` VALUES ('2765', 'CUXM9D8', 'RO2Q6BQ', 'RENENHW', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2766', 'CUXM9D8', 'RO2Q6BQ', 'REYH3JV', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2767', 'CUXM9D8', 'RO2Q6BQ', 'REXVYQ2', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2768', 'CUXM9D8', 'RO2Q6BQ', 'REQ4U3D', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2769', 'CUXM9D8', 'RO2Q6BQ', 'REXC74E', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2770', 'CUXM9D8', 'RO2Q6BQ', 'REIC9XI', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2771', 'CUXM9D8', 'RO2Q6BQ', 'REMCRYZ', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2772', 'CUXM9D8', 'RO2Q6BQ', 'REIFNAG', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2773', 'CUXM9D8', 'RO2Q6BQ', 'REVY5UA', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2774', 'CUXM9D8', 'RO2Q6BQ', 'RE9IAF5', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2775', 'CUXM9D8', 'RO2Q6BQ', 'REB22VF', '1', '1477475261000');
INSERT INTO `auth_role_resource` VALUES ('2781', 'CUXM9D8', 'ROI8B52', 'REYH3JV', '1', '1477478473000');
INSERT INTO `auth_role_resource` VALUES ('2782', '', 'ROUGGCS', 'RE9IAF5', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2783', '', 'ROUGGCS', 'REB22VF', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2784', '', 'ROUGGCS', 'REUDHKN', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2785', '', 'ROUGGCS', 'RE8KXT7', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2786', '', 'ROUGGCS', 'RELA7LX', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2787', '', 'ROUGGCS', 'REPYVUJ', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2788', '', 'ROUGGCS', 'RE5RC73', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2789', '', 'ROUGGCS', 'REUSQ38', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2790', '', 'ROUGGCS', 'RE13IH7', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2791', '', 'ROUGGCS', 'REQ1RI5', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2792', '', 'ROUGGCS', 'RERJ4ND', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2793', '', 'ROUGGCS', 'REGDLBL', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2794', '', 'ROUGGCS', 'RESMGQQ', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2795', '', 'ROUGGCS', 'RE83QZE', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2796', '', 'ROUGGCS', 'REIJMXJ', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2797', '', 'ROUGGCS', 'RERTSV7', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2798', '', 'ROUGGCS', 'REJDMR4', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2799', '', 'ROUGGCS', 'REXUJEN', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2800', '', 'ROUGGCS', 'REX9DCR', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2801', '', 'ROUGGCS', 'REM62IE', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2802', '', 'ROUGGCS', 'RELZTGI', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2803', '', 'ROUGGCS', 'RECE2F1', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2804', '', 'ROUGGCS', 'RE9KHQI', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2805', '', 'ROUGGCS', 'RE858N3', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2806', '', 'ROUGGCS', 'REDV46J', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2807', '', 'ROUGGCS', 'RE1GADU', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2808', '', 'ROUGGCS', 'REWJ61S', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2809', '', 'ROUGGCS', 'REZP81H', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2810', '', 'ROUGGCS', 'REHSF6Y', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2811', '', 'ROUGGCS', 'RE1CFT6', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2812', '', 'ROUGGCS', 'REQ8PTJ', '1', '1474617841000');
INSERT INTO `auth_role_resource` VALUES ('2813', '', 'ROSHD1G', 'RE9IAF5', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2814', '', 'ROSHD1G', 'REB22VF', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2815', '', 'ROSHD1G', 'REUDHKN', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2816', '', 'ROSHD1G', 'RE8KXT7', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2817', '', 'ROSHD1G', 'RELA7LX', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2818', '', 'ROSHD1G', 'REPYVUJ', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2819', '', 'ROSHD1G', 'RE5RC73', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2820', '', 'ROSHD1G', 'REUSQ38', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2821', '', 'ROSHD1G', 'RE13IH7', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2822', '', 'ROSHD1G', 'REQ1RI5', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2823', '', 'ROSHD1G', 'RERJ4ND', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2824', '', 'ROSHD1G', 'REGDLBL', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2825', '', 'ROSHD1G', 'RESMGQQ', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2826', '', 'ROSHD1G', 'RE83QZE', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2827', '', 'ROSHD1G', 'REIJMXJ', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2828', '', 'ROSHD1G', 'RERTSV7', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2829', '', 'ROSHD1G', 'REJDMR4', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2830', '', 'ROSHD1G', 'REXUJEN', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2831', '', 'ROSHD1G', 'REX9DCR', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2832', '', 'ROSHD1G', 'REM62IE', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2833', '', 'ROSHD1G', 'RELZTGI', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2834', '', 'ROSHD1G', 'RECE2F1', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2835', '', 'ROSHD1G', 'RE9KHQI', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2836', '', 'ROSHD1G', 'RE858N3', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2837', '', 'ROSHD1G', 'REDV46J', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2838', '', 'ROSHD1G', 'RE1GADU', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2839', '', 'ROSHD1G', 'REWJ61S', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2840', '', 'ROSHD1G', 'REZP81H', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2841', '', 'ROSHD1G', 'REHSF6Y', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2842', '', 'ROSHD1G', 'RE1CFT6', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2843', '', 'ROSHD1G', 'REQ8PTJ', '1', '1474617855000');
INSERT INTO `auth_role_resource` VALUES ('2844', '', 'RO9WIGK', 'RE9IAF5', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2845', '', 'RO9WIGK', 'REB22VF', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2846', '', 'RO9WIGK', 'REUDHKN', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2847', '', 'RO9WIGK', 'RE8KXT7', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2848', '', 'RO9WIGK', 'RELA7LX', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2849', '', 'RO9WIGK', 'REPYVUJ', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2850', '', 'RO9WIGK', 'RE5RC73', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2851', '', 'RO9WIGK', 'REUSQ38', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2852', '', 'RO9WIGK', 'RE13IH7', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2853', '', 'RO9WIGK', 'REQ1RI5', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2854', '', 'RO9WIGK', 'RERJ4ND', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2855', '', 'RO9WIGK', 'REGDLBL', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2856', '', 'RO9WIGK', 'RESMGQQ', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2857', '', 'RO9WIGK', 'RE83QZE', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2858', '', 'RO9WIGK', 'REIJMXJ', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2859', '', 'RO9WIGK', 'RERTSV7', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2860', '', 'RO9WIGK', 'REJDMR4', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2861', '', 'RO9WIGK', 'REXUJEN', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2862', '', 'RO9WIGK', 'REX9DCR', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2863', '', 'RO9WIGK', 'REM62IE', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2864', '', 'RO9WIGK', 'RELZTGI', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2865', '', 'RO9WIGK', 'RECE2F1', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2866', '', 'RO9WIGK', 'RE9KHQI', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2867', '', 'RO9WIGK', 'RE858N3', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2868', '', 'RO9WIGK', 'REDV46J', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2869', '', 'RO9WIGK', 'RE1GADU', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2870', '', 'RO9WIGK', 'REWJ61S', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2871', '', 'RO9WIGK', 'REZP81H', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2872', '', 'RO9WIGK', 'REHSF6Y', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2873', '', 'RO9WIGK', 'RE1CFT6', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2874', '', 'RO9WIGK', 'REQ8PTJ', '1', '1474617865000');
INSERT INTO `auth_role_resource` VALUES ('2875', '', 'ROKJ4JY', 'RE9IAF5', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2876', '', 'ROKJ4JY', 'REB22VF', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2877', '', 'ROKJ4JY', 'REJDMR4', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2878', '', 'ROKJ4JY', 'REXUJEN', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2879', '', 'ROKJ4JY', 'REX9DCR', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2880', '', 'ROKJ4JY', 'REM62IE', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2881', '', 'ROKJ4JY', 'RELZTGI', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2882', '', 'ROKJ4JY', 'RECE2F1', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2883', '', 'ROKJ4JY', 'RE9KHQI', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2884', '', 'ROKJ4JY', 'RE858N3', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2885', '', 'ROKJ4JY', 'REDV46J', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2886', '', 'ROKJ4JY', 'RE1GADU', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2887', '', 'ROKJ4JY', 'REWJ61S', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2888', '', 'ROKJ4JY', 'REZP81H', '1', '1474617890000');
INSERT INTO `auth_role_resource` VALUES ('2918', 'CUUDJBA', 'ROGVBHL', 'RE6ILBJ', '1', '1477635911000');
INSERT INTO `auth_role_resource` VALUES ('2919', 'CUUDJBA', 'ROGVBHL', 'RE3H19G', '1', '1477635911000');
INSERT INTO `auth_role_resource` VALUES ('2920', 'CUK47CG', 'ROHFH3Z', 'RE6ILBJ', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2921', 'CUK47CG', 'ROHFH3Z', 'REC5WYR', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2922', 'CUK47CG', 'ROHFH3Z', 'RE3H19G', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2923', 'CUK47CG', 'ROHFH3Z', 'REDPI5E', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2924', 'CUK47CG', 'ROHFH3Z', 'REYPFP2', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2925', 'CUK47CG', 'ROHFH3Z', 'REHTIA2', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2926', 'CUK47CG', 'ROHFH3Z', 'REQTSH6', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2927', 'CUK47CG', 'ROHFH3Z', 'REU9NJL', '1', '1477706841726');
INSERT INTO `auth_role_resource` VALUES ('2973', 'CURGNML', 'ROZJ69Y', 'RE6ILBJ', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2974', 'CURGNML', 'ROZJ69Y', 'RE3H19G', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2975', 'CURGNML', 'ROZJ69Y', 'REC5WYR', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2976', 'CURGNML', 'ROZJ69Y', 'REDPI5E', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2977', 'CURGNML', 'ROZJ69Y', 'REYPFP2', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2978', 'CURGNML', 'ROZJ69Y', 'REHTIA2', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2979', 'CURGNML', 'ROZJ69Y', 'REQTSH6', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('2980', 'CURGNML', 'ROZJ69Y', 'REU9NJL', '1', '1477893975385');
INSERT INTO `auth_role_resource` VALUES ('3142', 'CUV4SVC', 'RORV562', 'RE9IAF5', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3143', 'CUV4SVC', 'RORV562', 'REUTAFZ', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3144', 'CUV4SVC', 'RORV562', 'REB22VF', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3145', 'CUV4SVC', 'RORV562', 'REUDHKN', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3146', 'CUV4SVC', 'RORV562', 'RE8KXT7', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3147', 'CUV4SVC', 'RORV562', 'RELA7LX', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3148', 'CUV4SVC', 'RORV562', 'REPYVUJ', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3149', 'CUV4SVC', 'RORV562', 'RE5RC73', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3150', 'CUV4SVC', 'RORV562', 'REUSQ38', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3151', 'CUV4SVC', 'RORV562', 'RE13IH7', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3152', 'CUV4SVC', 'RORV562', 'REQ1RI5', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3153', 'CUV4SVC', 'RORV562', 'RERJ4ND', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3154', 'CUV4SVC', 'RORV562', 'REGDLBL', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3155', 'CUV4SVC', 'RORV562', 'RESMGQQ', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3156', 'CUV4SVC', 'RORV562', 'RE83QZE', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3157', 'CUV4SVC', 'RORV562', 'REIJMXJ', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3158', 'CUV4SVC', 'RORV562', 'RERTSV7', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3159', 'CUV4SVC', 'RORV562', 'REJDMR4', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3160', 'CUV4SVC', 'RORV562', 'REXUJEN', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3161', 'CUV4SVC', 'RORV562', 'REX9DCR', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3162', 'CUV4SVC', 'RORV562', 'REM62IE', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3163', 'CUV4SVC', 'RORV562', 'REBMD71', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3164', 'CUV4SVC', 'RORV562', 'RECE2F1', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3165', 'CUV4SVC', 'RORV562', 'RE9KHQI', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3166', 'CUV4SVC', 'RORV562', 'RE858N3', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3167', 'CUV4SVC', 'RORV562', 'REDV46J', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3168', 'CUV4SVC', 'RORV562', 'RE1GADU', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3169', 'CUV4SVC', 'RORV562', 'RENI1E4', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3170', 'CUV4SVC', 'RORV562', 'RED6N8Z', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3171', 'CUV4SVC', 'RORV562', 'REHSF6Y', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3172', 'CUV4SVC', 'RORV562', 'RE1CFT6', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3173', 'CUV4SVC', 'RORV562', 'REQ8PTJ', '1', '1476785125000');
INSERT INTO `auth_role_resource` VALUES ('3233', 'CU6MH81', 'ROII8J4', 'REB22VF', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3234', 'CU6MH81', 'ROII8J4', 'REZU2RG', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3235', 'CU6MH81', 'ROII8J4', 'RE9IAF5', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3236', 'CU6MH81', 'ROII8J4', 'RE8KXT7', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3237', 'CU6MH81', 'ROII8J4', 'RELA7LX', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3238', 'CU6MH81', 'ROII8J4', 'RE83QZE', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3239', 'CU6MH81', 'ROII8J4', 'REPYVUJ', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3240', 'CU6MH81', 'ROII8J4', 'RE5RC73', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3241', 'CU6MH81', 'ROII8J4', 'REUSQ38', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3242', 'CU6MH81', 'ROII8J4', 'RE13IH7', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3243', 'CU6MH81', 'ROII8J4', 'REQ1RI5', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3244', 'CU6MH81', 'ROII8J4', 'RERJ4ND', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3245', 'CU6MH81', 'ROII8J4', 'REIJMXJ', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3246', 'CU6MH81', 'ROII8J4', 'REGDLBL', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3247', 'CU6MH81', 'ROII8J4', 'RESMGQQ', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3248', 'CU6MH81', 'ROII8J4', 'RERTSV7', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3249', 'CU6MH81', 'ROII8J4', 'REUDHKN', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3250', 'CU6MH81', 'ROII8J4', 'RECE2F1', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3251', 'CU6MH81', 'ROII8J4', 'REXUJEN', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3252', 'CU6MH81', 'ROII8J4', 'REX9DCR', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3253', 'CU6MH81', 'ROII8J4', 'RE9KHQI', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3254', 'CU6MH81', 'ROII8J4', 'RE1GADU', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3255', 'CU6MH81', 'ROII8J4', 'REDV46J', '1', '1477880177000');
INSERT INTO `auth_role_resource` VALUES ('3256', 'CUH7BW7', 'ROJW9X9', 'RE6ILBJ', '1', '1478490820874');
INSERT INTO `auth_role_resource` VALUES ('3257', 'CUH7BW7', 'ROJW9X9', 'RE3H19G', '1', '1478490820874');
INSERT INTO `auth_role_resource` VALUES ('4157', 'CUXM9D8', 'ROMEC3Q', 'REB22VF', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4158', 'CUXM9D8', 'ROMEC3Q', 'RE9IAF5', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4159', 'CUXM9D8', 'ROMEC3Q', 'REPYVUJ', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4160', 'CUXM9D8', 'ROMEC3Q', 'RE5RC73', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4161', 'CUXM9D8', 'ROMEC3Q', 'REUSQ38', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4162', 'CUXM9D8', 'ROMEC3Q', 'RE13IH7', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4163', 'CUXM9D8', 'ROMEC3Q', 'REQ1RI5', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4164', 'CUXM9D8', 'ROMEC3Q', 'RERJ4ND', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4165', 'CUXM9D8', 'ROMEC3Q', 'REIJMXJ', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4166', 'CUXM9D8', 'ROMEC3Q', 'REGDLBL', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4167', 'CUXM9D8', 'ROMEC3Q', 'RESMGQQ', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4168', 'CUXM9D8', 'ROMEC3Q', 'RERTSV7', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4169', 'CUXM9D8', 'ROMEC3Q', 'RECE2F1', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4170', 'CUXM9D8', 'ROMEC3Q', 'REXUJEN', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4171', 'CUXM9D8', 'ROMEC3Q', 'REX9DCR', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4172', 'CUXM9D8', 'ROMEC3Q', 'RE9KHQI', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4173', 'CUXM9D8', 'ROMEC3Q', 'REM62IE', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4174', 'CUXM9D8', 'ROMEC3Q', 'RE858N3', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4175', 'CUXM9D8', 'ROMEC3Q', 'REJDMR4', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4176', 'CUXM9D8', 'ROMEC3Q', 'RE1GADU', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4177', 'CUXM9D8', 'ROMEC3Q', 'REDV46J', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4178', 'CUXM9D8', 'ROMEC3Q', 'REZU2RG', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4179', 'CUXM9D8', 'ROMEC3Q', 'RELIAZ3', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4180', 'CUXM9D8', 'ROMEC3Q', 'REG6IRK', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4181', 'CUXM9D8', 'ROMEC3Q', 'REH9V6H', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4182', 'CUXM9D8', 'ROMEC3Q', 'REPSJ91', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4183', 'CUXM9D8', 'ROMEC3Q', 'RE67PSB', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4184', 'CUXM9D8', 'ROMEC3Q', 'REBMD71', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4185', 'CUXM9D8', 'ROMEC3Q', 'RENI1E4', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4186', 'CUXM9D8', 'ROMEC3Q', 'RED6N8Z', '1', '1477547804000');
INSERT INTO `auth_role_resource` VALUES ('4914', 'CUXM9D8', 'ROKWXED', 'REXRGJW', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4915', 'CUXM9D8', 'ROKWXED', 'REB22VF', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4916', 'CUXM9D8', 'ROKWXED', 'REZU2RG', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4917', 'CUXM9D8', 'ROKWXED', 'RE9IAF5', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4918', 'CUXM9D8', 'ROKWXED', 'REV7K7Y', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4919', 'CUXM9D8', 'ROKWXED', 'REWXT24', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4920', 'CUXM9D8', 'ROKWXED', 'REKUDG1', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4921', 'CUXM9D8', 'ROKWXED', 'REEDRHT', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4922', 'CUXM9D8', 'ROKWXED', 'RECE2F1', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4923', 'CUXM9D8', 'ROKWXED', 'REM62IE', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4924', 'CUXM9D8', 'ROKWXED', 'REBMD71', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4925', 'CUXM9D8', 'ROKWXED', 'RE858N3', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4926', 'CUXM9D8', 'ROKWXED', 'REXUJEN', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4927', 'CUXM9D8', 'ROKWXED', 'REX9DCR', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4928', 'CUXM9D8', 'ROKWXED', 'RE9KHQI', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4929', 'CUXM9D8', 'ROKWXED', 'REJDMR4', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4930', 'CUXM9D8', 'ROKWXED', 'RE1M5QQ', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4931', 'CUXM9D8', 'ROKWXED', 'RE1IJSN', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4932', 'CUXM9D8', 'ROKWXED', 'REDXNCQ', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4933', 'CUXM9D8', 'ROKWXED', 'RETCMXN', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4934', 'CUXM9D8', 'ROKWXED', 'REK7116', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4935', 'CUXM9D8', 'ROKWXED', 'RE4DCP3', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4936', 'CUXM9D8', 'ROKWXED', 'RETV3ZD', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4937', 'CUXM9D8', 'ROKWXED', 'REGQEPX', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4938', 'CUXM9D8', 'ROKWXED', 'REXHJDD', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4939', 'CUXM9D8', 'ROKWXED', 'REGDFE2', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4940', 'CUXM9D8', 'ROKWXED', 'REYZ61A', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4941', 'CUXM9D8', 'ROKWXED', 'REYN9T2', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4942', 'CUXM9D8', 'ROKWXED', 'RESACSB', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4943', 'CUXM9D8', 'ROKWXED', 'RE63T1F', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4944', 'CUXM9D8', 'ROKWXED', 'RESU2H4', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4945', 'CUXM9D8', 'ROKWXED', 'RE84EWN', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4946', 'CUXM9D8', 'ROKWXED', 'RENI4P5', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4947', 'CUXM9D8', 'ROKWXED', 'REMA2HU', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4948', 'CUXM9D8', 'ROKWXED', 'REZX1KJ', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4949', 'CUXM9D8', 'ROKWXED', 'REXEF3G', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4950', 'CUXM9D8', 'ROKWXED', 'REB1DMT', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4951', 'CUXM9D8', 'ROKWXED', 'REXKBTL', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4952', 'CUXM9D8', 'ROKWXED', 'RESD9RR', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4953', 'CUXM9D8', 'ROKWXED', 'REXH4NH', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4954', 'CUXM9D8', 'ROKWXED', 'RE2E2HM', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4955', 'CUXM9D8', 'ROKWXED', 'REQ4KSL', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4956', 'CUXM9D8', 'ROKWXED', 'REH7PUY', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4957', 'CUXM9D8', 'ROKWXED', 'REDV46J', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4958', 'CUXM9D8', 'ROKWXED', 'REAHB1B', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4959', 'CUXM9D8', 'ROKWXED', 'REEK48U', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4960', 'CUXM9D8', 'ROKWXED', 'REAAM3A', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4961', 'CUXM9D8', 'ROKWXED', 'RE1CFT6', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4962', 'CUXM9D8', 'ROKWXED', 'REQ8PTJ', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4963', 'CUXM9D8', 'ROKWXED', 'REHSF6Y', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4964', 'CUXM9D8', 'ROKWXED', 'RE2XDKB', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4965', 'CUXM9D8', 'ROKWXED', 'RENMDZF', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4966', 'CUXM9D8', 'ROKWXED', 'RE4N241', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4967', 'CUXM9D8', 'ROKWXED', 'REMD8RW', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4968', 'CUXM9D8', 'ROKWXED', 'RE57Z7N', '1', '1476346006000');
INSERT INTO `auth_role_resource` VALUES ('4969', 'CUXM9D8', 'ROKWXED', 'REWTGTU', '1', '1476346006000');

-- ----------------------------
-- Table structure for auth_role_right
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_right`;
CREATE TABLE `auth_role_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `role_code` varchar(20) NOT NULL COMMENT '部门编号',
  `right_code` varchar(20) NOT NULL COMMENT '资源编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限对应表';

-- ----------------------------
-- Records of auth_role_right
-- ----------------------------

-- ----------------------------
-- Table structure for auth_role_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_user`;
CREATE TABLE `auth_role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `role_code` varchar(20) NOT NULL COMMENT '部门编号',
  `user_code` varchar(20) NOT NULL COMMENT '资源编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=386 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_role_user
-- ----------------------------
INSERT INTO `auth_role_user` VALUES ('1', 'CUXM9D8', 'RODQCCR', 'USD5R1H', '1', '1460614867000');
INSERT INTO `auth_role_user` VALUES ('2', 'CUXM9D8', 'RODQCCR', 'US5N5U9', '1', '1460616093000');
INSERT INTO `auth_role_user` VALUES ('3', 'CUXM9D8', 'RODQCCR', 'USR5ZAP', '1', '1460616141000');
INSERT INTO `auth_role_user` VALUES ('4', 'CULTGG4', 'RO8Y1C4', 'USYWY61', '1', '1460775102000');
INSERT INTO `auth_role_user` VALUES ('5', 'CUTMYL7', 'ROG1LNR', 'USSZG5G', '1', '1460876543000');
INSERT INTO `auth_role_user` VALUES ('6', 'CUTMYL7', 'ROG1LNR', 'USFCF1A', '1', '1460877939000');
INSERT INTO `auth_role_user` VALUES ('7', 'CUTMYL7', 'ROG1LNR', 'US41ADC', '1', '1461074185000');
INSERT INTO `auth_role_user` VALUES ('8', 'CUXVVBJ', 'ROTIFZ1', 'USDREZF', '1', '1461129491000');
INSERT INTO `auth_role_user` VALUES ('9', 'CUXM9D8', 'RODQCCR', 'USDH71Y', '9', '1461858395000');
INSERT INTO `auth_role_user` VALUES ('10', 'CUXM9D8', 'RODQCCR', 'USMZ7SF', '1', '1462284753000');
INSERT INTO `auth_role_user` VALUES ('11', 'CUXM9D8', 'ROEUFPE', 'USD9WJ7', '1', '1462287130000');
INSERT INTO `auth_role_user` VALUES ('12', 'CUDT48I', 'RO9ATWD', 'USLUWL7', '1', '1462287653000');
INSERT INTO `auth_role_user` VALUES ('13', 'CUXM9D8', 'RODQCCR', 'US8Q7B1', '1', '1462354748000');
INSERT INTO `auth_role_user` VALUES ('14', 'CUXM9D8', 'RODQCCR', 'US5E1CF', '1', '1462354907000');
INSERT INTO `auth_role_user` VALUES ('15', 'CU6HHX7', 'ROTI6RK', 'USJHMNW', '1', '1462378549000');
INSERT INTO `auth_role_user` VALUES ('16', 'CU6HHX7', 'ROTI6RK', 'USRUM9Y', '1', '1462452738000');
INSERT INTO `auth_role_user` VALUES ('17', 'CUXM9D8', 'ROXWX9R', 'USVBIHC', '1', '1462452921000');
INSERT INTO `auth_role_user` VALUES ('18', 'CU6HHX7', 'ROZMJQP', 'US3UC84', '1', '1462453256000');
INSERT INTO `auth_role_user` VALUES ('19', 'CUXM9D8', 'RODQCCR', 'US6SS51', '1', '1462499463000');
INSERT INTO `auth_role_user` VALUES ('20', 'CU6HHX7', 'ROEHD9D', 'US2BBVF', '1', '1462500094000');
INSERT INTO `auth_role_user` VALUES ('21', 'CUCDKWQ', 'RONXLZS', 'USQ12IM', '1', '1462940389000');
INSERT INTO `auth_role_user` VALUES ('22', 'CURJBDI', 'ROY2IUU', 'USGQRUG', '1', '1462966983000');
INSERT INTO `auth_role_user` VALUES ('23', 'CUYBUV5', 'ROGWDSQ', 'USZPTMS', '1', '1462969111000');
INSERT INTO `auth_role_user` VALUES ('24', 'CUYBUV5', 'ROGWDSQ', 'USW9VER', '1', '1463037020000');
INSERT INTO `auth_role_user` VALUES ('25', 'CUYBUV5', 'ROGWDSQ', 'USYRWMJ', '1', '1463107226000');
INSERT INTO `auth_role_user` VALUES ('26', 'CUCDKWQ', 'RONXLZS', 'USLTD6N', '1', '1463127735000');
INSERT INTO `auth_role_user` VALUES ('27', 'wwzg-cu-1', 'ROBRNPF', 'USU6BPX', '1', '1463546005000');
INSERT INTO `auth_role_user` VALUES ('28', 'CU5J2MG', 'ROFSBR6', 'USS5PEM', '1', '1463546195000');
INSERT INTO `auth_role_user` VALUES ('29', 'CUXM9D8', 'ROEUFPE', 'US8KSN8', '1', '1463647564000');
INSERT INTO `auth_role_user` VALUES ('30', 'CU2PXKW', 'ROLM3NH', 'USLXN5M', '1', '1463729737000');
INSERT INTO `auth_role_user` VALUES ('31', 'CU2PXKW', 'ROLM3NH', 'USNKXCR', '1', '1463729828000');
INSERT INTO `auth_role_user` VALUES ('32', 'CUTMYL7', 'ROG1LNR', 'USZLF8K', '1', '1463973744000');
INSERT INTO `auth_role_user` VALUES ('33', 'CUAVGM2', 'ROP1Y3L', 'USHSQDZ', '9', '1463983405000');
INSERT INTO `auth_role_user` VALUES ('34', 'CUPM8XR', 'ROLGLAW', 'US8EN5L', '1', '1463989728000');
INSERT INTO `auth_role_user` VALUES ('35', 'CU9LF9L', 'ROEV1T7', 'USYFMDG', '1', '1463989834000');
INSERT INTO `auth_role_user` VALUES ('36', 'CUPGFA4', 'ROWAKKA', 'USV939T', '1', '1464318143000');
INSERT INTO `auth_role_user` VALUES ('37', 'CU21MDZ', 'RO5XX28', 'USSM7DH', '1', '1464599298000');
INSERT INTO `auth_role_user` VALUES ('38', 'CUJR3CD', 'ROB2Y7S', 'USB2R5D', '1', '1464777067000');
INSERT INTO `auth_role_user` VALUES ('39', 'CUJR3CD', 'ROX2DFZ', 'USXHZLF', '1', '1464777158000');
INSERT INTO `auth_role_user` VALUES ('40', 'CUCDKWQ', 'RONXLZS', 'US1W8RQ', '1', '1465827486000');
INSERT INTO `auth_role_user` VALUES ('41', 'CUXM9D8', 'RODQCCR', 'US6WDC1', '1', '1466050562000');
INSERT INTO `auth_role_user` VALUES ('42', 'CUBV3BF', 'RO8EGUS', 'USDNJHA', '1', '1467018018000');
INSERT INTO `auth_role_user` VALUES ('43', 'CU66D23', 'RO2VFC2', 'US9X9JW', '1', '1467165746000');
INSERT INTO `auth_role_user` VALUES ('44', 'CUNI8SX', 'ROQ26Y4', 'USKYU6W', '1', '1467342902000');
INSERT INTO `auth_role_user` VALUES ('45', 'CUXM9D8', 'cloudinnov-role', 'administrator', '1', '1460614867000');
INSERT INTO `auth_role_user` VALUES ('46', 'CUXM9D8', 'cloudinnov-role', 'USG5VTI', '1', '1463120680000');
INSERT INTO `auth_role_user` VALUES ('47', 'CUBLHI7', 'ROJZF6W', 'USP6PLM', '1', '1463129633000');
INSERT INTO `auth_role_user` VALUES ('48', 'CUS9JPF', 'RO9Q5VN', 'USB3JGM', '1', '1463390355000');
INSERT INTO `auth_role_user` VALUES ('49', 'CUH56LT', 'ROQWA45', 'USJSIX6', '1', '1463394316000');
INSERT INTO `auth_role_user` VALUES ('50', 'CUXM9D8', 'cloudinnov-role', 'US7TE54', '9', '1464688369000');
INSERT INTO `auth_role_user` VALUES ('51', 'CUEPHVN', 'RO79ZT2', 'USWWBAZ', '1', '1464781082000');
INSERT INTO `auth_role_user` VALUES ('52', 'CU4HPEF', 'ROV2GEZ', 'USFND2N', '1', '1464781988000');
INSERT INTO `auth_role_user` VALUES ('53', 'CU97FXJ', 'ROBBXE4', 'US1MCY4', '1', '1464933730000');
INSERT INTO `auth_role_user` VALUES ('54', 'CUNDAHS', 'ROKTEX3', 'US24PXE', '1', '1467625388000');
INSERT INTO `auth_role_user` VALUES ('55', 'CUNDAHS', 'ROKTEX3', 'US7Y1US', '1', '1467625404000');
INSERT INTO `auth_role_user` VALUES ('56', 'CU1M9WR', 'ROLSNN7', 'US5IGUP', '1', '1467625611000');
INSERT INTO `auth_role_user` VALUES ('57', 'CUNBBDH', 'ROWRYUP', 'USRPXAW', '1', '1467778045000');
INSERT INTO `auth_role_user` VALUES ('58', 'CUIYZFY', 'ROPNNG3', 'USINMQM', '1', '1467778322000');
INSERT INTO `auth_role_user` VALUES ('59', 'CUNI8SX', 'ROQ26Y4', 'US1IBZG', '1', '1467787589000');
INSERT INTO `auth_role_user` VALUES ('60', 'CUFT1YA', 'ROZPH7M', 'US8EPUH', '1', '1467943528000');
INSERT INTO `auth_role_user` VALUES ('61', 'CU2A1FN', 'ROQCBDA', 'US8PZK5', '1', '1468224162000');
INSERT INTO `auth_role_user` VALUES ('62', 'CUGC9WK', 'ROANH67', 'USCPMY1', '1', '1468292502000');
INSERT INTO `auth_role_user` VALUES ('63', 'CUFGNSA', 'RO6ITC1', 'USGG5T9', '1', '1468392448000');
INSERT INTO `auth_role_user` VALUES ('64', 'CU5J2MG', 'ROFSBR6', 'USLAQ32', '1', '1468553843000');
INSERT INTO `auth_role_user` VALUES ('65', 'CUISUX9', 'RO4YV13', 'USJHUWB', '1', '1468642611000');
INSERT INTO `auth_role_user` VALUES ('66', 'CU9EENT', 'ROFC9AW', 'USAJMDC', '1', '1468645730000');
INSERT INTO `auth_role_user` VALUES ('67', 'CU5RWXY', 'ROWD31H', 'USHBIUC', '1', '1468651865000');
INSERT INTO `auth_role_user` VALUES ('68', 'CUWIZR9', 'ROC2BDX', 'USNP7NE', '1', '1468654438000');
INSERT INTO `auth_role_user` VALUES ('69', 'CUGA5NP', 'RO57S64', 'USV1CCQ', '1', '1468654744000');
INSERT INTO `auth_role_user` VALUES ('70', 'CU5RWXY', 'ROWD31H', 'USDQXXT', '1', '1468654884000');
INSERT INTO `auth_role_user` VALUES ('71', 'CU9EENT', 'ROFC9AW', 'USG78Q9', '1', '1468655798000');
INSERT INTO `auth_role_user` VALUES ('72', 'CU8YFU5', 'ROWTG4Q', 'USAAFCE', '1', '1469065571000');
INSERT INTO `auth_role_user` VALUES ('73', 'CU7NG95', 'ROMTU2Q', 'USUSQ2H', '1', '1469087819000');
INSERT INTO `auth_role_user` VALUES ('74', 'CUNDAHS', 'ROHGICQ', 'USI9EWN', '1', '1469510027000');
INSERT INTO `auth_role_user` VALUES ('75', 'CUGC9WK', 'RO8BZD4', 'USDC2HN', '1', '1469511087000');
INSERT INTO `auth_role_user` VALUES ('76', 'CUQ7UW8', 'ROVPJU4', 'USD9XZK', '1', '1469511633000');
INSERT INTO `auth_role_user` VALUES ('77', 'CUQME9A', 'ROJ8TY7', 'USGBBWI', '1', '1469518065000');
INSERT INTO `auth_role_user` VALUES ('78', 'CU5J2MG', 'ROFSBR6', 'US86H62', '1', '1469593928000');
INSERT INTO `auth_role_user` VALUES ('79', 'CUQ378M', 'RO5QDV7', 'USUFXKC', '1', '1470104549000');
INSERT INTO `auth_role_user` VALUES ('80', 'CU67HI2', 'RO9P59W', 'USDLVJN', '1', '1470109004000');
INSERT INTO `auth_role_user` VALUES ('81', 'CUXM9D8', 'RODQCCR', 'USP9P85', '1', '1470109764000');
INSERT INTO `auth_role_user` VALUES ('82', 'CUA9JU6', 'RO8ACWF', 'USW8Z5H', '1', '1470211887000');
INSERT INTO `auth_role_user` VALUES ('83', 'CUDX738', 'RO4XRK3', 'USKGT4Z', '1', '1470279544000');
INSERT INTO `auth_role_user` VALUES ('84', 'CUDX738', 'RO4XRK3', 'USEKP21', '1', '1470280728000');
INSERT INTO `auth_role_user` VALUES ('85', 'CUDX738', 'RO4XRK3', 'USTIXT3', '1', '1470281050000');
INSERT INTO `auth_role_user` VALUES ('86', 'CULM169', 'ROGB4G9', 'USFHIPD', '1', '1470281711000');
INSERT INTO `auth_role_user` VALUES ('87', 'CULM169', 'ROGB4G9', 'USM14GL', '1', '1470281824000');
INSERT INTO `auth_role_user` VALUES ('88', 'CU8A9SV', 'RORX3P8', 'USZ7AQI', '1', '1470294425000');
INSERT INTO `auth_role_user` VALUES ('89', 'CU8A9SV', 'RORX3P8', 'USXD1RJ', '1', '1470295252000');
INSERT INTO `auth_role_user` VALUES ('90', 'CUIDTYB', 'ROA4PB9', 'USQDHRC', '1', '1470297486000');
INSERT INTO `auth_role_user` VALUES ('91', 'CUTMYL7', 'ROG1LNR', 'US34U42', '1', '1470367307000');
INSERT INTO `auth_role_user` VALUES ('92', 'CUTMYL7', 'ROG1LNR', 'US7US4L', '1', '1470367427000');
INSERT INTO `auth_role_user` VALUES ('93', 'CUTMYL7', 'ROG1LNR', 'US422CL', '1', '1470367564000');
INSERT INTO `auth_role_user` VALUES ('94', 'CU2UGA8', 'ROCL4GV', 'USSCLK2', '1', '1470367805000');
INSERT INTO `auth_role_user` VALUES ('95', 'CUJ4T8H', 'ROVZTES', 'USRNF4K', '1', '1470392703000');
INSERT INTO `auth_role_user` VALUES ('96', 'CUJ4T8H', 'RO978YG', 'USD6YB8', '1', '1470393782000');
INSERT INTO `auth_role_user` VALUES ('97', 'CUJ4T8H', 'RO978YG', 'US5KKRS', '1', '1470395840000');
INSERT INTO `auth_role_user` VALUES ('98', 'CUJ4T8H', 'RO978YG', 'USUR7GJ', '1', '1470396028000');
INSERT INTO `auth_role_user` VALUES ('99', 'CUJ4T8H', 'RO978YG', 'USJYSIK', '1', '1470396322000');
INSERT INTO `auth_role_user` VALUES ('100', 'CUJ4T8H', 'RO978YG', 'USZGDTR', '1', '1470397147000');
INSERT INTO `auth_role_user` VALUES ('101', 'CUJ4T8H', 'RO978YG', 'USYPTUI', '1', '1470397323000');
INSERT INTO `auth_role_user` VALUES ('102', 'CUXM9D8', 'ROQQCJB', 'USAN9Z9', '1', '1471575729000');
INSERT INTO `auth_role_user` VALUES ('103', 'CUPDDC1', 'RORSX3E', 'USFD683', '1', '1471579700000');
INSERT INTO `auth_role_user` VALUES ('104', 'CUPDDC1', 'RORSX3E', 'US6SE39', '1', '1471580187000');
INSERT INTO `auth_role_user` VALUES ('105', 'CUTMYL7', 'ROG1LNR', 'USEBXTU', '1', '1472012311000');
INSERT INTO `auth_role_user` VALUES ('106', 'CU21MDZ', 'RO5XX28', 'USLDF9E', '1', '1472440554000');
INSERT INTO `auth_role_user` VALUES ('107', 'CUXM9D8', 'RO3JSPB', 'USD6YLN', '1', '1472805182000');
INSERT INTO `auth_role_user` VALUES ('108', 'CU7HSDJ', 'RO1UXPV', 'USXZ76Q', '1', '1472805576000');
INSERT INTO `auth_role_user` VALUES ('109', 'CUXM9D8', 'RO3JSPB', 'USBMUWY', '1', '1473739876000');
INSERT INTO `auth_role_user` VALUES ('110', 'CUNI8SX', 'ROQ26Y4', 'US5Q3B1', '1', '1473747549000');
INSERT INTO `auth_role_user` VALUES ('111', 'CUXM9D8', 'RO3JSPB', 'USGD6KA', '1', '1473753410000');
INSERT INTO `auth_role_user` VALUES ('112', 'CUXM9D8', 'RO3JSPB', 'USTBUF2', '1', '1473753526000');
INSERT INTO `auth_role_user` VALUES ('113', 'CUXM9D8', 'RO3JSPB', 'US15DR6', '1', '1473754511000');
INSERT INTO `auth_role_user` VALUES ('114', 'CUXM9D8', 'RO3JSPB', 'USZSXGL', '1', '1474166101000');
INSERT INTO `auth_role_user` VALUES ('115', 'CUXM9D8', 'RO3JSPB', 'US38JP4', '1', '1474166317000');
INSERT INTO `auth_role_user` VALUES ('116', 'CUXM9D8', 'RO3JSPB', 'USEW9XI', '1', '1474166428000');
INSERT INTO `auth_role_user` VALUES ('117', 'CUXM9D8', 'ROQQCJB', 'USRSWJX', '1', '1474166577000');
INSERT INTO `auth_role_user` VALUES ('118', 'CUXM9D8', 'RO3JSPB', 'USI9HB3', '1', '1474166638000');
INSERT INTO `auth_role_user` VALUES ('119', 'CUXM9D8', 'RO3JSPB', 'USKYPR3', '1', '1474166722000');
INSERT INTO `auth_role_user` VALUES ('120', 'CUXM9D8', 'RO3JSPB', 'US4CCYZ', '1', '1474167012000');
INSERT INTO `auth_role_user` VALUES ('121', 'CUXM9D8', 'RO3JSPB', 'USE2QTM', '1', '1474167181000');
INSERT INTO `auth_role_user` VALUES ('122', 'CUXM9D8', 'RO3JSPB', 'US264HS', '1', '1474167252000');
INSERT INTO `auth_role_user` VALUES ('123', 'CUXM9D8', 'RO3JSPB', 'USGXSGY', '1', '1474167364000');
INSERT INTO `auth_role_user` VALUES ('124', 'CUXM9D8', 'RO3JSPB', 'USBBQW7', '1', '1474167455000');
INSERT INTO `auth_role_user` VALUES ('125', 'CUXM9D8', 'RO3JSPB', 'USRRM6G', '1', '1474167749000');
INSERT INTO `auth_role_user` VALUES ('126', 'CUXM9D8', 'RO3JSPB', 'USUYMAG', '1', '1474167846000');
INSERT INTO `auth_role_user` VALUES ('127', 'CUXM9D8', 'RO3JSPB', 'USQE3HV', '1', '1474167904000');
INSERT INTO `auth_role_user` VALUES ('128', 'CUXM9D8', 'RO3JSPB', 'US6MW21', '1', '1474167952000');
INSERT INTO `auth_role_user` VALUES ('129', 'CUXM9D8', 'RO3JSPB', 'US2H5M7', '1', '1474168062000');
INSERT INTO `auth_role_user` VALUES ('130', 'CUXM9D8', 'RO3JSPB', 'USGPG81', '1', '1474168358000');
INSERT INTO `auth_role_user` VALUES ('131', 'CUXM9D8', 'RO3JSPB', 'USJ2V7E', '1', '1474168461000');
INSERT INTO `auth_role_user` VALUES ('132', 'CUXM9D8', 'RO3JSPB', 'US571M9', '1', '1474168521000');
INSERT INTO `auth_role_user` VALUES ('133', 'CUXM9D8', 'RO3JSPB', 'USK2H4Y', '1', '1474168644000');
INSERT INTO `auth_role_user` VALUES ('134', 'CUXM9D8', 'RO3JSPB', 'USPACK6', '1', '1474168691000');
INSERT INTO `auth_role_user` VALUES ('135', 'CUXM9D8', 'ROQQCJB', 'USFVAHF', '1', '1474168781000');
INSERT INTO `auth_role_user` VALUES ('136', 'CUXM9D8', 'RO3JSPB', 'USBY8NP', '1', '1474168840000');
INSERT INTO `auth_role_user` VALUES ('137', 'CUXM9D8', 'RO3JSPB', 'USYKBCQ', '1', '1474168917000');
INSERT INTO `auth_role_user` VALUES ('138', 'CUXM9D8', 'RO3JSPB', 'USBUA5K', '1', '1474169017000');
INSERT INTO `auth_role_user` VALUES ('139', 'CUXM9D8', 'RO3JSPB', 'US272BE', '1', '1474169124000');
INSERT INTO `auth_role_user` VALUES ('140', 'CUXM9D8', 'RO3JSPB', 'US62YR9', '1', '1474169395000');
INSERT INTO `auth_role_user` VALUES ('141', 'CUXM9D8', 'RO3JSPB', 'USSV3GF', '1', '1474169479000');
INSERT INTO `auth_role_user` VALUES ('142', 'CUXM9D8', 'RO3JSPB', 'USU5M6A', '1', '1474169557000');
INSERT INTO `auth_role_user` VALUES ('143', 'CU66D23', 'ROL3UV5', 'USJH1WR', '1', '1474270140000');
INSERT INTO `auth_role_user` VALUES ('144', 'CU66D23', 'ROL3UV5', 'USP9IW2', '1', '1474340276000');
INSERT INTO `auth_role_user` VALUES ('145', 'CU66D23', 'ROGNAQF', 'USP9IW2', '1', '1474340277000');
INSERT INTO `auth_role_user` VALUES ('146', 'CU66D23', 'RO2VFC2', 'USP9IW2', '1', '1474340278000');
INSERT INTO `auth_role_user` VALUES ('147', 'CU66D23', 'ROL3UV5', 'USYZZJ4', '1', '1474340781000');
INSERT INTO `auth_role_user` VALUES ('148', 'CU66D23', 'ROGNAQF', 'USYZZJ4', '1', '1474340781000');
INSERT INTO `auth_role_user` VALUES ('149', 'CU66D23', 'RO2VFC2', 'USYZZJ4', '1', '1474340781000');
INSERT INTO `auth_role_user` VALUES ('150', 'CUXM9D8', 'undefined', 'USVKAGX', '1', '1474453672000');
INSERT INTO `auth_role_user` VALUES ('151', 'CUXM9D8', 'undefined', 'UST6YXA', '1', '1474453861000');
INSERT INTO `auth_role_user` VALUES ('152', 'CUXM9D8', 'undefined', 'US2TK5N', '1', '1474454984000');
INSERT INTO `auth_role_user` VALUES ('153', 'CUXM9D8', 'undefined', 'US53KXM', '1', '1474455283000');
INSERT INTO `auth_role_user` VALUES ('154', 'CUXM9D8', 'undefined', 'USY3C9F', '1', '1474455336000');
INSERT INTO `auth_role_user` VALUES ('155', 'CUXM9D8', 'undefined', 'USFF2EV', '1', '1474509795000');
INSERT INTO `auth_role_user` VALUES ('156', 'CUXM9D8', 'undefined', 'USJIZ8R', '1', '1474509932000');
INSERT INTO `auth_role_user` VALUES ('157', 'CUXM9D8', 'undefined', 'US6J5GY', '1', '1474513657000');
INSERT INTO `auth_role_user` VALUES ('158', 'CUXM9D8', 'undefined', 'USS4CAA', '1', '1474514219000');
INSERT INTO `auth_role_user` VALUES ('159', 'CUXM9D8', 'undefined', 'USCYC72', '1', '1474514326000');
INSERT INTO `auth_role_user` VALUES ('160', 'CUXM9D8', 'RO3JSPB', 'US87WGF', '1', '1474514615000');
INSERT INTO `auth_role_user` VALUES ('161', 'CUXM9D8', 'ROQQCJB', 'US87WGF', '1', '1474514615000');
INSERT INTO `auth_role_user` VALUES ('162', 'CUXM9D8', 'RODQCCR', 'US87WGF', '1', '1474514615000');
INSERT INTO `auth_role_user` VALUES ('163', 'CUXM9D8', 'RO3JSPB', 'USV55XP', '1', '1474514698000');
INSERT INTO `auth_role_user` VALUES ('164', 'CUXM9D8', 'ROQQCJB', 'USV55XP', '1', '1474514698000');
INSERT INTO `auth_role_user` VALUES ('165', 'CUXM9D8', 'RO3JSPB', 'USS9DJV', '1', '1474514775000');
INSERT INTO `auth_role_user` VALUES ('166', 'CUXM9D8', 'ROQQCJB', 'USS9DJV', '1', '1474514775000');
INSERT INTO `auth_role_user` VALUES ('167', 'CUXM9D8', 'RODQCCR', 'USS9DJV', '1', '1474514775000');
INSERT INTO `auth_role_user` VALUES ('168', 'CUXM9D8', 'RO3JSPB', 'USAHLAR', '1', '1474514976000');
INSERT INTO `auth_role_user` VALUES ('169', 'CUXM9D8', 'ROQQCJB', 'USAHLAR', '1', '1474514976000');
INSERT INTO `auth_role_user` VALUES ('170', 'CUXM9D8', 'RODQCCR', 'USAHLAR', '1', '1474514976000');
INSERT INTO `auth_role_user` VALUES ('171', 'CUXM9D8', 'RO3JSPB', 'US28BSX', '1', '1474515052000');
INSERT INTO `auth_role_user` VALUES ('172', 'CUXM9D8', 'ROQQCJB', 'US28BSX', '1', '1474515052000');
INSERT INTO `auth_role_user` VALUES ('173', 'CUXM9D8', 'RODQCCR', 'US28BSX', '1', '1474515052000');
INSERT INTO `auth_role_user` VALUES ('174', 'CUXM9D8', 'RO3JSPB', 'USUHRIK', '1', '1474515145000');
INSERT INTO `auth_role_user` VALUES ('175', 'CUXM9D8', 'RODQCCR', 'USUHRIK', '1', '1474515145000');
INSERT INTO `auth_role_user` VALUES ('176', 'CUXM9D8', 'ROQQCJB', 'USUHRIK', '1', '1474515145000');
INSERT INTO `auth_role_user` VALUES ('177', 'CUXM9D8', 'RO3JSPB', 'US5HNIJ', '1', '1474515367000');
INSERT INTO `auth_role_user` VALUES ('178', 'CUXM9D8', 'RODQCCR', 'US5HNIJ', '1', '1474515367000');
INSERT INTO `auth_role_user` VALUES ('179', 'CUXM9D8', 'ROQQCJB', 'US5HNIJ', '1', '1474515367000');
INSERT INTO `auth_role_user` VALUES ('180', 'CUXM9D8', 'RODQCCR', 'USE7GW5', '1', '1474516342000');
INSERT INTO `auth_role_user` VALUES ('181', 'CUXM9D8', 'RO3JSPB', 'USE7GW5', '1', '1474516342000');
INSERT INTO `auth_role_user` VALUES ('182', 'CUXM9D8', 'ROQQCJB', 'USE7GW5', '1', '1474516342000');
INSERT INTO `auth_role_user` VALUES ('183', 'CUXM9D8', 'RO3JSPB', 'US9GIME', '1', '1474698113000');
INSERT INTO `auth_role_user` VALUES ('184', 'CUXM9D8', 'ROQQCJB', 'US7WHMG', '1', '1474698211000');
INSERT INTO `auth_role_user` VALUES ('185', 'CUXM9D8', 'RO3JSPB', 'UST51N3', '9', '1474698449000');
INSERT INTO `auth_role_user` VALUES ('186', 'CUXM9D8', 'RO3JSPB', 'USEMN4B', '1', '1474698582000');
INSERT INTO `auth_role_user` VALUES ('187', 'CUAVGM2', 'ROP1Y3L', 'USAXWTZ', '1', '1474698769000');
INSERT INTO `auth_role_user` VALUES ('188', 'CUWHNJU', 'undefined', 'US2L466', '1', '1474805585000');
INSERT INTO `auth_role_user` VALUES ('189', 'CUWHNJU', 'undefined', 'USZ71XX', '1', '1474805883000');
INSERT INTO `auth_role_user` VALUES ('190', 'CUWHNJU', 'undefined', 'US618KA', '1', '1474806195000');
INSERT INTO `auth_role_user` VALUES ('191', 'CUWHNJU', 'RO1NT9U', 'USHLXBU', '1', '1474806346000');
INSERT INTO `auth_role_user` VALUES ('192', 'CUWHNJU', 'RO1NT9U', 'USIW6HK', '1', '1474806940000');
INSERT INTO `auth_role_user` VALUES ('193', 'CUWHNJU', 'RO1NT9U', 'USK1UKD', '1', '1474874616000');
INSERT INTO `auth_role_user` VALUES ('194', 'CUWHNJU', 'ROECJ39', 'USK1UKD', '1', '1474874617000');
INSERT INTO `auth_role_user` VALUES ('195', 'CUWHNJU', 'ROV1V1P', 'USK1UKD', '1', '1474874617000');
INSERT INTO `auth_role_user` VALUES ('196', 'CUWHNJU', 'ROK981P', 'USK1UKD', '1', '1474874618000');
INSERT INTO `auth_role_user` VALUES ('197', 'CUWHNJU', 'ROTAIJ7', 'USK1UKD', '1', '1474874619000');
INSERT INTO `auth_role_user` VALUES ('198', 'CUWHNJU', 'ROTAIJ7', 'USQQD7D', '1', '1474875620000');
INSERT INTO `auth_role_user` VALUES ('199', 'CUWHNJU', 'ROK981P', 'USQQD7D', '1', '1474875620000');
INSERT INTO `auth_role_user` VALUES ('200', 'CUWHNJU', 'ROV1V1P', 'USQQD7D', '1', '1474875620000');
INSERT INTO `auth_role_user` VALUES ('201', 'CUWHNJU', 'ROECJ39', 'USQQD7D', '1', '1474875621000');
INSERT INTO `auth_role_user` VALUES ('202', 'CUWHNJU', 'RO1NT9U', 'USQQD7D', '1', '1474875621000');
INSERT INTO `auth_role_user` VALUES ('203', 'CUWHNJU', 'RO1NT9U', 'US6X5YT', '1', '1474875739000');
INSERT INTO `auth_role_user` VALUES ('204', 'CUWHNJU', 'ROECJ39', 'US6X5YT', '1', '1474875739000');
INSERT INTO `auth_role_user` VALUES ('205', 'CUWHNJU', 'ROV1V1P', 'US6X5YT', '1', '1474875739000');
INSERT INTO `auth_role_user` VALUES ('206', 'CUWHNJU', 'ROK981P', 'US6X5YT', '1', '1474875739000');
INSERT INTO `auth_role_user` VALUES ('207', 'CUWHNJU', 'ROTAIJ7', 'US6X5YT', '1', '1474875739000');
INSERT INTO `auth_role_user` VALUES ('208', 'CUWHNJU', 'RO1NT9U', 'USPH3LE', '1', '1474876161000');
INSERT INTO `auth_role_user` VALUES ('209', 'CUWHNJU', 'ROECJ39', 'USPH3LE', '1', '1474876161000');
INSERT INTO `auth_role_user` VALUES ('210', 'CUWHNJU', 'ROV1V1P', 'USPH3LE', '1', '1474876161000');
INSERT INTO `auth_role_user` VALUES ('211', 'CUWHNJU', 'RO1NT9U', 'USVXZ2W', '9', '1474876991000');
INSERT INTO `auth_role_user` VALUES ('212', 'CUWHNJU', 'ROECJ39', 'USVXZ2W', '9', '1474876991000');
INSERT INTO `auth_role_user` VALUES ('213', 'CUWHNJU', 'ROV1V1P', 'USVXZ2W', '9', '1474876991000');
INSERT INTO `auth_role_user` VALUES ('214', 'CUWHNJU', 'RO1NT9U', 'USVXZ2W', '9', '1474878024000');
INSERT INTO `auth_role_user` VALUES ('215', 'CUWHNJU', 'ROECJ39', 'USVXZ2W', '9', '1474878024000');
INSERT INTO `auth_role_user` VALUES ('216', 'CUWHNJU', 'ROV1V1P', 'USVXZ2W', '9', '1474878024000');
INSERT INTO `auth_role_user` VALUES ('217', 'CUWHNJU', 'ROK981P', 'USVXZ2W', '9', '1474878024000');
INSERT INTO `auth_role_user` VALUES ('218', 'CUWHNJU', 'ROTAIJ7', 'USVXZ2W', '9', '1474878024000');
INSERT INTO `auth_role_user` VALUES ('219', 'CUWHNJU', 'RO1NT9U', 'USVXZ2W', '1', '1474878026000');
INSERT INTO `auth_role_user` VALUES ('220', 'CUWHNJU', 'ROECJ39', 'USVXZ2W', '1', '1474878026000');
INSERT INTO `auth_role_user` VALUES ('221', 'CUWHNJU', 'ROV1V1P', 'USVXZ2W', '1', '1474878026000');
INSERT INTO `auth_role_user` VALUES ('222', 'CUWHNJU', 'ROV3GXA', 'USVXZ2W', '1', '1474878026000');
INSERT INTO `auth_role_user` VALUES ('223', 'CUWHNJU', 'ROK981P', 'USVXZ2W', '1', '1474878026000');
INSERT INTO `auth_role_user` VALUES ('224', 'CUXM9D8', 'ROECJ39', 'USWUWKW', '9', '1474878259000');
INSERT INTO `auth_role_user` VALUES ('225', 'CUXM9D8', 'ROV1V1P', 'USWUWKW', '9', '1474878259000');
INSERT INTO `auth_role_user` VALUES ('226', 'CUXM9D8', 'ROK981P', 'USWUWKW', '9', '1474878259000');
INSERT INTO `auth_role_user` VALUES ('227', 'CUXM9D8', 'ROTAIJ7', 'USWUWKW', '9', '1474878259000');
INSERT INTO `auth_role_user` VALUES ('228', 'CUXM9D8', 'RO3JSPB', 'USWUWKW', '9', '1474878259000');
INSERT INTO `auth_role_user` VALUES ('229', 'CUXM9D8', 'ROECJ39', 'USWUWKW', '1', '1474878471000');
INSERT INTO `auth_role_user` VALUES ('230', 'CUXM9D8', 'ROV1V1P', 'USWUWKW', '1', '1474878471000');
INSERT INTO `auth_role_user` VALUES ('231', 'CUWHNJU', 'ROV3GXA', 'USILSB7', '9', '1474882586000');
INSERT INTO `auth_role_user` VALUES ('232', 'CUWHNJU', 'RO1NT9U', 'USILSB7', '9', '1474882586000');
INSERT INTO `auth_role_user` VALUES ('233', 'CUWHNJU', 'ROV3GXA', 'USILSB7', '9', '1474882785000');
INSERT INTO `auth_role_user` VALUES ('234', 'CUWHNJU', 'RO1NT9U', 'USILSB7', '9', '1474882785000');
INSERT INTO `auth_role_user` VALUES ('235', 'CUWHNJU', 'ROECJ39', 'USILSB7', '9', '1474882786000');
INSERT INTO `auth_role_user` VALUES ('236', 'CUWHNJU', 'ROV1V1P', 'USILSB7', '9', '1474882786000');
INSERT INTO `auth_role_user` VALUES ('237', 'CUWHNJU', 'ROK981P', 'USILSB7', '9', '1474882786000');
INSERT INTO `auth_role_user` VALUES ('238', 'CUWHNJU', 'ROV3GXA', 'USILSB7', '1', '1474882801000');
INSERT INTO `auth_role_user` VALUES ('239', 'CUWHNJU', 'RO1NT9U', 'USILSB7', '1', '1474882801000');
INSERT INTO `auth_role_user` VALUES ('240', 'CUWHNJU', 'ROECJ39', 'USILSB7', '1', '1474882801000');
INSERT INTO `auth_role_user` VALUES ('241', 'CUXM9D8', 'cloudinnov-role', 'US7TE54', '1', '1474886444000');
INSERT INTO `auth_role_user` VALUES ('242', 'CUXM9D8', 'ROEUFPE', 'US7TE54', '1', '1474886444000');
INSERT INTO `auth_role_user` VALUES ('243', 'CUXM9D8', 'ROV46CM', 'US7TE54', '1', '1474886444000');
INSERT INTO `auth_role_user` VALUES ('244', 'CUXM9D8', 'ROQQCJB', 'USDH71Y', '9', '1474954751000');
INSERT INTO `auth_role_user` VALUES ('245', 'CUXM9D8', 'RO3JSPB', 'USDH71Y', '9', '1474961323000');
INSERT INTO `auth_role_user` VALUES ('246', 'CUXM9D8', 'RO2T76U', 'USDH71Y', '9', '1474962298000');
INSERT INTO `auth_role_user` VALUES ('247', 'CUWHNJU', 'ROV3GXA', 'US88HZE', '1', '1475910451000');
INSERT INTO `auth_role_user` VALUES ('248', 'CUWHNJU', 'ROV3GXA', 'USLIF34', '1', '1475910569000');
INSERT INTO `auth_role_user` VALUES ('249', 'CUAVGM2', 'ROP1Y3L', 'USHSQDZ', '1', '1475995488000');
INSERT INTO `auth_role_user` VALUES ('250', 'CUTMYL7', 'ROTAIJ7', 'USBWXA9', '1', '1476242030000');
INSERT INTO `auth_role_user` VALUES ('251', 'CUTMYL7', 'ROTAIJ7', 'USGVRDT', '1', '1476242197000');
INSERT INTO `auth_role_user` VALUES ('252', 'CUWHNJU', 'ROK981P', 'US6Y6IG', '1', '1476260741000');
INSERT INTO `auth_role_user` VALUES ('253', 'CUWHNJU', 'ROV1V1P', 'US6Y6IG', '1', '1476260741000');
INSERT INTO `auth_role_user` VALUES ('254', 'CUWHNJU', 'ROECJ39', 'USDUW1W', '1', '1476261230000');
INSERT INTO `auth_role_user` VALUES ('255', 'CUWHNJU', 'ROV1V1P', 'USDUW1W', '1', '1476261230000');
INSERT INTO `auth_role_user` VALUES ('256', 'CUY4GJK', 'ROAQQFP', 'US3XYSX', '1', '1476340917000');
INSERT INTO `auth_role_user` VALUES ('257', 'CUXM9D8', 'ROKWXED', 'US62K32', '1', '1476346061000');
INSERT INTO `auth_role_user` VALUES ('258', 'CUTT1L4', 'ROUGGCS', 'USQE5ZG', '1', '1476346582000');
INSERT INTO `auth_role_user` VALUES ('259', 'CUXM9D8', 'ROKWXED', 'USDH71Y', '9', '1476674973000');
INSERT INTO `auth_role_user` VALUES ('260', 'CUXM9D8', 'RO2T76U', 'USDH71Y', '9', '1476675067000');
INSERT INTO `auth_role_user` VALUES ('261', 'CUXM9D8', 'ROKWXED', 'USDH71Y', '9', '1476675211000');
INSERT INTO `auth_role_user` VALUES ('262', 'CUV4SVC', 'RORV562', 'USF5X74', '1', '1476785265000');
INSERT INTO `auth_role_user` VALUES ('263', 'CUZQBD6', 'RO86134', 'USSQT6A', '9', '1476785652000');
INSERT INTO `auth_role_user` VALUES ('264', 'CUZQBD6', 'RO86134', 'USSDSH8', '9', '1476790210000');
INSERT INTO `auth_role_user` VALUES ('265', 'CUZQBD6', 'RO86134', 'USSQT6A', '9', '1476790361000');
INSERT INTO `auth_role_user` VALUES ('266', 'CUXM9D8', 'RODQCCR', 'USHP2E2', '9', '1477476569000');
INSERT INTO `auth_role_user` VALUES ('267', 'CUXM9D8', 'ROKWXED', 'USXJLW6', '9', '1477479397000');
INSERT INTO `auth_role_user` VALUES ('268', 'CUXM9D8', 'RO2T76U', 'USXJLW6', '9', '1477479397000');
INSERT INTO `auth_role_user` VALUES ('269', 'CUXM9D8', 'ROKWXED', 'USXJLW6', '1', '1477479412000');
INSERT INTO `auth_role_user` VALUES ('270', 'CUXM9D8', 'RO2T76U', 'USXJLW6', '1', '1477479412000');
INSERT INTO `auth_role_user` VALUES ('271', 'CUXM9D8', 'RODQCCR', 'USXJLW6', '1', '1477479412000');
INSERT INTO `auth_role_user` VALUES ('272', 'CUXM9D8', 'ROQQCJB', 'USXJLW6', '1', '1477479412000');
INSERT INTO `auth_role_user` VALUES ('273', 'CUXM9D8', 'RO3JSPB', 'USZ1IXG', '1', '1477536897000');
INSERT INTO `auth_role_user` VALUES ('274', 'CUXM9D8', 'ROQQCJB', 'USZ1IXG', '1', '1477536897000');
INSERT INTO `auth_role_user` VALUES ('275', 'CUXM9D8', 'undefined', 'US5AUGH', '1', '1477536943000');
INSERT INTO `auth_role_user` VALUES ('276', 'CUXM9D8', 'RO2T76U', 'USX9V8W', '1', '1477546779000');
INSERT INTO `auth_role_user` VALUES ('277', 'CUXM9D8', 'ROKWXED', 'USX9V8W', '1', '1477546779000');
INSERT INTO `auth_role_user` VALUES ('278', 'CUXM9D8', 'undefined', 'USQSRV5', '1', '1477547997000');
INSERT INTO `auth_role_user` VALUES ('279', 'CUXM9D8', 'undefined', 'USR5ZL3', '1', '1477548054000');
INSERT INTO `auth_role_user` VALUES ('280', 'CUXM9D8', 'RODQCCR', 'USHP2E2', '9', '1477548422000');
INSERT INTO `auth_role_user` VALUES ('281', 'CUXM9D8', 'RO2T76U', 'USHP2E2', '9', '1477548422000');
INSERT INTO `auth_role_user` VALUES ('282', 'CUXM9D8', 'RO3JSPB', 'UST51N3', '9', '1477555484000');
INSERT INTO `auth_role_user` VALUES ('283', 'CUXM9D8', 'RO3JSPB', 'UST51N3', '1', '1477555500000');
INSERT INTO `auth_role_user` VALUES ('284', 'CUXM9D8', 'RO2T76U', 'USHP2E2', '9', '1477565508000');
INSERT INTO `auth_role_user` VALUES ('285', 'CUXM9D8', 'RODQCCR', 'USHP2E2', '9', '1477565508000');
INSERT INTO `auth_role_user` VALUES ('286', 'CUXM9D8', 'RO2T76U', 'USHP2E2', '1', '1477621322000');
INSERT INTO `auth_role_user` VALUES ('287', 'CUXM9D8', 'RODQCCR', 'USHP2E2', '1', '1477621322000');
INSERT INTO `auth_role_user` VALUES ('288', 'CUUDJBA', 'ROECJ39', 'USNMT54', '1', '1477625452000');
INSERT INTO `auth_role_user` VALUES ('289', 'CUUDJBA', 'ROV1V1P', 'USNMT54', '1', '1477625452000');
INSERT INTO `auth_role_user` VALUES ('290', 'CUZQBD6', 'RO86134', 'USSQT6A', '1', '1477627653000');
INSERT INTO `auth_role_user` VALUES ('291', 'CUZQBD6', 'ROECJ39', 'USSQT6A', '1', '1477627653000');
INSERT INTO `auth_role_user` VALUES ('292', 'CUZQBD6', 'RO86134', 'USSDSH8', '1', '1477627659000');
INSERT INTO `auth_role_user` VALUES ('293', 'CUZQBD6', 'ROECJ39', 'USSDSH8', '1', '1477627659000');
INSERT INTO `auth_role_user` VALUES ('294', 'CUV4SVC', 'RORV562', 'US4DBB6', '1', '1477652185000');
INSERT INTO `auth_role_user` VALUES ('295', 'CUK47CG', 'ROHFH3Z', 'US6UQKV', '1', '1477707089000');
INSERT INTO `auth_role_user` VALUES ('296', 'CUK47CG', 'ROECJ39', 'US6UQKV', '1', '1477707089000');
INSERT INTO `auth_role_user` VALUES ('297', 'CUXM9D8', 'ROMEC3Q', 'USMWWJK', '1', '1477707895000');
INSERT INTO `auth_role_user` VALUES ('298', 'CU6MH81', 'ROII8J4', 'USR9QKP', '9', '1477880255000');
INSERT INTO `auth_role_user` VALUES ('299', 'CU6MH81', 'ROII8J4', 'USR9QKP', '1', '1477880265000');
INSERT INTO `auth_role_user` VALUES ('300', 'CUXM9D8', 'ROMEC3Q', 'US7HFKY', '1', '1477885469000');
INSERT INTO `auth_role_user` VALUES ('301', 'CU7BN8A', 'ROG1NPD', 'US5CDGD', '1', '1477888282000');
INSERT INTO `auth_role_user` VALUES ('302', 'CUXM9D8', 'ROMEC3Q', 'USNUAIY', '1', '1477892821000');
INSERT INTO `auth_role_user` VALUES ('303', 'CU7BN8A', 'ROG1NPD', 'USXD766', '1', '1477892853000');
INSERT INTO `auth_role_user` VALUES ('304', 'CUXM9D8', 'ROMEC3Q', 'USWZ9HJ', '1', '1477893857000');
INSERT INTO `auth_role_user` VALUES ('305', 'CURGNML', 'ROZJ69Y', 'USGUFVU', '1', '1477894092000');
INSERT INTO `auth_role_user` VALUES ('306', 'CUXM9D8', 'ROMEC3Q', 'US4PUW6', '1', '1477898850000');
INSERT INTO `auth_role_user` VALUES ('307', 'CUXM9D8', 'ROMEC3Q', 'USN13H7', '1', '1477899197000');
INSERT INTO `auth_role_user` VALUES ('308', 'CU7BN8A', 'ROG1NPD', 'USGVRFA', '1', '1477899227000');
INSERT INTO `auth_role_user` VALUES ('309', 'CUXM9D8', 'ROMEC3Q', 'USL3XWU', '1', '1477899618000');
INSERT INTO `auth_role_user` VALUES ('310', 'CUXM9D8', 'ROMEC3Q', 'USL87XZ', '1', '1477899753000');
INSERT INTO `auth_role_user` VALUES ('311', 'CUXM9D8', 'ROMEC3Q', 'USEWWG4', '1', '1477899803000');
INSERT INTO `auth_role_user` VALUES ('312', 'CUXM9D8', 'ROMEC3Q', 'USZG7KU', '1', '1477899912000');
INSERT INTO `auth_role_user` VALUES ('313', 'CUXM9D8', 'ROMEC3Q', 'US5IGWB', '1', '1477900047000');
INSERT INTO `auth_role_user` VALUES ('314', 'CU7BN8A', 'ROG1NPD', 'USBBTV1', '1', '1477900070000');
INSERT INTO `auth_role_user` VALUES ('315', 'CU6MH81', 'ROII8J4', 'USFYFGY', '1', '1478065291000');
INSERT INTO `auth_role_user` VALUES ('316', 'CUXM9D8', 'ROMEC3Q', 'USTERNT', '1', '1478254567000');
INSERT INTO `auth_role_user` VALUES ('317', 'CU7BN8A', 'ROG1NPD', 'USXHV54', '1', '1478254609000');
INSERT INTO `auth_role_user` VALUES ('318', 'CUXM9D8', 'ROMEC3Q', 'USAYF3H', '1', '1478485465000');
INSERT INTO `auth_role_user` VALUES ('319', 'CUXM9D8', 'ROMEC3Q', 'USJXK19', '1', '1478485783000');
INSERT INTO `auth_role_user` VALUES ('320', 'CUXM9D8', 'ROMEC3Q', 'USWK1Z7', '1', '1478485951000');
INSERT INTO `auth_role_user` VALUES ('321', 'CU7BN8A', 'ROG1NPD', 'US84UU4', '1', '1478485991000');
INSERT INTO `auth_role_user` VALUES ('322', 'CUXM9D8', 'ROMEC3Q', 'USV2B7G', '1', '1478486560000');
INSERT INTO `auth_role_user` VALUES ('323', 'CUXM9D8', 'ROMEC3Q', 'USE17X7', '1', '1478486929000');
INSERT INTO `auth_role_user` VALUES ('324', 'CUXM9D8', 'ROMEC3Q', 'US1VQPE', '1', '1478487017000');
INSERT INTO `auth_role_user` VALUES ('325', 'CUXM9D8', 'ROMEC3Q', 'US5BBWZ', '1', '1478487085000');
INSERT INTO `auth_role_user` VALUES ('326', 'CU7BN8A', 'ROG1NPD', 'USWVMBL', '1', '1478487263000');
INSERT INTO `auth_role_user` VALUES ('327', 'CUH7BW7', 'ROECJ39', 'USGF6M8', '9', '1478490783000');
INSERT INTO `auth_role_user` VALUES ('328', 'CUH7BW7', 'ROV1V1P', 'USDQYIH', '1', '1478490789000');
INSERT INTO `auth_role_user` VALUES ('329', 'CUH7BW7', 'ROJW9X9', 'USGF6M8', '1', '1478490838000');
INSERT INTO `auth_role_user` VALUES ('330', 'CUXM9D8', 'undefined', 'US4C82R', '1', '1478750452000');
INSERT INTO `auth_role_user` VALUES ('331', 'CUXM9D8', 'undefined', 'USKR9NN', '1', '1479104508000');
INSERT INTO `auth_role_user` VALUES ('332', 'CU24FU6', 'ROECJ39', 'USZ63ZM', '1', '1479104632000');
INSERT INTO `auth_role_user` VALUES ('333', 'CUXM9D8', 'undefined', 'USG8L31', '1', '1479104704000');
INSERT INTO `auth_role_user` VALUES ('338', 'CUXM9D8', 'RO2T76U', 'US8R1HM', '1', '1479106234000');
INSERT INTO `auth_role_user` VALUES ('339', 'CUXM9D8', 'RO3JSPB', 'US8R1HM', '1', '1479106234000');
INSERT INTO `auth_role_user` VALUES ('340', 'CUXM9D8', 'undefined', 'US3D512', '1', '1479107820000');
INSERT INTO `auth_role_user` VALUES ('341', 'CUXM9D8', 'undefined', 'USK23S9', '1', '1479107923000');
INSERT INTO `auth_role_user` VALUES ('342', 'CUXM9D8', 'ROMEC3Q', 'US6XN4W', '1', '1479108018000');
INSERT INTO `auth_role_user` VALUES ('343', 'CUXM9D8', 'ROKWXED', 'US6XN4W', '1', '1479108018000');
INSERT INTO `auth_role_user` VALUES ('344', 'CUXM9D8', 'RO3JSPB', 'USDE8AB', '1', '1479108248000');
INSERT INTO `auth_role_user` VALUES ('345', 'CUXM9D8', 'ROQQCJB', 'USDE8AB', '1', '1479108248000');
INSERT INTO `auth_role_user` VALUES ('346', 'CUXM9D8', 'RO3JSPB', 'USEESVU', '1', '1479109039000');
INSERT INTO `auth_role_user` VALUES ('347', 'CUXM9D8', 'ROQQCJB', 'USEESVU', '1', '1479109039000');
INSERT INTO `auth_role_user` VALUES ('348', 'CUXM9D8', 'undefined', 'USQCYGS', '1', '1479110098000');
INSERT INTO `auth_role_user` VALUES ('349', 'CUXM9D8', 'undefined', 'USHEAM1', '1', '1479110172000');
INSERT INTO `auth_role_user` VALUES ('350', 'CUXM9D8', 'RO2T76U', 'USH8KP2', '1', '1479110403000');
INSERT INTO `auth_role_user` VALUES ('351', 'CUXM9D8', 'ROQQCJB', 'USEZDPQ', '1', '1479110782000');
INSERT INTO `auth_role_user` VALUES ('352', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479111212000');
INSERT INTO `auth_role_user` VALUES ('353', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479111212000');
INSERT INTO `auth_role_user` VALUES ('354', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479111240000');
INSERT INTO `auth_role_user` VALUES ('355', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479111240000');
INSERT INTO `auth_role_user` VALUES ('356', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479111361000');
INSERT INTO `auth_role_user` VALUES ('357', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479111361000');
INSERT INTO `auth_role_user` VALUES ('358', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479112333000');
INSERT INTO `auth_role_user` VALUES ('359', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479112333000');
INSERT INTO `auth_role_user` VALUES ('360', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479112341000');
INSERT INTO `auth_role_user` VALUES ('361', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479112341000');
INSERT INTO `auth_role_user` VALUES ('362', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479112363000');
INSERT INTO `auth_role_user` VALUES ('363', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479112363000');
INSERT INTO `auth_role_user` VALUES ('364', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479112383000');
INSERT INTO `auth_role_user` VALUES ('365', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479112383000');
INSERT INTO `auth_role_user` VALUES ('366', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479113296000');
INSERT INTO `auth_role_user` VALUES ('367', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479113296000');
INSERT INTO `auth_role_user` VALUES ('368', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '9', '1479113337000');
INSERT INTO `auth_role_user` VALUES ('369', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '9', '1479113338000');
INSERT INTO `auth_role_user` VALUES ('370', 'CUXM9D8', 'ROKWXED', 'US2V6NL', '1', '1479113352000');
INSERT INTO `auth_role_user` VALUES ('371', 'CUXM9D8', 'RO2T76U', 'US2V6NL', '1', '1479113352000');
INSERT INTO `auth_role_user` VALUES ('372', 'CUNE616', 'ROECJ39', 'USDTG5Z', '1', '1479726201000');
INSERT INTO `auth_role_user` VALUES ('373', 'CUXM9D8', 'ROKWXED', 'USDH71Y', '9', '1481596517000');
INSERT INTO `auth_role_user` VALUES ('374', 'CUXM9D8', 'ROKWXED', 'USDH71Y', '9', '1481596560000');
INSERT INTO `auth_role_user` VALUES ('375', 'CUXM9D8', 'RO2T76U', 'USU3XBV', '9', '1481596676000');
INSERT INTO `auth_role_user` VALUES ('376', 'CUXM9D8', 'ROQQCJB', 'USX6GHJ', '9', '1481602956000');
INSERT INTO `auth_role_user` VALUES ('377', 'CUXM9D8', 'ROQQCJB', 'USX6GHJ', '1', '1481603144000');
INSERT INTO `auth_role_user` VALUES ('378', 'CUXM9D8', 'RO3JSPB', 'USYZAP4', '9', '1481603186000');
INSERT INTO `auth_role_user` VALUES ('379', 'CUXM9D8', 'RO3JSPB', 'USYZAP4', '9', '1481603205000');
INSERT INTO `auth_role_user` VALUES ('380', 'CUXM9D8', 'RO3JSPB', 'USYZAP4', '1', '1481603699000');
INSERT INTO `auth_role_user` VALUES ('381', 'CUXM9D8', 'ROV46CM', 'USILWG5', '1', '1481603744000');
INSERT INTO `auth_role_user` VALUES ('382', 'CUXM9D8', 'cloudinnov-role', 'US2UUA9', '1', '1481603778000');
INSERT INTO `auth_role_user` VALUES ('383', 'CUXM9D8', 'RO2T76U', 'USU3XBV', '9', '1481604514000');
INSERT INTO `auth_role_user` VALUES ('384', 'CUXM9D8', 'RO2T76U', 'USU3XBV', '1', '1481604846000');
INSERT INTO `auth_role_user` VALUES ('385', 'CUXM9D8', 'ROKWXED', 'USDH71Y', '1', '1481706434000');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL,
  `login_name` varchar(20) NOT NULL COMMENT '登录名',
  `login_password` varchar(50) NOT NULL COMMENT '登录密码',
  `phone` varchar(20) NOT NULL COMMENT '联系电话',
  `logo` varchar(256) DEFAULT NULL,
  `email` varchar(128) NOT NULL COMMENT '邮箱',
  `company_code` varchar(20) NOT NULL COMMENT '公司编号',
  `default_language` varchar(8) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  `user_type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('3', 'USDH71Y', 'cqew-gov', '34a2c87198567d0a9f50a279b04c0732', '15130275312', 'http://54.222.236.3:8003/upload/201612/e3d2852dc5b04105b0bf745fbbdef7a6.png', '123@11.com', 'CUXM9D8', 'cn', '1', '1461858395000', null, 'oem');
INSERT INTO `auth_user` VALUES ('4', 'USKR9NN', 'qwe', '46f94c8de14fb3668085768ff1b7f2a', '18630016586', 'http://54.222.236.3:8000/upload/201611/ff1a3f7b955047e2aac3a24951e446a9.png', '213@1.com', 'CUXM9D8', 'cn', '9', '1479104508000', null, 'oem');
INSERT INTO `auth_user` VALUES ('5', 'USZ63ZM', '111', '4fceeaf9963617ffde51bafd98a5a313', '18630016545', 'http://54.222.236.3:8000/upload/201611/89def2f63a7d453eb4568631440ab925.png', '1005266424@qq.com', 'CU24FU6', 'cn', '1', '1479104543000', null, 'user');
INSERT INTO `auth_user` VALUES ('6', 'USG8L31', 'qwe', '46f94c8de14fb3668085768ff1b7f2a', '18630015695', 'http://54.222.236.3:8000/upload/201611/71287b9b6bca45fd9382534a43ebbcb7.png', 'q@q.com', 'CUXM9D8', 'cn', '9', '1479104704000', null, 'oem');
INSERT INTO `auth_user` VALUES ('9', 'US8R1HM', 'qwe', '46f94c8de14fb3668085768ff1b7f2a', '15968535755', 'http://54.222.236.3:8000/upload/201611/93ce138ca6574b5fa472bff65b1fda1b.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479106233000', null, 'oem');
INSERT INTO `auth_user` VALUES ('10', 'US3D512', 'asd', 'bfd59291e825b5f2bbf1eb76569f8fe7', '18210057152', 'http://54.222.236.3:8000/upload/201611/8cf7c5660c284ddb99218c978fe1b3b6.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479107820000', null, 'oem');
INSERT INTO `auth_user` VALUES ('11', 'USK23S9', 'asd', 'bfd59291e825b5f2bbf1eb76569f8fe7', '18210057152', 'http://54.222.236.3:8000/upload/201611/600369f75f8341e3867770fefe5ab659.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479107923000', null, 'oem');
INSERT INTO `auth_user` VALUES ('12', 'US6XN4W', 'asd', 'bfd59291e825b5f2bbf1eb76569f8fe7', '18210057152', 'http://54.222.236.3:8000/upload/201611/fdb9946bd447490ba8597210f886b06a.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479108017000', null, 'oem');
INSERT INTO `auth_user` VALUES ('13', 'USDE8AB', 'asd', 'bfd59291e825b5f2bbf1eb76569f8fe7', '18210057152', 'http://54.222.236.3:8000/upload/201611/7ebb4894b68842b8a17db4e037d7f44e.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479108248000', null, 'oem');
INSERT INTO `auth_user` VALUES ('14', 'USEESVU', 'qwe', '20820e3227815ed1756a6b531e7e0d2', '18240059657', 'http://54.222.236.3:8000/upload/201611/a17b8ecba01c42649c59fe2472df0614.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479109039000', null, 'oem');
INSERT INTO `auth_user` VALUES ('15', 'USQCYGS', 'qwe', '20820e3227815ed1756a6b531e7e0d2', '18210057152', 'http://54.222.236.3:8000/upload/201611/f9756f07d5f94f5fb72a3667231ee64b.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479110098000', null, 'oem');
INSERT INTO `auth_user` VALUES ('16', 'USHEAM1', '123', 'bfd59291e825b5f2bbf1eb76569f8fe7', '18210057165', 'http://54.222.236.3:8000/upload/201611/21ac963d19634a26be066e6cfd99b30e.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479110172000', null, 'oem');
INSERT INTO `auth_user` VALUES ('17', 'USH8KP2', '123', '46f94c8de14fb3668085768ff1b7f2a', '15210255112', 'http://54.222.236.3:8000/upload/201611/78284d0357d24690984373cdb85cd274.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479110403000', null, 'oem');
INSERT INTO `auth_user` VALUES ('18', 'USEZDPQ', 'qwe', '20820e3227815ed1756a6b531e7e0d2', '15210255158', 'http://54.222.236.3:8000/upload/201611/66b418e053614d73bcafc9a8b0be1113.png', '123@123.com', 'CUXM9D8', 'cn', '9', '1479110782000', null, 'oem');
INSERT INTO `auth_user` VALUES ('19', 'US2V6NL', 'qwe', '20820e3227815ed1756a6b531e7e0d2', '15210255153', 'http://54.222.236.3:8000/upload/201611/33f1b8a025074990ab15c3131d968747.png', '123@123.com', 'CUXM9D8', 'cn', '1', '1479111212000', null, 'oem');
INSERT INTO `auth_user` VALUES ('20', 'USDTG5Z', 'q', '46f94c8de14fb3668085768ff1b7f2a', '18210057152', 'http://54.222.236.3:8002/upload/201611/f025f2ecf48646c988009498ad22b28a.png', '4@1.com', 'CUNE616', 'cn', '1', '1479726201000', null, 'user');
INSERT INTO `auth_user` VALUES ('21', 'USU3XBV', 'ceshi', '46f94c8de14fb3668085768ff1b7f2a', '15210255145', 'http://54.222.236.3:8003/upload/201612/4154dfec8d3a4abd990f5a56d452ccc5.png', '123@qq.com', 'CUXM9D8', 'cn', '1', '1481596676000', null, 'oem');
INSERT INTO `auth_user` VALUES ('22', 'USX6GHJ', '11111111', 'ed7a8514b2da6d1ce3ebe45325db2d', '15600000000', 'http://54.222.236.3:8000/upload/201612/51152aa91e4c407790dacca03ca80bbf.png', '15177778888@qq.com', 'CUXM9D8', 'cn', '9', '1481602956000', null, 'oem');
INSERT INTO `auth_user` VALUES ('23', 'USYZAP4', '67666', 'ed7a8514b2da6d1ce3ebe45325db2d', '15210255140', 'http://54.222.236.3:8003/upload/201612/75ed890c689248a49d41a382f1caf1c3.png', '123@qq.com', 'CUXM9D8', 'cn', '9', '1481603186000', null, 'oem');
INSERT INTO `auth_user` VALUES ('24', 'USILWG5', 'ceshi1', 'ed7a8514b2da6d1ce3ebe45325db2d', '18630000000', 'http://54.222.236.3:8003/upload/201612/9872f1aaab254abb9814ac63e84dfd8d.png', '1005266424@qq.com', 'CUXM9D8', 'cn', '9', '1481603744000', null, 'oem');
INSERT INTO `auth_user` VALUES ('25', 'US2UUA9', 'chengning', 'ed7a8514b2da6d1ce3ebe45325db2d', '18630000000', 'http://54.222.236.3:8003/upload/201612/9e8154bdb36343baa22c681363bf143b.png', '1005266424@qq.com', 'CUXM9D8', 'cn', '1', '1481603778000', null, 'oem');

-- ----------------------------
-- Table structure for auth_user_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_info`;
CREATE TABLE `auth_user_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `position` varchar(20) DEFAULT NULL COMMENT '职位',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_user_info
-- ----------------------------
INSERT INTO `auth_user_info` VALUES ('cn', 'US2UUA9', 'chengning', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'US2V6NL', 'qwe', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'US3D512', 'asd', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'US6XN4W', '123', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'US8R1HM', 'qwe', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USDE8AB', 'asd', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USDH71Y', '高速公路-管理员', '管理员', null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USDTG5Z', '123', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USEESVU', 'qwe', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USEZDPQ', 'qwe', '123', null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USG8L31', '123', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USH8KP2', '123', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USHEAM1', 'asd', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USILWG5', 'chengning', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USK23S9', 'asd', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USKR9NN', 'qwe', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USQCYGS', 'qwe', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USU3XBV', 'ces11', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USX6GHJ', '111', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USYZAP4', 'ffffff', null, null);
INSERT INTO `auth_user_info` VALUES ('cn', 'USZ63ZM', '程宁', null, null);

-- ----------------------------
-- Table structure for board_solution_config
-- ----------------------------
DROP TABLE IF EXISTS `board_solution_config`;
CREATE TABLE `board_solution_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `solution_code` varchar(20) NOT NULL COMMENT '编码',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `equipment_classify` varchar(20) DEFAULT NULL,
  `row_count` int(11) DEFAULT NULL COMMENT '行数',
  `color` varchar(20) DEFAULT NULL COMMENT '颜色',
  `display_type` int(11) DEFAULT NULL COMMENT '展现方式',
  `send_content` varchar(500) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COMMENT='情报板控制配置表';

-- ----------------------------
-- Records of board_solution_config
-- ----------------------------
INSERT INTO `board_solution_config` VALUES ('75', 'CSTFI7A3', 'EQRER2C', null, null, '#ff0000', '0', '[{\"duration\":222,\"sendContent\":\"1111\"},{\"duration\":444,\"sendContent\":\"3333\"}]', '1', '雨天路滑', '1481526701000', '1481526701000');
INSERT INTO `board_solution_config` VALUES ('76', 'CSTFI7A3', 'EQUJGI8', null, null, '#ff0000', '0', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '雨天路滑', '1481526701000', '1481526701000');
INSERT INTO `board_solution_config` VALUES ('77', 'CSTFI7A3', 'EQX7KTS', null, null, '#0000CD', '0', '[{\"duration\":2222,\"sendContent\":\"11111\"},{\"duration\":4444,\"sendContent\":\"3333\"}]', '1', '雨天路滑', '1481526701000', '1481526701000');
INSERT INTO `board_solution_config` VALUES ('85', 'CST4BG4N', 'EQ4U992', 'QSSXT', null, null, null, '1', '1', '11232', '1481534236000', '1481534236000');
INSERT INTO `board_solution_config` VALUES ('86', 'CSTVQNNL', 'EQ4U992', 'QSSXT', null, null, null, '0', '1', '11', '1481624136000', '1481624136000');
INSERT INTO `board_solution_config` VALUES ('87', 'CSTVQNNL', 'EQDQ4CC', 'QXSXT', null, null, null, '1', '1', '11', '1481624137000', '1481624137000');
INSERT INTO `board_solution_config` VALUES ('89', 'CSTJCU49', 'EQRER2C', null, null, '#ff0000', '3', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', 'undefined', '1481786401000', '1481786401000');
INSERT INTO `board_solution_config` VALUES ('90', 'CSTJCU49', 'EQUJGI8', null, null, '#ff0000', '0', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', 'undefined', '1481786401000', '1481786401000');
INSERT INTO `board_solution_config` VALUES ('91', 'CSTJCU49', 'EQX7KTS', null, null, '#ff0000', '3', '[{\"sendContent\":\"wwww\"}]', '1', 'undefined', '1481786401000', '1481786401000');
INSERT INTO `board_solution_config` VALUES ('92', 'CSTX4I7W', 'EQRER2C', null, null, '#FFFF00', '5', '[{\"duration\":2,\"sendContent\":\"21312\"}]', '1', '1231', '1481786500000', '1481786500000');
INSERT INTO `board_solution_config` VALUES ('93', 'CSTX4I7W', 'EQUJGI8', null, null, '#ff0000', '3', '[{\"duration\":1,\"sendContent\":\"1232\"}]', '1', '1231', '1481786500000', '1481786500000');
INSERT INTO `board_solution_config` VALUES ('94', 'CSTK9P2R', 'EQX7KTS', null, null, '#ff0000', '3', '[{\"duration\":2,\"sendContent\":\"wrerewrewrew\"}]', '1', '312321323212321', '1481786561000', '1481786561000');
INSERT INTO `board_solution_config` VALUES ('95', 'CSTK9P2R', 'EQQQ2SD', null, null, '#FFFF00', '5', '[{\"duration\":4,\"sendContent\":\"ewqewqewqewqewqeqwewq\"}]', '1', '312321323212321', '1481786562000', '1481786562000');
INSERT INTO `board_solution_config` VALUES ('96', 'CST4IHF4', 'EQRER2C', null, null, '#ff0000', '5', '[{\"duration\":3,\"sendContent\":\"雨天路滑\"},{\"duration\":2,\"sendContent\":\"下雨了\"}]', '1', '唉', '1481786707000', '1481786707000');
INSERT INTO `board_solution_config` VALUES ('97', 'CST4IHF4', 'EQWBK8A', null, null, '#ff0000', '5', '[{\"duration\":3,\"sendContent\":\"不下了\"},{\"sendContent\":\"下大了\"}]', '1', '唉', '1481786708000', '1481786708000');
INSERT INTO `board_solution_config` VALUES ('164', 'CSTZLLV1', 'EQ5Z91M', null, null, '#ff0000', '3', '[{\"duration\":3333,\"sendContent\":\"sdfasd\"}]', '1', '12321321', '1482123026000', '1482123026000');
INSERT INTO `board_solution_config` VALUES ('165', 'CSTZLLV1', 'EQRER2C', 'FXKBQBB', null, '#ff0000', '3', '[{\"duration\":2,\"sendContent\":\"ewrwrerew\"}]', '1', '12321321', '1482123026000', '1482123026000');
INSERT INTO `board_solution_config` VALUES ('166', 'CSTZLLV1', 'EQUJGI8', 'MJSKBQBB', null, '#FFFF00', '3', '[{\"duration\":3,\"sendContent\":\"vxcvzxcvxzcvxzcvzxcv\"}]', '1', '12321321', '1482123026000', '1482123026000');
INSERT INTO `board_solution_config` VALUES ('188', 'CST1EUFS', 'EQ4U992', 'QSSXT', null, null, null, '0', '1', '11', '1482139910000', '1482139910000');
INSERT INTO `board_solution_config` VALUES ('189', 'CST1EUFS', 'EQDQ4CC', 'QXSXT', null, null, null, '1', '1', '11', '1482139910000', '1482139910000');
INSERT INTO `board_solution_config` VALUES ('190', 'CST1EUFS', 'EQHE76P', null, null, null, null, '0', '1', '11', '1482139910000', '1482139910000');
INSERT INTO `board_solution_config` VALUES ('192', 'CSTZ3CQ2', 'EQQQ2SD', 'FXKBQBB', null, '#ff0000', '5', '[{\"duration\":232,\"sendContent\":\"ewewqewq\"}]', '1', 'wqewqewqewqeqw', '1482228531000', '1482228531000');
INSERT INTO `board_solution_config` VALUES ('193', 'CSTZ3CQ2', 'EQRER2C', 'FXKBQBB', null, '#ff0000', '5', '[{\"duration\":2,\"sendContent\":\"ewqewqewqe\"}]', '1', 'wqewqewqewqeqw', '1482228531000', '1482228531000');
INSERT INTO `board_solution_config` VALUES ('194', 'CSTJU5PI', 'EQUJGI8', 'MJSKBQBB', null, '#ff0000', '4', '[{\"duration\":11,\"sendContent\":\"sdfsdf\"}]', '1', '1111', '1482304501000', '1482304501000');
INSERT INTO `board_solution_config` VALUES ('199', 'CST245PG', 'EQ5Z91M', 'FXKBQBB', null, '#ff0000', '5', '[{\"duration\":-32,\"sendContent\":\"111\"}]', '1', '1111', '1482306150000', '1482306150000');
INSERT INTO `board_solution_config` VALUES ('200', 'CST245PG', 'EQUJGI8', 'MJSKBQBB', null, '#ff0000', '5', '[{\"duration\":111,\"sendContent\":\"撒大声地\"}]', '1', '1111', '1482306150000', '1482306150000');
INSERT INTO `board_solution_config` VALUES ('203', 'CSTU52LW', 'EQDQ4CC', 'QXSXT', null, null, null, '0', '1', 'qq群', '1482307957000', '1482307957000');
INSERT INTO `board_solution_config` VALUES ('204', 'CSTU52LW', 'EQHE76P', 'WBCLJCQ', null, null, null, '1', '1', 'qq群', '1482307957000', '1482307957000');
INSERT INTO `board_solution_config` VALUES ('205', 'CST9KS8N', 'EQDQ4CC', 'QXSXT', null, null, null, '1', '1', '其味无穷无群无群二群无', '1482310168000', '1482310168000');
INSERT INTO `board_solution_config` VALUES ('206', 'CST9KS8N', 'EQQ735V', 'COJCQ', null, null, null, '0', '1', '其味无穷无群无群二群无', '1482310168000', '1482310168000');
INSERT INTO `board_solution_config` VALUES ('215', 'CSTIQBKN', 'EQDW9GR', 'FXKBQBB', null, '#ff0000', '3', '[{\"duration\":2222,\"sendContent\":\"11\"}]', '1', 'undefined', '1482329320000', '1482329320000');
INSERT INTO `board_solution_config` VALUES ('216', 'CSTIQBKN', 'EQUJGI8', 'MJSKBQBB', null, '#ff0000', '5', '[{\"duration\":3,\"sendContent\":\"测试\"}]', '1', 'undefined', '1482329320000', '1482329320000');
INSERT INTO `board_solution_config` VALUES ('217', 'CST7T6AF', 'EQ5Z91M', 'FXKBQBB', null, '#FFFF00', '3', '[{\"duration\":4,\"sendContent\":\"dasdsadsad\"}]', '1', 'dsfsdfsdfds', '1482377704000', '1482377704000');
INSERT INTO `board_solution_config` VALUES ('218', 'CST7T6AF', 'EQRER2C', 'FXKBQBB', null, '#FFFF00', '3', '[{\"duration\":4,\"sendContent\":\"qweqweqwqw\"}]', '1', 'dsfsdfsdfds', '1482377704000', '1482377704000');
INSERT INTO `board_solution_config` VALUES ('219', 'CST4FQAA', 'EQDW9GR', 'FXKBQBB', null, '#ff0000', '3', '[{\"duration\":33333,\"sendContent\":\"12312\"}]', '1', '1', '1482382009000', '1482382009000');
INSERT INTO `board_solution_config` VALUES ('220', 'CST4FQAA', 'EQUJGI8', 'MJSKBQBB', null, '#ff0000', '3', '[{\"duration\":3333,\"sendContent\":\"请问请问\"}]', '1', '1', '1482382009000', '1482382009000');
INSERT INTO `board_solution_config` VALUES ('221', 'CSTGIT36', 'EQCPY4G', null, null, null, null, '1', '1', '111', '1482386905000', '1482386905000');
INSERT INTO `board_solution_config` VALUES ('222', 'CSTGIT36', 'EQDQ4CC', null, null, null, null, '1', '1', '111', '1482386905000', '1482386905000');
INSERT INTO `board_solution_config` VALUES ('223', 'CSTGIT36', 'EQHE76P', null, null, null, null, '0', '1', '111', '1482386905000', '1482386905000');
INSERT INTO `board_solution_config` VALUES ('224', 'CSTNECI3', 'EQ4U992', 'QSSXT', null, null, null, '0', '1', '111', '1482395666000', '1482395666000');
INSERT INTO `board_solution_config` VALUES ('225', 'CSTNECI3', 'EQDQ4CC', 'QXSXT', null, null, null, '1', '1', '111', '1482395666000', '1482395666000');
INSERT INTO `board_solution_config` VALUES ('226', 'CST3E352', 'EQDQ4CC', 'QXSXT', null, null, null, '1', '1', '自行车自行车', '1482395784000', '1482395784000');
INSERT INTO `board_solution_config` VALUES ('227', 'CST11JE6', 'EQDW9GR', 'FXKBQBB', null, '#FFFF00', '4', '[{\"duration\":3,\"sendContent\":\"qwewqewq\"}]', '1', 'wqewqewq', '1482909633000', '1482909633000');
INSERT INTO `board_solution_config` VALUES ('228', 'CST11JE6', 'EQRER2C', 'FXKBQBB', null, '#ff0000', '5', '[{\"duration\":3,\"sendContent\":\"asdas\"}]', '1', 'wqewqewq', '1482909633000', '1482909633000');

-- ----------------------------
-- Table structure for bom
-- ----------------------------
DROP TABLE IF EXISTS `bom`;
CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL COMMENT '公司编码',
  `oem_code` varchar(100) NOT NULL COMMENT '设备制造商编码',
  `model` varchar(100) NOT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL COMMENT '分类(预留)',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  `ci_code` varchar(100) DEFAULT NULL COMMENT '云采编码',
  `ci_product_url` varchar(500) DEFAULT NULL COMMENT '云采商品链接',
  `ci_trans_status` int(11) DEFAULT NULL COMMENT '同步标识(0 已同步 1未同步)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of bom
-- ----------------------------

-- ----------------------------
-- Table structure for bom_info
-- ----------------------------
DROP TABLE IF EXISTS `bom_info`;
CREATE TABLE `bom_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of bom_info
-- ----------------------------

-- ----------------------------
-- Table structure for capacity_stat
-- ----------------------------
DROP TABLE IF EXISTS `capacity_stat`;
CREATE TABLE `capacity_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `production_line_code` varchar(20) NOT NULL COMMENT '产线编码',
  `material_code` varchar(20) NOT NULL COMMENT '原料编码',
  `product_code` varchar(20) DEFAULT NULL COMMENT '产物编码',
  `production_time` varchar(20) NOT NULL COMMENT '生产时间',
  `yield` decimal(10,2) NOT NULL COMMENT '产量',
  `other_cost` varchar(256) DEFAULT NULL COMMENT '其它成本',
  `material_cost` decimal(10,0) DEFAULT NULL COMMENT '原料成本',
  `sale_price` decimal(10,2) DEFAULT NULL COMMENT '销售单价',
  `profit` decimal(10,2) DEFAULT NULL COMMENT '利润',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of capacity_stat
-- ----------------------------

-- ----------------------------
-- Table structure for car_detector_data
-- ----------------------------
DROP TABLE IF EXISTS `car_detector_data`;
CREATE TABLE `car_detector_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(20) DEFAULT NULL,
  `deviceId` varchar(20) NOT NULL,
  `period` int(11) DEFAULT NULL,
  `lane_total` int(11) DEFAULT NULL,
  `car_total` int(11) DEFAULT NULL,
  `lane_data` varchar(500) DEFAULT NULL,
  `summary_time` varchar(20) DEFAULT NULL,
  `create_time` varchar(20) NOT NULL,
  `last_update_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录车检仪器上传的数据，进行汇总';

-- ----------------------------
-- Records of car_detector_data
-- ----------------------------

-- ----------------------------
-- Table structure for communication_device
-- ----------------------------
DROP TABLE IF EXISTS `communication_device`;
CREATE TABLE `communication_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '唯一编码',
  `device_number` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '通讯设备序列号',
  `customer_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '客户编码',
  `validity_period` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '有效期限',
  `active` int(11) DEFAULT NULL COMMENT '是否激活 1 激活 0 未激活',
  `send_fre` int(11) DEFAULT NULL,
  `connect_status` int(11) DEFAULT NULL COMMENT '连接状态',
  `status` int(11) DEFAULT NULL,
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of communication_device
-- ----------------------------

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `type` varchar(10) NOT NULL COMMENT '类型：oem-设备制造商，agent-渠道商，enduser-最终客户',
  `industry_code` varchar(20) DEFAULT NULL COMMENT '行业编码',
  `oem_code` varchar(20) DEFAULT NULL COMMENT '设备制造商编码',
  `agent_code` varchar(20) DEFAULT NULL COMMENT '代理商编码',
  `integrator_code` varchar(20) DEFAULT NULL COMMENT '集成商编码',
  `email` varchar(100) DEFAULT NULL,
  `service_telephone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `homepage` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL COMMENT '位置，在地图上的经纬度',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('2', 'CUXM9D8', 'oem', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8000/upload/201608/180d12a4cd644f008ed1d73d58208d9a.png', null, '1', '1463123729000', '1470022159000');
INSERT INTO `company` VALUES ('3', 'CU24FU6', 'customer', null, 'CUXM9D8', null, null, null, null, '7', '6', '4', 'http://54.222.236.3:8002/upload/201611/fc6ad69a5dd24a82a4eb4e6b2e5a2737.png', null, '9', '1478858049000', '1479722903000');
INSERT INTO `company` VALUES ('4', 'CUQA2EG', 'expy', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8002/upload/201611/dfec59bededf4d2f8580279405408937.png', null, '1', '1479700829000', null);
INSERT INTO `company` VALUES ('5', 'CUUZ4XR', 'expy', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8002/upload/201611/3a289675189b4f8daefabf26d22ec610.png', null, '1', '1479701266000', null);
INSERT INTO `company` VALUES ('6', 'CUY88QY', 'expy', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8000/upload/201611/f330f3fb7b3d4eec888c90ccf3a99cac.png', '-31.593280473412264,28.761487680634087', '1', '1479701815000', null);
INSERT INTO `company` VALUES ('7', 'CUNE616', 'customer', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8002/upload/201611/455ec08bb9744053a1b5998383ed6463.png', null, '9', '1479726080000', null);
INSERT INTO `company` VALUES ('8', 'CUWSBZE', 'customer', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8002/upload/201611/b05265afc9c4413f9c8bb344571a7cae.png', null, '9', '1480134233000', null);
INSERT INTO `company` VALUES ('9', 'CUSWZ8A', 'customer', null, 'CUXM9D8', null, null, null, null, null, null, null, 'http://54.222.236.3:8002/upload/201611/894beb2bb4ed479f8fdaa0964174adab.png', null, '9', '1480134360000', null);

-- ----------------------------
-- Table structure for company_info
-- ----------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `short_name` varchar(20) DEFAULT NULL COMMENT '简称',
  `contact` varchar(50) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT '所属地区',
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of company_info
-- ----------------------------
INSERT INTO `company_info` VALUES ('cn', 'CU24FU6', '1', '2', '5', null, null, null, '3', '8');
INSERT INTO `company_info` VALUES ('cn', 'CUNE616', '1', '2', null, null, null, null, null, null);
INSERT INTO `company_info` VALUES ('cn', 'CUQA2EG', '请问请问c', null, null, null, null, null, '请问请问', '请问请问');
INSERT INTO `company_info` VALUES ('cn', 'CUSWZ8A', '路段监控', null, null, null, null, null, null, null);
INSERT INTO `company_info` VALUES ('cn', 'CUUZ4XR', '测试路段', null, null, null, null, null, '来广营', '我跑你追，追到我，我就让你嘿嘿嘿。');
INSERT INTO `company_info` VALUES ('cn', 'CUWSBZE', '隧道监控', null, null, null, null, null, null, null);
INSERT INTO `company_info` VALUES ('cn', 'CUXM9D8', '高速公路科技有限公司', '高速公路', '某先生', '中国', null, null, null, null);
INSERT INTO `company_info` VALUES ('cn', 'CUY88QY', '123', null, null, null, null, null, 'qweqwe', 'dasdasdasd');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(50) DEFAULT NULL COMMENT '配置项名称',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `start_time` varchar(50) DEFAULT NULL COMMENT '启动时间',
  `create_time` varchar(50) DEFAULT NULL,
  `last_update_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'transcloud_dev', '2', '', '1468948023', '1468948023');
INSERT INTO `config` VALUES ('2', 'initdb', '2', null, '1468948023', '1468948023');
INSERT INTO `config` VALUES ('3', 'transcloud_demon', '2', null, '1468948023', '1468948023');
INSERT INTO `config` VALUES ('4', 'transcloud_prd', '2', null, '1468948023', '1468948023');

-- ----------------------------
-- Table structure for control_solution
-- ----------------------------
DROP TABLE IF EXISTS `control_solution`;
CREATE TABLE `control_solution` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `solution_type` int(11) DEFAULT NULL COMMENT '方案类型：1-情报板，2-其它设备',
  `level` int(11) NOT NULL COMMENT '等级：1-普通，2-一般，3-紧急，4-特级',
  `type` int(11) NOT NULL COMMENT '处理类型：1-自动，2-手动',
  `tigger_code` varchar(20) DEFAULT NULL COMMENT '触发器编码',
  `effective_length` int(11) DEFAULT NULL,
  `send_content` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，0-禁用，1-正常 2-未启用 9-删除 ',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  `on_off` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='群控方案表';

-- ----------------------------
-- Records of control_solution
-- ----------------------------
INSERT INTO `control_solution` VALUES ('1', 'CSTK9P2R', '1232321', '1', '3', '1', 'TGRKT1H', '23', null, '1', '312321323212321', '1481098495000', '1481786560000', '0');
INSERT INTO `control_solution` VALUES ('2', 'CST4IHF4', '多条内容', '1', '3', '1', 'TG794H8', '12', null, '1', '唉', '1481252868000', '1481786705000', '0');
INSERT INTO `control_solution` VALUES ('3', 'CST4FYSA', '123', '1', '3', '1', 'TG794H8', '12', null, '1', 'qweqweqwe', '1481264430000', null, '0');
INSERT INTO `control_solution` VALUES ('4', 'CSTXFZ96', '123', '1', '3', '1', 'TG794H8', '12', null, '1', '23423423', '1481264513000', null, '0');
INSERT INTO `control_solution` VALUES ('6', 'CSTIQYDU', '123', '1', '4', '1', 'TGKSD5W', '123', null, '1', 'sadfsadf', '1481264608000', null, '0');
INSERT INTO `control_solution` VALUES ('8', 'CST9CBH3', '123', '1', '3', '2', '', '10', null, '1', '111', '1481266127000', '1481269173000', '0');
INSERT INTO `control_solution` VALUES ('9', 'CSTHNELA', '从头再来', '1', '2', '1', 'TG794H8', '111', null, '1', '从新的内容', '1481271414000', '1481274152000', '0');
INSERT INTO `control_solution` VALUES ('12', 'CSTIHMAQ', '测试', '1', '2', '1', 'TGKSD5W', '10', null, '1', '而测试', '1481271709000', '1481508819000', '0');
INSERT INTO `control_solution` VALUES ('13', 'CSTFI7A3', '看看有颜色吗', '1', '3', '1', 'TG794H8', '12', null, '1', '雨天路滑', '1481509406000', '1481526701000', '0');
INSERT INTO `control_solution` VALUES ('14', 'CSTJCU49', 'displayType改动', '1', '4', '1', 'TGEASZB', '20', null, '1', 'undefined', '1481513055000', '1481786400000', '0');
INSERT INTO `control_solution` VALUES ('15', 'CSTWDKZJ', '凄凄切切', '1', '1', '1', 'TG794H8', '1', null, '1', 'undefined', '1481526169000', '1481526453000', '0');
INSERT INTO `control_solution` VALUES ('16', 'CSTVQNNL', '群控', '2', '1', '1', 'TG794H8', '20', null, '1', '11', '1481527612000', '1481624134000', '0');
INSERT INTO `control_solution` VALUES ('17', 'CSTX4I7W', 'qwe', '1', '1', '1', 'TGEASZB', '1', null, '1', '1231', '1481532946000', '1481786500000', '0');
INSERT INTO `control_solution` VALUES ('18', 'CST4BG4N', 'qwe', '2', '1', '1', 'TGEASZB', '1', null, '1', '11232', '1481534236000', null, '1');
INSERT INTO `control_solution` VALUES ('20', 'CST94ZJM', ',,,,,,,', '1', '2', '1', 'TG794H8', '3', null, '1', 'refgtrgrgtgt', '1481786777000', '1481787250000', '0');
INSERT INTO `control_solution` VALUES ('21', 'CST8SMZT', '111111', '1', '2', '1', 'TGQY95W', '2', null, '1', 'ewrwerwerewrewrewrew', '1481787363000', '1481787387000', '0');
INSERT INTO `control_solution` VALUES ('22', 'CSTQ5SD4', '111111', '1', '2', '1', 'TGQY95W', '2', null, '1', 'wqerwerewrewrewr', '1481787450000', '1481788052000', '0');
INSERT INTO `control_solution` VALUES ('24', 'CSTZ3CQ2', '111', '1', '2', '2', '', '2', null, '1', 'wqewqewqewqeqw', '1481788212000', '1482228531000', '0');
INSERT INTO `control_solution` VALUES ('25', 'CSTIQBKN', '我是手动', '1', '3', '2', '', '10', null, '1', 'undefined', '1481882562000', '1482329320000', '0');
INSERT INTO `control_solution` VALUES ('26', 'CSTGIT36', '群控配置修改', '2', '4', '2', '', '222', null, '1', '111', '1481950324000', '1482386905000', '0');
INSERT INTO `control_solution` VALUES ('27', 'CSTBHCN8', '232', '2', '2', '1', 'TGTLINH', '20', null, '9', 'asewqeqweqwewqewq', '1482115316000', null, '0');
INSERT INTO `control_solution` VALUES ('28', 'CSTGKB3P', '123', '2', '2', '1', 'TGRJCWZ', '2', null, '9', 'werqwerwerwerw', '1482116274000', null, '0');
INSERT INTO `control_solution` VALUES ('30', 'CSTZLLV1', '1212', '1', '4', '1', 'TGFMJ6J', '1', null, '1', '12321321', '1482117415000', '1482123026000', '0');
INSERT INTO `control_solution` VALUES ('32', 'CSTA1ISN', '11', '2', '2', '1', 'TGTLINH', '111', null, '9', '22', '1482118558000', null, '0');
INSERT INTO `control_solution` VALUES ('33', 'CSTRWIVW', '111', '2', '2', '2', '', '2', null, '9', '11', '1482118766000', '1482118803000', '0');
INSERT INTO `control_solution` VALUES ('34', 'CST1EUFS', '1111', '2', '2', '2', '', '33', null, '1', '11', '1482118870000', '1482139910000', '1');
INSERT INTO `control_solution` VALUES ('35', 'CST4FQAA', '1', '1', '2', '1', 'TG62NA6', '2', null, '1', '1', '1482122408000', '1482382009000', '0');
INSERT INTO `control_solution` VALUES ('36', 'CSTKA655', 'qwer', '2', '2', '1', 'TGTLINH', '33', null, '9', 'wqewqeqwewqewq', '1482123041000', null, '0');
INSERT INTO `control_solution` VALUES ('37', 'CST7T6AF', 'qq', '1', '2', '1', 'TGRJCWZ', '3', null, '1', 'dsfsdfsdfds', '1482123660000', '1482377703000', '0');
INSERT INTO `control_solution` VALUES ('38', 'CSTB3PCM', '232', '2', '1', '1', 'TGRJCWZ', '3', null, '9', 'werwerwerew', '1482123856000', null, '0');
INSERT INTO `control_solution` VALUES ('39', 'CST6TYTC', '2wee', '2', '1', '1', 'TG794H8', '3', null, '9', 'qqweqwewqw', '1482126346000', null, '0');
INSERT INTO `control_solution` VALUES ('40', 'CST9B33C', 'qwer', '2', '2', '1', 'TGTLINH', '4', null, '9', 'sdfsdfsdfdsds', '1482126790000', null, '0');
INSERT INTO `control_solution` VALUES ('41', 'CSTMJK8B', 'qwer', '2', '2', '1', 'TG794H8', '4', null, '9', 'wqerewrewrew', '1482127020000', '1482130655000', '0');
INSERT INTO `control_solution` VALUES ('42', 'CST2IQDG', 'qw123', '1', '2', '1', 'TGRJCWZ', '3', null, '9', 'eqweqwwqewqwqwqwq', '1482130039000', '1482130592000', '0');
INSERT INTO `control_solution` VALUES ('43', 'CSTJU5PI', '11111', '1', '2', '1', 'TG62NA6', '0', null, '1', '1111', '1482304499000', null, '1');
INSERT INTO `control_solution` VALUES ('44', 'CSTXWUPQ', '进去了吗', '1', '2', '2', '', '0', null, '1', '111', '1482304603000', '1482387432000', '1');
INSERT INTO `control_solution` VALUES ('45', 'CST245PG', '1111', '1', '2', '2', '', '0', null, '1', '1111', '1482305593000', '1482306150000', '0');
INSERT INTO `control_solution` VALUES ('46', 'CSTU52LW', '11', '2', '1', '1', 'TG794H8', '22', null, '1', 'qq群', '1482306968000', '1482307954000', '0');
INSERT INTO `control_solution` VALUES ('47', 'CST9KS8N', '丢对', '2', '1', '1', 'TGQY95W', '1', null, '1', '其味无穷无群无群二群无', '1482310168000', null, '1');
INSERT INTO `control_solution` VALUES ('48', 'CSTLE5KV', '测试', '1', '2', '1', 'TGBXH7G', '0', null, '9', '测试', '1482314772000', null, '0');
INSERT INTO `control_solution` VALUES ('49', 'CSTCTIMR', '11111', '2', '1', '1', 'TGEASZB', '2222', null, '9', '111', '1482326771000', '1482326776000', '0');
INSERT INTO `control_solution` VALUES ('50', 'CSTZ8PT3', '1', '1', '1', '1', 'TGQY95W', '0', null, '9', 'qweqwe', '1482326810000', '1482326823000', '1');
INSERT INTO `control_solution` VALUES ('51', 'CSTNECI3', '触发器编码', '2', '1', '1', 'TG2VZ1W', '1111', null, '1', '111', '1482395666000', null, '1');
INSERT INTO `control_solution` VALUES ('52', 'CST3E352', '群控配置修改', '2', '1', '1', 'TGMY8J1', '1110', null, '1', '自行车自行车', '1482395784000', null, '1');
INSERT INTO `control_solution` VALUES ('53', 'CST11JE6', 'qqqqq', '1', '2', '1', 'TGXBF42', '0', null, '1', 'wqewqewq', '1482909633000', null, '1');
INSERT INTO `control_solution` VALUES ('54', 'CSTJ68RW', '详情右名字吗', '1', '2', '1', 'TGXBF42', '0', null, '1', '123', '1482909705000', null, '1');
INSERT INTO `control_solution` VALUES ('55', 'CSTEG155', '名字', '2', '1', '1', 'TGFCAUK', '2', null, '1', null, '1482909809000', null, '0');

-- ----------------------------
-- Table structure for control_solution_config
-- ----------------------------
DROP TABLE IF EXISTS `control_solution_config`;
CREATE TABLE `control_solution_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `solution_code` varchar(20) NOT NULL COMMENT '编码',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `equipment_classify` varchar(20) DEFAULT NULL,
  `send_content` varchar(500) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COMMENT='设备控制配置表';

-- ----------------------------
-- Records of control_solution_config
-- ----------------------------
INSERT INTO `control_solution_config` VALUES ('75', 'CSTFI7A3', 'EQRER2C', null, '[{\"duration\":222,\"sendContent\":\"1111\"},{\"duration\":444,\"sendContent\":\"3333\"}]', '1', '雨天路滑', '1481526701000', null);
INSERT INTO `control_solution_config` VALUES ('77', 'CSTFI7A3', 'EQX7KTS', null, '[{\"duration\":2222,\"sendContent\":\"11111\"},{\"duration\":4444,\"sendContent\":\"3333\"}]', '1', '雨天路滑', '1481526701000', null);
INSERT INTO `control_solution_config` VALUES ('85', 'CST4BG4N', 'EQ4U992', 'QSSXT', '1', '1', '11232', '1481534236000', null);
INSERT INTO `control_solution_config` VALUES ('86', 'CSTVQNNL', 'EQ4U992', 'QSSXT', '0', '1', '11', '1481624136000', null);
INSERT INTO `control_solution_config` VALUES ('87', 'CSTVQNNL', 'EQDQ4CC', 'QXSXT', '1', '1', '11', '1481624137000', null);
INSERT INTO `control_solution_config` VALUES ('89', 'CSTJCU49', 'EQRER2C', null, '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', 'undefined', '1481786401000', null);
INSERT INTO `control_solution_config` VALUES ('90', 'CSTJCU49', 'EQUJGI8', null, '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', 'undefined', '1481786401000', null);
INSERT INTO `control_solution_config` VALUES ('91', 'CSTJCU49', 'EQX7KTS', null, '[{\"sendContent\":\"wwww\"}]', '1', 'undefined', '1481786401000', null);
INSERT INTO `control_solution_config` VALUES ('92', 'CSTX4I7W', 'EQRER2C', null, '[{\"duration\":2,\"sendContent\":\"21312\"}]', '1', '1231', '1481786500000', null);
INSERT INTO `control_solution_config` VALUES ('93', 'CSTX4I7W', 'EQUJGI8', null, '[{\"duration\":1,\"sendContent\":\"1232\"}]', '1', '1231', '1481786500000', null);
INSERT INTO `control_solution_config` VALUES ('94', 'CSTK9P2R', 'EQX7KTS', 'CSTGIT36', '[{\"duration\":2,\"sendContent\":\"wrerewrewrew\"}]', '1', '312321323212321', '1481786561000', null);
INSERT INTO `control_solution_config` VALUES ('95', 'CSTK9P2R', 'EQQQ2SD', null, '[{\"duration\":4,\"sendContent\":\"ewqewqewqewqewqeqwewq\"}]', '1', '312321323212321', '1481786562000', null);
INSERT INTO `control_solution_config` VALUES ('96', 'CST4IHF4', 'EQRER2C', null, '[{\"duration\":3,\"sendContent\":\"雨天路滑\"},{\"duration\":2,\"sendContent\":\"下雨了\"}]', '1', '唉', '1481786706000', null);
INSERT INTO `control_solution_config` VALUES ('97', 'CST4IHF4', 'EQWBK8A', null, '[{\"duration\":3,\"sendContent\":\"不下了\"},{\"sendContent\":\"下大了\"}]', '1', '唉', '1481786708000', null);
INSERT INTO `control_solution_config` VALUES ('164', 'CSTZLLV1', 'EQ5Z91M', null, '[{\"duration\":3333,\"sendContent\":\"sdfasd\"}]', '1', '12321321', '1482123026000', null);
INSERT INTO `control_solution_config` VALUES ('165', 'CSTZLLV1', 'EQRER2C', 'FXKBQBB', '[{\"duration\":2,\"sendContent\":\"ewrwrerew\"}]', '1', '12321321', '1482123026000', null);
INSERT INTO `control_solution_config` VALUES ('166', 'CSTZLLV1', 'EQUJGI8', 'MJSKBQBB', '[{\"duration\":3,\"sendContent\":\"vxcvzxcvxzcvxzcvzxcv\"}]', '1', '12321321', '1482123026000', null);
INSERT INTO `control_solution_config` VALUES ('188', 'CST1EUFS', 'EQ4U992', 'QSSXT', '0', '1', '11', '1482139910000', null);
INSERT INTO `control_solution_config` VALUES ('189', 'CST1EUFS', 'EQDQ4CC', 'QXSXT', '1', '1', '11', '1482139910000', null);
INSERT INTO `control_solution_config` VALUES ('190', 'CST1EUFS', 'EQHE76P', null, '0', '1', '11', '1482139910000', null);
INSERT INTO `control_solution_config` VALUES ('192', 'CSTZ3CQ2', 'EQQQ2SD', 'FXKBQBB', '[{\"duration\":232,\"sendContent\":\"ewewqewq\"}]', '1', 'wqewqewqewqeqw', '1482228531000', null);
INSERT INTO `control_solution_config` VALUES ('193', 'CSTZ3CQ2', 'EQRER2C', 'FXKBQBB', '[{\"duration\":2,\"sendContent\":\"ewqewqewqe\"}]', '1', 'wqewqewqewqeqw', '1482228531000', null);
INSERT INTO `control_solution_config` VALUES ('194', 'CSTJU5PI', 'EQUJGI8', 'MJSKBQBB', '[{\"duration\":11,\"sendContent\":\"sdfsdf\"}]', '1', '1111', '1482304501000', null);
INSERT INTO `control_solution_config` VALUES ('199', 'CST245PG', 'EQ5Z91M', 'FXKBQBB', '[{\"duration\":-32,\"sendContent\":\"111\"}]', '1', '1111', '1482306150000', null);
INSERT INTO `control_solution_config` VALUES ('200', 'CST245PG', 'EQUJGI8', 'MJSKBQBB', '[{\"duration\":111,\"sendContent\":\"撒大声地\"}]', '1', '1111', '1482306150000', null);
INSERT INTO `control_solution_config` VALUES ('203', 'CSTU52LW', 'EQDQ4CC', 'QXSXT', '0', '1', 'qq群', '1482307956000', null);
INSERT INTO `control_solution_config` VALUES ('204', 'CSTU52LW', 'EQHE76P', 'WBCLJCQ', '1', '1', 'qq群', '1482307957000', null);
INSERT INTO `control_solution_config` VALUES ('205', 'CST9KS8N', 'EQDQ4CC', 'QXSXT', '1', '1', '其味无穷无群无群二群无', '1482310168000', null);
INSERT INTO `control_solution_config` VALUES ('206', 'CST9KS8N', 'EQQ735V', 'COJCQ', '0', '1', '其味无穷无群无群二群无', '1482310168000', null);
INSERT INTO `control_solution_config` VALUES ('215', 'CSTIQBKN', 'EQDW9GR', 'FXKBQBB', '[{\"duration\":2222,\"sendContent\":\"11\"}]', '1', 'undefined', '1482329320000', null);
INSERT INTO `control_solution_config` VALUES ('216', 'CSTIQBKN', 'EQUJGI8', 'MJSKBQBB', '[{\"duration\":3,\"sendContent\":\"测试\"}]', '1', 'undefined', '1482329320000', null);
INSERT INTO `control_solution_config` VALUES ('217', 'CST7T6AF', 'EQ5Z91M', 'FXKBQBB', '[{\"duration\":4,\"sendContent\":\"dasdsadsad\"}]', '1', 'dsfsdfsdfds', '1482377704000', null);
INSERT INTO `control_solution_config` VALUES ('218', 'CST7T6AF', 'EQRER2C', 'FXKBQBB', '[{\"duration\":4,\"sendContent\":\"qweqweqwqw\"}]', '1', 'dsfsdfsdfds', '1482377704000', null);
INSERT INTO `control_solution_config` VALUES ('219', 'CST4FQAA', 'EQDW9GR', 'FXKBQBB', '[{\"duration\":33333,\"sendContent\":\"12312\"}]', '1', '1', '1482382009000', null);
INSERT INTO `control_solution_config` VALUES ('220', 'CST4FQAA', 'EQUJGI8', 'MJSKBQBB', '[{\"duration\":3333,\"sendContent\":\"请问请问\"}]', '1', '1', '1482382009000', null);
INSERT INTO `control_solution_config` VALUES ('221', 'CSTGIT36', 'EQCPY4G', null, '1', '1', '111', '1482386905000', null);
INSERT INTO `control_solution_config` VALUES ('222', 'CSTGIT36', 'EQDQ4CC', null, '1', '1', '111', '1482386905000', null);
INSERT INTO `control_solution_config` VALUES ('223', 'CSTGIT36', 'EQHE76P', null, '0', '1', '111', '1482386905000', null);
INSERT INTO `control_solution_config` VALUES ('224', 'CSTNECI3', 'EQ4U992', 'QSSXT', '0', '1', '111', '1482395666000', null);
INSERT INTO `control_solution_config` VALUES ('225', 'CSTNECI3', 'EQDQ4CC', 'QXSXT', '1', '1', '111', '1482395666000', null);
INSERT INTO `control_solution_config` VALUES ('226', 'CST3E352', 'EQDQ4CC', 'QXSXT', '1', '1', '自行车自行车', '1482395784000', null);
INSERT INTO `control_solution_config` VALUES ('227', 'CST11JE6', 'EQDW9GR', 'FXKBQBB', '[{\"duration\":3,\"sendContent\":\"qwewqewq\"}]', '1', 'wqewqewq', '1482909633000', null);
INSERT INTO `control_solution_config` VALUES ('228', 'CST11JE6', 'EQRER2C', 'FXKBQBB', '[{\"duration\":3,\"sendContent\":\"asdas\"}]', '1', 'wqewqewq', '1482909633000', null);

-- ----------------------------
-- Table structure for control_solution_send_record
-- ----------------------------
DROP TABLE IF EXISTS `control_solution_send_record`;
CREATE TABLE `control_solution_send_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `level` int(11) NOT NULL COMMENT '等级：1-普通，2-一般，3-紧急，4-特级',
  `send_content` varchar(500) DEFAULT NULL,
  `effect_time` varchar(20) DEFAULT NULL,
  `expire_time` varchar(20) DEFAULT NULL,
  `send_stauts` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COMMENT='群控方案发送记录表';

-- ----------------------------
-- Records of control_solution_send_record
-- ----------------------------
INSERT INTO `control_solution_send_record` VALUES ('1', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667510000');
INSERT INTO `control_solution_send_record` VALUES ('2', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667537000');
INSERT INTO `control_solution_send_record` VALUES ('3', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667597000');
INSERT INTO `control_solution_send_record` VALUES ('4', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667657000');
INSERT INTO `control_solution_send_record` VALUES ('5', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667723000');
INSERT INTO `control_solution_send_record` VALUES ('6', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667780000');
INSERT INTO `control_solution_send_record` VALUES ('7', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667837000');
INSERT INTO `control_solution_send_record` VALUES ('8', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667897000');
INSERT INTO `control_solution_send_record` VALUES ('9', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480667957000');
INSERT INTO `control_solution_send_record` VALUES ('10', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668021000');
INSERT INTO `control_solution_send_record` VALUES ('11', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668077000');
INSERT INTO `control_solution_send_record` VALUES ('12', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668140000');
INSERT INTO `control_solution_send_record` VALUES ('13', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668199000');
INSERT INTO `control_solution_send_record` VALUES ('14', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668259000');
INSERT INTO `control_solution_send_record` VALUES ('15', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668321000');
INSERT INTO `control_solution_send_record` VALUES ('16', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668382000');
INSERT INTO `control_solution_send_record` VALUES ('17', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668439000');
INSERT INTO `control_solution_send_record` VALUES ('18', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668499000');
INSERT INTO `control_solution_send_record` VALUES ('19', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668559000');
INSERT INTO `control_solution_send_record` VALUES ('20', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668619000');
INSERT INTO `control_solution_send_record` VALUES ('21', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668679000');
INSERT INTO `control_solution_send_record` VALUES ('22', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668739000');
INSERT INTO `control_solution_send_record` VALUES ('23', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668799000');
INSERT INTO `control_solution_send_record` VALUES ('24', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668859000');
INSERT INTO `control_solution_send_record` VALUES ('25', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668919000');
INSERT INTO `control_solution_send_record` VALUES ('26', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480668987000');
INSERT INTO `control_solution_send_record` VALUES ('27', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669046000');
INSERT INTO `control_solution_send_record` VALUES ('28', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669100000');
INSERT INTO `control_solution_send_record` VALUES ('29', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669160000');
INSERT INTO `control_solution_send_record` VALUES ('30', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669220000');
INSERT INTO `control_solution_send_record` VALUES ('31', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669280000');
INSERT INTO `control_solution_send_record` VALUES ('32', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669340000');
INSERT INTO `control_solution_send_record` VALUES ('33', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669399000');
INSERT INTO `control_solution_send_record` VALUES ('34', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669459000');
INSERT INTO `control_solution_send_record` VALUES ('35', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669518000');
INSERT INTO `control_solution_send_record` VALUES ('36', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669585000');
INSERT INTO `control_solution_send_record` VALUES ('37', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669639000');
INSERT INTO `control_solution_send_record` VALUES ('38', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669698000');
INSERT INTO `control_solution_send_record` VALUES ('39', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669758000');
INSERT INTO `control_solution_send_record` VALUES ('40', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669818000');
INSERT INTO `control_solution_send_record` VALUES ('41', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669881000');
INSERT INTO `control_solution_send_record` VALUES ('42', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669938000');
INSERT INTO `control_solution_send_record` VALUES ('43', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480669998000');
INSERT INTO `control_solution_send_record` VALUES ('44', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670059000');
INSERT INTO `control_solution_send_record` VALUES ('45', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670119000');
INSERT INTO `control_solution_send_record` VALUES ('46', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670178000');
INSERT INTO `control_solution_send_record` VALUES ('47', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670238000');
INSERT INTO `control_solution_send_record` VALUES ('48', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670303000');
INSERT INTO `control_solution_send_record` VALUES ('49', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480670966000');
INSERT INTO `control_solution_send_record` VALUES ('50', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671023000');
INSERT INTO `control_solution_send_record` VALUES ('51', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671083000');
INSERT INTO `control_solution_send_record` VALUES ('52', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671142000');
INSERT INTO `control_solution_send_record` VALUES ('53', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671202000');
INSERT INTO `control_solution_send_record` VALUES ('54', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671258000');
INSERT INTO `control_solution_send_record` VALUES ('55', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671323000');
INSERT INTO `control_solution_send_record` VALUES ('56', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671383000');
INSERT INTO `control_solution_send_record` VALUES ('57', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671442000');
INSERT INTO `control_solution_send_record` VALUES ('58', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671501000');
INSERT INTO `control_solution_send_record` VALUES ('59', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671557000');
INSERT INTO `control_solution_send_record` VALUES ('60', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671618000');
INSERT INTO `control_solution_send_record` VALUES ('61', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671680000');
INSERT INTO `control_solution_send_record` VALUES ('62', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671737000');
INSERT INTO `control_solution_send_record` VALUES ('63', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671797000');
INSERT INTO `control_solution_send_record` VALUES ('64', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671857000');
INSERT INTO `control_solution_send_record` VALUES ('65', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671921000');
INSERT INTO `control_solution_send_record` VALUES ('66', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480671980000');
INSERT INTO `control_solution_send_record` VALUES ('67', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672046000');
INSERT INTO `control_solution_send_record` VALUES ('68', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672106000');
INSERT INTO `control_solution_send_record` VALUES ('69', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672162000');
INSERT INTO `control_solution_send_record` VALUES ('70', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672224000');
INSERT INTO `control_solution_send_record` VALUES ('71', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672282000');
INSERT INTO `control_solution_send_record` VALUES ('72', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672342000');
INSERT INTO `control_solution_send_record` VALUES ('73', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672403000');
INSERT INTO `control_solution_send_record` VALUES ('74', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672462000');
INSERT INTO `control_solution_send_record` VALUES ('75', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672520000');
INSERT INTO `control_solution_send_record` VALUES ('76', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672583000');
INSERT INTO `control_solution_send_record` VALUES ('77', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672644000');
INSERT INTO `control_solution_send_record` VALUES ('78', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672706000');
INSERT INTO `control_solution_send_record` VALUES ('79', 'CSTGKXB9', '1', '1', null, '1480665933000', '1512201933000', '1', '1', '1480672758000');
INSERT INTO `control_solution_send_record` VALUES ('80', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906655000');
INSERT INTO `control_solution_send_record` VALUES ('81', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906666000');
INSERT INTO `control_solution_send_record` VALUES ('82', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906699000');
INSERT INTO `control_solution_send_record` VALUES ('83', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906758000');
INSERT INTO `control_solution_send_record` VALUES ('84', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906818000');
INSERT INTO `control_solution_send_record` VALUES ('85', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906878000');
INSERT INTO `control_solution_send_record` VALUES ('86', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906937000');
INSERT INTO `control_solution_send_record` VALUES ('87', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480906999000');
INSERT INTO `control_solution_send_record` VALUES ('88', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907063000');
INSERT INTO `control_solution_send_record` VALUES ('89', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907120000');
INSERT INTO `control_solution_send_record` VALUES ('90', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907180000');
INSERT INTO `control_solution_send_record` VALUES ('91', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907239000');
INSERT INTO `control_solution_send_record` VALUES ('92', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907299000');
INSERT INTO `control_solution_send_record` VALUES ('93', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907359000');
INSERT INTO `control_solution_send_record` VALUES ('94', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907419000');
INSERT INTO `control_solution_send_record` VALUES ('95', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907480000');
INSERT INTO `control_solution_send_record` VALUES ('96', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907539000');
INSERT INTO `control_solution_send_record` VALUES ('97', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907600000');
INSERT INTO `control_solution_send_record` VALUES ('98', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907659000');
INSERT INTO `control_solution_send_record` VALUES ('99', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907720000');
INSERT INTO `control_solution_send_record` VALUES ('100', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480907779000');
INSERT INTO `control_solution_send_record` VALUES ('101', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908389000');
INSERT INTO `control_solution_send_record` VALUES ('102', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908445000');
INSERT INTO `control_solution_send_record` VALUES ('103', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908505000');
INSERT INTO `control_solution_send_record` VALUES ('104', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908566000');
INSERT INTO `control_solution_send_record` VALUES ('105', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908627000');
INSERT INTO `control_solution_send_record` VALUES ('106', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908686000');
INSERT INTO `control_solution_send_record` VALUES ('107', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480908747000');
INSERT INTO `control_solution_send_record` VALUES ('108', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909648000');
INSERT INTO `control_solution_send_record` VALUES ('109', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909706000');
INSERT INTO `control_solution_send_record` VALUES ('110', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909766000');
INSERT INTO `control_solution_send_record` VALUES ('111', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909827000');
INSERT INTO `control_solution_send_record` VALUES ('112', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909886000');
INSERT INTO `control_solution_send_record` VALUES ('113', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480909946000');
INSERT INTO `control_solution_send_record` VALUES ('114', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910005000');
INSERT INTO `control_solution_send_record` VALUES ('115', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910065000');
INSERT INTO `control_solution_send_record` VALUES ('116', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910121000');
INSERT INTO `control_solution_send_record` VALUES ('117', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910184000');
INSERT INTO `control_solution_send_record` VALUES ('118', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910241000');
INSERT INTO `control_solution_send_record` VALUES ('119', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910302000');
INSERT INTO `control_solution_send_record` VALUES ('120', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910361000');
INSERT INTO `control_solution_send_record` VALUES ('121', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910420000');
INSERT INTO `control_solution_send_record` VALUES ('122', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910480000');
INSERT INTO `control_solution_send_record` VALUES ('123', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910540000');
INSERT INTO `control_solution_send_record` VALUES ('124', 'CSTV3VE9', '情报板', '2', null, '1480665933000', '1512201933000', '1', '1', '1480910600000');
INSERT INTO `control_solution_send_record` VALUES ('125', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515588000');
INSERT INTO `control_solution_send_record` VALUES ('126', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515600000');
INSERT INTO `control_solution_send_record` VALUES ('127', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515644000');
INSERT INTO `control_solution_send_record` VALUES ('128', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515708000');
INSERT INTO `control_solution_send_record` VALUES ('129', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515764000');
INSERT INTO `control_solution_send_record` VALUES ('130', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481515838000');
INSERT INTO `control_solution_send_record` VALUES ('131', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516110000');
INSERT INTO `control_solution_send_record` VALUES ('132', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516791000');
INSERT INTO `control_solution_send_record` VALUES ('133', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516870000');
INSERT INTO `control_solution_send_record` VALUES ('134', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516945000');
INSERT INTO `control_solution_send_record` VALUES ('135', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516959000');
INSERT INTO `control_solution_send_record` VALUES ('136', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481516975000');
INSERT INTO `control_solution_send_record` VALUES ('137', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481517330000');
INSERT INTO `control_solution_send_record` VALUES ('138', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481517346000');
INSERT INTO `control_solution_send_record` VALUES ('139', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481517389000');
INSERT INTO `control_solution_send_record` VALUES ('140', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481517449000');
INSERT INTO `control_solution_send_record` VALUES ('141', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481517509000');
INSERT INTO `control_solution_send_record` VALUES ('142', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481521772000');
INSERT INTO `control_solution_send_record` VALUES ('143', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481521830000');
INSERT INTO `control_solution_send_record` VALUES ('144', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481521889000');
INSERT INTO `control_solution_send_record` VALUES ('145', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481521950000');
INSERT INTO `control_solution_send_record` VALUES ('146', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481522010000');
INSERT INTO `control_solution_send_record` VALUES ('147', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481522071000');
INSERT INTO `control_solution_send_record` VALUES ('148', 'CSTJCU49', 'displayType改动', '4', null, '1480665933000', '1512201933000', '1', '1', '1481522135000');

-- ----------------------------
-- Table structure for control_solution_send_record_item
-- ----------------------------
DROP TABLE IF EXISTS `control_solution_send_record_item`;
CREATE TABLE `control_solution_send_record_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) DEFAULT NULL COMMENT '编码',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `send_content` varchar(500) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1072 DEFAULT CHARSET=utf8 COMMENT='发送记录设备表';

-- ----------------------------
-- Records of control_solution_send_record_item
-- ----------------------------
INSERT INTO `control_solution_send_record_item` VALUES ('1', 'CSTGKXB9', 'EQUJGI8', '今天天气不错', '1', '1480667508000');
INSERT INTO `control_solution_send_record_item` VALUES ('2', 'CSTGKXB9', 'EQUJGI8', '今天天气不错', '1', '1480667537000');
INSERT INTO `control_solution_send_record_item` VALUES ('3', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667597000');
INSERT INTO `control_solution_send_record_item` VALUES ('4', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667657000');
INSERT INTO `control_solution_send_record_item` VALUES ('5', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667722000');
INSERT INTO `control_solution_send_record_item` VALUES ('6', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667779000');
INSERT INTO `control_solution_send_record_item` VALUES ('7', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667837000');
INSERT INTO `control_solution_send_record_item` VALUES ('8', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667897000');
INSERT INTO `control_solution_send_record_item` VALUES ('9', 'CSTGKXB9', 'EQUJGI8', 'hello', '1', '1480667957000');
INSERT INTO `control_solution_send_record_item` VALUES ('10', 'CSTGKXB9', 'EQUJGI8', '11111111111111111111111111111111111111111111111111111', '1', '1480668021000');
INSERT INTO `control_solution_send_record_item` VALUES ('11', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668077000');
INSERT INTO `control_solution_send_record_item` VALUES ('12', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668140000');
INSERT INTO `control_solution_send_record_item` VALUES ('13', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668199000');
INSERT INTO `control_solution_send_record_item` VALUES ('14', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668259000');
INSERT INTO `control_solution_send_record_item` VALUES ('15', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668320000');
INSERT INTO `control_solution_send_record_item` VALUES ('16', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668381000');
INSERT INTO `control_solution_send_record_item` VALUES ('17', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668439000');
INSERT INTO `control_solution_send_record_item` VALUES ('18', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668499000');
INSERT INTO `control_solution_send_record_item` VALUES ('19', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668558000');
INSERT INTO `control_solution_send_record_item` VALUES ('20', 'CSTGKXB9', 'EQUJGI8', '2222222', '1', '1480668619000');
INSERT INTO `control_solution_send_record_item` VALUES ('21', 'CSTGKXB9', 'EQUJGI8', '同志们加油,么么哒', '1', '1480668679000');
INSERT INTO `control_solution_send_record_item` VALUES ('22', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480668739000');
INSERT INTO `control_solution_send_record_item` VALUES ('23', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480668799000');
INSERT INTO `control_solution_send_record_item` VALUES ('24', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480668859000');
INSERT INTO `control_solution_send_record_item` VALUES ('25', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480668919000');
INSERT INTO `control_solution_send_record_item` VALUES ('26', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480668986000');
INSERT INTO `control_solution_send_record_item` VALUES ('27', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480669045000');
INSERT INTO `control_solution_send_record_item` VALUES ('28', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480669100000');
INSERT INTO `control_solution_send_record_item` VALUES ('29', 'CSTGKXB9', 'EQUJGI8', '0123456789', '1', '1480669160000');
INSERT INTO `control_solution_send_record_item` VALUES ('30', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669220000');
INSERT INTO `control_solution_send_record_item` VALUES ('31', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669280000');
INSERT INTO `control_solution_send_record_item` VALUES ('32', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669340000');
INSERT INTO `control_solution_send_record_item` VALUES ('33', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669399000');
INSERT INTO `control_solution_send_record_item` VALUES ('34', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669459000');
INSERT INTO `control_solution_send_record_item` VALUES ('35', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669518000');
INSERT INTO `control_solution_send_record_item` VALUES ('36', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669584000');
INSERT INTO `control_solution_send_record_item` VALUES ('37', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669639000');
INSERT INTO `control_solution_send_record_item` VALUES ('38', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669698000');
INSERT INTO `control_solution_send_record_item` VALUES ('39', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669758000');
INSERT INTO `control_solution_send_record_item` VALUES ('40', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669818000');
INSERT INTO `control_solution_send_record_item` VALUES ('41', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669881000');
INSERT INTO `control_solution_send_record_item` VALUES ('42', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669938000');
INSERT INTO `control_solution_send_record_item` VALUES ('43', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480669998000');
INSERT INTO `control_solution_send_record_item` VALUES ('44', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670059000');
INSERT INTO `control_solution_send_record_item` VALUES ('45', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670118000');
INSERT INTO `control_solution_send_record_item` VALUES ('46', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670178000');
INSERT INTO `control_solution_send_record_item` VALUES ('47', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670238000');
INSERT INTO `control_solution_send_record_item` VALUES ('48', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670279000');
INSERT INTO `control_solution_send_record_item` VALUES ('49', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670302000');
INSERT INTO `control_solution_send_record_item` VALUES ('50', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670344000');
INSERT INTO `control_solution_send_record_item` VALUES ('51', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670643000');
INSERT INTO `control_solution_send_record_item` VALUES ('52', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670703000');
INSERT INTO `control_solution_send_record_item` VALUES ('53', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670763000');
INSERT INTO `control_solution_send_record_item` VALUES ('54', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670884000');
INSERT INTO `control_solution_send_record_item` VALUES ('55', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670941000');
INSERT INTO `control_solution_send_record_item` VALUES ('56', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480670965000');
INSERT INTO `control_solution_send_record_item` VALUES ('57', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671002000');
INSERT INTO `control_solution_send_record_item` VALUES ('58', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671023000');
INSERT INTO `control_solution_send_record_item` VALUES ('59', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671063000');
INSERT INTO `control_solution_send_record_item` VALUES ('60', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671082000');
INSERT INTO `control_solution_send_record_item` VALUES ('61', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671119000');
INSERT INTO `control_solution_send_record_item` VALUES ('62', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671141000');
INSERT INTO `control_solution_send_record_item` VALUES ('63', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671178000');
INSERT INTO `control_solution_send_record_item` VALUES ('64', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671202000');
INSERT INTO `control_solution_send_record_item` VALUES ('65', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671238000');
INSERT INTO `control_solution_send_record_item` VALUES ('66', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671257000');
INSERT INTO `control_solution_send_record_item` VALUES ('67', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671323000');
INSERT INTO `control_solution_send_record_item` VALUES ('68', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671359000');
INSERT INTO `control_solution_send_record_item` VALUES ('69', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671382000');
INSERT INTO `control_solution_send_record_item` VALUES ('70', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671423000');
INSERT INTO `control_solution_send_record_item` VALUES ('71', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671441000');
INSERT INTO `control_solution_send_record_item` VALUES ('72', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671482000');
INSERT INTO `control_solution_send_record_item` VALUES ('73', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671500000');
INSERT INTO `control_solution_send_record_item` VALUES ('74', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671541000');
INSERT INTO `control_solution_send_record_item` VALUES ('75', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671557000');
INSERT INTO `control_solution_send_record_item` VALUES ('76', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671600000');
INSERT INTO `control_solution_send_record_item` VALUES ('77', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671617000');
INSERT INTO `control_solution_send_record_item` VALUES ('78', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671661000');
INSERT INTO `control_solution_send_record_item` VALUES ('79', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671680000');
INSERT INTO `control_solution_send_record_item` VALUES ('80', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671720000');
INSERT INTO `control_solution_send_record_item` VALUES ('81', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671737000');
INSERT INTO `control_solution_send_record_item` VALUES ('82', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671780000');
INSERT INTO `control_solution_send_record_item` VALUES ('83', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671797000');
INSERT INTO `control_solution_send_record_item` VALUES ('84', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671843000');
INSERT INTO `control_solution_send_record_item` VALUES ('85', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671857000');
INSERT INTO `control_solution_send_record_item` VALUES ('86', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671900000');
INSERT INTO `control_solution_send_record_item` VALUES ('87', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671921000');
INSERT INTO `control_solution_send_record_item` VALUES ('88', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671960000');
INSERT INTO `control_solution_send_record_item` VALUES ('89', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480671980000');
INSERT INTO `control_solution_send_record_item` VALUES ('90', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672022000');
INSERT INTO `control_solution_send_record_item` VALUES ('91', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672045000');
INSERT INTO `control_solution_send_record_item` VALUES ('92', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672088000');
INSERT INTO `control_solution_send_record_item` VALUES ('93', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672106000');
INSERT INTO `control_solution_send_record_item` VALUES ('94', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672145000');
INSERT INTO `control_solution_send_record_item` VALUES ('95', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672161000');
INSERT INTO `control_solution_send_record_item` VALUES ('96', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672198000');
INSERT INTO `control_solution_send_record_item` VALUES ('97', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672223000');
INSERT INTO `control_solution_send_record_item` VALUES ('98', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672265000');
INSERT INTO `control_solution_send_record_item` VALUES ('99', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672282000');
INSERT INTO `control_solution_send_record_item` VALUES ('100', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672322000');
INSERT INTO `control_solution_send_record_item` VALUES ('101', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672341000');
INSERT INTO `control_solution_send_record_item` VALUES ('102', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672381000');
INSERT INTO `control_solution_send_record_item` VALUES ('103', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672402000');
INSERT INTO `control_solution_send_record_item` VALUES ('104', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672444000');
INSERT INTO `control_solution_send_record_item` VALUES ('105', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672462000');
INSERT INTO `control_solution_send_record_item` VALUES ('106', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672504000');
INSERT INTO `control_solution_send_record_item` VALUES ('107', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672519000');
INSERT INTO `control_solution_send_record_item` VALUES ('108', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672567000');
INSERT INTO `control_solution_send_record_item` VALUES ('109', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672583000');
INSERT INTO `control_solution_send_record_item` VALUES ('110', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672619000');
INSERT INTO `control_solution_send_record_item` VALUES ('111', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672643000');
INSERT INTO `control_solution_send_record_item` VALUES ('112', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672685000');
INSERT INTO `control_solution_send_record_item` VALUES ('113', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672706000');
INSERT INTO `control_solution_send_record_item` VALUES ('114', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672744000');
INSERT INTO `control_solution_send_record_item` VALUES ('115', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672758000');
INSERT INTO `control_solution_send_record_item` VALUES ('116', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480672804000');
INSERT INTO `control_solution_send_record_item` VALUES ('117', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673160000');
INSERT INTO `control_solution_send_record_item` VALUES ('118', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673218000');
INSERT INTO `control_solution_send_record_item` VALUES ('119', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673278000');
INSERT INTO `control_solution_send_record_item` VALUES ('120', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673339000');
INSERT INTO `control_solution_send_record_item` VALUES ('121', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673398000');
INSERT INTO `control_solution_send_record_item` VALUES ('122', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673458000');
INSERT INTO `control_solution_send_record_item` VALUES ('123', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673518000');
INSERT INTO `control_solution_send_record_item` VALUES ('124', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673578000');
INSERT INTO `control_solution_send_record_item` VALUES ('125', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673638000');
INSERT INTO `control_solution_send_record_item` VALUES ('126', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673698000');
INSERT INTO `control_solution_send_record_item` VALUES ('127', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673758000');
INSERT INTO `control_solution_send_record_item` VALUES ('128', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673818000');
INSERT INTO `control_solution_send_record_item` VALUES ('129', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673879000');
INSERT INTO `control_solution_send_record_item` VALUES ('130', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673938000');
INSERT INTO `control_solution_send_record_item` VALUES ('131', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480673998000');
INSERT INTO `control_solution_send_record_item` VALUES ('132', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674058000');
INSERT INTO `control_solution_send_record_item` VALUES ('133', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674118000');
INSERT INTO `control_solution_send_record_item` VALUES ('134', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674178000');
INSERT INTO `control_solution_send_record_item` VALUES ('135', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674239000');
INSERT INTO `control_solution_send_record_item` VALUES ('136', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674298000');
INSERT INTO `control_solution_send_record_item` VALUES ('137', 'CSTGKXB9', 'EQUJGI8', 'gogogogogogogogogogg', '1', '1480674358000');
INSERT INTO `control_solution_send_record_item` VALUES ('138', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906446000');
INSERT INTO `control_solution_send_record_item` VALUES ('139', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906503000');
INSERT INTO `control_solution_send_record_item` VALUES ('140', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906564000');
INSERT INTO `control_solution_send_record_item` VALUES ('141', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906624000');
INSERT INTO `control_solution_send_record_item` VALUES ('142', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906655000');
INSERT INTO `control_solution_send_record_item` VALUES ('143', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906666000');
INSERT INTO `control_solution_send_record_item` VALUES ('144', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906683000');
INSERT INTO `control_solution_send_record_item` VALUES ('145', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906699000');
INSERT INTO `control_solution_send_record_item` VALUES ('146', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906744000');
INSERT INTO `control_solution_send_record_item` VALUES ('147', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906758000');
INSERT INTO `control_solution_send_record_item` VALUES ('148', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906818000');
INSERT INTO `control_solution_send_record_item` VALUES ('149', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906878000');
INSERT INTO `control_solution_send_record_item` VALUES ('150', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906924000');
INSERT INTO `control_solution_send_record_item` VALUES ('151', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906937000');
INSERT INTO `control_solution_send_record_item` VALUES ('152', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906984000');
INSERT INTO `control_solution_send_record_item` VALUES ('153', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480906999000');
INSERT INTO `control_solution_send_record_item` VALUES ('154', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907063000');
INSERT INTO `control_solution_send_record_item` VALUES ('155', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907104000');
INSERT INTO `control_solution_send_record_item` VALUES ('156', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907120000');
INSERT INTO `control_solution_send_record_item` VALUES ('157', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907164000');
INSERT INTO `control_solution_send_record_item` VALUES ('158', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907180000');
INSERT INTO `control_solution_send_record_item` VALUES ('159', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907224000');
INSERT INTO `control_solution_send_record_item` VALUES ('160', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907239000');
INSERT INTO `control_solution_send_record_item` VALUES ('161', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907284000');
INSERT INTO `control_solution_send_record_item` VALUES ('162', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907299000');
INSERT INTO `control_solution_send_record_item` VALUES ('163', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907344000');
INSERT INTO `control_solution_send_record_item` VALUES ('164', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907359000');
INSERT INTO `control_solution_send_record_item` VALUES ('165', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907419000');
INSERT INTO `control_solution_send_record_item` VALUES ('166', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907464000');
INSERT INTO `control_solution_send_record_item` VALUES ('167', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907480000');
INSERT INTO `control_solution_send_record_item` VALUES ('168', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907524000');
INSERT INTO `control_solution_send_record_item` VALUES ('169', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907539000');
INSERT INTO `control_solution_send_record_item` VALUES ('170', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907585000');
INSERT INTO `control_solution_send_record_item` VALUES ('171', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907600000');
INSERT INTO `control_solution_send_record_item` VALUES ('172', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907644000');
INSERT INTO `control_solution_send_record_item` VALUES ('173', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907659000');
INSERT INTO `control_solution_send_record_item` VALUES ('174', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907704000');
INSERT INTO `control_solution_send_record_item` VALUES ('175', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907720000');
INSERT INTO `control_solution_send_record_item` VALUES ('176', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907764000');
INSERT INTO `control_solution_send_record_item` VALUES ('177', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907779000');
INSERT INTO `control_solution_send_record_item` VALUES ('178', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907824000');
INSERT INTO `control_solution_send_record_item` VALUES ('179', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907884000');
INSERT INTO `control_solution_send_record_item` VALUES ('180', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480907944000');
INSERT INTO `control_solution_send_record_item` VALUES ('181', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908004000');
INSERT INTO `control_solution_send_record_item` VALUES ('182', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908124000');
INSERT INTO `control_solution_send_record_item` VALUES ('183', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908184000');
INSERT INTO `control_solution_send_record_item` VALUES ('184', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908304000');
INSERT INTO `control_solution_send_record_item` VALUES ('185', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908388000');
INSERT INTO `control_solution_send_record_item` VALUES ('186', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908426000');
INSERT INTO `control_solution_send_record_item` VALUES ('187', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908445000');
INSERT INTO `control_solution_send_record_item` VALUES ('188', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908484000');
INSERT INTO `control_solution_send_record_item` VALUES ('189', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908504000');
INSERT INTO `control_solution_send_record_item` VALUES ('190', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908565000');
INSERT INTO `control_solution_send_record_item` VALUES ('191', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908606000');
INSERT INTO `control_solution_send_record_item` VALUES ('192', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908626000');
INSERT INTO `control_solution_send_record_item` VALUES ('193', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908671000');
INSERT INTO `control_solution_send_record_item` VALUES ('194', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908686000');
INSERT INTO `control_solution_send_record_item` VALUES ('195', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908729000');
INSERT INTO `control_solution_send_record_item` VALUES ('196', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908746000');
INSERT INTO `control_solution_send_record_item` VALUES ('197', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908789000');
INSERT INTO `control_solution_send_record_item` VALUES ('198', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908851000');
INSERT INTO `control_solution_send_record_item` VALUES ('199', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908919000');
INSERT INTO `control_solution_send_record_item` VALUES ('200', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908970000');
INSERT INTO `control_solution_send_record_item` VALUES ('201', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480908979000');
INSERT INTO `control_solution_send_record_item` VALUES ('202', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909032000');
INSERT INTO `control_solution_send_record_item` VALUES ('203', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909042000');
INSERT INTO `control_solution_send_record_item` VALUES ('204', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909086000');
INSERT INTO `control_solution_send_record_item` VALUES ('205', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909098000');
INSERT INTO `control_solution_send_record_item` VALUES ('206', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909146000');
INSERT INTO `control_solution_send_record_item` VALUES ('207', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909159000');
INSERT INTO `control_solution_send_record_item` VALUES ('208', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909205000');
INSERT INTO `control_solution_send_record_item` VALUES ('209', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909218000');
INSERT INTO `control_solution_send_record_item` VALUES ('210', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909272000');
INSERT INTO `control_solution_send_record_item` VALUES ('211', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909282000');
INSERT INTO `control_solution_send_record_item` VALUES ('212', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909329000');
INSERT INTO `control_solution_send_record_item` VALUES ('213', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909339000');
INSERT INTO `control_solution_send_record_item` VALUES ('214', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909399000');
INSERT INTO `control_solution_send_record_item` VALUES ('215', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909453000');
INSERT INTO `control_solution_send_record_item` VALUES ('216', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909459000');
INSERT INTO `control_solution_send_record_item` VALUES ('217', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909509000');
INSERT INTO `control_solution_send_record_item` VALUES ('218', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909519000');
INSERT INTO `control_solution_send_record_item` VALUES ('219', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909569000');
INSERT INTO `control_solution_send_record_item` VALUES ('220', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909630000');
INSERT INTO `control_solution_send_record_item` VALUES ('221', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909647000');
INSERT INTO `control_solution_send_record_item` VALUES ('222', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909688000');
INSERT INTO `control_solution_send_record_item` VALUES ('223', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909705000');
INSERT INTO `control_solution_send_record_item` VALUES ('224', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909750000');
INSERT INTO `control_solution_send_record_item` VALUES ('225', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909765000');
INSERT INTO `control_solution_send_record_item` VALUES ('226', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909811000');
INSERT INTO `control_solution_send_record_item` VALUES ('227', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909827000');
INSERT INTO `control_solution_send_record_item` VALUES ('228', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909869000');
INSERT INTO `control_solution_send_record_item` VALUES ('229', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909885000');
INSERT INTO `control_solution_send_record_item` VALUES ('230', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909929000');
INSERT INTO `control_solution_send_record_item` VALUES ('231', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909946000');
INSERT INTO `control_solution_send_record_item` VALUES ('232', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480909991000');
INSERT INTO `control_solution_send_record_item` VALUES ('233', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910004000');
INSERT INTO `control_solution_send_record_item` VALUES ('234', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910046000');
INSERT INTO `control_solution_send_record_item` VALUES ('235', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910064000');
INSERT INTO `control_solution_send_record_item` VALUES ('236', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910106000');
INSERT INTO `control_solution_send_record_item` VALUES ('237', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910121000');
INSERT INTO `control_solution_send_record_item` VALUES ('238', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910164000');
INSERT INTO `control_solution_send_record_item` VALUES ('239', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910184000');
INSERT INTO `control_solution_send_record_item` VALUES ('240', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910225000');
INSERT INTO `control_solution_send_record_item` VALUES ('241', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910241000');
INSERT INTO `control_solution_send_record_item` VALUES ('242', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910285000');
INSERT INTO `control_solution_send_record_item` VALUES ('243', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910302000');
INSERT INTO `control_solution_send_record_item` VALUES ('244', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910344000');
INSERT INTO `control_solution_send_record_item` VALUES ('245', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910361000');
INSERT INTO `control_solution_send_record_item` VALUES ('246', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910404000');
INSERT INTO `control_solution_send_record_item` VALUES ('247', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910420000');
INSERT INTO `control_solution_send_record_item` VALUES ('248', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910464000');
INSERT INTO `control_solution_send_record_item` VALUES ('249', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910480000');
INSERT INTO `control_solution_send_record_item` VALUES ('250', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910524000');
INSERT INTO `control_solution_send_record_item` VALUES ('251', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910540000');
INSERT INTO `control_solution_send_record_item` VALUES ('252', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910585000');
INSERT INTO `control_solution_send_record_item` VALUES ('253', 'CSTV3VE9', 'EQUJGI8', '丢对', '1', '1480910600000');
INSERT INTO `control_solution_send_record_item` VALUES ('254', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279346000');
INSERT INTO `control_solution_send_record_item` VALUES ('255', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279346000');
INSERT INTO `control_solution_send_record_item` VALUES ('256', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279404000');
INSERT INTO `control_solution_send_record_item` VALUES ('257', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279404000');
INSERT INTO `control_solution_send_record_item` VALUES ('258', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279464000');
INSERT INTO `control_solution_send_record_item` VALUES ('259', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279465000');
INSERT INTO `control_solution_send_record_item` VALUES ('260', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279524000');
INSERT INTO `control_solution_send_record_item` VALUES ('261', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279524000');
INSERT INTO `control_solution_send_record_item` VALUES ('262', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279584000');
INSERT INTO `control_solution_send_record_item` VALUES ('263', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279584000');
INSERT INTO `control_solution_send_record_item` VALUES ('264', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279653000');
INSERT INTO `control_solution_send_record_item` VALUES ('265', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279654000');
INSERT INTO `control_solution_send_record_item` VALUES ('266', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279709000');
INSERT INTO `control_solution_send_record_item` VALUES ('267', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279711000');
INSERT INTO `control_solution_send_record_item` VALUES ('268', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279771000');
INSERT INTO `control_solution_send_record_item` VALUES ('269', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279772000');
INSERT INTO `control_solution_send_record_item` VALUES ('270', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279830000');
INSERT INTO `control_solution_send_record_item` VALUES ('271', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279831000');
INSERT INTO `control_solution_send_record_item` VALUES ('272', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279893000');
INSERT INTO `control_solution_send_record_item` VALUES ('273', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279894000');
INSERT INTO `control_solution_send_record_item` VALUES ('274', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481279951000');
INSERT INTO `control_solution_send_record_item` VALUES ('275', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481279952000');
INSERT INTO `control_solution_send_record_item` VALUES ('276', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280010000');
INSERT INTO `control_solution_send_record_item` VALUES ('277', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280012000');
INSERT INTO `control_solution_send_record_item` VALUES ('278', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280071000');
INSERT INTO `control_solution_send_record_item` VALUES ('279', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280072000');
INSERT INTO `control_solution_send_record_item` VALUES ('280', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280130000');
INSERT INTO `control_solution_send_record_item` VALUES ('281', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280131000');
INSERT INTO `control_solution_send_record_item` VALUES ('282', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280192000');
INSERT INTO `control_solution_send_record_item` VALUES ('283', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280193000');
INSERT INTO `control_solution_send_record_item` VALUES ('284', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280250000');
INSERT INTO `control_solution_send_record_item` VALUES ('285', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280251000');
INSERT INTO `control_solution_send_record_item` VALUES ('286', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280325000');
INSERT INTO `control_solution_send_record_item` VALUES ('287', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280326000');
INSERT INTO `control_solution_send_record_item` VALUES ('288', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280378000');
INSERT INTO `control_solution_send_record_item` VALUES ('289', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280380000');
INSERT INTO `control_solution_send_record_item` VALUES ('290', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280435000');
INSERT INTO `control_solution_send_record_item` VALUES ('291', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280436000');
INSERT INTO `control_solution_send_record_item` VALUES ('292', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280492000');
INSERT INTO `control_solution_send_record_item` VALUES ('293', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280493000');
INSERT INTO `control_solution_send_record_item` VALUES ('294', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280551000');
INSERT INTO `control_solution_send_record_item` VALUES ('295', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280552000');
INSERT INTO `control_solution_send_record_item` VALUES ('296', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280611000');
INSERT INTO `control_solution_send_record_item` VALUES ('297', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280612000');
INSERT INTO `control_solution_send_record_item` VALUES ('298', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280670000');
INSERT INTO `control_solution_send_record_item` VALUES ('299', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280672000');
INSERT INTO `control_solution_send_record_item` VALUES ('300', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481280731000');
INSERT INTO `control_solution_send_record_item` VALUES ('301', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481280732000');
INSERT INTO `control_solution_send_record_item` VALUES ('302', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481507226000');
INSERT INTO `control_solution_send_record_item` VALUES ('303', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508572000');
INSERT INTO `control_solution_send_record_item` VALUES ('304', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508720000');
INSERT INTO `control_solution_send_record_item` VALUES ('305', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508723000');
INSERT INTO `control_solution_send_record_item` VALUES ('306', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481508723000');
INSERT INTO `control_solution_send_record_item` VALUES ('307', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508782000');
INSERT INTO `control_solution_send_record_item` VALUES ('308', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481508782000');
INSERT INTO `control_solution_send_record_item` VALUES ('309', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508843000');
INSERT INTO `control_solution_send_record_item` VALUES ('310', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481508843000');
INSERT INTO `control_solution_send_record_item` VALUES ('311', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508902000');
INSERT INTO `control_solution_send_record_item` VALUES ('312', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481508902000');
INSERT INTO `control_solution_send_record_item` VALUES ('313', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481508962000');
INSERT INTO `control_solution_send_record_item` VALUES ('314', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481508962000');
INSERT INTO `control_solution_send_record_item` VALUES ('315', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509022000');
INSERT INTO `control_solution_send_record_item` VALUES ('316', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509022000');
INSERT INTO `control_solution_send_record_item` VALUES ('317', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509082000');
INSERT INTO `control_solution_send_record_item` VALUES ('318', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509082000');
INSERT INTO `control_solution_send_record_item` VALUES ('319', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509142000');
INSERT INTO `control_solution_send_record_item` VALUES ('320', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509142000');
INSERT INTO `control_solution_send_record_item` VALUES ('321', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509202000');
INSERT INTO `control_solution_send_record_item` VALUES ('322', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509203000');
INSERT INTO `control_solution_send_record_item` VALUES ('323', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509262000');
INSERT INTO `control_solution_send_record_item` VALUES ('324', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509262000');
INSERT INTO `control_solution_send_record_item` VALUES ('325', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509267000');
INSERT INTO `control_solution_send_record_item` VALUES ('326', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509272000');
INSERT INTO `control_solution_send_record_item` VALUES ('327', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509322000');
INSERT INTO `control_solution_send_record_item` VALUES ('328', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509322000');
INSERT INTO `control_solution_send_record_item` VALUES ('329', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509382000');
INSERT INTO `control_solution_send_record_item` VALUES ('330', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509382000');
INSERT INTO `control_solution_send_record_item` VALUES ('331', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509442000');
INSERT INTO `control_solution_send_record_item` VALUES ('332', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509442000');
INSERT INTO `control_solution_send_record_item` VALUES ('333', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509563000');
INSERT INTO `control_solution_send_record_item` VALUES ('334', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509563000');
INSERT INTO `control_solution_send_record_item` VALUES ('335', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509622000');
INSERT INTO `control_solution_send_record_item` VALUES ('336', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509622000');
INSERT INTO `control_solution_send_record_item` VALUES ('337', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509685000');
INSERT INTO `control_solution_send_record_item` VALUES ('338', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509685000');
INSERT INTO `control_solution_send_record_item` VALUES ('339', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509712000');
INSERT INTO `control_solution_send_record_item` VALUES ('340', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509740000');
INSERT INTO `control_solution_send_record_item` VALUES ('341', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509742000');
INSERT INTO `control_solution_send_record_item` VALUES ('342', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509742000');
INSERT INTO `control_solution_send_record_item` VALUES ('343', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509802000');
INSERT INTO `control_solution_send_record_item` VALUES ('344', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509803000');
INSERT INTO `control_solution_send_record_item` VALUES ('345', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509863000');
INSERT INTO `control_solution_send_record_item` VALUES ('346', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509863000');
INSERT INTO `control_solution_send_record_item` VALUES ('347', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509866000');
INSERT INTO `control_solution_send_record_item` VALUES ('348', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509866000');
INSERT INTO `control_solution_send_record_item` VALUES ('349', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509922000');
INSERT INTO `control_solution_send_record_item` VALUES ('350', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509922000');
INSERT INTO `control_solution_send_record_item` VALUES ('351', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509924000');
INSERT INTO `control_solution_send_record_item` VALUES ('352', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509924000');
INSERT INTO `control_solution_send_record_item` VALUES ('353', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509982000');
INSERT INTO `control_solution_send_record_item` VALUES ('354', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509982000');
INSERT INTO `control_solution_send_record_item` VALUES ('355', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481509984000');
INSERT INTO `control_solution_send_record_item` VALUES ('356', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481509984000');
INSERT INTO `control_solution_send_record_item` VALUES ('357', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510016000');
INSERT INTO `control_solution_send_record_item` VALUES ('358', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510022000');
INSERT INTO `control_solution_send_record_item` VALUES ('359', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510025000');
INSERT INTO `control_solution_send_record_item` VALUES ('360', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510025000');
INSERT INTO `control_solution_send_record_item` VALUES ('361', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510042000');
INSERT INTO `control_solution_send_record_item` VALUES ('362', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510043000');
INSERT INTO `control_solution_send_record_item` VALUES ('363', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510044000');
INSERT INTO `control_solution_send_record_item` VALUES ('364', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510045000');
INSERT INTO `control_solution_send_record_item` VALUES ('365', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510068000');
INSERT INTO `control_solution_send_record_item` VALUES ('366', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510075000');
INSERT INTO `control_solution_send_record_item` VALUES ('367', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510102000');
INSERT INTO `control_solution_send_record_item` VALUES ('368', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510103000');
INSERT INTO `control_solution_send_record_item` VALUES ('369', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510106000');
INSERT INTO `control_solution_send_record_item` VALUES ('370', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510106000');
INSERT INTO `control_solution_send_record_item` VALUES ('371', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510162000');
INSERT INTO `control_solution_send_record_item` VALUES ('372', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510162000');
INSERT INTO `control_solution_send_record_item` VALUES ('373', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510164000');
INSERT INTO `control_solution_send_record_item` VALUES ('374', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510164000');
INSERT INTO `control_solution_send_record_item` VALUES ('375', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510222000');
INSERT INTO `control_solution_send_record_item` VALUES ('376', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510222000');
INSERT INTO `control_solution_send_record_item` VALUES ('377', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510224000');
INSERT INTO `control_solution_send_record_item` VALUES ('378', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510224000');
INSERT INTO `control_solution_send_record_item` VALUES ('379', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510282000');
INSERT INTO `control_solution_send_record_item` VALUES ('380', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510282000');
INSERT INTO `control_solution_send_record_item` VALUES ('381', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510284000');
INSERT INTO `control_solution_send_record_item` VALUES ('382', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510284000');
INSERT INTO `control_solution_send_record_item` VALUES ('383', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510342000');
INSERT INTO `control_solution_send_record_item` VALUES ('384', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510342000');
INSERT INTO `control_solution_send_record_item` VALUES ('385', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510344000');
INSERT INTO `control_solution_send_record_item` VALUES ('386', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510344000');
INSERT INTO `control_solution_send_record_item` VALUES ('387', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510402000');
INSERT INTO `control_solution_send_record_item` VALUES ('388', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510403000');
INSERT INTO `control_solution_send_record_item` VALUES ('389', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510404000');
INSERT INTO `control_solution_send_record_item` VALUES ('390', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510404000');
INSERT INTO `control_solution_send_record_item` VALUES ('391', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510462000');
INSERT INTO `control_solution_send_record_item` VALUES ('392', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510462000');
INSERT INTO `control_solution_send_record_item` VALUES ('393', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510464000');
INSERT INTO `control_solution_send_record_item` VALUES ('394', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510465000');
INSERT INTO `control_solution_send_record_item` VALUES ('395', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510522000');
INSERT INTO `control_solution_send_record_item` VALUES ('396', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510522000');
INSERT INTO `control_solution_send_record_item` VALUES ('397', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510524000');
INSERT INTO `control_solution_send_record_item` VALUES ('398', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510524000');
INSERT INTO `control_solution_send_record_item` VALUES ('399', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510582000');
INSERT INTO `control_solution_send_record_item` VALUES ('400', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510583000');
INSERT INTO `control_solution_send_record_item` VALUES ('401', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510584000');
INSERT INTO `control_solution_send_record_item` VALUES ('402', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510585000');
INSERT INTO `control_solution_send_record_item` VALUES ('403', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510642000');
INSERT INTO `control_solution_send_record_item` VALUES ('404', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510642000');
INSERT INTO `control_solution_send_record_item` VALUES ('405', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510644000');
INSERT INTO `control_solution_send_record_item` VALUES ('406', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510644000');
INSERT INTO `control_solution_send_record_item` VALUES ('407', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510704000');
INSERT INTO `control_solution_send_record_item` VALUES ('408', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510704000');
INSERT INTO `control_solution_send_record_item` VALUES ('409', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510704000');
INSERT INTO `control_solution_send_record_item` VALUES ('410', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510704000');
INSERT INTO `control_solution_send_record_item` VALUES ('411', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510763000');
INSERT INTO `control_solution_send_record_item` VALUES ('412', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510763000');
INSERT INTO `control_solution_send_record_item` VALUES ('413', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510764000');
INSERT INTO `control_solution_send_record_item` VALUES ('414', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510764000');
INSERT INTO `control_solution_send_record_item` VALUES ('415', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510822000');
INSERT INTO `control_solution_send_record_item` VALUES ('416', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510822000');
INSERT INTO `control_solution_send_record_item` VALUES ('417', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510824000');
INSERT INTO `control_solution_send_record_item` VALUES ('418', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510824000');
INSERT INTO `control_solution_send_record_item` VALUES ('419', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510882000');
INSERT INTO `control_solution_send_record_item` VALUES ('420', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510883000');
INSERT INTO `control_solution_send_record_item` VALUES ('421', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510885000');
INSERT INTO `control_solution_send_record_item` VALUES ('422', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510885000');
INSERT INTO `control_solution_send_record_item` VALUES ('423', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510943000');
INSERT INTO `control_solution_send_record_item` VALUES ('424', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510943000');
INSERT INTO `control_solution_send_record_item` VALUES ('425', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481510944000');
INSERT INTO `control_solution_send_record_item` VALUES ('426', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481510944000');
INSERT INTO `control_solution_send_record_item` VALUES ('427', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511002000');
INSERT INTO `control_solution_send_record_item` VALUES ('428', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511003000');
INSERT INTO `control_solution_send_record_item` VALUES ('429', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511004000');
INSERT INTO `control_solution_send_record_item` VALUES ('430', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511004000');
INSERT INTO `control_solution_send_record_item` VALUES ('431', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511062000');
INSERT INTO `control_solution_send_record_item` VALUES ('432', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511062000');
INSERT INTO `control_solution_send_record_item` VALUES ('433', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511064000');
INSERT INTO `control_solution_send_record_item` VALUES ('434', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511065000');
INSERT INTO `control_solution_send_record_item` VALUES ('435', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511123000');
INSERT INTO `control_solution_send_record_item` VALUES ('436', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511123000');
INSERT INTO `control_solution_send_record_item` VALUES ('437', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511124000');
INSERT INTO `control_solution_send_record_item` VALUES ('438', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511124000');
INSERT INTO `control_solution_send_record_item` VALUES ('439', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511183000');
INSERT INTO `control_solution_send_record_item` VALUES ('440', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511183000');
INSERT INTO `control_solution_send_record_item` VALUES ('441', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511184000');
INSERT INTO `control_solution_send_record_item` VALUES ('442', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511185000');
INSERT INTO `control_solution_send_record_item` VALUES ('443', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511242000');
INSERT INTO `control_solution_send_record_item` VALUES ('444', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511243000');
INSERT INTO `control_solution_send_record_item` VALUES ('445', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511244000');
INSERT INTO `control_solution_send_record_item` VALUES ('446', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511244000');
INSERT INTO `control_solution_send_record_item` VALUES ('447', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511303000');
INSERT INTO `control_solution_send_record_item` VALUES ('448', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511303000');
INSERT INTO `control_solution_send_record_item` VALUES ('449', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511304000');
INSERT INTO `control_solution_send_record_item` VALUES ('450', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511305000');
INSERT INTO `control_solution_send_record_item` VALUES ('451', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511363000');
INSERT INTO `control_solution_send_record_item` VALUES ('452', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511363000');
INSERT INTO `control_solution_send_record_item` VALUES ('453', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511364000');
INSERT INTO `control_solution_send_record_item` VALUES ('454', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511365000');
INSERT INTO `control_solution_send_record_item` VALUES ('455', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511423000');
INSERT INTO `control_solution_send_record_item` VALUES ('456', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511423000');
INSERT INTO `control_solution_send_record_item` VALUES ('457', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511424000');
INSERT INTO `control_solution_send_record_item` VALUES ('458', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511425000');
INSERT INTO `control_solution_send_record_item` VALUES ('459', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511438000');
INSERT INTO `control_solution_send_record_item` VALUES ('460', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511445000');
INSERT INTO `control_solution_send_record_item` VALUES ('461', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511447000');
INSERT INTO `control_solution_send_record_item` VALUES ('462', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511447000');
INSERT INTO `control_solution_send_record_item` VALUES ('463', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511463000');
INSERT INTO `control_solution_send_record_item` VALUES ('464', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511465000');
INSERT INTO `control_solution_send_record_item` VALUES ('465', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511484000');
INSERT INTO `control_solution_send_record_item` VALUES ('466', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511484000');
INSERT INTO `control_solution_send_record_item` VALUES ('467', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511544000');
INSERT INTO `control_solution_send_record_item` VALUES ('468', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511544000');
INSERT INTO `control_solution_send_record_item` VALUES ('469', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511604000');
INSERT INTO `control_solution_send_record_item` VALUES ('470', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511604000');
INSERT INTO `control_solution_send_record_item` VALUES ('471', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511664000');
INSERT INTO `control_solution_send_record_item` VALUES ('472', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511664000');
INSERT INTO `control_solution_send_record_item` VALUES ('473', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511724000');
INSERT INTO `control_solution_send_record_item` VALUES ('474', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511724000');
INSERT INTO `control_solution_send_record_item` VALUES ('475', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511784000');
INSERT INTO `control_solution_send_record_item` VALUES ('476', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511785000');
INSERT INTO `control_solution_send_record_item` VALUES ('477', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511844000');
INSERT INTO `control_solution_send_record_item` VALUES ('478', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511844000');
INSERT INTO `control_solution_send_record_item` VALUES ('479', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511904000');
INSERT INTO `control_solution_send_record_item` VALUES ('480', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511904000');
INSERT INTO `control_solution_send_record_item` VALUES ('481', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481511964000');
INSERT INTO `control_solution_send_record_item` VALUES ('482', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481511964000');
INSERT INTO `control_solution_send_record_item` VALUES ('483', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512025000');
INSERT INTO `control_solution_send_record_item` VALUES ('484', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512025000');
INSERT INTO `control_solution_send_record_item` VALUES ('485', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512084000');
INSERT INTO `control_solution_send_record_item` VALUES ('486', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512085000');
INSERT INTO `control_solution_send_record_item` VALUES ('487', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512144000');
INSERT INTO `control_solution_send_record_item` VALUES ('488', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512144000');
INSERT INTO `control_solution_send_record_item` VALUES ('489', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512204000');
INSERT INTO `control_solution_send_record_item` VALUES ('490', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512204000');
INSERT INTO `control_solution_send_record_item` VALUES ('491', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512264000');
INSERT INTO `control_solution_send_record_item` VALUES ('492', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512264000');
INSERT INTO `control_solution_send_record_item` VALUES ('493', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512325000');
INSERT INTO `control_solution_send_record_item` VALUES ('494', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512325000');
INSERT INTO `control_solution_send_record_item` VALUES ('495', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512385000');
INSERT INTO `control_solution_send_record_item` VALUES ('496', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512385000');
INSERT INTO `control_solution_send_record_item` VALUES ('497', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512444000');
INSERT INTO `control_solution_send_record_item` VALUES ('498', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512444000');
INSERT INTO `control_solution_send_record_item` VALUES ('499', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512504000');
INSERT INTO `control_solution_send_record_item` VALUES ('500', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512504000');
INSERT INTO `control_solution_send_record_item` VALUES ('501', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512567000');
INSERT INTO `control_solution_send_record_item` VALUES ('502', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512568000');
INSERT INTO `control_solution_send_record_item` VALUES ('503', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512625000');
INSERT INTO `control_solution_send_record_item` VALUES ('504', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512625000');
INSERT INTO `control_solution_send_record_item` VALUES ('505', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512686000');
INSERT INTO `control_solution_send_record_item` VALUES ('506', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512686000');
INSERT INTO `control_solution_send_record_item` VALUES ('507', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512744000');
INSERT INTO `control_solution_send_record_item` VALUES ('508', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512744000');
INSERT INTO `control_solution_send_record_item` VALUES ('509', 'CST9CBH3', 'EQUJGI8', '[{\"duration\":444,\"sendContent\":\"wer\"},{\"duration\":66,\"sendContent\":\"sdf\"}]', '1', '1481512804000');
INSERT INTO `control_solution_send_record_item` VALUES ('510', 'CST9CBH3', 'EQX7KTS', '[{\"duration\":44,\"sendContent\":\"123\"},{\"duration\":66,\"sendContent\":\"55555\"}]', '1', '1481512805000');
INSERT INTO `control_solution_send_record_item` VALUES ('511', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513102000');
INSERT INTO `control_solution_send_record_item` VALUES ('512', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513105000');
INSERT INTO `control_solution_send_record_item` VALUES ('513', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513164000');
INSERT INTO `control_solution_send_record_item` VALUES ('514', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513166000');
INSERT INTO `control_solution_send_record_item` VALUES ('515', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513222000');
INSERT INTO `control_solution_send_record_item` VALUES ('516', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513224000');
INSERT INTO `control_solution_send_record_item` VALUES ('517', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513282000');
INSERT INTO `control_solution_send_record_item` VALUES ('518', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513284000');
INSERT INTO `control_solution_send_record_item` VALUES ('519', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513342000');
INSERT INTO `control_solution_send_record_item` VALUES ('520', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513344000');
INSERT INTO `control_solution_send_record_item` VALUES ('521', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513403000');
INSERT INTO `control_solution_send_record_item` VALUES ('522', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513405000');
INSERT INTO `control_solution_send_record_item` VALUES ('523', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513462000');
INSERT INTO `control_solution_send_record_item` VALUES ('524', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513464000');
INSERT INTO `control_solution_send_record_item` VALUES ('525', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513522000');
INSERT INTO `control_solution_send_record_item` VALUES ('526', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513524000');
INSERT INTO `control_solution_send_record_item` VALUES ('527', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513582000');
INSERT INTO `control_solution_send_record_item` VALUES ('528', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513584000');
INSERT INTO `control_solution_send_record_item` VALUES ('529', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513642000');
INSERT INTO `control_solution_send_record_item` VALUES ('530', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513644000');
INSERT INTO `control_solution_send_record_item` VALUES ('531', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513702000');
INSERT INTO `control_solution_send_record_item` VALUES ('532', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513704000');
INSERT INTO `control_solution_send_record_item` VALUES ('533', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513762000');
INSERT INTO `control_solution_send_record_item` VALUES ('534', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513765000');
INSERT INTO `control_solution_send_record_item` VALUES ('535', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513822000');
INSERT INTO `control_solution_send_record_item` VALUES ('536', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513825000');
INSERT INTO `control_solution_send_record_item` VALUES ('537', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513882000');
INSERT INTO `control_solution_send_record_item` VALUES ('538', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513884000');
INSERT INTO `control_solution_send_record_item` VALUES ('539', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481513942000');
INSERT INTO `control_solution_send_record_item` VALUES ('540', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481513944000');
INSERT INTO `control_solution_send_record_item` VALUES ('541', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514002000');
INSERT INTO `control_solution_send_record_item` VALUES ('542', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514004000');
INSERT INTO `control_solution_send_record_item` VALUES ('543', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514062000');
INSERT INTO `control_solution_send_record_item` VALUES ('544', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514064000');
INSERT INTO `control_solution_send_record_item` VALUES ('545', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514122000');
INSERT INTO `control_solution_send_record_item` VALUES ('546', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514124000');
INSERT INTO `control_solution_send_record_item` VALUES ('547', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514182000');
INSERT INTO `control_solution_send_record_item` VALUES ('548', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514184000');
INSERT INTO `control_solution_send_record_item` VALUES ('549', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514243000');
INSERT INTO `control_solution_send_record_item` VALUES ('550', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514245000');
INSERT INTO `control_solution_send_record_item` VALUES ('551', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514302000');
INSERT INTO `control_solution_send_record_item` VALUES ('552', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514304000');
INSERT INTO `control_solution_send_record_item` VALUES ('553', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514362000');
INSERT INTO `control_solution_send_record_item` VALUES ('554', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514364000');
INSERT INTO `control_solution_send_record_item` VALUES ('555', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514422000');
INSERT INTO `control_solution_send_record_item` VALUES ('556', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514424000');
INSERT INTO `control_solution_send_record_item` VALUES ('557', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514482000');
INSERT INTO `control_solution_send_record_item` VALUES ('558', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514484000');
INSERT INTO `control_solution_send_record_item` VALUES ('559', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514544000');
INSERT INTO `control_solution_send_record_item` VALUES ('560', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514546000');
INSERT INTO `control_solution_send_record_item` VALUES ('561', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514602000');
INSERT INTO `control_solution_send_record_item` VALUES ('562', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514605000');
INSERT INTO `control_solution_send_record_item` VALUES ('563', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514662000');
INSERT INTO `control_solution_send_record_item` VALUES ('564', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514664000');
INSERT INTO `control_solution_send_record_item` VALUES ('565', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514722000');
INSERT INTO `control_solution_send_record_item` VALUES ('566', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514724000');
INSERT INTO `control_solution_send_record_item` VALUES ('567', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514782000');
INSERT INTO `control_solution_send_record_item` VALUES ('568', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514784000');
INSERT INTO `control_solution_send_record_item` VALUES ('569', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514844000');
INSERT INTO `control_solution_send_record_item` VALUES ('570', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514846000');
INSERT INTO `control_solution_send_record_item` VALUES ('571', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514902000');
INSERT INTO `control_solution_send_record_item` VALUES ('572', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514904000');
INSERT INTO `control_solution_send_record_item` VALUES ('573', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481514962000');
INSERT INTO `control_solution_send_record_item` VALUES ('574', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481514964000');
INSERT INTO `control_solution_send_record_item` VALUES ('575', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515022000');
INSERT INTO `control_solution_send_record_item` VALUES ('576', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515024000');
INSERT INTO `control_solution_send_record_item` VALUES ('577', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515082000');
INSERT INTO `control_solution_send_record_item` VALUES ('578', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515084000');
INSERT INTO `control_solution_send_record_item` VALUES ('579', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515143000');
INSERT INTO `control_solution_send_record_item` VALUES ('580', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515145000');
INSERT INTO `control_solution_send_record_item` VALUES ('581', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515202000');
INSERT INTO `control_solution_send_record_item` VALUES ('582', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515204000');
INSERT INTO `control_solution_send_record_item` VALUES ('583', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515263000');
INSERT INTO `control_solution_send_record_item` VALUES ('584', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515265000');
INSERT INTO `control_solution_send_record_item` VALUES ('585', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515322000');
INSERT INTO `control_solution_send_record_item` VALUES ('586', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515324000');
INSERT INTO `control_solution_send_record_item` VALUES ('587', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515382000');
INSERT INTO `control_solution_send_record_item` VALUES ('588', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515384000');
INSERT INTO `control_solution_send_record_item` VALUES ('589', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515443000');
INSERT INTO `control_solution_send_record_item` VALUES ('590', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515445000');
INSERT INTO `control_solution_send_record_item` VALUES ('591', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515503000');
INSERT INTO `control_solution_send_record_item` VALUES ('592', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515505000');
INSERT INTO `control_solution_send_record_item` VALUES ('593', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515563000');
INSERT INTO `control_solution_send_record_item` VALUES ('594', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515565000');
INSERT INTO `control_solution_send_record_item` VALUES ('595', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515576000');
INSERT INTO `control_solution_send_record_item` VALUES ('596', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515588000');
INSERT INTO `control_solution_send_record_item` VALUES ('597', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515591000');
INSERT INTO `control_solution_send_record_item` VALUES ('598', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515600000');
INSERT INTO `control_solution_send_record_item` VALUES ('599', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515622000');
INSERT INTO `control_solution_send_record_item` VALUES ('600', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515625000');
INSERT INTO `control_solution_send_record_item` VALUES ('601', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515637000');
INSERT INTO `control_solution_send_record_item` VALUES ('602', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515644000');
INSERT INTO `control_solution_send_record_item` VALUES ('603', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515684000');
INSERT INTO `control_solution_send_record_item` VALUES ('604', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515687000');
INSERT INTO `control_solution_send_record_item` VALUES ('605', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515701000');
INSERT INTO `control_solution_send_record_item` VALUES ('606', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515708000');
INSERT INTO `control_solution_send_record_item` VALUES ('607', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515744000');
INSERT INTO `control_solution_send_record_item` VALUES ('608', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515746000');
INSERT INTO `control_solution_send_record_item` VALUES ('609', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515758000');
INSERT INTO `control_solution_send_record_item` VALUES ('610', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515764000');
INSERT INTO `control_solution_send_record_item` VALUES ('611', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515802000');
INSERT INTO `control_solution_send_record_item` VALUES ('612', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515804000');
INSERT INTO `control_solution_send_record_item` VALUES ('613', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515824000');
INSERT INTO `control_solution_send_record_item` VALUES ('614', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515838000');
INSERT INTO `control_solution_send_record_item` VALUES ('615', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515863000');
INSERT INTO `control_solution_send_record_item` VALUES ('616', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515865000');
INSERT INTO `control_solution_send_record_item` VALUES ('617', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515924000');
INSERT INTO `control_solution_send_record_item` VALUES ('618', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515926000');
INSERT INTO `control_solution_send_record_item` VALUES ('619', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481515982000');
INSERT INTO `control_solution_send_record_item` VALUES ('620', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481515984000');
INSERT INTO `control_solution_send_record_item` VALUES ('621', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516044000');
INSERT INTO `control_solution_send_record_item` VALUES ('622', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516046000');
INSERT INTO `control_solution_send_record_item` VALUES ('623', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516090000');
INSERT INTO `control_solution_send_record_item` VALUES ('624', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516102000');
INSERT INTO `control_solution_send_record_item` VALUES ('625', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516105000');
INSERT INTO `control_solution_send_record_item` VALUES ('626', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516110000');
INSERT INTO `control_solution_send_record_item` VALUES ('627', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516162000');
INSERT INTO `control_solution_send_record_item` VALUES ('628', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516164000');
INSERT INTO `control_solution_send_record_item` VALUES ('629', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516168000');
INSERT INTO `control_solution_send_record_item` VALUES ('630', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516222000');
INSERT INTO `control_solution_send_record_item` VALUES ('631', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516224000');
INSERT INTO `control_solution_send_record_item` VALUES ('632', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516282000');
INSERT INTO `control_solution_send_record_item` VALUES ('633', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516284000');
INSERT INTO `control_solution_send_record_item` VALUES ('634', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516346000');
INSERT INTO `control_solution_send_record_item` VALUES ('635', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516349000');
INSERT INTO `control_solution_send_record_item` VALUES ('636', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516402000');
INSERT INTO `control_solution_send_record_item` VALUES ('637', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516404000');
INSERT INTO `control_solution_send_record_item` VALUES ('638', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516462000');
INSERT INTO `control_solution_send_record_item` VALUES ('639', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516464000');
INSERT INTO `control_solution_send_record_item` VALUES ('640', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516522000');
INSERT INTO `control_solution_send_record_item` VALUES ('641', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516524000');
INSERT INTO `control_solution_send_record_item` VALUES ('642', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516582000');
INSERT INTO `control_solution_send_record_item` VALUES ('643', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516584000');
INSERT INTO `control_solution_send_record_item` VALUES ('644', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516642000');
INSERT INTO `control_solution_send_record_item` VALUES ('645', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516644000');
INSERT INTO `control_solution_send_record_item` VALUES ('646', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516707000');
INSERT INTO `control_solution_send_record_item` VALUES ('647', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516710000');
INSERT INTO `control_solution_send_record_item` VALUES ('648', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516767000');
INSERT INTO `control_solution_send_record_item` VALUES ('649', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516770000');
INSERT INTO `control_solution_send_record_item` VALUES ('650', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516790000');
INSERT INTO `control_solution_send_record_item` VALUES ('651', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516827000');
INSERT INTO `control_solution_send_record_item` VALUES ('652', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516830000');
INSERT INTO `control_solution_send_record_item` VALUES ('653', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":3,\"sendContent\":\"这个是是向上的颜色为蓝色\"}]', '1', '1481516870000');
INSERT INTO `control_solution_send_record_item` VALUES ('654', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516882000');
INSERT INTO `control_solution_send_record_item` VALUES ('655', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516885000');
INSERT INTO `control_solution_send_record_item` VALUES ('656', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516914000');
INSERT INTO `control_solution_send_record_item` VALUES ('657', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516942000');
INSERT INTO `control_solution_send_record_item` VALUES ('658', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481516942000');
INSERT INTO `control_solution_send_record_item` VALUES ('659', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516944000');
INSERT INTO `control_solution_send_record_item` VALUES ('660', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516951000');
INSERT INTO `control_solution_send_record_item` VALUES ('661', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516958000');
INSERT INTO `control_solution_send_record_item` VALUES ('662', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516969000');
INSERT INTO `control_solution_send_record_item` VALUES ('663', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481516975000');
INSERT INTO `control_solution_send_record_item` VALUES ('664', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517005000');
INSERT INTO `control_solution_send_record_item` VALUES ('665', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517008000');
INSERT INTO `control_solution_send_record_item` VALUES ('666', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517062000');
INSERT INTO `control_solution_send_record_item` VALUES ('667', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517065000');
INSERT INTO `control_solution_send_record_item` VALUES ('668', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517122000');
INSERT INTO `control_solution_send_record_item` VALUES ('669', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517124000');
INSERT INTO `control_solution_send_record_item` VALUES ('670', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517182000');
INSERT INTO `control_solution_send_record_item` VALUES ('671', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517184000');
INSERT INTO `control_solution_send_record_item` VALUES ('672', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517247000');
INSERT INTO `control_solution_send_record_item` VALUES ('673', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517250000');
INSERT INTO `control_solution_send_record_item` VALUES ('674', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517283000');
INSERT INTO `control_solution_send_record_item` VALUES ('675', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517302000');
INSERT INTO `control_solution_send_record_item` VALUES ('676', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517304000');
INSERT INTO `control_solution_send_record_item` VALUES ('677', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517330000');
INSERT INTO `control_solution_send_record_item` VALUES ('678', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517339000');
INSERT INTO `control_solution_send_record_item` VALUES ('679', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517346000');
INSERT INTO `control_solution_send_record_item` VALUES ('680', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517362000');
INSERT INTO `control_solution_send_record_item` VALUES ('681', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517365000');
INSERT INTO `control_solution_send_record_item` VALUES ('682', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517383000');
INSERT INTO `control_solution_send_record_item` VALUES ('683', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517389000');
INSERT INTO `control_solution_send_record_item` VALUES ('684', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517422000');
INSERT INTO `control_solution_send_record_item` VALUES ('685', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517424000');
INSERT INTO `control_solution_send_record_item` VALUES ('686', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517443000');
INSERT INTO `control_solution_send_record_item` VALUES ('687', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517449000');
INSERT INTO `control_solution_send_record_item` VALUES ('688', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"}]', '1', '1481517482000');
INSERT INTO `control_solution_send_record_item` VALUES ('689', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517484000');
INSERT INTO `control_solution_send_record_item` VALUES ('690', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517503000');
INSERT INTO `control_solution_send_record_item` VALUES ('691', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517509000');
INSERT INTO `control_solution_send_record_item` VALUES ('692', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517544000');
INSERT INTO `control_solution_send_record_item` VALUES ('693', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517546000');
INSERT INTO `control_solution_send_record_item` VALUES ('694', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517603000');
INSERT INTO `control_solution_send_record_item` VALUES ('695', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517605000');
INSERT INTO `control_solution_send_record_item` VALUES ('696', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517665000');
INSERT INTO `control_solution_send_record_item` VALUES ('697', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517667000');
INSERT INTO `control_solution_send_record_item` VALUES ('698', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517727000');
INSERT INTO `control_solution_send_record_item` VALUES ('699', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517730000');
INSERT INTO `control_solution_send_record_item` VALUES ('700', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517783000');
INSERT INTO `control_solution_send_record_item` VALUES ('701', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517785000');
INSERT INTO `control_solution_send_record_item` VALUES ('702', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517842000');
INSERT INTO `control_solution_send_record_item` VALUES ('703', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517844000');
INSERT INTO `control_solution_send_record_item` VALUES ('704', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517902000');
INSERT INTO `control_solution_send_record_item` VALUES ('705', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517904000');
INSERT INTO `control_solution_send_record_item` VALUES ('706', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481517962000');
INSERT INTO `control_solution_send_record_item` VALUES ('707', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481517964000');
INSERT INTO `control_solution_send_record_item` VALUES ('708', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518022000');
INSERT INTO `control_solution_send_record_item` VALUES ('709', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518024000');
INSERT INTO `control_solution_send_record_item` VALUES ('710', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518082000');
INSERT INTO `control_solution_send_record_item` VALUES ('711', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518084000');
INSERT INTO `control_solution_send_record_item` VALUES ('712', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518145000');
INSERT INTO `control_solution_send_record_item` VALUES ('713', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518147000');
INSERT INTO `control_solution_send_record_item` VALUES ('714', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518203000');
INSERT INTO `control_solution_send_record_item` VALUES ('715', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518205000');
INSERT INTO `control_solution_send_record_item` VALUES ('716', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518267000');
INSERT INTO `control_solution_send_record_item` VALUES ('717', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518270000');
INSERT INTO `control_solution_send_record_item` VALUES ('718', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518329000');
INSERT INTO `control_solution_send_record_item` VALUES ('719', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518332000');
INSERT INTO `control_solution_send_record_item` VALUES ('720', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518386000');
INSERT INTO `control_solution_send_record_item` VALUES ('721', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518390000');
INSERT INTO `control_solution_send_record_item` VALUES ('722', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518448000');
INSERT INTO `control_solution_send_record_item` VALUES ('723', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518450000');
INSERT INTO `control_solution_send_record_item` VALUES ('724', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518507000');
INSERT INTO `control_solution_send_record_item` VALUES ('725', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518510000');
INSERT INTO `control_solution_send_record_item` VALUES ('726', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518563000');
INSERT INTO `control_solution_send_record_item` VALUES ('727', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518565000');
INSERT INTO `control_solution_send_record_item` VALUES ('728', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518623000');
INSERT INTO `control_solution_send_record_item` VALUES ('729', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518625000');
INSERT INTO `control_solution_send_record_item` VALUES ('730', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518686000');
INSERT INTO `control_solution_send_record_item` VALUES ('731', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518689000');
INSERT INTO `control_solution_send_record_item` VALUES ('732', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518744000');
INSERT INTO `control_solution_send_record_item` VALUES ('733', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518747000');
INSERT INTO `control_solution_send_record_item` VALUES ('734', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518803000');
INSERT INTO `control_solution_send_record_item` VALUES ('735', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518805000');
INSERT INTO `control_solution_send_record_item` VALUES ('736', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518862000');
INSERT INTO `control_solution_send_record_item` VALUES ('737', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518864000');
INSERT INTO `control_solution_send_record_item` VALUES ('738', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518924000');
INSERT INTO `control_solution_send_record_item` VALUES ('739', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518926000');
INSERT INTO `control_solution_send_record_item` VALUES ('740', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481518983000');
INSERT INTO `control_solution_send_record_item` VALUES ('741', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481518986000');
INSERT INTO `control_solution_send_record_item` VALUES ('742', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519043000');
INSERT INTO `control_solution_send_record_item` VALUES ('743', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519045000');
INSERT INTO `control_solution_send_record_item` VALUES ('744', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519102000');
INSERT INTO `control_solution_send_record_item` VALUES ('745', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519104000');
INSERT INTO `control_solution_send_record_item` VALUES ('746', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519162000');
INSERT INTO `control_solution_send_record_item` VALUES ('747', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519164000');
INSERT INTO `control_solution_send_record_item` VALUES ('748', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519222000');
INSERT INTO `control_solution_send_record_item` VALUES ('749', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519224000');
INSERT INTO `control_solution_send_record_item` VALUES ('750', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519282000');
INSERT INTO `control_solution_send_record_item` VALUES ('751', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519284000');
INSERT INTO `control_solution_send_record_item` VALUES ('752', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519343000');
INSERT INTO `control_solution_send_record_item` VALUES ('753', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519345000');
INSERT INTO `control_solution_send_record_item` VALUES ('754', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519402000');
INSERT INTO `control_solution_send_record_item` VALUES ('755', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519405000');
INSERT INTO `control_solution_send_record_item` VALUES ('756', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519463000');
INSERT INTO `control_solution_send_record_item` VALUES ('757', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519465000');
INSERT INTO `control_solution_send_record_item` VALUES ('758', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519522000');
INSERT INTO `control_solution_send_record_item` VALUES ('759', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519525000');
INSERT INTO `control_solution_send_record_item` VALUES ('760', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519586000');
INSERT INTO `control_solution_send_record_item` VALUES ('761', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519589000');
INSERT INTO `control_solution_send_record_item` VALUES ('762', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519642000');
INSERT INTO `control_solution_send_record_item` VALUES ('763', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519645000');
INSERT INTO `control_solution_send_record_item` VALUES ('764', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519702000');
INSERT INTO `control_solution_send_record_item` VALUES ('765', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519705000');
INSERT INTO `control_solution_send_record_item` VALUES ('766', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519766000');
INSERT INTO `control_solution_send_record_item` VALUES ('767', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519768000');
INSERT INTO `control_solution_send_record_item` VALUES ('768', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519824000');
INSERT INTO `control_solution_send_record_item` VALUES ('769', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519827000');
INSERT INTO `control_solution_send_record_item` VALUES ('770', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519883000');
INSERT INTO `control_solution_send_record_item` VALUES ('771', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519885000');
INSERT INTO `control_solution_send_record_item` VALUES ('772', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481519944000');
INSERT INTO `control_solution_send_record_item` VALUES ('773', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481519947000');
INSERT INTO `control_solution_send_record_item` VALUES ('774', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520003000');
INSERT INTO `control_solution_send_record_item` VALUES ('775', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520005000');
INSERT INTO `control_solution_send_record_item` VALUES ('776', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520063000');
INSERT INTO `control_solution_send_record_item` VALUES ('777', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520066000');
INSERT INTO `control_solution_send_record_item` VALUES ('778', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520122000');
INSERT INTO `control_solution_send_record_item` VALUES ('779', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520125000');
INSERT INTO `control_solution_send_record_item` VALUES ('780', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520183000');
INSERT INTO `control_solution_send_record_item` VALUES ('781', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520186000');
INSERT INTO `control_solution_send_record_item` VALUES ('782', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520246000');
INSERT INTO `control_solution_send_record_item` VALUES ('783', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520249000');
INSERT INTO `control_solution_send_record_item` VALUES ('784', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520304000');
INSERT INTO `control_solution_send_record_item` VALUES ('785', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520307000');
INSERT INTO `control_solution_send_record_item` VALUES ('786', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520362000');
INSERT INTO `control_solution_send_record_item` VALUES ('787', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520364000');
INSERT INTO `control_solution_send_record_item` VALUES ('788', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520422000');
INSERT INTO `control_solution_send_record_item` VALUES ('789', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520424000');
INSERT INTO `control_solution_send_record_item` VALUES ('790', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520482000');
INSERT INTO `control_solution_send_record_item` VALUES ('791', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520484000');
INSERT INTO `control_solution_send_record_item` VALUES ('792', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520542000');
INSERT INTO `control_solution_send_record_item` VALUES ('793', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520544000');
INSERT INTO `control_solution_send_record_item` VALUES ('794', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520603000');
INSERT INTO `control_solution_send_record_item` VALUES ('795', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520605000');
INSERT INTO `control_solution_send_record_item` VALUES ('796', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520663000');
INSERT INTO `control_solution_send_record_item` VALUES ('797', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520665000');
INSERT INTO `control_solution_send_record_item` VALUES ('798', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520722000');
INSERT INTO `control_solution_send_record_item` VALUES ('799', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520724000');
INSERT INTO `control_solution_send_record_item` VALUES ('800', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520782000');
INSERT INTO `control_solution_send_record_item` VALUES ('801', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520784000');
INSERT INTO `control_solution_send_record_item` VALUES ('802', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520843000');
INSERT INTO `control_solution_send_record_item` VALUES ('803', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520845000');
INSERT INTO `control_solution_send_record_item` VALUES ('804', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520902000');
INSERT INTO `control_solution_send_record_item` VALUES ('805', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520904000');
INSERT INTO `control_solution_send_record_item` VALUES ('806', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481520963000');
INSERT INTO `control_solution_send_record_item` VALUES ('807', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481520965000');
INSERT INTO `control_solution_send_record_item` VALUES ('808', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521024000');
INSERT INTO `control_solution_send_record_item` VALUES ('809', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521026000');
INSERT INTO `control_solution_send_record_item` VALUES ('810', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521082000');
INSERT INTO `control_solution_send_record_item` VALUES ('811', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521084000');
INSERT INTO `control_solution_send_record_item` VALUES ('812', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521110000');
INSERT INTO `control_solution_send_record_item` VALUES ('813', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521113000');
INSERT INTO `control_solution_send_record_item` VALUES ('814', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521143000');
INSERT INTO `control_solution_send_record_item` VALUES ('815', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521145000');
INSERT INTO `control_solution_send_record_item` VALUES ('816', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521205000');
INSERT INTO `control_solution_send_record_item` VALUES ('817', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521207000');
INSERT INTO `control_solution_send_record_item` VALUES ('818', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521220000');
INSERT INTO `control_solution_send_record_item` VALUES ('819', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521222000');
INSERT INTO `control_solution_send_record_item` VALUES ('820', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521262000');
INSERT INTO `control_solution_send_record_item` VALUES ('821', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521265000');
INSERT INTO `control_solution_send_record_item` VALUES ('822', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521322000');
INSERT INTO `control_solution_send_record_item` VALUES ('823', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521324000');
INSERT INTO `control_solution_send_record_item` VALUES ('824', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521382000');
INSERT INTO `control_solution_send_record_item` VALUES ('825', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521384000');
INSERT INTO `control_solution_send_record_item` VALUES ('826', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521443000');
INSERT INTO `control_solution_send_record_item` VALUES ('827', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521446000');
INSERT INTO `control_solution_send_record_item` VALUES ('828', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521502000');
INSERT INTO `control_solution_send_record_item` VALUES ('829', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521504000');
INSERT INTO `control_solution_send_record_item` VALUES ('830', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521520000');
INSERT INTO `control_solution_send_record_item` VALUES ('831', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521526000');
INSERT INTO `control_solution_send_record_item` VALUES ('832', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521534000');
INSERT INTO `control_solution_send_record_item` VALUES ('833', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521536000');
INSERT INTO `control_solution_send_record_item` VALUES ('834', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521563000');
INSERT INTO `control_solution_send_record_item` VALUES ('835', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521565000');
INSERT INTO `control_solution_send_record_item` VALUES ('836', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521586000');
INSERT INTO `control_solution_send_record_item` VALUES ('837', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521623000');
INSERT INTO `control_solution_send_record_item` VALUES ('838', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521625000');
INSERT INTO `control_solution_send_record_item` VALUES ('839', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521645000');
INSERT INTO `control_solution_send_record_item` VALUES ('840', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521683000');
INSERT INTO `control_solution_send_record_item` VALUES ('841', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521685000');
INSERT INTO `control_solution_send_record_item` VALUES ('842', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521742000');
INSERT INTO `control_solution_send_record_item` VALUES ('843', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521745000');
INSERT INTO `control_solution_send_record_item` VALUES ('844', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521765000');
INSERT INTO `control_solution_send_record_item` VALUES ('845', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521772000');
INSERT INTO `control_solution_send_record_item` VALUES ('846', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521803000');
INSERT INTO `control_solution_send_record_item` VALUES ('847', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521807000');
INSERT INTO `control_solution_send_record_item` VALUES ('848', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521823000');
INSERT INTO `control_solution_send_record_item` VALUES ('849', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521830000');
INSERT INTO `control_solution_send_record_item` VALUES ('850', 'CSTJCU49', 'EQRER2C', '[{\"duration\":20,\"sendContent\":\"这个是红色的卫向左\"},{\"duration\":33,\"sendContent\":\"这个是新添加的\"}]', '1', '1481521862000');
INSERT INTO `control_solution_send_record_item` VALUES ('851', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521864000');
INSERT INTO `control_solution_send_record_item` VALUES ('852', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521883000');
INSERT INTO `control_solution_send_record_item` VALUES ('853', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521889000');
INSERT INTO `control_solution_send_record_item` VALUES ('854', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521925000');
INSERT INTO `control_solution_send_record_item` VALUES ('855', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521943000');
INSERT INTO `control_solution_send_record_item` VALUES ('856', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521950000');
INSERT INTO `control_solution_send_record_item` VALUES ('857', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481521985000');
INSERT INTO `control_solution_send_record_item` VALUES ('858', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522003000');
INSERT INTO `control_solution_send_record_item` VALUES ('859', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522010000');
INSERT INTO `control_solution_send_record_item` VALUES ('860', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522045000');
INSERT INTO `control_solution_send_record_item` VALUES ('861', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522063000');
INSERT INTO `control_solution_send_record_item` VALUES ('862', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522070000');
INSERT INTO `control_solution_send_record_item` VALUES ('863', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522108000');
INSERT INTO `control_solution_send_record_item` VALUES ('864', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522128000');
INSERT INTO `control_solution_send_record_item` VALUES ('865', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522135000');
INSERT INTO `control_solution_send_record_item` VALUES ('866', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522164000');
INSERT INTO `control_solution_send_record_item` VALUES ('867', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522224000');
INSERT INTO `control_solution_send_record_item` VALUES ('868', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522284000');
INSERT INTO `control_solution_send_record_item` VALUES ('869', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522344000');
INSERT INTO `control_solution_send_record_item` VALUES ('870', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522404000');
INSERT INTO `control_solution_send_record_item` VALUES ('871', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522466000');
INSERT INTO `control_solution_send_record_item` VALUES ('872', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522525000');
INSERT INTO `control_solution_send_record_item` VALUES ('873', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522584000');
INSERT INTO `control_solution_send_record_item` VALUES ('874', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522644000');
INSERT INTO `control_solution_send_record_item` VALUES ('875', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522704000');
INSERT INTO `control_solution_send_record_item` VALUES ('876', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522765000');
INSERT INTO `control_solution_send_record_item` VALUES ('877', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522824000');
INSERT INTO `control_solution_send_record_item` VALUES ('878', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522885000');
INSERT INTO `control_solution_send_record_item` VALUES ('879', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481522945000');
INSERT INTO `control_solution_send_record_item` VALUES ('880', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523005000');
INSERT INTO `control_solution_send_record_item` VALUES ('881', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523065000');
INSERT INTO `control_solution_send_record_item` VALUES ('882', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523125000');
INSERT INTO `control_solution_send_record_item` VALUES ('883', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523185000');
INSERT INTO `control_solution_send_record_item` VALUES ('884', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523245000');
INSERT INTO `control_solution_send_record_item` VALUES ('885', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523305000');
INSERT INTO `control_solution_send_record_item` VALUES ('886', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523364000');
INSERT INTO `control_solution_send_record_item` VALUES ('887', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523424000');
INSERT INTO `control_solution_send_record_item` VALUES ('888', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523484000');
INSERT INTO `control_solution_send_record_item` VALUES ('889', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523544000');
INSERT INTO `control_solution_send_record_item` VALUES ('890', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523604000');
INSERT INTO `control_solution_send_record_item` VALUES ('891', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523665000');
INSERT INTO `control_solution_send_record_item` VALUES ('892', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523724000');
INSERT INTO `control_solution_send_record_item` VALUES ('893', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523785000');
INSERT INTO `control_solution_send_record_item` VALUES ('894', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523845000');
INSERT INTO `control_solution_send_record_item` VALUES ('895', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523904000');
INSERT INTO `control_solution_send_record_item` VALUES ('896', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481523965000');
INSERT INTO `control_solution_send_record_item` VALUES ('897', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524024000');
INSERT INTO `control_solution_send_record_item` VALUES ('898', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524085000');
INSERT INTO `control_solution_send_record_item` VALUES ('899', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524145000');
INSERT INTO `control_solution_send_record_item` VALUES ('900', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524205000');
INSERT INTO `control_solution_send_record_item` VALUES ('901', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524265000');
INSERT INTO `control_solution_send_record_item` VALUES ('902', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524324000');
INSERT INTO `control_solution_send_record_item` VALUES ('903', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524384000');
INSERT INTO `control_solution_send_record_item` VALUES ('904', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524444000');
INSERT INTO `control_solution_send_record_item` VALUES ('905', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524505000');
INSERT INTO `control_solution_send_record_item` VALUES ('906', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524565000');
INSERT INTO `control_solution_send_record_item` VALUES ('907', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524624000');
INSERT INTO `control_solution_send_record_item` VALUES ('908', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524685000');
INSERT INTO `control_solution_send_record_item` VALUES ('909', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524744000');
INSERT INTO `control_solution_send_record_item` VALUES ('910', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524805000');
INSERT INTO `control_solution_send_record_item` VALUES ('911', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524864000');
INSERT INTO `control_solution_send_record_item` VALUES ('912', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524925000');
INSERT INTO `control_solution_send_record_item` VALUES ('913', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481524985000');
INSERT INTO `control_solution_send_record_item` VALUES ('914', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525045000');
INSERT INTO `control_solution_send_record_item` VALUES ('915', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525105000');
INSERT INTO `control_solution_send_record_item` VALUES ('916', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525165000');
INSERT INTO `control_solution_send_record_item` VALUES ('917', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525225000');
INSERT INTO `control_solution_send_record_item` VALUES ('918', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525286000');
INSERT INTO `control_solution_send_record_item` VALUES ('919', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525345000');
INSERT INTO `control_solution_send_record_item` VALUES ('920', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525405000');
INSERT INTO `control_solution_send_record_item` VALUES ('921', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525465000');
INSERT INTO `control_solution_send_record_item` VALUES ('922', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525525000');
INSERT INTO `control_solution_send_record_item` VALUES ('923', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525585000');
INSERT INTO `control_solution_send_record_item` VALUES ('924', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525644000');
INSERT INTO `control_solution_send_record_item` VALUES ('925', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525704000');
INSERT INTO `control_solution_send_record_item` VALUES ('926', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525764000');
INSERT INTO `control_solution_send_record_item` VALUES ('927', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525824000');
INSERT INTO `control_solution_send_record_item` VALUES ('928', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525884000');
INSERT INTO `control_solution_send_record_item` VALUES ('929', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481525945000');
INSERT INTO `control_solution_send_record_item` VALUES ('930', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481525945000');
INSERT INTO `control_solution_send_record_item` VALUES ('931', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526005000');
INSERT INTO `control_solution_send_record_item` VALUES ('932', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526005000');
INSERT INTO `control_solution_send_record_item` VALUES ('933', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526067000');
INSERT INTO `control_solution_send_record_item` VALUES ('934', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526067000');
INSERT INTO `control_solution_send_record_item` VALUES ('935', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526124000');
INSERT INTO `control_solution_send_record_item` VALUES ('936', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526124000');
INSERT INTO `control_solution_send_record_item` VALUES ('937', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526187000');
INSERT INTO `control_solution_send_record_item` VALUES ('938', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526187000');
INSERT INTO `control_solution_send_record_item` VALUES ('939', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526244000');
INSERT INTO `control_solution_send_record_item` VALUES ('940', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526244000');
INSERT INTO `control_solution_send_record_item` VALUES ('941', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526304000');
INSERT INTO `control_solution_send_record_item` VALUES ('942', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526305000');
INSERT INTO `control_solution_send_record_item` VALUES ('943', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526364000');
INSERT INTO `control_solution_send_record_item` VALUES ('944', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526364000');
INSERT INTO `control_solution_send_record_item` VALUES ('945', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526427000');
INSERT INTO `control_solution_send_record_item` VALUES ('946', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526427000');
INSERT INTO `control_solution_send_record_item` VALUES ('947', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526484000');
INSERT INTO `control_solution_send_record_item` VALUES ('948', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526485000');
INSERT INTO `control_solution_send_record_item` VALUES ('949', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526545000');
INSERT INTO `control_solution_send_record_item` VALUES ('950', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526545000');
INSERT INTO `control_solution_send_record_item` VALUES ('951', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526605000');
INSERT INTO `control_solution_send_record_item` VALUES ('952', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526605000');
INSERT INTO `control_solution_send_record_item` VALUES ('953', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526664000');
INSERT INTO `control_solution_send_record_item` VALUES ('954', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526664000');
INSERT INTO `control_solution_send_record_item` VALUES ('955', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526724000');
INSERT INTO `control_solution_send_record_item` VALUES ('956', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526724000');
INSERT INTO `control_solution_send_record_item` VALUES ('957', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526786000');
INSERT INTO `control_solution_send_record_item` VALUES ('958', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526786000');
INSERT INTO `control_solution_send_record_item` VALUES ('959', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526845000');
INSERT INTO `control_solution_send_record_item` VALUES ('960', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526845000');
INSERT INTO `control_solution_send_record_item` VALUES ('961', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526904000');
INSERT INTO `control_solution_send_record_item` VALUES ('962', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526904000');
INSERT INTO `control_solution_send_record_item` VALUES ('963', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481526964000');
INSERT INTO `control_solution_send_record_item` VALUES ('964', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481526964000');
INSERT INTO `control_solution_send_record_item` VALUES ('965', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481527025000');
INSERT INTO `control_solution_send_record_item` VALUES ('966', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481527025000');
INSERT INTO `control_solution_send_record_item` VALUES ('967', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481527086000');
INSERT INTO `control_solution_send_record_item` VALUES ('968', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481527087000');
INSERT INTO `control_solution_send_record_item` VALUES ('969', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481527144000');
INSERT INTO `control_solution_send_record_item` VALUES ('970', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481527144000');
INSERT INTO `control_solution_send_record_item` VALUES ('971', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481527204000');
INSERT INTO `control_solution_send_record_item` VALUES ('972', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481527204000');
INSERT INTO `control_solution_send_record_item` VALUES ('973', 'CSTJCU49', 'EQUJGI8', '[{\"duration\":2,\"sendContent\":\"我是内容\"},{\"duration\":3,\"sendContent\":\"我是内容2\"}]', '1', '1481527264000');
INSERT INTO `control_solution_send_record_item` VALUES ('974', 'CSTJCU49', 'EQRER2C', '[{\"duration\":2222,\"sendContent\":\"123\"},{\"duration\":33333,\"sendContent\":\"撒旦法士大夫\"}]', '1', '1481527264000');
INSERT INTO `control_solution_send_record_item` VALUES ('975', 'CST4FQAA', 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481536714000');
INSERT INTO `control_solution_send_record_item` VALUES ('976', 'CST4FQAA', 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481537344000');
INSERT INTO `control_solution_send_record_item` VALUES ('977', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481537352000');
INSERT INTO `control_solution_send_record_item` VALUES ('978', null, 'EQUJGI8', '雨雪天气，请小心慢行，小心慢行', '1', '1481538339000');
INSERT INTO `control_solution_send_record_item` VALUES ('979', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481538349000');
INSERT INTO `control_solution_send_record_item` VALUES ('980', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481538678000');
INSERT INTO `control_solution_send_record_item` VALUES ('981', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481538688000');
INSERT INTO `control_solution_send_record_item` VALUES ('982', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481538843000');
INSERT INTO `control_solution_send_record_item` VALUES ('983', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481538854000');
INSERT INTO `control_solution_send_record_item` VALUES ('984', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481538923000');
INSERT INTO `control_solution_send_record_item` VALUES ('985', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481538933000');
INSERT INTO `control_solution_send_record_item` VALUES ('986', null, 'EQUJGI8', '雨雪天气，请小心慢行，小心慢行', '1', '1481539654000');
INSERT INTO `control_solution_send_record_item` VALUES ('987', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481539662000');
INSERT INTO `control_solution_send_record_item` VALUES ('988', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481539835000');
INSERT INTO `control_solution_send_record_item` VALUES ('989', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481539843000');
INSERT INTO `control_solution_send_record_item` VALUES ('990', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481540374000');
INSERT INTO `control_solution_send_record_item` VALUES ('991', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481540383000');
INSERT INTO `control_solution_send_record_item` VALUES ('992', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481540681000');
INSERT INTO `control_solution_send_record_item` VALUES ('993', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481540690000');
INSERT INTO `control_solution_send_record_item` VALUES ('994', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481542235000');
INSERT INTO `control_solution_send_record_item` VALUES ('995', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481542246000');
INSERT INTO `control_solution_send_record_item` VALUES ('996', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481542634000');
INSERT INTO `control_solution_send_record_item` VALUES ('997', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481542674000');
INSERT INTO `control_solution_send_record_item` VALUES ('998', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481542746000');
INSERT INTO `control_solution_send_record_item` VALUES ('999', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481542756000');
INSERT INTO `control_solution_send_record_item` VALUES ('1000', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481542822000');
INSERT INTO `control_solution_send_record_item` VALUES ('1001', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481542844000');
INSERT INTO `control_solution_send_record_item` VALUES ('1002', null, 'EQUJGI8', '新年快乐', '1', '1481594798000');
INSERT INTO `control_solution_send_record_item` VALUES ('1003', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481594812000');
INSERT INTO `control_solution_send_record_item` VALUES ('1004', null, 'EQUJGI8', '圣诞节快乐,大家好', '1', '1481594964000');
INSERT INTO `control_solution_send_record_item` VALUES ('1005', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481594979000');
INSERT INTO `control_solution_send_record_item` VALUES ('1006', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481595210000');
INSERT INTO `control_solution_send_record_item` VALUES ('1007', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481595223000');
INSERT INTO `control_solution_send_record_item` VALUES ('1008', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481595332000');
INSERT INTO `control_solution_send_record_item` VALUES ('1009', null, 'EQUJGI8', '圣诞节快乐，大家好', '1', '1481595345000');
INSERT INTO `control_solution_send_record_item` VALUES ('1010', null, 'EQUJGI8', '111111111111111111', '1', '1481595465000');
INSERT INTO `control_solution_send_record_item` VALUES ('1011', null, 'EQUJGI8', '2222222222222222', '1', '1481595479000');
INSERT INTO `control_solution_send_record_item` VALUES ('1012', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481595701000');
INSERT INTO `control_solution_send_record_item` VALUES ('1013', null, 'EQUJGI8', '22222222222', '1', '1481595715000');
INSERT INTO `control_solution_send_record_item` VALUES ('1014', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481596563000');
INSERT INTO `control_solution_send_record_item` VALUES ('1015', null, 'EQUJGI8', '雾霾天气，注意路滑', '1', '1481596574000');
INSERT INTO `control_solution_send_record_item` VALUES ('1016', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481597024000');
INSERT INTO `control_solution_send_record_item` VALUES ('1017', null, 'EQUJGI8', '雾霾天气，注意路滑', '1', '1481597039000');
INSERT INTO `control_solution_send_record_item` VALUES ('1018', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481597130000');
INSERT INTO `control_solution_send_record_item` VALUES ('1019', null, 'EQUJGI8', '雾霾天气，注意路滑', '1', '1481597161000');
INSERT INTO `control_solution_send_record_item` VALUES ('1020', null, 'EQUJGI8', '1111111111111111', '1', '1481597457000');
INSERT INTO `control_solution_send_record_item` VALUES ('1021', null, 'EQUJGI8', '22222222222222', '1', '1481597469000');
INSERT INTO `control_solution_send_record_item` VALUES ('1022', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481597510000');
INSERT INTO `control_solution_send_record_item` VALUES ('1023', null, 'EQUJGI8', '1111111天气，注意路滑', '1', '1481597524000');
INSERT INTO `control_solution_send_record_item` VALUES ('1024', null, 'EQUJGI8', '11111111111111111', '1', '1481597586000');
INSERT INTO `control_solution_send_record_item` VALUES ('1025', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481597599000');
INSERT INTO `control_solution_send_record_item` VALUES ('1026', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481599498000');
INSERT INTO `control_solution_send_record_item` VALUES ('1027', null, 'EQUJGI8', '1111天气，注意路滑', '1', '1481599511000');
INSERT INTO `control_solution_send_record_item` VALUES ('1028', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481599814000');
INSERT INTO `control_solution_send_record_item` VALUES ('1029', null, 'EQUJGI8', '2222天气，注意路滑', '1', '1481599829000');
INSERT INTO `control_solution_send_record_item` VALUES ('1030', null, 'EQUJGI8', '1111111111111111', '1', '1481599880000');
INSERT INTO `control_solution_send_record_item` VALUES ('1031', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481599895000');
INSERT INTO `control_solution_send_record_item` VALUES ('1032', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481599946000');
INSERT INTO `control_solution_send_record_item` VALUES ('1033', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481599960000');
INSERT INTO `control_solution_send_record_item` VALUES ('1034', null, 'EQUJGI8', '新年快乐', '1', '1481600156000');
INSERT INTO `control_solution_send_record_item` VALUES ('1035', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481600177000');
INSERT INTO `control_solution_send_record_item` VALUES ('1036', null, 'EQUJGI8', '国庆', '1', '1481600201000');
INSERT INTO `control_solution_send_record_item` VALUES ('1037', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481600217000');
INSERT INTO `control_solution_send_record_item` VALUES ('1038', null, 'EQUJGI8', '新年快乐', '1', '1481600242000');
INSERT INTO `control_solution_send_record_item` VALUES ('1039', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481600258000');
INSERT INTO `control_solution_send_record_item` VALUES ('1040', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481600316000');
INSERT INTO `control_solution_send_record_item` VALUES ('1041', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481600333000');
INSERT INTO `control_solution_send_record_item` VALUES ('1042', null, 'EQUJGI8', '新年快乐11111', '1', '1481601739000');
INSERT INTO `control_solution_send_record_item` VALUES ('1043', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481601753000');
INSERT INTO `control_solution_send_record_item` VALUES ('1044', null, 'EQUJGI8', '新年快乐111', '1', '1481601821000');
INSERT INTO `control_solution_send_record_item` VALUES ('1045', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481601834000');
INSERT INTO `control_solution_send_record_item` VALUES ('1046', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481601886000');
INSERT INTO `control_solution_send_record_item` VALUES ('1047', null, 'EQUJGI8', '1221212121212', '1', '1481601900000');
INSERT INTO `control_solution_send_record_item` VALUES ('1048', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481601951000');
INSERT INTO `control_solution_send_record_item` VALUES ('1049', null, 'EQUJGI8', '1111111111111', '1', '1481601964000');
INSERT INTO `control_solution_send_record_item` VALUES ('1050', null, 'EQUJGI8', '111111111', '1', '1481602010000');
INSERT INTO `control_solution_send_record_item` VALUES ('1051', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602023000');
INSERT INTO `control_solution_send_record_item` VALUES ('1052', null, 'EQUJGI8', '大风天气,请减速慢行哦', '1', '1481602111000');
INSERT INTO `control_solution_send_record_item` VALUES ('1053', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602124000');
INSERT INTO `control_solution_send_record_item` VALUES ('1054', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481602168000');
INSERT INTO `control_solution_send_record_item` VALUES ('1055', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602217000');
INSERT INTO `control_solution_send_record_item` VALUES ('1056', null, 'EQUJGI8', '国庆', '1', '1481602265000');
INSERT INTO `control_solution_send_record_item` VALUES ('1057', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602279000');
INSERT INTO `control_solution_send_record_item` VALUES ('1058', null, 'EQUJGI8', '新年快乐', '1', '1481602319000');
INSERT INTO `control_solution_send_record_item` VALUES ('1059', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602334000');
INSERT INTO `control_solution_send_record_item` VALUES ('1060', null, 'EQUJGI8', '雨雪天气，请小心慢行.', '1', '1481602376000');
INSERT INTO `control_solution_send_record_item` VALUES ('1061', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602390000');
INSERT INTO `control_solution_send_record_item` VALUES ('1062', null, 'EQUJGI8', '雨雪天气请小心慢行', '1', '1481602457000');
INSERT INTO `control_solution_send_record_item` VALUES ('1063', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602472000');
INSERT INTO `control_solution_send_record_item` VALUES ('1064', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1481602516000');
INSERT INTO `control_solution_send_record_item` VALUES ('1065', null, 'EQUJGI8', '雨雪天气，1注意路滑', '1', '1481602531000');
INSERT INTO `control_solution_send_record_item` VALUES ('1066', null, 'EQUJGI8', '雨雪天气，请小心慢行1', '1', '1481602577000');
INSERT INTO `control_solution_send_record_item` VALUES ('1067', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1481602593000');
INSERT INTO `control_solution_send_record_item` VALUES ('1068', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1482068931000');
INSERT INTO `control_solution_send_record_item` VALUES ('1069', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1482068933000');
INSERT INTO `control_solution_send_record_item` VALUES ('1070', null, 'EQUJGI8', '雨雪天气，请小心慢行', '1', '1482068944000');
INSERT INTO `control_solution_send_record_item` VALUES ('1071', null, 'EQUJGI8', '雨雪天气，注意路滑', '1', '1482068946000');

-- ----------------------------
-- Table structure for cost_config
-- ----------------------------
DROP TABLE IF EXISTS `cost_config`;
CREATE TABLE `cost_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of cost_config
-- ----------------------------

-- ----------------------------
-- Table structure for cost_config_info
-- ----------------------------
DROP TABLE IF EXISTS `cost_config_info`;
CREATE TABLE `cost_config_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of cost_config_info
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report
-- ----------------------------
DROP TABLE IF EXISTS `custom_report`;
CREATE TABLE `custom_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `report_type` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '报表类型：bar,line,pie',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report_access_right
-- ----------------------------
DROP TABLE IF EXISTS `custom_report_access_right`;
CREATE TABLE `custom_report_access_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `object_type` int(11) NOT NULL COMMENT '对象类型：1-角色，2-用户',
  `object_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '对象编码',
  `access_right` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '访问权限：r--读，u-改，d-删，用''|''分隔',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report_access_right
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report_dimension
-- ----------------------------
DROP TABLE IF EXISTS `custom_report_dimension`;
CREATE TABLE `custom_report_dimension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '报表编码',
  `dimension_type` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '维度类型：area-区域，industry-行业，customer-客户，equipment-设备',
  `dimension` varchar(4000) CHARACTER SET utf8 NOT NULL COMMENT '对象编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report_dimension
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report_index
-- ----------------------------
DROP TABLE IF EXISTS `custom_report_index`;
CREATE TABLE `custom_report_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '报表编码',
  `index_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '指标编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report_index
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report_info
-- ----------------------------
DROP TABLE IF EXISTS `custom_report_info`;
CREATE TABLE `custom_report_info` (
  `language` varchar(20) CHARACTER SET utf8 NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `comment` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report_info
-- ----------------------------

-- ----------------------------
-- Table structure for custom_report_time
-- ----------------------------
DROP TABLE IF EXISTS `custom_report_time`;
CREATE TABLE `custom_report_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '报表编码',
  `begin_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '开始时间',
  `end_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '结束时间',
  `summary_unit` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '汇总单位：year-年，quarter-季度，month-月，week-周，day-日，hour-时，minute-分钟，second-秒',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of custom_report_time
-- ----------------------------

-- ----------------------------
-- Table structure for customer_cost
-- ----------------------------
DROP TABLE IF EXISTS `customer_cost`;
CREATE TABLE `customer_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `cost_type` varchar(20) NOT NULL COMMENT '成本类型',
  `cost` decimal(10,2) NOT NULL COMMENT '成本',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of customer_cost
-- ----------------------------

-- ----------------------------
-- Table structure for customer_data_config
-- ----------------------------
DROP TABLE IF EXISTS `customer_data_config`;
CREATE TABLE `customer_data_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `config` varchar(1000) NOT NULL COMMENT '数据库连接配置',
  `accesskey` varchar(50) NOT NULL COMMENT 'accesskey',
  `secretkey` varchar(50) NOT NULL COMMENT 'secretkey',
  `validity_period` date NOT NULL COMMENT '有效期',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of customer_data_config
-- ----------------------------

-- ----------------------------
-- Table structure for customer_material_config
-- ----------------------------
DROP TABLE IF EXISTS `customer_material_config`;
CREATE TABLE `customer_material_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `material_code` varchar(20) NOT NULL COMMENT '原料编码',
  `cost` decimal(10,2) NOT NULL COMMENT '成本',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  `code` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of customer_material_config
-- ----------------------------

-- ----------------------------
-- Table structure for customer_product_config
-- ----------------------------
DROP TABLE IF EXISTS `customer_product_config`;
CREATE TABLE `customer_product_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `material_code` varchar(20) NOT NULL COMMENT '原料编码',
  `code` varchar(32) NOT NULL COMMENT '编码',
  `price` decimal(8,2) NOT NULL COMMENT '售价',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  `product_code` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of customer_product_config
-- ----------------------------

-- ----------------------------
-- Table structure for equipment
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(100) DEFAULT NULL,
  `customer_code` varchar(20) DEFAULT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `pile_no` varchar(20) NOT NULL,
  `section_code` varchar(20) NOT NULL COMMENT '路段编码',
  `serial_number` varchar(20) DEFAULT NULL COMMENT '序列号',
  `category_code` varchar(20) DEFAULT NULL COMMENT '分类编码',
  `oem_code` varchar(20) DEFAULT NULL COMMENT '设备制造商编码',
  `production_date` varchar(32) DEFAULT NULL COMMENT '出厂日期',
  `supplier_code` varchar(20) DEFAULT NULL COMMENT '代理商编码',
  `purchase_date` varchar(32) DEFAULT NULL COMMENT '购买日期',
  `guarantee_begin_date` varchar(32) DEFAULT NULL COMMENT '保修期开始时间',
  `warranty_period` int(11) DEFAULT NULL COMMENT '保修期',
  `oem_person` varchar(50) DEFAULT NULL COMMENT '设备制造商负责人',
  `supplier_person` varchar(50) DEFAULT NULL COMMENT '代理商负责人',
  `customer_person` varchar(50) DEFAULT NULL COMMENT '客户现场负责人',
  `current_state` int(11) DEFAULT NULL COMMENT ' ''当前状态：0-离线，1-停机，2-故障，3-正常'',',
  `location` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment
-- ----------------------------
INSERT INTO `equipment` VALUES ('1', '', null, 'EQ4U992', '', 'CUXYEC3', null, 'QSSXT', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', '30.067104018465677,105.21547470418092', '1', '1480139093000', '1481783213000');
INSERT INTO `equipment` VALUES ('2', '', null, 'EQDQ4CC', '', 'CUXYEC3', null, 'QXSXT', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', '30.675054246264224,104.47346840230104', '1', '1480139109000', '1481783220000');
INSERT INTO `equipment` VALUES ('3', '', null, 'EQDW9GR', '', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', null, '1', '1480139221000', '1480643637000');
INSERT INTO `equipment` VALUES ('4', '', null, 'EQUJGI8', '', 'CUXYEC3', null, 'MJSKBQBB', 'CUXM9D8', null, null, null, null, null, '', null, '', '2', '30.26919,104.977733', '1', '1480139240000', '1483496091000');
INSERT INTO `equipment` VALUES ('5', '', null, 'EQHE76P', '', 'CUXYEC3', null, 'WBCLJCQ', 'CUXM9D8', null, null, null, null, null, '', null, '', '3', '30.14146252550217,105.14022801247717', '1', '1480139256000', '1481783235000');
INSERT INTO `equipment` VALUES ('6', '', null, 'EQCPY4G', '', 'CUXYEC3', null, 'CDZSQ', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', null, '1', '1480139277000', '1480643606000');
INSERT INTO `equipment` VALUES ('7', '', null, 'EQQ735V', '', 'CUXYEC3', null, 'COJCQ', 'CUXM9D8', null, null, null, null, null, '', null, '', '2', null, '1', '1480139294000', '1480643596000');
INSERT INTO `equipment` VALUES ('8', '', null, 'EQHZPYC', '', 'CUXYEC3', null, 'VIJCQ', 'CUXM9D8', null, null, null, null, null, '', null, '', '2', '30.323523531968533,104.91251383153077', '1', '1480139314000', '1481786913000');
INSERT INTO `equipment` VALUES ('9', '', null, 'EQZMGKD', '', 'CUXYEC3', null, 'JTXHD', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, '', '3', '30.537237608681032,104.70768037926776', '1', '1480139333000', '1481782149000');
INSERT INTO `equipment` VALUES ('10', '', null, 'EQGWGG8', '', 'CUXYEC3', null, 'SLFJ', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', null, '1', '1480139352000', '1480643567000');
INSERT INTO `equipment` VALUES ('11', '', null, 'EQFAZUN', '', 'CUXYEC3', null, 'DNZM', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', null, '1', '1480139375000', '1480643555000');
INSERT INTO `equipment` VALUES ('12', '', null, 'EQFYDBF', '', 'CUXYEC3', null, 'DWZM', 'CUXM9D8', null, null, null, null, null, '', null, '', '0', '30.576527152988767,104.42036066380616', '1', '1480139391000', '1481707328000');
INSERT INTO `equipment` VALUES ('13', '', null, 'EQY7A5F', '', 'CUXYEC3', null, 'JSB', 'CUXM9D8', null, null, null, null, null, '', null, '', '2', null, '1', '1480139441000', '1480643533000');
INSERT INTO `equipment` VALUES ('14', '', null, 'EQRUWQ1', '', 'CUXYEC3', null, 'HDM', 'CUXM9D8', null, null, null, null, null, '', null, '', '3', '30.283112519240056,104.99418173161627', '1', '1480139457000', '1480643499000');
INSERT INTO `equipment` VALUES ('15', '', null, 'EQQ8L2R', '', 'CUPYYBU', '008-0012', 'ECA66CT', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1480927665000', null);
INSERT INTO `equipment` VALUES ('16', '', null, 'EQ5Z91M', '', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1481007948000', null);
INSERT INTO `equipment` VALUES ('17', '', null, 'EQARHFX', '', 'CUXYEC3', null, 'ECBXWZ5', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1481008506000', null);
INSERT INTO `equipment` VALUES ('18', '', null, 'EQRER2C', '', 'CUXYEC3', '11111111', 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, '', '0', '30.168532515442998,105.18588457432863', '1', '1481009187000', '1481782463000');
INSERT INTO `equipment` VALUES ('19', '', null, 'EQWBK8A', '005', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1481011584000', null);
INSERT INTO `equipment` VALUES ('20', '', null, 'EQX7KTS', '1', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, '', '0', '30.283112519240056,104.99418173161627', '1', '1481103885000', '1481783563000');
INSERT INTO `equipment` VALUES ('21', '', null, 'EQMM13V', '23', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, '', '0', '30.580554,104.429982', '1', '1481186263000', '1481703323000');
INSERT INTO `equipment` VALUES ('22', '', null, 'EQQQ2SD', '10', 'CUPYYBU', '002', 'FXKBQBB', 'CUXM9D8', '1482163200000', null, '1481817600000', '1482249600000', '2', 'USDH71Y,', null, '', '0', '30.257050196887747,104.99476108876344', '1', '1481708741000', '1481708871000');
INSERT INTO `equipment` VALUES ('23', '', null, 'EQ4C8E3', '10', 'CUPYYBU', '002', 'FXKBQBB', 'CUXM9D8', '1482163200000', null, '1481817600000', '1482249600000', '2', 'USDH71Y,', null, '', '0', '30.257050196887747,104.99476108876344', '1', '1481812130000', null);
INSERT INTO `equipment` VALUES ('24', '', null, 'EQMVBF4', '1', 'CUXYEC3', null, 'DWZM', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '2', '30.378000130888147,104.79236517190522', '1', '1481875133000', null);
INSERT INTO `equipment` VALUES ('25', '', null, 'EQ6R6PJ', '2', 'CUWSTC6', null, 'DWZM', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', '30.216241053640516,105.05997796591502', '1', '1481877942000', null);
INSERT INTO `equipment` VALUES ('26', '', null, 'EQM1FNL', '100', 'CUXYEC3', '10', 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1482111982000', null);
INSERT INTO `equipment` VALUES ('27', '', null, 'EQ976WZ', '20', 'CUWSTC6', '1', 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', '30.231197250095377,105.01128349629518', '1', '1482114939000', null);
INSERT INTO `equipment` VALUES ('28', '', null, 'EQ3NG9Z', '1222222', 'CUPYYBU', null, 'QXSXT', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1482116146000', null);
INSERT INTO `equipment` VALUES ('29', '', null, 'EQI1JYC', '1111', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,', null, null, '0', '29.941633,104.850389', '1', '1482204180000', null);
INSERT INTO `equipment` VALUES ('30', '', null, 'EQPNVFC', '1', 'CUJLRWF', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,', null, null, '0', null, '1', '1482209244000', null);
INSERT INTO `equipment` VALUES ('31', '', null, 'EQFNFIU', '111', 'CUWSTC6', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USU3XBV,', null, null, '0', null, '1', '1482215086000', null);
INSERT INTO `equipment` VALUES ('32', '', null, 'EQSAJ1J', '6', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1482219083000', null);
INSERT INTO `equipment` VALUES ('33', '', null, 'EQKB6L7', '111', 'CUWSTC6', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', null, '1', '1482223930000', null);
INSERT INTO `equipment` VALUES ('34', '', null, 'EQHIPKF', '11', 'CUXYEC3', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, null, '0', '30.437103,105.089554', '1', '1482228390000', null);
INSERT INTO `equipment` VALUES ('35', '', null, 'EQ2V8EG', '2222', 'CUWSTC6', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,USU3XBV,', null, '', '0', null, '1', '1482300586000', '1482301092000');
INSERT INTO `equipment` VALUES ('36', '', null, 'EQGIC49', '6', 'CUWSTC6', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'USDH71Y,', null, '', '0', null, '1', '1482301566000', '1482310123000');
INSERT INTO `equipment` VALUES ('37', '', null, 'EQR5IM2', '1', 'CUWSTC6', null, 'CDZSQ', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,', null, null, '0', null, '1', '1482377031000', null);
INSERT INTO `equipment` VALUES ('38', '', null, 'EQASVAS', '1', 'CUWSTC6', null, 'JTXHD', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,USU3XBV,', null, null, '0', null, '1', '1482396206000', null);
INSERT INTO `equipment` VALUES ('39', '', null, 'EQZZ7YY', '2', 'CUWSTC6', null, 'QSSXT', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,', null, null, '0', '30.297009,104.915355', '9', '1482990094000', null);
INSERT INTO `equipment` VALUES ('40', '', null, 'EQKI57Q', '5', 'CUWSTC6', null, 'FXKBQBB', 'CUXM9D8', null, null, null, null, null, 'US2UUA9,', null, null, '0', '30.285533,104.960773', '1', '1482990395000', null);
INSERT INTO `equipment` VALUES ('41', '', null, 'EQCJPPC', '5', 'CUXYEC3', '序列号', 'FXKBQBB', 'CUXM9D8', '1480435200000', null, '1480521600000', '1480608000000', '2', 'USU3XBV,', null, null, '0', null, '9', '1483006928000', null);

-- ----------------------------
-- Table structure for equipment_bom
-- ----------------------------
DROP TABLE IF EXISTS `equipment_bom`;
CREATE TABLE `equipment_bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) NOT NULL COMMENT '公司编码',
  `equipment_code` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `oem_bom_code` varchar(20) DEFAULT NULL COMMENT '关联oem元件code',
  `serial_number` varchar(20) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `install_amount` int(11) DEFAULT NULL,
  `stock_amount` int(11) DEFAULT NULL,
  `image_url` varchar(100) DEFAULT NULL COMMENT '图片',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_bom
-- ----------------------------

-- ----------------------------
-- Table structure for equipment_bom_info
-- ----------------------------
DROP TABLE IF EXISTS `equipment_bom_info`;
CREATE TABLE `equipment_bom_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `position` varchar(50) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_bom_info
-- ----------------------------

-- ----------------------------
-- Table structure for equipment_fault_channel
-- ----------------------------
DROP TABLE IF EXISTS `equipment_fault_channel`;
CREATE TABLE `equipment_fault_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `library_code` varchar(20) NOT NULL COMMENT '翻译库编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(50) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_fault_channel
-- ----------------------------
INSERT INTO `equipment_fault_channel` VALUES ('1', 'EQRUWQ1', 'TLA4NJU', 'FCG81H4', '1', '1480476341000', '1480496958000');
INSERT INTO `equipment_fault_channel` VALUES ('2', 'undefined', 'TL944B8', 'FC5EQ7H', '1', '1482308309000', null);

-- ----------------------------
-- Table structure for equipment_fault_channel_info
-- ----------------------------
DROP TABLE IF EXISTS `equipment_fault_channel_info`;
CREATE TABLE `equipment_fault_channel_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_fault_channel_info
-- ----------------------------
INSERT INTO `equipment_fault_channel_info` VALUES ('cn', 'FC5EQ7H', '到底', null);
INSERT INTO `equipment_fault_channel_info` VALUES ('cn', 'FCG81H4', '设备某故障通道', null);

-- ----------------------------
-- Table structure for equipment_info
-- ----------------------------
DROP TABLE IF EXISTS `equipment_info`;
CREATE TABLE `equipment_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `brand` varchar(20) DEFAULT NULL COMMENT '品牌',
  `model` varchar(20) DEFAULT NULL COMMENT '型号',
  `category_name` varchar(50) DEFAULT NULL,
  `place_of_origin` varchar(20) DEFAULT NULL COMMENT '产地',
  `country` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT '所属地区',
  `city` varchar(20) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_info
-- ----------------------------
INSERT INTO `equipment_info` VALUES ('cn', 'EQ2V8EG', '4', null, null, 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ3NG9Z', '查看桩号', null, 'EM5UZY4', '球形摄像头', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ4C8E3', 'F型可变情报板001#1', null, 'EMYBILA', 'F型可变情报板', null, '中国', 'null', '', null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ4U992', '枪式摄像头001', null, 'EMPEZRX', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ5Z91M', 'F型可变情报板001', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ6R6PJ', '洞外照明001', null, 'EMEFBXL', '洞外照明', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQ976WZ', 'F型可变情报板007', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQARHFX', '121212qwqwqwqw', null, 'EMAYL8P', '1212121212', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQASVAS', '1', null, null, '交通信号灯', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQCJPPC', '设备名称', null, null, 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQCPY4G', '车道指示器001', null, 'EMLJWCW', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQDQ4CC', '球形摄像头001', null, 'EM5UZY4', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQDW9GR', 'F型可变情报板001', null, 'EMYBILA', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQFAZUN', '洞内照明1001', null, 'EMYT83S', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQFNFIU', 'F型可变情报板001', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQFYDBF', '洞外照明001', null, 'EMEFBXL', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQGIC49', 'sdfsdfsdfsdf', null, null, 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQGWGG8', '射流风机001', null, 'EMRSCLI', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQHE76P', '微波车辆检测器001', null, 'EMHQ3XK', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQHIPKF', '的费撒旦发生的', null, null, 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQHZPYC', 'VI检测器001', null, 'EMS1TTN', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQI1JYC', 'F型可变情报板001', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQKB6L7', '111', null, null, 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQKI57Q', '4', null, null, 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQM1FNL', '有宽高的情报板', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQMM13V', '变态情报板', null, 'EMYBILA', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQMVBF4', '洞外照明001', null, 'EMEFBXL', '洞外照明', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQPNVFC', 'F型可变情报板001', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQQ735V', 'CO检测器001', null, 'EMCWE7B', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQQ8L2R', '群控配置设备', null, 'EMMBPIY', '群控配置分类', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQQQ2SD', 'F型可变情报板001', null, 'EMYBILA', 'undefined', null, '中国', 'null', '', null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQR5IM2', '111', null, null, '车道指示器', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQRER2C', '情报板添加预览', null, 'EMYBILA', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQRUWQ1', '横道门001', null, 'EMZIKIK', 'undefined', null, '中国', '吉林', '白城', null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQSAJ1J', '5', null, null, 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQUJGI8', '门架式可变情报板001', null, 'EM6YHF8', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQWBK8A', 'F型可变情报板宽高', null, 'EMYBILA', 'F型可变情报板', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQX7KTS', '1234567', null, 'EMYBILA', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQY7A5F', '给水泵001', null, 'EMMGYEG', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQZMGKD', '交通信号灯1001', null, 'EMCJL5S', 'undefined', null, null, null, null, null);
INSERT INTO `equipment_info` VALUES ('cn', 'EQZZ7YY', '1', null, null, '枪式摄像头', null, null, null, null, null);

-- ----------------------------
-- Table structure for equipment_inspection_config
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_config`;
CREATE TABLE `equipment_inspection_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `begin_date` varchar(50) NOT NULL COMMENT '开始时间',
  `period` int(11) NOT NULL COMMENT '巡检周期：单位（天）',
  `advance_days` int(11) NOT NULL COMMENT '提前通知时间：单位（天）',
  `title` varchar(50) DEFAULT NULL COMMENT '巡检标题',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(50) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_config
-- ----------------------------
INSERT INTO `equipment_inspection_config` VALUES ('1', 'ICRW11J', 'CUXM9D8', 'EQHZPYC', '1480348800000', '19', '1', '的', '9', '1480401373000', '1480401373000');
INSERT INTO `equipment_inspection_config` VALUES ('2', 'ICH2MAH', 'CUXM9D8', 'EQRUWQ1', '1478448000000', '12', '4', 'cbvbcvccxbc', '9', '1480405122000', '1480405122000');
INSERT INTO `equipment_inspection_config` VALUES ('4', 'ICRYDZP', 'CUXM9D8', 'EQRUWQ1', '1478188800000', '5', '2', 'llll', '9', '1480406287000', '1480406287000');
INSERT INTO `equipment_inspection_config` VALUES ('5', 'IC2IGGC', 'CUXM9D8', 'EQFYDBF', '1478620800000', '1', '1', '123', '1', '1480415212000', '1480415212000');
INSERT INTO `equipment_inspection_config` VALUES ('6', 'IC14QP9', 'CUXM9D8', 'EQFAZUN', '1481040000000', '1', '2', '地对地导弹', '9', '1481623512000', '1481623512000');
INSERT INTO `equipment_inspection_config` VALUES ('7', 'ICY1A1X', 'CUXM9D8', 'EQ4U992', '1481040000000', '2222', '3333', '11111', '9', '1481623572000', '1481623572000');
INSERT INTO `equipment_inspection_config` VALUES ('8', 'IC625XS', 'CUXM9D8', 'EQQQ2SD', '1482076800000', '30', '1', '测试巡检', '9', '1482121609000', '1482121609000');
INSERT INTO `equipment_inspection_config` VALUES ('9', 'ICNHGNE', 'CUXM9D8', 'EQQQ2SD', '1482076800000', '30', '1', '测试巡检', '9', '1482121609000', '1482121609000');
INSERT INTO `equipment_inspection_config` VALUES ('10', 'ICSRFD3', 'CUXM9D8', 'EQQQ2SD', '1482076800000', '30', '1', '测试巡检', '9', '1482121609000', '1482121609000');
INSERT INTO `equipment_inspection_config` VALUES ('11', 'ICHAD8M', 'CUXM9D8', 'EQQQ2SD', '1482076800000', '20', '1', '测试', '9', '1482121819000', '1482121819000');

-- ----------------------------
-- Table structure for equipment_inspection_config_work_item
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_config_work_item`;
CREATE TABLE `equipment_inspection_config_work_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_code` varchar(20) NOT NULL,
  `work_item_code` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `create_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_config_work_item
-- ----------------------------
INSERT INTO `equipment_inspection_config_work_item` VALUES ('1', 'ICRW11J', 'IWI19YIE', '1', '1480401373000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('2', 'ICRW11J', 'IWIUFILA', '1', '1480401373000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('3', 'ICH2MAH', 'IWICC98T', '1', '1480405122000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('4', 'ICRYDZP', 'IWIMS24D', '1', '1480406287000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('5', 'IC2IGGC', 'IWIAQ8NX', '1', '1480415212000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('6', 'IC2IGGC', 'IWI3T2C5', '1', '1480415266000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('7', 'IC14QP9', 'IWIKK9VQ', '1', '1481623512000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('8', 'ICY1A1X', 'IWIG4TA2', '1', '1481623573000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('9', 'ICNHGNE', 'IWINNQ4W', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('10', 'ICNHGNE', 'IWI3FBR2', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('11', 'ICNHGNE', 'IWI7SI81', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('12', 'IC625XS', 'IWITTI19', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('13', 'IC625XS', 'IWI7D8L5', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('14', 'IC625XS', 'IWICABJR', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('15', 'ICSRFD3', 'IWI4J98C', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('16', 'ICSRFD3', 'IWIL5DYK', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('17', 'ICSRFD3', 'IWIQ37B5', '1', '1482121609000');
INSERT INTO `equipment_inspection_config_work_item` VALUES ('18', 'ICHAD8M', 'IWIG7AFL', '1', '1482121819000');

-- ----------------------------
-- Table structure for equipment_inspection_result
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_result`;
CREATE TABLE `equipment_inspection_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `work_item_code` varchar(50) NOT NULL COMMENT '工作项编码',
  `result` varchar(1000) NOT NULL COMMENT '结果',
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_result
-- ----------------------------

-- ----------------------------
-- Table structure for equipment_inspection_work_item
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_work_item`;
CREATE TABLE `equipment_inspection_work_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `customer_code` varchar(20) NOT NULL,
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `select_type` int(11) NOT NULL COMMENT '选项类型：1-多选，2-单选, 3-文本',
  `select_item` varchar(1000) DEFAULT NULL COMMENT '选项：分号分隔',
  `order_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(50) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_work_item
-- ----------------------------
INSERT INTO `equipment_inspection_work_item` VALUES ('1', 'IWI19YIE', 'CUXM9D8', 'EQHZPYC', '2', null, null, '9', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('2', 'IWIUFILA', 'CUXM9D8', 'EQHZPYC', '1', null, null, '9', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('3', 'IWICC98T', 'CUXM9D8', 'EQRUWQ1', '2', null, null, '9', '1480405122000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('4', 'IWIMS24D', 'CUXM9D8', 'EQRUWQ1', '2', null, null, '9', '1480406287000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('5', 'IWIAQ8NX', 'CUXM9D8', 'EQFYDBF', '2', null, null, '9', '1480415212000', '1480415266000');
INSERT INTO `equipment_inspection_work_item` VALUES ('6', 'IWI3T2C5', 'CUXM9D8', 'EQFYDBF', '2', null, null, '9', '1480415266000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('7', 'IWIKK9VQ', 'CUXM9D8', 'EQFAZUN', '2', null, null, '1', '1481623512000', '1481623531000');
INSERT INTO `equipment_inspection_work_item` VALUES ('8', 'IWIG4TA2', 'CUXM9D8', 'EQ4U992', '1', null, null, '1', '1481623573000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('9', 'IWINNQ4W', 'CUXM9D8', 'EQQQ2SD', '2', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('10', 'IWI3FBR2', 'CUXM9D8', 'EQQQ2SD', '1', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('11', 'IWI7SI81', 'CUXM9D8', 'EQQQ2SD', '3', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('12', 'IWITTI19', 'CUXM9D8', 'EQQQ2SD', '2', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('13', 'IWI7D8L5', 'CUXM9D8', 'EQQQ2SD', '1', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('14', 'IWICABJR', 'CUXM9D8', 'EQQQ2SD', '3', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('15', 'IWI4J98C', 'CUXM9D8', 'EQQQ2SD', '2', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('16', 'IWIL5DYK', 'CUXM9D8', 'EQQQ2SD', '1', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('17', 'IWIQ37B5', 'CUXM9D8', 'EQQQ2SD', '3', null, null, '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item` VALUES ('18', 'IWIG7AFL', 'CUXM9D8', 'EQQQ2SD', '2', null, null, '1', '1482121819000', null);

-- ----------------------------
-- Table structure for equipment_inspection_work_item_info
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_work_item_info`;
CREATE TABLE `equipment_inspection_work_item_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(500) NOT NULL COMMENT '工作项名称',
  `description` varchar(2000) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_work_item_info
-- ----------------------------
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI19YIE', '吃谁的醋', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI3FBR2', '步骤2', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI3T2C5', 'QWE', '');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI4J98C', '步骤1', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI7D8L5', '步骤2', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWI7SI81', '步骤3', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIAQ8NX', '123123123', '123123123');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWICABJR', '步骤3', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWICC98T', 'yutertyre', 'fsdfsdfdsf');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIG4TA2', '111111', '上升到');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIG7AFL', '测试', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIKK9VQ', '11111', '呜呜呜呜');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIL5DYK', '步骤2', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIMS24D', 'ere', 'erreeerer');
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWINNQ4W', '步骤1', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIQ37B5', '步骤3', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWITTI19', '步骤1', null);
INSERT INTO `equipment_inspection_work_item_info` VALUES ('cn', 'IWIUFILA', '错误的市场', null);

-- ----------------------------
-- Table structure for equipment_inspection_work_item_option
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_work_item_option`;
CREATE TABLE `equipment_inspection_work_item_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `work_item_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '工作项编码',
  `select_item` varchar(1000) CHARACTER SET utf8 NOT NULL COMMENT '选项：分号分隔',
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_work_item_option
-- ----------------------------
INSERT INTO `equipment_inspection_work_item_option` VALUES ('1', 'IWI19YIE', '吃谁的醋', '1', '1', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('2', 'IWI19YIE', '才收到v刹', '2', '1', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('3', 'IWIUFILA', '沉淀池', '1', '1', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('4', 'IWIUFILA', 'v是地v', '2', '1', '1480401373000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('5', 'IWICC98T', 'efsfds', '1', '1', '1480405122000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('6', 'IWIMS24D', 'erere', '1', '1', '1480406287000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('7', 'IWIAQ8NX', '123123', '1', '1', '1480415212000', '1480415266000');
INSERT INTO `equipment_inspection_work_item_option` VALUES ('8', 'IWIAQ8NX', '12312312', '2', '1', '1480415212000', '1480415266000');
INSERT INTO `equipment_inspection_work_item_option` VALUES ('9', 'IWI3T2C5', 'QWE', '1', '1', '1480415266000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('10', 'IWI3T2C5', 'QWE', '2', '1', '1480415266000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('11', 'IWIKK9VQ', '222', '1', '1', '1481623512000', '1481623531000');
INSERT INTO `equipment_inspection_work_item_option` VALUES ('12', 'IWIKK9VQ', '333', '2', '1', '1481623512000', '1481623531000');
INSERT INTO `equipment_inspection_work_item_option` VALUES ('13', 'IWIKK9VQ', '444', '3', '1', '1481623512000', '1481623531000');
INSERT INTO `equipment_inspection_work_item_option` VALUES ('14', 'IWIG4TA2', '1111', '1', '1', '1481623572000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('15', 'IWIG4TA2', '222', '2', '1', '1481623572000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('16', 'IWIG4TA2', '呜呜呜呜', '3', '1', '1481623572000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('17', 'IWIG4TA2', '4444', '4', '1', '1481623572000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('18', 'IWINNQ4W', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('19', 'IWI4J98C', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('20', 'IWITTI19', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('21', 'IWITTI19', '巡检选项2', '2', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('22', 'IWINNQ4W', '巡检选项2', '2', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('23', 'IWI4J98C', '巡检选项2', '2', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('24', 'IWI7D8L5', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('25', 'IWIL5DYK', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('26', 'IWI3FBR2', '巡检选项1', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('27', 'IWICABJR', '点击修改选项内容', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('28', 'IWIQ37B5', '点击修改选项内容', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('29', 'IWI7SI81', '点击修改选项内容', '1', '1', '1482121609000', null);
INSERT INTO `equipment_inspection_work_item_option` VALUES ('30', 'IWIG7AFL', '测试', '1', '1', '1482121819000', null);

-- ----------------------------
-- Table structure for equipment_inspection_work_order
-- ----------------------------
DROP TABLE IF EXISTS `equipment_inspection_work_order`;
CREATE TABLE `equipment_inspection_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `config_code` varchar(50) DEFAULT NULL,
  `equipment_code` varchar(20) NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `inspection_date` varchar(50) NOT NULL COMMENT '巡检日期',
  `order_status` int(11) NOT NULL,
  `padding_by` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(50) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(50) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_inspection_work_order
-- ----------------------------
INSERT INTO `equipment_inspection_work_order` VALUES ('1', 'IWO76AKL', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1480690800013', '1', '', '1', 'work order comment', '1480690812000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('2', 'IWOQ74QJ', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1480777200014', '1', '', '1', 'work order comment', '1480777202000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('3', 'IWOJSGAM', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1480863600003', '1', '', '1', 'work order comment', '1480863602000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('4', 'IWO6LWFP', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1480950000006', '1', '', '1', 'work order comment', '1480950005000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('5', 'IWORCYNQ', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481036400008', '1', '', '1', 'work order comment', '1481036407000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('6', 'IWOVX1PN', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481122800011', '1', '', '1', 'work order comment', '1481122810000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('7', 'IWOAEE1M', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481209200013', '1', '', '1', 'work order comment', '1481209212000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('8', 'IWONYSFB', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481295600016', '1', '', '1', 'work order comment', '1481295615000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('9', 'IWON3F9P', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481382000006', '1', '', '1', 'work order comment', '1481382002000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('10', 'IWOEHKL2', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481468400008', '1', '', '1', 'work order comment', '1481468400000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('11', 'IWO198W7', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481554800010', '1', '', '1', 'work order comment', '1481554803000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('12', 'IWOW9GI1', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481641200013', '1', '', '1', 'work order comment', '1481641206000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('13', 'IWOCDEE4', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481727600015', '1', '', '1', 'work order comment', '1481727608000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('14', 'IWO27CFE', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481814000002', '1', '', '1', 'work order comment', '1481814011000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('15', 'IWOJ2IP2', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481900400005', '1', '', '1', 'work order comment', '1481900413000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('16', 'IWO6G6W8', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1481986800136', '1', '', '1', 'work order comment', '1481986802000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('17', 'IWOGBHS4', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482073200014', '1', '', '1', 'work order comment', '1482073200000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('18', 'IWO3569Y', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482073200011', '1', '', '1', 'work order comment', '1482073203000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('19', 'IWOIUC9J', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482159600016', '1', '', '1', 'work order comment', '1482159603000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('20', 'IWOMD93B', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482246000003', '1', '', '1', 'work order comment', '1482246006000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('21', 'IWO5BC11', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482332400006', '1', '', '1', 'work order comment', '1482332408000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('22', 'IWOT33L6', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482418800008', '1', '', '1', 'work order comment', '1482418811000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('23', 'IWOJ7UCU', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482505200011', '1', '', '1', 'work order comment', '1482505214000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('24', 'IWOUCIV8', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482591600029', '1', '', '1', 'work order comment', '1482591602000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('25', 'IWOD45DG', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482678000006', '1', '', '1', 'work order comment', '1482678002000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('26', 'IWOBV84H', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482764400009', '1', '', '1', 'work order comment', '1482764405000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('27', 'IWOV8D44', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482850800011', '1', '', '1', 'work order comment', '1482850807000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('28', 'IWOBQ72N', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1482937200014', '1', '', '1', 'work order comment', '1482937210000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('29', 'IWONFMN4', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483023600011', '1', '', '1', 'work order comment', '1483023613000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('30', 'IWO2E3DQ', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483110000000', '1', '', '1', 'work order comment', '1483110015000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('31', 'IWO1N7U6', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483196400002', '1', '', '1', 'work order comment', '1483196402000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('32', 'IWOMFMIZ', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483282800005', '1', '', '1', 'work order comment', '1483282801000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('33', 'IWOXEIPX', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483369200007', '1', '', '1', 'work order comment', '1483369204000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('34', 'IWOEPQ3P', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483455600010', '1', '', '1', 'work order comment', '1483455606000', null);
INSERT INTO `equipment_inspection_work_order` VALUES ('35', 'IWOMYDI5', 'CUXM9D8', null, 'EQFYDBF', 'Inspection work order test', 'content', '1483542000021', '1', '', '1', 'work order comment', '1483542009000', null);

-- ----------------------------
-- Table structure for equipment_model
-- ----------------------------
DROP TABLE IF EXISTS `equipment_model`;
CREATE TABLE `equipment_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `equipment_category_code` varchar(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` varchar(32) NOT NULL,
  `last_update_time` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_model
-- ----------------------------
INSERT INTO `equipment_model` VALUES ('1', 'EMYBILA', 'FXKBQBB', '1', '1480135406000', null);
INSERT INTO `equipment_model` VALUES ('2', 'EMLJWCW', 'CDZSQ', '1', '1480135413000', null);
INSERT INTO `equipment_model` VALUES ('3', 'EMCWE7B', 'COJCQ', '1', '1480135421000', null);
INSERT INTO `equipment_model` VALUES ('4', 'EMYT83S', 'DNZM', '1', '1480135429000', '1480482560000');
INSERT INTO `equipment_model` VALUES ('5', 'EMEFBXL', 'DWZM', '1', '1480135449000', null);
INSERT INTO `equipment_model` VALUES ('6', 'EMZIKIK', 'HDM', '1', '1480135456000', null);
INSERT INTO `equipment_model` VALUES ('7', 'EMYG5DK', 'JSB', '1', '1480135464000', null);
INSERT INTO `equipment_model` VALUES ('8', 'EMCJL5S', 'JTXHD', '1', '1480135471000', '1480482536000');
INSERT INTO `equipment_model` VALUES ('9', 'EMAASS9', 'MJSKBQBB', '9', '1480135477000', null);
INSERT INTO `equipment_model` VALUES ('10', 'EMPEZRX', 'QSSXT', '1', '1480135483000', null);
INSERT INTO `equipment_model` VALUES ('11', 'EM5UZY4', 'QXSXT', '1', '1480135489000', null);
INSERT INTO `equipment_model` VALUES ('12', 'EMBU1T3', 'SLFJ', '9', '1480135496000', null);
INSERT INTO `equipment_model` VALUES ('13', 'EMS1TTN', 'VIJCQ', '1', '1480135521000', null);
INSERT INTO `equipment_model` VALUES ('14', 'EMHQ3XK', 'WBCLJCQ', '1', '1480135528000', null);
INSERT INTO `equipment_model` VALUES ('15', 'EM6YHF8', 'MJSKBQBB', '1', '1480135534000', '1480586479000');
INSERT INTO `equipment_model` VALUES ('16', 'EMRSCLI', 'SLFJ', '1', '1480135552000', '1480586607000');
INSERT INTO `equipment_model` VALUES ('17', 'EMMGYEG', 'JSB', '1', '1480139045000', '1480586499000');
INSERT INTO `equipment_model` VALUES ('18', 'EMXBG8Z', 'WBCLJCQ', '1', '1480482280000', null);
INSERT INTO `equipment_model` VALUES ('19', 'EMTJBRP', 'WBCLJCQ', '1', '1480482289000', null);
INSERT INTO `equipment_model` VALUES ('20', 'EMMBPIY', 'ECA66CT', '1', '1480927598000', null);
INSERT INTO `equipment_model` VALUES ('21', 'EMAYL8P', 'ECBXWZ5', '1', '1481008471000', null);

-- ----------------------------
-- Table structure for equipment_model_info
-- ----------------------------
DROP TABLE IF EXISTS `equipment_model_info`;
CREATE TABLE `equipment_model_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_model_info
-- ----------------------------
INSERT INTO `equipment_model_info` VALUES ('cn', 'EM5UZY4', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EM6YHF8', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMAASS9', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMAYL8P', 'weewewe', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMBU1T3', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMCJL5S', '1001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMCWE7B', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMEFBXL', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMHQ3XK', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMLJWCW', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMMBPIY', '群控配置型号', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMMGYEG', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMPEZRX', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMRSCLI', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMS1TTN', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMTJBRP', '1002', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMXBG8Z', '1001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMYBILA', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMYG5DK', '001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMYT83S', '1001', null);
INSERT INTO `equipment_model_info` VALUES ('cn', 'EMZIKIK', '001', null);

-- ----------------------------
-- Table structure for equipment_state_log
-- ----------------------------
DROP TABLE IF EXISTS `equipment_state_log`;
CREATE TABLE `equipment_state_log` (
  `code` varchar(20) NOT NULL COMMENT '编码',
  `state` int(11) NOT NULL COMMENT '状态：0-离线，1-正常，2-停机，3-故障',
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipment_state_log
-- ----------------------------

-- ----------------------------
-- Table structure for equipments_attr
-- ----------------------------
DROP TABLE IF EXISTS `equipments_attr`;
CREATE TABLE `equipments_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(20) NOT NULL COMMENT '分类编码',
  `custom_tag` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(200) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='设备附加属性表';

-- ----------------------------
-- Records of equipments_attr
-- ----------------------------
INSERT INTO `equipments_attr` VALUES ('3', ' EQ4U992', 'width', '宽度', '500', '1', '1480135879000');
INSERT INTO `equipments_attr` VALUES ('4', 'EQ1CU8E', 'height', '高度', '300', '1', '1480135879000');
INSERT INTO `equipments_attr` VALUES ('5', 'EQ677AP', 'height', '高度', '1', '1', '1480137235000');
INSERT INTO `equipments_attr` VALUES ('6', 'EQ677AP', 'width', '宽度', '2', '1', '1480137235000');
INSERT INTO `equipments_attr` VALUES ('9', 'EQQ8L2R', 'width', '宽', '160', '1', '1480927665000');
INSERT INTO `equipments_attr` VALUES ('10', 'EQQ8L2R', 'height', '高', '32', '1', '1480927665000');
INSERT INTO `equipments_attr` VALUES ('11', 'EQ5Z91M', 'height', '高度', '100', '1', '1481007948000');
INSERT INTO `equipments_attr` VALUES ('12', 'EQ5Z91M', 'width', '宽度', '200', '1', '1481007948000');
INSERT INTO `equipments_attr` VALUES ('13', 'EQARHFX', 'width', 'kuan', '1111', '1', '1481008506000');
INSERT INTO `equipments_attr` VALUES ('14', 'EQARHFX', 'height', 'gao', '2222', '1', '1481008506000');
INSERT INTO `equipments_attr` VALUES ('15', 'EQRER2C', 'height', '高度', '32', '1', '1481009187000');
INSERT INTO `equipments_attr` VALUES ('16', 'EQRER2C', 'width', '宽度', '160', '1', '1481009187000');
INSERT INTO `equipments_attr` VALUES ('17', 'EQWBK8A', 'height', '高度', '32', '1', '1481011584000');
INSERT INTO `equipments_attr` VALUES ('18', 'EQWBK8A', 'width', '宽度', '160', '1', '1481011584000');
INSERT INTO `equipments_attr` VALUES ('19', 'EQX7KTS', 'height', '高度', '45', '1', '1481103885000');
INSERT INTO `equipments_attr` VALUES ('20', 'EQX7KTS', 'width', '宽度', '36', '1', '1481103885000');
INSERT INTO `equipments_attr` VALUES ('21', 'EQMM13V', 'height', '高度', '500', '1', '1481186263000');
INSERT INTO `equipments_attr` VALUES ('22', 'EQMM13V', 'width', '宽度', '100', '1', '1481186263000');
INSERT INTO `equipments_attr` VALUES ('23', 'EQQQ2SD', 'height', '高度', '100', '1', '1481708741000');
INSERT INTO `equipments_attr` VALUES ('24', 'EQQQ2SD', 'width', '宽度', '20', '1', '1481708741000');
INSERT INTO `equipments_attr` VALUES ('25', 'EQ4C8E3', 'height', '高度', '100', '1', '1481812130000');
INSERT INTO `equipments_attr` VALUES ('26', 'EQ4C8E3', 'width', '宽度', '20', '1', '1481812130000');
INSERT INTO `equipments_attr` VALUES ('27', 'EQM1FNL', 'height', '高度', '160', '1', '1482111982000');
INSERT INTO `equipments_attr` VALUES ('28', 'EQM1FNL', 'width', '宽度', '32', '1', '1482111982000');
INSERT INTO `equipments_attr` VALUES ('29', 'EQ976WZ', 'height', '高度', '32', '1', '1482114939000');
INSERT INTO `equipments_attr` VALUES ('30', 'EQ976WZ', 'width', '宽度', '160', '1', '1482114939000');
INSERT INTO `equipments_attr` VALUES ('31', 'EQI1JYC', 'height', '高度', '111', '1', '1482204180000');
INSERT INTO `equipments_attr` VALUES ('32', 'EQI1JYC', 'width', '宽度', '2222', '1', '1482204180000');
INSERT INTO `equipments_attr` VALUES ('33', 'EQPNVFC', 'height', '高度', '43', '1', '1482209244000');
INSERT INTO `equipments_attr` VALUES ('34', 'EQPNVFC', 'width', '宽度', '545', '1', '1482209244000');
INSERT INTO `equipments_attr` VALUES ('35', 'EQFNFIU', 'height', '高度', '1111', '1', '1482215086000');
INSERT INTO `equipments_attr` VALUES ('36', 'EQFNFIU', 'ip', '地址', '2222', '1', '1482215086000');
INSERT INTO `equipments_attr` VALUES ('37', 'EQFNFIU', 'width', '宽度', '111111', '1', '1482215086000');
INSERT INTO `equipments_attr` VALUES ('38', 'EQSAJ1J', 'height', '高度', '2', '1', '1482219083000');
INSERT INTO `equipments_attr` VALUES ('39', 'EQSAJ1J', 'ip', '地址', '3', '1', '1482219083000');
INSERT INTO `equipments_attr` VALUES ('40', 'EQSAJ1J', 'width', '宽度', '4', '1', '1482219083000');
INSERT INTO `equipments_attr` VALUES ('41', 'EQKB6L7', 'height', '高度', '1', '1', '1482223930000');
INSERT INTO `equipments_attr` VALUES ('42', 'EQKB6L7', 'ip', '地址', '2', '1', '1482223931000');
INSERT INTO `equipments_attr` VALUES ('43', 'EQKB6L7', 'width', '宽度', '3', '1', '1482223931000');
INSERT INTO `equipments_attr` VALUES ('44', 'EQHIPKF', 'height', '高度', '1', '1', '1482228390000');
INSERT INTO `equipments_attr` VALUES ('45', 'EQHIPKF', 'ip', '地址', '2', '1', '1482228391000');
INSERT INTO `equipments_attr` VALUES ('46', 'EQHIPKF', 'width', '宽度', '4', '1', '1482228391000');
INSERT INTO `equipments_attr` VALUES ('53', 'EQ2V8EG', 'height', '高度', '12', '1', '1482301092000');
INSERT INTO `equipments_attr` VALUES ('54', ' EQ4U992', 'ip', '地址', '34', '1', '1482301092000');
INSERT INTO `equipments_attr` VALUES ('55', ' ', 'width', '宽度', '44', '1', '1482301092000');
INSERT INTO `equipments_attr` VALUES ('65', ' ', 'height', '高度', '1', '1', '1482310123000');
INSERT INTO `equipments_attr` VALUES ('66', ' ', 'ip', '地址', '2', '1', '1482310123000');
INSERT INTO `equipments_attr` VALUES ('67', ' ', 'width', '宽度', '666666666', '1', '1482310123000');
INSERT INTO `equipments_attr` VALUES ('68', ' ', 'height', '高度', '1', '1', '1482990395000');
INSERT INTO `equipments_attr` VALUES ('69', ' ', 'ip', '地址', '2', '1', '1482990395000');
INSERT INTO `equipments_attr` VALUES ('70', ' ', 'width', '宽度', '3', '1', '1482990395000');
INSERT INTO `equipments_attr` VALUES ('71', ' ', 'height', '高度', '1', '1', '1483006928000');
INSERT INTO `equipments_attr` VALUES ('72', ' ', 'ip', '地址', '2', '1', '1483006928000');
INSERT INTO `equipments_attr` VALUES ('73', ' ', 'width', '宽度', '3', '1', '1483006928000');
INSERT INTO `equipments_attr` VALUES ('74', ' ', 'height', '高度', '32', '1', '1483496091000');
INSERT INTO `equipments_attr` VALUES ('75', ' ', 'width', '宽度', '160', '1', '1483496091000');
INSERT INTO `equipments_attr` VALUES ('76', ' ', 'ip', 'ip地址', '192.168.1.66', '1', '1483496091000');
INSERT INTO `equipments_attr` VALUES ('77', ' ', 'port', '端口', '2929', '1', '1483496092000');

-- ----------------------------
-- Table structure for equipments_category
-- ----------------------------
DROP TABLE IF EXISTS `equipments_category`;
CREATE TABLE `equipments_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oem_code` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `parent_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipments_category
-- ----------------------------
INSERT INTO `equipments_category` VALUES ('1', 'CUXM9D8', 'FXKBQBB', '0', '3', '1', '1480134971000', '1482214927000');
INSERT INTO `equipments_category` VALUES ('3', 'CUXM9D8', 'CDZSQ', '0', '3', '1', '1480135108000', null);
INSERT INTO `equipments_category` VALUES ('4', 'CUXM9D8', 'COJCQ', '0', '1', '9', '1480135158000', '1481117534000');
INSERT INTO `equipments_category` VALUES ('5', 'CUXM9D8', 'DNZM', '0', '2', '1', '1480135198000', '1481263867000');
INSERT INTO `equipments_category` VALUES ('6', 'CUXM9D8', 'DWZM', '0', '3', '1', '1480135208000', null);
INSERT INTO `equipments_category` VALUES ('7', 'CUXM9D8', 'HDM', '0', '3', '1', '1480135224000', '1481263064000');
INSERT INTO `equipments_category` VALUES ('8', 'CUXM9D8', 'JSB', '0', '3', '1', '1480135245000', null);
INSERT INTO `equipments_category` VALUES ('9', 'CUXM9D8', 'JTXHD', '0', '3', '1', '1480135250000', null);
INSERT INTO `equipments_category` VALUES ('10', 'CUXM9D8', 'MJSKBQBB', '0', '3', '1', '1480135261000', null);
INSERT INTO `equipments_category` VALUES ('11', 'CUXM9D8', 'QSSXT', '0', '3', '1', '1480135268000', null);
INSERT INTO `equipments_category` VALUES ('12', 'CUXM9D8', 'QXSXT', '0', '3', '1', '1480135289000', null);
INSERT INTO `equipments_category` VALUES ('13', 'CUXM9D8', 'SLFJ', '0', '3', '1', '1480135297000', '1481262270000');
INSERT INTO `equipments_category` VALUES ('14', 'CUXM9D8', 'VIJCQ', '0', '3', '9', '1480135305000', '1481263033000');
INSERT INTO `equipments_category` VALUES ('15', 'CUXM9D8', 'WBCLJCQ', '0', '3', '9', '1480135312000', '1481263314000');
INSERT INTO `equipments_category` VALUES ('18', 'CUXM9D8', 'ECA66CT', '0', '1', '9', '1480927549000', '1481263334000');
INSERT INTO `equipments_category` VALUES ('19', 'CUXM9D8', 'ECBXWZ5', '0', '1', '9', '1481008440000', null);
INSERT INTO `equipments_category` VALUES ('21', 'CUXM9D8', 'ECRH3I3', '0', '1', '9', '1481166968000', '1481166983000');
INSERT INTO `equipments_category` VALUES ('22', 'CUXM9D8', 'ECYGPSI', '0', '1', '9', '1481167121000', '1481167139000');
INSERT INTO `equipments_category` VALUES ('23', 'CUXM9D8', 'EC3JVIX', '0', '1', '9', '1481256785000', null);
INSERT INTO `equipments_category` VALUES ('24', 'CUXM9D8', 'ECPA799', '0', '1', '9', '1481263265000', null);
INSERT INTO `equipments_category` VALUES ('25', 'CUXM9D8', 'ECC5QEA', '0', '1', '9', '1481263357000', '1481263451000');
INSERT INTO `equipments_category` VALUES ('26', 'CUXM9D8', 'ECT1SVT', '0', '1', '9', '1481263532000', null);
INSERT INTO `equipments_category` VALUES ('27', 'CUXM9D8', 'ECFTC18', '0', '1', '1', '1481263619000', null);
INSERT INTO `equipments_category` VALUES ('28', 'CUXM9D8', 'EC1J6UF', '0', '1', '9', '1481263844000', null);
INSERT INTO `equipments_category` VALUES ('29', 'CUXM9D8', 'EC4HBZ2', '0', '1', '1', '1481276420000', '1481277106000');
INSERT INTO `equipments_category` VALUES ('30', 'CUXM9D8', 'ECZL8MM', '0', '1', '9', '1481276805000', null);
INSERT INTO `equipments_category` VALUES ('31', 'CUXM9D8', 'ECN1YUR', '0', '1', '1', '1481279418000', '1481513241000');
INSERT INTO `equipments_category` VALUES ('32', 'CUXM9D8', 'QSSXT111', '0', '4', '9', '1482476359000', null);

-- ----------------------------
-- Table structure for equipments_category_attr
-- ----------------------------
DROP TABLE IF EXISTS `equipments_category_attr`;
CREATE TABLE `equipments_category_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(20) NOT NULL COMMENT '分类编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `custom_tag` varchar(20) NOT NULL,
  `is_require` int(11) NOT NULL COMMENT '是否必填：0-否，1-是',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='设备分类附加属性表';

-- ----------------------------
-- Records of equipments_category_attr
-- ----------------------------
INSERT INTO `equipments_category_attr` VALUES ('1', 'FXKBQBB', 'ECAX82S9', 'width', '1', '9', '1480135074000');
INSERT INTO `equipments_category_attr` VALUES ('2', 'FXKBQBB', 'ECAMH835', 'width', '1', '1', '1480135158000');
INSERT INTO `equipments_category_attr` VALUES ('3', 'FXKBQBB', 'ECACQ25B', 'height', '1', '1', '1480135158000');
INSERT INTO `equipments_category_attr` VALUES ('4', 'MJSKBQBB', 'ECA5GPS8', 'width', '1', '1', '1480135198000');
INSERT INTO `equipments_category_attr` VALUES ('5', 'MJSKBQBB', 'ECAWXEY8', 'height', '1', '1', '1480135198000');
INSERT INTO `equipments_category_attr` VALUES ('6', 'ECA66CT', 'ECA68BLB', 'width', '1', '1', '1480927549000');
INSERT INTO `equipments_category_attr` VALUES ('7', 'ECA66CT', 'ECA8MFGA', 'height', '1', '1', '1480927549000');
INSERT INTO `equipments_category_attr` VALUES ('8', 'ECBXWZ5', 'ECA5T46T', 'width', '1', '1', '1481008440000');
INSERT INTO `equipments_category_attr` VALUES ('9', 'ECBXWZ5', 'ECAAVS8U', 'height', '1', '1', '1481008440000');
INSERT INTO `equipments_category_attr` VALUES ('10', 'ECYGPSI', 'ECAFIX72', '下次再v', '1', '1', '1481167121000');
INSERT INTO `equipments_category_attr` VALUES ('11', 'EC3JVIX', 'ECA7YEPF', '3123123', '0', '1', '1481256785000');
INSERT INTO `equipments_category_attr` VALUES ('12', 'EC3JVIX', 'ECAR2YEU', '12312312', '0', '1', '1481256785000');
INSERT INTO `equipments_category_attr` VALUES ('13', 'EC4HBZ2', 'ECAL2K9B', '2', '0', '1', '1481276420000');
INSERT INTO `equipments_category_attr` VALUES ('14', 'EC4HBZ2', 'ECAT28TA', 'w', '0', '1', '1481276420000');
INSERT INTO `equipments_category_attr` VALUES ('15', 'EC4HBZ2', 'ECAVMRD3', '是的发生d', '0', '1', '1481276420000');
INSERT INTO `equipments_category_attr` VALUES ('16', 'ECZL8MM', 'ECAWGESD', '地对地导弹', '0', '1', '1481276805000');
INSERT INTO `equipments_category_attr` VALUES ('17', 'ECN1YUR', 'ECANIBM7', 'asxasx', '0', '1', '1481279418000');
INSERT INTO `equipments_category_attr` VALUES ('18', 'ECN1YUR', 'ECAGJBPF', 'xsxsxsxasds356', '0', '1', '1481279418000');
INSERT INTO `equipments_category_attr` VALUES ('19', 'FXKBQBB', 'ECAFQRHW', 'ip', '1', '1', '1482214928000');

-- ----------------------------
-- Table structure for equipments_category_attr_info
-- ----------------------------
DROP TABLE IF EXISTS `equipments_category_attr_info`;
CREATE TABLE `equipments_category_attr_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备分类附加属性信息表';

-- ----------------------------
-- Records of equipments_category_attr_info
-- ----------------------------
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECA5GPS8', '宽度', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECA5T46T', 'kuan', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECA68BLB', '宽', '1');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECA7YEPF', '12312', '13123123');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECA8MFGA', '高', '2');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAAVS8U', 'gao', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECACQ25B', '高度', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAFIX72', '萨菲的', '暗室逢灯');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAFQRHW', '地址', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAGJBPF', 'xsxsx', 'xsxsxs123213');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAL2K9B', '1', '4');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAMH835', '宽度', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECANIBM7', 'asxasx', 'xsxsx');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAR2YEU', '123123123', '3123123123');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAT28TA', '亲爱', 'e');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAVMRD3', '撒地方', '444');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAWGESD', '撒旦法士大夫', '3333');
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAWXEY8', '高度', null);
INSERT INTO `equipments_category_attr_info` VALUES ('cn', 'ECAX82S9', '宽度', null);

-- ----------------------------
-- Table structure for equipments_category_info
-- ----------------------------
DROP TABLE IF EXISTS `equipments_category_info`;
CREATE TABLE `equipments_category_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of equipments_category_info
-- ----------------------------
INSERT INTO `equipments_category_info` VALUES ('cn', 'CDZSQ', '车道指示器', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'COJCQ', 'CO/VI', '');
INSERT INTO `equipments_category_info` VALUES ('cn', 'DNZM', '洞内基本照明', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'DWZM', '洞外照明', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'EC1J6UF', '111', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'EC3JVIX', '1231', '123123');
INSERT INTO `equipments_category_info` VALUES ('cn', 'EC4HBZ2', '12312312', '请问请问我去');
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECA66CT', '群控配置分类111', '');
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECBXWZ5', '1212121212', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECC5QEA', '1112222', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECFTC18', 'CO/VI', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECN1YUR', 'ssxdee', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECPA799', 'qweqwe', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECRH3I3', '吃v字形吃v字形才', '不v刹保存v吧');
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECT1SVT', 'qweqweqwe', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECYGPSI', '艾丝凡', '周星驰v');
INSERT INTO `equipments_category_info` VALUES ('cn', 'ECZL8MM', '额外热污染', '撒的发生的');
INSERT INTO `equipments_category_info` VALUES ('cn', 'FXKBQBB', 'F型可变情报板', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'HDM', '横洞门', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'JSB', '给水泵', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'JTXHD', '交通信号灯', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'MJSKBQBB', '门架式可变情报板', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'QSSXT', '枪式摄像头', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'QSSXT111', '111', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'QXSXT', '球形摄像头', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'SLFJ', '射流风机', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'VIJCQ', 'VI', null);
INSERT INTO `equipments_category_info` VALUES ('cn', 'WBCLJCQ', '微波车辆检测器', null);

-- ----------------------------
-- Table structure for fault_code
-- ----------------------------
DROP TABLE IF EXISTS `fault_code`;
CREATE TABLE `fault_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oem_code` varchar(20) NOT NULL COMMENT '设备制造商编码',
  `library_code` varchar(20) NOT NULL COMMENT '翻译库编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `fault_code` varchar(20) NOT NULL COMMENT '故障码',
  `type` int(11) DEFAULT NULL COMMENT '分类',
  `is_create_order` int(11) DEFAULT NULL COMMENT '是否产生工单',
  `level` int(11) DEFAULT NULL COMMENT '告警级别1.严重2紧急3一般',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_code
-- ----------------------------
INSERT INTO `fault_code` VALUES ('1', 'CUXM9D8', 'TLA4NJU', 'FAJXEVE', '10000', '1', '1', '1', '1', '1480136405000', '1480476377000');
INSERT INTO `fault_code` VALUES ('2', 'CUXM9D8', 'TL944B8', 'FADDVGM', '10001', '1', '1', '2', '1', '1480136478000', null);
INSERT INTO `fault_code` VALUES ('3', 'CUXM9D8', 'TLA4NJU', 'FACWQ19', '10001', '1', '1', '3', '1', '1480136507000', '1480496868000');
INSERT INTO `fault_code` VALUES ('4', 'CUXM9D8', 'TLA4NJU', 'FAPZLF3', '10002', '1', '1', '1', '1', '1480496902000', null);
INSERT INTO `fault_code` VALUES ('5', 'CUXM9D8', 'TLA4NJU', 'FA8CBSQ', '10003', '1', '1', '2', '1', '1480497201000', null);
INSERT INTO `fault_code` VALUES ('6', 'CUXM9D8', 'TLA4NJU', 'FAQCQ87', '10004', '1', '1', '3', '1', '1480497224000', '1480497237000');
INSERT INTO `fault_code` VALUES ('7', 'CUXM9D8', 'TLA4NJU', 'FA9483K', '10005', '1', '1', '1', '1', '1480497257000', '1482139295000');
INSERT INTO `fault_code` VALUES ('8', 'CUXM9D8', 'TLA4NJU', 'FAE3L1G', '10006', '1', '1', '2', '1', '1480497278000', '1482139258000');
INSERT INTO `fault_code` VALUES ('9', 'CUXM9D8', 'TLA4NJU', 'FARZ9XL', '10007', '1', '1', '3', '1', '1480497306000', '1482139252000');
INSERT INTO `fault_code` VALUES ('13', 'CUXM9D8', 'TL944B8', 'FANXL2B', '22', '1', '1', '1', '1', '1481968431000', '1481968586000');
INSERT INTO `fault_code` VALUES ('14', 'CUXM9D8', 'TL944B8', 'FAPLQJQ', '11', '1', '1', '1', '1', '1481968897000', null);

-- ----------------------------
-- Table structure for fault_code_info
-- ----------------------------
DROP TABLE IF EXISTS `fault_code_info`;
CREATE TABLE `fault_code_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `fault_name` varchar(255) NOT NULL COMMENT '故障名称',
  `description` text COMMENT '描述',
  `handling_suggestion` text COMMENT '处理建议',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_code_info
-- ----------------------------
INSERT INTO `fault_code_info` VALUES ('cn', 'FA8CBSQ', '', '雨天路滑', '请减速慢行', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FA9483K', '5', '风机故障', '请检查电路', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FACWQ19', '', '温度过高', '降低温度', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FADDVGM', '', '向左滑动500米', '停止', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FAE3L1G', '4', '空调故障', '请检查线路', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FAJXEVE', '', '向左滑动500米', '向右滑动', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FANXL2B', '22', '33', '44', '55');
INSERT INTO `fault_code_info` VALUES ('cn', 'FAPLQJQ', '名称', '22', '33', '44');
INSERT INTO `fault_code_info` VALUES ('cn', 'FAPZLF3', '', '湿度多高', '除湿', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FAQCQ87', '', '前方道路塌方', '请绕道而行', null);
INSERT INTO `fault_code_info` VALUES ('cn', 'FARZ9XL', '3', '照明故障', '请检查线路', null);

-- ----------------------------
-- Table structure for fault_stat
-- ----------------------------
DROP TABLE IF EXISTS `fault_stat`;
CREATE TABLE `fault_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `production_line_code` varchar(20) NOT NULL COMMENT '产线编码',
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `fault_channel_code` varchar(20) DEFAULT NULL,
  `alarm_order_code` varchar(30) DEFAULT NULL,
  `fault_code` varchar(20) NOT NULL COMMENT '故障码',
  `failure_time` varchar(32) NOT NULL COMMENT '发生时间',
  `recovery_time` varchar(32) DEFAULT NULL COMMENT '恢复时间',
  `duration` varchar(32) DEFAULT NULL COMMENT '故障持续时长',
  `user_code` varchar(20) NOT NULL COMMENT '处理人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_stat
-- ----------------------------

-- ----------------------------
-- Table structure for fault_translation_library
-- ----------------------------
DROP TABLE IF EXISTS `fault_translation_library`;
CREATE TABLE `fault_translation_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oem_code` varchar(20) NOT NULL COMMENT '设备制造商编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `type` int(11) DEFAULT NULL COMMENT '1-通用库，2-自定义库''',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_translation_library
-- ----------------------------
INSERT INTO `fault_translation_library` VALUES ('1', 'CUXM9D8', 'TLA4NJU', '2', '1', '1479967243000', '1480063591000');
INSERT INTO `fault_translation_library` VALUES ('2', 'CUXM9D8', 'TL944B8', '2', '1', '1480136444000', null);

-- ----------------------------
-- Table structure for fault_translation_library_info
-- ----------------------------
DROP TABLE IF EXISTS `fault_translation_library_info`;
CREATE TABLE `fault_translation_library_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_translation_library_info
-- ----------------------------
INSERT INTO `fault_translation_library_info` VALUES ('cn', 'TL944B8', '高速公路故障码翻译库2', null);
INSERT INTO `fault_translation_library_info` VALUES ('cn', 'TLA4NJU', '高速公路故障翻译库', null);

-- ----------------------------
-- Table structure for fault_view
-- ----------------------------
DROP TABLE IF EXISTS `fault_view`;
CREATE TABLE `fault_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `fault_code` varchar(20) NOT NULL COMMENT '公司编号',
  `view_code` varchar(20) NOT NULL COMMENT '用户组编号',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of fault_view
-- ----------------------------

-- ----------------------------
-- Table structure for fault_work_order
-- ----------------------------
DROP TABLE IF EXISTS `fault_work_order`;
CREATE TABLE `fault_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(50) NOT NULL,
  `equipment_code` varchar(20) NOT NULL,
  `fault_channel_code` varchar(20) NOT NULL COMMENT '公司编码',
  `fault_code` varchar(20) NOT NULL,
  `description` text,
  `handing_suggestion` text,
  `first_time` varchar(0) NOT NULL,
  `count` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='故障工单详情表';

-- ----------------------------
-- Records of fault_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for help_work_order
-- ----------------------------
DROP TABLE IF EXISTS `help_work_order`;
CREATE TABLE `help_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(50) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `equipment_code` varchar(20) DEFAULT NULL,
  `sponsor` varchar(20) NOT NULL,
  `order_status` int(11) NOT NULL,
  `padding_by` varchar(20) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '工单类型 1.设备工单 2.求助工单',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of help_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for help_work_order_info
-- ----------------------------
DROP TABLE IF EXISTS `help_work_order_info`;
CREATE TABLE `help_work_order_info` (
  `language` varchar(32) NOT NULL COMMENT '编号',
  `code` varchar(50) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of help_work_order_info
-- ----------------------------

-- ----------------------------
-- Table structure for inspection_work_order
-- ----------------------------
DROP TABLE IF EXISTS `inspection_work_order`;
CREATE TABLE `inspection_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `equipment_code` varchar(20) NOT NULL COMMENT '设备编码',
  `inspection_code` varchar(20) NOT NULL COMMENT '巡检任务编码',
  `inspection_date` date NOT NULL COMMENT '巡检日期',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备巡检工单详情';

-- ----------------------------
-- Records of inspection_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `reference_value` decimal(10,0) NOT NULL COMMENT '成本参考值',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of material
-- ----------------------------

-- ----------------------------
-- Table structure for material_info
-- ----------------------------
DROP TABLE IF EXISTS `material_info`;
CREATE TABLE `material_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of material_info
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_view
-- ----------------------------
DROP TABLE IF EXISTS `monitor_view`;
CREATE TABLE `monitor_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) NOT NULL COMMENT '公司编码',
  `user_code` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  `config` varchar(1000) DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `compare_customers` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of monitor_view
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_view_info
-- ----------------------------
DROP TABLE IF EXISTS `monitor_view_info`;
CREATE TABLE `monitor_view_info` (
  `language` varchar(32) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of monitor_view_info
-- ----------------------------

-- ----------------------------
-- Table structure for point
-- ----------------------------
DROP TABLE IF EXISTS `point`;
CREATE TABLE `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `object_code` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `range` int(11) NOT NULL COMMENT ' ''监控范围：1-产线，2-设备'',',
  `type` varchar(10) DEFAULT NULL,
  `index_type` varchar(20) DEFAULT NULL COMMENT '指标分类',
  `config` varchar(4000) DEFAULT NULL,
  `bom_code` varchar(20) DEFAULT NULL,
  `is_productivity` int(11) DEFAULT NULL COMMENT '是否为产能点位',
  `is_crucial` int(11) DEFAULT NULL COMMENT '是否关键点位',
  `view_type` int(11) DEFAULT NULL COMMENT '数据展示类型：1-实时曲线，2-仪表盘',
  `relation_equipment` varchar(20) DEFAULT NULL COMMENT '关联设备(运行|停止)',
  `order_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of point
-- ----------------------------
INSERT INTO `point` VALUES ('1', null, 'EQ48PER', 'Dl3lsgkk', '2', 'sz', 'SITL7AU', '关|$|开|$|red|$|green', null, '1', '0', null, null, '1', '1', '1480137864000', null);
INSERT INTO `point` VALUES ('2', null, 'EQMN9LC', 'Dw5k3828', '2', 'sz', 'SITL7AU', '关|$|开|$|red|$|green', null, '1', '0', null, null, '1', '1', '1480137900000', null);
INSERT INTO `point` VALUES ('3', null, 'EQRUWQ1', 'Dybfrx9y', '2', 'sz', 'SITBE8G', '关|$|开|$|red|$|yellow', null, '1', '0', null, null, '1', '1', '1480140059000', null);
INSERT INTO `point` VALUES ('4', null, 'EQRUWQ1', 'Ahe66ang', '2', 'mn', 'SITRCT7', 'V|$|2|$|3|$|4|$|5', null, '1', '0', null, null, '6', '1', '1480143988000', null);
INSERT INTO `point` VALUES ('5', null, 'EQMM13V', 'Dwa3ru5h', '2', 'sz', 'SITL7AU', '关机|$|开机|$|red|$|green', null, '1', '0', null, null, '1', '1', '1481686283000', null);
INSERT INTO `point` VALUES ('6', null, 'EQGIC49', 'Aaq77ptc', '2', 'mn', 'SITZLDU', 'undefined|$|1|$|10|$||$|', null, '1', '0', null, null, '1', '1', '1482308443000', null);
INSERT INTO `point` VALUES ('7', null, 'EQGIC49', 'A7d9mhvj', '2', 'mn', 'SITRCT7', 'V|$|1|$|10|$||$|', null, '1', '0', null, null, '1', '1', '1482310124000', null);
INSERT INTO `point` VALUES ('8', null, 'EQFNFIU', 'Akud427v', '2', 'mn', 'SITRCT7', '单位|$|1|$|10|$||$|', null, '1', '0', null, null, '1', '1', '1482310513000', '1482311082000');
INSERT INTO `point` VALUES ('9', null, 'EQFNFIU', 'Apsfq4rk', '2', 'mn', 'SITRCT7', 'V|$|1|$|10|$||$|', null, '1', '0', null, null, '1', '9', '1482311154000', null);
INSERT INTO `point` VALUES ('10', null, 'EQFNFIU', 'Abvnknjn', '2', 'mn', 'SITRCT7', '测试单位|$|1|$|10|$||$|', null, '1', '0', null, null, '1', '9', '1482311767000', null);
INSERT INTO `point` VALUES ('11', null, 'EQFNFIU', 'D75m7rhl', '2', 'sz', 'SITRCT7', '0|$|1|$|red|$|green', null, '1', '0', null, null, '2', '9', '1482312604000', null);
INSERT INTO `point` VALUES ('12', null, 'EQASVAS', 'Aeru83ge', '2', 'mn', 'SITRCT7', '单位|$|1|$|10|$|1|$|', null, '1', '0', null, null, '1', '9', '1482812211000', null);

-- ----------------------------
-- Table structure for point_info
-- ----------------------------
DROP TABLE IF EXISTS `point_info`;
CREATE TABLE `point_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of point_info
-- ----------------------------
INSERT INTO `point_info` VALUES ('cn', 'A7d9mhvj', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'Aaq77ptc', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'Abvnknjn', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'Aeru83ge', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'Ahe66ang', '测试点位', null);
INSERT INTO `point_info` VALUES ('cn', 'Akud427v', 'ces', null);
INSERT INTO `point_info` VALUES ('cn', 'Apsfq4rk', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'D75m7rhl', 'ces', null);
INSERT INTO `point_info` VALUES ('cn', 'Dl3lsgkk', '枪式摄像头点位', null);
INSERT INTO `point_info` VALUES ('cn', 'Dw5k3828', '球形摄像头', null);
INSERT INTO `point_info` VALUES ('cn', 'Dwa3ru5h', '测试', null);
INSERT INTO `point_info` VALUES ('cn', 'Dybfrx9y', '测试单位1', null);

-- ----------------------------
-- Table structure for prevention_work_order
-- ----------------------------
DROP TABLE IF EXISTS `prevention_work_order`;
CREATE TABLE `prevention_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `equipment_code` varchar(20) NOT NULL,
  `sparepart_code` varchar(20) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '维护类型：1-保养，2-备件更换',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预防性维护工单详情';

-- ----------------------------
-- Records of prevention_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `material_code` varchar(20) NOT NULL COMMENT '原料编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `standard` varchar(50) NOT NULL COMMENT '规格',
  `price` decimal(8,2) NOT NULL COMMENT '售价',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of product_info
-- ----------------------------

-- ----------------------------
-- Table structure for production_line
-- ----------------------------
DROP TABLE IF EXISTS `production_line`;
CREATE TABLE `production_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `section_code` varchar(20) NOT NULL,
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of production_line
-- ----------------------------

-- ----------------------------
-- Table structure for production_line_equipment
-- ----------------------------
DROP TABLE IF EXISTS `production_line_equipment`;
CREATE TABLE `production_line_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `production_line_code` varchar(20) NOT NULL COMMENT '编码',
  `equipment_code` varchar(20) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of production_line_equipment
-- ----------------------------

-- ----------------------------
-- Table structure for production_line_info
-- ----------------------------
DROP TABLE IF EXISTS `production_line_info`;
CREATE TABLE `production_line_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '名称',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of production_line_info
-- ----------------------------

-- ----------------------------
-- Table structure for production_schedule
-- ----------------------------
DROP TABLE IF EXISTS `production_schedule`;
CREATE TABLE `production_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(32) DEFAULT NULL,
  `customer_code` varchar(20) NOT NULL COMMENT '客户编码',
  `production_line_code` varchar(20) NOT NULL COMMENT '生产线编码',
  `material_code` varchar(20) NOT NULL COMMENT '原料编码',
  `product_code` varchar(20) NOT NULL COMMENT '产物编码',
  `begin_date` varchar(32) NOT NULL COMMENT '生产开始日期',
  `end_date` varchar(32) NOT NULL COMMENT '生产结束日期',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of production_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for real_time_data
-- ----------------------------
DROP TABLE IF EXISTS `real_time_data`;
CREATE TABLE `real_time_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) DEFAULT NULL COMMENT '公司编号',
  `value` varchar(20) DEFAULT NULL COMMENT '部门编号',
  `time` varchar(20) DEFAULT NULL COMMENT '资源编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of real_time_data
-- ----------------------------

-- ----------------------------
-- Table structure for screen
-- ----------------------------
DROP TABLE IF EXISTS `screen`;
CREATE TABLE `screen` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `index` int(11) NOT NULL COMMENT '大屏编号',
  `camera_code` varchar(20) DEFAULT NULL COMMENT '摄像头编码',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='大屏表';

-- ----------------------------
-- Records of screen
-- ----------------------------
INSERT INTO `screen` VALUES ('1', 'CRNR2H9', '枪式摄像头001', '1', 'EQ4U992', '1', '123', '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('2', 'CRYB2RH', '', '2', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('3', 'CRQE8SB\r\n', '球形摄像头001', '3', 'EQDQ4CC', '1', null, '1483528046000', '1483528046000');
INSERT INTO `screen` VALUES ('4', 'CR266EP', '', '4', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('5', 'CRV6SKY', '', '5', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('6', 'CRERLGV\r\n', '球形摄像头001', '6', 'EQDQ4CC', '1', null, '1483528079000', '1483528079000');
INSERT INTO `screen` VALUES ('7', 'CRJYFCN', '', '7', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('8', 'CREFK42\r\n', '', '8', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('9', 'CRBWRHW', '', '9', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('10', 'CRK4SHZ\r\n', '', '10', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('11', 'CRBKISK', '', '11', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('12', 'CRKVYQM', '', '12', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('13', 'CR3Y5A1', '', '13', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('14', 'CRT26HM\r\n', '', '14', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('15', 'CRDEE9L\r\n', '', '15', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('16', 'CRBRSBM', '', '16', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('17', 'CRXUQHQ', '', '17', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('18', 'CRKCMZ9', '', '18', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('19', 'CR86VB7', '', '19', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('20', 'CR2KQRH', '', '20', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('21', 'CR6KIP1', '', '21', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('22', 'CRU54MX', '', '22', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('23', 'CRA2LQ8', '', '23', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('24', 'CRA2WDP', '', '24', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('25', 'CRU55ED', '', '25', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('26', 'CRRRELJ', '', '26', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('27', 'CRVZDHN', '', '27', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('28', 'CRN2Y2Y\r\n', '', '28', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('29', 'CRGZA58\r\n', '', '29', null, '0', null, '1481008506000', '1481008506000');
INSERT INTO `screen` VALUES ('30', 'CR27BL1', '', '30', null, '0', null, '1481008506000', '1481008506000');

-- ----------------------------
-- Table structure for section
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `type` varchar(10) NOT NULL COMMENT '类型：expy-高速路段，tunnel-隧道',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `address` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `location` varchar(50) DEFAULT NULL COMMENT '位置',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='路段表';

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', 'CUXYEC3', 'tunnel', '隧道监控-110KM', null, null, null, '30.155713,105.11485', '1', null, '1480134936000', '1482981617000');
INSERT INTO `section` VALUES ('2', 'CUWSTC6', 'expy', '高速公路-G6', null, null, null, '30.564557,104.450248', '1', null, '1480149558000', '1482981591000');
INSERT INTO `section` VALUES ('3', 'CUPYYBU', 'expy', '高速公路-G4', '重庆', '重庆', '338KM处', '29.841435,105.4552', '1', null, '1480562797000', '1482981580000');
INSERT INTO `section` VALUES ('4', 'CUHJN7D', 'expy', '11', null, null, null, null, '9', null, '1481698733000', null);
INSERT INTO `section` VALUES ('5', 'CUJLRWF', 'expy', '11111', null, null, '成安渝', '30.230696683792814,105.0116482767212', '1', null, '1482114530000', '1482981550000');

-- ----------------------------
-- Table structure for sparepart_prevention_condition
-- ----------------------------
DROP TABLE IF EXISTS `sparepart_prevention_condition`;
CREATE TABLE `sparepart_prevention_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` int(11) NOT NULL,
  `formula` varchar(500) CHARACTER SET utf8 NOT NULL COMMENT '条件公式：点位编码，点位名称，运算符，值，单位|点位编码，点位名称，运算符，值，单位',
  `warning_value` int(11) NOT NULL COMMENT '预警值',
  `warning_content` text CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  `is_had_work_order` int(11) NOT NULL DEFAULT '0' COMMENT '0未生产工单，1已生成工单并且未处理',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sparepart_prevention_condition
-- ----------------------------

-- ----------------------------
-- Table structure for sparepart_prevention_config
-- ----------------------------
DROP TABLE IF EXISTS `sparepart_prevention_config`;
CREATE TABLE `sparepart_prevention_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) CHARACTER SET utf8 NOT NULL,
  `equipment_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '设备编码',
  `sparepart_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '元件code',
  `type` int(11) NOT NULL COMMENT '预防性维护类型：1-按使用时长，2-按使用次数',
  `point_code` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '关键监控点位',
  `condition_point` varchar(500) CHARACTER SET utf8 NOT NULL COMMENT '条件监控点位：格式：点位编码|点位名称|单位;点位编码|点位名称|单位',
  `warning_value` int(11) NOT NULL COMMENT '默认预警值',
  `warning_content` text CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  `is_had_work_order` int(11) DEFAULT '0' COMMENT '0未生产工单，1已生成工单并且未处理',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sparepart_prevention_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL COMMENT '编码',
  `type` varchar(10) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `phone_code` varchar(32) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('1', 'ARTI1FA', null, '0', '0086', null, '1', '1460617540000', null);
INSERT INTO `sys_area` VALUES ('2', 'ARHYXDJ', null, '1', null, null, '1', '1460617550000', null);
INSERT INTO `sys_area` VALUES ('3', 'ARF96R1', null, '2', null, null, '1', '1460617557000', null);
INSERT INTO `sys_area` VALUES ('4', 'ARLMYT4', null, '3', null, null, '1', '1460617567000', null);
INSERT INTO `sys_area` VALUES ('5', 'AR8FQ85', null, '2', null, null, '1', '1460625633000', null);
INSERT INTO `sys_area` VALUES ('6', 'ARD9X88', null, '0', null, null, '9', '1460706816000', null);
INSERT INTO `sys_area` VALUES ('7', 'ARQUMWW', null, '0', null, null, '9', '1460706847000', null);
INSERT INTO `sys_area` VALUES ('8', 'ARHPDGP', null, '1', null, null, '1', '1460707423000', '1460713775000');
INSERT INTO `sys_area` VALUES ('9', 'AR1Z4YR', null, '1', null, null, '1', '1460707431000', null);
INSERT INTO `sys_area` VALUES ('10', 'AR2AVS8', null, '1', null, null, '1', '1460707441000', null);
INSERT INTO `sys_area` VALUES ('11', 'AR7BLB9', null, '0', null, null, '9', '1460707449000', null);
INSERT INTO `sys_area` VALUES ('12', 'ARX59UF', null, '0', null, null, '9', '1460707454000', null);
INSERT INTO `sys_area` VALUES ('13', 'ARTF7F2', null, '1', null, null, '1', '1460707551000', null);
INSERT INTO `sys_area` VALUES ('14', 'ARGAZ1R', null, '1', null, null, '1', '1460707570000', null);
INSERT INTO `sys_area` VALUES ('15', 'AR8UYYZ', null, '1', null, null, '1', '1460707597000', null);
INSERT INTO `sys_area` VALUES ('16', 'ARD5RKF', null, '0', null, null, '9', '1460707603000', null);
INSERT INTO `sys_area` VALUES ('17', 'AR34HZ2', null, '1', null, null, '1', '1460707613000', null);
INSERT INTO `sys_area` VALUES ('18', 'ARPUAC3', null, '1', null, null, '1', '1460707651000', null);
INSERT INTO `sys_area` VALUES ('19', 'AR4MY32', null, '1', null, null, '1', '1460707660000', null);
INSERT INTO `sys_area` VALUES ('20', 'ARI513F', null, '1', null, null, '1', '1460707673000', null);
INSERT INTO `sys_area` VALUES ('21', 'ARYK5EY', null, '1', null, null, '1', '1460707678000', null);
INSERT INTO `sys_area` VALUES ('22', 'ARFURCC', null, '1', null, null, '1', '1460707690000', null);
INSERT INTO `sys_area` VALUES ('23', 'AR3YL7V', null, '1', null, null, '1', '1460707697000', null);
INSERT INTO `sys_area` VALUES ('24', 'ARS3EUU', null, '1', null, null, '1', '1460707705000', null);
INSERT INTO `sys_area` VALUES ('25', 'ARGB9HC', null, '1', null, null, '1', '1460707718000', null);
INSERT INTO `sys_area` VALUES ('26', 'ARDH5CI', null, '1', null, null, '1', '1460707736000', null);
INSERT INTO `sys_area` VALUES ('27', 'AR53TTT', null, '1', null, null, '1', '1460707748000', null);
INSERT INTO `sys_area` VALUES ('28', 'ARLQRCH', null, '1', null, null, '1', '1460707770000', null);
INSERT INTO `sys_area` VALUES ('29', 'ARUCUY9', null, '1', null, null, '1', '1460707793000', null);
INSERT INTO `sys_area` VALUES ('30', 'ARM7INL', null, '1', null, null, '1', '1460707813000', null);
INSERT INTO `sys_area` VALUES ('31', 'ARC9KKZ', null, '1', null, null, '1', '1460707822000', null);
INSERT INTO `sys_area` VALUES ('32', 'AR56QIE', null, '1', null, null, '1', '1460707828000', null);
INSERT INTO `sys_area` VALUES ('33', 'ARBZRJV', null, '1', null, null, '1', '1460707837000', null);
INSERT INTO `sys_area` VALUES ('34', 'ARCTPCE', null, '1', null, null, '1', '1460707844000', null);
INSERT INTO `sys_area` VALUES ('35', 'ARRHETK', null, '1', null, null, '1', '1460707870000', null);
INSERT INTO `sys_area` VALUES ('36', 'ARLFC5R', null, '1', null, null, '1', '1460707878000', null);
INSERT INTO `sys_area` VALUES ('37', 'ARZ55IL', null, '1', null, null, '1', '1460707895000', null);
INSERT INTO `sys_area` VALUES ('38', 'AR3NTM6', null, '1', null, null, '1', '1460707931000', null);
INSERT INTO `sys_area` VALUES ('39', 'AR9WB4R', null, '1', null, null, '1', '1460707963000', null);
INSERT INTO `sys_area` VALUES ('40', 'ARPGBMN', null, '1', null, null, '1', '1460707972000', '1460713797000');
INSERT INTO `sys_area` VALUES ('41', 'ARY9BEE', null, '2', null, null, '1', '1460708029000', null);
INSERT INTO `sys_area` VALUES ('42', 'ARPNVXU', null, '2', null, null, '1', '1460708038000', null);
INSERT INTO `sys_area` VALUES ('43', 'AR9QWR3', null, '2', null, null, '1', '1460708051000', null);
INSERT INTO `sys_area` VALUES ('44', 'AR8F32B', null, '2', null, null, '1', '1460708064000', null);
INSERT INTO `sys_area` VALUES ('45', 'ARNQXDD', null, '2', null, null, '1', '1460708073000', null);
INSERT INTO `sys_area` VALUES ('46', 'ARZ5GLZ', null, '2', null, null, '1', '1460708092000', null);
INSERT INTO `sys_area` VALUES ('47', 'ARGTYH2', null, '2', null, null, '1', '1460708120000', null);
INSERT INTO `sys_area` VALUES ('48', 'ARW82ZX', null, '2', null, null, '1', '1460708136000', null);
INSERT INTO `sys_area` VALUES ('49', 'ARVWGKF', null, '2', null, null, '1', '1460708146000', null);
INSERT INTO `sys_area` VALUES ('50', 'ARSWUT7', null, '2', null, null, '1', '1460708155000', null);
INSERT INTO `sys_area` VALUES ('51', 'ARMIIHD', null, '2', null, null, '1', '1460708208000', null);
INSERT INTO `sys_area` VALUES ('52', 'ARFM95B', null, '9', null, null, '1', '1460708225000', null);
INSERT INTO `sys_area` VALUES ('53', 'AR8XLD5', null, '9', null, null, '1', '1460708235000', null);
INSERT INTO `sys_area` VALUES ('54', 'AR3I1PL', null, '9', null, null, '1', '1460708247000', null);
INSERT INTO `sys_area` VALUES ('55', 'AR9F4KL', null, '9', null, null, '1', '1460708255000', null);
INSERT INTO `sys_area` VALUES ('56', 'ARYTTIB', null, '9', null, null, '1', '1460708272000', null);
INSERT INTO `sys_area` VALUES ('57', 'AR9P2L6', null, '9', null, null, '1', '1460708281000', null);
INSERT INTO `sys_area` VALUES ('58', 'AR3DTM4', null, '9', null, null, '1', '1460708290000', null);
INSERT INTO `sys_area` VALUES ('59', 'ARTN7XY', null, '9', null, null, '1', '1460708297000', null);
INSERT INTO `sys_area` VALUES ('60', 'AR8MIMG', null, '9', null, null, '1', '1460708313000', null);
INSERT INTO `sys_area` VALUES ('61', 'AR1NH6K', null, '9', null, null, '1', '1460708321000', null);
INSERT INTO `sys_area` VALUES ('62', 'AR6L9AL', null, '10', null, null, '1', '1460708505000', null);
INSERT INTO `sys_area` VALUES ('63', 'ARVJYTK', null, '10', null, null, '1', '1460708513000', null);
INSERT INTO `sys_area` VALUES ('64', 'ARH9WVT', null, '10', null, null, '1', '1460708527000', null);
INSERT INTO `sys_area` VALUES ('65', 'AR5GVLF', null, '10', null, null, '1', '1460708538000', null);
INSERT INTO `sys_area` VALUES ('66', 'ARU8YY1', null, '10', null, null, '1', '1460708548000', null);
INSERT INTO `sys_area` VALUES ('67', 'ARFVZSD', null, '10', null, null, '1', '1460708562000', null);
INSERT INTO `sys_area` VALUES ('68', 'AR3BCMW', null, '10', null, null, '1', '1460708574000', null);
INSERT INTO `sys_area` VALUES ('69', 'AR2LW54', null, '10', null, null, '1', '1460708585000', null);
INSERT INTO `sys_area` VALUES ('70', 'ARSSWDB', null, '10', null, null, '1', '1460708593000', null);
INSERT INTO `sys_area` VALUES ('71', 'AR9LPIG', null, '10', null, null, '1', '1460708602000', null);
INSERT INTO `sys_area` VALUES ('72', 'ARG6SY3', null, '10', null, null, '1', '1460708610000', null);
INSERT INTO `sys_area` VALUES ('73', 'ARYIUAY', null, '10', null, null, '1', '1460708626000', null);
INSERT INTO `sys_area` VALUES ('74', 'ARKEVC3', null, '10', null, null, '1', '1460708671000', null);
INSERT INTO `sys_area` VALUES ('75', 'ARLQGEW', null, '13', null, null, '1', '1460708693000', null);
INSERT INTO `sys_area` VALUES ('76', 'ARYACJY', null, '13', null, null, '1', '1460708704000', null);
INSERT INTO `sys_area` VALUES ('77', 'ARNU96I', null, '13', null, null, '1', '1460708726000', null);
INSERT INTO `sys_area` VALUES ('78', 'ARNJJ2R', null, '13', null, null, '1', '1460708737000', null);
INSERT INTO `sys_area` VALUES ('79', 'AR7XKFA', null, '13', null, null, '1', '1460708749000', null);
INSERT INTO `sys_area` VALUES ('80', 'ARGV8I1', null, '13', null, null, '1', '1460708760000', null);
INSERT INTO `sys_area` VALUES ('81', 'ARUWTMC', null, '13', null, null, '1', '1460708772000', null);
INSERT INTO `sys_area` VALUES ('82', 'AR8LJCS', null, '13', null, null, '1', '1460708788000', null);
INSERT INTO `sys_area` VALUES ('83', 'AR8UJFQ', null, '13', null, null, '1', '1460708798000', null);
INSERT INTO `sys_area` VALUES ('84', 'ARIYGQX', null, '13', null, null, '1', '1460708807000', null);
INSERT INTO `sys_area` VALUES ('85', 'ARVCQZ7', null, '13', null, null, '1', '1460708820000', null);
INSERT INTO `sys_area` VALUES ('86', 'ARHXMKB', null, '13', null, null, '1', '1460708844000', null);
INSERT INTO `sys_area` VALUES ('87', 'ARE5G7B', null, '13', null, null, '1', '1460708854000', null);
INSERT INTO `sys_area` VALUES ('88', 'ARE3DX2', null, '13', null, null, '1', '1460708921000', null);
INSERT INTO `sys_area` VALUES ('89', 'ARHGBTS', null, '13', null, null, '9', '1460708930000', null);
INSERT INTO `sys_area` VALUES ('90', 'ARQP4RX', null, '13', null, null, '1', '1460708990000', null);
INSERT INTO `sys_area` VALUES ('91', 'ARPLMPS', null, '13', null, null, '1', '1460708997000', null);
INSERT INTO `sys_area` VALUES ('92', 'ART1QSC', null, '8', null, null, '9', '1460709353000', null);
INSERT INTO `sys_area` VALUES ('93', 'AR5AYNN', null, '8', null, null, '9', '1460709363000', null);
INSERT INTO `sys_area` VALUES ('94', 'AR9B1EV', null, '8', null, null, '9', '1460709378000', null);
INSERT INTO `sys_area` VALUES ('95', 'ARK9JGE', null, '8', null, null, '9', '1460709388000', null);
INSERT INTO `sys_area` VALUES ('96', 'ARN6ETC', null, '8', null, null, '9', '1460709399000', null);
INSERT INTO `sys_area` VALUES ('97', 'ARAKUFM', null, '8', null, null, '9', '1460709410000', null);
INSERT INTO `sys_area` VALUES ('98', 'ARIP343', null, '8', null, null, '9', '1460709422000', null);
INSERT INTO `sys_area` VALUES ('99', 'ARP2G96', null, '8', null, null, '9', '1460709430000', null);
INSERT INTO `sys_area` VALUES ('100', 'ARP69I4', null, '8', null, null, '1', '1460709446000', '1460713966000');
INSERT INTO `sys_area` VALUES ('101', 'ARC7RFN', null, '8', null, null, '1', '1460709454000', '1460713991000');
INSERT INTO `sys_area` VALUES ('102', 'AR24LLG', null, '14', null, null, '1', '1460709552000', null);
INSERT INTO `sys_area` VALUES ('103', 'ARCLXJ1', null, '14', null, null, '1', '1460709563000', null);
INSERT INTO `sys_area` VALUES ('104', 'AR8LALE', null, '14', null, null, '1', '1460709578000', null);
INSERT INTO `sys_area` VALUES ('105', 'ARTSDPX', null, '14', null, null, '1', '1460709586000', '1460798259000');
INSERT INTO `sys_area` VALUES ('106', 'ARMS1NS', null, '14', null, null, '1', '1460709601000', null);
INSERT INTO `sys_area` VALUES ('107', 'ARUYXAC', null, '14', null, null, '1', '1460709662000', null);
INSERT INTO `sys_area` VALUES ('108', 'AR9T75I', null, '14', null, null, '1', '1460709671000', null);
INSERT INTO `sys_area` VALUES ('109', 'ARS11GI', null, '0', null, null, '9', '1460709676000', null);
INSERT INTO `sys_area` VALUES ('110', 'ARUPI93', null, '0', null, null, '9', '1460709683000', null);
INSERT INTO `sys_area` VALUES ('111', 'ARSEC8J', null, '14', null, null, '1', '1460709766000', null);
INSERT INTO `sys_area` VALUES ('112', 'AR69PGH', null, '14', null, null, '1', '1460709831000', null);
INSERT INTO `sys_area` VALUES ('113', 'AR3FWW4', null, '14', null, null, '1', '1460709845000', null);
INSERT INTO `sys_area` VALUES ('114', 'ARY8AR3', null, '14', null, null, '1', '1460709854000', null);
INSERT INTO `sys_area` VALUES ('115', 'ARREGPV', null, '15', null, null, '1', '1460709876000', null);
INSERT INTO `sys_area` VALUES ('116', 'AR6K2VF', null, '15', null, null, '1', '1460709885000', null);
INSERT INTO `sys_area` VALUES ('117', 'ARBICW2', null, '15', null, null, '1', '1460709893000', null);
INSERT INTO `sys_area` VALUES ('118', 'ARAKH9H', null, '15', null, null, '1', '1460709951000', null);
INSERT INTO `sys_area` VALUES ('119', 'AREQ61H', null, '15', null, null, '1', '1460709962000', null);
INSERT INTO `sys_area` VALUES ('120', 'AR2I42I', null, '15', null, null, '1', '1460709984000', null);
INSERT INTO `sys_area` VALUES ('121', 'AR9MP9B', null, '15', null, null, '1', '1460710013000', null);
INSERT INTO `sys_area` VALUES ('122', 'AR7PMR3', null, '15', null, null, '1', '1460710029000', null);
INSERT INTO `sys_area` VALUES ('123', 'AR2BMES', null, '15', null, null, '1', '1460710040000', null);
INSERT INTO `sys_area` VALUES ('124', 'ARG8HR5', null, '15', null, null, '1', '1460710047000', null);
INSERT INTO `sys_area` VALUES ('125', 'AREY33Z', null, '15', null, null, '1', '1460710054000', null);
INSERT INTO `sys_area` VALUES ('126', 'ARU2XH4', null, '17', null, null, '1', '1460710101000', null);
INSERT INTO `sys_area` VALUES ('127', 'ARARE2P', null, '0', null, null, '9', '1460710116000', '1460710183000');
INSERT INTO `sys_area` VALUES ('128', 'ARYIB66', null, '17', null, null, '1', '1460710209000', null);
INSERT INTO `sys_area` VALUES ('129', 'ARL7RUM', null, '17', null, null, '1', '1460710215000', null);
INSERT INTO `sys_area` VALUES ('130', 'AR5RYYG', null, '17', null, null, '1', '1460710226000', null);
INSERT INTO `sys_area` VALUES ('131', 'ARJFV9V', null, '17', null, null, '1', '1460710242000', null);
INSERT INTO `sys_area` VALUES ('132', 'ARSHVYU', null, '17', null, null, '1', '1460710251000', null);
INSERT INTO `sys_area` VALUES ('133', 'ARSWBU1', null, '17', null, null, '1', '1460710268000', null);
INSERT INTO `sys_area` VALUES ('134', 'ARSSKBK', null, '17', null, null, '1', '1460710275000', null);
INSERT INTO `sys_area` VALUES ('135', 'ARSCJAS', null, '17', null, null, '1', '1460710284000', null);
INSERT INTO `sys_area` VALUES ('136', 'ARQI5TN', null, '18', null, null, '1', '1460710419000', null);
INSERT INTO `sys_area` VALUES ('137', 'ARX71YU', null, '18', null, null, '1', '1460710430000', null);
INSERT INTO `sys_area` VALUES ('138', 'AR69TJT', null, '18', null, null, '1', '1460710469000', null);
INSERT INTO `sys_area` VALUES ('139', 'ARYVAXK', null, '18', null, null, '1', '1460710629000', null);
INSERT INTO `sys_area` VALUES ('140', 'ARV6X83', null, '18', null, null, '1', '1460710635000', null);
INSERT INTO `sys_area` VALUES ('141', 'AR6D9XF', null, '18', null, null, '1', '1460710648000', null);
INSERT INTO `sys_area` VALUES ('142', 'ARZZBFH', null, '18', null, null, '1', '1460710658000', null);
INSERT INTO `sys_area` VALUES ('143', 'ARPI4D1', null, '18', null, null, '1', '1460710674000', null);
INSERT INTO `sys_area` VALUES ('144', 'ARDE9UX', null, '18', null, null, '1', '1460710689000', null);
INSERT INTO `sys_area` VALUES ('145', 'ARL5UIA', null, '18', null, null, '1', '1460710700000', null);
INSERT INTO `sys_area` VALUES ('146', 'ARC22PA', null, '18', null, null, '1', '1460710716000', null);
INSERT INTO `sys_area` VALUES ('147', 'AR3A3A3', null, '19', null, null, '1', '1460710741000', null);
INSERT INTO `sys_area` VALUES ('148', 'ARIG2G3', null, '19', null, null, '1', '1460710747000', null);
INSERT INTO `sys_area` VALUES ('149', 'AR6MNGP', null, '19', null, null, '1', '1460710754000', null);
INSERT INTO `sys_area` VALUES ('150', 'AR3QHGG', null, '20', null, null, '1', '1460710773000', null);
INSERT INTO `sys_area` VALUES ('151', 'ARJID4J', null, '20', null, null, '1', '1460710782000', null);
INSERT INTO `sys_area` VALUES ('152', 'ARJ221C', null, '20', null, null, '1', '1460710788000', null);
INSERT INTO `sys_area` VALUES ('153', 'ARXB9Q8', null, '21', null, null, '1', '1460710799000', null);
INSERT INTO `sys_area` VALUES ('154', 'ARDG4AK', null, '21', null, null, '1', '1460710813000', null);
INSERT INTO `sys_area` VALUES ('155', 'ARY81AN', null, '21', null, null, '1', '1460710823000', null);
INSERT INTO `sys_area` VALUES ('156', 'ARS3IIY', null, '22', null, null, '1', '1460710832000', null);
INSERT INTO `sys_area` VALUES ('157', 'ARJX647', null, '22', null, null, '1', '1460710843000', null);
INSERT INTO `sys_area` VALUES ('158', 'ARZMVDG', null, '22', null, null, '1', '1460710854000', null);
INSERT INTO `sys_area` VALUES ('159', 'ARKSRCC', null, '23', null, null, '1', '1460710903000', null);
INSERT INTO `sys_area` VALUES ('160', 'AR181FQ', null, '23', null, null, '1', '1460710910000', null);
INSERT INTO `sys_area` VALUES ('161', 'ARRZSGA', null, '23', null, null, '1', '1460710919000', null);
INSERT INTO `sys_area` VALUES ('162', 'AR5RJBY', null, '23', null, null, '1', '1460710928000', null);
INSERT INTO `sys_area` VALUES ('163', 'ARES1TH', null, '24', null, null, '1', '1460710936000', null);
INSERT INTO `sys_area` VALUES ('164', 'ARK8S86', null, '24', null, null, '1', '1460710944000', null);
INSERT INTO `sys_area` VALUES ('165', 'AR8B6IC', null, '24', null, null, '1', '1460710975000', null);
INSERT INTO `sys_area` VALUES ('166', 'AR7P9A5', null, '24', null, null, '1', '1460710982000', null);
INSERT INTO `sys_area` VALUES ('167', 'ARNPMQ4', null, '25', null, null, '1', '1460710988000', null);
INSERT INTO `sys_area` VALUES ('168', 'AR41ECN', null, '25', null, null, '1', '1460710998000', null);
INSERT INTO `sys_area` VALUES ('169', 'AR1APK1', null, '26', null, null, '1', '1460711031000', null);
INSERT INTO `sys_area` VALUES ('170', 'ARJBLTM', null, '26', null, null, '1', '1460711040000', null);
INSERT INTO `sys_area` VALUES ('171', 'AR4RML8', null, '26', null, null, '1', '1460711053000', null);
INSERT INTO `sys_area` VALUES ('172', 'ARDDI7A', null, '26', null, null, '1', '1460711065000', null);
INSERT INTO `sys_area` VALUES ('173', 'ARQIZ21', null, '27', null, null, '1', '1460711105000', '1460796633000');
INSERT INTO `sys_area` VALUES ('174', 'ARSCV9R', null, '27', null, null, '1', '1460711120000', null);
INSERT INTO `sys_area` VALUES ('175', 'ARSDZAL', null, '27', null, null, '1', '1460711129000', null);
INSERT INTO `sys_area` VALUES ('176', 'AR61KH3', null, '28', null, null, '1', '1460711158000', null);
INSERT INTO `sys_area` VALUES ('177', 'ARXSIQY', null, '28', null, null, '1', '1460711166000', null);
INSERT INTO `sys_area` VALUES ('178', 'ARDA53T', null, '28', null, null, '1', '1460711173000', null);
INSERT INTO `sys_area` VALUES ('179', 'ARPWJGN', null, '29', null, null, '1', '1460711183000', null);
INSERT INTO `sys_area` VALUES ('180', 'AR7U3YS', null, '29', null, null, '1', '1460711192000', null);
INSERT INTO `sys_area` VALUES ('181', 'ARKPK4X', null, '29', null, null, '1', '1460711231000', null);
INSERT INTO `sys_area` VALUES ('182', 'ARX272P', null, '29', null, null, '1', '1460711238000', null);
INSERT INTO `sys_area` VALUES ('183', 'ARZU3C9', null, '30', null, null, '1', '1460711253000', '1460796364000');
INSERT INTO `sys_area` VALUES ('184', 'ARBFRQZ', null, '30', null, null, '1', '1460711260000', null);
INSERT INTO `sys_area` VALUES ('185', 'ARD5CTT', null, '30', null, null, '1', '1460711278000', null);
INSERT INTO `sys_area` VALUES ('186', 'ARNQR9W', null, '30', null, null, '1', '1460711288000', null);
INSERT INTO `sys_area` VALUES ('187', 'ARLKCGV', null, '30', null, null, '1', '1460711296000', null);
INSERT INTO `sys_area` VALUES ('188', 'ARR532S', null, '31', null, null, '1', '1460711386000', null);
INSERT INTO `sys_area` VALUES ('189', 'ARLBQJU', null, '31', null, null, '1', '1460711395000', null);
INSERT INTO `sys_area` VALUES ('190', 'ARZVR3E', null, '31', null, null, '1', '1460711403000', null);
INSERT INTO `sys_area` VALUES ('191', 'ARWN82H', null, '32', null, null, '1', '1460711413000', null);
INSERT INTO `sys_area` VALUES ('192', 'ARCHWGK', null, '32', null, null, '1', '1460711426000', null);
INSERT INTO `sys_area` VALUES ('193', 'ARHVYWR', null, '32', null, null, '1', '1460711434000', null);
INSERT INTO `sys_area` VALUES ('194', 'ARYZPYR', null, '33', null, null, '1', '1460711444000', null);
INSERT INTO `sys_area` VALUES ('195', 'AR75HXG', null, '33', null, null, '1', '1460711472000', null);
INSERT INTO `sys_area` VALUES ('196', 'ARH7J9M', null, '33', null, null, '1', '1460711479000', null);
INSERT INTO `sys_area` VALUES ('197', 'ARJATCF', null, '34', null, null, '1', '1460711525000', null);
INSERT INTO `sys_area` VALUES ('198', 'AR37EEE', null, '34', null, null, '1', '1460711533000', null);
INSERT INTO `sys_area` VALUES ('199', 'ARVNUWF', null, '34', null, null, '1', '1460711540000', null);
INSERT INTO `sys_area` VALUES ('200', 'ARY7AWL', null, '35', null, null, '1', '1460711547000', null);
INSERT INTO `sys_area` VALUES ('201', 'ARP5UI2', null, '35', null, null, '1', '1460711556000', null);
INSERT INTO `sys_area` VALUES ('202', 'ARE9GDT', null, '36', null, null, '1', '1460711564000', null);
INSERT INTO `sys_area` VALUES ('203', 'AR7FGJT', null, '36', null, null, '1', '1460711572000', null);
INSERT INTO `sys_area` VALUES ('204', 'ARYWD7Q', null, '36', null, null, '1', '1460711600000', null);
INSERT INTO `sys_area` VALUES ('205', 'ARSVCYF', null, '36', null, null, '1', '1460711612000', null);
INSERT INTO `sys_area` VALUES ('206', 'ARE4DGR', null, '37', null, null, '1', '1460711624000', null);
INSERT INTO `sys_area` VALUES ('207', 'AR6HC22', null, '37', null, null, '1', '1460711635000', null);
INSERT INTO `sys_area` VALUES ('208', 'ARIR563', null, '37', null, null, '1', '1460711642000', null);
INSERT INTO `sys_area` VALUES ('209', 'AR2NBYN', null, '38', null, null, '1', '1460711650000', null);
INSERT INTO `sys_area` VALUES ('210', 'AR7WVMW', null, '38', null, null, '1', '1460711657000', null);
INSERT INTO `sys_area` VALUES ('211', 'ARATF96', null, '39', null, null, '1', '1460711788000', null);
INSERT INTO `sys_area` VALUES ('212', 'ARXYW8J', null, '39', null, null, '1', '1460711796000', null);
INSERT INTO `sys_area` VALUES ('213', 'ARYQQ3A', null, '0', null, null, '1', '1460711839000', null);
INSERT INTO `sys_area` VALUES ('214', 'AR27I9Z', null, '0', null, null, '1', '1460711845000', null);
INSERT INTO `sys_area` VALUES ('215', 'AR6G16I', null, '0', null, null, '1', '1460711854000', null);
INSERT INTO `sys_area` VALUES ('216', 'ARVXWEV', null, '0', null, null, '1', '1460711863000', null);
INSERT INTO `sys_area` VALUES ('217', 'ARG4RER', null, '0', null, null, '1', '1460711872000', null);
INSERT INTO `sys_area` VALUES ('218', 'ARVKECY', null, '0', null, null, '1', '1460711878000', null);
INSERT INTO `sys_area` VALUES ('219', 'ARHELAZ', null, '0', null, null, '1', '1460711974000', null);
INSERT INTO `sys_area` VALUES ('220', 'ARNB6XD', null, '0', null, null, '1', '1460712017000', '1460794318000');
INSERT INTO `sys_area` VALUES ('221', 'ARJXN6Y', null, '0', null, null, '1', '1460712023000', '1460805357000');
INSERT INTO `sys_area` VALUES ('222', 'ARLP258', null, '1', null, null, '1', '1460712848000', '1460713783000');
INSERT INTO `sys_area` VALUES ('223', 'ARG5V27', null, '8', null, null, '1', '1460714621000', null);
INSERT INTO `sys_area` VALUES ('224', 'AR61FAK', null, '8', null, null, '1', '1460714644000', null);
INSERT INTO `sys_area` VALUES ('225', 'ARF6IMP', null, '8', null, null, '1', '1460714694000', null);
INSERT INTO `sys_area` VALUES ('226', 'ARQP7BG', null, '8', null, null, '1', '1460714715000', null);
INSERT INTO `sys_area` VALUES ('227', 'ARATD3M', null, '8', null, null, '1', '1460714740000', '1460714918000');
INSERT INTO `sys_area` VALUES ('228', 'AR75XR7', null, '227', null, null, '1', '1460714750000', null);
INSERT INTO `sys_area` VALUES ('229', 'ARUJ7SG', null, '8', null, null, '1', '1460714797000', null);
INSERT INTO `sys_area` VALUES ('230', 'AR4M7MZ', null, '8', null, null, '1', '1460714808000', null);
INSERT INTO `sys_area` VALUES ('231', 'ARW5RU2', null, '8', null, null, '1', '1460714826000', null);
INSERT INTO `sys_area` VALUES ('232', 'ARIQGM8', null, '8', null, null, '1', '1460714835000', null);
INSERT INTO `sys_area` VALUES ('233', 'ARZ4X1Y', null, '8', null, null, '1', '1460714847000', null);
INSERT INTO `sys_area` VALUES ('234', 'ARLHRQR', null, '8', null, null, '1', '1460714859000', null);
INSERT INTO `sys_area` VALUES ('235', 'ARDCREM', null, '8', null, null, '1', '1460714868000', null);
INSERT INTO `sys_area` VALUES ('236', 'ARWPGG2', null, '8', null, null, '1', '1460714878000', null);
INSERT INTO `sys_area` VALUES ('237', 'AR4RP33', null, '8', null, null, '1', '1460714890000', null);
INSERT INTO `sys_area` VALUES ('238', 'ARJL4J6', null, '221', null, null, '9', '1460781564000', null);
INSERT INTO `sys_area` VALUES ('239', 'ARP63H9', null, '40', null, null, '1', '1460788738000', null);
INSERT INTO `sys_area` VALUES ('240', 'ARFD2SE', null, '0', '123', null, '9', '1474609607000', null);
INSERT INTO `sys_area` VALUES ('241', 'ARNDVTM', null, '0', '123', null, '9', '1474610689000', null);
INSERT INTO `sys_area` VALUES ('242', 'ARY2R4Z', null, '221', '2', null, '9', '1474616867000', null);

-- ----------------------------
-- Table structure for sys_area_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_area_info`;
CREATE TABLE `sys_area_info` (
  `language` varchar(20) NOT NULL,
  `code` varchar(32) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `district` varchar(10) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_area_info
-- ----------------------------
INSERT INTO `sys_area_info` VALUES ('cn', 'AR181FQ', '毕节', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR1APK1', '榆林', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR1NH6K', '杭州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR1Z4YR', '浙江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR24LLG', '邯郸', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR27I9Z', '墨西哥', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR2AVS8', '安徽', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR2BMES', '上饶', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR2I42I', '赣州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR2LW54', '蚌埠', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR2NBYN', '三亚', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR34HZ2', '福建', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR37EEE', '海东', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3A3A3', '宜昌', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3BCMW', '滁州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3DTM4', '嘉兴', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3FWW4', '保定', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3I1PL', '台州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3NTM6', '海南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3QHGG', '汕头', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR3YL7V', '贵州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR41ECN', '重庆', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR4M7MZ', '枣庄', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR4MY32', '湖北', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR4RML8', '汉中', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR4RP33', '济南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR53TTT', '山西', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR56QIE', '黑龙江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR5AYNN', '金山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR5GVLF', '马鞍山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR5RJBY', '贵阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR5RYYG', '莆田', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR61FAK', '淄博', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR61KH3', '固原', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR69PGH', '衡水', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR69TJT', '常德', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6D9XF', '怀化', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6G16I', '智利', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6HC22', '台中', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6K2VF', '宜春', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6L9AL', '安庆', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR6MNGP', '武汉', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR75HXG', '陇南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR75XR7', '潍坊', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7BLB9', '山东', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7FGJT', '和田', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7P9A5', '成都', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7PMR3', '抚州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7U3YS', '包头', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7WVMW', '海口', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR7XKFA', '平顶山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8B6IC', '自贡', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8F32B', '泰州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8FQ85', '南京', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8LALE', '秦皇岛', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8LJCS', '濮阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8MIMG', '宁波', null, '');
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8UJFQ', '新乡', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8UYYZ', '江西', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR8XLD5', '丽水', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9B1EV', '青浦', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9F4KL', '温州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9LPIG', '淮北', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9MP9B', '吉安', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9P2L6', '绍兴', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9QWR3', '宿迁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9T75I', '邢台', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AR9WB4R', '天津', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARAKH9H', '新余', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARAKUFM', '松江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARARE2P', '三明', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARATD3M', '东营', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARATF96', '滨海新区', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARBFRQZ', '大连', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARBICW2', '景德镇', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARBZRJV', '甘肃', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARC22PA', '长沙', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARC7RFN', '临沂', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARC9KKZ', '吉林', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARCHWGK', '齐齐哈尔', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARCLXJ1', '德州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARCTPCE', '青海', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARD5CTT', '四平', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARD5RKF', '福建', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARD9X88', '美国', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDA53T', '银川', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDCREM', '烟台', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDDI7A', '西安', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDE9UX', '娄底', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDG4AK', '柳州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARDH5CI', '陕西', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARE3DX2', '周口', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARE4DGR', '台南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARE5G7B', '洛阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARE9GDT', '哈密', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AREQ61H', '鹰潭', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARES1TH', '攀枝花', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'AREY33Z', '南昌', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARF6IMP', '菏泽', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARF96R1', '淮安', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARFD2SE', '123', null, '123');
INSERT INTO `sys_area_info` VALUES ('cn', 'ARFM95B', '衢州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARFURCC', '云南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARFVZSD', '六安', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARG4RER', '巴拉圭', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARG5V27', '济宁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARG6SY3', '宿州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARG8HR5', '九江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARGAZ1R', '河北', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARGB9HC', '重庆', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARGTYH2', '南通', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARGV8I1', '焦作', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARH7J9M', '兰州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARH9WVT', '芜湖', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHELAZ', '玻利维亚', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHGBTS', '安阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHPDGP', '山东', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHVYWR', '哈尔滨', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHXMKB', '开封', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARHYXDJ', '江苏', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARI513F', '广东', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARIG2G3', '襄阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARIP343', '宝山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARIQGM8', '滨州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARIR563', '台北', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARIYGQX', '济源', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJ221C', '广州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJATCF', '玉树', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJBLTM', '延安', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJFV9V', '南平', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJID4J', '深圳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJL4J6', 'dasdas', null, 'dasda');
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJX647', '丽江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARJXN6Y', '巴西', null, '巴西');
INSERT INTO `sys_area_info` VALUES ('cn', 'ARK8S86', '绵阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARK9JGE', '奉贤', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARKEVC3', '亳州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARKPK4X', '赤峰', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARKSRCC', '遵义', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARL5UIA', '湘潭', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARL7RUM', '龙岩', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLBQJU', '白城', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLFC5R', '新疆', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLHRQR', '威海', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLKCGV', '沈阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLMYT4', '洪泽', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLP258', '北京', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLQGEW', '南阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARLQRCH', '宁夏', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARM7INL', '辽宁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARMIIHD', '苏州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARMS1NS', '张家口', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARN6ETC', '嘉定', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNB6XD', '阿根廷', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNDVTM', '123', null, '123');
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNJJ2R', '许昌', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNPMQ4', '万州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNQR9W', '营口', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNQXDD', '扬州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARNU96I', '驻马店', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARP2G96', '闵行', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARP5UI2', '拉萨', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARP63H9', '浦东新区', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARP69I4', '菏泽', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPGBMN', '上海', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPI4D1', '株洲', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPLMPS', '郑州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPNVXU', '盐城', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPUAC3', '湖南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARPWJGN', '鄂尔多斯', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARQI5TN', '湘西', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARQIZ21', '大同', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARQP4RX', '鹤壁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARQP7BG', '聊城', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARQUMWW', '墨西哥', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARR532S', '通化', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARREGPV', '萍乡', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARRHETK', '西藏', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARRZSGA', '六盘水', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARS11GI', '承德', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARS3EUU', '四川', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARS3IIY', '大理', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSCJAS', '福州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSCV9R', '运城', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSDZAL', '太原', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSEC8J', '承德', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSHVYU', '泉州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSSKBK', '厦门', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSSWDB', '淮南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSVCYF', '乌鲁木齐', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSWBU1', '漳州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARSWUT7', '无锡', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ART1QSC', '浦东新区', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARTF7F2', '河南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARTI1FA', '中国', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARTN7XY', '湖州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARTSDPX', '唐山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARU2XH4', '平潭', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARU8YY1', '铜陵', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARUCUY9', '内蒙古', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARUJ7SG', '莱芜', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARUPI93', '保定', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARUWTMC', '三门峡', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARUYXAC', '沧州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARV6X83', '永州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVCQZ7', '漯河', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVJYTK', '黄山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVKECY', '哥伦比亚', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVNUWF', '西宁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVWGKF', '徐州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARVXWEV', '秘鲁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARW5RU2', '德州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARW82ZX', '常州', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARWN82H', '大庆', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARWPGG2', '青岛', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARX272P', '乌海', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARX59UF', '河南', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARX71YU', '张家界', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARXB9Q8', '桂林', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARXSIQY', '石嘴山', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARXYW8J', '天津', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARY2R4Z', '1', null, '3');
INSERT INTO `sys_area_info` VALUES ('cn', 'ARY7AWL', '林芝', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARY81AN', '南宁', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARY8AR3', '石家庄', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARY9BEE', '连云港', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYACJY', '安阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYIB66', '宁德', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYIUAY', '阜阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYK5EY', '广西', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYQQ3A', '美国', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYTTIB', '金华', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYVAXK', '衡阳', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYWD7Q', '吐鲁番', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARYZPYR', '酒泉', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZ4X1Y', '日照', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZ55IL', '台湾', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZ5GLZ', '镇江', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZMVDG', '昆明', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZU3C9', '盘锦', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZVR3E', '长春', null, null);
INSERT INTO `sys_area_info` VALUES ('cn', 'ARZZBFH', '常德', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR181FQ', 'Bijie', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR1APK1', 'Yulin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR1NH6K', 'Hangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR1Z4YR', 'Zhejiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR24LLG', 'Handan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR27I9Z', 'Mexico', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR2AVS8', 'Anhui', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR2BMES', 'Shangrao', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR2I42I', 'Ganzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR2LW54', 'Bengbu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR2NBYN', 'Sanya', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR34HZ2', 'Fujian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR37EEE', 'Haidong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3A3A3', 'Yichang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3BCMW', 'Chuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3DTM4', 'Jiaxing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3FWW4', 'Baoding', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3I1PL', 'Taizhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3NTM6', 'Hainan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3QHGG', 'Shantou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR3YL7V', 'Guizhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR41ECN', 'Chongqing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR4M7MZ', 'Zaozhuang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR4MY32', 'Hubei', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR4RML8', 'Hanzhong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR4RP33', 'Jinan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR53TTT', 'Shanxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR56QIE', 'Heilong Jiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR5GVLF', 'Maan Shan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR5RJBY', 'Guiyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR5RYYG', 'Putian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR61FAK', 'Zibo', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR61KH3', 'Guyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR69PGH', 'Hengshui', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR69TJT', 'Changde', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6D9XF', 'Huaihua', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6G16I', 'Chile', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6HC22', 'Taizhong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6K2VF', 'Yichun', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6L9AL', 'Anqing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR6MNGP', 'Wuhan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR75HXG', 'Longnan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR75XR7', 'Weifang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7FGJT', 'Hetian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7P9A5', 'Chengdu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7PMR3', 'Fuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7U3YS', 'Baotou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7WVMW', 'Haikou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR7XKFA', 'Pingdingshan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8B6IC', 'Zigong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8F32B', 'Qinzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8FQ85', 'Nanjing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8LALE', 'Qinhuang Dao', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8LJCS', 'Puyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8MIMG', 'Ningbo', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8UJFQ', 'Xinxiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8UYYZ', 'Jiangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR8XLD5', 'Lishui', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9F4KL', 'Wenzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9LPIG', 'Huaibei', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9MP9B', 'Jian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9P2L6', 'Shaoxing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9QWR3', 'Suqian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9T75I', 'Xintai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AR9WB4R', 'Tianjin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARAKH9H', 'Xinyu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARATD3M', 'Dongying', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARATF96', 'Binhai New Distric', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARBFRQZ', 'Dalian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARBICW2', 'Jingde Zhen', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARBZRJV', 'Gansu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARC22PA', 'Changsha', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARC7RFN', 'Linyi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARC9KKZ', 'Jilin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARCHWGK', 'Qiqi Ha Er', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARCLXJ1', 'Dezhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARCTPCE', 'Qinghai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARD5CTT', 'Siping', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDA53T', 'Yinchuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDCREM', 'Yantai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDDI7A', 'Xian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDE9UX', 'Loudi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDG4AK', 'Liuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARDH5CI', 'Shanxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARE3DX2', 'Zhoukou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARE4DGR', 'Tainan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARE5G7B', 'Luoyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARE9GDT', 'Hami', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AREQ61H', 'Yingtan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARES1TH', 'Panzhi Hua', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'AREY33Z', 'Nanchang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARF6IMP', 'Heze', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARF96R1', 'Huaian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARFM95B', 'Hengzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARFURCC', 'Yunnan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARFVZSD', 'Liuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARG4RER', 'Paraguay', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARG5V27', 'Jining', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARG6SY3', 'Suzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARG8HR5', 'Jiujiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARGAZ1R', 'Hebei', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARGB9HC', 'Chongqing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARGTYH2', 'Nantong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARGV8I1', 'Jiaozuo', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARH7J9M', 'Lanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARH9WVT', 'Wuhu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARHELAZ', 'Bolivia', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARHPDGP', 'Shandong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARHVYWR', 'Haer Bin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARHXMKB', 'Kaifeng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARHYXDJ', 'Jiangsu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARI513F', 'Guangdong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARIG2G3', 'Xiangyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARIQGM8', 'Binzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARIR563', 'Taibei', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARIYGQX', 'Jiyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJ221C', 'Guangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJATCF', 'Yushu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJBLTM', 'Yanan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJFV9V', 'Nanping', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJID4J', 'Shenzhen', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJX647', 'Lijiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARJXN6Y', 'Brazil', null, 'Brazil');
INSERT INTO `sys_area_info` VALUES ('en', 'ARK8S86', 'Mianyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARKEVC3', 'Haozhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARKPK4X', 'Chifeng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARKSRCC', 'Zunyi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARL5UIA', 'Xiangtan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARL7RUM', 'Longyan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLBQJU', 'Baicheng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLFC5R', 'Xijiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLHRQR', 'Weihai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLKCGV', 'Shenyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLMYT4', 'Hongze', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLP258', 'Beijing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLQGEW', 'Nanyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARLQRCH', 'Ningxia', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARM7INL', 'Liaoning', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARMIIHD', 'Suzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARMS1NS', 'Zhangjia Kou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNB6XD', 'Argentina', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNJJ2R', 'Xuchang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNPMQ4', 'Wanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNQR9W', 'Yingkou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNQXDD', 'Yangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARNU96I', 'Zhuma Dian', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARP5UI2', 'Lasa', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARP63H9', 'Pudong New District', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARP69I4', 'Heze', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPGBMN', 'Shanghai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPI4D1', 'Zhuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPLMPS', 'Zhengzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPNVXU', 'Yancheng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPUAC3', 'Hunan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARPWJGN', 'Eer Duo Si', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARQI5TN', 'Xiangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARQIZ21', 'Datong', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARQP4RX', 'Hebi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARQP7BG', 'Liaocheng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARR532S', 'Tonghua', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARREGPV', 'Pingxiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARRHETK', 'Xizang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARRZSGA', 'Liupan Shui', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARS3EUU', 'Sichuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARS3IIY', 'Dali', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSCJAS', 'Fuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSCV9R', 'Yuncheng', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSDZAL', 'Taiyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSEC8J', 'Chengde', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSHVYU', 'Quanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSSKBK', 'Xiamen', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSSWDB', 'Huainan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSVCYF', 'Wulu Mu Qi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSWBU1', 'Zhangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARSWUT7', 'Wuxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARTF7F2', 'Henan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARTI1FA', 'China', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARTN7XY', 'Huzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARTSDPX', 'Tangshan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARU2XH4', 'Pingtan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARU8YY1', 'Tongling', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARUCUY9', 'Neimeng Gu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARUJ7SG', 'Laiwu', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARUWTMC', 'Sanmen Xia', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARUYXAC', 'Cangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARV6X83', 'Yongzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVCQZ7', 'Leihe', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVJYTK', 'Huangshan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVKECY', 'Columbia', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVNUWF', 'Xining', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVWGKF', 'Xuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARVXWEV', 'Peru', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARW5RU2', 'Dezhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARW82ZX', 'Changzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARWN82H', 'Daqing', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARWPGG2', 'Qingdao', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARX272P', 'Wuhai', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARX71YU', 'Zhangjia Jie', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARXB9Q8', 'Guilin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARXSIQY', 'Shizui Shan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARXYW8J', 'Tianjin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARY7AWL', 'Linzhi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARY81AN', 'Nanning', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARY8AR3', 'Shijia Zhuang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARY9BEE', 'Lianyun Gang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYACJY', 'Anyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYIB66', 'Ningde', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYIUAY', 'Fuyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYK5EY', 'Guangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYQQ3A', 'American', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYTTIB', 'Jinhua', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYVAXK', 'Hengyang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYWD7Q', 'Tulu Fan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARYZPYR', 'Jiuquan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZ4X1Y', 'Rizhao', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZ55IL', 'Taiwan', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZ5GLZ', 'Zhenjiang', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZMVDG', 'Kunming', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZU3C9', 'Panjin', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZVR3E', 'Changchun', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'ARZZBFH', 'Changde', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'SINBDJG', 'kkkk', null, null);
INSERT INTO `sys_area_info` VALUES ('en', 'SINKHYK', '2', null, '2');
INSERT INTO `sys_area_info` VALUES ('es', 'AR181FQ', 'Bijie', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR1APK1', 'Yulin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR1NH6K', 'Hangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR1Z4YR', 'Zhejiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR24LLG', 'Handan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR27I9Z', 'México', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR2AVS8', 'Anhui', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR2BMES', 'Shangrao', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR2I42I', 'Ganzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR2LW54', 'Bengbu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR2NBYN', 'Sanya', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR34HZ2', 'Fujian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR37EEE', 'Haidong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3A3A3', 'Yichang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3BCMW', 'Chuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3DTM4', 'Jiaxing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3FWW4', 'Baoding', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3I1PL', 'Taizhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3NTM6', 'Hainan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3QHGG', 'Shantou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR3YL7V', 'Guizhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR41ECN', 'Chongqing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR4M7MZ', 'Zaozhuang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR4MY32', 'Hubei', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR4RML8', 'Hanzhong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR4RP33', 'Jinan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR53TTT', 'Shanxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR56QIE', 'Heilong Jiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR5GVLF', 'Maan Shan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR5RJBY', 'Guiyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR5RYYG', 'Putian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR61FAK', 'Zibo', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR61KH3', 'Guyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR69PGH', 'Hengshui', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR69TJT', 'Changde', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6D9XF', 'Huaihua', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6G16I', 'Chile', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6HC22', 'Taizhong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6K2VF', 'Yichun', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6L9AL', 'Anqing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR6MNGP', 'Wuhan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR75HXG', 'Longnan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR75XR7', 'Weifang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7FGJT', 'Hetian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7P9A5', 'Chengdu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7PMR3', 'Fuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7U3YS', 'Baotou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7WVMW', 'Haikou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR7XKFA', 'Pingding Shan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8B6IC', 'Zigong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8F32B', 'Qinzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8FQ85', 'Nanjing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8LALE', 'Qinhuang Dao', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8LJCS', 'Puyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8MIMG', 'Ningbo', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8UJFQ', 'Xinxiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8UYYZ', 'Jiangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR8XLD5', 'Lishui', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9F4KL', 'Wenzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9LPIG', 'Huaibei', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9MP9B', 'Jian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9P2L6', 'Shaoxing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9QWR3', 'Suqian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9T75I', 'Xintai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AR9WB4R', 'Tianjin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARAKH9H', 'Xinyu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARATD3M', 'Dongying', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARATF96', 'Binhai New District', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARBFRQZ', 'Dalian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARBICW2', 'Jingde Zhen', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARBZRJV', 'Gansu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARC22PA', 'Changsha', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARC7RFN', 'Linyi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARC9KKZ', 'Jilin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARCHWGK', 'Qiqi Ha Er', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARCLXJ1', 'Dezhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARCTPCE', 'Qinghai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARD5CTT', 'Siping', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDA53T', 'Yinchuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDCREM', 'Yantai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDDI7A', 'Xian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDE9UX', 'Loudi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDG4AK', 'Liuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARDH5CI', 'Shanxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARE3DX2', 'Zhoukou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARE4DGR', 'Tainan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARE5G7B', 'Luoyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARE9GDT', 'Hami', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AREQ61H', 'Yingtan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARES1TH', 'Panzhi Hua', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'AREY33Z', 'Nanchang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARF6IMP', 'Heze', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARF96R1', 'Huanian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARFM95B', 'Quzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARFURCC', 'Yunnan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARFVZSD', 'Liuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARG4RER', 'Paraguay', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARG5V27', 'Jining', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARG6SY3', 'Suzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARG8HR5', 'Jiujiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARGAZ1R', 'Hebei', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARGB9HC', 'Chongqing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARGTYH2', 'Nantong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARGV8I1', 'Jiaozuo', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARH7J9M', 'Lanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARH9WVT', 'Wuhu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARHELAZ', 'Bolivia', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARHPDGP', 'Shandong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARHVYWR', 'Haer Bin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARHXMKB', 'Kaifeng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARHYXDJ', 'Jiangsu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARI513F', 'Guangdong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARIG2G3', 'Xiangyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARIQGM8', 'Binzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARIR563', 'Taibei', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARIYGQX', 'Jiyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJ221C', 'Guangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJATCF', 'Yushu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJBLTM', 'Yanan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJFV9V', 'Nanping', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJID4J', 'Shenzhen', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJX647', 'Lijiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARJXN6Y', 'Brasil', null, 'Brasil');
INSERT INTO `sys_area_info` VALUES ('es', 'ARK8S86', 'Mianyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARKEVC3', 'Haozhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARKPK4X', 'Chifeng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARKSRCC', 'Zunyi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARL5UIA', 'Xiangtan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARL7RUM', 'Longyan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLBQJU', 'Baicheng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLFC5R', 'Xinjiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLHRQR', 'Weihai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLKCGV', 'Shenyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLMYT4', 'Hongze', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLP258', 'Beijing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLQGEW', 'Nanyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARLQRCH', 'Ningxia', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARM7INL', 'Liaoning', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARMIIHD', 'Suzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARMS1NS', 'Zhangjia Kou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNB6XD', 'Argentina', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNJJ2R', 'Xuchang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNPMQ4', 'Wanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNQR9W', 'Yingkou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNQXDD', 'Yangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARNU96I', 'Zhuma Dian', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARP5UI2', 'Lasa', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARP63H9', 'Pudong New District', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARP69I4', 'Heze', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPGBMN', 'Shanghai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPI4D1', 'Zhuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPLMPS', 'Zhengzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPNVXU', 'Yancheng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPUAC3', 'Hunan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARPWJGN', 'Eer Duo Si', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARQI5TN', 'Xiangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARQIZ21', 'Datong', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARQP4RX', 'Hebi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARQP7BG', 'Liaocheng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARR532S', 'Tonghua', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARREGPV', 'Pingxiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARRHETK', 'Xizang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARRZSGA', 'Liupan Shui', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARS3EUU', 'Sichuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARS3IIY', 'Dali', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSCJAS', 'Fuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSCV9R', 'Yuncheng', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSDZAL', 'Taiyuan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSEC8J', 'Chengde', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSHVYU', 'Quanzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSSKBK', 'Xiamen', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSSWDB', 'Huainan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSVCYF', 'Wulu Mu Qi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSWBU1', 'Zhangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARSWUT7', 'Wuxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARTF7F2', 'Henan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARTI1FA', 'China', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARTN7XY', 'Huzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARTSDPX', 'Tangshan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARU2XH4', 'Pingtan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARU8YY1', 'Tongling', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARUCUY9', 'Neimeng Gu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARUJ7SG', 'Laiwu', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARUWTMC', 'Sanmen Xia', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARUYXAC', 'Cangzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARV6X83', 'Yongzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVCQZ7', 'Leihe', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVJYTK', 'Huangshan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVKECY', 'Colombia', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVNUWF', 'Xining', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVWGKF', 'Xuzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARVXWEV', 'Perú', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARW5RU2', 'Dezhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARW82ZX', 'Changzhou', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARWN82H', 'Daqing', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARWPGG2', 'Qingdao', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARX272P', 'Wuhai', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARX71YU', 'Zhangjia Jie', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARXB9Q8', 'Guilin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARXSIQY', 'Shizui Shan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARXYW8J', 'Tianjin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARY7AWL', 'Linzhi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARY81AN', 'Nanning', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARY8AR3', 'Shijia Zhuang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARY9BEE', 'Lianyun Gang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYACJY', 'Anyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYIB66', 'Ningde', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYIUAY', 'Fuyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYK5EY', 'Guangxi', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYQQ3A', 'América', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYTTIB', 'Jinhua', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYVAXK', 'Hengyang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYWD7Q', 'Tulu Fan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARYZPYR', 'Jiuquan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZ4X1Y', 'Rizhao', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZ55IL', 'Taiwan', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZ5GLZ', 'Zhenjiang', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZMVDG', 'Kunming', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZU3C9', 'Panjin', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZVR3E', 'Changchun', null, null);
INSERT INTO `sys_area_info` VALUES ('es', 'ARZZBFH', 'Changde', null, null);

-- ----------------------------
-- Table structure for sys_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_code`;
CREATE TABLE `sys_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL COMMENT '用来区分不同code',
  `current_value` varchar(255) DEFAULT '1' COMMENT '最后可用值',
  `oem_code` varchar(255) NOT NULL COMMENT '所属oem_code',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_code
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_code` varchar(20) DEFAULT NULL,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '1', '1', '管理门户', '0', '', '', null);
INSERT INTO `sys_dict` VALUES ('2', '1', '2', 'OEM门户', '0', '', '', null);
INSERT INTO `sys_dict` VALUES ('3', '1', '3', '代理商门户', '0', null, '', null);
INSERT INTO `sys_dict` VALUES ('4', '1', '4', '用户门户', '0', '', '', null);
INSERT INTO `sys_dict` VALUES ('5', '1', '5', '集成商门户', '0', null, '', null);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `parent_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------

-- ----------------------------
-- Table structure for sys_index_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_index_type`;
CREATE TABLE `sys_index_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `type` int(11) DEFAULT NULL,
  `unit` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '单位',
  `data_type` int(11) NOT NULL COMMENT '数据类型：1-整数，2-小数，3-日期，4-文本',
  `digits` int(11) DEFAULT NULL COMMENT '小数位数',
  `monitor_type` int(11) DEFAULT NULL COMMENT '''图表类型：1-面板，2-仪表盘，3-实时曲线，4-仪表盘+实时曲线'',',
  `chart_type` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '''报表编辑工具时支持的图表类型，可多选：bar,line,pie'',',
  `value_type` int(11) NOT NULL COMMENT '值统计类型：1-累加值，2-瞬时值',
  `is_user_customer_report` int(11) NOT NULL COMMENT '是否运用于自定义报表：0-否，1-是',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_index_type
-- ----------------------------
INSERT INTO `sys_index_type` VALUES ('6', 'SIT774H', '1', 'V', '1', '5', '1', 'pie|', '2', '1', '9', '1474696674000', '1479451951000');
INSERT INTO `sys_index_type` VALUES ('7', 'SITA378', '2', '℃', '2', '2', '2', 'pie|', '1', '1', '9', '1476784923000', '1479451957000');
INSERT INTO `sys_index_type` VALUES ('8', 'SITBF32', '2', '11', '1', null, '2', 'bar|line|pie|', '2', '2', '9', '1477563542000', '1479451964000');
INSERT INTO `sys_index_type` VALUES ('9', 'SITF2WP', '1', '℃', '1', null, '1', 'bar|line|pie|', '1', '1', '9', '1477644516000', '1479452010000');
INSERT INTO `sys_index_type` VALUES ('10', 'SITIQG9', '1', '21', '1', null, '1', 'bar|line|pie|', '1', '1', '9', '1477644698000', '1479451981000');
INSERT INTO `sys_index_type` VALUES ('11', 'SITKVAY', '1', null, '1', null, '1', 'bar|', '1', '1', '9', '1479449771000', '1479451984000');
INSERT INTO `sys_index_type` VALUES ('12', 'SITL7AU', '1', null, '1', null, '1', 'bar|', '1', '1', '1', '1479452494000', null);
INSERT INTO `sys_index_type` VALUES ('13', 'SITRCT7', '2', 'V', '1', null, '2', 'bar|', '1', '1', '1', '1479452516000', null);
INSERT INTO `sys_index_type` VALUES ('14', 'SITBE8G', '1', null, '1', null, '1', 'bar|', '2', '2', '1', '1479811177000', null);
INSERT INTO `sys_index_type` VALUES ('15', 'SITZLDU', '1', null, '1', null, '1', 'bar|', '2', '2', '1', '1479811202000', '1479811399000');

-- ----------------------------
-- Table structure for sys_index_type_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_index_type_info`;
CREATE TABLE `sys_index_type_info` (
  `language` varchar(20) CHARACTER SET utf8 NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `comment` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_index_type_info
-- ----------------------------
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SIT774H', 'ceshi', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITA378', '温度', '121大胆');
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITBE8G', '开机信号', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITBF32', 'CESHI', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITF2WP', '开关', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITIQG9', '11212', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITKVAY', 'test', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITL7AU', '开关量', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITRCT7', '模拟量', null);
INSERT INTO `sys_index_type_info` VALUES ('cn', 'SITZLDU', '运行信号', null);
INSERT INTO `sys_index_type_info` VALUES ('en', 'SITA378', '121212', null);

-- ----------------------------
-- Table structure for sys_industry
-- ----------------------------
DROP TABLE IF EXISTS `sys_industry`;
CREATE TABLE `sys_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `parent_id` int(20) NOT NULL,
  `grade` int(11) NOT NULL,
  `path` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '路径',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_industry
-- ----------------------------

-- ----------------------------
-- Table structure for sys_industry_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_industry_info`;
CREATE TABLE `sys_industry_info` (
  `language` varchar(20) CHARACTER SET utf8 NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '编码',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `comment` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`language`,`code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_industry_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_language
-- ----------------------------
DROP TABLE IF EXISTS `sys_language`;
CREATE TABLE `sys_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_language
-- ----------------------------
INSERT INTO `sys_language` VALUES ('1', 'cn', '简体中文', '1', '简体中文', '1460614875000', null);
INSERT INTO `sys_language` VALUES ('2', 'en', 'English', '1', '英语', '1460614875000', null);
INSERT INTO `sys_language` VALUES ('4', 'es', 'La lengua española', '1', '西班牙语', '1460614875000', null);

-- ----------------------------
-- Table structure for sys_parameter
-- ----------------------------
DROP TABLE IF EXISTS `sys_parameter`;
CREATE TABLE `sys_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `pkey` varchar(20) NOT NULL,
  `pvalue` varchar(50) NOT NULL,
  `status` int(11) NOT NULL COMMENT '''0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(32) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(32) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for sys_simcard
-- ----------------------------
DROP TABLE IF EXISTS `sys_simcard`;
CREATE TABLE `sys_simcard` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'ID主键非空',
  `industry_user_no` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '行业用户号',
  `real_no` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '真实号（运营商唯一编号）',
  `product_type` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '套餐类型',
  `activation_date` datetime NOT NULL COMMENT '激活日期',
  `deadline_date` datetime NOT NULL COMMENT '到期日期',
  `simcard_user` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SIM卡用户（采购SIM卡的设备制造商或设备用户公司资料。中科云创负责录入此信息，云中控忽略此栏位）',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `create_time` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_simcard
-- ----------------------------

-- ----------------------------
-- Table structure for task_schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `task_schedule_job`;
CREATE TABLE `task_schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_code` varchar(20) DEFAULT NULL COMMENT '关联触发器或告警规则编码',
  `job_name` varchar(255) DEFAULT NULL,
  `job_group` varchar(255) DEFAULT NULL,
  `job_status` varchar(255) DEFAULT NULL,
  `cron_expression` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `bean_class` varchar(255) DEFAULT NULL,
  `is_concurrent` varchar(255) DEFAULT NULL COMMENT '1',
  `spring_id` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) NOT NULL,
  `env` varchar(50) DEFAULT NULL COMMENT '运行环境',
  `is_run` int(11) DEFAULT NULL COMMENT '是否正在运行',
  `effect_time` varchar(20) DEFAULT NULL COMMENT '生效时间',
  `expire_time` varchar(20) DEFAULT NULL COMMENT '时效时间',
  `status` int(11) DEFAULT NULL COMMENT '0-禁用，1-正常 2-未启用 9-删除 ',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `group_cron` (`job_group`,`cron_expression`,`env`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task_schedule_job
-- ----------------------------
INSERT INTO `task_schedule_job` VALUES ('1', null, '历史任务处理服务-dev', '历史任务组', '1', '0 0 0/1 * * ?', '一小时执行一次', 'com.cloudinnov.task.HistoryQuartzJob', '0', null, 'execute', 'dev', '1', null, null, null, '2016-12-02 08:11:55', '2016-12-02 08:11:55');
INSERT INTO `task_schedule_job` VALUES ('3', null, '历史数据处理任务-demon', '历史任务组', '1', '0 0 0/1 * * ?', '一小时执行一次', 'com.cloudinnov.task.HistoryQuartzJob', '0', null, 'execute', 'demon', '0', null, null, null, '2016-12-02 03:58:38', '2016-12-02 03:58:38');
INSERT INTO `task_schedule_job` VALUES ('5', null, '告警规则处理服务-dev', '告警规则任务组', '0', '0/5 * * * * ?', '告警规则添加', 'com.cloudinnov.task.AlarmRuleQuartzJob', '1', null, 'execute', 'dev', '0', null, null, null, '2016-12-02 03:58:31', '2016-12-02 03:58:31');
INSERT INTO `task_schedule_job` VALUES ('19', 'TGEASZB', '触发器处理服务1480608025899', '触发器组', '0', '0 0/1 * * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', '0', '1480665933000', '1512201933000', null, '2016-12-12 05:57:58', '2016-12-12 05:57:58');
INSERT INTO `task_schedule_job` VALUES ('21', 'TG794H8', '触发器处理服务1480907688291', '触发器组', '1', '0 20 11 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '1480907700000', '1480957500000', null, '2016-12-05 03:16:46', '2016-12-05 03:16:46');
INSERT INTO `task_schedule_job` VALUES ('22', 'TG1P4CZ', '触发器处理服务1482286832897', '触发器组', '1', '0 15 3 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 10:20:33', null);
INSERT INTO `task_schedule_job` VALUES ('23', 'TG1P4CZ', '触发器处理服务1482286833100', '触发器组', '1', '0 30 14 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 10:20:33', null);
INSERT INTO `task_schedule_job` VALUES ('30', 'TGQY95W', '触发器处理服务1482289245542', '触发器组', '1', '0 25 9 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 11:00:46', null);
INSERT INTO `task_schedule_job` VALUES ('37', 'TGPX4NH', '触发器处理服务1482289931771', '触发器组', '1', '0 5 5 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 11:12:12', null);
INSERT INTO `task_schedule_job` VALUES ('39', 'TGBXH7G', '触发器处理服务1482289976957', '触发器组', '1', '0 10 2 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 11:12:57', null);
INSERT INTO `task_schedule_job` VALUES ('43', 'TGFV568', '触发器处理服务1482313605870', '触发器组', '1', '0 5 1 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-21 17:46:46', null);
INSERT INTO `task_schedule_job` VALUES ('47', 'TG2VZ1W', '触发器处理服务1482385595132', '触发器组', '1', '0 15 5 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-22 13:46:35', null);
INSERT INTO `task_schedule_job` VALUES ('58', 'TGDJY63', '触发器处理服务1482461190732', '触发器组', '1', '0 0 0 ? * 1', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 10:46:31', null);
INSERT INTO `task_schedule_job` VALUES ('61', 'TGSK3V4', '触发器处理服务1482464114887', '触发器组', '1', '0 0 0 14, * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 11:35:15', null);
INSERT INTO `task_schedule_job` VALUES ('73', 'TGFCAUK', '触发器处理服务1482468416828', '触发器组', '1', '0 55 15 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 12:46:57', null);
INSERT INTO `task_schedule_job` VALUES ('74', 'TGFCAUK', '触发器处理服务1482468416883', '触发器组', '1', '0 0 23 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 12:46:57', null);
INSERT INTO `task_schedule_job` VALUES ('117', 'TGY3ZKB', '触发器处理服务1482491109577', '触发器组', '1', '0 0 0 * * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 19:06:06', null);
INSERT INTO `task_schedule_job` VALUES ('118', 'TG28XNH', '触发器处理服务1482491310095', '触发器组', '1', '0 0 0 1,2, * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 19:08:30', null);
INSERT INTO `task_schedule_job` VALUES ('119', 'TG28XNH', '触发器处理服务1482491310135', '触发器组', '1', '0 5 1 1,2, * ?', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 19:08:30', null);
INSERT INTO `task_schedule_job` VALUES ('120', 'TG52E2F', '触发器处理服务1482491660594', '触发器组', '1', '0 0 0 ? * 1,2', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 19:14:21', null);
INSERT INTO `task_schedule_job` VALUES ('121', 'TG52E2F', '触发器处理服务1482491660631', '触发器组', '1', '0 5 1 ? * 1,2', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-23 19:14:21', null);
INSERT INTO `task_schedule_job` VALUES ('125', 'TGXBF42', '触发器处理服务1482675367288', '触发器组', '1', '0 0 0 5 11 ? *', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-25 22:16:07', null);
INSERT INTO `task_schedule_job` VALUES ('127', 'TGAHJR8', '触发器处理服务1482676061004', '触发器组', '1', '0 5 1 28 11 ? *', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-25 22:27:41', null);
INSERT INTO `task_schedule_job` VALUES ('128', 'TGAHJR8', '触发器处理服务1482676061063', '触发器组', '1', '0 0 0 28 11 ? *', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-25 22:27:41', null);
INSERT INTO `task_schedule_job` VALUES ('129', 'TGAHJR8', '触发器处理服务1482676061095', '触发器组', '1', '0 5 1 3 11 ? *', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-25 22:27:41', null);
INSERT INTO `task_schedule_job` VALUES ('130', 'TGAHJR8', '触发器处理服务1482676061123', '触发器组', '1', '0 0 0 3 11 ? *', 'dev', 'com.cloudinnov.task.TriggerQuartzJob', null, null, 'sendText', 'dev', null, '', '', '1', '2016-12-25 22:27:41', null);

-- ----------------------------
-- Table structure for tigger
-- ----------------------------
DROP TABLE IF EXISTS `tigger`;
CREATE TABLE `tigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `tigger_rule` int(11) NOT NULL COMMENT '触发规则：1-按时间，2-按条件\r\n            按时间的规则需要配置重复周期，开始时间，结束时间和时间规则\r\n            按条件的规则需要配置条件规则',
  `repeat_period` int(11) DEFAULT NULL COMMENT '重复周期：1-天，2-周，3-月，4-年',
  `year_day` varchar(200) DEFAULT NULL COMMENT '每年某天，重复周期为年时设置，可设置多个，格式为 10-01|05-01',
  `month_day` varchar(50) DEFAULT NULL COMMENT '每月第几天，重复周期为月时设置，可设置多个，格式为 1|2',
  `week` varchar(50) DEFAULT NULL COMMENT '星期，重复周期为周时设置，可设置多个，格式为 1|2',
  `time_rule` varchar(200) DEFAULT NULL COMMENT '时间，所有重复周期都可以需要设置，可设置多个，格式为 12:00:00|18:00:00',
  `condition` varchar(500) DEFAULT NULL COMMENT '触发规则为按条件时配置，选择监控点位，可选择多个，格式为 point1,点1,>,50|point2,点2,=,1',
  `effect_time` varchar(20) DEFAULT NULL COMMENT '规则生效时间',
  `expire_time` varchar(20) DEFAULT NULL COMMENT '规则失效时间',
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常 2-未启用 9-删除 ',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  `on_off` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COMMENT='触发器表';

-- ----------------------------
-- Records of tigger
-- ----------------------------
INSERT INTO `tigger` VALUES ('1', 'TGRKT1H', '1212', '1', '1', '', '', '', '1480438800000|', null, '1330552800000', '1454256000000', '9', null, '1480578296000', null, '1');
INSERT INTO `tigger` VALUES ('12', 'TGPUGMS', '测试后台任务', '1', '1', '', '', '', '1480607400000|1480607700000|', null, '1480608000000', '1480694400000', '9', null, '1480607655000', null, '0');
INSERT INTO `tigger` VALUES ('13', 'TGEASZB', '后台任务测试', '1', '1', '', '', '', '1480608000000|', null, '1480694400000', '1480780800000', '9', null, '1480608031000', null, '0');
INSERT INTO `tigger` VALUES ('15', 'TGBKZ1U', '1111', '1', '2', '', '', '星期二|', '1480875000000|', null, '1399339500000', '1433294700000', '9', null, '1480906833000', null, '0');
INSERT INTO `tigger` VALUES ('16', 'TG794H8', '2016年12月5日触发器', '1', '1', '', '', '', '1480908000000|', null, '1480907700000', '1480957500000', '9', null, '1480907700000', null, '0');
INSERT INTO `tigger` VALUES ('17', 'TGKSD5W', 'qwe', '3', null, '', '', '', '', '10001|', 'NaN', 'NaN', '9', null, '1480926358000', null, '0');
INSERT INTO `tigger` VALUES ('18', 'TG19SHB', '1', '3', null, '', '', '', '', '10001|10006|10005|', '1433116800000', '1433116800000', '9', '撒旦飞洒发生的', '1481277788000', '1481278424000', '0');
INSERT INTO `tigger` VALUES ('19', 'TGTLINH', '测试', '3', null, '', '', '', '', '10007|', ' ', ' ', '9', null, '1481859304000', '1482128971000', '0');
INSERT INTO `tigger` VALUES ('20', 'TGRJCWZ', '11', '3', null, '', '', '', '', '10001|', ' ', ' ', '9', '11', '1481874365000', '1482129280000', '0');
INSERT INTO `tigger` VALUES ('21', 'TGFMJ6J', '1', '3', null, '', '', '', '', '10001|', ' ', ' ', '9', 'qqwe', '1481874803000', null, '0');
INSERT INTO `tigger` VALUES ('22', 'TG62NA6', '1111', '3', null, '', '', '', '', '10007|22|', ' ', ' ', '9', null, '1481875647000', '1482129303000', '0');
INSERT INTO `tigger` VALUES ('23', 'TGVJ4CS', '1', '3', null, '', '', '', '', '10007|', '1481779500000', '1481799000000', '9', null, '1482128177000', '1482128937000', '0');
INSERT INTO `tigger` VALUES ('24', 'TGFNUFT', '1', '3', null, '', '', '', '', '11|', '', '1482580500000', '9', null, '1482129391000', '1482130426000', '0');
INSERT INTO `tigger` VALUES ('25', 'TG5Q9AD', '1', '3', null, '', '', '', '', '11|', ' ', ' ', '9', null, '1482134162000', null, '0');
INSERT INTO `tigger` VALUES ('26', 'TG1ND29', '1111', '3', null, '', '', '', '', '11|22|', ' ', ' ', '9', null, '1482141168000', null, '0');
INSERT INTO `tigger` VALUES ('27', 'TGBJ83C', '111', '3', null, '', '', '', '', '11|22|10001|', ' ', ' ', '9', null, '1482141365000', null, '0');
INSERT INTO `tigger` VALUES ('28', 'TGUUFPR', '最后一发', '3', null, '', '', '', '', '22|11|10007|', ' ', ' ', '9', null, '1482144391000', null, '0');
INSERT INTO `tigger` VALUES ('29', 'TG3RDTG', '最后一发', '3', null, '', '', '', '', '22|10007|', '', '', '9', null, '1482144608000', '1482144683000', '0');
INSERT INTO `tigger` VALUES ('30', 'TGPT5YX', '1', '3', null, '', '', '', '', '11|22|', '', '', '9', null, '1482201355000', '1482202568000', '0');
INSERT INTO `tigger` VALUES ('31', 'TG4LVXT', '111', '3', null, '', '', '', '', '[{\"code\":\"FANXL2B\",\"faultCode\":\"22\"},{\"code\":\"FARZ9XL\",\"faultCode\":\"10007\"},{\"code\":\"FAPLQJQ\",\"faultCode\":\"11\"}]', '', '', '9', null, '1482202705000', '1482202811000', '0');
INSERT INTO `tigger` VALUES ('32', 'TG1P4CZ', '111', '1', '1', '', '', '', '1482261300000|1482301800000|', '[]', '', '', '9', null, '1482286839000', null, '0');
INSERT INTO `tigger` VALUES ('38', 'TGQY95W', '啊实打实的', '1', '1', '', '', '', '1482283500000|', '[]', '', '', '9', null, '1482289252000', null, '0');
INSERT INTO `tigger` VALUES ('43', 'TGPX4NH', '1', '1', '1', '', '', '', '1482267900000|', '[]', '', '', '9', null, '1482289938000', '1482381529000', '0');
INSERT INTO `tigger` VALUES ('45', 'TGBXH7G', '1', '1', '1', '', '', '', '1482257400000|', '[]', '', '', '9', null, '1482289983000', '1482290000000', '0');
INSERT INTO `tigger` VALUES ('47', 'TGAJ7NC', '111', '3', null, '', '', '', '', '[{\"code\":\"FANXL2B\",\"faultCode\":\"22\"},{\"code\":\"FARZ9XL\",\"faultCode\":\"10007\"}]', '', '', '9', null, '1482290114000', null, '0');
INSERT INTO `tigger` VALUES ('50', 'TGFV568', '1', '1', '1', '', '', '', '1482253500000|', '[]', '', '', '9', null, '1482313610000', null, '0');
INSERT INTO `tigger` VALUES ('53', 'TGMY8J1', '故障码', '3', null, '', '', '', '', '[{\"code\":\"FANXL2B\",\"faultCode\":\"22\"},{\"code\":\"FAPLQJQ\",\"faultCode\":\"11\"},{\"code\":\"FARZ9XL\",\"faultCode\":\"10007\"}]', '1481148600000', '1481235000000', '1', 'sdfasdfasdfasdfasdfasdf', '1482381142000', '1482459609000', '1');
INSERT INTO `tigger` VALUES ('55', 'TG2VZ1W', '1111', '1', '1', '', '', '', '1482354900000|1482430200000|1482448500000|', '[]', '', '', '1', null, '1482385605000', '1482461166000', '0');
INSERT INTO `tigger` VALUES ('68', 'TGDJY63', 'qwe', '1', '3', '1481126400000|', '3|5|', '星期一|星期四|', '1482422400000|1482444900000|', '[]', '', '', '1', null, '1482461197000', '1482480584000', '1');
INSERT INTO `tigger` VALUES ('71', 'TGSK3V4', 'qwe', '1', '3', '1480953600000|1480953600000|1480953600000|1480953600000|', '6|3|15|', '', '1482422400000|1482440700000|1482422400000|1482440700000|1482422400000|1482440700000|1482422400000|1482440700000|', '[]', '', '', '1', null, '1482464121000', '1482480295000', '1');
INSERT INTO `tigger` VALUES ('81', 'TGFCAUK', '阿萨德', '1', '1', '', '', '', '1481615700000|1481641200000|', '[]', '', '', '1', null, '1482468423000', null, '1');
INSERT INTO `tigger` VALUES ('117', 'TGY3ZKB', 'qwe', '1', '1', '', '', '', '1482422400000|1482426300000|', '[]', '', '', '1', null, '1482491116000', null, '1');
INSERT INTO `tigger` VALUES ('118', 'TGAC4FH', 'days', '1', '1', '', '', '', '1482422400000|1482426300000|', '[]', '', '', '1', null, '1482491256000', null, '1');
INSERT INTO `tigger` VALUES ('119', 'TG28XNH', 'months', '1', '3', '', '1|2|3|', '', '1482422400000|1482426300000|', '[]', '', '', '1', null, '1482491316000', '1482491552000', '1');
INSERT INTO `tigger` VALUES ('120', 'TG52E2F', 'weeks', '1', '2', '', '', '星期一|星期三|', '1482516600000|', '[]', '', '', '1', null, '1482491667000', '1482547762000', '1');
INSERT INTO `tigger` VALUES ('124', 'TGXBF42', 'qwe', '1', '4', '1480867200000|', '', '', '-2209104000000|', '[]', '', '', '1', null, '1482675369000', null, '0');
INSERT INTO `tigger` VALUES ('126', 'TGAHJR8', 'years', '1', '4', '1480262400000|1478102400000|', '', '', '1482599100000|1482595200000|', '[]', '', '', '9', null, '1482676063000', null, '1');

-- ----------------------------
-- Table structure for work_order
-- ----------------------------
DROP TABLE IF EXISTS `work_order`;
CREATE TABLE `work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(50) NOT NULL,
  `section_code` varchar(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text,
  `type` int(11) NOT NULL COMMENT '工单类型：1-故障工单，2-巡检工单，3-保养工单， 4-备件更换工单，5-事故工单',
  `level` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `padding_by` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `comment` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工单表';

DROP TABLE IF EXISTS `history_data`;
CREATE TABLE `history_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oem_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `oem_name` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `supplier_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `supplier_name` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `customer_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `customer_name` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `equipment_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `equipment_name` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `point_code` varchar(20) DEFAULT NULL COMMENT '公司编码',
  `point_name` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `value` longtext COMMENT '部门编号',
  `time` datetime DEFAULT NULL COMMENT '资源编号',
  PRIMARY KEY (`id`),
  KEY `point_time` (`point_code`,`time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3240 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `user_code` varchar(255) DEFAULT NULL,
  `login_name` varchar(30) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `methods` varchar(30) DEFAULT NULL,
  `actionTime` varchar(30) DEFAULT NULL,
  `userIP` varchar(30) DEFAULT NULL,
  `create_time` varchar(32) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 正常操作 -1异常操作',
  `oem_code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20905 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_log
-- ----------------------------
DROP TABLE IF EXISTS `work_order_log`;
CREATE TABLE `work_order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `work_order` varchar(50) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `user_code` varchar(20) NOT NULL,
  `submit_time` varchar(20) NOT NULL,
  `type` int(11) NOT NULL COMMENT '内容类型：1-文字和图片，2-语音，3-视频',
  `operation` int(11) DEFAULT NULL,
  `target_company` varchar(20) DEFAULT NULL,
  `target_user` varbinary(20) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0-禁用，1-正常，9-删除',
  `solution` text COMMENT '解决方案',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `last_update_time` varchar(20) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='处理日志表';

-- ----------------------------
-- Records of work_order
-- ----------------------------
