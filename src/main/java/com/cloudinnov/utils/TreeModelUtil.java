package com.cloudinnov.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cloudinnov.model.TreeModel;

/**
 * @author guochao
 * @date 2016年3月21日上午11:46:34
 * @email chaoguo@cloudinnov.com
 * @remark 把一个list集合,里面的bean含有 parentId 转为树形式
 * @version
 */
public class TreeModelUtil {

	/**
	 * getChildTreeObjects
	 * 
	 * @Description: 根据父节点的ID获取所有子节点
	 * @param @param list
	 * @param @param praentId
	 * @param @return 参数
	 * @return List<TreeObject> 返回类型
	 */
	public List<TreeModel> getChildTreeObjects(List<TreeModel> list,
			String praentCode) {
		List<TreeModel> returnList = new ArrayList<TreeModel>();
		for (Iterator<TreeModel> iterator = list.iterator(); iterator
				.hasNext();) {
			TreeModel t = (TreeModel) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentCode().equals(praentCode)) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}

	/**
	 * recursionFn
	 * 
	 * @Description: 把一个list集合,里面的bean含有 parentId 转为树形式
	 * @param @param list
	 * @param @param t 参数
	 * @return void 返回类型
	 */
	private void recursionFn(List<TreeModel> list, TreeModel t) {
		List<TreeModel> childList = getChildList(list, t);// 得到子节点列表
		t.setChildren(childList);
		for (TreeModel tChild : childList) {
			if (hasChild(list, tChild)) {// 判断是否有子节点
				// returnList.add(TreeObject);
				Iterator<TreeModel> it = childList.iterator();
				while (it.hasNext()) {
					TreeModel n = (TreeModel) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	// 得到子节点列表
	private List<TreeModel> getChildList(List<TreeModel> list, TreeModel t) {

		List<TreeModel> tlist = new ArrayList<TreeModel>();
		Iterator<TreeModel> it = list.iterator();
		while (it.hasNext()) {
			TreeModel n = (TreeModel) it.next();
			if (n.getParentCode().equals(t.getCode())) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	List<TreeModel> returnList = new ArrayList<TreeModel>();

	/**
	 * getChildTreeObjects
	 * 
	 * @Description: 根据父节点的ID获取所有子节点
	 * @param @param list
	 * @param @param typeId 父节点id
	 * @param @param prefix 前缀
	 * @param @return 参数
	 * @return List<TreeObject> 返回类型
	 */
	public List<TreeModel> getChildTreeObjects(List<TreeModel> list,
			String parentCode, String prefix) {
		if (list == null)
			return null;
		for (Iterator<TreeModel> iterator = list.iterator(); iterator
				.hasNext();) {
			TreeModel node = (TreeModel) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (node.getParentCode().equals(parentCode)) {
				recursionFn(list, node, prefix);
			}
		}
		return returnList;
	}

	private void recursionFn(List<TreeModel> list, TreeModel node, String p) {
		List<TreeModel> childList = getChildList(list, node);// 得到子节点列表
		if (hasChild(list, node)) {// 判断是否有子节点
			returnList.add(node);
			Iterator<TreeModel> it = childList.iterator();

			while (it.hasNext()) {
				TreeModel n = (TreeModel) it.next();
				n.setName(p + n.getName());
				recursionFn(list, n, p + p);
			}
		} else {
			returnList.add(node);
		}
	}

	// 判断是否有子节点
	private boolean hasChild(List<TreeModel> list, TreeModel t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}

}
