package com.cloudinnov.dao;

import java.util.ArrayList;
import java.util.Map;

import com.cloudinnov.model.AlarmLog;
import com.github.pagehelper.Page;

public interface AlarmLogDao extends IBaseDao<AlarmLog>{
   
	Page<AlarmLog> search(Map<String, Object> params);
	
	/** 表中存在此设备的故障码,则把count发生次数加1
	 * @Title: updateCount 
	 * @Description: TODO
	 * @param alarmLog
	 * @return
	 * @return: int
	 */
	int updateCount(AlarmLog alarmLog);

	Page<AlarmLog> selectAlarm(Map<String, Object> params);

	int insertFireAlarmLog(AlarmLog alarmLog);
	
	Page<AlarmLog> selectLatestFireListPage();
	
	ArrayList<AlarmLog> selectLatestFireList();
}