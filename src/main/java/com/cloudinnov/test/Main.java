package com.cloudinnov.test;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;

public class Main {
	// 简体中文的编码范围从B0A1（45217）一直到F7FE（63486）
	private static int BEGIN = 45217;
	private static int END = 63486;
	// 按照声 母表示，这个表是在GB2312中的出现的第一个汉字，也就是说“啊”是代表首字母a的第一个汉字。
	// i, u, v都不做声母, 自定规则跟随前面的字母
	private static char[] chartable = { '啊', '芭', '擦', '搭', '蛾', '发', '噶', '哈', '哈', '击', '喀', '垃', '妈', '拿', '哦', '啪',
			'期', '然', '撒', '塌', '塌', '塌', '挖', '昔', '压', '匝', };
	// 二十六个字母区间对应二十七个端点
	// GB2312码汉字区间十进制表示
	private static int[] table = new int[27];
	// 对应首字母区间表
	private static char[] initialtable = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'H', 'J', 'K', 'I', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'T', 'T', 'W', 'X', 'Y', 'Z', };
	// 初始化
	static {
		for (int i = 0; i < 26; i++) {
			table[i] = gbValue(chartable[i]);// 得到GB2312码的首字母区间端点表，十进制。
		}
		table[26] = END;// 区间表结尾
	}

	// ------------------------public方法区------------------------
	// 根据一个包含汉字的字符串返回一个汉字拼音首字母的字符串 最重要的一个方法，思路如下：一个个字符读入、判断、输出
	public static String cn2py(String SourceStr) {
		String Result = "";
		int StrLength = SourceStr.length();
		int i;
		try {
			for (i = 0; i < StrLength; i++) {
				Result += Char2Initial(SourceStr.charAt(i));
			}
		} catch (Exception e) {
			Result = "";
			e.printStackTrace();
		}
		return Result;
	}
	// ------------------------private方法区------------------------
	/**
	 * 输入字符,得到他的声母,英文字母返回对应的大写字母,其他非简体汉字返回 '0' *
	 */
	private static char Char2Initial(char ch) {
		// 对英文字母的处理：小写字母转换为大写，大写的直接返回
		if (ch >= 'a' && ch <= 'z') {
			return (char) (ch - 'a' + 'A');
		}
		if (ch >= 'A' && ch <= 'Z') {
			return ch;
		}
		// 对非英文字母的处理：转化为首字母，然后判断是否在码表范围内，
		// 若不是，则直接返回。
		// 若是，则在码表内的进行判断。
		int gb = gbValue(ch);// 汉字转换首字母
		if ((gb < BEGIN) || (gb > END))// 在码表区间之前，直接返回
		{
			return ch;
		}
		int i;
		for (i = 0; i < 26; i++) {// 判断匹配码表区间，匹配到就break,判断区间形如“[,)”
			if ((gb >= table[i]) && (gb < table[i + 1])) {
				break;
			}
		}
		if (gb == END) {// 补上GB2312区间最右端
			i = 25;
		}
		return initialtable[i]; // 在码表区间中，返回首字母
	}
	/**
	 * 取出汉字的编码 cn 汉字
	 */
	private static int gbValue(char ch) {// 将一个汉字（GB2312）转换为十进制表示。
		String str = new String();
		str += ch;
		try {
			byte[] bytes = str.getBytes("GB2312");
			if (bytes.length < 2) {
				return 0;
			}
			return (bytes[0] << 8 & 0xff00) + (bytes[1] & 0xff);
		} catch (Exception e) {
			return 0;
		}
	}
	public static void main(String[] args) throws Exception {
		Date startTime = null;
		Date endTime = null;
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(startTime != null ? startTime : new Date());
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(endTime != null ? endTime : new Date());
		if (startTime == null || endTime == null) {
			oneDayStart.add(Calendar.YEAR, 0);
			oneDayEnd.add(Calendar.YEAR, 1);
			oneDayStart.set(Calendar.MONTH, 0);
			oneDayStart.set(Calendar.DAY_OF_MONTH, 1);
			oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
			oneDayEnd.set(Calendar.MONTH, 0);
			oneDayStart.set(Calendar.HOUR_OF_DAY, 0);
			oneDayStart.set(Calendar.MINUTE, 0);
			oneDayStart.set(Calendar.SECOND, 0);
			oneDayStart.set(Calendar.MILLISECOND, 0);
			oneDayEnd.set(Calendar.HOUR_OF_DAY, 0);
			oneDayEnd.set(Calendar.MINUTE, 0);
			oneDayEnd.set(Calendar.SECOND, 0);
			oneDayEnd.set(Calendar.MILLISECOND, 0);
		}
		SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd");
		String dayStart = formatTime.format(oneDayStart.getTime());
		String dayEnd = formatTime.format(oneDayEnd.getTime());
		// System.out.println(cn2py("重庆重视发展"));
		/*System.out.println(//序列号
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(8, 24));
		System.out.println(//日期
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(40, 54));
		System.out.println(//周期
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(54, 58));
		System.out.println(//车辆总数
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(58, 62));
		System.out.println(//车道数
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(62, 64));
		System.out.println(//
				"ff0000346748042816010290020115d0000023fa30251905060117001e00000200000000000000000000000000640000000000000000000082d3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
						.substring(62, 64));
		String content ="\\S05adsdsdsdsds";
		System.out.println(content.substring(content.indexOf("\\S")+4, content.length()));
		
		CarDetectorModel model = new CarDetectorModel();
		model.setDeviceId("6705556464");
		CarDetectorKafkaConsumer.carDetectorQueue.put(model);*/
		/*String summaryTime = "30541603110117";
		String time = summaryTime.substring(12, 14)  + summaryTime.substring(10,12) +summaryTime.substring(8, 10) + summaryTime.substring(4,6)+  summaryTime.substring(2,4) + summaryTime.substring(0,2);
		System.out.println(time);
		lineSeparator();
		 
		 //System.out.println(sumField("carDetectorData", "carTotal", new Criteria()));
		 Calendar oneDayStart = Calendar.getInstance();  
		 oneDayStart.add(Calendar.YEAR, 1);
		 oneDayStart.add(Calendar.MONTH, 0);  
		 oneDayStart.add(Calendar.DAY_OF_MONTH, 0);  
		 oneDayStart.set(Calendar.HOUR_OF_DAY, 0);  
		 oneDayStart.set(Calendar.MINUTE, 0);  
		 oneDayStart.set(Calendar.SECOND, 0);  
		 oneDayStart.set(Calendar.MILLISECOND,0);  
	        System.out.println(oneDayStart.getTime());
	       
	     //System.out.println(parseToColor("\cc255000000000").getBlue());
	     String content = "\\c255000000000";
	     String rgb = content.substring(content.indexOf("\\c")+2, content.indexOf("\\c") + 14);
	     System.out.println(rgb);*/
	}
	

	public static Color parseToColor(final String c) {
	    Color convertedColor = Color.ORANGE;
	    try {
	        convertedColor = new Color(Integer.parseInt(c, 16));
	    } catch(NumberFormatException e) {
	        // codes to deal with this exception
	    }
	    return convertedColor;
	}
	
	
	/**  
	 * 获取当前系统的换行符  
	 */    
	public static void lineSeparator() {    
		//注意在将流写入文件时，换行应根据操作系统的不同来决定。    
		//在程序我们应尽量使用System.getProperty("line.separator")来获取当前系统的换    
		//行符，而不是写/r/n或/n。    
		//这样写程序不够灵活    
		//当我们在java控制台输出的时候，/r和/n都能达到换行的效果。
		System.out.println(System.getProperty("line.separator"));
		if (System.getProperty("line.separator").equals("/n/r")) {    
		    System.out.println("//r//n is for windows");    
		} else if (System.getProperty("line.separator").equals("/r")) {    
		    System.out.println("//r is for Mac");    
		} else if (System.getProperty("line.separator").equals("/n")) {    
		    System.out.println("//n is for Unix/Linux");    
		}    
	}
}
