package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.HistoryData;

/**
 * @author chengning
 * @date 2016年2月24日下午4:44:36
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface HistoryDataLogic extends IBaseLogic<HistoryData> {
	/**
	 * insertHistoryList
	 * @Description: 批量插入历史数据
	 * @param @param list
	 * @param @param tableName
	 * @param @return 参数
	 * @return int 返回类型
	 */
	int insertHistoryList(List<HistoryData> list, String tableName);
	public int insert(Map<String, Object> map);
	List<List<String>> selectHistoryDatas(String codes, String startTime, String endTime, Map<String, Object> param);
	List<List<String>> selectHistoryDatasByMongoDB(String codes, String startTime, String endTime,
			Map<String, Object> param);
	/**
	 * 获取redis中的历史数据,存储到MongoDB中
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return 参数
	 * @return int 返回类型
	 */
	int saveDataToMongoDBByRedis();
}
