package com.cloudinnov.dao;

import com.cloudinnov.model.CarDetectorData;

public interface CarDetectorDataDao extends IBaseDao<CarDetectorData>{
	
	int carTotalToWebsocket();

}