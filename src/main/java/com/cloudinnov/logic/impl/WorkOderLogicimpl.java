package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.AccidentWorkOrderDao;
import com.cloudinnov.dao.WorkOrderDao;
import com.cloudinnov.logic.WorkOrderLogic;
import com.cloudinnov.model.AccidentWorkOrder;
import com.cloudinnov.model.WorkOrder;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service
public class WorkOderLogicimpl extends BaseLogicImpl<WorkOrder> implements WorkOrderLogic {
	@Autowired
	private WorkOrderDao workOrderDao;
	@Autowired
	private AccidentWorkOrderDao accidentWorkOrderDao;
	
	

	@Override
	public int save(WorkOrder workOrder) {
		String code = CodeUtil.workOrderCode(5);
		workOrder.setCode(code);	
		int result = workOrderDao.insert(workOrder);
		if (result == CommonUtils.SUCCESS_NUM) {
			return 1;
		} else {
			return 0;
		}
	}
	
	@Override
	public int accidentWorkOrderSave(WorkOrder workOrder) {
		String code = CodeUtil.workOrderCode(5);
		workOrder.setCode(code);
		workOrder.setType(5);
		// 将json类型的数据转化成list
		List<AccidentWorkOrder> list = JSON.parseArray(workOrder.getAccidentWorkOrder(),
				AccidentWorkOrder.class);
		int result = workOrderDao.insert(workOrder);
		if (result == 1) {
			AccidentWorkOrder accidentWorkOrder = new AccidentWorkOrder();
			accidentWorkOrder.setCode(workOrder.getCode());
			for (AccidentWorkOrder accident : list) {
				accidentWorkOrder.setCode(code);
				accidentWorkOrder.setFaultCode(accident.getFaultCode());
				accidentWorkOrder.setDescription(accident.getDescription());
				accidentWorkOrder.setHandingSuggestion(accident.getHandingSuggestion());
				accidentWorkOrderDao.insert(accidentWorkOrder);
			}
			return 1;
		} else {
			return 0;
		}
	}
	@Override
	public Page<WorkOrder> selectWorkOrderList(WorkOrder workOrder, int index, int size) {
		PageHelper.startPage(index, size);
		List<WorkOrder> list = workOrderDao.selectListByCondition(workOrder);
		return (Page<WorkOrder>) list;
	}
	@Override
	public AccidentWorkOrder selectWorkOrderInfo(AccidentWorkOrder accidentWorkOrder) {
		return accidentWorkOrderDao.selectEntityByCondition(accidentWorkOrder);
	}
	@Override
	public int deleteWorkOrder(WorkOrder workOrder) {
		// 将code放入accidentWorkOrder中
		AccidentWorkOrder accidentWorkOrder = new AccidentWorkOrder();
		accidentWorkOrder.setCode(workOrder.getCode());
		int result = workOrderDao.deleteByCondition(workOrder);
		if (result == 1) {
			accidentWorkOrderDao.deleteByCondition(accidentWorkOrder);
			return 1;
		} else {
			return 0;
		}
	}
	@Override
	public int updateWorkOrder(WorkOrder workOrder) {
		int result = workOrderDao.updateByCondition(workOrder);
		AccidentWorkOrder accidentWorkOrder = new AccidentWorkOrder();
		accidentWorkOrder.setCode(workOrder.getCode());
		// 将json类型的数据转化成list
		List<AccidentWorkOrder> list = JSON.parseArray(workOrder.getAccidentWorkOrder(),
				AccidentWorkOrder.class);
		if (result == 1) {
			// 根据code先删除事故工单
			accidentWorkOrderDao.deleteByCondition(accidentWorkOrder);
			for (AccidentWorkOrder accident : list) {
				accidentWorkOrder.setFaultCode(accident.getFaultCode());
				accidentWorkOrder.setDescription(accident.getDescription());
				accidentWorkOrder.setHandingSuggestion(accident.getHandingSuggestion());
				// 将事故工单再次添加
				accidentWorkOrderDao.insert(accidentWorkOrder);
			}
			return 1;
		} else {
			return 0;
		}
	}
	@Override
	public Page<AccidentWorkOrder> selectAccidentWorkOrderInfo(AccidentWorkOrder accidentWorkOrder,
			int index, int size) {
		// 查询事故工单
		PageHelper.startPage(index, size);
		List<AccidentWorkOrder> list = accidentWorkOrderDao.selectListByCondition(accidentWorkOrder);
		return (Page<AccidentWorkOrder>) list;
	}
}
