package com.cloudinnov.dao;

import com.cloudinnov.model.CustomReportIndex;

public interface CustomReportIndexDao extends IBaseDao<CustomReportIndex> {
	int deleteByPrimaryKey(Integer id);

	int insert(CustomReportIndex record);

	int insertSelective(CustomReportIndex record);

	CustomReportIndex selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CustomReportIndex record);

	int updateByPrimaryKey(CustomReportIndex record);
}