package com.cloudinnov.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.utils.FireAlarmServer;

/**
 * @version 1.5
 * @remark 维持与火灾控制器的TCP长连接
 */
public class FireAlarmConnectWanPing extends Thread {
	private static final String CODEDFORMAT = "gb2312";
	private static final int REVEIVEBUFFERSIZE = 1024 * 8;// 接收缓存8k
	private Socket socket;
	private boolean run = true;

	public FireAlarmConnectWanPing(Socket socket) {
		this.socket = socket;
	}
	@Override
	public void run() {
		connectReceive();
	}
	public void connectReceive() {
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		try {
			inputStream = (InputStream) socket.getInputStream();// 获取一个输入流，接收服务端的信息
			inputStreamReader = new InputStreamReader(inputStream, CODEDFORMAT);// 包装成字符流，提高效率,gbk
			bufferedReader = new BufferedReader(inputStreamReader);// 缓冲区
			char[] rcchar = new char[REVEIVEBUFFERSIZE];
			// TCP长连接，十秒一个心跳数据包，连接断开会抛出异常退出接收线程
			while (run) {// 长连接读取CRT信息，连接断开会抛出异常
				int len = bufferedReader.read(rcchar);// 堵塞读取
				if (len == -1) {
					throw new Exception("socket连接出错");
				}
				String crtstrs = String.valueOf(rcchar, 0, len);
				System.err.println(crtstrs);
				parseFireMsg(crtstrs);
			}
		} catch (Exception e) {
			// 与火灾系统连接中断
			// System.err.println("connect lost");
			try {
				bufferedReader.close();
				inputStreamReader.close();
				inputStream.close();
				socket.close();
			} catch (IOException e1) {
			}
		}
	}
	
	public void parseFireMsg(String fireMsg) {
		try {
			String[] receiveArr = fireMsg.split("\r\n");
			 
			if(receiveArr.length<4) {
				String pile=receiveArr[2];
				String[] alarmMsgArr=receiveArr[1].split("::");
				System.err.println(alarmMsgArr);
				String fireType=alarmMsgArr[0];
				System.err.println("result:"+fireType.trim().equals("手报"));
				if(fireType.trim().equals("手报")||fireType.trim().equals("火警")) {
					
					String[] fireContentArr = alarmMsgArr[1].split(" ");
					System.err.println(fireContentArr);
					String time = fireContentArr[2];
					String date = fireContentArr[3];
					String controlPanNum = fireContentArr[4].split(":")[1];//盘号
					String controlCardNum = fireContentArr[6].split(":")[1];//卡号
					String controlDeviceNum = fireContentArr[8].split(":")[1];//器件号
					System.err.println(fireType+"  "+time+"  "+date+"  "+controlPanNum+"  "+controlCardNum+"  "+controlDeviceNum+" "+pile);
					String diviceId=controlPanNum+controlCardNum+controlDeviceNum;
					
					FireCRTEvent fireCRTEvent = new FireCRTEvent();
					
					fireCRTEvent.setOccurrenceTime(System.currentTimeMillis());
					fireCRTEvent.setEvent(fireType);
					fireCRTEvent.setDevice(pile);
	 
					fireCRTEvent.setPosition(pile);
					fireCRTEvent.setDevice(fireType);
					fireCRTEvent.setDeviceId(diviceId);
					
					FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(fireCRTEvent));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("解析失败："+e);
		}
	}
}

 
