package com.cloudinnov.logic;

import com.cloudinnov.model.AuthRoleResource;

/**
 * @author guochao
 * @date 2016年3月22日下午3:12:03
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public interface AuthRoleResourceLogic extends IBaseLogic<AuthRoleResource> {
}
