package com.cloudinnov.test;


/*
 * 旧桩号（原桩号）为设计图纸方案，新桩号为现行实际标准
 * 建议切换一下平台桩号信息，与实际相符
 * 
 * */

public class PileTurn {
	
	//旧桩号起点，递增
	public static final String OLDPILE="K0+000";
	//新桩号起点，递减
	public static final String NEWPILE="K253+950";//size: 253950
	
	public static String oldTurnNew(String oldpile){
		Pile pile = new Pile(oldpile);
		return new Pile(253950-pile.size()).toString();
	}
	
	public static void main(String[] args) {
		
		System.out.println(PileTurn.oldTurnNew("K121+250"));
		
	}
	

}



class Pile{
	
	int k;
	int len;
	
	public Pile(String pile){
		int index = pile.indexOf("+");
		k = Integer.valueOf(pile.substring(1,index));
		len = Integer.valueOf(pile.substring(index,pile.length()));
	}
	
	public Pile(int size){
		k=size/1000;
		len=size%1000;
	}
	
	public int size(){
		return k*1000+len;
	}
	
	public String toString(){
		return "K"+k+"+"+ String.format("%03d", len);
	}
}
