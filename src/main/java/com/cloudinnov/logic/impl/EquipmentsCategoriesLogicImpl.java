package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.EquipmentsCategoriesDao;
import com.cloudinnov.dao.EquipmentsCategoryAttrDao;
import com.cloudinnov.logic.EquipmentsCategoriesLogic;
import com.cloudinnov.model.EquipmentsCategories;
import com.cloudinnov.model.EquipmentsCategoryAttr;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.PinYin2Abbreviation;

/**
 * @author guochao
 * @date 2016年2月17日下午6:00:15
 * @email chaoguo@cloudinnov.com
 * @remark 设备分类service接口实现
 * @version
 */
@Service
public class EquipmentsCategoriesLogicImpl extends BaseLogicImpl<EquipmentsCategories>
		implements EquipmentsCategoriesLogic {
	public static final String CATE_CDZSQ = "CDZSQ";
	public static final String CATE_HDZSQ = "HDZSQ";
	public static final String CATE_GQJCQ = "GQJCQ";
	private int digit = 5;
	@Autowired
	private EquipmentsCategoriesDao equipmentsCategoriesDao;
	@Autowired
	private EquipmentsCategoryAttrDao equipmentsCategoryAttrDao;

	public int save(EquipmentsCategories equipmentsCategories) {
		String code = PinYin2Abbreviation.cn2py(equipmentsCategories.getName());// 生成设备分类code
		if (equipmentsCategories.getParentId() != 0) {
			EquipmentsCategories model = new EquipmentsCategories();
			model.setParentId(equipmentsCategories.getParentId());
			model.setLanguage(equipmentsCategories.getLanguage());
			model = equipmentsCategoriesDao.selectEntityByParentId(model);
			code = code + model.getCode();
			equipmentsCategories.setName(equipmentsCategories.getName() + model.getName());
		}
		equipmentsCategories.setStatus(CommonUtils.STATUS_NORMAL);
		equipmentsCategories.setOemCode(equipmentsCategories.getOemCode());
		equipmentsCategories.setCode(code);
		equipmentsCategories.setCreateTime(String.valueOf(System.currentTimeMillis()));
		List<EquipmentsCategoryAttr> list = JSON.parseArray(equipmentsCategories.getPoropertyList(),
				EquipmentsCategoryAttr.class);// 将复杂的JSON类型的数据转换成List
		EquipmentsCategoryAttr attrModel = new EquipmentsCategoryAttr();// 创建设备分类附加属性对象
		int result = equipmentsCategoriesDao.insert(equipmentsCategories);
		if (result == 1) {
			for (EquipmentsCategoryAttr attr : list) {
				attrModel.setCategoryCode(code);
				attrModel.setCode(CodeUtil.equipmentCateAttrCode(digit));
				attrModel.setLanguage(equipmentsCategories.getLanguage());
				attrModel.setName(attr.getName());
				attrModel.setIsRequire(attr.getIsRequire());
				attrModel.setCustomTag(attr.getCustomTag());
				attrModel.setComment(attr.getComment());
				equipmentsCategoryAttrDao.insert(attrModel);
				equipmentsCategoryAttrDao.insertInfo(attrModel);
			}
			return equipmentsCategoriesDao.insertInfo(equipmentsCategories);
		} else {
			return 0;
		}
	}
	public int update(EquipmentsCategories equipmentsCategories) {
		List<EquipmentsCategoryAttr> list = JSON.parseArray(equipmentsCategories.getPoropertyList(),
				EquipmentsCategoryAttr.class);// 将复杂的JSON类型的数据转换成List
		EquipmentsCategoryAttr attrModel = null;// 创建设备分类附加属性对象
		int result = equipmentsCategoriesDao.updateByCondition(equipmentsCategories);
		if (result == 1) {
			attrModel = new EquipmentsCategoryAttr();// 删除原有分类附加属性
			attrModel.setCategoryCode(equipmentsCategories.getCode());
			equipmentsCategoryAttrDao.deleteByCondition(attrModel);
			for (EquipmentsCategoryAttr attr : list) {
				attrModel = new EquipmentsCategoryAttr();// 创建设备分类附加属性对象
				attrModel.setCode(attr.getCode());
				attrModel.setLanguage(equipmentsCategories.getLanguage());
				attrModel.setName(attr.getName());
				attrModel.setIsRequire(attr.getIsRequire());
				attrModel.setCustomTag(attr.getCustomTag());
				attrModel.setComment(attr.getComment());
				attrModel.setCategoryCode(equipmentsCategories.getCode());
				attrModel.setCode(CodeUtil.equipmentCateAttrCode(digit));
				equipmentsCategoryAttrDao.insert(attrModel);
				equipmentsCategoryAttrDao.insertInfo(attrModel);
			}
			return result;
		} else {
			return 0;
		}
	}
	@Override
	public int saveOtherLanguage(EquipmentsCategories equipmentsCategories) {
		return equipmentsCategoriesDao.insertInfo(equipmentsCategories);
	}
	@Override
	public List<EquipmentsCategories> addSelectList(EquipmentsCategories equipmentsCategories) {
		return equipmentsCategoriesDao.selectListByCondition(equipmentsCategories);
	}
	@Override
	public List<EquipmentsCategories> tableList(String[] codes, String language, String oemCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("codes", codes);
		map.put("oemCode", oemCode);
		return equipmentsCategoriesDao.tableList(map);
	}
	@Override
	public int updateOtherLanguage(EquipmentsCategories equipmentsCategories) {
		return equipmentsCategoriesDao.updateInfoByCondition(equipmentsCategories);
	}
}
