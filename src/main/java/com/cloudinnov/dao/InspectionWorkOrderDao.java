package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.InspectionWorkOrder;

public interface InspectionWorkOrderDao extends IBaseDao<InspectionWorkOrder> {
	public int deleteByPrimaryKey(Integer id);

	public int insert(InspectionWorkOrder record);

	public int insertSelective(InspectionWorkOrder record);

	public InspectionWorkOrder selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(InspectionWorkOrder record);

	public int updateByPrimaryKey(InspectionWorkOrder record);

	public List<InspectionWorkOrder> search(Map<String, Object> map);
}