package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthRoleDao;
import com.cloudinnov.dao.AuthRoleResourceDao;
import com.cloudinnov.dao.AuthRoleRightDao;
import com.cloudinnov.logic.AuthRoleLogic;
import com.cloudinnov.model.AuthRole;
import com.cloudinnov.model.AuthRoleResource;
import com.cloudinnov.model.AuthRoleRight;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author guochao
 * @date 2016年3月16日下午2:29:07
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class AuthRoleLogicImpl extends BaseLogicImpl<AuthRole> implements AuthRoleLogic {

	private int digit = 5;

	@Autowired
	private AuthRoleDao authRoleDao;
	@Autowired
	private AuthRoleResourceDao arrDao;
	@Autowired
	private AuthRoleRightDao authRoleRightDao;

	@Override
	public int saveOtherLanguage(AuthRole authRole) {
		return authRoleDao.insertInfo(authRole);
	}

	public int save(AuthRole authRole) {
		String roleCode = CodeUtil.roleCode(digit);
		authRole.setCode(roleCode);
		authRole.setStatus(CommonUtils.STATUS_NORMAL);
		String cretime = String.valueOf(System.currentTimeMillis());
		authRole.setCreateTime(cretime);
		int returnCode = authRoleDao.insert(authRole);
		if (returnCode == 1) {
			returnCode = authRoleDao.insertInfo(authRole);
			String resources = authRole.getResourceCodesArray();
			if (CommonUtils.isNotEmpty(resources)) {
				String[] resourceArray = resources.split(CommonUtils.SPLIT_COMMA);
				if (resourceArray != null) {
					int arrayLength = resourceArray.length;
					List<AuthRoleResource> list = new ArrayList<AuthRoleResource>();
					Map<String, Object> map = new HashMap<String, Object>();
					String companyCode = authRole.getCompanyCode();
					AuthRoleResource arr = null;
					for (int i = 0; i < arrayLength; i++) {
						arr = new AuthRoleResource();
						arr.setCompanyCode(companyCode);
						arr.setResourceCode(resourceArray[i]);
						arr.setRoleCode(roleCode);
						arr.setCreateTime(cretime);
						arr.setStatus(CommonUtils.STATUS_NORMAL);
						list.add(arr);
					}
					map.put("roleCode", roleCode);
					map.put("lists", list);
					arrDao.insertList(map);
				}
			}
			
			if (CommonUtils.isNotEmpty(authRole.getRightCodesArray())) {
				String[] rightArray = authRole.getRightCodesArray().split(CommonUtils.SPLIT_COMMA);
				if (rightArray != null && rightArray.length >0) {
					int arrayLength = rightArray.length;
					List<AuthRoleRight> list = new ArrayList<AuthRoleRight>();
					Map<String, Object> map = new HashMap<String, Object>();
					String companyCode = authRole.getCompanyCode();
					AuthRoleRight arr = null;
					for (int i = 0; i < arrayLength; i++) {
						arr = new AuthRoleRight();
						arr.setCompanyCode(companyCode);
						arr.setRightCode(rightArray[i]);
						arr.setRoleCode(roleCode);
						arr.setCreateTime(cretime);
						arr.setStatus(CommonUtils.STATUS_NORMAL);
						list.add(arr);
					}
					map.put("roleCode", roleCode);
					map.put("lists", list);
					authRoleRightDao.insertList(map);
				}
			}
		}
		return returnCode;
	}

	public int update(AuthRole authRole) {
		int returnCode = authRoleDao.updateByCondition(authRole);
		String roleCode = authRole.getCode();
		String cretime = authRole.getCreateTime();
		if (returnCode == 1) {
			String resources = authRole.getResourceCodesArray();
			if (resources != null) {
				String[] resourceArray = resources.split(",");
				if (resourceArray != null) {
					int arrayLength = resourceArray.length;
					List<AuthRoleResource> list = new ArrayList<AuthRoleResource>();
					Map<String, Object> map = new HashMap<String, Object>();
					String companyCode = authRole.getCompanyCode();
					for (int i = 0; i < arrayLength; i++) {
						AuthRoleResource arr = new AuthRoleResource();
						arr.setCompanyCode(companyCode);
						arr.setResourceCode(resourceArray[i]);
						arr.setRoleCode(roleCode);
						arr.setCreateTime(cretime);
						arr.setStatus(CommonUtils.STATUS_NORMAL);
						list.add(arr);
					}
					map.put("roleCode", roleCode);
					map.put("lists", list);
					arrDao.insertList(map);
				}
			}
		}
		return returnCode;
	}

	@Override
	public int updateOtherLanguage(AuthRole authRole) {
		return authRoleDao.updateByCondition(authRole);
	}
	
	public Page<AuthRole> selectListPage(PageModel page,AuthRole authRole,boolean isOrderBy){
		PageHelper.startPage(page.getIndex(), page.getSize());
		if(isOrderBy){
			PageHelper.orderBy("create_time desc");
		}	
		Page<AuthRole> list = (Page<AuthRole>) authRoleDao.selectListByCondition(authRole);
		if(list!=null && list.size()>0){
			for(AuthRole role : list.getResult()){
				if(role.getRoleType()!=null && role.getRoleType() == CommonUtils.ROLE_TYPE_SYSTEM){
					role.setDeleteStatus(CommonUtils.ROLE_DELETE_NO);
				}
			}
		}
		return list;
	}
	
	public List<AuthRole> selectList(AuthRole authRole,boolean isOrderBy){
		if(isOrderBy){
			PageHelper.orderBy("create_time desc");
		}	
		Page<AuthRole> list = (Page<AuthRole>) authRoleDao.selectListByCondition(authRole);
		if(list!=null && list.size()>0){
			for(AuthRole role : list.getResult()){
				if(role.getRoleType()!=null && role.getRoleType() == CommonUtils.ROLE_TYPE_SYSTEM){
					role.setDeleteStatus(CommonUtils.ROLE_DELETE_NO);
				}
			}
		}
		return list;
	}
}
