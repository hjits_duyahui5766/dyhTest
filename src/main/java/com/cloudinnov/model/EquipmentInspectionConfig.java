package com.cloudinnov.model;

import java.util.List;

public class EquipmentInspectionConfig extends BaseModel {

	private String customerCode;

	private String customerName;

	private String equipmentCode;

	private String equipmentName;

	private String beginDate;

	private Integer period;

	private Integer advanceDays;
	
	private String items;
	
	private String customerPerson;
	
	private String imageUrl;
	
	private List<EquipmentInspectionWorkItem> workItems;
	
	private String title;
	
	private Integer currentIndex;
	
	private Integer state;
	
	public String getCustomerPerson() {
		return customerPerson;
	}

	public void setCustomerPerson(String customerPerson) {
		this.customerPerson = customerPerson;
	}

	public List<EquipmentInspectionWorkItem> getWorkItems() {
		return workItems;
	}

	public void setWorkItems(List<EquipmentInspectionWorkItem> workItems) {
		this.workItems = workItems;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Integer getAdvanceDays() {
		return advanceDays;
	}

	public void setAdvanceDays(Integer advanceDays) {
		this.advanceDays = advanceDays;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	
}