package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.TriggerLogic;
import com.cloudinnov.model.Trigger;
import com.github.pagehelper.Page;

@RequestMapping("/webapi/tigger")
@Controller
public class TiggerController extends BaseController {

	@Autowired
	private TriggerLogic tiggerLogic;

	/**
	 * 触发器添加
	 * 
	 * @param tigger
	 * @param day
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器添加")
	public void save(Trigger tigger, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = tiggerLogic.save(tigger);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 触发器删除
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器删除")
	public void delete(Trigger tigger, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = tiggerLogic.delete(tigger);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 触发器修改
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器修改")
	public void update(Trigger tigger, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = tiggerLogic.update(tigger);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 根据触发器code修改是否启用状态
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateOnOff", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "根据触发器code修改是否启用状态")
	public void updateOnOff(Trigger tigger, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = tiggerLogic.updateOnOff(tigger);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);

	}

	/**
	 * 触发器列表（带分页）
	 * 
	 * @param tigger
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器列表")
	public void listPage(Trigger tigger, int index, int size, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Page<Trigger> tiggers = tiggerLogic.selectListPage(index, size, tigger, false);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (tiggers != null) {
			map.put("list", tiggers);
			map.put("total", tiggers.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");

		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 触发器列表（无分页）
	 * 
	 * @param tigger
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/listNoPage", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器列表")
	public void listNoPage(Trigger tigger, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<Trigger> tiggers = tiggerLogic.selectList(tigger, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (tiggers != null) {
			map.put("list", tiggers);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");

		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 获取触发器信息（返回单条数据）
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "触发器管理", methods = "触发器信息（单条）")
	public void select(Trigger tigger, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Trigger result = tiggerLogic.select(tigger);
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

}
