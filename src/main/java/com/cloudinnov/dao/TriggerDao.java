package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.Trigger;

public interface TriggerDao extends IBaseDao<Trigger> {
	public List<Trigger> selectListByFaultCode(String faultCode);
	public int updateOnOff(Trigger trigger);
	public List<Trigger> selectTriggerByFaultCode();
}
