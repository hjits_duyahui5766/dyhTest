package com.cloudinnov.dao;


import com.cloudinnov.model.FaultTranslationLibraries;

public interface FaultTranslationLibrariesDao extends IBaseDao<FaultTranslationLibraries>{

	public int insertInfo(FaultTranslationLibraries faultTransLib);
	
	public int updateInfoByCondition(FaultTranslationLibraries faultTransLib);
}