<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
	xmlns:tx="http://www.springframework.org/schema/tx" xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:task="http://www.springframework.org/schema/task" xmlns:int="http://www.springframework.org/schema/integration" 
	xmlns:int-kafka="http://www.springframework.org/schema/integration/kafka" xmlns:rabbit="http://www.springframework.org/schema/rabbit"
	xsi:schemaLocation="http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.1.xsd
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.1.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.1.xsd
		http://www.springframework.org/schema/rabbit http://www.springframework.org/schema/rabbit/spring-rabbit-1.4.xsd 
		http://www.springframework.org/schema/integration/kafka http://www.springframework.org/schema/integration/kafka/spring-integration-kafka.xsd
      	http://www.springframework.org/schema/integration http://www.springframework.org/schema/integration/spring-integration.xsd">

	<!-- 隐式地向 Spring 容器注册 -->
	<context:annotation-config />
	<context:component-scan base-package="com.cloudinnov.dao" />
	<context:component-scan base-package="com.cloudinnov.dao.mongo"/>
	<context:component-scan base-package="com.cloudinnov.logic" />
	<context:component-scan base-package="com.cloudinnov.task" />
	
	<context:property-placeholder location="classpath:config.properties"
		ignore-unresolvable="true" />
	
	<!-- 数据源 -->
	<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource"
		init-method="init" destroy-method="close">
		<property name="username" value="${jdbc.user}" />
		<property name="password" value="${jdbc.password}" />
		<property name="url" value="${jdbc.url}" />
		<property name="driverClassName" value="${jdbc.driverClass}" />

		<!-- 配置初始化大小、最小、最大 -->
		<property name="initialSize" value="0" />
		<property name="maxActive" value="500" />
		<property name="maxIdle" value="20" />
		<property name="minIdle" value="50" />
		<!-- 配置获取连接等待超时的时间 -->
		<property name="maxWait" value="6000" />

		<!-- 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 -->
		<property name="timeBetweenEvictionRunsMillis" value="60000" />

		<!-- 配置一个连接在池中最小生存的时间，单位是毫秒 -->
		<property name="minEvictableIdleTimeMillis" value="300000" />

		<property name="validationQuery" value="SELECT 'x'" />
		<property name="testWhileIdle" value="true" />
		<property name="testOnBorrow" value="false" />
		<property name="testOnReturn" value="false" />
		
		<property name="maxOpenPreparedStatements" value="20" />
		<property name="removeAbandoned" value="true" />
		<property name="removeAbandonedTimeout" value="500" />
		
		<!-- 打开PSCache，并且指定每个连接上PSCache的大小 -->
		<property name="poolPreparedStatements" value="true" />
		<property name="maxPoolPreparedStatementPerConnectionSize"
			value="20" />

		<!-- 配置监控统计拦截的filters，去掉后监控界面sql无法统计 -->
		<property name="filters" value="stat" />
	</bean>
	<!-- 使用ehcache缓存 -->
	<bean id="ehCacheManager"
		class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean">
		<property name="configLocation" value="classpath:ehcache.xml" />
	</bean>

	<!-- sqlSessionFactory -->
	<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
		<property name="dataSource" ref="dataSource" />
		<property name="typeAliasesPackage" value="com.cloudinnov.model" />
		<property name="mapperLocations">
			<list>
				<value>classpath:com/cloudinnov/mapper/*Mapper.xml</value>
			</list>
		</property>
		<property name="plugins">
			<array>
				<bean class="com.github.pagehelper.PageHelper">
					<property name="properties">
						<value>
							dialect=MySQL
							offsetAsPageNum=true
							rowBoundsWithCount=true
							pageSizeZero=true
							reasonable=true
							params=pageNum=start;pageSize=limit;pageSizeZero=zero;reasonable=heli;count=contsql
						</value>
					</property>
				</bean>
			</array>
		</property>
	</bean>

	<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
		<property name="basePackage" value="com.cloudinnov.dao" />
		<property name="sqlSessionFactoryBeanName" value="sqlSessionFactory" />
	</bean>

	<!-- 事务 -->
	<bean id="txManager"
		class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
		<property name="dataSource" ref="dataSource" />
	</bean>

	<tx:advice id="txAdvice" transaction-manager="txManager">
		<tx:attributes>
			<tx:method name="save*" propagation="REQUIRED" />
			<tx:method name="insert*" propagation="REQUIRED" />
			<tx:method name="update*" propagation="REQUIRED" />
			<tx:method name="modify*" propagation="REQUIRED" />
			<tx:method name="delete*" propagation="REQUIRED" />

			<tx:method name="select*" read-only="true" />
			<tx:method name="query*" read-only="true" />
			<tx:method name="get*" read-only="true" />
			<tx:method name="list*" read-only="true" />
		</tx:attributes>
	</tx:advice>
	
	<!-- 配置到实现类可以做到异常 数据库回滚 -->
	<aop:config>
		<aop:advisor advice-ref="txAdvice"
			pointcut="execution(* com.cloudinnov.logic.impl..*(..))" />
	</aop:config>

	<bean id="jedisPoolConfig" class="redis.clients.jedis.JedisPoolConfig">
		<property name="maxTotal" value="${redis.pool.maxActive}" />
		<property name="maxIdle" value="${redis.pool.maxIdle}" />
		<property name="maxWaitMillis" value="${redis.pool.maxWait}" />
		<property name="testOnBorrow" value="${redis.pool.testOnBorrow}" />
	</bean>

	<!-- redis的连接池pool，不是必选项：timeout/password -->
	<bean id="jedisPool" class="redis.clients.jedis.JedisPool">
		<constructor-arg index="0" ref="jedisPoolConfig" />
		<constructor-arg index="1" value="${redis.hostname}" />
		<constructor-arg index="2" value="${redis.port}" type="int" />
		<constructor-arg index="3" value="60000" type="int" />
		<constructor-arg index="4" value="${redis.password}" />
	</bean>

	<!-- 全局初始化 -->
	 <!--    <bean id="contextListener" class="com.cloudinnov.task.ContextListener"
		init-method="initialize" />  -->

	<bean id="springUtils" class="com.cloudinnov.utils.support.spring.SpringUtils"/>
	
	<!-- rabbitMQ连接配置 -->
	<rabbit:connection-factory id="connectionFactory"
		host="${rabbitMQ.hostname}" username="${rabbitMQ.username}" password="${rabbitMQ.password}"
		port="${rabbitMQ.port}" virtual-host="${rabbitMQ.virtualhost}"/> 
	<rabbit:admin connection-factory="connectionFactory" />
	<!-- 实时queue 队列声明-->
   	<rabbit:queue id="realtime" durable="true" auto-delete="false" exclusive="false" name="${rabbitMQ.realtime.queue.name}"/>
   	<!-- 故障queue 队列声明-->
   	<rabbit:queue id="fault" durable="true" auto-delete="false" exclusive="false" name="${rabbitMQ.fault.queue.name}"/>
   	<!-- 紧急电话queue 队列声明-->
   	<rabbit:queue id="call" durable="true" auto-delete="false" exclusive="false" name="${rabbitMQ.call.queue.name}"/>
   	<!-- 火灾报警queue 队列声明-->
   	<rabbit:queue id="fire" durable="true" auto-delete="false" exclusive="false" name="${rabbitMQ.fire.queue.name}"/>
   	
   	<!-- 队列交换机绑定 exchange queue binging key 绑定 auto-declare="true" 不存在自动创建 -->
	<rabbit:direct-exchange name="${rabbitMQ.realtime.exchange.name}"
		auto-delete="false" id="realtimeExchange" auto-declare="true">
		<rabbit:bindings>
			<rabbit:binding queue="realtime" key="${rabbitMQ.realtime.routing.key}" />
		</rabbit:bindings>
	</rabbit:direct-exchange>
	<rabbit:direct-exchange name="${rabbitMQ.fault.exchange.name}"
		auto-delete="false" id="faultExchange" auto-declare="true" >
		<rabbit:bindings>
			<rabbit:binding queue="fault" key="${rabbitMQ.fault.routing.key}" />
		</rabbit:bindings>
	</rabbit:direct-exchange>
	<rabbit:direct-exchange name="${rabbitMQ.call.exchange.name}"
		auto-delete="false" id="callExchange" auto-declare="true" >
		<rabbit:bindings>
			<rabbit:binding queue="call" key="${rabbitMQ.call.routing.key}" />
		</rabbit:bindings>
	</rabbit:direct-exchange>
	<rabbit:direct-exchange name="${rabbitMQ.fire.exchange.name}"
		auto-delete="false" id="fireExchange" auto-declare="true" >
		<rabbit:bindings>
			<rabbit:binding queue="fire" key="${rabbitMQ.fire.routing.key}" />
		</rabbit:bindings>
	</rabbit:direct-exchange>
	
	<!-- spring template声明 -->
	<rabbit:template id="realTimeTemplate"
		connection-factory="connectionFactory" exchange="${rabbitMQ.realtime.exchange.name}"
		routing-key="${rabbitMQ.realtime.routing.key}" queue="${rabbitMQ.realtime.queue.name}"/>
		
	<rabbit:template id="faultTemplate"
		connection-factory="connectionFactory" exchange="${rabbitMQ.fault.exchange.name}"
		routing-key="${rabbitMQ.fault.routing.key}" queue="${rabbitMQ.fault.queue.name}"/>
		
	<rabbit:template id="callTemplate"
		connection-factory="connectionFactory" exchange="${rabbitMQ.call.exchange.name}"
		routing-key="${rabbitMQ.call.routing.key}" queue="${rabbitMQ.call.queue.name}" />
		
	<rabbit:template id="fireTemplate"
		connection-factory="connectionFactory" exchange="${rabbitMQ.fire.exchange.name}"
		routing-key="${rabbitMQ.fire.routing.key}" queue="${rabbitMQ.fire.queue.name}" />

	<!-- 故障队列监听 queue litener 观察 监听模式 当有消息到达时会通知监听在对应的队列上的监听对象 -->
	<bean id="faultQueueLitener" class="com.cloudinnov.task.mq.FaultMQConsumer" />
	<rabbit:listener-container
		connection-factory="connectionFactory" acknowledge="manual" prefetch="1" >
		<rabbit:listener queues="fault" ref="faultQueueLitener" />
	</rabbit:listener-container>
	
	<!-- 紧急电话队列监听 queue litener 观察 监听模式 当有消息到达时会通知监听在对应的队列上的监听对象 -->
	<bean id="callQueueLitener" class="com.cloudinnov.task.mq.CallMQConsumer" />
	<rabbit:listener-container
		connection-factory="connectionFactory" acknowledge="manual" prefetch="1">
		<rabbit:listener queues="call" ref="callQueueLitener" />
	</rabbit:listener-container>
	
	<!-- 火灾报警队列监听 queue litener 观察 监听模式 当有消息到达时会通知监听在对应的队列上的监听对象 -->
	<bean id="fireQueueLitener" class="com.cloudinnov.task.mq.FireMQConsumer" />
	<rabbit:listener-container
		connection-factory="connectionFactory" acknowledge="manual" prefetch="1">
		<rabbit:listener queues="fire" ref="fireQueueLitener" />
	</rabbit:listener-container>
		
	<!--实时数据状态监测Job -->
	<bean id="pointValueMonitorQuartzJob" class="com.cloudinnov.task.PointValuePushQuartzJob"></bean>
	<bean id="pointValueMonitorQuartzJobMethod"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject">
			<ref bean="pointValueMonitorQuartzJob" />
		</property>
		<property name="targetMethod">
			<value>monitor</value>
		</property>
	</bean>
	
	<!--数据 搜集job -->
	<bean id="collectDataQuartzJob" class="com.cloudinnov.task.CollectDataQuartzJob"></bean>
	<bean id="collectDataQuartzJobMethod"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject">
			<ref bean="collectDataQuartzJob" />
		</property>
		<property name="targetMethod">
			<value>collectData</value>
		</property>
	</bean>
	
	<!-- 车辆日数据采集 -->
	<bean id="collectCarDayDataQuartzJob" class="com.cloudinnov.task.CollectCarDayDataQuartzJob"></bean>
	<bean id="collectCarDayDataQuartzJobMethod" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject">
			<ref bean="collectCarDayDataQuartzJob"/>
		</property>
		<property name="targetMethod">
			<value>collecCarDayData</value>
		</property>
	</bean>
	
	<!--调度触发器 -->
	<bean id="pointValueMonitorQuartzJobCronTriggerBean"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="pointValueMonitorQuartzJobMethod"></property>
		<property name="cronExpression" value="${quartz.point.indextype.push.cron}"></property>
	</bean>
	
	<bean id="collectDataMonitorQuartzJobCronTriggerBean"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="collectDataQuartzJobMethod"></property>
		<property name="cronExpression" value="${quartz.collectData.cron}"></property>
	</bean>
	
	<bean id="collectCarDayDataQuartzJobCronTriggerBean"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="collectCarDayDataQuartzJobMethod"></property>
		<property name="cronExpression" value="${quartz.collectCarDayData.cron}"></property>
	</bean>
	
	<!--调度工厂 -->
	<bean id="SpringJobSchedulerFactoryBean"
		class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
		<property name="triggers">
			<list>
				<ref bean="pointValueMonitorQuartzJobCronTriggerBean" />
				<ref bean="collectDataMonitorQuartzJobCronTriggerBean" />
				<ref bean="collectCarDayDataQuartzJobCronTriggerBean"/>
			</list>
		</property>
	</bean>
</beans>
