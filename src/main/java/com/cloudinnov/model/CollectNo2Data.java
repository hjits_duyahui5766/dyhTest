package com.cloudinnov.model;

import java.util.Date;

public class CollectNo2Data {
	
	private String equipCode;
	private String categoryCode;
	private String sectionName;
	private String sectionCode;
    private Date utcTime;
    private long timeMillis;
    private double No2Value;
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public Date getUtcTime() {
		return utcTime;
	}
	public void setUtcTime(Date utcTime) {
		this.utcTime = utcTime;
	}
	public long getTimeMillis() {
		return timeMillis;
	}
	public void setTimeMillis(long timeMillis) {
		this.timeMillis = timeMillis;
	}
	public double getNo2Value() {
		return No2Value;
	}
	public void setNo2Value(double no2Value) {
		No2Value = no2Value;
	}
    

}
