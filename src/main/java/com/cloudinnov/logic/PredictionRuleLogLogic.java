package com.cloudinnov.logic;

import com.cloudinnov.model.PredictionRuleLog;
import com.github.pagehelper.Page;

public interface PredictionRuleLogLogic extends IBaseLogic<PredictionRuleLog>{
	
	String insertPredictionRuleLog(PredictionRuleLog predictionRuleLog);
	
	Page<PredictionRuleLog> selectPredictionRuleLog(PredictionRuleLog predictionRuleLog, int index, int size);
	
	int updatePredictionRuleLog(PredictionRuleLog predictionRuleLog);
	
}
