package com.cloudinnov.model;

public class EquipmentModel extends BaseModel {

	private String equipmentCategoryCode;

	private String name;
	
	private String equipmentCateName;

	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getEquipmentCateName() {
		return equipmentCateName;
	}

	public void setEquipmentCateName(String equipmentCateName) {
		this.equipmentCateName = equipmentCateName;
	}

	public String getEquipmentCategoryCode() {
		return equipmentCategoryCode;
	}

	public void setEquipmentCategoryCode(String equipmentCategoryCode) {
		this.equipmentCategoryCode = equipmentCategoryCode;
	}
}