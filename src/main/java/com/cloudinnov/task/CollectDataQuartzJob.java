package com.cloudinnov.task;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.model.CollectCoviData;
import com.cloudinnov.model.CollectGqData;
import com.cloudinnov.model.CollectLightData;
import com.cloudinnov.model.CollectNo2Data;
import com.cloudinnov.model.CollectWsData;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.utils.CommonUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class CollectDataQuartzJob {

	private static final Logger LOG = LoggerFactory.getLogger(CollectDataQuartzJob.class);
	private static final String POINT_NAME = "point:";
	private static final String VALUE_NAME = ":value";
	private static final String REALTIME_SPLITTER_LEVEL1 = ",";
	private static final String REALTIME_SPLITTER_LEVEL2 = ";";
	private static final int REALTIME_TIME_LEVEL1 = 0;
	private static final int REALTIME_TIME_OUT_SECOND = 60;
	private static final int REALTIME_TIME_OUT_MILL = 1000;

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	JedisPool jedisPool;
	@Autowired
	EquipmentsDao equipmentsDao;

	Jedis redis;
	private List<Equipments> NO2Data;
	private List<Equipments> CoviData;
	private List<Equipments> njdData;
	private List<Equipments> wsData;
	private List<Equipments> gqData;
	
	private void collectData() {
		saveCollectDataToMongoDB();
	}

	public void saveCollectDataToMongoDB() {

		try {
			redis = jedisPool.getResource();
			 
			if(CommonUtils.COLLECT_DATA_MAP.get("NO2JCQ")==null) {
				NO2Data = equipmentsDao.selectEquipmentPointByCategoryCode("NO2JCQ");
				CommonUtils.COLLECT_DATA_MAP.put("NO2JCQ", NO2Data);
			}else {
				NO2Data=(List<Equipments>) CommonUtils.COLLECT_DATA_MAP.get("NO2JCQ");
			}
			 
			for (int i = 0; i < NO2Data.size(); i++) {
				Equipments equipments = NO2Data.get(i);
				getRedisKey(equipments, redis);
			}
			
			if(CommonUtils.COLLECT_DATA_MAP.get("COVIJCQ")==null) {
				CoviData = equipmentsDao.selectEquipmentPointByCategoryCode("COVIJCQ");
				CommonUtils.COLLECT_DATA_MAP.put("COVIJCQ", CoviData);
			}else {
				CoviData=(List<Equipments>) CommonUtils.COLLECT_DATA_MAP.get("COVIJCQ");
			}
			 
			for (int i = 0; i < CoviData.size(); i++) {
				Equipments equipments = CoviData.get(i);
				getRedisKey(equipments, redis);
			}

			if(CommonUtils.COLLECT_DATA_MAP.get("NJDJCQ")==null) {
				njdData = equipmentsDao.selectNjdEquipmentByCategoryCode("NJDJCQ");
				CommonUtils.COLLECT_DATA_MAP.put("NJDJCQ", njdData);
			}else {
				njdData=(List<Equipments>) CommonUtils.COLLECT_DATA_MAP.get("NJDJCQ");
			}
			  
			for (int i = 0; i < njdData.size(); i++) {
				Equipments equipments = njdData.get(i);
				getRedisKey(equipments, redis);
			}

			if(CommonUtils.COLLECT_DATA_MAP.get("FSFXJCQ")==null) {
				wsData = equipmentsDao.selectEquipmentPointByCategoryCode("FSFXJCQ");
				CommonUtils.COLLECT_DATA_MAP.put("FSFXJCQ", wsData);
			}else {
				wsData=(List<Equipments>) CommonUtils.COLLECT_DATA_MAP.get("FSFXJCQ");
			}
			 
			for (int i = 0; i < wsData.size(); i++) {
				Equipments equipments = wsData.get(i);
				getRedisKey(equipments, redis);
			}
			
			if(CommonUtils.COLLECT_DATA_MAP.get("GQJCQ")==null) {
				gqData = equipmentsDao.selectEquipmentPointByCategoryCode("GQJCQ");
				CommonUtils.COLLECT_DATA_MAP.put("GQJCQ", gqData);
			}else {
				gqData=(List<Equipments>) CommonUtils.COLLECT_DATA_MAP.get("GQJCQ");
			}
			 
			for (int i = 0; i < gqData.size(); i++) {
				Equipments equipments = gqData.get(i);
				getRedisKey(equipments, redis);
			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			// TODO: handle finally clause
			jedisPool.returnResource(redis);
		}

	}

	private void getRedisKey(Equipments equipments, Jedis redis) {

		if (equipments.getCategoryCode().equals("NO2JCQ")) {
			for (int i = 0; i < equipments.getPoints().size(); i++) {
				if (equipments.getPoints().get(i).getIndexType().equals("NO2")) {
					String code = equipments.getPoints().get(i).getCode();
					String redisValue = getRedisValue(code, redis);
					CollectNo2Data collectNo2Data = new CollectNo2Data();
					collectNo2Data.setCategoryCode(equipments.getCategoryCode());
					collectNo2Data.setEquipCode(equipments.getCode());
					collectNo2Data.setNo2Value(Double.parseDouble(redisValue));
					collectNo2Data.setSectionCode(equipments.getSectionCode());
					collectNo2Data.setSectionName(equipments.getSectionName());
					collectNo2Data.setTimeMillis(System.currentTimeMillis());
					collectNo2Data.setUtcTime(new Date());
					mongoTemplate.insert(collectNo2Data);

				}
			}
		} else if (equipments.getCategoryCode().equals("COVIJCQ")) {

			CollectCoviData collectCoviData = new CollectCoviData();
			collectCoviData.setCategoryCode(equipments.getCategoryCode());
			collectCoviData.setEquipCode(equipments.getCode());
			collectCoviData.setSectionCode(equipments.getSectionCode());
			collectCoviData.setSectionName(equipments.getSectionName());
			collectCoviData.setTimeMillis(System.currentTimeMillis());
			collectCoviData.setUtcTime(new Date());
			for (int i = 0; i < equipments.getPoints().size(); i++) {
				if (equipments.getPoints().get(i).getIndexType().equals("CO")
						|| equipments.getPoints().get(i).getIndexType().equals("VI")) {
					String code = equipments.getPoints().get(i).getCode();
					String redisValue = getRedisValue(code, redis);
					if (equipments.getPoints().get(i).getIndexType().equals("CO")) {
						collectCoviData.setCoValue(Double.parseDouble(redisValue));
					} else {
						collectCoviData.setViValue(Double.parseDouble(redisValue));
					}
				}
			}
			mongoTemplate.insert(collectCoviData);

		} else if (equipments.getCategoryCode().equals("NJDJCQ")) {
			String equipCode = equipments.getCode();
			String redisValue = getLightRedisValue(equipCode, redis);
			CollectLightData collectNjdData = new CollectLightData();
			collectNjdData.setCategoryCode(equipments.getCategoryCode());
			collectNjdData.setEquipCode(equipments.getCode());
			collectNjdData.setValue(Double.parseDouble(redisValue));
			collectNjdData.setSectionCode(equipments.getSectionCode());
			collectNjdData.setSectionName(equipments.getSectionName());
			collectNjdData.setTimeMillis(System.currentTimeMillis());
			collectNjdData.setUtcTime(new Date());
			mongoTemplate.insert(collectNjdData);
		} else if (equipments.getCategoryCode().equals("FSFXJCQ")) {
			for (int i = 0; i < equipments.getPoints().size(); i++) {
				if (equipments.getPoints().get(i).getIndexType().equals("WIND_SPEED")) {
					String code = equipments.getPoints().get(i).getCode();
					String redisValue = getRedisValue(code, redis);
					CollectWsData collectWsData = new CollectWsData();
					collectWsData.setCategoryCode(equipments.getCategoryCode());
					collectWsData.setEquipCode(equipments.getCode());
					collectWsData.setWsData(Double.parseDouble(redisValue));
					collectWsData.setSectionCode(equipments.getSectionCode());
					collectWsData.setSectionName(equipments.getSectionName());
					collectWsData.setTimeMillis(System.currentTimeMillis());
					collectWsData.setUtcTime(new Date());
					mongoTemplate.insert(collectWsData);

				}
			}

		} else if (equipments.getCategoryCode().equals("GQJCQ")) {
			for (int i = 0; i < equipments.getPoints().size(); i++) {
				if (equipments.getPoints().get(i).getIndexType().equals("LIGHT_INTENSITY")
						|| equipments.getPoints().get(i).getIndexType().equals("ILLUMINATION")) {
					String code = equipments.getPoints().get(i).getCode();
					String redisValue = getRedisValue(code, redis);
					CollectGqData collectGqData = new CollectGqData();
					collectGqData.setCategoryCode(equipments.getCategoryCode());
					collectGqData.setEquipCode(equipments.getCode());
					collectGqData.setGqData(Double.parseDouble(redisValue));
					collectGqData.setSectionCode(equipments.getSectionCode());
					collectGqData.setSectionName(equipments.getSectionName());
					collectGqData.setTimeMillis(System.currentTimeMillis());
					collectGqData.setUtcTime(new Date());
					mongoTemplate.insert(collectGqData);

				}
			}
		}
	}

	private String getRedisValue(String code, Jedis redis) {
		String key = POINT_NAME + code + VALUE_NAME;
		String value = redis.lindex(key, 0);
		if (value != null) {
			value = value.replace(" ", "");
			String[] valueArray = value.split(REALTIME_SPLITTER_LEVEL1);
			/**
			 * 判断实时redis中的第一条数据时间和当前时间相差是否大于60s
			 */
			long timeout = 0L;
			if (valueArray[0].length() > 11) {
				timeout = (System.currentTimeMillis() - Long.parseLong(valueArray[0])) / REALTIME_TIME_OUT_MILL;
			} else {
				timeout = ((System.currentTimeMillis() / 1000) - Long.parseLong(valueArray[0]));
			}
			if (timeout < REALTIME_TIME_OUT_SECOND) {
				return valueArray[1];
			} else {
				return 2000 + "";
			}
		} else {
			return 0 + "";
		}
	}

	private String getLightRedisValue(String code, Jedis redis) {
		String key = "light:deviceId:" + code + ":value";
		String value = redis.lindex(key, 0);

		if (value != null) {
			value = value.replace(" ", "");
			String[] valueArray = value.split(REALTIME_SPLITTER_LEVEL1);
			/**
			 * 判断实时redis中的第一条数据时间和当前时间相差是否大于60s
			 */
			long timeout = 0L;
			if (valueArray[0].length() > 11) {
				timeout = (System.currentTimeMillis() - Long.parseLong(valueArray[0])) / REALTIME_TIME_OUT_MILL;
			} else {
				timeout = ((System.currentTimeMillis() / 1000) - Long.parseLong(valueArray[0]));
			}
			if (timeout < REALTIME_TIME_OUT_SECOND) {
				return valueArray[1];
			} else {
				return 2000 + "";
			}
		} else {
			return 0 + "";
		}
	}

}
