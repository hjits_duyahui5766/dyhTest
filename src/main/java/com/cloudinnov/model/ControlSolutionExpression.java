package com.cloudinnov.model;

import java.util.List;

public class ControlSolutionExpression {

	private String solutionCode;
	private List<ControlSolutionExpre> points;
	
//	private String categoryCode;// 设备分类
//	private String categoryName;
//	private String pileNo;
	/*
	 * public class Points{ private String code; private String name; private String
	 * pointCode; private String condition;
	 * 
	 * public String getCode() { return code; } public void setCode(String code) {
	 * this.code = code; } public String getName() { return name; } public void
	 * setName(String name) { this.name = name; } public String getPointCode() {
	 * return pointCode; } public void setPointCode(String pointCode) {
	 * this.pointCode = pointCode; } public String getCondition() { return
	 * condition; } public void setCondition(String condition) { this.condition =
	 * condition; }
	 * 
	 * }
	 */

//	public String getCategoryCode() {
//		return categoryCode;
//	}
//
//	public void setCategoryCode(String categoryCode) {
//		this.categoryCode = categoryCode;
//	}
//
//	public String getCategoryName() {
//		return categoryName;
//	}
//
//	public void setCategoryName(String categoryName) {
//		this.categoryName = categoryName;
//	}
//
//	public String getPileNo() {
//		return pileNo;
//	}
//
//	public void setPileNo(String pileNo) {
//		this.pileNo = pileNo;
//	}
	
	public String getSolutionCode() {
		return solutionCode;
	}

	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}

	public List<ControlSolutionExpre> getPoints() {
		return points;
	}

	public void setPoints(List<ControlSolutionExpre> points) {
		this.points = points;
	}

}
