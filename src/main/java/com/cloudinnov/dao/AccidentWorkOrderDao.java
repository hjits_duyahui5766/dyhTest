package com.cloudinnov.dao;

import com.cloudinnov.model.AccidentWorkOrder;

public interface AccidentWorkOrderDao extends IBaseDao<AccidentWorkOrder> {

}