package com.cloudinnov.model;

import java.math.BigDecimal;

public class FaultStat extends BaseModel{

    private String customerCode;

    private String productionLineCode;

    private String equipmentCode;
    
    private String alarmWorkCode;

    private String faultCode;

    private String failureTime;

    private String recoveryTime;

    private BigDecimal duration;

    private String userCode;

    private String day;
    
    private String serieName;
    
    private String faultCount;
    
    private String year;
    
    private String month;
       
    private String week;
    
    private AlarmWorkOrders alarm;
    
    private ProductionLines proline;
    
    private Equipments equipment;
    
    private Companies company;
    
    
    
	public Companies getCompany() {
		return company;
	}

	public void setCompany(Companies company) {
		this.company = company;
	}

	public AlarmWorkOrders getAlarm() {
		return alarm;
	}

	public void setAlarm(AlarmWorkOrders alarm) {
		this.alarm = alarm;
	}

	public ProductionLines getProline() {
		return proline;
	}

	public void setProline(ProductionLines proline) {
		this.proline = proline;
	}

	public Equipments getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getSerieName() {
		return serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getFaultCount() {
		return faultCount;
	}

	public void setFaultCount(String faultCount) {
		this.faultCount = faultCount;
	}

	public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getProductionLineCode() {
        return productionLineCode;
    }

    public void setProductionLineCode(String productionLineCode) {
        this.productionLineCode = productionLineCode;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

   

    public String getFailureTime() {
		return failureTime;
	}

	public void setFailureTime(String failureTime) {
		this.failureTime = failureTime;
	}

	public String getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(String recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

	public String getAlarmWorkCode() {
		return alarmWorkCode;
	}

	public void setAlarmWorkCode(String alarmWorkCode) {
		this.alarmWorkCode = alarmWorkCode;
	}
    
    
}