package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AlarmWorkOrders;
import com.cloudinnov.model.AlarmWorkUser;
import com.cloudinnov.model.Companies;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.WorkOrderLogs;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年2月17日上午11:47:09
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface AlarmWorkOrdersLogic extends IBaseLogic<AlarmWorkOrders> {

	/**
	 * 工单数量修改
	 * 
	 * @Description: 工单修改
	 * @param @param alarmWorkOrders 参数
	 * @return void 返回类型
	 */
	public int updateCount(AlarmWorkOrders alarmWorkOrders);

	/**
	 * selectListHistory
	 * 
	 * @Description: 查询历史工单列表
	 * @param @param equipmentCode
	 * @param @return 参数
	 * @return List<AlarmWorkOrders> 返回类型
	 */
	public List<AlarmWorkOrders> selectListHistory(AlarmWorkOrders alarmWorkOrdersWhere);

	/**
	 * selectListLike
	 * 
	 * @Description: 查看相似工单列表
	 * @param @param faultCode
	 * @param @return 参数
	 * @return List<AlarmWorkOrders> 返回类型
	 */
	public List<AlarmWorkOrders> selectListLike(AlarmWorkOrders alarmWorkOrdersWhere);

	/**
	 * selecAllOrderStatus
	 * 
	 * @Description: 所有工单状态状态汇总
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型
	 */
	
	public Map<String, Object> selecAllOrderStatus(AlarmWorkOrders alarmWorkOrdersWhere);

	/** 查询我的工单 
	* selecAlarmsByUserCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param alarmWorkOrdersWhere
	* @param @return    参数
	* @return Map<String,Object>    返回类型 
	*/
	public Map<String, Object> selecAlarmsByUserCode(AlarmWorkOrders alarmWorkOrdersWhere);

	/**
	 * selectWorkOrderLogsList
	 * 
	 * @Description: 查询工单日志
	 * @param @return 参数
	 * @return List<Map<String,Object>> 返回类型
	 */
	public List<WorkOrderLogs> selectWorkOrderLogsList(PageModel page, WorkOrderLogs work);

	/**
	 * saveWorkOrderLogs
	 * 
	 * @Description: 处理工单或者转派工单都调用此方法向工单日志表插入一条记录
	 * @param @param workOrderLogs
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int saveWorkOrderLogs(WorkOrderLogs workOrderLogs);

	/**
	 * selectWorkOrdersTotalByCustomerId
	 * 
	 * @Description: 根据客户id查询该客户下的所有工单总数
	 * @param @param customerId
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int selectWorkOrdersTotalByCustomerCode(String comCode);

	public List<AlarmWorkOrders> selectListWhere(AlarmWorkOrders alarmWorkOrders);

	public Map<String, Object> search(PageModel page,Map<String, Object> params);
	
	/** 查询代办工单
	 * @Title: selectAgencyWorkOrderList 
	 * @Description: TODO
	 * @param page
	 * @param params
	 * @return
	 * @return: Map<String,Object>
	 */
	public Map<String, Object> selectAgencyWorkOrderList(PageModel page,Map<String, Object> params);

	public int selectUndoneWorkOrdersTotalByOemCode(Companies company);

	public List<AlarmWorkOrders> selectFaultInfoByChannelAndFaultCode(
			String channel, String faultCode);
	
	/** 查看我的工单列表
	* selectAlarmWorksByUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param alarmWorkOrders
	* @param @return    参数
	* @return List<AlarmWorkOrders>    返回类型 
	*/
	public List<AlarmWorkOrders> selectAlarmWorksByUser(AlarmWorkOrders alarmWorkOrders);
	
	
	/** 查看我的工单列表  分页
	* selectAlarmWorksByUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param alarmWorkOrders
	* @param @return    参数
	* @return List<AlarmWorkOrders>    返回类型 
	*/
	public Page<AlarmWorkOrders> selectAlarmWorksByUser(AlarmWorkOrders alarmWorkOrder,int index,int size);
	
	
	public int save(List<AlarmWorkOrders> alarmWorkOrders) ;
	
	/** 获取一段时间设备故障占比
	 * @Title: selectEquipmentFaultProportion 
	 * @Description: 获取一段时间设备故障占比
	 * @param alarmWorkOrder
	 * @param startTime
	 * @param endTime
	 * @return 返回一段时间内设备故障数量
	 * @return: List<AlarmWorkOrders>
	 */
	public List<AlarmWorkOrders> selectEquipmentFaultProportion(AlarmWorkOrders alarmWorkOrder,long startTime, long endTime);
	
	public Map<String, Object> searchAlarmAndHelp(PageModel page, String country, String province, String city, String key,AlarmWorkOrders alarm);
	
	/** 接单向工单责任表插入记录，有则更新，无则插入
	 * @Title: receiveAlarmWorkOrder 
	 * @Description: TODO
	 * @param alarmWorkUser
	 * @return
	 * @return: int
	 */
	public int receiveAlarmWorkOrder(AlarmWorkUser alarmWorkUser);
	
	public int sendMesssageToDialogueWebSocket(String content);

	public List<AlarmWorkOrders> selectUntreatedWorkOrder(String categoryCode, Integer orderStatus);
	
	
	
}
