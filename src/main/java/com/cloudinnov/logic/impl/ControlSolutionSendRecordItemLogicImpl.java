package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.ControlSolutionSendRecordItemDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.logic.ControlSolutionSendRecordItemLogic;
import com.cloudinnov.model.BoardSolutionConfig;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年6月20日上午10:55:35
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service("controlSolutionSendRecordItemLogic")
public class ControlSolutionSendRecordItemLogicImpl extends BaseLogicImpl<ControlSolutionSendRecordItem>
        implements ControlSolutionSendRecordItemLogic {
    private static final String STAY_TIME = "stayTime";
    private static final String SPEED = "speed";
    private static final String DISPLAY_TYPE = "displayType";
    private static final String FONT_COLOR = "color";
    private static final String FONT_SIZE = "fontSize";
    private static final String FONT_NAME = "fontName";
    private static final String FONT_COUNT = "fontCount";
    private static final String TOP = "top";
    private static final String LEFT = "left";
    private static final String ITEM = "item";
    private static final String ITEM_UUID = "uuid";
    private static final String PROGRAM_NAME = "programName";
    private static final String SOLUTION_CODE = "solutionCode";
    private static final String BOARD_TEMPLATE_CODE = "boardTemplateCode";
    private static final String ROW = "row";
    private static final String CONTENT = "content";
    private static final String SPACING = "spacing";
    @Autowired
    private ControlSolutionSendRecordItemDao controlSolutionSendRecordItemDao;
    @Autowired
    private EquipmentsDao equipmentsDao;

    @Override
    public Page<ControlSolutionSendRecordItem> search(PageModel page, Map<String, Object> map) {
        // 首先根据路段编码和设备编码查询所有设备
        String sectionCode = (String) map.get("sectionCode");
        String cateCode = (String) map.get("cateCode");
        Equipments equModel = new Equipments();
        equModel.setCategoryCode(cateCode);
        equModel.setSectionCode(sectionCode);
        // 查询路段和分类下的所有设备
        List<Equipments> equLists = equipmentsDao.selectEquCodesByCateCodeAndSectionCode(equModel);
        // 创建数组存放所有设备编码
        String[] equCodes = new String[equLists.size()];
        // 存放所有的设备编码
        for (int index = 0; index < equLists.size(); index++) {
            equCodes[index] = equLists.get(index).getCode();
        }
        // 加入查询参数equipmentCodes数组
        map.put("equipmentCodes", equCodes);
        long startTime = System.currentTimeMillis();
        // 分页大小
        PageHelper.startPage(page.getIndex(), page.getSize());
        // 查询条件时间内的情报板发送记录
        Page<ControlSolutionSendRecordItem> result = (Page<ControlSolutionSendRecordItem>) controlSolutionSendRecordItemDao
                .search(map);
        System.out.println("查询发送日志耗时:" + (System.currentTimeMillis() - startTime));
        // 遍历返回的发送结果查询设备的一些属性,如设备名称 路段 分类名称 桩号等
        Equipments equResult = null;
        for (ControlSolutionSendRecordItem recordItem : result.getResult()) {
            equResult = equipmentsDao.selectSingleEquipmentByEquCode(recordItem.getEquipmentCode());
            if (equResult != null) {
                recordItem.setCateCode(equResult.getCategoryCode());
                recordItem.setCateName(equResult.getCategoryName());
                recordItem.setEquipmentCode(equResult.getCode());
                recordItem.setEquipmentName(equResult.getName());
                recordItem.setPileNo(equResult.getPileNo());
                recordItem.setSolutionName(equResult.getSectionName());
                recordItem.setSectionName(equResult.getSectionName());
            }
        }
        System.out.println("查询设备耗时:" + (System.currentTimeMillis() - startTime));
        return result;
    }
    @Override
    public ControlSolutionSendRecordItem selectListByEquCodeAndSendTime(String equipmentCode, String sendTime) {
        ControlSolutionSendRecordItem para = new ControlSolutionSendRecordItem();
        para.setEquipmentCode(equipmentCode);
        para.setSendTime(sendTime);
        List<ControlSolutionSendRecordItem> sendLogs = controlSolutionSendRecordItemDao
                .selectListByEquCodeAndSendTime(para);
        if (JudgeNullUtil.iList(sendLogs)) {
            List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
            int itemNo = 0;
            for (ControlSolutionSendRecordItem log : sendLogs) {
                String programName = "";
                BoardSolutionConfig boardSolutionConfig = JSON.parseObject(log.getConfigContent(),
                        BoardSolutionConfig.class);
                String itemContent = boardSolutionConfig.getSendContent();
                Map<String, Object> map = new HashMap<String, Object>();
                List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                map.put(SOLUTION_CODE, boardSolutionConfig.getSolutionCode());
                map.put(BOARD_TEMPLATE_CODE, boardSolutionConfig.getBoardTemplateCode());
                map.put(ITEM, "item" + itemNo);
                map.put(ITEM_UUID, log.getCode() + "-" + CodeUtil.generateRandom(8));
                map.put(STAY_TIME, (boardSolutionConfig.getStayTime() != null ? boardSolutionConfig.getStayTime() : 0));// 停留时间
                if (boardSolutionConfig.getDisplayType() != null && boardSolutionConfig.getDisplayType() != 1) {
                    map.put(SPEED, (boardSolutionConfig.getSpeed() != null ? boardSolutionConfig.getSpeed() : 0) / 50);// 出字速度
                }
                map.put(DISPLAY_TYPE, boardSolutionConfig.getDisplayType());
                List<BoardSolutionConfig.Data> items = JSON.parseArray(itemContent, BoardSolutionConfig.Data.class);
                for (BoardSolutionConfig.Data item : items) {
                    Map<String, Object> rowMap = new HashMap<String, Object>();
                    programName += item.getContent();// 追加节目内容为节目名称
                    rowMap.put(ROW, item.getRow());
                    rowMap.put(FONT_NAME, item.getFontName());
                    rowMap.put(FONT_SIZE, item.getFontSize());
                    rowMap.put(FONT_COLOR, item.getColor());
                    rowMap.put(CONTENT, item.getContent());
                    rowMap.put(SPACING, item.getSpacing());
                    rowMap.put(FONT_COUNT, item.getContent() != null ? item.getContent().length() : 0);
                    rowMap.put(LEFT, item.getLeft());
                    rowMap.put(TOP, item.getTop());
                    rowData.add(rowMap);
                }
                map.put(PROGRAM_NAME,
                        boardSolutionConfig.getName() != null ? boardSolutionConfig.getName() : programName);
                map.put("data", rowData);
                data.add(map);
                itemNo++;
            }
            sendLogs.get(0).setConfigContent(null);
            sendLogs.get(0).setProgramList(data);
            return sendLogs.get(0);
        }
        return null;
    }
}
