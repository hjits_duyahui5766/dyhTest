package com.cloudinnov.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductionLines extends BaseModel {
	private String customerCode;

	private String name;

	private String comment;

	private String equCodes;

	@JsonIgnore
	private List<String> equipmentCodes;

	private Integer isCrucial;

	private List<Equipments> equipments;

	private Companies company;

	private List<EquipmentPoints> points;

	
	private String time;
	//总产能
	private double value;
	
	//总产能单位
	private String valueUnit = "T";

	private String image;
	
	private List<Attach> images;
	
	private String startTime;
	
	//总运行时长
	private String runTime;
	//总运行时长单位
	private String runTimeUnit = "小时";
	//单日产量
	private String dayProduction;
	//单日产量单位
	private String dayProductionUnit = "T";
	
	//运行状态
	private Integer currentState = -1;
	
	@JsonIgnore
	private String productivityCode;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	

	public List<String> getEquipmentCodes() {
		return equipmentCodes;
	}

	public void setEquipmentCodes(List<String> equipmentCodes) {
		this.equipmentCodes = equipmentCodes;
	}

	public Integer getIsCrucial() {
		return isCrucial;
	}

	public void setIsCrucial(Integer isCrucial) {
		this.isCrucial = isCrucial;
	}

	public List<EquipmentPoints> getPoints() {
		return points;
	}

	public void setPoints(List<EquipmentPoints> points) {
		this.points = points;
	}

	public Companies getCompany() {
		return company;
	}

	public void setCompany(Companies company) {
		this.company = company;
	}

	public List<Equipments> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<Equipments> equipments) {
		this.equipments = equipments;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getEquCodes() {
		return equCodes;
	}

	public void setEquCodes(String equCodes) {
		this.equCodes = equCodes;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public List<Attach> getImages() {
		return images;
	}

	public void setImages(List<Attach> images) {
		this.images = images;
	}


	public String getRunTime() {
		return runTime;
	}

	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}

	public String getDayProduction() {
		return dayProduction;
	}

	public void setDayProduction(String dayProduction) {
		this.dayProduction = dayProduction;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Integer getCurrentState() {
		return currentState;
	}

	public void setCurrentState(Integer currentState) {
		this.currentState = currentState;
	}

	public String getProductivityCode() {
		return productivityCode;
	}

	public void setProductivityCode(String productivityCode) {
		this.productivityCode = productivityCode;
	}

	public String getValueUnit() {
		return valueUnit;
	}

	public void setValueUnit(String valueUnit) {
		this.valueUnit = valueUnit;
	}

	public String getRunTimeUnit() {
		return runTimeUnit;
	}

	public void setRunTimeUnit(String runTimeUnit) {
		this.runTimeUnit = runTimeUnit;
	}

	public String getDayProductionUnit() {
		return dayProductionUnit;
	}

	public void setDayProductionUnit(String dayProductionUnit) {
		this.dayProductionUnit = dayProductionUnit;
	}
	
	
}