package com.cloudinnov.logic; 

import java.util.List;
import java.util.Map;
import com.github.pagehelper.Page;


/**
 * @author chengning
 * @date 2016年3月4日下午6:37:55
 * @email ningcheng@cloudinnov.com
 * @remark IBaseService 定义Service通用操作方法
 * @version 
 */
public interface IBaseLogic<T> {
	
	/** 
	* save 
	* @Description: 通用保存方法
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int save(T entity);
	/** 
	* delete 
	* @Description: 删除通用方法
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int delete(T entity);
	/** 
	* update 
	* @Description: 修改通用方法
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int update(T entity);
	/** 
	* selectList 
	* @Description: 查询单个表返回List 
	* @param @param index
	* @param @param size
	* @param @param entity
	* @param @return    参数
	* @return List<T>    返回类型 
	*/
	public List<T> selectList(T entity,boolean isOrderBy);
	
	
	/** 
	* selectListPage 
	* @Description: 查询单个表返回list 带分页
	* @param @param entity
	* @param @return    参数
	* @return List<T>    返回类型 
	*/
	public Page<T> selectListPage(int index, int size,T entity,boolean isOrderBy);

	/** 
	* selectListMap 
	* @Description: 查询多个表返回List<Map> 
	* @param @param condition
	* @param @return    参数
	* @return List<Map<String,Object>>    返回类型 
	*/
	public List<Map<String, Object>> selectListMap(Map<String, Object> condition);
	
	/** 
	* selectListMap 
	* @Description: 查询多个表返回List<Map>  带分页
	* @param @param index
	* @param @param size
	* @param @param condition
	* @param @return    参数
	* @return List<Map<String,Object>>    返回类型 
	*/
	public Page<T> selectListMapPage(int index, int size,Map<String, Object> condition);
	/** 
	* selectMap 
	* @Description: 查询多个表返回一条记录 
	* @param @param condition
	* @param @return    参数
	* @return Map<String,Object>    返回类型 
	*/
	public Map<String, Object> selectMap(Map<String, Object> condition);

		
	/** 
	* select 
	* @Description: 查询单个实体 单个表
	* @param @param entity
	* @param @return    参数
	* @return T    返回类型 
	*/
	public T select(T entity);
		
}
