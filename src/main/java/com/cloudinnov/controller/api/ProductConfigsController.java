package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.ProductConfigsLogic;
import com.cloudinnov.model.ProductConfigs;
import com.github.pagehelper.Page;

/**
 * @author nilixin
 * @date 2016年2月23日下午6:10:25
 * @email
 * @remark 产物配置 controller
 * @version
 */
@Controller
@RequestMapping("/webapi/system/productConfigs")
public class ProductConfigsController extends BaseController {

	@Autowired
	private ProductConfigsLogic productConfigsLogic;

	/**
	 * save
	 * 
	 * @Description: 保存产物配置
	 * @param @param
	 *            productConfig
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "产物配置", methods = "产物配置信息保存")
	public void save(ProductConfigs productConfig, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		productConfig.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = productConfigsLogic.save(productConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "产物配置", methods = "添加其他语言")
	public void saveOtherLanguage(ProductConfigs productConfig, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 获取oemCode
		productConfig.setLanguage(productConfig.getOtherLanguage());
		int result = productConfigsLogic.saveOtherLanguage(productConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "产物配置", methods = "更新其他语言")
	public void updateOtherLanguage(ProductConfigs productConfig, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 获取oemCode
		productConfig.setLanguage(productConfig.getOtherLanguage());
		int result = productConfigsLogic.updateOtherLanguage(productConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据product的code 删除设备product
	 * @param @param
	 *            productConfigCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "产物配置", methods = "产物配置信息删除")
	public void delete(ProductConfigs productConfig, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productConfigsLogic.delete(productConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 更新产品配置
	 * @param @param
	 *            productConfig
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "产物配置", methods = "产物配置信息修改")
	public void update(ProductConfigs productConfig, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productConfigsLogic.update(productConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 分页查找所有产品配置
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, ProductConfigs productConfig, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		productConfig.setOemCode(getUserInfo(request).get(OEM_CODE));
		Page<ProductConfigs> productConfigs = productConfigsLogic.selectListPage(index, size, productConfig, true);
		if (productConfig != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("list", productConfigs);
			map.put("total", productConfigs.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
			response.getWriter().println(returnData);
		}
	}

	@RequestMapping(value = "/listNoPage")
	@ResponseBody
	public void listNoPage(ProductConfigs productConfig, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		productConfig.setOemCode(getUserInfo(request).get(OEM_CODE));
		List<ProductConfigs> productConfigs = productConfigsLogic.selectList(productConfig, true);
		if (productConfig != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("list", productConfigs);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
			response.getWriter().println(returnData);
		}
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找产品配置
	 * @param @param
	 *            productConfigCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(ProductConfigs productConfigCode, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (productConfigCode.getOtherLanguage() != null && productConfigCode.getOtherLanguage() != "") {
			productConfigCode.setLanguage(productConfigCode.getOtherLanguage());
		}
		ProductConfigs result = productConfigsLogic.select(productConfigCode);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}
}
