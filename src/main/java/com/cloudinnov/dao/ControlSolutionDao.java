package com.cloudinnov.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionExpre;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.model.PredictionContacter;
import com.github.pagehelper.Page;

public interface ControlSolutionDao extends IBaseDao<ControlSolution> {
	public int deleteByPrimaryKey(Integer id);

	// 删除的方法
	public int deleteExpreByCondition(ControlSolutionExpre solutionExpre);

	public int insert(ControlSolution record);

	public int insertSelective(ControlSolution record);

	ControlSolution selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(ControlSolution record);

	public int updateByPrimaryKey(ControlSolution record);

	/**
	 * 根据触发器编码查询群控方案列表
	 * 
	 * @Title: selectListByTriggerCode
	 * @Description: TODO
	 * @param triggerCode
	 * @return
	 * @return: List<ControlSolution>
	 */
	public List<ControlSolution> selectListByTriggerCode(String triggerCode);

	public Page<ControlSolution> selectInformationBoard(ControlSolution controlSolution);

	// 关键字查询群控方案么?
	public List<ControlSolution> selectControlSolutionByEquiCode(ControlSolution controlSulotion);

	public ControlSolution selectControlSolutionByConCode(@Param("code") String code);

	public int updateControOnOff(@Param("code") String code, @Param("onOff") Integer onOff);

	/**
	 * 根据触发器编码查询趋空方案列表
	 * 
	 * @param code
	 * @return
	 */
	public List<ControlSolution> selectControllerSolution(String code);

	public List<ControlSolution> selectControlSolutions(String tiggerCode);

	/**
	 * 根据方案编码查询群控方案
	 * 
	 * @param code
	 * @return
	 */
	public List<ControlSolution> sendControllerSolution(String code);

	public List<ControlSolution> sendControllerSolutionRecover(@Param("code") String code,
			@Param("isRecover") int isRecover);

	public List<ControlSolution> selectControlSolutionByFaultCode(String faultCode);

	/**
	 * 群控定时扫描
	 */
	// public List<ControlSolutionExpression>
	// selectScheduleExpression(@Param("isRecover")Integer isRecover);
	public List<ControlSolutionExpre> selectScheduleExpression(@Param("isRecover") Integer isRecover);

	/**
	 * 表达式的增删改查
	 */
	// public ControlSolutionExpression selectSolutionExpression(ControlSolution
	// controlSolution);
	public ControlSolutionExpre selectSolutionExpression(ControlSolution controlSolution);

	public ControlSolutionExpre selectSolutionExpressionByRecover(ControlSolutionExpre solutionExpre);

	public ControlSolutionExpre selectSolutionExprePointNameByRecover(ControlSolutionExpre solutionExpre);

	public int insertExpre(ControlSolutionExpre solutionExpre);

	public int insertSchedule(ControlSolution record);

	public int deleteExpressionBySolutionCode(String solutionCode);

	// 该方法未确定
	public int updateExpre(ControlSolutionExpre solutionExpre);

//	public ControlSolutionExpression selectControlSolutionExpreByRecover(@Param("solutionCode") String solutionCode, @Param("isRecover") Integer isRecover);
	public ControlSolutionExpre selectControlSolutionExpreByRecover(@Param("solutionCode") String solutionCode,
			@Param("isRecover") Integer isRecover);

	// 判断火灾异常等级后,要获取的数据
//	public FireCRTEvent selectFireCRTEventByCode(@Param("code") String code, @Param("isRecover") Integer isRecover);
	public FireCRTEvent selectFireCRTEventByCode(@Param("code") String code, @Param("pointCode") String pointCode,
			@Param("isRecover") Integer isRecover);

	

}
