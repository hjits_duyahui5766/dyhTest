package com.cloudinnov.logic;

import com.cloudinnov.model.SysCode;

/**
 * @author guochao
 * @date 2016年2月26日下午12:10:46
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public interface SysCodeLogic extends IBaseLogic<SysCode> {
	/**
	 * selectEquipmentCurrentValue
	 * 
	 * @Description: 获取设备可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquipmentCurrentValue(String oemCode);

	/**
	 * selectEquipmentBomCurrentValue
	 * 
	 * @Description: 获取设备bom可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquipmentBomCurrentValue(String oemCode);

	/**
	 * selectEquipmentPointCurrentValue
	 * 
	 * @Description: 获取设备点位可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquipmentPointCurrentValue(String oemCode, String type);

	/**
	 * selectEquipmentFaultChannelCurrentValue
	 * 
	 * @Description: 获取设备鼓掌通道可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquipmentFaultChannelCurrentValue(String oemCode);

	/**
	 * selectCompanyCurrentValue
	 * 
	 * @Description: 获取客户可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectCompanyCurrentValue(String oemCode);

	/**
	 * selectFaultCurrentValue
	 * 
	 * @Description: 获取故障可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquipmentFaultCurrentValue(String oemCode);

	/**
	 * selectFaultCurrentValue
	 * 
	 * @Description: 获取故障可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectWorkOrderCurrentValue(String oemCode);

	/**
	 * selectSysCodeCurrentValue
	 * 
	 * @Description: 获取系统日志code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectSysLogCodeCurrentValue(String oemCode);

	/**
	 * selectEquCategoryCurrentValue
	 * 
	 * @Description: 获取设备分类code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectEquCategoryCurrentValue(String oemCode);

	/**
	 * selectFaultTranslateLibraryCurrentValue
	 * 
	 * @Description: 获取故障翻译库当前可用的code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectFaultTranslateLibraryCurrentValue(String oemCode);

	/**
	 * selectFaultCurrentValue
	 * 
	 * @Description: 获取故障码可用code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectFaultCurrentValue(String oemCode);

	/**
	 * selectProductionLinesCurrentValue
	 * 
	 * @Description: 获取生产线
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectProductionLinesCurrentValue(String oemCode);

	/**
	 * selectMonitorCurrentValue
	 * 
	 * @Description:获取监控视图code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectMonitorCurrentValue(String oemCode);

	/**
	 * selectUserCurrentValue
	 * 
	 * @Description:获取用户code
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectUserCurrentValue(String oemCode);
	
	/** 
	* selectResCurrentValue 
	* @Description: 获取资源code
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectResCurrentValue(String oemCode);
	

	/** 角色code生成
	* selectRoleCurrentValue 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectRoleCurrentValue(String oemCode);
	
	/** 原料code生成
	* selectMaterialCurrentValue 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectMaterialCurrentValue(String oemCode);
	
	/** 产物code生成
	* selectProductCurrentValue 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectProductCurrentValue(String oemCode);
	
	/** 工单code生成
	* selectAlarmCurrentValue 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectAlarmCurrentValue(String oemCode);
	
	/** 求助工单code生成
	* selectHelpAlarmCurrentValue 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param oemCode
	* @param @return    参数
	* @return String    返回类型 
	*/
	String selectHelpAlarmCurrentValue(String oemCode);

}
