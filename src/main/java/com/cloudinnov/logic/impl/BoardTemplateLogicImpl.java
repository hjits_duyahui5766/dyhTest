package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.BoardTemplateDao;
import com.cloudinnov.logic.BoardTemplateLogic;
import com.cloudinnov.model.BoardTemplate;
import com.cloudinnov.model.BoardTemplateChilren;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

@Service
public class BoardTemplateLogicImpl extends BaseLogicImpl<BoardTemplate> implements BoardTemplateLogic {
	private static int digit = 5;
	@Autowired
	private BoardTemplateDao boardTemplateDao;

	@Override
	public int save(BoardTemplate entity) {
		if (CommonUtils.isNotEmpty(entity.getData())) {
			entity.setCode(CodeUtil.boardTemplateCode(digit));
			List<BoardTemplateChilren> chilrens = JSON.parseArray(entity.getData(), BoardTemplateChilren.class);
			entity.setRows(chilrens.size());
			entity.setRowsConfig(entity.getData());
			return boardTemplateDao.insert(entity);
		}
		return CommonUtils.ERROR_NUM;
	}
	@Override
	public int update(BoardTemplate entity) {
		if (CommonUtils.isNotEmpty(entity.getData())) {
			List<BoardTemplateChilren> chilrens = JSON.parseArray(entity.getData(), BoardTemplateChilren.class);
			entity.setRows(chilrens.size());
			entity.setRowsConfig(entity.getData());
			return boardTemplateDao.updateByCondition(entity);
		}
		return CommonUtils.ERROR_NUM;
	}
	@Override
	public List<BoardTemplate> selectList(BoardTemplate entity, boolean isOrderBy) {
		List<BoardTemplate> list = boardTemplateDao.selectListByCondition(entity);
		for (BoardTemplate model : list) {
			model.setList(JSON.parseArray(model.getRowsConfig(), BoardTemplate.Data.class));
		}
		return list;
	}
	@Override
	public BoardTemplate select(BoardTemplate entity) {
		BoardTemplate model = boardTemplateDao.selectEntityByCondition(entity);
		model.setList(JSON.parseArray(model.getRowsConfig(), BoardTemplate.Data.class));
		return model;
	}
}
