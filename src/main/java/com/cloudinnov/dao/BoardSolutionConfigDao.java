package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.BoardSolutionConfig;

public interface BoardSolutionConfigDao extends IBaseDao<BoardSolutionConfig> {
	/**
	 * 根据群控方案获取群控配置列表
	 * @Title: selectListBySolutionCode
	 * @Description: TODO
	 * @param record
	 * @return
	 * @return: List<ControlSolutionConfig>
	 */
	public List<BoardSolutionConfig> selectListBySolutionCode(BoardSolutionConfig record);
	/**
	 * 根据设备分类查询情报板方案(用于手动方案)
	 * @Title: selectBoardListByEquCate
	 * @Description: TODO
	 * @param record
	 * @return
	 * @return: List<BoardSolutionConfig>
	 */
	List<BoardSolutionConfig> selectBoardListByEquCate(BoardSolutionConfig record);
	/**
	 * 跟据群控方案查询设备编码
	 * @param model
	 * @return
	 */
	public List<BoardSolutionConfig> selectBoardBySolutionCode(BoardSolutionConfig model);
}
