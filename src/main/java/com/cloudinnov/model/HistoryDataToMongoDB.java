package com.cloudinnov.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年3月30日上午10:35:58
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public class HistoryDataToMongoDB implements Serializable {
	private static final long serialVersionUID = 1L;
	private String pointCode;
	private String pointName;
	private List<Data> data;
	private Date time;
	private String equipmentCode;
	private String sectionCode;

	public String getPointCode() {
		return pointCode;
	}
	public void setPointCode(String pointCode) {
		this.pointCode = pointCode;
	}
	public String getPointName() {
		return pointName;
	}
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	public List<Data> getData() {
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public class Data {
		private Long time;
		private Object value;

		public Long getTime() {
			return time;
		}
		public void setTime(Long time) {
			this.time = time;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return "Data [time=" + time + ", value=" + value + "]";
		}
	}

	@Override
	public String toString() {
		return "HistoryDataToMongoDB [pointCode=" + pointCode + ", pointName=" + pointName + ", data=" + data
				+ ", time=" + time + ", equipmentCode=" + equipmentCode + ", sectionCode=" + sectionCode + "]";
	}
}
