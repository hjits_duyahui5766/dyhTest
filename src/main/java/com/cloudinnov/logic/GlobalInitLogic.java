package com.cloudinnov.logic;

import java.io.IOException;

/** 
*
 * @author chengning
 * @date 2017年9月12日上午11:11:58
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface GlobalInitLogic {
    

    /** 
    * 紧急电话Server启动
    * @return void    返回类型 
    */
    void initEmergecyPhoneRevice();
    /** 
    * 打开火灾告警系统数据接收服务     参数
    * @return void    返回类型 
    */
    void initFireCRERevice();
    /** 
    * 车检仪Server启动
    * @return void    返回类型 
    */
    void initCarTcpRevice();
    /**
     * 初始化所有大屏列表
     * @Title: initTcpRevice
     * @Description: TODO
     * @return: void
     * @throws IOException
     */
    void initAllScreen();
    
    /**
     * 初始化内邓火灾报警
     */
    void initNDFireService();
    
    void initLightTcpService();
    
    /** 
    * 初始化数据库
    * @return void    返回类型 
    */
    void initDataBase();
    
    /**
     * 启动全局方案扫描
     */
    void initPlanScanThread();
    
    

}
