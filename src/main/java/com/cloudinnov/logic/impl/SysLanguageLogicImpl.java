package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.SysLanguageLogic;
import com.cloudinnov.model.SysLanguage;

@Service
public class SysLanguageLogicImpl extends BaseLogicImpl<SysLanguage> implements
		SysLanguageLogic {
}
