package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthUsers;
/**
 * @author guochao
 * @date 2016年2月18日上午9:25:41
 * @email chaoguo@cloudinnov.com
 * @remark 资源service接口
 * @version
 */
public interface AuthResourcesLogic extends IBaseLogic<AuthResource> {

	public int saveOtherLanguage(AuthResource authResource);
	
	public int updateOtherLanguage(AuthResource authResource);
	
	/** 获取用户资源，考虑多角色资源需要取并集
	 * @Title: selectResourceByUser 
	 * @Description: TODO
	 * @param au
	 * @return
	 * @return: List<AuthResource>
	 */
	public List<AuthResource> selectResourceByUser(AuthUsers au);

	public List<AuthResource> selectTreeList(String roleCode, String lanugage);

	public List<AuthResource> tableList(String[] codes, String language);
	
	public List<AuthResource> selectSencondResourceByUser(Map<String,Object> map);
	
	/** 获取常用菜单
	 * @Title: selectCommonResource 
	 * @Description: TODO
	 * @param au
	 * @return
	 * @return: List<AuthResource>
	 */
	public List<AuthResource> selectCommonsResource(AuthUsers au);
	
	/** 获取一级菜单
	 * @Title: selectCommonResource 
	 * @Description: TODO
	 * @param au
	 * @return
	 * @return: List<AuthResource>
	 */
	public List<AuthResource> selectParentsResource(AuthUsers au);
	
	/** 获取子菜单
	 * @Title: selectCommonResource 
	 * @Description: TODO
	 * @param au
	 * @return
	 * @return: List<AuthResource>
	 */
	public List<AuthResource> selectChildrensResource(AuthUsers au, int parentId);

}
