package com.cloudinnov.dao;

import com.cloudinnov.model.SysCode;

/**
 * @author guochao
 * @date 2016年2月26日下午12:09:20
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public interface SysCodeDao extends IBaseDao<SysCode>{
	/**
	 * selectCurrentValue
	 * 
	 * @Description: 获取当前可用类型的code值 
	 * @param @param cate
	 * @param @param oemCode
	 * @param @return 参数
	 * @return String 返回类型
	 */
	String selectCurrentValue(String cate, String oemCode);
}