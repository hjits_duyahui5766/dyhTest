package com.cloudinnov.task;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.logic.impl.CarDetectorLogicImpl;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.ReportCar;
import com.cloudinnov.utils.CommonUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import redis.clients.jedis.JedisPool;
import scala.collection.parallel.ParIterableLike.Foreach;

/**
 * 定时每天获取mongoDB的车辆数据存到mysql中
 * 
 * @author duyah
 *
 */
public class CollectCarDayDataQuartzJob {

	private static final Logger LOG = LoggerFactory.getLogger(CollectCarDayDataQuartzJob.class);
	private static SimpleDateFormat sdfSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final static int YEAR_TYPE = 1; // 标识符,作为标识获取何种类型指标数据
	private final static int MONTH_TYPE = 2;
	private final static int DAY_TYPE = 3;
	private final static int HOUR_TYPE = 4;

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EquipmentsDao equipmentsDao;
	@Autowired
	EquipmentsAttrDao equipmentsAttrDao;

	private void collecCarDayData() {
		try {
			LOG.debug("车辆月数据统计job开始执行!");
			System.err.println("车辆月数据统计job开始执行!");
			collectCarDayDataFromMongo();
		} catch (Exception e) {
			LOG.error("车辆月数据统计时,出现的报错: " + e.toString());
			e.printStackTrace();
		}
	}

	private void collectCarDayDataFromMongo() throws Exception {
		// 创建查询的时间条件
		String beforefirstMonthdate = getBeforeFirstMonthdate();
		String beforeLastMonthdate = getBeforeLastMonthdate();
		Date startTime = sdfSecond.parse(beforefirstMonthdate);
		Date endTime = sdfSecond.parse(beforeLastMonthdate);

		String equipCode = null;
		// 先从mysql中查询有多少个sectionCode值
//		List<Equipments> equipments = equipmentsDao.selectListSectionCode();
//		for (Equipments equipment : equipments) {
			// 再从mysql中根据sectionCode值查询下面微波车辆检测器的设备的code值
			List<Equipments> EquipmentGroupBySectionCode = equipmentsDao.selectEquipmentGroupBySectionCode();
			for (Equipments equipments2 : EquipmentGroupBySectionCode) {
				equipCode = equipments2.getCode();
 				List<Map<String, Object>> carMonthDataFromDB = selectTotalDataByTime(DAY_TYPE, equipCode, startTime,
						endTime);
				// 创建往mysql保存的实体类
				ReportCar reportCar = new ReportCar();

				String equipmentCode = equipCode;
				int carTotal = 0;
				String utcTime = null;
				int firstLaneOneKind = 0;
				int firstLaneTwoKind = 0;
				int firstLaneThreeKind = 0;
				int firstLaneFourKind = 0;
				int firstLaneAverageSpeed = 0;
				int firstLaneOccuPancy = 0;
				int firstLaneFlow = 0;

				int secondLaneOneKind = 0;
				int secondLaneTwoKind = 0;
				int secondLaneThreeKind = 0;
				int secondLaneFourKind = 0;
				int secondLaneAverageSpeed = 0;
				int secondLaneOccuPancy = 0;
				int secondLaneFlow = 0;

				int thirdLaneOneKind = 0;
				int thirdLaneTwoKind = 0;
				int thirdLaneThreeKind = 0;
				int thirdLaneFourKind = 0;
				int thirdLaneAverageSpeed = 0;
				int thirdLaneOccuPancy = 0;
				int thirdLaneFlow = 0;

				int forthLaneOneKind = 0;
				int forthLaneTwoKind = 0;
				int forthLaneThreeKind = 0;
				int forthLaneFourKind = 0;
				int forthLaneAverageSpeed = 0;
				int forthLaneOccuPancy = 0;
				int forthLaneFlow = 0;

				int fifthLaneOneKind = 0;
				int fifthLaneTwoKind = 0;
				int fifthLaneThreeKind = 0;
				int fifthLaneFourKind = 0;
				int fifthLaneAverageSpeed = 0;
				int fifthLaneOccuPancy = 0;
				int fifthLaneFlow = 0;

				int sixthLaneOneKind = 0;
				int sixthLaneTwoKind = 0;
				int sixthLaneThreeKind = 0;
				int sixthLaneFourKind = 0;
				int sixthLaneAverageSpeed = 0;
				int sixthLaneOccuPancy = 0;
				int sixthLaneFlow = 0;

				// 遍历mongoDB结果
				for (Map<String, Object> map : carMonthDataFromDB) {
					if (map.get("code").toString() == null) {
						equipmentCode = map.get("code").toString();
					}
					
					carTotal = Integer.parseInt(map.get("value") + "");
					utcTime = map.get("time") + "";

					firstLaneOneKind = Integer.parseInt(map.get("firstLaneOneKind") + "");
					firstLaneTwoKind = Integer.parseInt(map.get("firstLaneTwoKind") + "");
					firstLaneThreeKind = Integer.parseInt(map.get("firstLaneThreeKind") + "");
					firstLaneFourKind = Integer.parseInt(map.get("firstLaneFourKind") + "");
					firstLaneAverageSpeed = Integer.parseInt(map.get("firstLaneAverageSpeed") + "");
					firstLaneOccuPancy = Integer.parseInt(map.get("firstLaneOccuPancy") + "");
					firstLaneFlow = Integer.parseInt(map.get("firstLaneFlow") + "");

					secondLaneOneKind = Integer.parseInt(map.get("secondLaneOneKind") + "");
					secondLaneTwoKind = Integer.parseInt(map.get("secondLaneTwoKind") + "");
					secondLaneThreeKind = Integer.parseInt(map.get("secondLaneThreeKind") + "");
					secondLaneFourKind = Integer.parseInt(map.get("secondLaneFourKind") + "");
					secondLaneAverageSpeed = Integer.parseInt(map.get("secondLaneAverageSpeed") + "");
					secondLaneOccuPancy = Integer.parseInt(map.get("secondLaneOccuPancy") + "");
					secondLaneFlow = Integer.parseInt(map.get("secondLaneFlow") + "");

					thirdLaneOneKind = Integer.parseInt(map.get("thirdLaneOneKind") + "");
					thirdLaneTwoKind = Integer.parseInt(map.get("thirdLaneTwoKind") + "");
					thirdLaneThreeKind = Integer.parseInt(map.get("thirdLaneThreeKind") + "");
					thirdLaneFourKind = Integer.parseInt(map.get("thirdLaneFourKind") + "");
					thirdLaneAverageSpeed = Integer.parseInt(map.get("thirdLaneAverageSpeed") + "");
					thirdLaneOccuPancy = Integer.parseInt(map.get("thirdLaneOccuPancy") + "");
					thirdLaneFlow = Integer.parseInt(map.get("thirdLaneFlow") + "");

					forthLaneOneKind = Integer.parseInt(map.get("forthLaneOneKind") + "");
					forthLaneTwoKind = Integer.parseInt(map.get("forthLaneTwoKind") + "");
					forthLaneThreeKind = Integer.parseInt(map.get("forthLaneThreeKind") + "");
					forthLaneFourKind = Integer.parseInt(map.get("forthLaneFourKind") + "");
					forthLaneAverageSpeed = Integer.parseInt(map.get("forthLaneAverageSpeed") + "");
					forthLaneOccuPancy = Integer.parseInt(map.get("forthLaneOccuPancy") + "");
					forthLaneFlow = Integer.parseInt(map.get("forthLaneFlow") + "");

					fifthLaneOneKind = Integer.parseInt(map.get("fifthLaneOneKind") + "");
					fifthLaneTwoKind = Integer.parseInt(map.get("fifthLaneTwoKind") + "");
					fifthLaneThreeKind = Integer.parseInt(map.get("fifthLaneThreeKind") + "");
					fifthLaneFourKind = Integer.parseInt(map.get("fifthLaneFourKind") + "");
					fifthLaneAverageSpeed = Integer.parseInt(map.get("fifthLaneAverageSpeed") + "");
					fifthLaneOccuPancy = Integer.parseInt(map.get("fifthLaneOccuPancy") + "");
					fifthLaneFlow = Integer.parseInt(map.get("fifthLaneFlow") + "");

					sixthLaneOneKind = Integer.parseInt(map.get("sixthLaneOneKind") + "");
					sixthLaneTwoKind = Integer.parseInt(map.get("sixthLaneTwoKind") + "");
					sixthLaneThreeKind = Integer.parseInt(map.get("sixthLaneThreeKind") + "");
					sixthLaneFourKind = Integer.parseInt(map.get("sixthLaneFourKind") + "");
					sixthLaneAverageSpeed = Integer.parseInt(map.get("sixthLaneAverageSpeed") + "");
					sixthLaneOccuPancy = Integer.parseInt(map.get("sixthLaneOccuPancy") + "");
					sixthLaneFlow = Integer.parseInt(map.get("sixthLaneFlow") + "");

					reportCar.setEquipmentCode(equipmentCode);
					reportCar.setCarTotal(carTotal);
					reportCar.setUtcTime(utcTime);

					reportCar.setFirstLaneOneKind(firstLaneOneKind);
					reportCar.setFirstLaneTwoKind(firstLaneTwoKind);
					reportCar.setFirstLaneThreeKind(firstLaneThreeKind);
					reportCar.setFirstLaneFourKind(firstLaneFourKind);
					reportCar.setFirstLaneAverageSpeed(firstLaneAverageSpeed);
					reportCar.setFirstLaneOccuPancy(firstLaneOccuPancy);
					reportCar.setFirstLaneFlow(firstLaneFlow);

					reportCar.setSecondLaneOneKind(secondLaneOneKind);
					reportCar.setSecondLaneTwoKind(secondLaneTwoKind);
					reportCar.setSecondLaneThreeKind(secondLaneThreeKind);
					reportCar.setSecondLaneFourKind(secondLaneFourKind);
					reportCar.setSecondLaneAverageSpeed(secondLaneAverageSpeed);
					reportCar.setSecondLaneOccuPancy(secondLaneOccuPancy);
					reportCar.setSecondLaneFlow(secondLaneFlow);

					reportCar.setThirdLaneOneKind(thirdLaneOneKind);
					reportCar.setThirdLaneTwoKind(thirdLaneTwoKind);
					reportCar.setThirdLaneThreeKind(thirdLaneThreeKind);
					reportCar.setThirdLaneFourKind(thirdLaneFourKind);
					reportCar.setThirdLaneAverageSpeed(thirdLaneAverageSpeed);
					reportCar.setThirdLaneOccuPancy(thirdLaneOccuPancy);
					reportCar.setThirdLaneFlow(thirdLaneFlow);

					reportCar.setForthLaneOneKind(forthLaneOneKind);
					reportCar.setForthLaneTwoKind(forthLaneTwoKind);
					reportCar.setForthLaneThreeKind(forthLaneThreeKind);
					reportCar.setForthLaneFourKind(forthLaneFourKind);
					reportCar.setForthLaneAverageSpeed(forthLaneAverageSpeed);
					reportCar.setForthLaneOccuPancy(forthLaneOccuPancy);
					reportCar.setForthLaneFlow(forthLaneFlow);

					reportCar.setFifthLaneOneKind(fifthLaneOneKind);
					reportCar.setFifthLaneTwoKind(fifthLaneTwoKind);
					reportCar.setForthLaneThreeKind(fifthLaneThreeKind);
					reportCar.setFifthLaneFourKind(fifthLaneFourKind);
					reportCar.setFifthLaneAverageSpeed(fifthLaneAverageSpeed);
					reportCar.setFifthLaneOccuPancy(fifthLaneOccuPancy);
					reportCar.setFifthLaneFlow(fifthLaneFlow);

					reportCar.setSixthLaneOneKind(sixthLaneOneKind);
					reportCar.setSixthLaneTwoKind(sixthLaneTwoKind);
					reportCar.setSixthLaneThreeKind(sixthLaneThreeKind);
					reportCar.setSixthLaneFourKind(sixthLaneFourKind);
					reportCar.setSixthLaneAverageSpeed(sixthLaneAverageSpeed);
					reportCar.setSixthLaneOccuPancy(sixthLaneOccuPancy);
					reportCar.setSixthLaneFlow(sixthLaneFlow);

					// 把reportCar保存到mysql中
					int result = 0;
					if (reportCar != null) {
						result = equipmentsAttrDao.saveCarDayData(reportCar);
					}
					if (result == 1) {
						LOG.debug("保存车辆日数据成功!");
					} else {
						LOG.error("保存车辆日数据失败!");
					}
				}
			}
//		}
	}

	@SuppressWarnings("deprecation")
	private List<Map<String, Object>> selectTotalDataByTime(int type, String equCode, Date startTime, Date endTime) {
     	String groupStr = "";
		if (CommonUtils.isNotEmpty(equCode)) {
			if (type == 1) {// 年
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+ "}}";
			} else if (type == 2) {// 月
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			} else if (type == 3) {// 日
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" },month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }  }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+ "}}";
			} else if (type == 4) {// 小时
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			}
		} else {
			if (type == 1) {// 年
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			} else if (type == 2) {// 月
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+"}}";
			} else if (type == 3) {// 日
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" },month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }  }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+ "}}";
			} else if (type == 4) {// 小时
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+"}}";
			}
		}
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(startTime != null ? startTime : new Date());
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(endTime != null ? endTime : new Date());
		if (startTime == null) {
			if (type == 1 || type == 2) {// 月
				oneDayStart.add(Calendar.YEAR, 0);
				oneDayEnd.add(Calendar.YEAR, 1);
				oneDayStart.set(Calendar.MONTH, 0);
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.set(Calendar.MONTH, 0);
			} else if (type == 3) {// 日
				oneDayStart.add(Calendar.MONTH, 0);
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.MONTH, 1);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
			} else if (type == 4) {// 小时
				oneDayStart.add(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.DAY_OF_MONTH, 1);
			}
			oneDayStart.set(Calendar.HOUR_OF_DAY, 0);
			oneDayStart.set(Calendar.MINUTE, 0);
			oneDayStart.set(Calendar.SECOND, 0);
			oneDayStart.set(Calendar.MILLISECOND, 0);
			oneDayEnd.set(Calendar.HOUR_OF_DAY, 0);
			oneDayEnd.set(Calendar.MINUTE, 0);
			oneDayEnd.set(Calendar.SECOND, 0);
			oneDayEnd.set(Calendar.MILLISECOND, 0);
		}
		BasicDBObject[] array = null;
		if (CommonUtils.isNotEmpty(equCode)) {
			EquipmentsAttr model = new EquipmentsAttr();
			model.setEquipmentCode(equCode);
			model.setCustomTag(CarDetectorData.DEVICE_ID);
			model = equipmentsAttrDao.selectEntityByCondition(model);
			if (model == null) {
				return Collections.emptyList();
			}
			String deviceId = model.getValue();
			array = new BasicDBObject[] {
					new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
					new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
					new BasicDBObject("deviceId", deviceId) };
		} else {
			array = new BasicDBObject[] {
					new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
					new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())) };
		}
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		DBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("total", "$total");
		projectFields.put("year", "$_id.year");
		projectFields.put("firstLaneOneKind", "$firstLaneOneKind");
		projectFields.put("firstLaneTwoKind", "$firstLaneTwoKind");
		projectFields.put("firstLaneThreeKind", "$firstLaneThreeKind");
		projectFields.put("firstLaneFourKind", "$firstLaneFourKind");
		projectFields.put("firstLaneAverageSpeed", "$firstLaneAverageSpeed");
		projectFields.put("firstLaneOccuPancy", "$firstLaneOccuPancy");
		projectFields.put("firstLaneFlow", "$firstLaneFlow");
		
		projectFields.put("secondLaneOneKind", "$secondLaneOneKind");
		projectFields.put("secondLaneTwoKind", "$secondLaneTwoKind");
		projectFields.put("secondLaneThreeKind", "$secondLaneThreeKind");
		projectFields.put("secondLaneFourKind", "$secondLaneFourKind");
		projectFields.put("secondLaneAverageSpeed", "$secondLaneAverageSpeed");
		projectFields.put("secondLaneOccuPancy", "$secondLaneOccuPancy");
		projectFields.put("secondLaneFlow", "$secondLaneFlow");
		
		projectFields.put("thirdLaneOneKind", "$thirdLaneOneKind");
		projectFields.put("thirdLaneTwoKind", "$thirdLaneTwoKind");
		projectFields.put("thirdLaneThreeKind", "$thirdLaneThreeKind");
		projectFields.put("thirdLaneFourKind", "$thirdLaneFourKind");
		projectFields.put("thirdLaneAverageSpeed", "$thirdLaneAverageSpeed");
		projectFields.put("thirdLaneOccuPancy", "$thirdLaneOccuPancy");
		projectFields.put("thirdLaneFlow", "$thirdLaneFlow");
		
		projectFields.put("forthLaneOneKind", "$forthLaneOneKind");
		projectFields.put("forthLaneTwoKind", "$forthLaneTwoKind");
		projectFields.put("forthLaneThreeKind", "$forthLaneThreeKind");
		projectFields.put("forthLaneFourKind", "$forthLaneFourKind");
		projectFields.put("forthLaneAverageSpeed", "$forthLaneAverageSpeed");
		projectFields.put("forthLaneOccuPancy", "$forthLaneOccuPancy");
		projectFields.put("forthLaneFlow", "$forthLaneFlow");
		
		projectFields.put("fifthLaneOneKind", "$fifthLaneOneKind");
		projectFields.put("fifthLaneTwoKind", "$fifthLaneTwoKind");
		projectFields.put("fifthLaneThreeKind", "$fifthLaneThreeKind");
		projectFields.put("fifthLaneFourKind", "$fifthLaneFourKind");
		projectFields.put("fifthLaneAverageSpeed", "$fifthLaneAverageSpeed");
		projectFields.put("fifthLaneOccuPancy", "$fifthLaneOccuPancy");
		projectFields.put("fifthLaneFlow", "$fifthLaneFlow");
		
		projectFields.put("sixthLaneOneKind", "$sixthLaneOneKind");
		projectFields.put("sixthLaneTwoKind", "$sixthLaneTwoKind");
		projectFields.put("sixthLaneThreeKind", "$sixthLaneThreeKind");
		projectFields.put("sixthLaneFourKind", "$sixthLaneFourKind");
		projectFields.put("sixthLaneAverageSpeed", "$sixthLaneAverageSpeed");
		projectFields.put("sixthLaneOccuPancy", "$sixthLaneOccuPancy");
		projectFields.put("sixthLaneFlow", "$sixthLaneFlow");
		if (type == 2) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type == 3) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type == 4) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipmentCode", "$_id.equipmentCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		/* 查看Group结果 */
		AggregationOutput output = mongoTemplate.getCollection("carDetectorData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> map = null;
		int firstLaneOneKind = 0;
		int firstLaneTwoKind = 0;
		int firstLaneThreeKind = 0;
		int firstLaneFourKind = 0;
		int firstLaneAverageSpeed = 0;
		int firstLaneOccuPancy = 0;
		int firstLaneFlow = 0;
		
		int secondLaneOneKind = 0;
		int secondLaneTwoKind = 0;
		int secondLaneThreeKind = 0;
		int secondLaneFourKind = 0;
		int secondLaneAverageSpeed = 0;
		int secondLaneOccuPancy = 0;
		int secondLaneFlow = 0;
		
		int thirdLaneOneKind = 0;
		int thirdLaneTwoKind = 0;
		int thirdLaneThreeKind = 0;
		int thirdLaneFourKind = 0;
		int thirdLaneAverageSpeed = 0;
		int thirdLaneOccuPancy = 0;
		int thirdLaneFlow = 0;
		
		int forthLaneOneKind = 0;
		int forthLaneTwoKind = 0;
		int forthLaneThreeKind = 0;
		int forthLaneFourKind = 0;
		int forthLaneAverageSpeed = 0;
		int forthLaneOccuPancy = 0;
		int forthLaneFlow = 0;
		
		int fifthLaneOneKind = 0;
		int fifthLaneTwoKind = 0;
		int fifthLaneThreeKind = 0;
		int fifthLaneFourKind = 0;
		int fifthLaneAverageSpeed = 0;
		int fifthLaneOccuPancy = 0;
		int fifthLaneFlow = 0;
		
		int sixthLaneOneKind = 0;
		int sixthLaneTwoKind = 0;
		int sixthLaneThreeKind = 0;
		int sixthLaneFourKind = 0;
		int sixthLaneAverageSpeed = 0;
		int sixthLaneOccuPancy = 0;
		int sixthLaneFlow = 0;
		
		for (DBObject dbObject : output.results()) {
			map = new HashMap<>();
			if (type == 1) {
				map.put("time", dbObject.get("year").toString());
			} else if (type == 2) {// 月
				map.put("time", dbObject.get("year").toString() + "-" + dbObject.get("month"));
			} else if (type == 3) {// 日
				String month = dbObject.get("month") != null ? String.valueOf(dbObject.get("month")) : "";
				if (month.length() == 1) {
					month = "0" + month;
				}
				String day = dbObject.get("day") != null ? String.valueOf(dbObject.get("day")) : "";
				if (day.length() == 1) {
					day = "0" + day;
				}

				map.put("time", dbObject.get("year").toString() + "-" + month + "-" + day);
			} else if (type == 4) {// 小时
				String month = dbObject.get("month") != null ? String.valueOf(dbObject.get("month")) : "";
				if (month.length() == 1) {
					month = "0" + month;
				}
				String day = dbObject.get("day") != null ? String.valueOf(dbObject.get("day")) : "";
				if (day.length() == 1) {
					day = "0" + day;
				}
				int hour = Integer.parseInt(dbObject.get("hour").toString());
				// if (hour / 10 == 0) {
				hour += 8;
				// }
				String hourStr = hour + "";
				if (hourStr.length() == 1) {
					hourStr = "0" + hourStr;
				}
				map.put("time", dbObject.get("year").toString() + "-" + month + "-" + day + " " + hourStr);
			}
			String temp = "";
			temp = dbObject.get("firstLaneOneKind").toString();
			firstLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneTwoKind").toString();
			firstLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneThreeKind").toString();
			firstLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneFourKind").toString();
			firstLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("firstLaneAverageSpeed")!=null) {
				temp = dbObject.get("firstLaneAverageSpeed").toString();
				firstLaneAverageSpeed = (int)Double.parseDouble(temp);	
			}else {
				firstLaneOneKind = 0;
			}
			if(dbObject.get("firstLaneOccuPancy")!=null) {
				temp = dbObject.get("firstLaneOccuPancy").toString();
				firstLaneOccuPancy = (int)Double.parseDouble(temp);	
			}else {
				firstLaneOccuPancy = 0;
			}
			temp = dbObject.get("firstLaneFlow").toString();
			firstLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("secondLaneOneKind").toString();
			secondLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneTwoKind").toString();
			secondLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneThreeKind").toString();
			secondLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneFourKind").toString();
			secondLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("secondLaneAverageSpeed")!=null) {
				temp = dbObject.get("secondLaneAverageSpeed").toString();
				secondLaneAverageSpeed = (int)Double.parseDouble(temp);	
			}else {
				secondLaneAverageSpeed = 0;
			}
			if(dbObject.get("secondLaneOccuPancy")!=null) {
				temp = dbObject.get("secondLaneOccuPancy").toString();
				secondLaneOccuPancy = (int)Double.parseDouble(temp);	
			}else {
				secondLaneOccuPancy = 0;
			}
			temp = dbObject.get("secondLaneFlow").toString();
			secondLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("thirdLaneOneKind").toString();
			thirdLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneTwoKind").toString();
			thirdLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneThreeKind").toString();
			thirdLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneFourKind").toString();
			thirdLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("thirdLaneAverageSpeed")!=null) {
				temp = dbObject.get("thirdLaneAverageSpeed").toString();
				thirdLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				thirdLaneAverageSpeed = 0;
			}
			if(dbObject.get("thirdLaneOccuPancy")!=null) {
				temp = dbObject.get("thirdLaneOccuPancy").toString();
				thirdLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				thirdLaneOccuPancy = 0;
			}
			temp = dbObject.get("thirdLaneFlow").toString();
			thirdLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("forthLaneOneKind").toString();
			forthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneTwoKind").toString();
			forthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneThreeKind").toString();
			forthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneFourKind").toString();
			forthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("forthLaneAverageSpeed")!=null) {
				temp = dbObject.get("forthLaneAverageSpeed").toString();
				forthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				forthLaneAverageSpeed = 0;
			}
			if(dbObject.get("forthLaneOccuPancy")!=null) {
				temp = dbObject.get("forthLaneOccuPancy").toString();
				forthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				forthLaneOccuPancy = 0;
			}
			temp = dbObject.get("forthLaneFlow").toString();
			forthLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("fifthLaneOneKind").toString();
			fifthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneTwoKind").toString();
			fifthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneThreeKind").toString();
			fifthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneFourKind").toString();
			fifthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("fifthLaneAverageSpeed")!=null) {
				temp = dbObject.get("fifthLaneAverageSpeed").toString();
				fifthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				fifthLaneAverageSpeed = 0;
			}
			if(dbObject.get("fifthLaneOccuPancy")!=null) {
				temp = dbObject.get("fifthLaneOccuPancy").toString();
				fifthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				fifthLaneOccuPancy = 0;
			}
			temp = dbObject.get("fifthLaneFlow").toString();
			fifthLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("sixthLaneOneKind").toString();
			sixthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneTwoKind").toString();
			sixthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneThreeKind").toString();
			sixthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneFourKind").toString();
			sixthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("sixthLaneAverageSpeed")!=null) {
				temp = dbObject.get("sixthLaneAverageSpeed").toString();
				sixthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				sixthLaneAverageSpeed = 0;
			}
			if(dbObject.get("sixthLaneOccuPancy")!=null) {
				temp = dbObject.get("sixthLaneOccuPancy").toString();
				sixthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				sixthLaneOccuPancy = 0;
			}
			temp = dbObject.get("sixthLaneFlow").toString();
			sixthLaneFlow = Integer.parseInt(temp);
			
			map.put("firstLaneOneKind", firstLaneOneKind);
			map.put("firstLaneTwoKind", firstLaneTwoKind);
			map.put("firstLaneThreeKind", firstLaneThreeKind);
			map.put("firstLaneFourKind", firstLaneFourKind);
			map.put("firstLaneAverageSpeed", firstLaneAverageSpeed);
			map.put("firstLaneOccuPancy", firstLaneOccuPancy);
			map.put("firstLaneFlow", firstLaneFlow);

			map.put("secondLaneOneKind", secondLaneOneKind);
			map.put("secondLaneTwoKind", secondLaneTwoKind);
			map.put("secondLaneThreeKind", secondLaneThreeKind);
			map.put("secondLaneFourKind", secondLaneFourKind);
			map.put("secondLaneAverageSpeed", secondLaneAverageSpeed);
			map.put("secondLaneOccuPancy", secondLaneOccuPancy);
			map.put("secondLaneFlow", secondLaneFlow);

			map.put("thirdLaneOneKind", thirdLaneOneKind);
			map.put("thirdLaneTwoKind", thirdLaneTwoKind);
			map.put("thirdLaneThreeKind", thirdLaneThreeKind);
			map.put("thirdLaneFourKind", thirdLaneFourKind);
			map.put("thirdLaneAverageSpeed", thirdLaneAverageSpeed);
			map.put("thirdLaneOccuPancy", thirdLaneOccuPancy);
			map.put("thirdLaneFlow", thirdLaneFlow);
			
			map.put("forthLaneOneKind", forthLaneOneKind);
			map.put("forthLaneTwoKind", forthLaneTwoKind);
			map.put("forthLaneThreeKind", forthLaneThreeKind);
			map.put("forthLaneFourKind", forthLaneFourKind);
			map.put("forthLaneAverageSpeed", forthLaneAverageSpeed);
			map.put("forthLaneOccuPancy", forthLaneOccuPancy);
			map.put("forthLaneFlow", forthLaneFlow);
			
			map.put("fifthLaneOneKind", fifthLaneOneKind);
			map.put("fifthLaneTwoKind", fifthLaneTwoKind);
			map.put("fifthLaneThreeKind", fifthLaneThreeKind);
			map.put("fifthLaneFourKind", fifthLaneFourKind);
			map.put("fifthLaneAverageSpeed", fifthLaneAverageSpeed);
			map.put("fifthLaneOccuPancy", fifthLaneOccuPancy);
			map.put("fifthLaneFlow", fifthLaneFlow);
			
			map.put("sixthLaneOneKind", sixthLaneOneKind);
			map.put("sixthLaneTwoKind", sixthLaneTwoKind);
			map.put("sixthLaneThreeKind", sixthLaneThreeKind);
			map.put("sixthLaneFourKind", sixthLaneFourKind);
			map.put("sixthLaneAverageSpeed", sixthLaneAverageSpeed);
			map.put("sixthLaneOccuPancy", sixthLaneOccuPancy);
			map.put("sixthLaneFlow", sixthLaneFlow);	
			
			map.put("value", dbObject.get("total"));
			String valueString = dbObject.get("total").toString();
			Integer allCount=Integer.parseInt(valueString);
			int chengdu=allCount*5/9;
			int chongqing=allCount-chengdu;
			map.put("chengdu", chengdu);
			map.put("chongqing", chongqing);
			if (CommonUtils.isNotEmpty(equCode)) {
				if (dbObject.get("equipmentCode") != null) {
					map.put("code", dbObject.get("equipmentCode"));
				}else {
					// car的数据在mongoDB中没有存equCode,直接把参数再传回去
					map.put("code", equCode);
				}
			}
			data.add(map);
		}
		return data;
	}
	
	/**
	 * 获取昨天的00:00:00点时间的方法,需要转换成伦敦时间,差八个小时
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getBeforeFirstMonthdate() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0 );
		String startTime = sdfSecond.format(calendar.getTime());
		return startTime;
	}

	/**
	 * 获取昨天的23:59:59点时间的方法,需要转换成伦敦时间,差八个小时
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getBeforeLastMonthdate() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 7);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		String endTime = sdfSecond.format(calendar.getTime());
		return endTime;
	}
}
