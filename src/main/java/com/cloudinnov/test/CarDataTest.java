package com.cloudinnov.test;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.model.CarDetectorData.CarDetectorDataChilren;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.MultiThreadServer;
import com.cloudinnov.utils.support.spring.SpringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/applicationContext.xml", "classpath*:/kafka.xml",
		"classpath*:/spring-mongodb.xml" })
public class CarDataTest {
	private static final int DATA_LENGTH = 178;
	public static final String TOTAL_CAR = "total:car";
	public static final String CAL_NAME = "car:deviceId:";
	public static final String CAL_VALUE = ":value";
	private static final int RECENT_LIST_COUNT = 10000;
	public static final String SPLITTER_LEVEL0 = ",";
	
	static final Logger LOG = LoggerFactory.getLogger(MultiThreadServer.class);
	private ServerSocket serverSocket;
	private ThreadPoolExecutor executor;
	static CarDetectorLogic carDetectorLogic = SpringUtils.getBean("carDetectorLogic");
	
	static JedisPool jedisPool = SpringUtils.getBean("jedisPool");
 
	@Test
	public  void redis() {
		String requestData="ff0000646748042816010397020115d00001547115340902281117012c0039062604000900080001000000004f04000b00090000000200007503000900090000000000006503000800080000000000003004000800040003000100002302000c000b0001000000009e66";
		System.err.println(requestData.length());
		Jedis redis = null;
		if (CommonUtils.isNotEmpty(requestData) && requestData.length() >= 64 && requestData.length() <= 215) {
			String keyRealtime, totalName, value;
			CarDetectorData model = null;
			 
			model = new CarDetectorData();
			 
			int lineId = 1;
			// 得到设备唯一ID
			model.setDeviceId(requestData.substring(8, 24));
			String deviceId=requestData.substring(8, 24);
			// 获取时间和日期
			model.setSummaryTime(requestData.substring(40, 54));
			// 获取周期
			model.setPeriod(Integer.parseInt(requestData.substring(54, 58), 16));
			// 车辆总数
			model.setCarTotal(Long.parseLong(requestData.substring(58, 62), 16));
			// 车道数
			model.setLaneTotal(Integer.parseInt(requestData.substring(62, 64), 16));
			
			int firstLaneData = Integer.parseInt(requestData.substring(68, 72),16);
			int secondLaneData=Integer.parseInt(requestData.substring(92,96),16);
			int thirdLaneData=Integer.parseInt(requestData.substring(116,120),16);
			int forthLaneData=Integer.parseInt(requestData.substring(140,144),16);
			int fifthLaneData=Integer.parseInt(requestData.substring(164,168),16);
			int sixthLaneData=Integer.parseInt(requestData.substring(188,192),16);
			
			int beyondLaneData= firstLaneData+secondLaneData+thirdLaneData;
			int behindLaneData=forthLaneData+fifthLaneData+sixthLaneData;
			int allData=beyondLaneData+behindLaneData;
			
			model.setUtcTime(new Date());
			model.setTimeMillis(System.currentTimeMillis());
			// 车道数据共12个字节和2字节crc校验包括：
			String lineData = requestData.substring(64, requestData.length() - 4);
			keyRealtime = CAL_NAME + model.getDeviceId() + CAL_VALUE;
			totalName = TOTAL_CAR + CAL_VALUE;
			value = model.getCarTotal() + SPLITTER_LEVEL0 + model.getTimeMillis();
			System.err.println(value);
		 
		 	redis.lpush(keyRealtime, value);// 单个车检仪器只保存最新的3000条数据
			redis.ltrim(keyRealtime, 0L, RECENT_LIST_COUNT);
			redis.set(totalName, value);// 总数只保留最新的一条
		}
	}
}
