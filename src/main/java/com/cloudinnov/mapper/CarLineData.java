package com.cloudinnov.mapper;

public class CarLineData {

	private Integer lineId;
	private Integer lineAvgVelocuty;
	private Integer lineFlow;
	private Integer lineOccRate;
	private Integer lineOneClassflow;
	private Integer lineTwoClassflow;
	private Integer lineThreeClassflow;
	private Integer lineFourClassflow;
	public Integer getLineId() {
		return lineId;
	}
	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}
	public Integer getLineAvgVelocuty() {
		return lineAvgVelocuty;
	}
	public void setLineAvgVelocuty(Integer lineAvgVelocuty) {
		this.lineAvgVelocuty = lineAvgVelocuty;
	}
	public Integer getLineFlow() {
		return lineFlow;
	}
	public void setLineFlow(Integer lineFlow) {
		this.lineFlow = lineFlow;
	}
	public Integer getLineOccRate() {
		return lineOccRate;
	}
	public void setLineOccRate(Integer lineOccRate) {
		this.lineOccRate = lineOccRate;
	}
	public Integer getLineOneClassflow() {
		return lineOneClassflow;
	}
	public void setLineOneClassflow(Integer lineOneClassflow) {
		this.lineOneClassflow = lineOneClassflow;
	}
	public Integer getLineTwoClassflow() {
		return lineTwoClassflow;
	}
	public void setLineTwoClassflow(Integer lineTwoClassflow) {
		this.lineTwoClassflow = lineTwoClassflow;
	}
	public Integer getLineThreeClassflow() {
		return lineThreeClassflow;
	}
	public void setLineThreeClassflow(Integer lineThreeClassflow) {
		this.lineThreeClassflow = lineThreeClassflow;
	}
	public Integer getLineFourClassflow() {
		return lineFourClassflow;
	}
	public void setLineFourClassflow(Integer lineFourClassflow) {
		this.lineFourClassflow = lineFourClassflow;
	}
	
	
 
 
	
}
