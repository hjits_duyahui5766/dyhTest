package com.cloudinnov.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.utils.control.ControlModel;
import com.cloudinnov.utils.control.EquipmentControlLog;

/**
 * @ahtuor libo
 * @email boli@cloudinnov.com
 * @日期 2017年4月7日 下午5:20:46
 */
public class RemoteControllerUtil {
	public static String BOLOMI_RECEIVE_URL = null;
	static {
		BOLOMI_RECEIVE_URL = PropertiesUtils.findPropertiesKey("bolomi.remote.url");
	}

	/**
	 * 控制设备--与bolomi通信
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param remote
	 * @return 参数
	 * @return EquipmentControlLog 返回类型
	 */
	public static EquipmentControlLog controller(List<ControlModel> remote) {
		EquipmentControlLog model = new EquipmentControlLog();
		Map<String, String> params = new HashMap<String, String>();
		params.put("data", JSON.toJSONString(remote));
		String result = HttpUtil.doPost(BOLOMI_RECEIVE_URL, params, null);
		if (CommonUtils.isNotEmpty(result)) {
			model = JSON.parseObject(result, EquipmentControlLog.class);
		}
		return model;
	}
}
