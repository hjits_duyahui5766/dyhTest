package com.cloudinnov.logic;

import com.cloudinnov.model.EquipmentInspectionResult;

public interface EquipmentInspectionResultLogic extends IBaseLogic<EquipmentInspectionResult> {

}
