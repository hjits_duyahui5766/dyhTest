package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.CameraModel;
import com.cloudinnov.model.HikCameraData;
import com.cloudinnov.model.Screen;

public interface CameraControlLogic extends IBaseLogic<Screen> {
	int updateSwitchCamera(Screen screen);
	List<Screen> selectScreen(Screen screen);
	int cameraStatus(CameraModel cameraModel);
	int controllerCamera(CameraModel cameraModel);
	String switchCamera(CameraModel cameraModel);
	void saveAllScreensByTwallCode(String twCode);
	/**
	 * 查询海康数据库,获取所有摄像头
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param model
	 * @param @return 参数
	 * @return List<HikCameraData> 返回类型
	 */
	List<HikCameraData> saveAllHikCamera(HikCameraData model);
}
