package com.cloudinnov.model;

import java.util.List;

public class FireCRTEvent {
	public static final String DEVICE_ID = "alertorId";
	public static final String FAULT_CODE = "faultCode";
	private String event;
	private String position;
	private String device;
	private String deviceId = null;// 支持给设备定义长度16内的位置描述，可以不填
	private Long occurrenceTime;
	private String equipmentCode;
	private String categoryCode;
	private String solutionCode;
	
	public String getSolutionCode() {
		return solutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}
	public static String getFaultCode() {
		return FAULT_CODE;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		if (event.endsWith(":")) {
			this.event = event.substring(0, event.length() - 1);
		} else {
			this.event = event;
		}
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public Long getOccurrenceTime() {
		return occurrenceTime;
	}
	public void setOccurrenceTime(Long occurrenceTime) {
		this.occurrenceTime = occurrenceTime;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	@Override
	public String toString() {
		return "FireCRTEvent [event=" + event + ", position=" + position + ", device=" + device + ", deviceId="
				+ deviceId + ", occurrenceTime=" + occurrenceTime + ", equipmentCode=" + equipmentCode
				+ ", categoryCode=" + categoryCode + "]";
	}
}
