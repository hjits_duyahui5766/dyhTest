package com.cloudinnov.logic;

import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkItem;
import com.github.pagehelper.Page;

public interface EquipmentInspectionWorkItemLogic extends IBaseLogic<EquipmentInspectionWorkItem> {
	public Page<EquipmentInspectionWorkItem> selectByConfigCode(int index, int size, EquipmentInspectionConfig entity);
}
