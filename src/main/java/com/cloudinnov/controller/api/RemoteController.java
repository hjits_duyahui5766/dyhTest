package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.dao.AlarmLogDao;
import com.cloudinnov.logic.BxManagerLogic;
import com.cloudinnov.logic.EquipmentControlLogic;
import com.cloudinnov.model.AlarmLog;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.RemoteControllerUtil;
import com.cloudinnov.utils.control.ControlModel;
import com.cloudinnov.utils.control.EquipmentControlLog;

/**
 * @author libo
 * @Email boli@cloudinnov.com
 * @version 2017年3月1日 下午3:05:38
 */
@Controller
@RequestMapping("/webapi/remote")
public class RemoteController extends BaseController {
    @Autowired
    private EquipmentControlLogic equipmentControlLogic;
    
    @Autowired
    private AlarmLogDao alarmLogDao;
    @Autowired
    private BxManagerLogic bxManagerLogic;

    /**
     * 远程控制修改参数
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "远程管理", methods = "远程修改参数")
    public void sendRemoteData(String command, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int returnCode = equipmentControlLogic.sendContrlCommands(command);
        Map<String, Object> map = new HashMap<String, Object>();
        if (returnCode == SUCCESS) {
            String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
            response.getWriter().print(returnData);
        } else {
            String returnData = returnJsonAllRequest(request, response, map, ERROR, null);
            response.getWriter().print(returnData);
        }
    }
    
    @RequestMapping(value = "/sendFireSolution", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "远程火灾管理", methods = "远程火灾修改参数")
    public void sendFireSolution(String command, String solutionCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int returnCode = equipmentControlLogic.sendContrlCommands(command);
        AlarmLog alarmLog = new AlarmLog();
        alarmLog.setSolutionCode(solutionCode);
        alarmLogDao.insertFireAlarmLog(alarmLog);
        Map<String, Object> map = new HashMap<String, Object>();
        if (returnCode == SUCCESS) {
            String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
            response.getWriter().print(returnData);
        } else {
            String returnData = returnJsonAllRequest(request, response, map, ERROR, null);
            response.getWriter().print(returnData);
        }
    }
    
    /**
     * 动态预案发送控制命令方法
     * @param commands
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/sendPredictionRules", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "预案方案控制命令发送", methods = "预案方案控制的命令参数")
    public void sendPreceptSolution(String commands, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
    	String returnData = "";
    	Map<String, Object> map = new HashMap<>();
    	List<Map<String, Object>> listResult = new ArrayList<>();
    	Map<String, Object> resultMap = new HashMap<>();
    	
    	if (CommonUtils.isNotEmpty(commands)) {
			List<Equipments> equipments = JSON.parseArray(commands, Equipments.class);
			if (JudgeNullUtil.iList(equipments) ) {
				for (Equipments equipment : equipments) {
					//对情报板设备分类的控制命令发送
					if (equipment.getCategoryCode().contains("QBB")) {
						String solutionCode = equipment.getSolutionCode();
						int result = bxManagerLogic.forcedToInforBorad(solutionCode);
						if (result == 1) {
							resultMap.put("code", equipment.getCode());
							resultMap.put("status", 1);
							listResult.add(resultMap);
						}else {
							resultMap.put("code", equipment.getCode());
							resultMap.put("status", 0);
							listResult.add(resultMap);
						}
					} else { //其他设备分类的控制命令发送方式
						List<ControlModel> commonds = equipmentControlLogic.generateControllerCommond(equipment);
						if (JudgeNullUtil.iList(commonds)) {
							EquipmentControlLog equipCtrlLog = RemoteControllerUtil.controller(commonds); //调用往bolomi发命令的方法
							Integer result = equipCtrlLog.getCode();
							if (result != null) {
								resultMap.put("code", equipment.getCode());
								resultMap.put("status", 1);
								listResult.add(resultMap);
							}else {
								resultMap.put("code", equipment.getCode());
								resultMap.put("status", 0);
								listResult.add(resultMap);
							}
						}
					}
				}
			}
		}
    	map.put("sign", listResult);
        if (commands!= null) {
            returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
        } else {
            returnData = returnJsonAllRequest(request, response, map, ERROR, null);
        }
        response.getWriter().print(returnData);
    }
}
