package com.cloudinnov.logic.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AlarmWorkOrdersDao;
import com.cloudinnov.dao.CompaniesDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.dao.ProductionLinesDao;
import com.cloudinnov.logic.CompaniesLogic;
import com.cloudinnov.model.Companies;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLines;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author chengning
 * @date 2016年2月17日下午2:15:54
 * @email ningcheng@cloudinnov.com
 * @remark 客户管理Service实现
 * @version
 */
@Service
public class CompaniesLogicImpl extends BaseLogicImpl<Companies> implements CompaniesLogic {
	private int digit = 5;

	@Autowired
	private CompaniesDao companiesDao;

	@Autowired
	private EquipmentsDao equipmentsDao;

	@Autowired
	private ProductionLinesDao productionLinesDao;

	@Autowired
	private AlarmWorkOrdersDao alarmWorkOrdersDao;

	public int save(Companies company) {
		String companyCode = CodeUtil.customerCode(digit);
		company.setCode(companyCode);
		if (company.getType().equals(CommonUtils.OEM_TYPE)) {
			company.setOemCode(companyCode);
		}
		if (company.getType().equals(CommonUtils.INTEGRATOR_TYPE)) {
			company.setIntegratorCode(companyCode);
		}
		// 判断所属oem是否属于集成商
		String oemCode = company.getOemCode();
		if (oemCode != null) {
			Companies selectEntity = new Companies();
			selectEntity.setCode(oemCode);
			Companies oemCompany = companiesDao.selectEntityByCondition(selectEntity);
			if (oemCompany != null) {
				String integratorCode = oemCompany.getIntegratorCode();
				if (integratorCode != null) {
					company.setIntegratorCode(integratorCode);
				}
			}

		}
		int returnCode = companiesDao.insert(company);
		if (returnCode == 1) {
			returnCode = companiesDao.insertInfo(company);
		}
		return returnCode;
	}

	public int update(Companies company) {
		int returnCode = companiesDao.updateByCondition(company);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = companiesDao.updateInfoByCondition(company);
		}
		return returnCode;
	}

	@Override
	public List<Companies> selectCompanyCodeAndName(Map<String, Object> map) {
		List<Companies> list = companiesDao.selectCompanyCodeAndName(map);
		return list;
	}

	public List<Companies> selectCompanyByOemCode(Map<String, Object> map) {
		List<Companies> list = companiesDao.selectCompanyCodeAndName(map);
		if (list != null && list.size() > 0) {
			Equipments equipments = new Equipments();
			for (Companies com : list) {
				equipments.setCustomerCode(com.getCode());
				equipments.setLanguage((String) map.get("language"));
				equipments.setCurrentState(CommonUtils.EQU_STATE_FAULT);
				equipments.setOemCode((String) map.get("oemcode"));
				List<Equipments> faultEquList = equipmentsDao.selectEquStateListByCustomCode(equipments);
				if (faultEquList!=null && faultEquList.size() >CommonUtils.DEFAULT_NUM) {
					com.setIsError(faultEquList.size());
					com.setErrorEquipments(faultEquList);
				}
			}
		}
		return list;
	}

	@Override
	public Page<Companies> search(int index, int size, String country, String province, String city, String key,
			String oemCode, String integratorCode,String type, String language) {
		PageHelper.startPage(index, size);
		Map<String, String> map = new HashMap<String, String>();
		map.put("integratorCode", integratorCode);
		map.put("oemCode", oemCode);
		map.put("country", country);
		map.put("province", province);
		map.put("city", city);
		map.put("name", key);
		map.put("type", type);
		map.put("language", language);
		return (Page<Companies>) companiesDao.search(map);
	}

	@Override
	public Map<String, Object> selectEquLineAlarmCountByCompanyCode(String comCode) {
		Map<String, Object> countMap = new HashMap<String, Object>();
		/**
		 * 客户下的设备总数
		 */
		int equCount = equipmentsDao.selectEquipmentTotalByCustomerCode(comCode);
		/**
		 * 客户下的产线总数
		 */
		int prolineCount = productionLinesDao.selectProductionLineTotalByCustomerCode(comCode);
		/**
		 * 客户下的工单总数
		 */
		int alarmCount = alarmWorkOrdersDao.selectWorkOrdersTotalByCustomerCode(comCode);
		countMap.put("equCount", equCount);
		countMap.put("prolineCount", prolineCount);
		countMap.put("alarmCount", alarmCount);

		return countMap;
	}

	@Override
	public int selectCompaniesTotalByOemCode(Companies company) {
		return companiesDao.selectCompaniesTotalByOemCode(company);
	}

	public Map<String, Object> selectCustomerEquipmentState(Equipments equipments) {
		List<Equipments> list = equipmentsDao.listByCustomer(equipments);
		Map<String, Object> faultEqu = new HashMap<String, Object>();
		for (Equipments equ : list) {
			if (equ.getCurrentState() == CommonUtils.EQU_STATE_FAULT) {
				faultEqu.put(equ.getCode(), CommonUtils.EQU_STATE_FAULT);
			}
		}
		return faultEqu;

	}

	@Override
	public List<ProductionLines> selectCustomerProLines(ProductionLines productionLines) {
		List<ProductionLines> list = productionLinesDao.selectListByCondition(productionLines);
		return list;
	}

	@Override
	public int saveOtherLanguage(Companies company) {
		return companiesDao.insertInfo(company);
	}

	@Override
	public int updateOtherLanguage(Companies company) {
		return companiesDao.updateInfoByCondition(company);
	}

	@Override
	public List<Companies> selectCompanyByOemCode(Companies company) {
		if(company.getCountry()!=null ){
			String[] countrys = company.getCountry().split(",");
		    company.setCountrys(Arrays.asList(countrys));
			
		}if(company.getProvince()!=null ){
			String[] provinces = company.getProvince().split(",");
		    company.setProvinces(Arrays.asList(provinces));
			
		}if(company.getCity()!=null ){
			String[] citys = company.getCity().split(",");
		    company.setCitys(Arrays.asList(citys));
			
		}
		if(company.getIndustryCode()!=null ){
			String[] industryCodes = company.getIndustryCode().split(",");
		    company.setIndustryCodes(Arrays.asList(industryCodes));
			
		}
		return companiesDao.selectCompanyCodeAndNameByOem(company);
	}

	@Override
	public List<Companies> listByCodes(String codes, String language, String oemCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] codesTem = codes.split(",");
		map.put("codes", codesTem);
		map.put("language", language);
		map.put("oemCode", oemCode);
		return companiesDao.listByCodes(map);
	}

	@Override
	public List<Companies> searchNoPage(String country, String province, String city, String key, String oemCode,
			String type, String language) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("oemCode", oemCode);
		map.put("country", country);
		map.put("province", province);
		map.put("city", city);
		map.put("name", key);
		map.put("type", type);
		map.put("language", language);
		return (List<Companies>) companiesDao.search(map);
	}

}
