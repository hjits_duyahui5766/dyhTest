package com.cloudinnov.model;

import java.util.Date;

public class InspectionWorkOrder extends BaseModel {
	private Integer id;

	private String code;

	private String equipmentCode;

	private String inspectionCode;

	private Date inspectionDate;

	private Integer status;

	private String createTime;

	private String lastUpdateTime;

	private Integer orderStatus;

	private String proLineCode;

	private String paddingBy;// 当前处理人

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getInspectionCode() {
		return inspectionCode;
	}

	public void setInspectionCode(String inspectionCode) {
		this.inspectionCode = inspectionCode;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getProLineCode() {
		return proLineCode;
	}

	public void setProLineCode(String proLineCode) {
		this.proLineCode = proLineCode;
	}

	public String getPaddingBy() {
		return paddingBy;
	}

	public void setPaddingBy(String paddingBy) {
		this.paddingBy = paddingBy;
	}
}