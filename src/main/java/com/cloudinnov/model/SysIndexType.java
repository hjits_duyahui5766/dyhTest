package com.cloudinnov.model;

public class SysIndexType extends BaseModel{

    private String unit;

    private Integer dataType;

    private Integer digits;

    private String chartType;
    
    private Integer monitorType;
    
    private Integer valueType;
    
    private Integer isUserCustomerReport;


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getDigits() {
        return digits;
    }

    public void setDigits(Integer digits) {
        this.digits = digits;
    }


	public String getChartType() {
		return chartType;
	}

	public void setChartType(String chartType) {
		this.chartType = chartType;
	}

	public Integer getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(Integer monitorType) {
		this.monitorType = monitorType;
	}

	public Integer getValueType() {
		return valueType;
	}

	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	public Integer getIsUserCustomerReport() {
		return isUserCustomerReport;
	}

	public void setIsUserCustomerReport(Integer isUserCustomerReport) {
		this.isUserCustomerReport = isUserCustomerReport;
	}
    

}