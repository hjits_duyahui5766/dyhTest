package com.cloudinnov.dao; 

import java.util.List;
import java.util.Map;
/**
 * @author chengning
 * @date 2016年3月4日下午2:16:49
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface IBaseDao<T> {
	/**
	 * 保存数据
	 * @param <T>
	 * @param <T>
	 * 
	 * @Description: 传入实体 保存数据
	 * @param @param 实体类
	 * @return void 返回类型
	 */
	public int insert(T entity);

	/** 
	* selectEntityByCondition 
	* @Description: 根据条件查询实体 返回Model
	* @param @param entity
	* @param @return    参数
	* @return T    返回类型 
	*/
	public T selectEntityByCondition(T entity);
	
	
	/** 
	* selectMapByCondition 
	* @Description: 根据条件查询多个表 返回Map
	* @param @param condition key条件名称 value 值
	* @param @return    参数
	* @return Map<String,Object>    返回类型 
	*/
	public Map<String, Object> selectMapByCondition(Map<String, Object> condition);
	
	/** 
	* selectListByCondition 
	* @Description: 根据条件查询单个表 返回List
	* @param @param entity
	* @param @return    参数
	* @return List<T>    返回类型 
	*/
	public List<T> selectListByCondition(T entity);
	
	/** 
	* selectListMapByCondition 
	* @Description: 根据条件查询多个表 返回List<Map>
	* @param @return    参数
	* @return List<Map<String,Object>>    返回类型 
	*/
	public List<Map<String, Object>> selectListMapByCondition(Map<String, Object> condition); 
	
	/** 
	* deleteByCondition 
	* @Description: 修改status为9 删除状态
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int deleteByCondition(T entity);
	
	/** 
	* updateByCondition 
	* @Description: 根据实体条件更新
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型  
	*/
	public int updateByCondition(T entity);
	 	
	/** 通过名称查找是否存在
	 * @param map <table,表名称>  <name,值>
	 * 			 
	 * @return 1 存在 0不存在
	 */
	public int findByName(Map<String,Object> map);
}
