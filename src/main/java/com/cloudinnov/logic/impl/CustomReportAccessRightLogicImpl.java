package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.CustomReportAccessRightLogic;
import com.cloudinnov.model.CustomReportAccessRight;

@Service
public class CustomReportAccessRightLogicImpl extends BaseLogicImpl<CustomReportAccessRight>
		implements CustomReportAccessRightLogic {

}
