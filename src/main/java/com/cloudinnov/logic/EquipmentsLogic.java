package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.Companies;
import com.cloudinnov.model.EquipmentArea;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月17日上午11:54:01
 * @email chaoguo@cloudinnov.com
 * @remark 设备service 接口
 * @version
 */
public interface EquipmentsLogic extends IBaseLogic<Equipments> {
	/**
	 * selectEquipmentTotalByCustomerId
	 * @Description: 根据客户id查询该客户下的所有设备数量
	 * @param @param customerId
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int selectEquipmentTotalByCustomerCode(String comCode);
	/**
	 * listByCustomer
	 * @Description: 根据客户id 查询该客户下的所有设备列表
	 * @param @param customerId
	 * @param @return 参数
	 * @return List<Equipments> 返回类型
	 */
	public List<Equipments> listByCustomer(Equipments equipment);
	public List<Equipments> listByCustomers(String language, String[] codes);
	/**
	 * search
	 * @Description: 检索设备方法
	 * @param @param index
	 * @param @param size
	 * @param @param country
	 * @param @param province
	 * @param @param city
	 * @param @param brand
	 * @param @param model
	 * @param @param key
	 * @param @param oemCode
	 * @param @return 参数
	 * @return Page<Equipments> 返回类型
	 */
	public Page<Equipments> search(int index, int size, String country, String province, String city,
			String categoryCode, String key, String sectionCode, String language, String type);
	/**
	 * saveOtherLanguage
	 * @Description: 添加设备其他语言信息
	 * @param @param equipment
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int saveOtherLanguage(Equipments equipment);
	/**
	 * 查询设备状态，如有故障返回工单
	 * @param equipments
	 * @return
	 */
	public Map<String, Object> selectEquipmentState(Equipments Equipments);
	/**
	 * 更新设备其他语言 updateOtherLanguage
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param equipment
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int updateOtherLanguage(Equipments equipment);
	/**
	 * 查询我的设备列表 分页 selectEquipmentsByUser
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param equipments
	 * @param @return 参数
	 * @return List<Equipments> 返回类型
	 */
	public Page<Equipments> selectEquipmentsByUser(Equipments equipments, int index, int size);
	/**
	 * 查询我的设备列表 selectEquipmentsByUser
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param equipments
	 * @param @return 参数
	 * @return List<Equipments> 返回类型
	 */
	public List<Equipments> selectEquipmentsByUser(Equipments equipments);
	/**
	 * 查询oem下的设备状态
	 * @Title: selectEquipmentStates
	 * @Description: 查询oem下的设备状态
	 * @param company
	 * @return
	 * @return: int
	 */
	public int selectEquipmentStates(Companies company);
	/**
	 * 查询oem下的设备列表 selectEquipmentListByOemCode
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param company
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int selectEquipmentListByOemCode(Companies company);
	/**
	 * 根据状态获取设备列表
	 * @Title: getEquipmentListByState
	 * @Description: 根据状态获取设备列表
	 * @param company oemcode，设备状态
	 * @return 设备信息，经纬度，设备状态等信息
	 * @return: List<Equipments>
	 */
	public List<Equipments> getEquipmentListByState(Companies company);
	/**
	 * 根据路段code获取设备
	 * @param equipments2
	 * @return 设备List
	 */
	public List<Equipments> selectEquListBySectionCode(Equipments equipments2);
	/**
	 * 复制设备 设备基本属性,设备自定义属性 设备点位,故障通道,设备元件
	 * @Title: copyEquipments
	 * @Description: TODO
	 * @param equipments 设备实体
	 * @param count 复制个数
	 * @return
	 * @return: int
	 */
	public int copyEquipments(Equipments equipments);
	/**
	 * 大屏监控数据获取
	 * @Title: selectMainMonitor
	 * @Description: TODO
	 * @param equipments
	 * @return
	 * @return: Map<String,Object>
	 */
	public List<Equipments> selectMainMonitor(Equipments equipments);
	public List<Equipments> selectEquListByCateCode(Equipments equipments);
	public Page<Equipments> selectEquipmentsInfo(int index, int size, Equipments equipments, boolean b);
	public Page<Equipments> selectInfoQuipments(Equipments equipments, int index, int size);
	public Page<Equipments> selectEqInfoBySectionCode(Equipments equipments, int index, int size);
	public List<Equipments> selectEquimentClassifyInfo(Equipments equipments2);
	public List<Equipments> selectEquimentClassifyInfos(Equipments equipments2);
	public Page<Equipments> selectEquipmentsBySeCode(Equipments equipments, int index, int size);
	public List<Equipments> selectEquiClassifyAndEquipments(Equipments equipments);
	public List<Equipments> selectEquipmentsByName(Equipments equipments);
	public Equipments selectEquipmentsByCode(Equipments equipments);
	public List<Equipments> selectEquiClassifyTreeAndEquipmentsTree(Equipments equipments);
	
	/**
	 * 查询摄像机设备
	 * @param equipments
	 * @return
	 */
	List<Equipments> selectCameraEquipmentsBySeCode();
		
	/**
	 * 查询该设备所在的区域
	 * @param equipmentsAttr
	 * @return
	 */
	Equipments selectEquipArea(EquipmentsAttr equipmentsAttr);

	//通过设备code查询设备
	Equipments selectPositionByCode(EquipmentsAttr equipmentsAttr);
	
	List<Equipments> selectEquipsBySectionAndCategory(Equipments equipments);
	
	List<Equipments> selectCameraAreaEquipments(Equipments equipments);
	
	List<Equipments> selectTriggerEquipmentsBySection(Equipments equipments);
	
	boolean insertEquipmentArea(EquipmentArea equipmentArea);
	
	boolean updateEquipmentArea(EquipmentArea equipmentArea);
	
	boolean deleteEquipmentArea(EquipmentArea equipmentArea);
	
	List<EquipmentArea> selectEquipmentAreaSection();
	
	Page<EquipmentArea> selectEquipmentArea(EquipmentArea equipmentArea, int index, int size);
	
	//查询单个摄像区域方法
	EquipmentArea selectSignleEquipmentArea(EquipmentArea equipmentArea);
	
	//查询该系统下的所有隧道信息
	List<Equipments> selectAllClassifyTunnelsAndAccidentArea();
	
	//查询某个隧道下事故区域下的监控区域及触发设备
	List<EquipmentArea> selectCameraAreaAndTriggerEquipment(Equipments equipments);
	
	//查询隧道下所有设备分类及事故区域
	List<Equipments> selectEquiClassifyAndAreaId(Equipments equipments);
	
	//在已知的车道和事故区域下查询车道指示器设备
	List<Equipments> selectEquipsBySectionAndCategoryAndLaneAndAreaId(Equipments equipments);
	
}
