package com.cloudinnov.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.cloudinnov.logic.EquipmentInspectionConfigLogic;
import com.cloudinnov.logic.EquipmentInspectionWorkOrderLogic;
import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkOrder;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.support.spring.SpringUtils;

/**
 * @author chengning
 * @date 2016年8月4日上午12:27:39
 * @email 
 * @remark （读取巡检配置项，生成巡检工单）
 * @version 
 */
public class InspectionQuartzJob {

	private static final Logger logger = Logger.getLogger(InspectionQuartzJob.class);

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	
	private EquipmentInspectionConfigLogic inspectionLogic = SpringUtils.getBean("equipmentInspectionConfigLogic");
	
	private EquipmentInspectionWorkOrderLogic workOrderLogic = SpringUtils.getBean("equipmentInspectionWorkOrderLogic");

	protected void execute() {
		Calendar cal = Calendar.getInstance();
		String start = String.valueOf(System.currentTimeMillis());
		
		logger.debug("----巡检定时处理服务开始启动----     " + sdf.format(cal.getTime()));
		
		List<EquipmentInspectionConfig> list = inspectionLogic.quartzList();
		
		final int length = list.size();
		int period;
		int advanceDays;
		for (int i = 0 ; i < length; i++) {
			String beginDate = list.get(i).getBeginDate();
			period = list.get(i).getPeriod();
			advanceDays = list.get(i).getAdvanceDays();
			if (timeHit(beginDate, period, advanceDays, start)) {
				logger.debug("---符合生成条件---");
				EquipmentInspectionWorkOrder entity = new EquipmentInspectionWorkOrder();
				entity.setCode(CodeUtil.inspectionWorkOrderCode(5));
				entity.setConfigCode(list.get(i).getCode());
				entity.setTitle("Inspection work order test");
				entity.setComment("work order comment");
				entity.setContent("content");
				entity.setEquipmentCode(list.get(i).getEquipmentCode());
				entity.setCustomerCode(list.get(i).getCustomerCode());
				entity.setPaddingBy(list.get(i).getCustomerPerson());
				entity.setInspectionDate(start);
				int result = workOrderLogic.save(entity);
				if (result == 1)
					logger.debug("---巡检定时处理服务---    客户code:"+list.get(i).getCustomerCode()+" 设备code:"+list.get(i).getEquipmentCode()+" ,处理成功。"+ cal.getTime());

			}
		}
		logger.debug("----巡检定时处理服务开始结束----     共 耗时(s)：" + (System.currentTimeMillis() - Long.valueOf(start)) / 1000);
	}
	
	
	/**
	 * 
	 * 巡检工单生成是否命中 by guochao
	 * 
	 * @param beginDate 配置巡检开始时间
	 * @param period 周期
	 * @param advanceDays 提前通知时间
	 * @param timestamp 当前时间 时间戳
	 * 24*60*60*1000 = 86400000 （一天的毫秒数）
	 * @return
	 */
	private static boolean timeHit(String beginDate, int period, int advanceDays, String timestamp) {
		long now = Long.valueOf(timestamp);
		long start = Long.valueOf(beginDate);
		
		//（现在时间+提前通知时间-巡检开始时间）对巡检周期取余 ，余数为0 则命中
		long result = ((now + advanceDays*86400000 - start)/86400000)%period;
		
		if(result == 0)
			return true;
		else
			return false;
	}
}
