package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.Bom;

public interface BomDao extends IBaseDao<Bom>{
	
    int deleteByPrimaryKey(Integer id);

    int insertInfo(Bom record);
    
    int updateInfo(Bom record);

    Bom selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Bom record);

    int updateByPrimaryKey(Bom record);
    
    List<Bom> selectListByCondition(Bom record);
}