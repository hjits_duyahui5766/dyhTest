package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.AlarmWorkOrdersDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.dao.ProductionLinesDao;
import com.cloudinnov.dao.SectionDao;
import com.cloudinnov.dao.SectionPlanConfigDao;
import com.cloudinnov.logic.SectionLogic;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.Section;
import com.cloudinnov.model.SectionPlanConfig;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service
public class SectionLogicImpl extends BaseLogicImpl<Section> implements SectionLogic {
	/** 高速路段类型 */
	@SuppressWarnings("unused")
	private static final String TYPE_EXPY = "expy";
	/** 隧道类型 */
	@SuppressWarnings("unused")
	private static final String TYPE_TUNNEL = "tunnel";
	private static final String SECTION_TYPE_SPLIT = "|";
	private int digit = 5;
	@Autowired
	private SectionDao sectionDao;
	@Autowired
	private EquipmentsDao equipmentsDao;
	@Autowired
	private ProductionLinesDao productionLinesDao;
	@Autowired
	private AlarmWorkOrdersDao alarmWorkOrdersDao;
	@Autowired
	private SectionPlanConfigDao sectionPlanConfigDao;

	public int save(Section section) {
		String sectionCode = CodeUtil.customerCode(digit);
		section.setCode(sectionCode);
		section.setStatus(CommonUtils.STATUS_NORMAL);
		if (CommonUtils.isNotEmpty(section.getType()) && section.getType().equals(TYPE_TUNNEL)) {// 如果是隧道类型
			String startPile = section.getLeftStartPile() + SECTION_TYPE_SPLIT + section.getRightStartPile();// 拼接左隧道开始和右隧道开始桩号,形如:K150+400|K140+500
			String endPile = section.getLeftEndPile() + SECTION_TYPE_SPLIT + section.getRightEndPile();
			section.setStartPile(startPile);
			section.setEndPile(endPile);
		}
		int result = sectionDao.insert(section);
		if (result == CommonUtils.SUCCESS_NUM) {
			return CommonUtils.SUCCESS_NUM;
		} else {
			return CommonUtils.DEFAULT_NUM;
		}
	}
	@Override
	public int update(Section section) {
		if (CommonUtils.isNotEmpty(section.getType()) && section.getType().equals(TYPE_TUNNEL)) {// 如果是隧道类型
			String startPile = section.getLeftStartPile() + SECTION_TYPE_SPLIT + section.getRightStartPile();// 拼接左隧道开始和右隧道开始桩号,形如:K150+400|K140+500
			String endPile = section.getLeftEndPile() + SECTION_TYPE_SPLIT + section.getRightEndPile();
			section.setStartPile(startPile);
			section.setEndPile(endPile);
		}
		int result = sectionDao.updateByCondition(section);
		if (result == CommonUtils.SUCCESS_NUM) {
			return CommonUtils.SUCCESS_NUM;
		} else {
			return CommonUtils.DEFAULT_NUM;
		}
	}
	@Override
	public Section select(Section model) {
		Section section = sectionDao.selectEntityByCondition(model);
		if (section != null && CommonUtils.isNotEmpty(section.getType()) && section.getType().equals(TYPE_TUNNEL)) {// 如果是隧道类型
			if (CommonUtils.isNotEmpty(section.getStartPile())) {// 隧道开始桩号不为空
				// 拼接左隧道开始和右隧道开始桩号,形如:K150+400|K140+500
				if (section.getStartPile().split("\\" + SECTION_TYPE_SPLIT).length == 2) {
					String leftStartPile = section.getStartPile().split("\\" + SECTION_TYPE_SPLIT)[0];
					String rightStartPile = section.getStartPile().split("\\" + SECTION_TYPE_SPLIT)[1];
					section.setLeftStartPile(leftStartPile);
					section.setRightStartPile(rightStartPile);
				}
				if (section.getEndPile().split("\\" + SECTION_TYPE_SPLIT).length == 2) {
					String leftEndPile = section.getEndPile().split("\\" + SECTION_TYPE_SPLIT)[0];
					String rightEndPile = section.getEndPile().split("\\" + SECTION_TYPE_SPLIT)[1];
					section.setLeftEndPile(leftEndPile);
					section.setRightEndPile(rightEndPile);
				}
			}
		}
		return section;
	}
	@Override
	public Page<Section> search(int index, int size, String country, String province, String city, String key,
			String type) {
		// TODO Auto-generated method stub
		PageHelper.startPage(index, size);
		Map<String, String> map = new HashMap<String, String>();
		map.put("country", country);
		map.put("province", province);
		map.put("city", city);
		map.put("name", key);
		map.put("type", type);
		return (Page<Section>) sectionDao.search(map);
	}
	@Override
	public Section searchSectionInfo(String equipCode) {
		return sectionDao.searchSectionInfo(equipCode);
	}
	@Override
	public Map<String, Object> selectEquLineAlarmCountBySectionCode(String sectionCode) {
		Map<String, Object> countMap = new HashMap<String, Object>();
		/**
		 * 路段下的设备总数
		 */
		int equCount = equipmentsDao.selectEquipmentTotalBySectionCode(sectionCode);
		/**
		 * 路段下的产线总数
		 */
		int prolineCount = productionLinesDao.selectProductionLineTotalBySectionCode(sectionCode);
		/**
		 * 路段下的工单总数
		 */
		int alarmCount = alarmWorkOrdersDao.selectWorkOrdersTotalBySectionCode(sectionCode);
		countMap.put("equCount", equCount);
		countMap.put("prolineCount", prolineCount);
		countMap.put("alarmCount", alarmCount);
		return countMap;
	}
	@Override
	public int saveSectionPlanConfig(SectionPlanConfig section) {
		// sectionPlanConfigDao
		// 将复杂的json格式转换成list
		List<SectionPlanConfig> list = JSON.parseArray(section.getConfig(), SectionPlanConfig.class);
		SectionPlanConfig sectionPlan = null;
		int result = 0;
		for (SectionPlanConfig sectionPlanConfig : list) {
			sectionPlan = new SectionPlanConfig();
			sectionPlan.setSectionCode(sectionPlanConfig.getSectionCode());
			sectionPlan.setEquipmentCode(sectionPlanConfig.getEquipmentCode());
			sectionPlan.setBasemap(sectionPlanConfig.getBasemap());
			List<SectionPlanConfig> sectionInfo = sectionPlanConfigDao.selectListByCondition(sectionPlan);
			if (sectionInfo.size() > 0) {
				for (SectionPlanConfig sectionConfig : sectionInfo) {
					result = sectionPlanConfigDao.delete(sectionConfig.getId());
				}
			}
			if (sectionPlanConfig.getTop() != null && sectionPlanConfig.getLeft() != null) {
				sectionPlan.setLocation(sectionPlanConfig.getTop() + "," + sectionPlanConfig.getLeft());
			}
			result = sectionPlanConfigDao.insert(sectionPlan);
		}
		return result;
	}
	@Override
	public Page<SectionPlanConfig> selectSectionPlanConfig(PageModel page, SectionPlanConfig sectionPlan) {
		PageHelper.startPage(page.getIndex(), page.getSize());
		List<SectionPlanConfig> list = sectionPlanConfigDao.selectListByCondition(sectionPlan);
		for (SectionPlanConfig sectionPlanConfig : list) {
			String location = sectionPlanConfig.getLocation();
			String[] locations = location.split(",");
			sectionPlanConfig.setTop(locations[0]);
			sectionPlanConfig.setLeft(locations[1]);
		}
		return (Page<SectionPlanConfig>) list;
	}
	@Override
	public int insertSectionConfig(SectionPlanConfig section) {
		// 将复杂的json数据转换为list
		List<SectionPlanConfig> list = JSON.parseArray(section.getConfig(), SectionPlanConfig.class);
		SectionPlanConfig config = null;
		int result = 0;
		for (SectionPlanConfig sectionPlanConfig : list) {
			int flag = 0;
			if (flag == 0) {
				config = new SectionPlanConfig();
				config.setSectionCode(sectionPlanConfig.getSectionCode());
				config.setEquipmentCode(sectionPlanConfig.getEquipmentCode());
				List<SectionPlanConfig> sectionInfo = sectionPlanConfigDao.selectListByCondition(config);
				if (list.size() > 0) {
					for (SectionPlanConfig sectionPlan2 : sectionInfo) {
						sectionPlanConfigDao.delete(sectionPlan2.getId());
					}
				}
				config.setBasemap(sectionPlanConfig.getBasemap());
				if (sectionPlanConfig.getTop() != null && sectionPlanConfig.getLeft() != null) {
					config.setLocation(sectionPlanConfig.getTop() + "," + sectionPlanConfig.getLeft());
				}
				result = sectionPlanConfigDao.insert(config);
			}
		}
		return result;
	}
	@Override
	public List<SectionPlanConfig> selectSectionConfig(SectionPlanConfig section) {
		return sectionPlanConfigDao.selectSectionConfig(section);
	}
	@Override
	public int insertSectionConfigHD(SectionPlanConfig section) {
		int result = 0;
		if (!section.getConfig().equals("[]")) {
			// 将复杂的json数据转换为list
			List<SectionPlanConfig> list = JSON.parseArray(section.getConfig(), SectionPlanConfig.class);
			// 查询路段下存在的横道门
			List<SectionPlanConfig> configList = null;
			for (SectionPlanConfig sectionPlanConfig : list) {
				configList = sectionPlanConfigDao.selectSectionConfig(sectionPlanConfig.getSectionCode());
			}
			if (configList.size() > 0 && configList != null) {
				for (SectionPlanConfig sectionPlanConfig : configList) {
					sectionPlanConfigDao.delete(sectionPlanConfig.getId());
				}
			}
			if (list != null && list.size() > 0) {
				SectionPlanConfig config = null;
				for (SectionPlanConfig sectionPlanConfig : list) {
					config = new SectionPlanConfig();
					config.setSectionCode(sectionPlanConfig.getSectionCode());
					config.setEquipmentCode(sectionPlanConfig.getEquipmentCode());
					config.setBasemap(sectionPlanConfig.getBasemap());
					if (sectionPlanConfig.getTop() != null && sectionPlanConfig.getLeft() != null) {
						config.setLocation(sectionPlanConfig.getTop() + "," + sectionPlanConfig.getLeft());
					}
					result = sectionPlanConfigDao.insert(config);
				}
			}
		} else {
			result = 1;
		}
		return result;
	}
	@Override
	public int deleteSectionConfig(SectionPlanConfig section) {
		int result = sectionPlanConfigDao.deleteSectionConfig(section);
		return result;
	}
}
