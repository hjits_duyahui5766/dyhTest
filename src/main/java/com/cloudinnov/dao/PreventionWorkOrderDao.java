package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.PreventionWorkOrder;

public interface PreventionWorkOrderDao extends IBaseDao<PreventionWorkOrder>{
	
	public List<PreventionWorkOrder> search(Map<String, Object> map);
   
}