package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysCodeDao;
import com.cloudinnov.logic.SysCodeLogic;
import com.cloudinnov.model.SysCode;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月26日下午12:11:40
 * @email chaoguo@cloudinnov.com
 * @remark 获取各种code
 * @version
 */
@Service
public class SysCodeLogicImpl extends BaseLogicImpl<SysCode> implements SysCodeLogic {

	
	@Autowired
	private SysCodeDao sysCodedao;

	@Override
	public String selectEquipmentCurrentValue(String oemCode) {
		String equipmentCode = sysCodedao.selectCurrentValue("equipment",
				oemCode);
		return oemCode + "-eq-" + equipmentCode;
	}

	@Override
	public String selectEquipmentBomCurrentValue(String oemCode) {
		String equipmentBomCode = sysCodedao.selectCurrentValue(
				"equipment_bom", oemCode);
		return oemCode + "-eqb-" + equipmentBomCode;
	}

	@Override
	public String selectEquipmentPointCurrentValue(String oemCode,String type) {
		String equipmentPointCode = sysCodedao.selectCurrentValue(
				"equipment_point", oemCode);		
		if(type.equals(CommonUtils.POINT_TYPE_MN)){
			equipmentPointCode =  CommonUtils.ANALOG_QUANTITY +"_"+ oemCode + "-eqp-" + equipmentPointCode;
		}else if(type.equals(CommonUtils.POINT_TYPE_SZ)){
			equipmentPointCode =  CommonUtils.DIGITAL_QUANTITY +"_"+  oemCode + "-eqp-" + equipmentPointCode;
		}
		return equipmentPointCode;
		
	}

	@Override
	public String selectEquipmentFaultChannelCurrentValue(String oemCode) {
		String equipmentFaultChannelCode = sysCodedao.selectCurrentValue(
				"equipment_fault_channel", oemCode);
		
		return CommonUtils.FAULT_QUANTITY +"_"+ oemCode + "-eqfc-" + equipmentFaultChannelCode;
	}

	@Override
	public String selectCompanyCurrentValue(String oemCode) {
		String companyCode = sysCodedao.selectCurrentValue("customer", oemCode);
		return oemCode + "-cu-" + companyCode;
	}

	@Override
	public String selectEquipmentFaultCurrentValue(String oemCode) {
		String faultCode = sysCodedao.selectCurrentValue("equipment_fault",
				oemCode);
		return oemCode + "-eqf-" + faultCode;
	}

	@Override
	public String selectWorkOrderCurrentValue(String oemCode) {
		String workOrderCode = sysCodedao.selectCurrentValue("fault_workorder",
				oemCode);
		return oemCode + "-fwd-" + workOrderCode;
	}

	@Override
	public String selectSysLogCodeCurrentValue(String oemCode) {
		String sysLogCode = sysCodedao.selectCurrentValue("sys_code",
				oemCode);
		return oemCode + "-syc-" + sysLogCode;
	}

	@Override
	public String selectEquCategoryCurrentValue(String oemCode) {
		String equCategory = sysCodedao.selectCurrentValue("equ_cate",
				oemCode);
		return oemCode + "-eqc-" + equCategory;
	}


	@Override
	public String selectFaultTranslateLibraryCurrentValue(String oemCode) {
		String faultTranslateLibraryCode = sysCodedao.selectCurrentValue(
				"fault_translate_library", oemCode);
		return oemCode+"-ftl-"+faultTranslateLibraryCode;
	}

	@Override
	public String selectFaultCurrentValue(String oemCode) {
		String faultCode = sysCodedao.selectCurrentValue(
				"fault", oemCode);
		return oemCode+"-f-"+faultCode;
	}

	@Override
	public String selectProductionLinesCurrentValue(String oemCode) {
		String productLineCode = sysCodedao.selectCurrentValue("proline", oemCode);
		return oemCode +"-ple-" + productLineCode;
	}

	@Override
	public String selectMonitorCurrentValue(String oemCode) {
		String monitorCode = sysCodedao.selectCurrentValue("monitor", oemCode);
		return oemCode +"-mon-" + monitorCode;
	}

	@Override
	public String selectUserCurrentValue(String oemCode) {
		String userCode = sysCodedao.selectCurrentValue("user", oemCode);
		return oemCode +"-user-"+userCode;
	}
	
	@Override
	public String selectRoleCurrentValue(String oemCode) {
		String roleCode = sysCodedao.selectCurrentValue("role", oemCode);
		return oemCode +"-role-"+roleCode;
	}
	@Override
	public String selectResCurrentValue(String oemCode) {
		String resCode = sysCodedao.selectCurrentValue("res", oemCode);
		return oemCode+"-res-"+resCode;
	}

	@Override
	public String selectMaterialCurrentValue(String oemCode) {
		String materialCode = sysCodedao.selectCurrentValue("mat", oemCode);
		return oemCode+"-mat-"+materialCode;
	}

	@Override
	public String selectProductCurrentValue(String oemCode) {
		String materialCode = sysCodedao.selectCurrentValue("mpr", oemCode);
		return oemCode+"-mpr-"+materialCode;
	}

	@Override
	public String selectAlarmCurrentValue(String oemCode) {
		String materialCode = sysCodedao.selectCurrentValue("flm", oemCode);
		return oemCode+"-flm-"+materialCode;
	}

	@Override
	public String selectHelpAlarmCurrentValue(String oemCode) {
		String materialCode = sysCodedao.selectCurrentValue("fhm", oemCode);
		return oemCode+"-fhm-"+materialCode;
	}

}
