package com.cloudinnov.logic.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cloudinnov.dao.EquipmentBomsDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.logic.EquipmentBomsLogic;
import com.cloudinnov.model.EquipmentBoms;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author guochao
 * @date 2016年2月17日下午4:42:20
 * @email chaoguo@cloudinnov.com
 * @remark 设备Bom 业务层接口实现
 * @version
 */
@Service
public class EquipmentBomsLogicImpl extends BaseLogicImpl<EquipmentBoms> implements EquipmentBomsLogic {
	
	private static final Logger logger = LoggerFactory.getLogger(EquipmentBomsLogicImpl.class);
	
	private int digit = 5;
	@Autowired
	private EquipmentBomsDao equipmentBomsDao;
	@Autowired
	private EquipmentsDao equipmentDao;

	public int save(EquipmentBoms equipmentBom) {
		String equipmentCode = equipmentBom.getEquipmentCode();
		String customerCode = equipmentDao.selectCustomerCodeByEquipmentCode(equipmentCode);
		equipmentBom.setCustomerCode(customerCode);
		String code = CodeUtil.equipmentBomCode(digit);
		equipmentBom.setCode(code);
		equipmentBom.setStatus(CommonUtils.STATUS_NORMAL);
		int result = equipmentBomsDao.insert(equipmentBom);
		if (result == 1) {
			return equipmentBomsDao.insertInfo(equipmentBom);
		} else {
			return 0;
		}
	}

	@Override
	public int update(EquipmentBoms equipmentBom) {
		int returnCode = equipmentBomsDao.updateByCondition(equipmentBom);
		returnCode = equipmentBomsDao.updateInfoByCondition(equipmentBom);
		return returnCode;
	}

	@Override
	public List<EquipmentBoms> listByCode(EquipmentBoms equipmentBom) {
		return equipmentBomsDao.selectListByCondition(equipmentBom);
	}

	@Override
	public int saveOtherLanguage(EquipmentBoms equipmentBom) {
		return equipmentBomsDao.insertInfo(equipmentBom);
	}

	@Override
	public int updateOtherLanguage(EquipmentBoms equipmentBom) {
		return equipmentBomsDao.updateInfoByCondition(equipmentBom);
	}

	@Override
	public int bomImport(String data, EquipmentBoms equipmentBom) {
		
		int returnCode = 0;
		ObjectMapper mapper = new ObjectMapper();  
		JsonNode node;
		try {
			node = mapper.readTree(data);
			EquipmentBoms equipmentBoms;
			for(int i=0;i<node.size();i++)
			{
				equipmentBoms = new EquipmentBoms();
				equipmentBoms.setName(node.get(i).get("元件名").asText());
				equipmentBoms.setBrand(node.get(i).get("原厂品牌").asText());
				equipmentBoms.setModel(node.get(i).get("型号").asText());
				equipmentBoms.setSerialNumber(node.get(i).get("物料编号").asText());
				equipmentBoms.setLanguage(node.get(i).get("语言").asText());
				equipmentBoms.setCustomerCode(equipmentBom.getCustomerCode());
				equipmentBoms.setEquipmentCode(equipmentBom.getEquipmentCode());
				equipmentBoms.setCode(CodeUtil.equipmentBomCode(digit));
				equipmentBoms.setStatus(CommonUtils.STATUS_NORMAL);			
				returnCode = equipmentBomsDao.insert(equipmentBoms);
				returnCode = equipmentBomsDao.insertInfo(equipmentBoms);
			}
		} catch (JsonProcessingException e) {
			logger.error("Bom导入异常 \r{}", e);
		} catch (IOException e) {
			logger.error("Bom导入异常\r{}", e);
		}
		
		return returnCode;
	}

}
