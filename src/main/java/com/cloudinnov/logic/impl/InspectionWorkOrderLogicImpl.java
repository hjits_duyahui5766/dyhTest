package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.InspectionWorkOrderDao;
import com.cloudinnov.dao.WorkOrderDao;
import com.cloudinnov.dao.WorkOrderLogsDao;
import com.cloudinnov.logic.InspectionWorkOrderLogic;
import com.cloudinnov.model.InspectionWorkOrder;
import com.cloudinnov.model.WorkOrder;
import com.cloudinnov.model.WorkOrderLogs;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author guobo
 * @date 2016年12月6日 下午4:56:15
 * @email boguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class InspectionWorkOrderLogicImpl extends BaseLogicImpl<InspectionWorkOrder>
		implements InspectionWorkOrderLogic {

	@Autowired
	private InspectionWorkOrderDao inspectionWorkOrderDao;

	@Autowired
	private WorkOrderDao workOrderDao;

	@Autowired
	private WorkOrderLogsDao workOrderLogsDao;

	@Override
	public Page<InspectionWorkOrder> search(int index, int size, InspectionWorkOrder iwo, String key) {
		PageHelper.startPage(index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		/*
		 * if (iwo.getCustomerCode() != null &&
		 * !iwo.getCustomerCode().equals("")) { map.put("customerCode",
		 * iwo.getCustomerCode()); map.put("customerCodes",
		 * iwo.getCustomerCode().split(",")); } if (iwo.getProLineCode() != null
		 * && !iwo.getProLineCode().equals("")) { map.put("proLineCode",
		 * iwo.getProLineCode()); map.put("proLineCodes",
		 * iwo.getProLineCode().split(",")); }
		 */
		if (iwo.getEquipmentCode() != null && !iwo.getEquipmentCode().equals("")) {
			map.put("equipmentCode", iwo.getEquipmentCode());
			map.put("equipmentCodes", iwo.getEquipmentCode().split(","));
		}
		map.put("keyword", key);
		map.put("orderstatus", iwo.getOrderStatus());
		map.put("language", iwo.getLanguage());
		Page<InspectionWorkOrder> list = (Page<InspectionWorkOrder>) inspectionWorkOrderDao.search(map);
		return list;
	}

	@Override
	public int update(InspectionWorkOrder iwo) {
		// 变更责任人
		int result;
		WorkOrder workOrder = new WorkOrder();
		workOrder.setPaddingBy(iwo.getPaddingBy());
		int wo = workOrderDao.updateByCondition(workOrder);
		// 记录日志
		if (wo == 1) {
			WorkOrderLogs workOrderLogs = new WorkOrderLogs();
			int wol = workOrderLogsDao.insert(workOrderLogs);
			if (wol == 1) {
				result = 1;
			} else {
				result = 0;
			}
		} else {
			result = 0;
		}
		return result;
	}
}
