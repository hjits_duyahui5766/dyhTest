package com.cloudinnov.model;

public class ControlSolutionSendRecord extends BaseModel{

    private String name;

    private Integer level;

    private String sendContent;

    private String effectTime;

    private String expireTime;

    private Integer sendStauts;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getSendContent() {
        return sendContent;
    }

    public void setSendContent(String sendContent) {
        this.sendContent = sendContent;
    }

    public String getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public Integer getSendStauts() {
        return sendStauts;
    }

    public void setSendStauts(Integer sendStauts) {
        this.sendStauts = sendStauts;
    }

}