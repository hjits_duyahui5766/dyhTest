package com.cloudinnov.logic;

import com.cloudinnov.model.PreventionWorkOrder;
import com.github.pagehelper.Page;

public interface PreventionWorkOrderLogic extends IBaseLogic<PreventionWorkOrder> {
	
	public Page<PreventionWorkOrder> search(int index, int size, PreventionWorkOrder preventionWorkOrder,
			String key);
	
}
