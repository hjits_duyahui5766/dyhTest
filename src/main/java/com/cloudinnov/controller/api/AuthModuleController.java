package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AuthModuleLogic;
import com.cloudinnov.model.AuthModule;
import com.cloudinnov.utils.CommonUtils;

/** 
 * @ClassName: AuthModuleController 
 * @Description: 模块管理
 * @author: ningmeng
 * @date: 2016年11月14日 下午12:23:51  
 */
@Controller
@RequestMapping("/webapi/module")
public class AuthModuleController extends BaseController {
	
	@Autowired
	private AuthModuleLogic authModuleLogic;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public void getList(AuthModule authModule, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		authModule.setCompanyCode(getUserInfo(request).get(COM_CODE));
		authModule.setPortal(CommonUtils.PORTAL_ADMIN_TYPE);
        List<AuthModule> list = authModuleLogic.selectList(authModule, false);
        if(list !=null){
        	map.put("list", list);
        }
        String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
        response.getWriter().println(returnData);
    }
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "模块管理", methods = "模块信息保存")
    public void save(AuthModule authModule, HttpServletRequest request, HttpServletResponse response) throws IOException {

		authModule.setCompanyCode(getUserInfo(request).get(COM_CODE));
		authModule.setPortal(CommonUtils.PORTAL_ADMIN_TYPE);
        int result = authModuleLogic.save(authModule);
        Map<String, Object> map = new HashMap<String, Object>();
        String returnData = returnJsonAllRequest(request, response, map, result, "");
        response.getWriter().println(returnData);
    }
	
	/**
	 * resourcesDelete
	 * @Description: 根据code删除一条资源记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "模块管理", methods = "信息删除")
	public void resourcesDelete(AuthModule authModule, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authModuleLogic.delete(authModule);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resourcesUpdate
	 * @Description: 更新资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "模块管理", methods = "信息修改")
	public void resourcesUpdate(AuthModule authModule, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authModuleLogic.update(authModule);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	
	/**
	 * resSaveOtherLanguage
	 * @Description: 资源增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "模块管理", methods = "修改其他语言")
	public void resSaveOtherLanguage(AuthModule authModule, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authModule.setLanguage(authModule.getOtherLanguage());
		int returnCode = authModuleLogic.saveOtherLanguage(authModule);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "模块管理", methods = "修改其他语言")
	public void resUpdateOtherLanguage(AuthModule authModule, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authModule.setLanguage(authModule.getOtherLanguage());
		int returnCode = authModuleLogic.updateOtherLanguage(authModule);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
}
