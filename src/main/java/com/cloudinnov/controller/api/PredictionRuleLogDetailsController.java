package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.PredictionRuleLogDetailsLogic;
import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleLogDetails;

@Controller
@RequestMapping("/webapi/predictionRuleLogDetails")
public class PredictionRuleLogDetailsController extends BaseController {
	static final Logger LOG = LoggerFactory.getLogger(PredictionRuleLogDetailsController.class);

	@Autowired
	private PredictionRuleLogDetailsLogic predictionRuleLogDetailsLogic;

	@RequestMapping(value = "/insertPredictionRuleLogDetails", method = RequestMethod.POST)
	@SystemLog(module = "预案执行记录详情添加", methods = "预案执行记录详情添加方法")
	public void insertPredictionRuleLogDetails(PredictionRuleLogDetails predictionRuleLogDetails, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//解决跨域问题的代码
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> predictionRuleResult = new HashMap<>();
		int result = predictionRuleLogDetailsLogic.insertPredictionRuleLogDetails(predictionRuleLogDetails);
		if (result == 1) {
			map.put("data", predictionRuleResult);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 查询预案执行记录详情方法
	 * 
	 * @param PredictionRuleLogDetails
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectPredictionRuleLogDetails", method = RequestMethod.GET)
	@SystemLog(module = "更新触发预案", methods = "更新触发预案方法")
	public void selectPredictionRuleLogDetails(PredictionRuleLog PredictionRuleLog, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		//查询预案执行记录
		PredictionRuleLog ruleLog = predictionRuleLogDetailsLogic.selectPredictionRuleLogDetails(PredictionRuleLog);
		Map<String, Object> map = new HashMap<String, Object>();
		if (null != ruleLog) {
			map.put("data", ruleLog);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
}
