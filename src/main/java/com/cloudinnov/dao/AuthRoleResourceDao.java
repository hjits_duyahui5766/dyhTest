package com.cloudinnov.dao;

import java.util.Map;

import com.cloudinnov.model.AuthRoleResource;


public interface AuthRoleResourceDao extends IBaseDao<AuthRoleResource>{
	public int insertList(Map<String,Object> map);
}