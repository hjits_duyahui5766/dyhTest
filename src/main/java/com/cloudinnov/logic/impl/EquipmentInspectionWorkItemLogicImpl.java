package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentInspectionWorkItemDao;
import com.cloudinnov.logic.EquipmentInspectionWorkItemLogic;
import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkItem;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service
public class EquipmentInspectionWorkItemLogicImpl extends BaseLogicImpl<EquipmentInspectionWorkItem> implements EquipmentInspectionWorkItemLogic {
	
	@Autowired
	private EquipmentInspectionWorkItemDao itemDao;
	
	public Page<EquipmentInspectionWorkItem> selectByConfigCode(int index, int size,EquipmentInspectionConfig entity) {
		PageHelper.startPage(index, size);
		return (Page<EquipmentInspectionWorkItem>)itemDao.selectByConfigCode(entity);
	}

}
