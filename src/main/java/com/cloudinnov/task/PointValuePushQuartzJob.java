package com.cloudinnov.task;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.logic.RealTimeDataLogic;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.websocket.EquipmentStateWebsocket;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月13日下午2:14:17
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public class PointValuePushQuartzJob {
	private static final Logger LOG = LoggerFactory.getLogger(PointValuePushQuartzJob.class);
	@Autowired
	private RealTimeDataLogic realTimeDataLogic;

	private void monitor() {
		sendMesssageToEquipmentStateWebsocket();// 推送数据给所有连接上来的路段
	}
	public int sendMesssageToEquipmentStateWebsocket() {
		// 遍历所有连接设备状态的路段连接,然后根据截取路段编码查询数据推送给指定的连接
		Iterator<Map.Entry<String, EquipmentStateWebsocket>> equipmentStateWebsocket = EquipmentStateWebsocket.webSocketMap
				.entrySet().iterator();
		while (equipmentStateWebsocket.hasNext()) {
			Map.Entry<String, EquipmentStateWebsocket> alarmDialogueWebsocketEntry = equipmentStateWebsocket.next();
			String sectionCode = alarmDialogueWebsocketEntry.getKey().split(EquipmentStateWebsocket.SPLIT1)[0];
			if (CommonUtils.isNotEmpty(sectionCode)) {
				
				List<Equipments> data = realTimeDataLogic.selectRealtimeIndexTypeDataBySectionCode(sectionCode);
				try {
					alarmDialogueWebsocketEntry.getValue().sendMessage(JSON.toJSONString(data));
				} catch (IOException e) {
					LOG.error("sendMesssageToEquipmentStateWebsocket is error, error: {}", e);
				}
			}
		}
		return 0;
	}
}
