package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cloudinnov.logic.PreventionWorkOrderLogic;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.PreventionWorkOrder;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/preventionWorkOrder")
public class PreventionWorkOrderController extends BaseController {
	
	@Autowired
	private PreventionWorkOrderLogic preventionWorkOrderLogic;
	
	@RequestMapping(value = "/search")
	@ResponseBody
	public void serach(PageModel page , String key,PreventionWorkOrder preventionWorkOrder, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		Page<PreventionWorkOrder> list = preventionWorkOrderLogic.search(page.getIndex(), page.getSize(), preventionWorkOrder, key);
		
		map.put("alarmUntreateds", CommonUtils.DEFAULT_NUM);
		map.put("alarmHandles", CommonUtils.DEFAULT_NUM);
		map.put("alarmCloses", CommonUtils.DEFAULT_NUM);
		map.put("alarmSees", CommonUtils.DEFAULT_NUM);
		
		if(list.getResult()!=null && list.getResult().size()>0){
			for(int i = 0 ;i<list.size();i++){
				if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_NEW){
					map.put("alarmUntreateds", (Integer) map.get("alarmUntreateds") + 1);
				}else if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_HANDLE){
					map.put("alarmHandles", (Integer) map.get("alarmHandles") + 1);
				}else if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_CLOSE){
					map.put("alarmCloses", (Integer) map.get("alarmCloses") + 1);
				}
			}
		}
		String returnData = "";	
		map.put("list", list);
		map.put("total", list.getTotal());
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(PreventionWorkOrder preventionWorkOrder, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PreventionWorkOrder model = preventionWorkOrderLogic.select(preventionWorkOrder);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (model != null) {
			map.put("model", model);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
}
