package com.cloudinnov.model;

import java.util.List;

public class AlarmContent {
	
	
	private Integer msgType;
	
	private String sendBy;
	
	private Integer canAnswer;
	
	private List<Message> messages;

	private Message message;
	
	private AuthUsers sponsor;

	public AuthUsers getSponsor() {
		return sponsor;
	}

	public void setSponsor(AuthUsers sponsor) {
		this.sponsor = sponsor;
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}



	public String getSendBy() {
		return sendBy;
	}



	public void setSendBy(String sendBy) {
		this.sendBy = sendBy;
	}



	public Integer getCanAnswer() {
		return canAnswer;
	}



	public void setCanAnswer(Integer canAnswer) {
		this.canAnswer = canAnswer;
	}


	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	
	public List<Message> getMessages() {
		return messages;
	}



	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}


	public class Message {
			
		private String url;
		
		private Integer length;
		
		private String content;
		
		private String pic;

		

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public Integer getLength() {
			return length;
		}

		public void setLength(Integer length) {
			this.length = length;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getPic() {
			return pic;
		}

		public void setPic(String pic) {
			this.pic = pic;
		}		
	}
}
