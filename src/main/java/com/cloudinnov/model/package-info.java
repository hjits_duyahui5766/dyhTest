// ----------------------------------------------------------------------------
// Copyright 2013-2016, Sino-Cloud Innovation Tech, Inc.
// All rights reserved
// ----------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// ----------------------------------------------------------------------------
// chengning 
// 2016年3月2日13:54:38
// ----------------------------------------------------------------------------
/**
*** model 实体类
**/
package com.cloudinnov.model;
