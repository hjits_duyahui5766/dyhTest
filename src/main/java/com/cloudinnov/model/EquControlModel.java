package com.cloudinnov.model;

import java.util.List;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年3月28日下午12:17:26
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public class EquControlModel {
	private String equCode;
	private String categoryCode;
	private String command;
	private String value;
	private List<Config> config;

	public String getEquCode() {
		return equCode;
	}
	public void setEquCode(String equCode) {
		this.equCode = equCode;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public List<Config> getConfig() {
		return config;
	}
	public void setConfig(List<Config> config) {
		this.config = config;
	}

	public static class Config {
		private Object value;
		private String indexType;
		private String type;

		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public String getIndexType() {
			return indexType;
		}
		public void setIndexType(String indexType) {
			this.indexType = indexType;
		}
		@Override
		public String toString() {
			return "Config [value=" + value + ", indexType=" + indexType + "]";
		}
	}

	@Override
	public String toString() {
		return "EquControlModel [equCode=" + equCode + ", command=" + command + ", value=" + value + ", config="
				+ config + "]";
	}
}
