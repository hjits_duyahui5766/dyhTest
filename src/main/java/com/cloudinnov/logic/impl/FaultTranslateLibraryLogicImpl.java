package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.FaultTranslationLibrariesDao;
import com.cloudinnov.logic.FaultTranslateLibraryLogic;
import com.cloudinnov.model.FaultTranslationLibraries;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年3月1日下午5:53:28
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class FaultTranslateLibraryLogicImpl extends BaseLogicImpl<FaultTranslationLibraries>
		implements FaultTranslateLibraryLogic {
	private int digit = 5;

	@Autowired
	private FaultTranslationLibrariesDao dao;

	public int save(FaultTranslationLibraries ftl) {
		ftl.setOemCode(ftl.getOemCode());
		String code = CodeUtil.faultTranslateLibraryCode(digit);
		ftl.setCode(code);
		int returnCode = dao.insert(ftl);
		if (returnCode == 1) {
			returnCode = dao.insertInfo(ftl);
		}
		return returnCode;
	}

	@Override
	public int update(FaultTranslationLibraries ftl) {
		int returnCode = dao.updateByCondition(ftl);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = dao.updateInfoByCondition(ftl);
		} else {
			returnCode = 0;
		}
		return returnCode;
	}

	@Override
	public List<FaultTranslationLibraries> listByCode(FaultTranslationLibraries ftl) {
		return dao.selectListByCondition(ftl);
	}

	@Override
	public int saveOtherLanguage(FaultTranslationLibraries faultTranslationLibrary) {
		return dao.insertInfo(faultTranslationLibrary);
	}

	@Override
	public int updateOtherLanguage(FaultTranslationLibraries faultTranslationLibrary) {
		return dao.updateInfoByCondition(faultTranslationLibrary);
	}

}
