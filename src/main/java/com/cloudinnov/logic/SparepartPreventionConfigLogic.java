package com.cloudinnov.logic;

import com.cloudinnov.model.SparepartPreventionCondition;
import com.cloudinnov.model.SparepartPreventionConfig;

public interface SparepartPreventionConfigLogic extends IBaseLogic<SparepartPreventionConfig> {
	public int save(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition);
	public SparepartPreventionConfig selectBySparepartCode(SparepartPreventionConfig sparepartPreventionConfig);
	public int update(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition);
}
