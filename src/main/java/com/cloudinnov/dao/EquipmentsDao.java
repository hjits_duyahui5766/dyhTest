package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.Companies;
import com.cloudinnov.model.EquipmentArea;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月17日上午11:49:08
 * @email chaoguo@cloudinnov.com
 * @remark 设备dao接口
 * @version
 */
/**
 * @author chengning
 * @date 2016年8月5日下午6:00:19
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface EquipmentsDao extends IBaseDao<Equipments> {
    /**
     * selectEquipmentTotalByCustomerId
     * @Description: 根据客户code
     * @param @param customerCode
     * @param @return 参数
     * @return int 返回类型
     */
    public int selectEquipmentTotalByCustomerCode(String comCode);
    /**
     * listByCustomer
     * @Description: 根据客户id 查询该客户下的所有设备
     * @param @param customerId
     * @param @return 参数
     * @return List<Equipments> 返回类型
     */
    public List<Equipments> listByCustomer(Equipments equipments);
    public List<Equipments> listByCustomers(Map<String, Object> map);
    /**
     * 按照客户code和设备状态查询设备列表 selectEquStateListByCustomCode
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param equipments
     * @param @return 参数
     * @return List<Equipments> 返回类型
     */
    public List<Equipments> selectEquStateListByCustomCode(Equipments equipments);
    /**
     * selectCustomerCodeByEquipmentCode
     * @Description: 根据设备code查询客户code
     * @param @param equipmentCode
     * @param @return 参数
     * @return String 返回类型
     */
    public String selectCustomerCodeByEquipmentCode(String equipmentCode);
    public Page<Equipments> search(Map<String, Object> map);
    public int insertInfo(Equipments equipments);
    public int updateInfoByCondition(Equipments equipments);
    /**
     * 查询我的设备列表 selectEquipmentsByUser
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param equipments
     * @param @return 参数
     * @return List<Equipments> 返回类型
     */
    public List<Equipments> selectEquipmentsByUser(Equipments equipments);
    /**
     * 查询离线 故障设备 selectEquipmentsStateByCondition
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @return 参数
     * @return int 返回类型
     */
    public int selectEquipmentsStateByCondition(Companies company);
    /**
     * 查询oem下的设备数量 selectEquipmentListByOemCode
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param company
     * @param @return 参数
     * @return int 返回类型
     */
    public int selectEquipmentListByOemCode(Companies company);
    /**
     * 更新设备状态
     * @param equipments
     * @return
     */
    public int updateCurrentStateByCondition(Equipments equipments);
    /**
     * 批量更新设备状态
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param map
     * @param @return 参数
     * @return int 返回类型
     */
    int updateBatchCurrentStateByCondition(Map<String, Object> map);
    int updateBatchByCondition(Map<String, Object> map);
    int insertBatch(Map<String, Object> map);
    /**
     * 根据状态获取设备列表
     * @Title: getEquipmentListByState
     * @Description: TODO 根据状态获取设备列表
     * @param company oemcode，设备状态
     * @return 设备信息，经纬度，设备状态等信息
     * @return: List<Equipments>
     */
    public List<Equipments> getEquipmentListByState(Companies company);
    /**
     * 根据路段code获取设备
     * @param record
     * @return
     */
    public List<Equipments> selectEquListBySectionCode(Equipments equipments);
    /**
     * 根据路段code获取设备数量
     * @param sectionCode
     * @return
     */
    public int selectEquipmentTotalBySectionCode(String sectionCode);
    public List<Equipments> selectMainMonitor(Equipments equipments);
    /**
     * 查询设备状态列表
     * @Title: selectEquipmentStateList
     * @Description: TODO
     * @param equipments
     * @return
     * @return: List<Equipments>
     */
    public List<Equipments> selectEquipmentStateList(Equipments equipments);
    /**
     * 查询设备分类下的设备列表
     * @Title: selectEquListByCateCode
     * @Description: TODO
     * @param equipments
     * @return
     * @return: List<Equipments>
     */
    public List<Equipments> selectEquListByCateCode(Equipments equipments);
    public Page<Equipments> selectEquipmentsInfo(Equipments equipments);
    public Page<Equipments> selectInfoQuipments(Equipments equipments);
    public Page<Equipments> selectEqInfoBySectionCode(Equipments equipments);
    public List<Equipments> selectEquimentClassifyInfo(Equipments equipments);
    public List<Equipments> selectEquimentClassifyInfos(Equipments equipments);
    public List<Equipments> selectEquipmentsBySeCode(Equipments equipments);
    public List<Equipments> selectEquipmentsByEquiClassify(@Param("categoryCode") String categoryCode,
            @Param("type") String type, @Param("sectionCode") String sectionCode);
    public List<Equipments> selectEquipmentsByName(Equipments equipments);
    public Equipments selectEquipmentsByCode(Equipments equipments);
    /**
     * 根据路段或者分类查询设备编码集合
     * @param equipments
     * @return 参数
     * @return List<Equipments> 返回类型
     */
    List<Equipments> selectEquCodesByCateCodeAndSectionCode(Equipments equipments);
    /**
     * 根据设备编码查询单个设备
     * @param equipmentCode
     * @return 参数 设备编码 设备桩号 设备路段 设备分类
     * @return Equipments 返回类型
     */
    Equipments selectSingleEquipmentByEquCode(@Param("equipmentCode") String equipmentCode);
    
    /**
     * 根据父设备编码的code查询子设备信息
     */
	public List<Equipments> selectChildrenEquipmentByCode(String equipmentCode);
	
	 /**
     * 根据子设备编码的code查询唯一的父设备信息
     */
	public Equipments selectParentEquipmentByCode(String equipmentCode);
	
	/**
	 * 由设备code值获取设备信息
	 * @param equipmentCode
	 * @return
	 */
	public Equipments selectEquipmentPointByCode(String equipmentCode);
	
	//成安渝监测添加方法
	 List<Equipments> selectEquipmentPointByCategoryCode(@Param("categoryCode") String categoryCode);
	
	 List<Equipments> selectNjdEquipmentByCategoryCode(@Param("categoryCode") String categoryCode);
	 Equipments selectEquPileNoByPointCode(@Param("code") String code);
	//查询表中所有sectionCode的值
	 List<Equipments> selectListSectionCode();
	 //根据sectionCode的值查询该sectionCode下的所有微波车辆检测器
	 List<Equipments> selectEquipmentGroupBySectionCode();
	 
	 //查询所有的隧道分类
	 List<Equipments> selectAllClassifyTunnel();
	 
	 //查询隧道下的事故区域
	 List<Equipments> selectAccidentAreaId(Equipments equipments);
	 
	 //查询单个隧道下右洞的所有枪型摄像头设备
	 List<Equipments> selectRightCameraEquipments(Equipments equipments);
	 //查询单个隧道下左洞的所有枪型摄像头设备
	 List<Equipments> selectLeftCameraEquipments(Equipments equipments);
	 
	 //根据设备code查询设备所在区域
	 Equipments selectEquipArea(EquipmentsAttr equipmentsAttr);
	 //通过EquipmentsAttr查询事故区域
	 String selectAreaIdByCanera(EquipmentsAttr equipmentsAttr);
	 
	 Equipments selectPositionByCode(EquipmentsAttr equipmentsAttr);
	 
	 //查询隧道下的横洞数(以带左转的车道指示器数量/2作为标准)
	 int selectHDCount(Equipments equipments);
	 
	 /**
	  * 通过SectionAndCategory查询设备
	  * @param equipments
	  * @return
	  */
	 public List<Equipments> selectEquipsBySectionAndCategory(Equipments equipments);
	 
	 //查询在该车道下和事故区域的车道指示器设备
	 List<Equipments> selectEquipsBySectionAndCategoryAndLaneAndAreaId(Equipments equipments);
	
	 //查询路段下的事故区域的触发设备分类
	 public List<Equipments> selectTrrigerClassEquipments(@Param("categorys")List<String> categorys, @Param("areaId")String areaId, @Param("sectionCode")String sectionCode);
	 
	 //查询路段下某事故区域的触发设备
	 public List<Equipments> selectTriggerEquipmentsBySection(Equipments equipments);
	 
	 //添加监控区域跟触发设备关联关系表
	 int insertEquipmentArea(EquipmentArea equipmentArea);
	 
	 //查询单个触发设备和监控区域的方法
	 List<EquipmentArea> selectEquipmentArea(EquipmentArea equipmentArea);

	 //更新一条监控区域关联关系表
	 int updateEquipmentArea(EquipmentArea equipmentArea);
	 
	 //删除一个监控区域的关联关系
	 int deleteEquipmentArea(EquipmentArea EquipmentArea);
	 
	 //根据sectionCode查询监控区域
	 List<EquipmentArea> selectCameraArea(EquipmentArea EquipmentArea);
	 
	 //查询隧道的名称
	 String selectSectionName(@Param("sectionCode")String sectionCode);
	 
	 //根据cameraCode查询摄像机name
	 Equipments selectCameraName(@Param("cameraCode")String cameraCode);
	 
	 //查询关系表中的隧道信息
	 List<EquipmentArea> selectEquipmentAreaSection();
	 
	 //查询事故区域下的监控区域信息
	 List<Equipments> selectCameraAreaBySection(Equipments equipments);
	 
	 List<Equipments> selectAreaIdByCategory(Equipments equipments);
	 
	 //查询隧道下每个洞有几个车道
	 List<Equipments> selectLanesBySection(Equipments equipments);
}
