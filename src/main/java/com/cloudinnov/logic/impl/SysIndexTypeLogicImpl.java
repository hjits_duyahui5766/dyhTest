package com.cloudinnov.logic.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysIndexTypeDao;
import com.cloudinnov.logic.SysIndexTypeLogic;
import com.cloudinnov.model.SysIndexType;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

@Service
public class SysIndexTypeLogicImpl extends BaseLogicImpl<SysIndexType> implements SysIndexTypeLogic {

	@Autowired
	private SysIndexTypeDao sysIndexTypeDao;
	
	public int save(SysIndexType sysIndexTypey) {
		String code = CodeUtil.sysIndexTypeCode(4);
		sysIndexTypey.setCode(code);
		int returnCode = sysIndexTypeDao.insert(sysIndexTypey);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = sysIndexTypeDao.insertInfo(sysIndexTypey);
		}
		return returnCode;
	}

	
	@Override
	public int saveOtherLanguage(SysIndexType sysIndexType) {
		// TODO Auto-generated method stub
		return sysIndexTypeDao.insertInfo(sysIndexType);
	}

	@Override
	public int updateOtherLanguage(SysIndexType sysIndexType) {
		// TODO Auto-generated method stub
		return sysIndexTypeDao.updateByCondition(sysIndexType);
	}


	@Override
	public List<SysIndexType> selectIndexTypeListByEquipmentCodes(SysIndexType indexType) {
		if(CommonUtils.isEmpty(indexType.getCode())){
			return Collections.emptyList();
		}
		Map<String, Object> params = new HashMap<>();
		params.put("codes", indexType.getCode().split(CommonUtils.SPLIT_COMMA));
		params.put("language", indexType.getLanguage());
		params.put("chartType", indexType.getChartType());
		return sysIndexTypeDao.selectIndexTypeListByEquipmentCodes(params);
	}

}
