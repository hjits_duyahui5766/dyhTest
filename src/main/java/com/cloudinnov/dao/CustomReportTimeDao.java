package com.cloudinnov.dao;

import com.cloudinnov.model.CustomReportTime;

public interface CustomReportTimeDao extends IBaseDao<CustomReportTime> {
	int deleteByPrimaryKey(Integer id);

	int insert(CustomReportTime record);

	int insertSelective(CustomReportTime record);

	CustomReportTime selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CustomReportTime record);

	int updateByPrimaryKey(CustomReportTime record);
}