package com.cloudinnov.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.cloudinnov.logic.FaultsLogic;
import com.cloudinnov.utils.support.spring.SpringUtils;

/**
 * @author chengning
 * @date 2016年8月4日上午12:28:43
 * @email 
 * @remark 故障统计。查询mysql工单表，把统计结果存入mysql faultstat表中。
 * @version 
 */
public class FaultStatQuartzJob {

	private static final Logger logger = Logger
			.getLogger(FaultStatQuartzJob.class);
	private SimpleDateFormat sdfSql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private FaultsLogic faultLogic = SpringUtils.getBean("faultsLogic");

	protected void execute() throws ParseException {
		Calendar cal = Calendar.getInstance();
    	long start  = System.currentTimeMillis();
    	logger.debug("故障统计服务开始启动     "+sdfSql.format(cal.getTime()));
    	int returnCode = faultLogic.insertFaultStat(start); 	
        logger.debug("故障统计服务处理完毕  处理  "+returnCode+"   条 耗时(s)"+(System.currentTimeMillis()-start)/1000);
	}
	
}
