package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.ProductionScheduleDao;
import com.cloudinnov.logic.ProductionSchedulesLogic;
import com.cloudinnov.model.ProductionSchedule;
import com.cloudinnov.utils.CommonUtils;


/**
 * @author nilixin
 * @date 2016年2月17日上午11:50:41
 * @email 
 * @remark
 * @version
 */
@Service
public class ProductionSchedulesLogicImpl extends BaseLogicImpl<ProductionSchedule> implements ProductionSchedulesLogic {

	@Autowired
	private ProductionScheduleDao productionSchedulesDao;
	

	public int save(ProductionSchedule productionSchedules) {
		productionSchedules.setCode(CommonUtils.getUUID().substring(0,10));
		return productionSchedulesDao.insert(productionSchedules);
	}


	@Override
	public List<ProductionSchedule> selectListStatByCondition(ProductionSchedule productionSchedules) {
		// TODO Auto-generated method stub
		return productionSchedulesDao.selectListStatByCondition(productionSchedules);
	}

	
}
