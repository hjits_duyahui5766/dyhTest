package com.cloudinnov.dao;

import com.cloudinnov.model.CustomReportDimension;

public interface CustomReportDimensionDao extends IBaseDao<CustomReportDimension> {
	int deleteByPrimaryKey(Integer id);

	int insert(CustomReportDimension record);

	int insertSelective(CustomReportDimension record);

	CustomReportDimension selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CustomReportDimension record);

	int updateByPrimaryKey(CustomReportDimension record);
}