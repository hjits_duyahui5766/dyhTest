package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.BomDao;
import com.cloudinnov.logic.BomLogic;
import com.cloudinnov.model.Bom;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author chengning
 * @date 2016年6月30日下午2:43:07
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Service
public class BomLogicImpl extends BaseLogicImpl<Bom> implements BomLogic {
	private int digit = 5;
	
	@Autowired
	private BomDao bomDao;
	
	public int save(Bom bom) {
		String code = CodeUtil.equipmentBomCode(digit);
		bom.setCode(code);
		bom.setStatus(CommonUtils.STATUS_NORMAL);
		int result = bomDao.insert(bom);
		if (result == 1) {
			return bomDao.insertInfo(bom);
		} else {
			return 0;
		}
	}

	@Override
	public int saveOtherLanguage(Bom bom) {
		int returnCode = bomDao.insertInfo(bom);
		if (returnCode != CommonUtils.SUCCESS_NUM) {
			returnCode = 0;
		}
		return returnCode;
	}

	@Override
	public int updateOtherLanguage(Bom bom) {
		int returnCode = bomDao.updateInfo(bom);
		if (returnCode != CommonUtils.SUCCESS_NUM) {
			returnCode = 0;
		}
		return returnCode;
	}

	@Override
	public List<Bom> listByCode(Bom bom) {
		// TODO Auto-generated method stub
		return bomDao.selectListByCondition(bom);
	}

	@Override
	public int bomImport(String data, Bom bom) {
		// TODO Auto-generated method stub
		return 0;
	}

}
