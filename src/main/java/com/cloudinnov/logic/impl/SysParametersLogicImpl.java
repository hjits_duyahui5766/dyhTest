package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysParametersDao;
import com.cloudinnov.logic.SysParametersLogic;
import com.cloudinnov.model.SysParameters;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author chengning
 * @date 2016年2月18日下午4:09:44
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class SysParametersLogicImpl extends BaseLogicImpl<SysParameters> implements SysParametersLogic {
	private int digit = 5;

	@Autowired
	private SysParametersDao sysParametersDao;

	public int save(SysParameters sp) {
		sp.setCode(CodeUtil.parameterCode(digit));
		sp.setStatus(CommonUtils.STATUS_NORMAL);
		return sysParametersDao.insert(sp);
	}

}
