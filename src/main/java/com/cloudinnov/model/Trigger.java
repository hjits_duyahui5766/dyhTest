package com.cloudinnov.model;

public class Trigger extends BaseModel {
	private Integer id;
	private String code;
	private String name;
	private Integer tiggerRule;
	private Integer repeatPeriod;
	private String yearDay;
	private String monthDay;
	private String week;
	private String timeRule;
	private String condition;
	private String effectTime;
	private String expireTime;
	private Integer status;
	private String comment;
	private String createTime;
	private String lastUpdateTime;
	private Integer onOff;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTiggerRule() {
		return tiggerRule;
	}
	public void setTiggerRule(Integer tiggerRule) {
		this.tiggerRule = tiggerRule;
	}
	public Integer getRepeatPeriod() {
		return repeatPeriod;
	}
	public void setRepeatPeriod(Integer repeatPeriod) {
		this.repeatPeriod = repeatPeriod;
	}
	public String getYearDay() {
		return yearDay;
	}
	public void setYearDay(String yearDay) {
		this.yearDay = yearDay;
	}
	public String getMonthDay() {
		return monthDay;
	}
	public void setMonthDay(String monthDay) {
		this.monthDay = monthDay;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getTimeRule() {
		return timeRule;
	}
	public void setTimeRule(String timeRule) {
		this.timeRule = timeRule;
	}
	public String getEffectTime() {
		return effectTime;
	}
	public void setEffectTime(String effectTime) {
		this.effectTime = effectTime;
	}
	public String getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Integer getOnOff() {
		return onOff;
	}
	public void setOnOff(Integer onOff) {
		this.onOff = onOff;
	}
	@Override
	public String toString() {
		return "Trigger [id=" + id + ", code=" + code + ", name=" + name + ", tiggerRule=" + tiggerRule
				+ ", repeatPeriod=" + repeatPeriod + ", yearDay=" + yearDay + ", monthDay=" + monthDay + ", week="
				+ week + ", timeRule=" + timeRule + ", condition=" + condition + ", effectTime=" + effectTime
				+ ", expireTime=" + expireTime + ", status=" + status + ", comment=" + comment + ", createTime="
				+ createTime + ", lastUpdateTime=" + lastUpdateTime + ", onOff=" + onOff + "]";
	}
}
