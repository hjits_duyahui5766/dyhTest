package com.cloudinnov.model;

import java.util.Date;
import java.util.List;

public class LightDetect {
	public static final String DEVICE_ID = "deviceId";
	public static final String PERIOD = "period";
	public static final String LANE_TOTAL = "laneTotal";
	public static final String IP = "ip";
	private String equipmentCode;
	private String value;
	private String deviceId;
	private Integer period;
	private Integer laneTotal;
	private String laneData;
	private Long lightTotal;
	private String summaryTime;
	private Date utcTime;
	private long timeMillis;
	private String ip;
	private Integer currentState;
	private List<LightDetectChilren> data;
	private String euqipmentName;
	private String sectionName;
	private String pileNo;
	private Integer type;// 1年，2月，3日，4时
	private Lightdata lightData;

	public static class Lightdata implements Comparable<Lightdata> {
		private String time;
		private String value;

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Lightdata [time=" + time + ", value=" + value + ", getTime()=" + getTime() + ", getValue()="
					+ getValue() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
					+ super.toString() + "]";
		}

		@Override
		public int compareTo(Lightdata lightData) {
			return this.getTime().compareTo(lightData.getTime());
		}
	}

	public Integer getLaneTotal() {
		return laneTotal;
	}

	public void setLaneTotal(Integer laneTotal) {
		this.laneTotal = laneTotal;
	}

	public Long getLightTotal() {
		return lightTotal;
	}

	public void setLightTotal(Long lightTotal) {
		this.lightTotal = lightTotal;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Lightdata getLightData() {
		return lightData;
	}

	public void setLightData(Lightdata lightData) {
		this.lightData = lightData;
	}

	public String getEuqipmentName() {
		return euqipmentName;
	}

	public void setEuqipmentName(String euqipmentName) {
		this.euqipmentName = euqipmentName;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getPileNo() {
		return pileNo;
	}

	public void setPileNo(String pileNo) {
		this.pileNo = pileNo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getLaneData() {
		return laneData;
	}

	public void setLaneData(String laneData) {
		this.laneData = laneData;
	}

	public String getSummaryTime() {
		return summaryTime;
	}

	public void setSummaryTime(String summaryTime) {
		this.summaryTime = summaryTime;
	}

	public Date getUtcTime() {
		return utcTime;
	}

	public void setUtcTime(Date utcTime) {
		this.utcTime = utcTime;
	}

	public long getTimeMillis() {
		return timeMillis;
	}

	public void setTimeMillis(long timeMillis) {
		this.timeMillis = timeMillis;
	}

	public List<LightDetectChilren> getData() {
		return data;
	}

	public void setData(List<LightDetectChilren> data) {
		this.data = data;
	}

	public Integer getCurrentState() {
		return currentState;
	}

	public void setCurrentState(Integer currentState) {
		this.currentState = currentState;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public class LightDetectChilren {
		private String deviceId;
		private long lineId;
		private long lineAvgVelocuty;
		private long lineFlow;
		private long lineOccRate;

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public long getLineId() {
			return lineId;
		}

		public void setLineId(long lineId) {
			this.lineId = lineId;
		}

		public long getLineAvgVelocuty() {
			return lineAvgVelocuty;
		}

		public void setLineAvgVelocuty(long lineAvgVelocuty) {
			this.lineAvgVelocuty = lineAvgVelocuty;
		}

		public long getLineFlow() {
			return lineFlow;
		}

		public void setLineFlow(long lineFlow) {
			this.lineFlow = lineFlow;
		}

		public long getLineOccRate() {
			return lineOccRate;
		}

		public void setLineOccRate(long lineOccRate) {
			this.lineOccRate = lineOccRate;
		}
	}

	@Override
	public String toString() {
		return "LightDetect [equipmentCode=" + equipmentCode + ", deviceId=" + deviceId + ", period=" + period
				+ ", laneTotal" + laneTotal + ", lightTotal" + lightTotal + ", laneData=" + laneData 
				+ ", summaryTime=" + summaryTime + ", utcTime=" + utcTime + ", timeMillis="+ timeMillis 
				+ ", currentState=" + currentState + ", data=" + data + "]";
	}

}
