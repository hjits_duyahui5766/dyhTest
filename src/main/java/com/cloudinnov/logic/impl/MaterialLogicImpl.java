package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.MaterialDao;
import com.cloudinnov.logic.MaterialLogic;
import com.cloudinnov.model.Material;
import com.cloudinnov.model.ProductionLines;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author chengning
 * @date 2016年3月31日下午12:11:32
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class MaterialLogicImpl extends BaseLogicImpl<Material> implements MaterialLogic {
	private int digit = 5;
	@Autowired
	private MaterialDao materialDao;

	@Override
	public int save(Material material) {
		material.setCode(CodeUtil.matCode(digit));
		int rerurnCode = materialDao.insert(material);
		if (rerurnCode == CommonUtils.SUCCESS_NUM) {
			rerurnCode = materialDao.insertInfo(material);
		} else {
			rerurnCode = 0;
		}
		return rerurnCode;
	}

	@Override
	public int update(Material material) {
		int rerurnCode = materialDao.updateByCondition(material);
		if (rerurnCode == CommonUtils.SUCCESS_NUM) {
			rerurnCode = materialDao.updateInfoByCondition(material);
		} else {
			rerurnCode = 0;
		}
		return rerurnCode;
	}

	@Override
	public int saveOtherLanguage(Material material) {
		return materialDao.insertInfo(material);
	}

	@Override
	public int updateOtherLanguage(Material material) {
		return materialDao.updateInfoByCondition(material);
	}

	@Override
	public List<Material> selectMaterialByProlineCode(String proLineCodes, String langauge) {
		if (proLineCodes == null) {
			return null;
		}
		String[] codes = proLineCodes.split(",");
		ProductionLines productionLine = null;
		List<Material> list = new ArrayList<Material>();
		for (int i = 0; i < codes.length; i++) {
			productionLine = new ProductionLines();
			productionLine.setLanguage(langauge);
			productionLine.setCode(codes[i]);
			List<Material> result = materialDao.selectMaterialByProlineCode(productionLine);
			list.addAll(result);
		}
		return list;
	}

}
