package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.FaultTranslateLibraryLogic;
import com.cloudinnov.model.FaultTranslationLibraries;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年3月1日下午5:46:11
 * @email chaoguo@cloudinnov.com
 * @remark 故障码翻译库controlller
 * @version
 */
@Controller
@RequestMapping("/webapi/faultTranslateLibrary")
public class FaultTranslateLibraryController extends BaseController {

	@Autowired
	private FaultTranslateLibraryLogic faultTranslateLibraryLogic;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障翻译库", methods = "信息保存")
	public void save(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		faultTranslationLibrary.setOemCode(getUserInfo(request).get(OEM_CODE));
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.ADMIN_TYPE)) {
			faultTranslationLibrary.setType(CommonUtils.FAULT_TRANS_GENERAL);
		} else {
			faultTranslationLibrary.setType(CommonUtils.FAULT_TRANS_CUSTOMER);
		}
		int result = faultTranslateLibraryLogic.save(faultTranslationLibrary);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障翻译库", methods = "添加其他语言")
	public void saveOtherLanguage(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		// 获取oemCode
		faultTranslationLibrary.setLanguage(faultTranslationLibrary.getOtherLanguage());
		int result = faultTranslateLibraryLogic.saveOtherLanguage(faultTranslationLibrary);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障翻译库", methods = "更新其他语言")
	public void updateOtherLanguage(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		// 获取oemCode
		faultTranslationLibrary.setLanguage(faultTranslationLibrary.getOtherLanguage());
		int result = faultTranslateLibraryLogic.updateOtherLanguage(faultTranslationLibrary);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设故障翻译库管理", methods = "信息删除")
	public void delete(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response, Model model)
			throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = faultTranslateLibraryLogic.delete(faultTranslationLibrary);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设故障翻译库管理", methods = "信息修改")
	public void update(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response, Model model)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = faultTranslateLibraryLogic.update(faultTranslationLibrary);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * 故障翻译库列表（带分页）
	 * 
	 * @param index
	 * @param size
	 * @param faultTranLib
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "故障翻译库", methods = "故障翻译库列表")
	public void list(int index, int size, FaultTranslationLibraries faultTranLib, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		faultTranLib.setOemCode(getUserInfo(request).get(OEM_CODE));
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.ADMIN_TYPE)) {
			faultTranLib.setType(CommonUtils.FAULT_TRANS_GENERAL);
		} else if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.OEM_TYPE)) {
			faultTranLib.setType(CommonUtils.FAULT_TRANS_CUSTOMER);
		}
		Page<FaultTranslationLibraries> faultTranslationLibrary = faultTranslateLibraryLogic.selectListPage(index, size,
				faultTranLib, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (faultTranslationLibrary != null) {
			map.put("total", faultTranslationLibrary.getTotal());
			map.put("list", faultTranslationLibrary);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 故障翻译库列表（无分页）
	 * 
	 * @param faultTranLib
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/listNoPage", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "故障翻译库", methods = "故障翻译库列表")
	public void listNoPage(FaultTranslationLibraries faultTranLib, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		faultTranLib.setOemCode(getUserInfo(request).get(OEM_CODE));
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.ADMIN_TYPE)) {
			faultTranLib.setType(CommonUtils.FAULT_TRANS_GENERAL);
		} else if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.OEM_TYPE)) {
			faultTranLib.setType(CommonUtils.FAULT_TRANS_CUSTOMER);
		}
		List<FaultTranslationLibraries> faultTranslationLibrary = faultTranslateLibraryLogic.selectList(faultTranLib,
				true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (faultTranslationLibrary != null) {
			map.put("list", faultTranslationLibrary);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(FaultTranslationLibraries faultTranslationLibrary, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		if (faultTranslationLibrary.getOtherLanguage() != null && faultTranslationLibrary.getOtherLanguage() != "") {
			faultTranslationLibrary.setLanguage(faultTranslationLibrary.getOtherLanguage());
		}
		faultTranslationLibrary.setOemCode(getUserInfo(request).get(OEM_CODE));
		FaultTranslationLibraries result = faultTranslateLibraryLogic.select(faultTranslationLibrary);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(FaultTranslationLibraries faultTranslationLibraries, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		if (faultTranslationLibraries.getType() == CommonUtils.FAULT_TRANS_CUSTOMER) {
			faultTranslationLibraries.setOemCode(getUserInfo(request).get(OEM_CODE));
		}
		List<FaultTranslationLibraries> ftl = faultTranslateLibraryLogic.listByCode(faultTranslationLibraries);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (ftl != null) {
			map.put("list", ftl);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
