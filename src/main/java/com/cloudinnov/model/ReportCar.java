package com.cloudinnov.model;

public class ReportCar{
    private int code;
    private String equipmentCode;
    private int carTotal;
    private String utcTime;
    private int firstLaneOneKind;
    private int firstLaneTwoKind;
    private int firstLaneThreeKind;
    private int firstLaneFourKind;
    private int firstLaneAverageSpeed;
    private int firstLaneOccuPancy;
    private int firstLaneFlow;
    
    private int secondLaneOneKind;
    private int secondLaneTwoKind;
    private int secondLaneThreeKind;
    private int secondLaneFourKind;
    private int secondLaneAverageSpeed;
    private int secondLaneOccuPancy;
    private int secondLaneFlow;
    
    private int thirdLaneOneKind;
    private int thirdLaneTwoKind;
    private int thirdLaneThreeKind;
    private int thirdLaneFourKind;
    private int thirdLaneAverageSpeed;
    private int thirdLaneOccuPancy;
    private int thirdLaneFlow;
    
    private int forthLaneOneKind;
    private int forthLaneTwoKind;
    private int forthLaneThreeKind;
    private int forthLaneFourKind;
    private int forthLaneAverageSpeed;
    private int forthLaneOccuPancy;
    private int forthLaneFlow;
    
    private int fifthLaneOneKind;
    private int fifthLaneTwoKind;
    private int fifthLaneThreeKind;
    private int fifthLaneFourKind;
    private int fifthLaneAverageSpeed;
    private int fifthLaneOccuPancy;
    private int fifthLaneFlow;
    
    private int sixthLaneOneKind;
    private int sixthLaneTwoKind;
    private int sixthLaneThreeKind;
    private int sixthLaneFourKind;
    private int sixthLaneAverageSpeed;
    private int sixthLaneOccuPancy;
    private int sixthLaneFlow;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public int getCarTotal() {
		return carTotal;
	}
	public void setCarTotal(int carTotal) {
		this.carTotal = carTotal;
	}
	public String getUtcTime() {
		return utcTime;
	}
	public void setUtcTime(String utcTime) {
		this.utcTime = utcTime;
	}
	public int getFirstLaneOneKind() {
		return firstLaneOneKind;
	}
	public void setFirstLaneOneKind(int firstLaneOneKind) {
		this.firstLaneOneKind = firstLaneOneKind;
	}
	public int getFirstLaneTwoKind() {
		return firstLaneTwoKind;
	}
	public void setFirstLaneTwoKind(int firstLaneTwoKind) {
		this.firstLaneTwoKind = firstLaneTwoKind;
	}
	public int getFirstLaneThreeKind() {
		return firstLaneThreeKind;
	}
	public void setFirstLaneThreeKind(int firstLaneThreeKind) {
		this.firstLaneThreeKind = firstLaneThreeKind;
	}
	public int getFirstLaneFourKind() {
		return firstLaneFourKind;
	}
	public void setFirstLaneFourKind(int firstLaneFourKind) {
		this.firstLaneFourKind = firstLaneFourKind;
	}
	public int getFirstLaneAverageSpeed() {
		return firstLaneAverageSpeed;
	}
	public void setFirstLaneAverageSpeed(int firstLaneAverageSpeed) {
		this.firstLaneAverageSpeed = firstLaneAverageSpeed;
	}
	public int getFirstLaneOccuPancy() {
		return firstLaneOccuPancy;
	}
	public void setFirstLaneOccuPancy(int firstLaneOccuPancy) {
		this.firstLaneOccuPancy = firstLaneOccuPancy;
	}
	public int getFirstLaneFlow() {
		return firstLaneFlow;
	}
	public void setFirstLaneFlow(int firstLaneFlow) {
		this.firstLaneFlow = firstLaneFlow;
	}
	public int getSecondLaneOneKind() {
		return secondLaneOneKind;
	}
	public void setSecondLaneOneKind(int secondLaneOneKind) {
		this.secondLaneOneKind = secondLaneOneKind;
	}
	public int getSecondLaneTwoKind() {
		return secondLaneTwoKind;
	}
	public void setSecondLaneTwoKind(int secondLaneTwoKind) {
		this.secondLaneTwoKind = secondLaneTwoKind;
	}
	public int getSecondLaneThreeKind() {
		return secondLaneThreeKind;
	}
	public void setSecondLaneThreeKind(int secondLaneThreeKind) {
		this.secondLaneThreeKind = secondLaneThreeKind;
	}
	public int getSecondLaneFourKind() {
		return secondLaneFourKind;
	}
	public void setSecondLaneFourKind(int secondLaneFourKind) {
		this.secondLaneFourKind = secondLaneFourKind;
	}
	public int getSecondLaneAverageSpeed() {
		return secondLaneAverageSpeed;
	}
	public void setSecondLaneAverageSpeed(int secondLaneAverageSpeed) {
		this.secondLaneAverageSpeed = secondLaneAverageSpeed;
	}
	public int getSecondLaneOccuPancy() {
		return secondLaneOccuPancy;
	}
	public void setSecondLaneOccuPancy(int secondLaneOccuPancy) {
		this.secondLaneOccuPancy = secondLaneOccuPancy;
	}
	public int getSecondLaneFlow() {
		return secondLaneFlow;
	}
	public void setSecondLaneFlow(int secondLaneFlow) {
		this.secondLaneFlow = secondLaneFlow;
	}
	public int getThirdLaneOneKind() {
		return thirdLaneOneKind;
	}
	public void setThirdLaneOneKind(int thirdLaneOneKind) {
		this.thirdLaneOneKind = thirdLaneOneKind;
	}
	public int getThirdLaneTwoKind() {
		return thirdLaneTwoKind;
	}
	public void setThirdLaneTwoKind(int thirdLaneTwoKind) {
		this.thirdLaneTwoKind = thirdLaneTwoKind;
	}
	public int getThirdLaneThreeKind() {
		return thirdLaneThreeKind;
	}
	public void setThirdLaneThreeKind(int thirdLaneThreeKind) {
		this.thirdLaneThreeKind = thirdLaneThreeKind;
	}
	public int getThirdLaneFourKind() {
		return thirdLaneFourKind;
	}
	public void setThirdLaneFourKind(int thirdLaneFourKind) {
		this.thirdLaneFourKind = thirdLaneFourKind;
	}
	public int getThirdLaneAverageSpeed() {
		return thirdLaneAverageSpeed;
	}
	public void setThirdLaneAverageSpeed(int thirdLaneAverageSpeed) {
		this.thirdLaneAverageSpeed = thirdLaneAverageSpeed;
	}
	public int getThirdLaneOccuPancy() {
		return thirdLaneOccuPancy;
	}
	public void setThirdLaneOccuPancy(int thirdLaneOccuPancy) {
		this.thirdLaneOccuPancy = thirdLaneOccuPancy;
	}
	public int getThirdLaneFlow() {
		return thirdLaneFlow;
	}
	public void setThirdLaneFlow(int thirdLaneFlow) {
		this.thirdLaneFlow = thirdLaneFlow;
	}
	public int getForthLaneOneKind() {
		return forthLaneOneKind;
	}
	public void setForthLaneOneKind(int forthLaneOneKind) {
		this.forthLaneOneKind = forthLaneOneKind;
	}
	public int getForthLaneTwoKind() {
		return forthLaneTwoKind;
	}
	public void setForthLaneTwoKind(int forthLaneTwoKind) {
		this.forthLaneTwoKind = forthLaneTwoKind;
	}
	public int getForthLaneThreeKind() {
		return forthLaneThreeKind;
	}
	public void setForthLaneThreeKind(int forthLaneThreeKind) {
		this.forthLaneThreeKind = forthLaneThreeKind;
	}
	public int getForthLaneFourKind() {
		return forthLaneFourKind;
	}
	public void setForthLaneFourKind(int forthLaneFourKind) {
		this.forthLaneFourKind = forthLaneFourKind;
	}
	public int getForthLaneAverageSpeed() {
		return forthLaneAverageSpeed;
	}
	public void setForthLaneAverageSpeed(int forthLaneAverageSpeed) {
		this.forthLaneAverageSpeed = forthLaneAverageSpeed;
	}
	public int getForthLaneOccuPancy() {
		return forthLaneOccuPancy;
	}
	public void setForthLaneOccuPancy(int forthLaneOccuPancy) {
		this.forthLaneOccuPancy = forthLaneOccuPancy;
	}
	public int getForthLaneFlow() {
		return forthLaneFlow;
	}
	public void setForthLaneFlow(int forthLaneFlow) {
		this.forthLaneFlow = forthLaneFlow;
	}
	public int getFifthLaneOneKind() {
		return fifthLaneOneKind;
	}
	public void setFifthLaneOneKind(int fifthLaneOneKind) {
		this.fifthLaneOneKind = fifthLaneOneKind;
	}
	public int getFifthLaneTwoKind() {
		return fifthLaneTwoKind;
	}
	public void setFifthLaneTwoKind(int fifthLaneTwoKind) {
		this.fifthLaneTwoKind = fifthLaneTwoKind;
	}
	public int getFifthLaneThreeKind() {
		return fifthLaneThreeKind;
	}
	public void setFifthLaneThreeKind(int fifthLaneThreeKind) {
		this.fifthLaneThreeKind = fifthLaneThreeKind;
	}
	public int getFifthLaneFourKind() {
		return fifthLaneFourKind;
	}
	public void setFifthLaneFourKind(int fifthLaneFourKind) {
		this.fifthLaneFourKind = fifthLaneFourKind;
	}
	public int getFifthLaneAverageSpeed() {
		return fifthLaneAverageSpeed;
	}
	public void setFifthLaneAverageSpeed(int fifthLaneAverageSpeed) {
		this.fifthLaneAverageSpeed = fifthLaneAverageSpeed;
	}
	public int getFifthLaneOccuPancy() {
		return fifthLaneOccuPancy;
	}
	public void setFifthLaneOccuPancy(int fifthLaneOccuPancy) {
		this.fifthLaneOccuPancy = fifthLaneOccuPancy;
	}
	public int getFifthLaneFlow() {
		return fifthLaneFlow;
	}
	public void setFifthLaneFlow(int fifthLaneFlow) {
		this.fifthLaneFlow = fifthLaneFlow;
	}
	public int getSixthLaneOneKind() {
		return sixthLaneOneKind;
	}
	public void setSixthLaneOneKind(int sixthLaneOneKind) {
		this.sixthLaneOneKind = sixthLaneOneKind;
	}
	public int getSixthLaneTwoKind() {
		return sixthLaneTwoKind;
	}
	public void setSixthLaneTwoKind(int sixthLaneTwoKind) {
		this.sixthLaneTwoKind = sixthLaneTwoKind;
	}
	public int getSixthLaneThreeKind() {
		return sixthLaneThreeKind;
	}
	public void setSixthLaneThreeKind(int sixthLaneThreeKind) {
		this.sixthLaneThreeKind = sixthLaneThreeKind;
	}
	public int getSixthLaneFourKind() {
		return sixthLaneFourKind;
	}
	public void setSixthLaneFourKind(int sixthLaneFourKind) {
		this.sixthLaneFourKind = sixthLaneFourKind;
	}
	public int getSixthLaneAverageSpeed() {
		return sixthLaneAverageSpeed;
	}
	public void setSixthLaneAverageSpeed(int sixthLaneAverageSpeed) {
		this.sixthLaneAverageSpeed = sixthLaneAverageSpeed;
	}
	public int getSixthLaneOccuPancy() {
		return sixthLaneOccuPancy;
	}
	public void setSixthLaneOccuPancy(int sixthLaneOccuPancy) {
		this.sixthLaneOccuPancy = sixthLaneOccuPancy;
	}
	public int getSixthLaneFlow() {
		return sixthLaneFlow;
	}
	public void setSixthLaneFlow(int sixthLaneFlow) {
		this.sixthLaneFlow = sixthLaneFlow;
	}
}