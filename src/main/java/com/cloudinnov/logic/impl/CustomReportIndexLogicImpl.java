package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.CustomReportIndexLogic;
import com.cloudinnov.model.CustomReportIndex;

@Service
public class CustomReportIndexLogicImpl extends BaseLogicImpl<CustomReportIndex> implements CustomReportIndexLogic {

}
