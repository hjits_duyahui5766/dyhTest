package com.cloudinnov.logic;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cloudinnov.model.EquipmentPoints;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月17日下午5:23:23
 * @email chaoguo@cloudinnov.com
 * @remark 设备点位service接口
 * @version
 */
public interface EquipmentPointLogic extends IBaseLogic<EquipmentPoints> {
	List<EquipmentPoints> selectSimulationAllPoint();
	public List<EquipmentPoints> listByCode(String[] codes, String language);
	public int saveOtherLanguage(EquipmentPoints equipmentPoints);
	public Page<EquipmentPoints> selectProductionLineListPage(int index, int size, EquipmentPoints equipmentPoint,
			boolean order);
	public int updateOtherLanguage(EquipmentPoints equipmentPoints);
	public List<EquipmentPoints> monitorEqusPoints(String[] codes, String language);
	/**
	 * 查询所有产线上的产能点位 isProductivityPoint
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @return 参数
	 * @return List<EquipmentPoints> 返回类型
	 */
	public List<EquipmentPoints> selectProductivityPoint();
	/**
	 * 点位导入 pointImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int pointImport(String data, EquipmentPoints equipmentPoints);
	/**
	 * 点位导入 pointImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public void pointExport(EquipmentPoints equipmentPoints, HttpServletResponse response);
	/**
	 * 通过设备附加属性查询所有设备状态
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int checkEquipmentsState(String customTag);
	/**
	 * 根据指标类型获取点位列表
	 * @Title: selectListByIndexType
	 * @Description: TODO
	 * @param equipmentsPoints
	 * @return
	 * @return: List<EquipmentPoints>
	 */
	public List<EquipmentPoints> selectListByIndexType(EquipmentPoints equipmentsPoints);
}
