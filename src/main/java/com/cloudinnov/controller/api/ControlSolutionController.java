package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.ibatis.annotations.Param;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionExpression;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;

/**
 * @author GuoBo
 */
@Controller
@RequestMapping("/webapi/controlSolution")
public class ControlSolutionController extends BaseController {
	@Autowired
	private ControlSolutionLogic controlSolutionLogic;
	@Autowired
	private EquipmentsLogic equipmentsLogic;

	/* 标记 */
	/**
	 * 群控方案添加
	 * 
	 * @param controlSolution
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案添加")
	public void save(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.saveControlSolutionConfig(controlSolution);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 群控方案添加
	 * 
	 * @param controlSolution
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/board/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案添加")
	public void saveBoard(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.saveBoardSolutionConfig(controlSolution);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 群控方案删除
	 * 
	 * @param controlSolution
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案删除")
	public void delete(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = controlSolutionLogic.delete(controlSolution);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 情报板方案删除
	 * 
	 * @param controlSolution
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/board/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案删除")
	public void boardDelete(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = controlSolutionLogic.deleteBoardSolutionConfig(controlSolution);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 群控方案修改
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案修改")
	public void update(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.update(controlSolution);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 情报板方案修改
	 * 
	 * @param tigger
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/board/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案修改")
	public void boardUpdate(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.updateBoardSolutionConfig(controlSolution);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 群控方案列表（带分页）
	 * 
	 * @param controlSolution
	 * @param size
	 * @param index
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案列表")
	public void list(ControlSolution controlSolution, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		// 修改字符编码
		String parameter = request.getParameter("name");
		String name = new String(parameter.getBytes("iso-8859-1"), "utf-8");
		controlSolution.setName(name);

		Page<ControlSolution> controlSolutions = controlSolutionLogic.selectListPage(index, size, controlSolution,
				true);
		if (controlSolutions != null) {
			map.put("list", controlSolutions);
			map.put("total", controlSolutions.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 获取触发器信息（返回单条数据）
	 * 
	 * @param controlSolution
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案信息（单条）")
	public void select(ControlSolution controlSolution, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		ControlSolution controlSolutions = controlSolutionLogic.select(controlSolution);
		if (controlSolutions != null) {
			map.put("model", controlSolutions);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 群控方案查询---单条 selectControlSolution
	 * 
	 * @Description
	 * @param
	 * @return 返回前端需要的群控方案（触发和恢复方案）和表达式
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectSingleCtrlSolution", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案查询--单条")
	public void selectSingleCtrlSolution(@Valid ControlSolution controlSulotion, BindingResult error,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		ControlSolution solution = controlSolutionLogic.selectControlSolutionAllKind(controlSulotion);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (solution != null) {
			map.put("model", solution);
			String jsonString = JSON.toJSONString(solution);
			Object json = JSON.toJSON(solution);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			// returnData = returnJsonAllRequest(request, response, "", ERROR, "");
			response.getWriter().print("");
		}
		// response.getWriter().print(returnData);
	}

	/**
	 * 群控方案保存---单条 saveSingleCtrlSolution
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/saveSingleCtrlSolution", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案添加--单条")
	public void saveSingleCtrlSolution(ControlSolution controlSolution, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.saveControlSolutionExpression(controlSolution);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 群控方案保存---单条 updateSingleCtrlSolution
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateSingleCtrlSolution", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案添加--单条")
	public void updateSingleCtrlSolution(ControlSolution controlSulotion, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = controlSolutionLogic.updateSolutionConfigExpression(controlSulotion);
		// controlSolutionLogic.updateControlSolutionExpression(controlExpression);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 获取设备信息--无分页 selectEquipment
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectQuipmentsInfo", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "设备信息查询")
	public void selectEquipment(Equipments equipments, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<Equipments> list = equipmentsLogic.selectEquipmentsInfo(index, size, equipments, true);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (list.size() != 0) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 群控方案查询---单条 selectControlSolution
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectControlSolution", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案查询--单条")
	public void selectControlSolution(@Valid ControlSolution controlSulotion, BindingResult error,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		ControlSolution solution = controlSolutionLogic.select(controlSulotion);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (solution != null) {
			map.put("model", solution);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 查看群控方案的详情 selectControlSolutionInfo
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectControlSolutionInfo")
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "群控方案详情查询")
	public void selectControlSolutionInfo(ControlSolution controlSulotion, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> mapControl = controlSolutionLogic.selectControlSolutionMap(controlSulotion);
		String returnData = returnJsonAllRequest(request, response, mapControl, SUCCESS, "");
		response.getWriter().print(returnData);
	}

	/**
	 * 查询情报板详情
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectBoardSolutionInfo")
	@ResponseBody
	public void selectBoardSolution(ControlSolution controlSulotion, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> mapControl = controlSolutionLogic.selectBoardSolution(controlSulotion);
		String returnData = returnJsonAllRequest(request, response, mapControl, SUCCESS, "");
		response.getWriter().print(returnData);
	}

	/**
	 * 情报板添加--设备查询 informationsave
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectInfoQuipments", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "情报板管理", methods = "情报板添加--设备查询")
	public void selectInfoQuipments(Equipments equipments, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<Equipments> InfoQuipments = equipmentsLogic.selectInfoQuipments(equipments, index, size);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (InfoQuipments.size() != 0) {
			map.put("InfoQuipments", InfoQuipments);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/***
	 * 情报板查询--分页 selectInformationBoard
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectInformationBoard", method = RequestMethod.GET)
	@ResponseBody
	public void selectInformationBoard(ControlSolution model, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<ControlSolution> pageInfo = controlSolutionLogic.selectInformationBoard(index, size, model);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (pageInfo != null) {
			map.put("list", pageInfo);
			map.put("total", pageInfo.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 根据设备分类code查询方案 selectInforBoardByEquiCode
	 * 
	 * @Description
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/selectControlSolutionByEquiCode", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "方案管理", methods = "根据设备分类code查询方案")
	public void selectInforBoardByEquiCode(ControlSolution controlSulotion, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<ControlSolution> conInfo = controlSolutionLogic.selectControlSolutionByEquiCode(controlSulotion);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (conInfo.size() != 0) {
			map.put("model", conInfo);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 修改方案开关
	 * 
	 * @param code
	 * @param onOff
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateControOnOff", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "方案管理", methods = "修改方案开关")
	public void updateControOnOff(String code, Integer onOff, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = controlSolutionLogic.updateControOnOff(code, onOff);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/sendHistory", method = RequestMethod.GET)
	public void selectBoardHistory(HttpServletRequest request, HttpServletResponse response, String equCateCode)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<ControlSolutionSendRecordItem> list = controlSolutionLogic.selectHistoryListByEquCateCode(equCateCode);
		map.put("list", list);
		returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		response.getWriter().println(returnData);
	}

	/**
	 * 强制触发触控方案
	 * 
	 * @param code
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/sendControllerSolution", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "强制触发群控方案")
	public void sendControllerSolution(String code, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		int result = controlSolutionLogic.sendControllerSolution(code);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 强制触发或者恢复触控方案
	 * 
	 * @param code,isRecover
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/sendForceControllerSolution", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "群控方案管理", methods = "强制触发或者恢复群控方案")
	public void sendForceControllerSolution(String code, int isRecover, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		int result = controlSolutionLogic.sendControllerSolutionRecover(code, isRecover);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/***
	 * 情报板查询 selectPredictionBoard
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectPredictionBoard", method = RequestMethod.GET)
	@ResponseBody
	public void selectPredictionBoard(ControlSolution model, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = controlSolutionLogic.selectPredictionBoard(model);
	
		String returnData = "";
		if (JudgeNullUtil.iMap(map)) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
