package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.EquipmentsCategories;

/**
 * @author guochao
 * @date 2016年2月17日下午5:51:43
 * @email chaoguo@cloudinnov.com
 * @remark 设备分类dao接口
 * @version
 */
public interface EquipmentsCategoriesDao extends IBaseDao<EquipmentsCategories> {
	public int insertInfo(EquipmentsCategories equipmentsCategories);
	public List<EquipmentsCategories> tableList(Map<String, Object> map);
	public int updateInfoByCondition(EquipmentsCategories equipmentsCategories);
	public EquipmentsCategories selectEntityByParentId(EquipmentsCategories equipmentsCategories);
	List<EquipmentsCategories> selectListByParentId(@Param("parentId") Integer parentId);
}
