package com.cloudinnov.model;

import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class EquipmentArea extends BaseModel{

	private String cameraCode;
	private String equipmentCode;
	private String sectionCode;
	private Integer status; //设备是否有效标识:1,有效;9,无效
	private String createTime;
	private String comment;
	private List<Equipments> triggerListEquipment; //摄像头区域下的触发设备集合
	private String triggerEquipments; //摄像头区域下的触发设备集合
	private String sectionName; //隧道名称
	private String cameraName; //隧道名称
	private String pileRange; //监控区域大小
	private String areaId; //监控区域所在的事故区域
	
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getPileRange() {
		return pileRange;
	}
	public void setPileRange(String pileRange) {
		this.pileRange = pileRange;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public List<Equipments> getTriggerListEquipment() {
		return triggerListEquipment;
	}
	@JSONField(serialize = false)
	public void setTriggerListEquipment(List<Equipments> triggerListEquipment) {
		this.triggerListEquipment = triggerListEquipment;
	}
	public String getTriggerEquipments() {
		return triggerEquipments;
	}
	public void setTriggerEquipments(String triggerEquipments) {
		this.triggerEquipments = triggerEquipments;
	}
	public String getCameraCode() {
		return cameraCode;
	}
	public void setCameraCode(String cameraCode) {
		this.cameraCode = cameraCode;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
