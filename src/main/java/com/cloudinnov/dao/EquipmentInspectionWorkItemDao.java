package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkItem;

public interface EquipmentInspectionWorkItemDao extends IBaseDao<EquipmentInspectionWorkItem> {

	int insertByList(List<EquipmentInspectionWorkItem> list);

	int insertConfigItems(List<EquipmentInspectionWorkItem> list);

	List<EquipmentInspectionWorkItem> selectByConfigCode(EquipmentInspectionConfig record);

	public int deleteRelationByConfigCode(String configCode);
}