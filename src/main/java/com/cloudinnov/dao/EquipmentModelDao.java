package com.cloudinnov.dao;

import com.cloudinnov.model.EquipmentModel;

public interface EquipmentModelDao extends IBaseDao<EquipmentModel> {
public int insertInfo(EquipmentModel equipmentModel);
	
	public int updateInfoByCondition(EquipmentModel equipmentModel);
}