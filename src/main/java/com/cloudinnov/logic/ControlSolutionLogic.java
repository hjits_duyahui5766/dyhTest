package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionExpre;
import com.cloudinnov.model.ControlSolutionExpression;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.model.PredictionContacter;
import com.github.pagehelper.Page;

public interface ControlSolutionLogic extends IBaseLogic<ControlSolution> {
	Map<String, Object> selectControlSolutionMap(ControlSolution controlSulotion);

	Page<ControlSolution> selectInformationBoard(int index, int size, ControlSolution model);

	List<ControlSolution> selectControlSolutionByEquiCode(ControlSolution controlSulotion);

	int updateControOnOff(String code, Integer onOff);

	List<ControlSolutionSendRecordItem> selectHistoryListByEquCateCode(String equCateCode);

	/**
	 * 添加情报板方案配置
	 * 
	 * @Title: saveBoardSolutionConfig
	 * @Description: TODO
	 * @param controlSolution
	 * @return
	 * @return: int
	 */
	int saveBoardSolutionConfig(ControlSolution controlSolution);

	/**
	 * 修改情报板方案配置
	 * 
	 * @Title: updateBoardSolutionConfig
	 * @Description: TODO
	 * @param controlSolution
	 * @return
	 * @return: int
	 */
	int updateBoardSolutionConfig(ControlSolution controlSolution);

	/**
	 * 删除情报板方案
	 * 
	 * @Title: deleteBoardSolutionConfig
	 * @Description: TODO
	 * @param controlSolution
	 * @return
	 * @return: int
	 */
	int deleteBoardSolutionConfig(ControlSolution controlSolution);

	/**
	 * 添加其他设备方案配置
	 * 
	 * @Title: saveControlSolutionConfig
	 * @Description: TODO
	 * @param controlSolution
	 * @return
	 * @return: int
	 */
	int saveControlSolutionConfig(ControlSolution controlSolution);

	Map<String, Object> selectBoardSolution(ControlSolution controlSolution);

	List<ControlSolution> selectControlSolutions(String objectCode);

	int sendControllerSolution(String code);

	int sendControllerSolutionRecover(String code, int isRecover);

	/**
	 * 跟怒故障码查询
	 * 
	 * @param faultCode
	 * @return
	 */
	List<ControlSolution> selectControlSolutionByFaultCode(String triggerCodes);

	/**
	 * 群控定时扫描
	 */
	public List<ControlSolutionExpre> selectScheduleExpression(@Param("isRecover") Integer isRecover);

	/**
	 * 表达式的增删改查
	 */
	ControlSolution selectSolutionExpression(ControlSolution controlSolution, int isRecover);

	int saveControlSolutionExpression(ControlSolution controlSolution);

//	int updateControlSolutionExpression(ControlSolutionExpression controlSolutionExpression);
	int updateSolutionConfigExpression(ControlSolution controlSolution);

//	ControlSolutionExpression selectControlSolutionExpreByRecover(String solutionCode, int isRecover);
	ControlSolutionExpre selectControlSolutionExpreByRecover(String solutionCode, int isRecover);

	ControlSolution selectControlSolutionAllKind(ControlSolution controlSolution);

	// 判断火灾异常等级后,要获取的数据
//	FireCRTEvent selectFireCRTEventByCode(@Param("code") String code, @Param("isRecover") Integer isRecover);
	FireCRTEvent selectFireCRTEventByCode(@Param("code") String code, @Param("pointCode") String pointCode,
			@Param("isRecover") Integer isRecover);

	//查询预案添加中的情报板信息
	Map<String, Object> selectPredictionBoard(ControlSolution controlSolution);
}
