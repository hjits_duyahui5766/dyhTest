package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthUsers;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年2月17日下午6:58:35
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface AuthUserLogic extends IBaseLogic<AuthUsers> {

	/**
	 * 用户登录
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            user
	 * @param @return
	 *            参数
	 * @return boolean true表示用户名密码正确
	 */
	public AuthUsers login(AuthUsers user);

	/**
	 * 查询用户的资源信息
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            userCode
	 * @param @param
	 *            companyCode
	 * @param @return
	 *            参数
	 * @return List<AuthResources> 返回类型
	 */
	public List<AuthResource> selectResourceByUserCodeAndCompanyCode(String userCode, String companyCode);

	/**
	 * selectOemUserList
	 * 
	 * @Description: 查找oem联系人 分页
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	public Page<AuthUsers> selectOemUserList(int index, int size, Map<String, Object> map);

	/**
	 * selectOemUserList
	 * 
	 * @Description: 查找oem联系人
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	public List<AuthUsers> selectOemUserList(Map<String, Object> map);

	/**
	 * selectAgentUserList
	 * 
	 * @Description: 查找渠道联系人
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	// public List<AuthUsers> selectAgentUserList();

	/**
	 * selectCustomerUserList
	 * 
	 * @Description: 查找客户联系人
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	public List<AuthUsers> selectCustomerUserList(Map<String, Object> map);

	public int updateLanguage(AuthUsers user);

	public int changePassword(AuthUsers user);
	
	public int updateUserInfo(AuthUsers user);
	
	/**
	 * @param user
	 * @return 1 表示存在 0 不存在
	 */
	public int checkLoginNameOrPhone(AuthUsers user);
	
	/**
	 * @param user
	 * @return 1 表示存在 0 不存在
	 */
	public int checkLoginPassword(AuthUsers user);
	
	/** 判断原设备是否下线
	* androidLoginCheck 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param user
	* @param @return    参数 0 首次登陆或和原设备ID一致  1原设备下线
	* @return int    返回类型 
	*/
	public int androidLoginCheck(AuthUsers user);
	
	public int checkVerifyCode(String type,AuthUsers user);
	
	/** 通过手机号查询出所对应的区号+手机号
	* selectAreaAndPhone 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    参数
	* @return List<AuthUsers>    返回类型 
	*/
	public AuthUsers selectAreaAndPhone(String phone);
	
	public int resetPassword(AuthUsers user);
}
