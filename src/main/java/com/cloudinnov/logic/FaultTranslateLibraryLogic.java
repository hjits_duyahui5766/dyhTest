package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.FaultTranslationLibraries;

/**
 * @author guochao
 * @date 2016年3月1日下午5:47:14
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public interface FaultTranslateLibraryLogic extends
		IBaseLogic<FaultTranslationLibraries> {

	public int saveOtherLanguage(
			FaultTranslationLibraries faultTranslationLibrary);

	public int updateOtherLanguage(FaultTranslationLibraries faultTranslationLibrary);
	
	public List<FaultTranslationLibraries> listByCode(FaultTranslationLibraries faultTranslationLibrary);
}
