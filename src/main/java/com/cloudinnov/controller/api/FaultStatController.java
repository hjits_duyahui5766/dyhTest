package com.cloudinnov.controller.api; 

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.FaultsLogic;
import com.cloudinnov.logic.impl.FaultsLogicImpl;
import com.cloudinnov.model.FaultStat;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年4月6日上午10:18:19
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Controller
@RequestMapping("/webapi/faultstat")
public class FaultStatController extends BaseController {
	
	private static final String SPLIT_LEVLER = ",";
	@Autowired
	private FaultsLogic faultsLogic;
	
	/** 工单统计Detail
	* select 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param request
	* @param @param response
	* @param @param model
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping("/detailstat")
	@ResponseBody
	public void selectDetailStat(String index,String size,String equCode,String customerCode,String lineCode,String language,String startTime,String endTime,HttpServletRequest request,
			HttpServletResponse response)
			throws IOException {	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("index", index);	
		map.put("size", size);	
		map.put("language", language);
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		if(equCode!=null&&!equCode.equals("")){
			map.put("equCode", Arrays.asList(equCode.split(SPLIT_LEVLER)));
		}else if(lineCode!=null&&!lineCode.equals("")){
			map.put("lineCode", Arrays.asList(lineCode.split(SPLIT_LEVLER)));
		}else if(getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)){
			map.put("customerCode", Arrays.asList(getUserInfo(request).get(COM_CODE).split(SPLIT_LEVLER)));
		}else if(customerCode!=null&&!customerCode.equals("")){
			map.put("customerCode", Arrays.asList(customerCode.split(SPLIT_LEVLER)));
		}	
		Page<FaultStat> result = faultsLogic.selectFaultStatsDetailByCondition(map);
		map = new HashMap<String, Object>();
		if(result!=null){	
			map.put("list", result);
			map.put("total", result.getTotal());
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, null);
			response.getWriter().print(returnData);
		}else{
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, null);
			response.getWriter().print(returnData);
		}
	}
	
	/** 工单统计 按天
	* select 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param request
	* @param @param response
	* @param @param model
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping("/daystat")
	@ResponseBody
	public void selectDayStat(String equCode,String customerCode,String lineCode,String startTime,String endTime,String language,HttpServletRequest request,
			HttpServletResponse response)
			throws IOException {	
		Map<String, Object> map = new HashMap<String, Object>();
		if(equCode!=null&&!equCode.equals("")){
			map.put("equCode", Arrays.asList(equCode.split(SPLIT_LEVLER)));
		}else if(lineCode!=null&&!lineCode.equals("")){
			map.put("lineCode", Arrays.asList(lineCode.split(SPLIT_LEVLER)));
		}else if(getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)){
			map.put("customerCode", Arrays.asList(getUserInfo(request).get(COM_CODE).split(SPLIT_LEVLER)));
		}else if(customerCode!=null&&!customerCode.equals("")){
			map.put("customerCode", Arrays.asList(customerCode.split(SPLIT_LEVLER)));
		}	
		map.put(FaultsLogicImpl.QUERY_TYPE, FaultsLogicImpl.QUERY_TYPE_DAY);
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		map.put("language", language);
		List<FaultStat> result = faultsLogic.selectFaultStatsByCondition(map);
		map = new HashMap<String, Object>();
		if(result!=null){
			map.put("list", result);
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, null);
			response.getWriter().print(returnData);
		}else{
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, null);
			response.getWriter().print(returnData);
		}
	}
	
	/** 工单统计 按周
	* select 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param request
	* @param @param response
	* @param @param model
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping("/weekstat")
	@ResponseBody
	public void selectWeekStat(String startYear,String startWeek,String endYear,String endWeek,String equCode,String customerCode,String lineCode,String language,HttpServletRequest request,
			HttpServletResponse response)
			throws IOException {	
		Map<String, Object> map = new HashMap<String, Object>();
		if(equCode!=null&&!equCode.equals("")){
			map.put("equCode", Arrays.asList(equCode.split(SPLIT_LEVLER)));
		}else if(lineCode!=null&&!lineCode.equals("")){
			map.put("lineCode", Arrays.asList(lineCode.split(SPLIT_LEVLER)));
		}else if(getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)){
			map.put("customerCode", Arrays.asList(getUserInfo(request).get(COM_CODE).split(SPLIT_LEVLER)));
		}else if(customerCode!=null&&!customerCode.equals("")){
			map.put("customerCode", Arrays.asList(customerCode.split(SPLIT_LEVLER)));
		}
		map.put(FaultsLogicImpl.QUERY_TYPE, FaultsLogicImpl.QUERY_TYPE_WEEK);
		map.put("startYear", startYear);
		map.put("startWeek", startWeek);
		map.put("endYear", endYear);
		map.put("endWeek", endWeek);
		map.put("language", language);
		List<FaultStat> result = faultsLogic.selectFaultStatsByCondition(map);
		map = new HashMap<String, Object>();
		if(result!=null){
			map.put("list", result);
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, null);
			response.getWriter().print(returnData);
		}else{
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, null);
			response.getWriter().print(returnData);
		}
	}
	
	/** 工单统计 按月
	* select 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param request
	* @param @param response
	* @param @param model
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping("/monthstat")
	@ResponseBody
	public void selectMonthStat(String startYear,String startMonth,String endYear,String endMonth,String equCode,String customerCode,String lineCode,String language,HttpServletRequest request,
			HttpServletResponse response)
			throws IOException {	
		Map<String, Object> map = new HashMap<String, Object>();
		if(equCode!=null&&!equCode.equals("")){
			map.put("equCode", Arrays.asList(equCode.split(SPLIT_LEVLER)));
		}else if(lineCode!=null&&!lineCode.equals("")){
			map.put("lineCode", Arrays.asList(lineCode.split(SPLIT_LEVLER)));
		}else if(getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)){
			map.put("customerCode", Arrays.asList(getUserInfo(request).get(COM_CODE).split(SPLIT_LEVLER)));
		}else if(customerCode!=null&&!customerCode.equals("")){
			map.put("customerCode", Arrays.asList(customerCode.split(SPLIT_LEVLER)));
		}	
		map.put(FaultsLogicImpl.QUERY_TYPE, FaultsLogicImpl.QUERY_TYPE_MONTH);
		map.put("startYear", startYear);
		map.put("startMonth", startMonth);
		map.put("endYear", endYear);
		map.put("endMonth", endMonth);
		map.put("language", language);
		List<FaultStat> result = faultsLogic.selectFaultStatsByCondition(map);
		map = new HashMap<String, Object>();
		if(result!=null){
			map.put("list", result);
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, null);
			response.getWriter().print(returnData);
		}else{
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, null);
			response.getWriter().print(returnData);
		}
	}
	
	/** 工单统计 按年
	* select 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param request
	* @param @param response
	* @param @param model
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping("/yearstat")
	@ResponseBody
	public void selectYearStat(String equCode,String customerCode,String lineCode,String startYear,String endYear,String language,HttpServletRequest request,
			HttpServletResponse response)
			throws IOException {	
		Map<String, Object> map = new HashMap<String, Object>();
		if(equCode!=null&&!equCode.equals("")){
			map.put("equCode", Arrays.asList(equCode.split(SPLIT_LEVLER)));
		}else if(lineCode!=null&&!lineCode.equals("")){
			map.put("lineCode", Arrays.asList(lineCode.split(SPLIT_LEVLER)));
		}else if(getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)){
			map.put("customerCode", Arrays.asList(getUserInfo(request).get(COM_CODE).split(SPLIT_LEVLER)));
		}else if(customerCode!=null&&!customerCode.equals("")){
			map.put("customerCode", Arrays.asList(customerCode.split(SPLIT_LEVLER)));
		}
		map.put(FaultsLogicImpl.QUERY_TYPE, FaultsLogicImpl.QUERY_TYPE_YEAR);
		map.put("startYear", startYear);	
		map.put("endYear", endYear);	
		map.put("language", language);
		List<FaultStat> result = faultsLogic.selectFaultStatsByCondition(map);
		map = new HashMap<String, Object>();
		if(result!=null){
			map.put("list", result);
			String returnData = returnJsonAllRequest(request, response, map,
					SUCCESS, null);
			response.getWriter().print(returnData);
		}else{
			String returnData = returnJsonAllRequest(request, response, map,
					ERROR, null);
			response.getWriter().print(returnData);
		}
	}
}
