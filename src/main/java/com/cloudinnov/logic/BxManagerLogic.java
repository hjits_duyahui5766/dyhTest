package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.BoardSolutionConfig;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.utils.model.BxClientModel;
import com.github.pagehelper.Page;

/**
 * 情报板业务类
 * @ClassName: BxManagerLogic
 * @Description: TODO
 * @author: ningmeng
 * @date: 2016年12月12日 下午4:54:25
 */
public interface BxManagerLogic extends IBaseLogic<BxClientModel> {
    /**
     * 发送文本到情报板(多条)
     * @Title: sendTextsToInformationBoard
     * @Description: TODO
     * @param bxModel
     * @return
     * @return: int
     */
    int sendTextsToInformationBoard(BxClientModel bxModel, String equipmentCode);
    Page<ControlSolutionSendRecordItem> selectListByEquCode(String equipmentCode, int size);
    int forcedToInforBorad(String code);
    List<ControlSolutionSendRecordItem> selectHistoryListByControlCode(String controlCode);
    List<ControlSolutionSendRecordItem> selectHistoryListByEquCateCode(String equCateCode);
    /**
     * 获取情报板当前显示内容 Use For 上海三思
     * @Title: selectBoardSanSiText
     * @Description: TODO
     * @return
     * @return: int
     */
    Map<String, Object> selectBoardSanSiText(String equipmentCode);
    /**
     * 获取情报板当前显示内容 Use For 西安诺瓦
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param equipmentCode
     * @return 参数
     * @return Map<String,Object> 返回类型
     */
    Map<String, Object> selectBoardSanNova(String equipmentCode);
    int sendTextBoardSanSi(BxClientModel bxModel, String equipmentCode);
    int sendTextsBoardSanSi(BxClientModel bxModel, String equipmentCode);
    /**
     * 发送文本到情报板(多条) from 诺瓦
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param bxModel
     * @param equipmentCode
     * @return 参数
     * @return int 返回类型
     */
    int sendTextsBoardNovaStar(BxClientModel bxModel, String equipmentCode);
    /**
     * 根据设备编码发送内容到指定情报板(用于群控方案触发)
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param equipmentCode
     * @return 参数
     * @return int 返回类型
     */
    int sendTextsToBoardUserForSolution(String equipmentCode, List<BoardSolutionConfig> config);
    boolean selectInfoBoardStatus(BxClientModel bxModel);
    /**
     * 查询播放列表模板
     * @Title: selectTemplate
     * @Description: TODO
     * @return
     * @return: Map<String,Object>
     */
    List<Map<String, Object>> selectTemplate();
    /**
     * 根据设备编码查询设备所属分类下的情报板方案
     * @Title: selectTemplateByCateCode
     * @Description: TODO
     * @param equipmentCode
     * @return
     * @return: List<Map<String,Object>>
     */
    List<Map<String, Object>> selectTemplateByCateCode(String equipmentCode);
    /**
     * 根据设备编码查询最后的发送记录(模板)
     * @Title: selectLastSendRecordByEquipmentCode
     * @Description: TODO
     * @param equipmentCode
     * @return
     * @return: List<Map<String,Object>>
     */
    List<Map<String, Object>> selectLastSendRecordByEquipmentCode(String equipmentCode);
    /**
     * 根据设备编码查询发送记录(模板)
     * @Title: selectSendRecordByEquipmentCode
     * @Description: TODO
     * @param equipmentCode
     * @param repeat 是否去重 1 去重 2 不去重
     * @return
     * @return: List<Map<String,Object>>
     */
    List<Map<String, Object>> selectSendRecordByEquipmentCode(String equipmentCode, int repeat);
    /**
     * 删除发送记录
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param model
     * @param @return 参数
     * @return int 返回类型
     */
    int deleteSendRecordLog(ControlSolutionSendRecordItem model);
}
