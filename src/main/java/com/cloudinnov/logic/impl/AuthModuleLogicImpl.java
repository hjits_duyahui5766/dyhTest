package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthModuleDao;
import com.cloudinnov.logic.AuthModuleLogic;
import com.cloudinnov.model.AuthModule;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

@Service
public class AuthModuleLogicImpl extends BaseLogicImpl<AuthModule> implements AuthModuleLogic {
	
	@Autowired
	private AuthModuleDao authModuleDao;
	
	@Override
	public int save(AuthModule model) {
		model.setCode(CodeUtil.authModuleCode(5));
		int returnCode = authModuleDao.insert(model);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = authModuleDao.insertInfo(model);
		}else{
			returnCode = CommonUtils.DEFAULT_NUM;
		}
		return returnCode;
	}
	@Override
	public int update(AuthModule model) {
		int returnCode = authModuleDao.updateByCondition(model);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = authModuleDao.updateInfoByCondition(model);
		}else{
			returnCode = CommonUtils.DEFAULT_NUM;
		}
		return returnCode;
	}
	
	@Override
	public int saveOtherLanguage(AuthModule model) {
		int returnCode = authModuleDao.insertInfo(model);
		return returnCode;
	}

	@Override
	public int updateOtherLanguage(AuthModule model) {
		int returnCode = authModuleDao.updateInfoByCondition(model);
		return returnCode;
	}
}
