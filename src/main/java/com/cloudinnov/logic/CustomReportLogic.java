package com.cloudinnov.logic;

import java.util.Map;

import com.cloudinnov.model.CustomReport;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;

public interface CustomReportLogic extends IBaseLogic<CustomReport> {
	public Page<CustomReport> search(PageModel page,Map<String, Object> params); 
}
