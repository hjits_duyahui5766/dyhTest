package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLineEquipments;

/**
 * @author nilixin
 * @date 2016年2月23日下午2:01:33
 * @email
 * @remark 生产线设备service接口
 * @version
 */
public interface ProductionLineEquipmentsLogic extends IBaseLogic<ProductionLineEquipments>{

	
	
	/** 查询产线下的设备 返回设备code和name
	* selectEquCodeNameByProLineCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @return    参数
	* @return List<Equipments>    返回类型 
	*/
	public List<Equipments> selectEquCodeNameByProLineCode(String prolineCodes,String language);
	
}
