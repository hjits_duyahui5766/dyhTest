package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.SysAreas;

/**
 * @author guochao
 * @date 2016年2月18日下午4:04:37
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public interface SysAreasLogic extends IBaseLogic<SysAreas> {
	
	public List<SysAreas> tableList(String[] codes, String language);
	
	public int saveOtherLanguage(SysAreas sysAreas);
	
	public int updateOtherLanguage(SysAreas sysAreas);
	
	public List<SysAreas> phoneList(SysAreas sysAreas);
	
	public List<SysAreas> search(SysAreas sysAreas);
}
