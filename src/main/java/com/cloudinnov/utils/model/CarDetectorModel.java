package com.cloudinnov.utils.model;

import java.util.HashMap;
import java.util.Map;

public class CarDetectorModel {
	
	private String deviceId;
	private String summaryTime;
	private Integer period;
	private Integer carTotal;
	private Integer laneTotal;
	
	private Map<Object, LaneData> data = new HashMap<>();
	
	

	public String getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	

	public String getSummaryTime() {
		return summaryTime;
	}


	public void setSummaryTime(String summaryTime) {
		this.summaryTime = summaryTime;
	}


	public Integer getPeriod() {
		return period;
	}


	public void setPeriod(Integer period) {
		this.period = period;
	}


	public Integer getCarTotal() {
		return carTotal;
	}


	public void setCarTotal(Integer carTotal) {
		this.carTotal = carTotal;
	}


	public Integer getLaneTotal() {
		return laneTotal;
	}


	public void setLaneTotal(Integer laneTotal) {
		this.laneTotal = laneTotal;
	}


	public Map<Object, LaneData> getData() {
		return data;
	}


	public void setData(Map<Object, LaneData> data) {
		this.data = data;
	}


	public class LaneData{
		private Integer lineId;
		private Integer avgVelocuty;
		private Integer occRate;
		private Integer flow;
		private Integer oneClassflow;
		private Integer twoClassflow;
		private Integer threeClassflow;
		private Integer fourClassflow;
		public Integer getLineId() {
			return lineId;
		}
		public void setLineId(Integer lineId) {
			this.lineId = lineId;
		}
		
		public Integer getAvgVelocuty() {
			return avgVelocuty;
		}
		public void setAvgVelocuty(Integer avgVelocuty) {
			this.avgVelocuty = avgVelocuty;
		}
		public Integer getOccRate() {
			return occRate;
		}
		public void setOccRate(Integer occRate) {
			this.occRate = occRate;
		}
		public Integer getFlow() {
			return flow;
		}
		public void setFlow(Integer flow) {
			this.flow = flow;
		}
		public Integer getOneClassflow() {
			return oneClassflow;
		}
		public void setOneClassflow(Integer oneClassflow) {
			this.oneClassflow = oneClassflow;
		}
		public Integer getTwoClassflow() {
			return twoClassflow;
		}
		public void setTwoClassflow(Integer twoClassflow) {
			this.twoClassflow = twoClassflow;
		}
		public Integer getThreeClassflow() {
			return threeClassflow;
		}
		public void setThreeClassflow(Integer threeClassflow) {
			this.threeClassflow = threeClassflow;
		}
		public Integer getFourClassflow() {
			return fourClassflow;
		}
		public void setFourClassflow(Integer fourClassflow) {
			this.fourClassflow = fourClassflow;
		}
		
		
	}
}
