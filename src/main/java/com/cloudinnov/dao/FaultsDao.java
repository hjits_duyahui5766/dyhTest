package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.FaultStat;
import com.cloudinnov.model.Faults;

public interface FaultsDao extends IBaseDao<Faults> {

	/**
	 * selectAlarmOrderInfoUseInsert
	 * 
	 * @Description: 通过故障通道查询出故障的基本信息 涉及到的表 equipment_fault_channel
	 *               fault_translation_libraries faults equipments
	 * @param @param
	 *            channelCode
	 * @param @return
	 *            参数
	 * @return Map<String,Object> 返回
	 *         equ.customer_code,channel.code,fault.description
	 *         ,fault.handling_suggestion
	 */
	public Map<String, Object> selectAlarmOrderInfoUseInsert(String channelCode, String faultCode);

	public List<Faults> listByLibCode(Map<String, Object> map);

	public int insertInfo(Faults faults);

	public int updateInfoByCondition(Faults faults);

	public int selectFaultCodeCount(Faults faults);

	/**
	 * 查看前1天的工单插入到故障统计表中
	 * 
	 * @return
	 */
	public int insertFaultStat(Map<String, Object> map);

	/**
	 * 工单详情统计 条件为equCode lineCode customerCode，默认可以不传
	 * selectFaultStatsByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            map
	 * @param @return
	 *            参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsDetailByCondition(Map<String, Object> map);

	/**
	 * 按天统计 条件为equCode lineCode customerCode 默认可以不传 day必须传
	 * selectFaultStatsDayByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            map
	 * @param @return
	 *            参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsDayByCondition(Map<String, Object> map);

	/**
	 * 按周统计 条件为equCode lineCode customerCode 默认可以不传
	 * startYear,endYear,startWeek,endWeek必须传 selectFaultStatsWeekByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            map
	 * @param @return
	 *            参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsWeekByCondition(Map<String, Object> map);

	/**
	 * 按月统计 条件为equCode lineCode customerCode 默认可以不传
	 * startYear,endYear,startMonth,endMonth必须传 selectFaultStatsMonthByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            map
	 * @param @return
	 *            参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsMonthByCondition(Map<String, Object> map);

	/**
	 * 按年统计 条件为equCode lineCode customerCode 默认可以不传
	 * selectFaultStatsYearByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            map
	 * @param @return
	 *            参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsYearByCondition(Map<String, Object> map);

	/**
	 * 查询故障统计报表实体 selectFaultStatByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            faultStat
	 * @param @return
	 *            参数
	 * @return FaultStat 返回类型
	 */
	public FaultStat selectFaultStatByCondition(FaultStat faultStat);

	/**
	 * 更新故障统计报表 updateFaultStatByCondition
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            faultStat
	 * @param @return
	 *            参数
	 * @return int 返回类型
	 */
	public int updateFaultStatByCondition(FaultStat faultStat);
	
	/** 根据故障码获取故障详情
	 * @Title: selectListByFaultCode 
	 * @Description: TODO
	 * @param faults
	 * @return
	 * @return: List<Faults>
	 */
	public List<Faults> selectListByFaultCode(Faults faults);

}