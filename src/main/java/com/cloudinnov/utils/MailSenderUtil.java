package com.cloudinnov.utils;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * @author 程宁
 * @date 2016年8月5日下午3:28:58
 * @email
 * @remark 发送邮件的工具类
 * @version
 */
public class MailSenderUtil {

	public static final int TEXT_TYPE = 1;
	public static final int HTML_TYPE = 2;

	/**
	 * 邮件发送 sendMail
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            receiverUser 收件人
	 * @param @param
	 *            subject 标题
	 * @param @param
	 *            content 内容
	 * @param @param
	 *            receiverType 1 发送文本格式 2 发送网页格式
	 * @param @return
	 *            参数
	 * @return boolean 返回类型
	 * @throws MessagingException 
	 */
	public static boolean sendMail(String receiverUser, String subject, String content, int receiverType) throws MessagingException {
		MailSenderUtil util = new MailSenderUtil();
		MultiMailSenderInfo mailInfo = util.new MultiMailSenderInfo();
		mailInfo.setMailServerHost(PropertiesUtils.findPropertiesKey("mail.smtp"));
		mailInfo.setValidate(true);
		// 发件邮箱用户名
		mailInfo.setUserName(PropertiesUtils.findPropertiesKey("mail.username"));
		// 发件邮箱密码
		mailInfo.setPassword(PropertiesUtils.findPropertiesKey("mail.password"));
		// 发件人邮箱
		mailInfo.setFromAddress(PropertiesUtils.findPropertiesKey("mail.username"));
		// 收件人邮箱
		mailInfo.setToAddress(receiverUser);
		// 邮件标题
		mailInfo.setSubject(subject);
		// 邮件内容
		mailInfo.setContent(content);
		boolean isSend = false;
		if (receiverType == TEXT_TYPE) {
			// 发送text格式
			isSend = MailSenderUtil.sendTextMail(mailInfo);
		}
		if (receiverType == HTML_TYPE) {
			// 发送text格式
			isSend = MailSenderUtil.sendHtmlMail(mailInfo);
		}
		return isSend;
	}

	public static boolean sendTextMail(MultiMailSenderInfo mailInfo) throws MessagingException {
		MailSenderUtil util = new MailSenderUtil();
		// 判断是否需要身份认证
		MyAuthenticator authenticator = null;
		Properties pro = mailInfo.getProperties();
		if (mailInfo.isValidate()) {
			// 如果需要身份认证，则创建一个密码验证器
			authenticator = util.new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
		}
		// 根据邮件会话属性和密码验证器构造一个发送邮件的session
		Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
		// 根据session创建一个邮件消息
		Message mailMessage = new MimeMessage(sendMailSession);
		// 创建邮件发送者地址
		Address from = new InternetAddress(mailInfo.getFromAddress());
		// 设置邮件消息的发送者
		mailMessage.setFrom(from);
		// 创建邮件的接收者地址，并设置到邮件消息中
		Address[] tos = null;
		String[] receivers = mailInfo.getReceivers();
		if (receivers != null) {
			// 为每个邮件接收者创建一个地址
			tos = new InternetAddress[receivers.length];
			// tos[0] = new InternetAddress(mailInfo.getToAddress());
			for (int i = 0; i < receivers.length; i++) {
				tos[i] = new InternetAddress(receivers[i]);
			}
		} else {
			tos = new InternetAddress[1];
			tos[0] = new InternetAddress(mailInfo.getToAddress());
		}
		// 将所有接收者地址都添加到邮件接收者属性中
		mailMessage.setRecipients(Message.RecipientType.TO, tos);

		mailMessage.setSubject(mailInfo.getSubject());
		mailMessage.setSentDate(new Date());
		// 设置邮件内容
		mailMessage.setText(mailInfo.getContent());
		// 发送邮件
		Transport.send(mailMessage);
		return true;
	}

	public static boolean sendHtmlMail(MailSenderInfo mailInfo) throws MessagingException {
		MailSenderUtil util = new MailSenderUtil();
		// 判断是否需要身份认证
		MyAuthenticator authenticator = null;
		Properties pro = mailInfo.getProperties();
		// 如果需要身份认证，则创建一个密码验证器
		if (mailInfo.isValidate()) {
			authenticator = util.new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
		}
		// 根据邮件会话属性和密码验证器构造一个发送邮件的session
		Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
		// 根据session创建一个邮件消息
		Message mailMessage = new MimeMessage(sendMailSession);
		// 创建邮件发送者地址
		Address from = new InternetAddress(mailInfo.getFromAddress());
		// 设置邮件消息的发送者
		mailMessage.setFrom(from);
		// 创建邮件的接收者地址，并设置到邮件消息中
		Address to = new InternetAddress(mailInfo.getToAddress());
		// Message.RecipientType.TO属性表示接收者的类型为TO
		mailMessage.setRecipient(Message.RecipientType.TO, to);
		// 设置邮件消息的主题
		mailMessage.setSubject(mailInfo.getSubject());
		// 设置邮件消息发送的时间
		mailMessage.setSentDate(new Date());
		// MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
		Multipart mainPart = new MimeMultipart();
		// 创建一个包含HTML内容的MimeBodyPart
		BodyPart html = new MimeBodyPart();
		// 设置HTML内容
		html.setContent(mailInfo.getContent(), "text/html; charset=utf-8");
		mainPart.addBodyPart(html);
		// 将MiniMultipart对象设置为邮件内容
		mailMessage.setContent(mainPart);
		// 发送邮件
		Transport.send(mailMessage);
		return true;

	}

	@SuppressWarnings("unused")
	private String getMailList(String[] mailArray) {

		StringBuffer toList = new StringBuffer();
		int length = mailArray.length;
		if (mailArray != null && length < 2) {
			toList.append(mailArray[0]);
		} else {
			for (int i = 0; i < length; i++) {
				toList.append(mailArray[i]);
				if (i != (length - 1)) {
					toList.append(",");
				}

			}
		}
		return toList.toString();

	}

	class MailSenderInfo {

		// 发送邮件的服务器的IP(或主机地址)
		private String mailServerHost;
		// 发送邮件的服务器的端口
		private String mailServerPort = "25";
		// 发件人邮箱地址
		private String fromAddress;
		// 收件人邮箱地址
		private String toAddress;
		// 登陆邮件发送服务器的用户名
		private String userName;
		// 登陆邮件发送服务器的密码
		private String password;
		// 是否需要身份验证
		private boolean validate = false;
		// 邮件主题
		private String subject;
		// 邮件的文本内容
		private String content;
		// 邮件附件的文件名
		private String[] attachFileNames;

		public MailSenderInfo() {
		}

		public MailSenderInfo(String mailServerHost, boolean validate, String userName, String password,
				String fromAddress) {
			this.mailServerHost = mailServerHost;
			this.validate = validate;
			this.userName = userName;
			this.password = password;
			this.fromAddress = fromAddress;
		}

		public Properties getProperties() {
			Properties p = new Properties();
			// 设置发送邮件协议
			p.put("mail.transport.protocol", "smtp");
			p.put("mail.smtp.host", this.mailServerHost);
			p.put("mail.smtp.port", this.mailServerPort);
			p.put("mail.smtp.auth", validate ? "true" : "false");
			return p;
		}

		public String getMailServerHost() {
			return mailServerHost;
		}

		public void setMailServerHost(String mailServerHost) {
			this.mailServerHost = mailServerHost;
		}

		public String getMailServerPort() {
			return mailServerPort;
		}

		public void setMailServerPort(String mailServerPort) {
			this.mailServerPort = mailServerPort;
		}

		public boolean isValidate() {
			return validate;
		}

		public void setValidate(boolean validate) {
			this.validate = validate;
		}

		public String[] getAttachFileNames() {
			return attachFileNames;
		}

		public void setAttachFileNames(String[] fileNames) {
			this.attachFileNames = fileNames;
		}

		public String getFromAddress() {
			return fromAddress;
		}

		public void setFromAddress(String fromAddress) {
			this.fromAddress = fromAddress;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getToAddress() {
			return toAddress;
		}

		public void setToAddress(String toAddress) {
			this.toAddress = toAddress;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String textContent) {
			this.content = textContent;
		}
	}

	public class MultiMailSenderInfo extends MailSenderInfo {

		// 邮件的接收者，可以有多个
		private String[] receivers;
		// 邮件的抄送者，可以有多个
		private String[] ccs;

		public String[] getCcs() {
			return ccs;
		}

		public void setCcs(String[] ccs) {
			this.ccs = ccs;
		}

		public String[] getReceivers() {
			return receivers;
		}

		public void setReceivers(String[] receivers) {
			this.receivers = receivers;
		}
	}

	class MyAuthenticator extends Authenticator {

		String userName = null;
		String password = null;

		public MyAuthenticator() {
		}

		public MyAuthenticator(String username, String password) {
			this.userName = username;
			this.password = password;
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(userName, password);
		}

	}
}
