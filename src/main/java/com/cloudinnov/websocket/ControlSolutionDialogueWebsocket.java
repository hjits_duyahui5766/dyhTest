package com.cloudinnov.websocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudinnov.utils.CommonUtils;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月6日下午4:08:04
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@ServerEndpoint("/controlSolutionDialogue")
public class ControlSolutionDialogueWebsocket {
	private static Logger LOG = LoggerFactory.getLogger(ControlSolutionDialogueWebsocket.class);
	public static final String SPLIT1 = ":";
	private String random;
	private static int onlineCount = 0;
	// 用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
	public static ConcurrentMap<String, ControlSolutionDialogueWebsocket> webSocketMap = new ConcurrentHashMap<String, ControlSolutionDialogueWebsocket>();
	// 用来发送数据
	private Session session;

	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		this.random = CommonUtils.getUUID();
		webSocketMap.put(random, this);
		//连接人数加1
		addOnlineCount();
		LOG.debug("设备状态推送开始连接:" + random);
	}
	@OnMessage
	public void onMessage(String message) {
	}
	@OnError
	public void onError(Throwable throwable, Session session) {
		LOG.error("设备状态推送异常:" + random);
	}
	@OnClose
	public void onClose(Session session) {
		webSocketMap.remove(random);
		subOnlineCount(); // 在线数减1
		LOG.debug("设备状态推送连接关闭:" + random);
	}
	public synchronized void sendMessage(String message) throws IOException {
		this.session.getBasicRemote().sendText(message);
	}
	public static synchronized int getOnlineCount() {
		return onlineCount;
	}
	public static synchronized void addOnlineCount() {
		ControlSolutionDialogueWebsocket.onlineCount++;
	}
	public static synchronized void subOnlineCount() {
		ControlSolutionDialogueWebsocket.onlineCount--;
	}
}
