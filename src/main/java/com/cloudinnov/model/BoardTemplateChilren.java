package com.cloudinnov.model;

import java.awt.Color;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BoardTemplateChilren {
	/**边框样式*/
	private int styleIndex;    	
		
	/**字体颜色*/
	private String fontColor = "red";
	
	private String horizontal = "000";
	
	private String vertical = "000";
	
	private Integer stayTime;
	
	private Integer speed;
	
	private List<Data> rowData;
	
	public int getStyleIndex() {
		return styleIndex;
	}

	public void setStyleIndex(int styleIndex) {
		this.styleIndex = styleIndex;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public String getHorizontal() {
		return horizontal;
	}

	public void setHorizontal(String horizontal) {
		this.horizontal = horizontal;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		this.vertical = vertical;
	}

	public Integer getStayTime() {
		return stayTime;
	}

	public void setStayTime(Integer stayTime) {
		this.stayTime = stayTime;
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	
	public List<Data> getRowData() {
		return rowData;
	}

	public void setRowData(List<Data> rowData) {
		this.rowData = rowData;
	}

	@JsonIgnore
	public Color getFontColorRGB(){
		if(fontColor.equals("red")){
			return Color.red;
		}else if(fontColor.equals("yellow")){
			return Color.yellow;
		}else if(fontColor.equals("blue")){
			return Color.blue;
		}else {
			return Color.red;
		}
	}
	
	public class Data {
		private Integer spacing = 05;//字间距
		
		/**字体大小*/
    	private int fontSize = 32;
    	
    	/**字体名称*/
    	private String fontName = "宋体";
    	
    	private Integer row;
    	
    	public int getFontSize() {
    		return fontSize;
    	}

    	public void setFontSize(int fontSize) {
    		this.fontSize = fontSize;
    	}

    	public String getFontName() {
    		return fontName;
    	}

    	public void setFontName(String fontName) {
    		this.fontName = fontName;
    	}
		
		public Integer getRow() {
			return row;
		}

		public void setRow(Integer row) {
			this.row = row;
		}

		public Integer getSpacing() {
			return spacing;
		}

		public void setSpacing(Integer spacing) {
			this.spacing = spacing;
		}
    	
    	
	}
}