package com.cloudinnov.websocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudinnov.utils.CommonUtils;

/**
 * 我的工单webSocket
 * @author chengning
 * @date 2016年4月11日上午11:13:03
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@ServerEndpoint("/myalarm/{user}")
public class MyAlarmWebsocket {
	private static Logger logger = LoggerFactory.getLogger(MyAlarmWebsocket.class);
	private static final String FAULT_REDIS_SPILT = "_";
	private String random;
	// 静态变量，用来记录当前在线连接数。
	private static int onlineCount = 0;
	// 用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
	public static ConcurrentMap<String, MyAlarmWebsocket> webSocketMap = new ConcurrentHashMap<String, MyAlarmWebsocket>();
	// 用来发送数据
	private Session session;

	/**
	 * 打开连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnOpen
	public void onOpen(@PathParam("user") String user, Session session) {
		this.session = session;
		this.random = CommonUtils.getUUID();
		webSocketMap.put(user + FAULT_REDIS_SPILT + random, this);
		// 连接人数加1
		addOnlineCount();
		logger.debug("我的工单推送开始连接:" + user + FAULT_REDIS_SPILT + random);
	}
	/**
	 * 收到客户端消息时触发 onMessage
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param select
	 * @param @param token
	 * @param @param message 参数
	 * @return void 返回类型
	 */
	@OnMessage
	public void onMessage(@PathParam("user") String user, String message) {
	}
	/**
	 * 异常时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnError
	public void onError(@PathParam("user") String user, Throwable throwable, Session session) {
		logger.error("我的工单推送异常:" + user + FAULT_REDIS_SPILT + random);
		logger.error(throwable.getMessage(), throwable);
	}
	/**
	 * 关闭连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnClose
	public void onClose(@PathParam("user") String user, Session session) {
		webSocketMap.remove(user + FAULT_REDIS_SPILT + random);
		subOnlineCount(); // 在线数减1
		logger.debug("我的工单推送连接关闭:" + user + FAULT_REDIS_SPILT + random);
	}
	/**
	 * 推送工单消息
	 * @param message
	 * @throws IOException
	 */
	public synchronized void sendMessage(String message) throws IOException {
		this.session.getBasicRemote().sendText(message);
	}
	public static synchronized int getOnlineCount() {
		return onlineCount;
	}
	public static synchronized void addOnlineCount() {
		MyAlarmWebsocket.onlineCount++;
	}
	public static synchronized void subOnlineCount() {
		MyAlarmWebsocket.onlineCount--;
	}
}
