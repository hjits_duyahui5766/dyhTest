package com.cloudinnov.logic;

import com.cloudinnov.model.HelpWorkOrder;

/**
 * @author chengning
 * @date 2016年5月4日上午11:01:39
 * @email ningcheng@cloudinnov.com
 * @remark 求助工单业务类
 * @version 
 */
public interface HelpWorkOrderLogic extends IBaseLogic<HelpWorkOrder> {

}
