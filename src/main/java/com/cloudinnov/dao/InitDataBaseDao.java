package com.cloudinnov.dao;

import java.util.List;

/** 初始化基础数据
 * @author chengning
 * @date 2016年4月10日下午10:21:00
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface InitDataBaseDao {
	
	/** 初始化数据库
	* newDatabaseCheck 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int newDatabaseCheck();

	/** 获取数据化的codes
	* selectInitCodes 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    参数
	* @return List<String>    返回类型 
	*/
	public List<String> selectInitCodes();
	
	/** 查询配置项
	* selectInitCodes 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    参数
	* @return List<String>    返回类型 
	*/
	public int selectConfig(String configName);
}
