package com.cloudinnov.controller.api;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sound.midi.SysexMessage;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.ControlSolutionConfigDao;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.mongo.FireCRTEventMongoDBDao;
import com.cloudinnov.model.ControlSolutionConfig;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.FireAlarmServer;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.websocket.ControlSolutionDialogueWebsocket;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/fire")
public class FireTestController {
	
	@Autowired
	private FireCRTEventMongoDBDao fireCRTEventMongoDBDao;
	@Autowired
	private ControlSolutionConfigDao controlSolutionConfigDao;
	@Autowired
	private EquipmentsAttrDao equipmentsAttrDao;
	@Autowired
	private AmqpTemplate faultTemplate;
	
	@RequestMapping("/string")
	@ResponseBody
	public String test() throws JsonProcessingException{
		
		FireCRTEvent fireEvent=new FireCRTEvent();
		//设备分类
		fireEvent.setCategoryCode("SDBJAN");
		//设备名称
		fireEvent.setDevice("火灾报警器");
		//设备编号
		fireEvent.setDeviceId("561611116694");
		//事件名称
		fireEvent.setEvent("手动报警按钮 ");
		//发生时间
		fireEvent.setOccurrenceTime(System.currentTimeMillis());
		//发生地点
		fireEvent.setPosition("K11+312");
		//解决方案
		fireEvent.setSolutionCode("RDASDF"); 
		Object json = JSON.toJSON(fireEvent);
		FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(json));
		return "ssss";
	}
	
	public int sendMesssageToDialogueWebSocket(String content) {
		// 遍历所有连接客户工单WebSocket 推送对话
		Iterator<Map.Entry<String, ControlSolutionDialogueWebsocket>> controlSolutionDialogueWebsocket = ControlSolutionDialogueWebsocket.webSocketMap
				.entrySet().iterator();
		while (controlSolutionDialogueWebsocket.hasNext()) {
			Map.Entry<String, ControlSolutionDialogueWebsocket> controlSolutionDialogueEntry = controlSolutionDialogueWebsocket
					.next();
			try {
				controlSolutionDialogueEntry.getValue().sendMessage(content);
			} catch (IOException e) {
			}
		}
		return 0;
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public String fire(){
		
		String string= "模拟火警: 0号机1回路3号地址 输入模块 火警 层 2017EQAF2HL2017-5-2 下午 03:58:40";
				 
		FireCRTEvent fireCRTEvent = StringUtil.StrToFireCRTEvent(string);
		FireCRTEvent crtEvent = new FireCRTEvent();
		crtEvent.setEvent("火灾测试");
		crtEvent.setEquipmentCode("AFDWDFAS");
		crtEvent.setOccurrenceTime(System.currentTimeMillis());
		crtEvent.setDeviceId("45353435");
		crtEvent.setCategoryCode("asdfa");
		crtEvent.setPosition("二号手报按钮");
		FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(crtEvent));
		return "success";
	}
	
	static class StringUtil {
		private static final String timeformatOFWIndowsXP = "y-M-d a hh:mm:ss";//// 处理火灾系统时间格式
		private static final String timeformatOFWindows10 = "y/M/d H:mm:ss";

		// 格式化CRT火灾系统时间到本地系统格式
		static private Long getLongByViewDate(String viewdate) {
			SimpleDateFormat df = new SimpleDateFormat(timeformatOFWIndowsXP);// 设置日期格式
			try {
				return df.parse(viewdate).getTime();
			} catch (ParseException e) {
				// 时间解析失败
			}
			df = new SimpleDateFormat(timeformatOFWindows10);// 设置日期格式
			try {
				return df.parse(viewdate).getTime();
			} catch (ParseException e) {
			}
			return null;
		}
		
		public static FireCRTEvent StrToFireCRTEvent(String recinf) {
			FireCRTEvent fireCRTEvent = new FireCRTEvent();
			Calendar a = Calendar.getInstance();
			int aindextimestart = recinf.lastIndexOf(String.valueOf(a.get(Calendar.YEAR)));
			String strViewTime = recinf.substring(aindextimestart, recinf.length());// 有用数据包会以时间为结尾
			fireCRTEvent.setOccurrenceTime(getLongByViewDate(strViewTime));
			String strinf = recinf.substring(0, aindextimestart).trim();
			String infs[] = strinf.split(" ");
			String info = infs[0];
			if (infs.length == 2) {// 与系统连接恢复时,特殊处理数据包eq: 系统故障恢复: 远程传输恢复
				fireCRTEvent.setEvent(infs[0]);
				fireCRTEvent.setDevice(infs[1]);
				return fireCRTEvent;
			}
			fireCRTEvent.setEvent(infs[0]);
			fireCRTEvent.setPosition(infs[1]);
			fireCRTEvent.setDevice(infs[2]);
			if (!info.equals("其他事件:") && !info.equals("故障:") && !info.equals("故障恢复:")) {
				if (!infs[infs.length - 1].equals("层")) {// 消息包含附加地理位置信息
					String[] values = infs[infs.length - 1].split("号");
					fireCRTEvent.setDeviceId(values[0]);
				}
			} else {
				fireCRTEvent = null;
			}
			return fireCRTEvent;
		}
	}
	
}
