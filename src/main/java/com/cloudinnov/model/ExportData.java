package com.cloudinnov.model;

import java.util.List;

import org.wuwz.poi.ExportConfig;

/** 
*
 * @author chengning
 * @date 2017年9月1日下午12:33:27
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public class ExportData {
    
    /* 时间趋势 */
    @ExportConfig(value = "时间趋势", width = 100)
    private String timesTrend;
    /* 时间 */
    @ExportConfig(value = "时间", width = 100)
    private List<String> time;
    /* 通车量 */
    @ExportConfig(value = "通车量", width = 100)
    private List<Object> value;

    public String getTimesTrend() {
        return timesTrend;
    }
    public void setTimesTrend(String timesTrend) {
        this.timesTrend = timesTrend;
    }
    public List<String> getTime() {
        return time;
    }
    public void setTime(List<String> time) {
        this.time = time;
    }
    public List<Object> getValue() {
        return value;
    }
    public void setValue(List<Object> value) {
        this.value = value;
    }
}
