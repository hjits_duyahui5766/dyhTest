package com.cloudinnov.utils.control;

import java.util.List;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月13日下午12:37:09
 * @email ningcheng@cloudinnov.com
 * @remark 设备控制日志记录
 * @version
 */
public class EquipmentControlLog {
	private Integer code;
	private List<Data> data;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public List<Data> getData() {
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "EquipmentControlLog [code=" + code + ", data=" + data + "]";
	}

	public static class Data {
		private Integer code;
		private String xid;
		private Object value;
		private Boolean result;

		public String getXid() {
			return xid;
		}
		public void setXid(String xid) {
			this.xid = xid;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public Boolean getResult() {
			return result;
		}
		public void setResult(Boolean result) {
			this.result = result;
		}
		public Integer getCode() {
			return code;
		}
		public void setCode(Integer code) {
			this.code = code;
		}
		@Override
		public String toString() {
			return "Data [code=" + code + ", xid=" + xid + ", value=" + value + ", result=" + result + "]";
		}
	}
}
