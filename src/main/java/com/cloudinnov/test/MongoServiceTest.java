package com.cloudinnov.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.model.CarDetectorData.CarDetectorDataChilren;
import com.cloudinnov.utils.CommonUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/applicationContext.xml", "classpath*:/kafka.xml",
		"classpath*:/spring-mongodb.xml" })
public class MongoServiceTest {
	private static final int DATA_LENGTH = 178;
	public static final String TOTAL_CAR = "total:car";
	public static final String CAL_NAME = "car:deviceId:";
	public static final String CAL_VALUE = ":value";
	private static final int RECENT_LIST_COUNT = 10000;
	public static final String SPLITTER_LEVEL0 = ",";
	
	private static SimpleDateFormat sdfHour = new SimpleDateFormat("yyyyMMddHHmmss");
	@Autowired
	private CarDetectorLogic carDetectorLogic;
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	JedisPool jedisPool;
 
	@Test
	public  void redis() {
		Jedis redis = jedisPool.getResource();
		String requestData="ff0000646748042816010397020115d00001547115340902281117012c0039062604000900080001000000004f04000b00090000000200007503000900090000000000006503000800080000000000003004000800040003000100002302000c000b0001000000009e66";
		System.err.println(requestData.length());
		 
		if (CommonUtils.isNotEmpty(requestData) && requestData.length() >= 64 && requestData.length() <= 215) {
			String keyRealtime, totalName, value;
			CarDetectorData model = null;
			 
			model = new CarDetectorData();
			 
			int lineId = 1;
			// 得到设备唯一ID
			model.setDeviceId(requestData.substring(8, 24));
			String deviceId=requestData.substring(8, 24);
			// 获取时间和日期
			model.setSummaryTime(requestData.substring(40, 54));
			// 获取周期
			model.setPeriod(Integer.parseInt(requestData.substring(54, 58), 16));
			// 车辆总数
			model.setCarTotal(Long.parseLong(requestData.substring(58, 62), 16));
			// 车道数
			model.setLaneTotal(Integer.parseInt(requestData.substring(62, 64), 16));
			
			int firstLaneData = Integer.parseInt(requestData.substring(68, 72),16);
			int secondLaneData=Integer.parseInt(requestData.substring(92,96),16);
			int thirdLaneData=Integer.parseInt(requestData.substring(116,120),16);
			int forthLaneData=Integer.parseInt(requestData.substring(140,144),16);
			int fifthLaneData=Integer.parseInt(requestData.substring(164,168),16);
			int sixthLaneData=Integer.parseInt(requestData.substring(188,192),16);
			
			int beyondLaneData= firstLaneData+secondLaneData+thirdLaneData;
			int behindLaneData=forthLaneData+fifthLaneData+sixthLaneData;
			int allData=beyondLaneData+behindLaneData;
			
			model.setUtcTime(new Date());
			model.setTimeMillis(System.currentTimeMillis());
			// 车道数据共12个字节和2字节crc校验包括：
			String lineData = requestData.substring(64, requestData.length() - 4);
			keyRealtime = CAL_NAME + model.getDeviceId() + CAL_VALUE;
			totalName = TOTAL_CAR + CAL_VALUE;
			value = model.getCarTotal() + SPLITTER_LEVEL0 + model.getTimeMillis();
			System.err.println(value);
		 
		 	redis.lpush(keyRealtime, value);// 单个车检仪器只保存最新的3000条数据
			redis.ltrim(keyRealtime, 0L, RECENT_LIST_COUNT);
			redis.set(totalName, value);// 总数只保留最新的一条
		}
	}
	
	@Test
	public void testMongodb() {
		CarDetectorData user = new CarDetectorData();
		user.setEquipmentCode("EQU000002");
		user.setCarTotal(50000l);
		user.setDeviceId("4545215646578974");
		// mongoOperations.save(user, "car_detector_data");
		// List<CarDetectorData> userGetFromMdb = mongoOperations.find(new Query(),
		// MongoServiceTest.CarDetectorData.class, "car_detector_data");
		// System.out.println(JSON.toJSON(userGetFromMdb));*/
		// Date date = format.parse("2012-01-20 00:00:00");
		CarDetectorData.CarDetectorDataChilren userChilren = null;
		int i = 0;
		while (i < 10) {
			List<CarDetectorDataChilren> data = new ArrayList<>();
			for (int k = 0; k < 12; k++) {
				userChilren = new CarDetectorData().new CarDetectorDataChilren();
				userChilren.setDeviceId(user.getDeviceId());
				userChilren.setLineFlow(50000);
				userChilren.setLineAvgVelocuty(120);
				userChilren.setLineId(k);
				// user.setData(userChilren);
				user.setTimeMillis(System.currentTimeMillis());
				user.setUtcTime(new Date());
				carDetectorLogic.saveRealTimeDataToMongoDB(user);
			}
			i++;
		}
		// mongoOperations.createCollection(CarDetectorData.class);
		// System.out.println(sumField("carDetectorData", "carTotal", new Criteria()));
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -3);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 0);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 5);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 23);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		System.out.println(oneDayStart.getTimeInMillis() - 1000000);
		// match
		BasicDBObject[] array = {
				new BasicDBObject("timeMillis", new BasicDBObject("$gte", new Date().getTime() - 1000000000)),
				new BasicDBObject("timeMillis", new BasicDBObject("$lte", new Date().getTime() + 100000000)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject projectFields = new BasicDBObject();
		projectFields.put("yearMonthDay", new BasicDBObject("$dateToString",
				new BasicDBObject("format", "%Y-%m-%d").append("date", "$createTime")));
		DBObject project = new BasicDBObject("$project", projectFields);
		DBObject match = new BasicDBObject("$match", cond);
		// group
		long total = 0l;
		DBObject groupFields = new BasicDBObject("_id", "$lastEvent");
		groupFields.put("carTotal", new BasicDBObject("$sum", "$carTotal"));
		DBObject reshapeGroup = new BasicDBObject("$group", groupFields);
		// sort
		DBObject sort = new BasicDBObject("$sort", new BasicDBObject("_id", 1));
		// limit
		DBObject limit = new BasicDBObject("$limit", 5);
		/*
		 * AggregationOutput output =
		 * mongoTemplate.getCollection("carDetectorData").aggregate(match, project);
		 * Iterable<DBObject> list= output.results(); for(DBObject dbObject:list){
		 * System.out.println(dbObject.get("_id") +" "+dbObject.get("yearMonthDay") +
		 * " "+dbObject.get("carTotal")); }
		 */
		// { "aggregate" : "carDetectorData" , "pipeline" : [ { "$group" : { "_id" : "$lastEvent" ,
		// "carTotal" : { "$sum" : "$carTotal"}}} , { "$project" : { "yearMonthDay" : {
		// "$dateToString" : { "format" : "%Y-%m-%d" , "date" : "$createTime"}}}}]}
		// String groupStr = "{\"$group\":{_id:{time:{\"$substr\":[\"$utcTime\",0,10]}},
		// total:{\"$sum\": \"$carTotal\"}, equipmentCode:{\"$sum\": \"$equipmentCode\"}}}";
		DBObject projectFields1 = new BasicDBObject();
		projectFields1.put("yearMonthDay", new BasicDBObject("$dateToString",
				new BasicDBObject("format", "%Y-%m-%d %H").append("date", "$utcTime")));
		DBObject project1 = new BasicDBObject("$project", projectFields1);
		String groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"}}}";
		DBObject group2 = JSON.parseObject(groupStr, BasicDBObject.class);
		// ReshapeGroup Result
		DBObject projectFields2 = new BasicDBObject();
		projectFields2.put("year", new BasicDBObject("$year", "$utcTime"));
		/*
		 * projectFields2.put("time","$_id.time");
		 * projectFields2.put("equipmentCode","$equipmentCode");
		 */
		DBObject project2 = new BasicDBObject("$project", projectFields2);
		//
		// 将结果push到一起
		DBObject groupAgainFields2 = new BasicDBObject("_id", "$lastEvent");
		groupAgainFields2.put("total", new BasicDBObject("$sum", "$total"));
		groupAgainFields2.put("time", new BasicDBObject("$sum", "$time"));
		DBObject reshapeGroup2 = new BasicDBObject("$group", groupAgainFields2);
		// 查看Group结果
		AggregationOutput output = mongoTemplate.getCollection("carDetectorData").aggregate(group2);
		System.out.println(output.getCommand());
		Iterable<DBObject> list = output.results();
		for (DBObject dbObject : list) {
			System.out.println("数据： \t" + dbObject.get("time"));
			System.out.println("数据： \t" + JSON.toJSONString(dbObject));
		}
		System.out.println(JSON.toJSONString(carDetectorLogic.selectDataByGroupAndCondition(0, null, null, null)));
	}
	/**
	 * 计算某个字段是和
	 * @param collection
	 * @param filedName
	 * @return
	 */
	public long sumField(String collection, String filedName, Criteria criteria) {
		long total = 0l;
		String reduce = "function(doc, aggr){" + "            aggr.total += parseInt((Math.round((doc." + filedName
				+ ")*100)/100));" + "       }";
		Query query = new Query();
		if (criteria != null) {
			query.addCriteria(criteria);
		}
		DBObject result = mongoOperations.getCollection(collection).group(null, query.getQueryObject(),
				new BasicDBObject("total", total), reduce);
		Map<String, BasicDBObject> map = result.toMap();
		if (map.size() > 0) {
			BasicDBObject bdbo = map.get("0");
			if (bdbo != null && bdbo.get("total") != null)
				total = bdbo.getLong("total");
		}
		return total;
	}
}
