package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.EquipmentModelLogic;
import com.cloudinnov.model.EquipmentModel;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;


@Controller
@RequestMapping("/webapi/equipmentModel")
public class EquipmentModelController extends BaseController {

	@Autowired
	private EquipmentModelLogic equipmentModelLogic;

	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(@Valid PageModel page, BindingResult result, EquipmentModel model, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		model.setOemCode(getUserInfo(request).get(OEM_CODE));
		Page<EquipmentModel> equipmentModel = equipmentModelLogic.selectListPage(page.getIndex(), page.getSize(), model, false);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentModel != null) {
			map.put("total", equipmentModel.getTotal());
			map.put("list", equipmentModel);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public void save(EquipmentModel equipmentModel, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = equipmentModelLogic.save(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	public void saveOtherLanguage(EquipmentModel equipmentModel, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = null;
		equipmentModel.setLanguage(equipmentModel.getOtherLanguage());
		int result = equipmentModelLogic.saveOtherLanguage(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	public void updateOtherLanguage(EquipmentModel equipmentModel, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentModel.setLanguage(equipmentModel.getOtherLanguage());
		int result = equipmentModelLogic.updateOtherLanguage(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public void delete(EquipmentModel equipmentModel, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = equipmentModelLogic.delete(equipmentModel);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public void update(EquipmentModel equipmentModel, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = equipmentModelLogic.update(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentModel equipmentModel, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (equipmentModel.getOtherLanguage() != null && equipmentModel.getOtherLanguage() != "") {
			equipmentModel.setLanguage(equipmentModel.getOtherLanguage());
		}
		EquipmentModel result = equipmentModelLogic.select(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/selectByCateCode")
	@ResponseBody
	public void selectByCateCode(EquipmentModel equipmentModel, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<EquipmentModel> result = equipmentModelLogic.selectByCateCode(equipmentModel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}
}
