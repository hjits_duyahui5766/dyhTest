package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.EquipmentModel;


public interface EquipmentModelLogic extends IBaseLogic<EquipmentModel> {

	public int saveOtherLanguage(EquipmentModel equipmentModel);

	public int updateOtherLanguage(EquipmentModel equipmentModel);

	public List<EquipmentModel> listByCode(EquipmentModel equipmentModel);
	
	public List<EquipmentModel> selectByCateCode(EquipmentModel equipmentModel);
}
