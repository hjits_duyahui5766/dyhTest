package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentInspectionResultDao;
import com.cloudinnov.logic.EquipmentInspectionResultLogic;
import com.cloudinnov.model.EquipmentInspectionResult;
import com.cloudinnov.utils.CodeUtil;

@Service
public class EquipmentInspectionResultLogicImpl extends BaseLogicImpl<EquipmentInspectionResult> implements EquipmentInspectionResultLogic {
	
	@Autowired
	private EquipmentInspectionResultDao equipmentInspectionResultDao;
	
	public int save(EquipmentInspectionResult entity) {
		entity.setCode(CodeUtil.inspectionResultCode(5));
		return equipmentInspectionResultDao.insert(entity);
	}
}
