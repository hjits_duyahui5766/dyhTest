package com.cloudinnov.dao;

import com.cloudinnov.model.Attach;

/** 附件表
 * @author chengning
 * @date 2016年4月14日下午2:12:29
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface AttachDao extends IBaseDao<Attach>{
   
}