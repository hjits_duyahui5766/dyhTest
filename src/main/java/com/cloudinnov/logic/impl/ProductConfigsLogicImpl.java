package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.ProductConfigsDao;
import com.cloudinnov.logic.ProductConfigsLogic;
import com.cloudinnov.model.ProductConfigs;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author
 * @date 2016年2月23日下午4:42:20
 * @email
 * @remark 产物配置 业务层接口实现
 * @version
 */
@Service
public class ProductConfigsLogicImpl extends BaseLogicImpl<ProductConfigs> implements ProductConfigsLogic {
	private int digit = 5;
	@Autowired
	private ProductConfigsDao productConfigsDao;

	public int save(ProductConfigs productConfig) {
		productConfig.setCode(CodeUtil.mprCode(digit));
		int returnCode = productConfigsDao.insert(productConfig);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = productConfigsDao.insertInfo(productConfig);
		}
		return returnCode;
	}

	@Override
	public int update(ProductConfigs productConfig) {
		int returnCode = productConfigsDao.updateByCondition(productConfig);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = productConfigsDao.updateInfoByCondition(productConfig);
		}
		return returnCode;
	}

	@Override
	public int saveOtherLanguage(ProductConfigs productConfig) {
		return productConfigsDao.insertInfo(productConfig);
	}

	@Override
	public int updateOtherLanguage(ProductConfigs productConfig) {
		return productConfigsDao.updateInfoByCondition(productConfig);
	}

}
