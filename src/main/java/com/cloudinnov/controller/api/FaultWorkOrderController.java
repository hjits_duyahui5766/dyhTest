package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.FaultWorkOrderLogic;
import com.cloudinnov.model.WorkOrder;
import com.github.pagehelper.Page;

/**
 * 故障工单管理
 * @author libo
 * @date 2016年12月6日 下午1:50:46
 * @email boli@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/faultWorkOrder")
public class FaultWorkOrderController extends BaseController {
	
	@Autowired
	private FaultWorkOrderLogic faultWorkOrderLogic;
	
	
	/**
	 * 保存
	 * save
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/save",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="故障工单管理",methods="故障工单添加")
	public void save(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = faultWorkOrderLogic.saveFaultWorkOrder(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	
	/**
	 * 故障工单查询--单条
	 * selectFaultWorkOrder
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/selectFault",method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module="故障工单管理",methods="故障工单查询--单条")
	public void selectFaultWorkOrder(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		WorkOrder workOrderInfo = faultWorkOrderLogic.selectFaultWorkOrderByCode(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(workOrderInfo != null){
			map.put("workOrderInfo", workOrderInfo);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	/**
	 * 故障工单修改
	 * update
	 * @Description
	 * @param
	 * @return
	 *
	 */
	@RequestMapping(value="/update",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="故障工单管理",methods="故障工单修改")
	public void update(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
			
		int result = faultWorkOrderLogic.updateFaultWorkOrder(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		} else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
		
	}
	
	/**
	 * 故障工单删除
	 * delete
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="故障工单管理",methods="故障工单删除")
	public void delete(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = faultWorkOrderLogic.deleteFaultWorkOrder(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	/**
	 * 故障工单查询--分页
	 * select
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="list",method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module="故障工单管理",methods="故障工单查询")
	public void select(WorkOrder workOrder,int index,int size,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		Integer type = 2;
		workOrder.setType(type);
		
		Page<WorkOrder> workOrderInfo =  faultWorkOrderLogic.selectFaultWorkOrder(workOrder,index,size);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(workOrderInfo != null){
			map.put("workOrderInfo", workOrderInfo);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map,ERROR,"");
		}
		response.getWriter().print(returnData);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
	