package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentBomsLogic;
import com.cloudinnov.model.EquipmentBoms;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月17日下午6:10:25
 * @email chaoguo@cloudinnov.com
 * @remark 设备bom controller
 * @version
 */
@Controller
@RequestMapping("/webapi/equipmentBom")
public class EquipmentBomsController extends BaseController {

	@Autowired
	private EquipmentBomsLogic equipmentBomsLogic;

	/**
	 * save
	 * 
	 * @Description: 保存设备bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备Bom信息保存")
	public void save(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		// 二进制流转化为图片，并保存
		String base64S = equipmentBom.getImageUrl();
		if (base64S != "" && base64S != null) {
			String newImg = storeBase64(base64S, request);
			equipmentBom.setImageUrl(newImg);
		}
		equipmentBom.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = equipmentBomsLogic.save(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备Bom信息保存")
	public void saveOtherLanguage(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		equipmentBom.setLanguage(equipmentBom.getOtherLanguage());
		int result = equipmentBomsLogic.saveOtherLanguage(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "点位添加其他语言")
	public void updateOtherLanguage(EquipmentBoms equipmentBom, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentBom.setLanguage(equipmentBom.getOtherLanguage());
		int result = equipmentBomsLogic.updateOtherLanguage(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据code或者id 删除设备bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备Bom信息删除")
	public void delete(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = equipmentBomsLogic.delete(equipmentBom);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 根据code或者id更新设备bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备Bom信息修改")
	public void update(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 二进制流转化为图片，并保存
		String base64S = equipmentBom.getImageUrl();
		if (base64S != "" && base64S != null) {
			String newImg = storeBase64(base64S, request);
			equipmentBom.setImageUrl(newImg);
		}
		int result = equipmentBomsLogic.update(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 分页查找设备bom
	 * @param @param
	 *            index 当前页数
	 * @param @param
	 *            size 页面总数
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentBoms bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Page<EquipmentBoms> equipmentBoms = equipmentBomsLogic.selectListPage(index, size, bom, false);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentBoms != null) {
			map.put("total", equipmentBoms.getTotal());
			map.put("list", equipmentBoms);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		List<EquipmentBoms> equipmentBoms = equipmentBomsLogic.listByCode(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentBoms != null) {
			map.put("list", equipmentBoms);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * select
	 * 
	 * @Description: 根据code或者id 查找设备bom实体
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentBoms equipmentBom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (equipmentBom.getOtherLanguage() != null && equipmentBom.getOtherLanguage() != "") {
			equipmentBom.setLanguage(equipmentBom.getOtherLanguage());
		}
		EquipmentBoms result = equipmentBomsLogic.select(equipmentBom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/** bom导入
	* bomImport 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param data
	* @param @param equipmentPoints
	* @param @param request
	* @param @param response
	* @param @throws JsonGenerationException
	* @param @throws JsonMappingException
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备Bom导入")
	public void bomImport(String data, EquipmentBoms equipmentBoms,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		Map<String, Object> map = new HashMap<String, Object>();
		int returnCode = equipmentBomsLogic.bomImport(data, equipmentBoms);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
