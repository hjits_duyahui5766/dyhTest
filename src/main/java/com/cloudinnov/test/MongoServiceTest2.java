package com.cloudinnov.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.mongo.EnvironmentMongoDBDao;
import com.cloudinnov.dao.mongo.FireCRTEventMongoDBDao;
import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.Environment;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.model.LightDetect;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/applicationContext.xml", "classpath*:/kafka.xml",
		"classpath*:/spring-mongodb.xml" })
public class MongoServiceTest2 {
	private static SimpleDateFormat sdfHour = new SimpleDateFormat("yyyyMMddHHmmss");
	@Autowired
	private CarDetectorLogic carDetectorLogic;
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	private FireCRTEventMongoDBDao fireCRTEventMongoDBDao;
 
	@Test
	public void testMongodb() {
		/*
		 * BasicDBObject[] array = new BasicDBObject[] { new BasicDBObject("pointCode", "Dn65wrq8")
		 * }; BasicDBObject cond = new BasicDBObject(); cond.put("$and", array); DBObject match =
		 * new BasicDBObject("$match", cond); AggregationOutput output =
		 * mongoTemplate.getCollection("historyDataToMongoDB").aggregate(match); for (DBObject
		 * dbObject : output.results()) { List<HistoryDataToMongoDB.Data> datas = (List<Data>)
		 * dbObject.get("data"); System.out.println(datas); }
		 */
		//String receiveMsg = "{\"event\":\"模拟火警\",\"position\":\"3号机2回路52号地址\",\"device\":\"智能感烟\",\"deviceId\":\"45454545\",\"occurrenceTime\":\"1491546001000\"}";
		//fireCRTEventMongoDBDao.save(JSON.parseObject(receiveMsg, FireCRTEvent.class));
		 
		 
 
		LightDetect lightDetect=new LightDetect();
		lightDetect.setEquipmentCode("EQMCDXC");
		lightDetect.setValue("2000.0");
		lightDetect.setTimeMillis(System.currentTimeMillis());
		lightDetect.setUtcTime(new Date());
		mongoTemplate.insert(lightDetect);
		 
	}
	private String dateFormat(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	/**
	 * 计算某个字段是和
	 * @param collection
	 * @param filedName
	 * @return
	 */
	public long sumField(String collection, String filedName, Criteria criteria) {
		long total = 0l;
		String reduce = "function(doc, aggr){" + "aggr.total += parseInt((Math.round((doc." + filedName
				+ ")*100)/100));" + "       }";
		Query query = new Query();
		if (criteria != null) {
			query.addCriteria(criteria);
		}
		DBObject result = mongoOperations.getCollection(collection).group(null, query.getQueryObject(),
				new BasicDBObject("total", total), reduce);
		Map<String, BasicDBObject> map = result.toMap();
		if (map.size() > 0) {
			BasicDBObject bdbo = map.get("0");
			if (bdbo != null && bdbo.get("total")     != null)
				total = bdbo.getLong("total");
		}
		return total;
	}
}
