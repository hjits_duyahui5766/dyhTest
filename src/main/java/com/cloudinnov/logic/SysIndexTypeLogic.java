package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.SysIndexType;

public interface SysIndexTypeLogic extends IBaseLogic<SysIndexType> {
	
public int saveOtherLanguage(SysIndexType sysIndexType) ;
	
	public int updateOtherLanguage(SysIndexType sysIndexType);
	
	/** 根据设备查询设备下点位的指标类型
	 * @Title: selectIndexTypeListByEquipmentCodes 
	 * @Description: TODO
	 * @param sysIndexType
	 * @return
	 * @return: List<SysIndexType>
	 */
	public List<SysIndexType> selectIndexTypeListByEquipmentCodes(SysIndexType indexType);
	
}
