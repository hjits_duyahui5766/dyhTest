package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.dao.PredictionRuleTableDao;
import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.PredictionRuleLogLogic;
import com.cloudinnov.logic.PredictionRuleTableLogic;
import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleTable;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;

import scala.collection.generic.BitOperations.Int;

@Controller
@RequestMapping("/webapi/predictionRuleLog")
public class PredictionRuleLogController extends BaseController {
	static final Logger LOG = LoggerFactory.getLogger(PredictionRuleLogController.class);

	@Autowired
	private PredictionRuleLogLogic predictionRuleLogLogic;

	/**
	 * 插入预案执行记录的方法
	 * 
	 * @param predictionRuleLog
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/insertPredictionRuleLog", method = RequestMethod.POST)
	@SystemLog(module = "插入预案执行记录", methods = "插入预案执行记录的方法")
	public void insertPredictionRuleLog(PredictionRuleLog predictionRuleLog, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		String code = predictionRuleLogLogic.insertPredictionRuleLog(predictionRuleLog);

		Map<String, Object> map = new HashMap<String, Object>();
		if (JudgeNullUtil.iStr(code)) {
			map.put("predictionRuleLogCode", code);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询预案执行记录的方法
	 * 
	 * @param predictionRuleLog
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectpredictionRuleLog", method = RequestMethod.GET)
	@SystemLog(module = "查询预案执行记录", methods = "查询预案执行记录的方法")
	public void selectPredictionRuleLog(PredictionRuleLog predictionRuleLog, int index, int size,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";

		Page<PredictionRuleLog> predictionRuleLogs = predictionRuleLogLogic.selectPredictionRuleLog(predictionRuleLog,
				index, size);

		Map<String, Object> map = new HashMap<String, Object>();
		if (JudgeNullUtil.iList(predictionRuleLogs)) {
			map.put("data", predictionRuleLogs);
			map.put("total", predictionRuleLogs.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 更新预案执行记录的方法
	 * 
	 * @param predictionRuleLog
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updatePredictionRuleLog", method = RequestMethod.POST)
	@SystemLog(module = "更新预案执行记录", methods = "更新预案执行记录的方法")
	public void updatePredictionRuleLog(PredictionRuleLog predictionRuleLog, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleLogLogic.updatePredictionRuleLog(predictionRuleLog);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

}
