package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.Section;
import com.cloudinnov.model.SectionPlanConfig;
import com.github.pagehelper.Page;

public interface SectionLogic extends IBaseLogic<Section> {
	public Page<Section> search(int index, int size, String country, String province, String city, String key,
			String type);
	/**
	 * 根据路段code统计设备、产线、工单数量
	 * @param comCode
	 * @return
	 */
	public Map<String, Object> selectEquLineAlarmCountBySectionCode(String sectionCode);
	/**
	 * 路段平面配置保存
	 * @param section
	 * @return
	 */
	public int saveSectionPlanConfig(SectionPlanConfig section);
	/**
	 * 查询路段平面配置信息
	 * @param page
	 * @param sectionPlan
	 * @return
	 */
	public Page<SectionPlanConfig> selectSectionPlanConfig(PageModel page, SectionPlanConfig sectionPlan);
	public int insertSectionConfig(SectionPlanConfig section);
	public List<SectionPlanConfig> selectSectionConfig(SectionPlanConfig section);
	/**
	 * 保存横道的信息
	 * @param section
	 * @return
	 */
	public int insertSectionConfigHD(SectionPlanConfig section);
	/**
	 * 删除路段平面图配置
	 * @param section
	 * @return
	 */
	public int deleteSectionConfig(SectionPlanConfig section);
	
	public Section searchSectionInfo(String equipCode);
}
