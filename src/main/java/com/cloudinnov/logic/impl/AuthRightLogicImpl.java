package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthRightDao;
import com.cloudinnov.logic.AuthRightLogic;
import com.cloudinnov.model.AuthRight;
import com.cloudinnov.utils.CommonUtils;

@Service
public class AuthRightLogicImpl extends BaseLogicImpl<AuthRight> implements AuthRightLogic {
	
	@Autowired
	private AuthRightDao authRightDao;
	
	@Override
	public int save(AuthRight model) {
		if(CommonUtils.isEmpty(model.getCode())){
			return CommonUtils.DEFAULT_NUM;
		}
		model.setCode(model.getModule()+CommonUtils.SPLIT_LINE + model.getCode());
		int returnCode = authRightDao.insert(model);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = authRightDao.insertInfo(model);
		}else{
			returnCode = CommonUtils.DEFAULT_NUM;
		}
		return returnCode;
	}

	@Override
	public int update(AuthRight model) {
		int returnCode = authRightDao.updateByCondition(model);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = authRightDao.updateInfoByCondition(model);
		}else{
			returnCode = CommonUtils.DEFAULT_NUM;
		}
		return returnCode;
	}
	
	@Override
	public int saveOtherLanguage(AuthRight model) {
		int returnCode = authRightDao.insertInfo(model);
		return returnCode;
	}

	@Override
	public int updateOtherLanguage(AuthRight model) {
		int returnCode = authRightDao.updateInfoByCondition(model);
		return returnCode;
	}
}
