package com.cloudinnov.dao;

import com.cloudinnov.model.EquipmentInspectionResult;

public interface EquipmentInspectionResultDao extends IBaseDao<EquipmentInspectionResult>{
   
}