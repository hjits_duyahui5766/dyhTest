package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentsCategoryAttrDao;
import com.cloudinnov.logic.EquipmentsCategoriesAttrLogic;
import com.cloudinnov.model.EquipmentsCategoryAttr;

@Service
public class EquipmentsCategoriesAttrLogicImpl extends BaseLogicImpl<EquipmentsCategoryAttr>
		implements EquipmentsCategoriesAttrLogic {

	@Autowired
	private EquipmentsCategoryAttrDao equipmentsCategoryAttrDao;

	/**
	 * 根据设备分类code，获取设备分类属性信息
	 * 
	 * @param equipmentsCategoryAttr
	 * @return
	 */
	public List<EquipmentsCategoryAttr> selectByCategoryCode(EquipmentsCategoryAttr equipmentsCategoryAttr) {
		return equipmentsCategoryAttrDao.selectListByCondition(equipmentsCategoryAttr);
	}
}
