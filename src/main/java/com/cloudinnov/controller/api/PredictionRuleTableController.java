package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.dao.PredictionRuleTableDao;
import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.PredictionRuleTableLogic;
import com.cloudinnov.model.EquipmentArea;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.model.PredictionRuleTable;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/predictionRule")
public class PredictionRuleTableController extends BaseController {
	static final Logger LOG = LoggerFactory.getLogger(PredictionRuleTableController.class);

	@Autowired
	private PredictionRuleTableLogic predictionRuleTableLogic;
	@Autowired
	private EquipmentsLogic equipmentsLogic;
	@Autowired
	private ControlSolutionLogic controlSolutionLogic;
	@Autowired
	private PredictionRuleTableDao predictionRuleTableDao;

	@RequestMapping(value = "/getAndSendRecoverPredictionRules", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "预案恢复方案生成,并下发命令执行", methods = "动态生成恢复预案")
	public void getAndSendRecoverPredictionRules(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> predictionRuleResult = new HashMap<>();
		if (predictionRuleTable != null) {
			String sectionCode = predictionRuleTable.getSectionCode();
			if (sectionCode != null) {
				predictionRuleTable.setIsRecover(1);
			} else {
				LOG.warn("动态生成预案恢复方案条件不足,查看发送条件的条件是否正确!!");
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
				response.getWriter().println(returnData);
			}
			predictionRuleResult = predictionRuleTableLogic.achieveAndSendRecoverPredictionRules(predictionRuleTable);
		}
		if (JudgeNullUtil.iMap(predictionRuleResult)) {
			map.put("data", predictionRuleResult);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/getPredictionAccidentTypes", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "返回预案表中对应的事故类型", methods = "返回预案规则中的事故类型")
	public void getPredictionAccidentTypes(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> accidentTypeMap = new HashMap<>();
		String returnData = "";
		// 往前端推送有什么预案事故类型
		List<String> accidentTypes = predictionRuleTableLogic.getAccidentTypes();
		if (JudgeNullUtil.iList(accidentTypes)) {
			accidentTypeMap.put("allAccidentType", accidentTypes);
			returnData = returnJsonAllRequest(request, response, accidentTypeMap, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, accidentTypeMap, ERROR, null);
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 手动点击按钮,弹出预案页面
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/getContacterAndSections", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "返回预案执行首页上的联系人和隧道及监控区域的信息", methods = "返回预案执行首页数据的方法")
	public void getContacterAndSections(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> prediction = new HashMap<>();
		String returnData = "";
		// 查询摄像机设备
//		List<Equipments> cameraEquipmentsBySeCode = equipmentsLogic.selectCameraEquipmentsBySeCode();
		List<Equipments> cameraEquipmentsBySeCode = equipmentsLogic.selectAllClassifyTunnelsAndAccidentArea();
		// 获取隧道事故时的紧急联系人方式
		PredictionContacter predictionContacter = new PredictionContacter();
		List<PredictionContacter> predictionContacters = predictionRuleTableDao
				.selectPredictionContacter(predictionContacter);
		if (JudgeNullUtil.iList(cameraEquipmentsBySeCode) && JudgeNullUtil.iList(predictionContacters)) {
			prediction.put("sectionsAndCameraEquipments", cameraEquipmentsBySeCode);
			prediction.put("predictionContacters", predictionContacters);
			returnData = returnJsonAllRequest(request, response, prediction, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, prediction, ERROR, null);
		}
		response.getWriter().print(returnData);
	}
	
	/**
	 * 手动弹出页面动态生成预案
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/manTrigGetPredictionRules", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "手动弹出页面动态生成预案", methods = "手动弹出页面动态生成预案方法")
	public void manTrigGetPredictionRules(String message, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> prediction = new HashMap<>();
		String returnData = "";

		// 调用预案动态生成方法
		Map<String, Object> predictionRules = predictionRuleTableLogic.manTrigGetPredictionRules(message);

		if (JudgeNullUtil.iMap(predictionRules)) {
			returnData = returnJsonAllRequest(request, response, predictionRules, SUCCESS, null);
		} else {
			predictionRules = prediction;
			returnData = returnJsonAllRequest(request, response, predictionRules, ERROR, null);
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 查询所有预案联系人方法
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectListContacters", method = RequestMethod.GET)
	@SystemLog(module = "查询所有预案联系人", methods = "查询所有预案联系人方法")
	public void selectListContacters(PredictionContacter predictionContacter, int index, int size,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		// 修改字符编码
		String parameter = predictionContacter.getName();
		String name = new String(parameter.getBytes("iso-8859-1"), "utf-8");
		predictionContacter.setName(name);
		Page<PredictionContacter> listContacters = predictionRuleTableLogic
				.selectPredictionContacter(predictionContacter, index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		if (JudgeNullUtil.iList(listContacters)) {
			map.put("listContacters", listContacters);
			map.put("total", listContacters.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 添加预案联系人方法
	 * 
	 * @param predictionContacter
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/insertContacters", method = RequestMethod.POST)
	@SystemLog(module = "添加预案联系人", methods = "添加预案联系人方法")
	public void insertContacters(PredictionContacter predictionContacter, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleTableLogic.insertContacters(predictionContacter);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 更新预案联系人方法
	 * 
	 * @param predictionContacter
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateContacters", method = RequestMethod.POST)
	@SystemLog(module = "更新预案联系人", methods = "更新预案联系人方法")
	public void updateContacters(PredictionContacter predictionContacter, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleTableLogic.updateContacters(predictionContacter);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 刪除预案联系人方法
	 * 
	 * @param predictionContacter
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/deleteContacters", method = RequestMethod.POST)
	@SystemLog(module = "更新预案联系人", methods = "更新预案联系人方法")
	public void deleteContacters(PredictionContacter predictionContacter, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleTableLogic.deleteContacters(predictionContacter);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 添加触发预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/insertPredictionRule", method = RequestMethod.POST)
	@SystemLog(module = "添加触发预案", methods = "添加触发预案方法")
	public void insertPredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleTableLogic.insertPredictionRule(predictionRuleTable);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			map.put("data", "该事故区域预案已经存在,请选择其他事故区域添加预案!");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 添加恢复预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/insertRecoverPredictionRule", method = RequestMethod.POST)
	@SystemLog(module = "添加触发预案", methods = "添加触发预案方法")
	public void insertRecoverPredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		
		int result = predictionRuleTableLogic.insertRecoverPredictionRule(predictionRuleTable);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			map.put("data", "该事故区域预案已经存在,请选择其他事故区域添加预案!");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询触发预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectPredictionRule", method = RequestMethod.GET)
	@SystemLog(module = "查询触发预案列表", methods = "查询触发预案列表")
	public void selectPredictionRule(PredictionRuleTable predictionRuleTable, int index, int size, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		if (predictionRuleTable.getPredictionName() != null && predictionRuleTable.getPredictionName() != "") {
			String predictionName = new String(predictionRuleTable.getPredictionName().getBytes("iso-8859-1"), "utf-8");
			predictionRuleTable.setPredictionName(predictionName);
		}
		Page<PredictionRuleTable> predictionRuleTables = predictionRuleTableLogic.selectPredictionRule(predictionRuleTable, index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		if (JudgeNullUtil.iList(predictionRuleTables)) {
			map.put("data", predictionRuleTables);
			map.put("total", predictionRuleTables.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询恢复预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectRecoverPredictionRule", method = RequestMethod.GET)
	@SystemLog(module = "查询触发预案列表", methods = "查询触发预案列表")
	public void selectRecoverPredictionRule(PredictionRuleTable predictionRuleTable, int index, int size, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		if (predictionRuleTable.getPredictionName() != null && predictionRuleTable.getPredictionName() != "") {
			String predictionName = new String(predictionRuleTable.getPredictionName().getBytes("iso-8859-1"), "utf-8");
			predictionRuleTable.setPredictionName(predictionName);
		}
		Page<PredictionRuleTable> predictionRuleTables = predictionRuleTableLogic.selectPredictionRule(predictionRuleTable, index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		if (JudgeNullUtil.iList(predictionRuleTables)) {
			map.put("data", predictionRuleTables);
			map.put("total", predictionRuleTables.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询单条触发预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectSinglePredictionRule", method = RequestMethod.GET)
	@SystemLog(module = "查询单条触发预案", methods = "查询单条触发预案方法")
	public void selectSinglePredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		String accidentType = new String(predictionRuleTable.getAccidentType().getBytes("iso-8859-1"), "utf-8");
		predictionRuleTable.setAccidentType(accidentType);
		//查询预案
		PredictionRuleTable predictionRuleTables = predictionRuleTableLogic.selectSinglePredictionRule(predictionRuleTable);
		//查询监控区域和触发设备
		Equipments equipments = new Equipments();
		equipments.setSectionCode(predictionRuleTable.getSectionCode());
		equipments.setAreaId(predictionRuleTable.getAccidentArea());
		List<EquipmentArea> triggerEquipmentArea = equipmentsLogic.selectCameraAreaAndTriggerEquipment(equipments);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (predictionRuleTables != null && JudgeNullUtil.iList(triggerEquipmentArea)) {
			map.put("data", predictionRuleTables);
			map.put("triggerData", triggerEquipmentArea);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询单条恢复预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectSingleRecoverPredictionRule", method = RequestMethod.GET)
	@SystemLog(module = "查询单条恢复预案", methods = "查询单条恢复预案方法")
	public void selectSingleRecoverPredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		//查询预案
		PredictionRuleTable predictionRuleTables = predictionRuleTableLogic.selectSinglePredictionRule(predictionRuleTable);
		Map<String, Object> map = new HashMap<String, Object>();
		if (predictionRuleTables != null ) {
			map.put("data", predictionRuleTables);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 更新触发预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updatePredictionRule", method = RequestMethod.POST)
	@SystemLog(module = "更新触发预案", methods = "更新触发预案方法")
	public void updatePredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";

		int result = predictionRuleTableLogic.updatePredictionRule(predictionRuleTable);

		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 更新恢复预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateRecoverPredictionRule", method = RequestMethod.POST)
	@SystemLog(module = "更新恢复预案", methods = "更新恢复预案方法")
	public void updateRecoverPredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		
		int result = predictionRuleTableLogic.updateRecoverPredictionRule(predictionRuleTable);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 删除预案方法
	 * 
	 * @param predictionRuleTable
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/deletePredictionRule", method = RequestMethod.POST)
	@SystemLog(module = "添加预案", methods = "添加预案方法")
	public void deletePredictionRule(PredictionRuleTable predictionRuleTable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		
		int result = predictionRuleTableLogic.deletePredictionRule(predictionRuleTable);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

}
