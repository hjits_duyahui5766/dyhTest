package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.BoardTemplateLogic;
import com.cloudinnov.model.BoardTemplate;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;

/**
 * 情报板模板
 * @ClassName: BoardTemplateContrllor
 * @Description: TODO
 * @author: ningmeng
 * @date: 2017年1月12日 上午11:39:40
 */
@Controller
@RequestMapping("/webapi/boardTemplate")
public class BoardTemplateContrllor extends BaseController {
	@Autowired
	private BoardTemplateLogic boardTemplateLogic;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@SystemLog(module = "情报板模板管理", methods = "信息保存")
	public void save(BoardTemplate model, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = boardTemplateLogic.save(model);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@SystemLog(module = "情报板模板管理", methods = "信息更新")
	public void update(BoardTemplate model, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = boardTemplateLogic.update(model);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@SystemLog(module = "情报板模板管理", methods = "信息删除")
	public void delete(BoardTemplate model, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = boardTemplateLogic.delete(model);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public void list(@Valid PageModel page, BindingResult error, BoardTemplate model, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		Page<BoardTemplate> boardTemps = boardTemplateLogic.selectListPage(page.getIndex(), page.getSize(), model, false);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", boardTemps);
		map.put("total", boardTemps.getTotal());
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/listByCateCode", method = RequestMethod.GET)
	public void list(BoardTemplate model, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		List<BoardTemplate> boardTemps = boardTemplateLogic.selectList(model, false);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", boardTemps);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public void select(BoardTemplate model, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		BoardTemplate boardTemps = boardTemplateLogic.select(model);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("model", boardTemps);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
}
