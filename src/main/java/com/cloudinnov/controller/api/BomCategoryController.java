package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.BomCategoryLogic;
import com.cloudinnov.model.BomCategory;
import com.cloudinnov.model.TreeObject;
import com.cloudinnov.utils.TreeUtil;
import com.github.pagehelper.Page;


/**
 * @author chengning
 * @date 2016年6月27日下午1:43:40
 * @email ningcheng@cloudinnov.com
 * @remark 元件分类Controller
 * @version 
 */
@Controller
@RequestMapping("/webapi/bom/category")
public class BomCategoryController extends BaseController {

	@Autowired
	private BomCategoryLogic bomCategoryLogic;

	/**
	 * save
	 * 
	 * @Description: 保存元件分类
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save")
	@ResponseBody
	@SystemLog(module = "元件分类", methods = "元件信息保存")
	public void save(BomCategory equipmentCategories, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		equipmentCategories.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = bomCategoryLogic.save(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * saveOtherLanguage
	 * 
	 * @Description: 添加元件分类其他语言信息
	 * @param @param
	 *            equipmentCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "元件分类", methods = "添加其他语言")
	public void saveOtherLanguage(BomCategory equipmentCategories, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = null;
		equipmentCategories.setLanguage(equipmentCategories.getOtherLanguage());
		int result = bomCategoryLogic.saveOtherLanguage(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件分类", methods = "修改其他语言")
	public void updateOtherLanguage(BomCategory equipmentCategories, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentCategories.setLanguage(equipmentCategories.getOtherLanguage());
		int result = bomCategoryLogic.updateOtherLanguage(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据category的code 删除元件
	 * @param @param
	 *            equipmentCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemLog(module = "元件分类", methods = "元件分类删除")
	public void delete(BomCategory equCate, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		int result = bomCategoryLogic.delete(equCate);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 更新元件分类
	 * @param @param
	 *            equipmentCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "元件分类", methods = "信息修改")
	public void update(BomCategory equipmentCategories, HttpServletRequest request,
			HttpServletResponse response, Model model)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		int result = bomCategoryLogic.update(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 分页查找所有元件分类
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, BomCategory ec, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		ec.setOemCode(getUserInfo(request).get(OEM_CODE));
		Page<BomCategory> equipmentCategory = bomCategoryLogic.selectListPage(index, size, ec, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentCategory != null) {
			map.put("list", equipmentCategory);
			map.put("total", equipmentCategory.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找元件
	 * @param @param
	 *            equipmentBomCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(BomCategory BomCategory, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		if (BomCategory.getOtherLanguage() != null && BomCategory.getOtherLanguage() != "") {
			BomCategory.setLanguage(BomCategory.getOtherLanguage());
		}
		BomCategory result = bomCategoryLogic.select(BomCategory);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("model", result);
		if (result != null) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(BomCategory ec, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		ec.setOemCode(getUserInfo(request).get(OEM_CODE));
		List<BomCategory> equipmentCategory = bomCategoryLogic.selectList(ec, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentCategory != null) {
			map.put("list", equipmentCategory);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

	/**
	 * addSelectList
	 * 
	 * @Description: 获取分类select数据 by guochao
	 * @param @param
	 *            BomCategory
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/addSelectList")
	@ResponseBody
	public void addSelectList(BomCategory BomCategory, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		String oemCode = (String) request.getAttribute("oemCode");
		BomCategory.setOemCode(oemCode);
		List<BomCategory> result = bomCategoryLogic.addSelectList(BomCategory);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * treeList
	 * 
	 * @Description: 分类列表 by guochao
	 * @param @param
	 *            BomCategory
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/treeList")
	@ResponseBody
	public void treeList(BomCategory BomCategory, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String oemCode = (String) request.getAttribute("oemCode");
		BomCategory.setOemCode(oemCode);
		List<BomCategory> result = bomCategoryLogic.addSelectList(BomCategory);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setIndexId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setText(result.get(i).getName());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/tableList")
	@ResponseBody
	public void tableList(String[] codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String oemCode = (String) request.getAttribute("oemCode");
		List<BomCategory> result = bomCategoryLogic.tableList(codes, language, oemCode);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
