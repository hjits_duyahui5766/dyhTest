package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysIndustryDao;
import com.cloudinnov.logic.SysIndustryLogic;
import com.cloudinnov.model.SysIndustry;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.PageHelper;

@Service
public class SysIndustryLogicImpl extends BaseLogicImpl<SysIndustry> implements SysIndustryLogic {
	
	@Autowired
	private SysIndustryDao sysIndustryDao;
	
	public int save(SysIndustry sysIndustry) {		
		String code = CodeUtil.sysIndustryCode(4);
		sysIndustry.setCode(code);
		if(sysIndustry.getParentId()==null){
			sysIndustry.setParentId(0);
			sysIndustry.setPath(code);
			sysIndustry.setGrade(1);
		}else{
			SysIndustry parent = sysIndustryDao.selectEntityByParentCode(sysIndustry);
			sysIndustry.setParentId(parent.getId());
			sysIndustry.setPath(parent.getCode() + CommonUtils.SPLIT_CLICK +code);
			sysIndustry.setGrade(parent.getGrade()+1);
			
		}
		int returnCode = sysIndustryDao.insert(sysIndustry);
		if(returnCode == CommonUtils.SUCCESS_NUM){
			returnCode = sysIndustryDao.insertInfo(sysIndustry);
		}
		return returnCode;
	}

	@Override
	public int saveOtherLanguage(SysIndustry sysIndustry) {
		// TODO Auto-generated method stub
		return sysIndustryDao.insertInfo(sysIndustry);
	}

	@Override
	public int updateOtherLanguage(SysIndustry sysIndustry) {
		// TODO Auto-generated method stub
		return sysIndustryDao.updateInfoByCondition(sysIndustry);
	}

	@Override
	public List<SysIndustry> tableList(String[] codes, String language) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("codes", codes);
		return sysIndustryDao.tableList(map);
	}

	@Override
	public List<SysIndustry> search(SysIndustry sysIndustry) {
		Map<String, Object> map = new HashMap<>();
		map.put("language", sysIndustry.getLanguage());
		if(sysIndustry.getParentIds()!=null){
			map.put("parentIds", sysIndustry.getParentIds().split(","));
		}
		return sysIndustryDao.search(map);
	}
	public List<SysIndustry> selectList(SysIndustry sysIndustry,boolean isOrderBy) {
		if(isOrderBy){
			PageHelper.orderBy("create_time desc");
		}	
		List<SysIndustry> list = sysIndustryDao.selectListByCondition(sysIndustry);
		Integer[] parentIds = new Integer[list.size()];
		for(int i = 0; i<list.size(); i++){
			parentIds[i] = list.get(i).getId();
		}
		Map<String, Object> map = new HashMap<>();
		map.put("language", sysIndustry.getLanguage());
		if(parentIds.length >0){
			map.put("parentIds", parentIds);
		}
		List<SysIndustry> childrens = sysIndustryDao.search(map);
		for(SysIndustry children: childrens){
			for(SysIndustry parent: list){
				if(parent!=null && children!=null && parent.getId()==children.getParentId()){
					parent.setChildren(true);
					break;
				}
			}
		}
		
		return list;
	}
}
