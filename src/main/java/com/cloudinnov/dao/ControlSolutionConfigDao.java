package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.ControlSolutionConfig;
import com.cloudinnov.model.ControlSolutionConfigFire;

public interface ControlSolutionConfigDao extends IBaseDao<ControlSolutionConfig> {
	public int deleteByPrimaryKey(Integer id);
	public int insert(ControlSolutionConfig record);
	public int insertSelective(ControlSolutionConfig record);
	public ControlSolutionConfig selectByPrimaryKey(Integer id);
	public int updateByPrimaryKeySelective(ControlSolutionConfig record);
	public int updateByPrimaryKey(ControlSolutionConfig record);
	/**
	 * 根据群控方案获取群控配置列表
	 * @Title: selectListBySolutionCode
	 * @Description: TODO
	 * @param record
	 * @return
	 * @return: List<ControlSolutionConfig>
	 */
	public List<ControlSolutionConfig> selectListBySolutionCode(ControlSolutionConfig record);
	public List<ControlSolutionConfig> selectListByConfigRecover(ControlSolutionConfig record);
	public List<ControlSolutionConfigFire> selectListByConfigFireRecover(ControlSolutionConfigFire record);
	public List<ControlSolutionConfig> selectListByEquCate(ControlSolutionConfig record);
	
	/**
	 * 根据报警设备ID，查找所在群控
	 */
	public List<ControlSolutionConfig> selectListByControlSolutionEquCode(ControlSolutionConfig record);
	
}
