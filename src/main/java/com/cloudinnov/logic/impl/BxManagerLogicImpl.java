package com.cloudinnov.logic.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.cloudinnov.dao.BoardSolutionConfigDao;
import com.cloudinnov.dao.ControlSolutionDao;
import com.cloudinnov.dao.ControlSolutionSendRecordItemDao;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.logic.BxManagerLogic;
import com.cloudinnov.model.BoardSolutionConfig;
import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.utils.BxClientUtil;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.TcpClientUtil;
import com.cloudinnov.utils.model.BxClientModel;
import com.cloudinnov.utils.model.ByteModelBack;
import com.dahua.main.NovaStarByteModel;
import com.dahua.main.NovaStarClientModel;
import com.dahua.main.NovaStarTcpUtlis;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service
public class BxManagerLogicImpl extends BaseLogicImpl<BxClientModel> implements BxManagerLogic {
    public final Logger LOG = LoggerFactory.getLogger(this.getClass());
    public static final String BOARD_DH_TYPE = "DH";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static final String STAY_TIME = "stayTime";
    private static final String SPEED = "speed";
    private static final String DISPLAY_TYPE = "displayType";
    private static final String FONT_COLOR = "color";
    private static final String FONT_SIZE = "fontSize";
    private static final String FONT_NAME = "fontName";
    private static final String FONT_COUNT = "fontCount";
    private static final String TOP = "top";
    private static final String LEFT = "left";
    private static final String ITEM = "item";
    private static final String ITEM_UUID = "uuid";
    private static final String PROGRAM_NAME = "programName";
    private static final String SOLUTION_NAME = "solutionName";
    private static final String SOLUTION_CODE = "solutionCode";
    private static final String BOARD_TEMPLATE_CODE = "boardTemplateCode";
    private static final String ROW = "row";
    private static final String CONTENT = "content";
    private static final String SPACING = "spacing";
    public static final String FILE_NAME = "play.lst";
    public static final String PLAY_LIST_START = "[playlist]\r\nitem_no={0}\r\n";
    public static final String PLAY_LIST_TEMPLATE = "item{0} = {1}, {2}, {3}, \\C{4}{5}\\f{6}{7}\\c{8}{9}{10}{11}\\S{12}{13}\r\n";
    public static final String PLAY_LIST_NOSTYLE_TEMPLATE = "item{0} = {1}, {2}, \\C{3}{4}\\f{5}{6}\\c{7}{8}{9}{10}\\S{11}{12}\r\n";
    public static final String PLAY_CONTENT_TEMPLATE_NOSPAC = "\\f{0}{1}\\C{2}{3}\\c{4}{5}{6}{7}{8}";
    public static final String PLAY_CONTENT_TEMPLATE = "\\f{0}{1}\\C{2}{3}\\S{4}\\c{5}{6}{7}{8}{9}";
    private static final int SOLUTION_TYPE_INFO_BOARD = 1;
    @SuppressWarnings("unused")
    private static final int SOLUTION_TYPE_INFO_OTHER = 2;
    private static final int SEND_STATUS_SUCCESS = 1;
    private static final int SEND_STATUS_ERROR = -1;
    @Autowired
    private ControlSolutionSendRecordItemDao controlSolutionItemLogDao;
    @Autowired
    private BoardSolutionConfigDao boardSolutionConfigDao;
    @Autowired
    private ControlSolutionDao controlSolutionDao;
    @Autowired
    private EquipmentsAttrDao equipmentsAttrDao;
    @Autowired
    private EquipmentsDao equipmentsDao;

    @Override
    public int sendTextsToInformationBoard(BxClientModel bxModel, String equipmentCode) {
        int returnCode = 0;
        if (bxModel != null && bxModel.getIp() != null && bxModel.getPort() != 0) {
            JSONArray texts = JSON.parseArray(bxModel.getText());// 多个文本
            ControlSolutionSendRecordItem itemLog = null;
            if (JudgeNullUtil.iList(texts)) {
                for (int i = 0; i < texts.size(); i++) {
                    itemLog = new ControlSolutionSendRecordItem();
                    bxModel.setDuration(texts.getJSONObject(i).getLongValue("duration"));
                    if (bxModel.getEffectiveLength() > 0) {
                        if (i == 0) {
                            BxClientUtil.deletePrograms(bxModel.getIp(), bxModel.getPort(), bxModel.getAlias());
                            bxModel.setStartTime(System.currentTimeMillis() + 2000);
                            bxModel.setEndTime(bxModel.getStartTime() + bxModel.getEffectiveLength() * 60000);
                        } else {
                            bxModel.setStartTime(bxModel.getStartTime() + bxModel.getEffectiveLength() * 60000
                                    + bxModel.getDuration() * 1000);
                            bxModel.setEndTime(bxModel.getEndTime() + bxModel.getEffectiveLength() * 60000);
                        }
                    }
                    bxModel.setProgramName(BxClientModel.PROGRAM_NAME + i);
                    bxModel.setText(texts.getJSONObject(i).getString("sendContent"));
                    itemLog.setEquipmentCode(equipmentCode);
                    itemLog.setSendContent(bxModel.getText());
                    try {
                        BxClientUtil.setScreenTexts(bxModel);
                        if (bxModel.isOK()) {
                            returnCode = 1;
                            itemLog.setStatus(returnCode);
                            LOG.debug("Send To InformationBoard Success,Data", bxModel);
                            controlSolutionItemLogDao.insert(itemLog);// 插入设备发送记录表
                        }
                    } catch (Exception e) {
                        LOG.error("Send To InformationBoard Error,Data:[" + bxModel + "]", e);
                        returnCode = -1;
                        itemLog.setStatus(returnCode);
                        controlSolutionItemLogDao.insert(itemLog);// 插入设备发送记录表
                    }
                }
            }
        }
        return returnCode;
    }
    @Override
    public Page<ControlSolutionSendRecordItem> selectListByEquCode(String equipmentCode, int size) {
        PageHelper.startPage(0, size);
        ControlSolutionSendRecordItem controlSolutionSendRecordItem = new ControlSolutionSendRecordItem();
        controlSolutionSendRecordItem.setEquipmentCode(equipmentCode);
        return (Page<ControlSolutionSendRecordItem>) controlSolutionItemLogDao
                .selectListByCondition(controlSolutionSendRecordItem);
    }
    @Override
    public int forcedToInforBorad(String code) {
        int returnCode = 0;
        // 根据情报板方案编码查询情报板方案
        BoardSolutionConfig model = null;// 设备控制配置
        ControlSolution controlSolutions = controlSolutionDao.selectControlSolutionByConCode(code);
        if (controlSolutions != null && controlSolutions.getSolutionType() == SOLUTION_TYPE_INFO_BOARD) {
            model = new BoardSolutionConfig();
            model.setSolutionCode(controlSolutions.getCode());
            List<BoardSolutionConfig> configs = boardSolutionConfigDao.selectBoardBySolutionCode(model);// 查询群控方案下的所有配置列表
            if (JudgeNullUtil.iList(configs)) {
                for (BoardSolutionConfig config : configs) {
                    if (config.getEquipmentCode() != null) {
                        model = new BoardSolutionConfig();
                        model.setSolutionCode(controlSolutions.getCode());
                        model.setEquipmentCode(config.getEquipmentCode());
                        List<BoardSolutionConfig> equConfigs = boardSolutionConfigDao.selectListBySolutionCode(model);// 查询同一设备下配置表
                        returnCode = sendTextsToBoardUserForSolution(config.getEquipmentCode(), equConfigs);
                    }
                }
            }
        }
        return returnCode;
    }
    @Override
    public List<ControlSolutionSendRecordItem> selectHistoryListByControlCode(String controlCode) {
        ControlSolutionSendRecordItem controlSolutionSendRecordItem = new ControlSolutionSendRecordItem();
        controlSolutionSendRecordItem.setCode(controlCode);
        return controlSolutionItemLogDao.selectListByCondition(controlSolutionSendRecordItem);
    }
    @Override
    public List<ControlSolutionSendRecordItem> selectHistoryListByEquCateCode(String equCateCode) {
        List<ControlSolutionSendRecordItem> list = new ArrayList<>();
        BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
        boardSolutionConfig.setEquipmentClassify(equCateCode);
        List<BoardSolutionConfig> boardList = boardSolutionConfigDao.selectListByCondition(boardSolutionConfig);
        ControlSolutionSendRecordItem controlSolutionSendRecordItem = null;
        for (BoardSolutionConfig boardModel : boardList) {
            controlSolutionSendRecordItem = new ControlSolutionSendRecordItem();
            controlSolutionSendRecordItem.setCode(boardModel.getSolutionCode());
            List<ControlSolutionSendRecordItem> recordList = controlSolutionItemLogDao
                    .selectListByCondition(controlSolutionSendRecordItem);
            for (ControlSolutionSendRecordItem record : recordList) {
                record.setSolutionName(boardModel.getName());
                record.setSolutionCode(boardModel.getSolutionCode());
            }
            list.addAll(recordList);
        }
        return list;
    }
    @Override
    public Map<String, Object> selectBoardSanSiText(String equipmentCode) {
        Map<String, Object> map = new HashMap<>();
        EquipmentsAttr equAttr = new EquipmentsAttr();
        equAttr.setEquipmentCode(equipmentCode);
        List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(equAttr);
        if (configList != null && configList.size() > 0) {
            BxClientModel bxModel = new BxClientModel();
            for (EquipmentsAttr attr : configList) {
                if (attr.getCustomTag() != null) {
                    if (attr.getCustomTag().equals(BxClientModel.IP)) {
                        bxModel.setIp(attr.getValue());
                    } else if (attr.getCustomTag().equals(BxClientModel.PORT)) {
                        bxModel.setPort(Integer.parseInt(attr.getValue()));
                    } else if (attr.getCustomTag().equals(BxClientModel.WIDHT)) {
                        bxModel.setWidth(Integer.parseInt(attr.getValue()));
                    } else if (attr.getCustomTag().equals(BxClientModel.HEIGHT)) {
                        bxModel.setHeight(Integer.parseInt(attr.getValue()));
                    }
                }
            }
            if (bxModel.getIp() != null && bxModel.getPort() != 0) {
                try {
                    String content = new TcpClientUtil().getContent(bxModel);
                    if (CommonUtils.isNotEmpty(content)) {
                        bxModel.setText(content);
                        List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                        // 000 节目序号(预置的节目不会返回节目编码)
                        // 00200 停留时间
                        // 01 出字方式
                        // 00000 出字速度
                        String[] contents = content.split("\\\\");
                        String params = contents[0];
                        // \fs3232\C036000\S03\c255255000000注意横风请减速慢行
                        map.put("width", bxModel.getWidth());
                        map.put("height", bxModel.getHeight());
                        map.put(STAY_TIME,
                                params.substring(params.length() - 12, params.length() - 7) != null
                                        ? Integer.parseInt(content.substring(4, 9)) / 100
                                        : 0);// 停留时间
                        map.put(DISPLAY_TYPE,
                                params.substring(params.length() - 7, params.length() - 5) != null
                                        ? Integer.parseInt(params.substring(params.length() - 8, params.length() - 6))
                                        : 0);
                        map.put(SPEED,
                                params.substring(params.length() - 5, params.length()) != null
                                        ? Integer.parseInt(params.substring(params.length() - 5, params.length())) / 50
                                        : 0);
                        int number = 1;
                        int k = 0;
                        int fontSize = 0;
                        while (content.indexOf("\\f", k) != -1 || content.indexOf("\\C", k) != -1) {
                            Map<String, Object> rowMap = new HashMap<String, Object>();
                            rowMap.put("row", number);
                            rowMap.put(LEFT, Integer.parseInt(
                                    content.substring(content.indexOf("\\C") + 2, content.indexOf("\\C") + 5)));
                            rowMap.put(TOP, Integer.parseInt(
                                    content.substring(content.indexOf("\\C") + 5, content.indexOf("\\C") + 8)));
                            rowMap.put(FONT_NAME, getFontNameDecode(
                                    content.substring(content.indexOf("\\f") + 2, content.indexOf("\\f") + 3)));
                            rowMap.put(FONT_SIZE, Integer.parseInt(
                                    content.substring(content.indexOf("\\f") + 3, content.indexOf("\\f") + 5)));
                            if ((int) rowMap.get(FONT_SIZE) == 0) {
                                rowMap.put(FONT_SIZE, fontSize);
                            }
                            fontSize = Integer.parseInt(
                                    content.substring(content.indexOf("\\f") + 3, content.indexOf("\\f") + 5));
                            String rgb = content.substring(content.indexOf("\\c") + 2, content.indexOf("\\c") + 14);
                            String color = CommonUtils.parseToColor(rgb);
                            content = content.substring(content.indexOf("\\c") + 14, content.length());
                            if (content.indexOf("\\") != -1) {
                                rowMap.put(CONTENT, content.substring(0, content.indexOf("\\")));
                                content = content.substring(content.indexOf("\\"), content.length());
                            } else {
                                rowMap.put(CONTENT, content);
                            }
                            rowMap.put(FONT_COLOR, color);
                            rowMap.put(FONT_COUNT,
                                    rowMap.get(CONTENT) != null ? String.valueOf(rowMap.get(CONTENT)).length() : 0);
                            rowData.add(rowMap);
                            number++;
                        }
                        map.put("data", rowData);
                        map.put("code", CommonUtils.SUCCESS_NUM);
                    }
                } catch (Exception e) {
                    LOG.error("method:[selectBoardSanSiText] is bad, data:  {},  eMsg:  {}", bxModel, e);
                    map.put("code", CommonUtils.SYSTEM_EXCEPTION_CODE);
                }
            } else {
                map.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
            }
        } else {
            map.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
        }
        return map;
    }
    public String getFontNameDecode(String fontName) {
        switch (fontName) {
            case "s":
                return "宋体";
            case "k":
                return "楷体";
            case "h":
                return "黑体";
            default:
                return "宋体";
        }
    }
    public String getFontNameEncode(String fontName) {
        switch (fontName) {
            case "宋体":
                return "s";
            case "楷体":
                return "k";
            case "黑体":
                return "h";
            default:
                return "s";
        }
    }
    @Override
    public int sendTextsBoardSanSi(BxClientModel bxModel, String equipmentCode) {
        String rMsg = null;
        if (CommonUtils.isNotEmpty(bxModel.getText())) {
            long sendTime = System.currentTimeMillis();
            JSONArray texts = JSON.parseArray(bxModel.getText());
            if (JudgeNullUtil.iList(texts)) {
                List<ControlSolutionSendRecordItem> sendLogs = new ArrayList<ControlSolutionSendRecordItem>();
                ControlSolutionSendRecordItem sendLog = null;
                StringBuffer content = new StringBuffer();
                for (int i = 0; i < texts.size(); i++) {
                    sendLog = new ControlSolutionSendRecordItem();
                    int displayType = texts.getJSONObject(i).getIntValue(DISPLAY_TYPE);
                    int stayTime = texts.getJSONObject(i).getIntValue(STAY_TIME);
                    String solutionCode = texts.getJSONObject(i).getString(SOLUTION_CODE);
                    String boardTemplateCode = texts.getJSONObject(i).getString(BOARD_TEMPLATE_CODE);
                    BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
                    boardSolutionConfig.setSolutionCode(solutionCode);
                    boardSolutionConfig.setName(texts.getJSONObject(i).getString(PROGRAM_NAME));
                    boardSolutionConfig.setBoardTemplateCode(boardTemplateCode);
                    boardSolutionConfig.setSendContent(texts.getJSONObject(i).getString("data"));
                    boardSolutionConfig.setDisplayType(displayType);
                    boardSolutionConfig.setStayTime(stayTime);
                    content.append("item" + i);
                    content.append("=");
                    content.append(stayTime * 100);// 百分之1秒
                    content.append(",");
                    content.append(displayType);
                    content.append(",");
                    if (displayType != 0 && displayType != 1) {// 出字方式不等于0标识有速度
                        content.append(texts.getJSONObject(i).getIntValue("speed") * 50 + ",");
                        boardSolutionConfig.setSpeed(texts.getJSONObject(i).getIntValue("speed") * 50);
                    } else {
                        content.append("0,");
                    }
                    boardSolutionConfigDao.updateByCondition(boardSolutionConfig);
                    List<BoardSolutionConfig.Data> rowDatas = JSON.parseArray(texts.getJSONObject(i).getString("data"),
                            BoardSolutionConfig.Data.class);
                    int size = 0;
                    boolean isSpac = false;
                    String spacing = "00";
                    String contentStr = "";
                    int count = 0;
                    for (BoardSolutionConfig.Data rowData : rowDatas) {
                        if (rowData.getContent().trim().length() == 0) {// 判断当行内容是否只有空格和没有填写内容
                            count++;
                            if (count == 1) {
                                content.append("\r\n");
                            }
                            continue;
                        }
                        size++;
                        if (rowData.getSpacing() != null && rowData.getSpacing() > 0) {// 是否有字体间隔
                            isSpac = true;
                            if (rowData.getSpacing() / 10 == 0) {// 如果字体间距小于10则进行补0操作
                                spacing = "0" + rowData.getSpacing();
                            } else {
                                spacing = String.valueOf(rowData.getSpacing());
                            }
                        }
                        String fontSize;
                        if (rowData.getFontSize() / 10 == 0) {// 如果字体小于10则进行补0操作
                            fontSize = "0" + rowData.getFontSize() + "0" + rowData.getFontSize();
                        } else {
                            fontSize = String.valueOf(rowData.getFontSize()) + String.valueOf(rowData.getFontSize());
                        }
                        String x, y;
                        if (rowData.getLeft() / 10 == 0) {// 如果距离左间距小于10则进行补0操作
                            x = "00" + rowData.getLeft();
                        } else if (rowData.getLeft() / 100 == 0) {// 如果距离左间距大于10小于100则进行补0操作
                            x = "0" + rowData.getLeft();
                        } else {
                            x = rowData.getLeft() + "";
                        }
                        if (rowData.getTop() / 10 == 0) {// 如果距离上间距小于10则进行补0操作
                            y = "00" + rowData.getTop();
                        } else if (rowData.getTop() / 100 == 0) {// 如果距离间距大于10小于100则进行补0操作
                            y = "0" + rowData.getTop();
                        } else {
                            y = rowData.getTop() + "";
                        }
                        bxModel.setFontColor(rowData.getColor());
                        if (!isSpac) {// 无间隔
                            content.append(MessageFormat.format(PLAY_CONTENT_TEMPLATE_NOSPAC, rowData.getFontName(),
                                    fontSize, x, y,
                                    bxModel.getFontColorRGB().getRed() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getRed(),
                                    bxModel.getFontColorRGB().getGreen() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getGreen(),
                                    bxModel.getFontColorRGB().getBlue() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getBlue(),
                                    "000", rowData.getContent()));
                        } else {// 有间隔
                            content.append(MessageFormat.format(PLAY_CONTENT_TEMPLATE, rowData.getFontName(), fontSize,
                                    x, y, spacing,
                                    bxModel.getFontColorRGB().getRed() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getRed(),
                                    bxModel.getFontColorRGB().getGreen() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getGreen(),
                                    bxModel.getFontColorRGB().getBlue() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getBlue(),
                                    "000", rowData.getContent()));
                        }
                        if (size == rowDatas.size()) {
                            content.append("\r\n");
                        }
                        contentStr += rowData.getContent();
                    }
                    sendLog.setCode(CodeUtil.generateRandom(8));
                    sendLog.setOperationUser(bxModel.getLoginName());
                    sendLog.setEquipmentCode(equipmentCode);
                    sendLog.setSendContent(contentStr);
                    sendLog.setConfigContent(JSON.toJSONString(boardSolutionConfig));
                    sendLog.setSendTime(sendTime + "");
                    sendLogs.add(sendLog);
                }
                if (CommonUtils.isNotEmpty(content.toString())) {
                    try {
                        rMsg = new TcpClientUtil().sendContent(bxModel, new ByteModelBack().setText(FILE_NAME,
                                MessageFormat.format(PLAY_LIST_START, texts.size()) + content.toString()));
                        LOG.debug("rMsg:" + rMsg);
                        if (CommonUtils.isNotEmpty(rMsg)) {
                            if (rMsg.equals(CommonUtils.SUCCESS_MSG)) {// 发送成功
                                for (ControlSolutionSendRecordItem log : sendLogs) {
                                    log.setSendStatus(SEND_STATUS_SUCCESS);
                                    controlSolutionItemLogDao.insert(log);
                                }
                                return CommonUtils.SUCCESS_NUM;
                            } else {// 发送失败
                                for (ControlSolutionSendRecordItem log : sendLogs) {
                                    log.setSendStatus(SEND_STATUS_ERROR);
                                    controlSolutionItemLogDao.insert(log);
                                }
                                return CommonUtils.ERROR_NUM;
                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        LOG.error("sendTextsBoardSanSi UnsupportedEncodingException Is Bad,Data:[" + bxModel + "]", e);
                    } catch (SocketException e1) {
                        LOG.error("sendTextsBoardSanSi SocketException Is Bad,Data:[" + bxModel + "]", e1);
                    }
                } else {
                    return CommonUtils.ERROR_NUM;
                }
            }
        }
        return CommonUtils.ERROR_NUM;
    }
    @Override
    public int sendTextBoardSanSi(BxClientModel bxModel, String equipmentCode) {
        String rMsg = null;
        if (CommonUtils.isNotEmpty(bxModel.getText())) {
            String content = null;
            int itemNoStart = 1;
            if (bxModel.getStyleIndex() == 0 || bxModel.getStyleIndex() == 1) {
                content = MessageFormat.format(PLAY_LIST_NOSTYLE_TEMPLATE, itemNoStart++, bxModel.getStayTime(),
                        bxModel.getStyleIndex(), bxModel.getHorizontal(), bxModel.getVertical(),
                        getFontNameEncode(bxModel.getFontName()), bxModel.getFontSize() + bxModel.getFontSize(),
                        bxModel.getFontColorRGB().getRed() == 0 ? "000" : bxModel.getFontColorRGB().getRed(),
                        bxModel.getFontColorRGB().getGreen() == 0 ? "000" : bxModel.getFontColorRGB().getGreen(),
                        bxModel.getFontColorRGB().getBlue() == 0 ? "000" : bxModel.getFontColorRGB().getBlue(),
                        bxModel.getFontColorRGB().getAlpha() == 0 ? "000" : bxModel.getFontColorRGB().getAlpha(),
                        bxModel.getSpacing(), bxModel.getText());
            } else {
                content = MessageFormat.format(PLAY_LIST_TEMPLATE, itemNoStart++, bxModel.getStayTime(),
                        bxModel.getStyleIndex(), bxModel.getSpeed(), bxModel.getHorizontal(), bxModel.getVertical(),
                        getFontNameEncode(bxModel.getFontName()), bxModel.getFontSize() + bxModel.getFontSize(),
                        bxModel.getFontColorRGB().getRed() == 0 ? "000" : bxModel.getFontColorRGB().getRed(),
                        bxModel.getFontColorRGB().getGreen() == 0 ? "000" : bxModel.getFontColorRGB().getGreen(),
                        bxModel.getFontColorRGB().getBlue() == 0 ? "000" : bxModel.getFontColorRGB().getBlue(),
                        bxModel.getFontColorRGB().getAlpha() == 0 ? "000" : bxModel.getFontColorRGB().getAlpha(),
                        bxModel.getSpacing(), bxModel.getText());
            }
            if (CommonUtils.isNotEmpty(content)) {
                try {
                    rMsg = new TcpClientUtil().sendContent(bxModel,
                            new ByteModelBack().setText(FILE_NAME, PLAY_LIST_START + content));
                } catch (UnsupportedEncodingException e) {
                    LOG.error("sendTextBoardSanSi UnsupportedEncodingException Is Bad,Data:[" + bxModel + "]", e);
                } catch (SocketException e1) {
                    LOG.error("sendTextBoardSanSi SocketException Is Bad,Data:[" + bxModel + "]", e1);
                }
            }
        }
        if (CommonUtils.isNotEmpty(rMsg)) {
            if (rMsg.equals(CommonUtils.SUCCESS_MSG)) {
                return CommonUtils.SUCCESS_NUM;
            }
        }
        return CommonUtils.ERROR_NUM;
    }
    @Override
    public boolean selectInfoBoardStatus(BxClientModel bxModel) {
        try {
            new TcpClientUtil().ping(bxModel);
        } catch (IOException e) {
            LOG.error("selectInfoBoardStatus Is Bad,error: ", e);
            return false;
        }
        return bxModel.isOK();
    }
    @Override
    public List<Map<String, Object>> selectTemplate() {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(
                    CommonUtils.class.getClassLoader().getResourceAsStream("play.properties"), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        int itemNo = Integer.parseInt(properties.getProperty("item_no"));
        if (itemNo > 0) {
            for (int i = 0; i < itemNo; i++) {
                String itemContent = properties.getProperty("item" + i);
                if (CommonUtils.isNotEmpty(itemContent)) {
                    String[] contens = itemContent.split(",");
                    Map<String, Object> map = new HashMap<String, Object>();
                    List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                    map.put("item", "item" + i);
                    map.put("stayTime", Integer.parseInt(contens[0]) / 100);// 停留时间
                    map.put("styleIndex", contens[1].trim());
                    map.put("speed", Integer.parseInt(contens[2].trim()) / 50);
                    int number = 1;
                    int k = 0;
                    String content = contens[3];
                    int fontSize = 0;
                    while (content.indexOf("\\f", k) != -1 || content.indexOf("\\C", k) != -1) {
                        Map<String, Object> rowMap = new HashMap<String, Object>();
                        rowMap.put("row", number);
                        rowMap.put("fontName", getFontNameDecode(
                                content.substring(content.indexOf("\\f") + 2, content.indexOf("\\f") + 3)));
                        rowMap.put("fontSize", Integer
                                .parseInt(content.substring(content.indexOf("\\f") + 3, content.indexOf("\\f") + 5)));
                        if ((int) rowMap.get("fontSize") == 0) {
                            rowMap.put("fontSize", fontSize);
                        }
                        fontSize = Integer
                                .parseInt(content.substring(content.indexOf("\\f") + 3, content.indexOf("\\f") + 5));
                        content = content.substring(content.indexOf("\\c") + 14, content.length());
                        if (content.indexOf("\\") != -1) {
                            rowMap.put("content", content.substring(0, content.indexOf("\\")));
                            content = content.substring(content.indexOf("\\"), content.length());
                        } else {
                            rowMap.put("content", content);
                        }
                        rowMap.put("fontCount", String.valueOf(rowMap.get("content")).length());
                        rowData.add(rowMap);
                        number++;
                    }
                    map.put("data", rowData);
                    data.add(map);
                }
            }
        }
        return data;
    }
    @Override
    public List<Map<String, Object>> selectTemplateByCateCode(String equipmentCode) {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        Equipments equipmentModel = new Equipments();
        equipmentModel.setCode(equipmentCode);
        equipmentModel = equipmentsDao.selectEntityByCondition(equipmentModel);
        if (equipmentModel != null && CommonUtils.isNotEmpty(equipmentModel.getCategoryCode())) {
            // 根据情报板的分类查询此分类下的所有方案
            BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
            boardSolutionConfig.setEquipmentClassify(equipmentModel.getCategoryCode());
            List<BoardSolutionConfig> boardSolutionConfigs = boardSolutionConfigDao
                    .selectListByCondition(boardSolutionConfig);
            for (int k = 0; k < boardSolutionConfigs.size(); k++) {// 遍历所有方案下的节目列表
                Map<String, Object> boardSolutionMap = new HashMap<String, Object>();
                boardSolutionMap.put(SOLUTION_CODE, boardSolutionConfigs.get(k).getSolutionCode());
                boardSolutionMap.put(BOARD_TEMPLATE_CODE, boardSolutionConfigs.get(k).getBoardTemplateCode());
                boardSolutionMap.put(ITEM_UUID, CodeUtil.generateRandom(8));
                boardSolutionMap.put(SOLUTION_NAME, boardSolutionConfigs.get(k).getName());
                List<BoardSolutionConfig> programList = boardSolutionConfigDao
                        .selectBoardListByEquCate(boardSolutionConfigs.get(k));
                int itemNo = 0;
                List<Map<String, Object>> programData = new ArrayList<Map<String, Object>>();
                for (int i = 0; i < programList.size(); i++) {
                    String itemContent = programList.get(i).getSendContent();
                    Map<String, Object> map = new HashMap<String, Object>();
                    List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                    map.put(BOARD_TEMPLATE_CODE, programList.get(i).getBoardTemplateCode());
                    map.put(PROGRAM_NAME, programList.get(i).getBoardTemplateName());
                    map.put(ITEM, "item" + itemNo);
                    map.put(ITEM_UUID, CodeUtil.generateRandom(8));
                    map.put(STAY_TIME,
                            (programList.get(i).getStayTime() != null ? programList.get(i).getStayTime() : 0));// 停留时间
                    if (programList.get(i).getDisplayType() != null && programList.get(i).getDisplayType() != 1) {
                        map.put(SPEED,
                                (programList.get(i).getSpeed() != null ? programList.get(i).getSpeed() : 0) * 50);// 出字速度
                    }
                    map.put(DISPLAY_TYPE, programList.get(i).getDisplayType());
                    List<BoardSolutionConfig.Data> items = JSON.parseArray(itemContent, BoardSolutionConfig.Data.class);
                    for (BoardSolutionConfig.Data item : items) {
                        Map<String, Object> rowMap = new HashMap<String, Object>();
                        rowMap.put(ROW, item.getRow());
                        rowMap.put(FONT_NAME, item.getFontName());
                        rowMap.put(FONT_SIZE, item.getFontSize());
                        rowMap.put(FONT_COLOR, item.getColor());
                        rowMap.put(CONTENT, item.getContent());
                        rowMap.put(SPACING, item.getSpacing());
                        rowMap.put(FONT_COUNT, item.getContent() != null ? item.getContent().length() : "");
                        rowMap.put(LEFT, item.getLeft());
                        rowMap.put(TOP, item.getTop());
                        rowData.add(rowMap);
                    }
                    map.put("data", rowData);
                    programData.add(map);
                    itemNo++;
                }
                boardSolutionMap.put("list", programData);
                data.add(boardSolutionMap);
            }
        } else {
            return Collections.emptyList();
        }
        return data;
    }
    @Override
    public List<Map<String, Object>> selectLastSendRecordByEquipmentCode(String equipmentCode) {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        if (CommonUtils.isNotEmpty(equipmentCode)) {
            // 查询发送记录
            ControlSolutionSendRecordItem logModel = new ControlSolutionSendRecordItem();
            logModel.setEquipmentCode(equipmentCode);
            List<ControlSolutionSendRecordItem> sendLogs = controlSolutionItemLogDao
                    .selectLastSendRecordByEquipmentCode(logModel);
            if (JudgeNullUtil.iList(sendLogs)) {
                int itemNo = 0;
                for (ControlSolutionSendRecordItem log : sendLogs) {
                    BoardSolutionConfig boardSolutionConfig = JSON.parseObject(log.getConfigContent(),
                            BoardSolutionConfig.class);
                    String itemContent = boardSolutionConfig.getSendContent();
                    Map<String, Object> map = new HashMap<String, Object>();
                    List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                    map.put(SOLUTION_CODE, boardSolutionConfig.getSolutionCode());
                    map.put(BOARD_TEMPLATE_CODE, boardSolutionConfig.getBoardTemplateCode());
                    map.put(ITEM, "item" + itemNo);
                    map.put(ITEM_UUID, log.getCode() + "-" + CodeUtil.generateRandom(8));
                    map.put(PROGRAM_NAME, boardSolutionConfig.getName());
                    map.put(STAY_TIME,
                            (boardSolutionConfig.getStayTime() != null ? boardSolutionConfig.getStayTime() : 0));// 停留时间
                    if (boardSolutionConfig.getDisplayType() != null && boardSolutionConfig.getDisplayType() != 1) {
                        map.put(SPEED,
                                (boardSolutionConfig.getSpeed() != null ? boardSolutionConfig.getSpeed() : 0) / 50);// 出字速度
                    }
                    map.put(DISPLAY_TYPE, boardSolutionConfig.getDisplayType());
                    List<BoardSolutionConfig.Data> items = JSON.parseArray(itemContent, BoardSolutionConfig.Data.class);
                    for (BoardSolutionConfig.Data item : items) {
                        Map<String, Object> rowMap = new HashMap<String, Object>();
                        rowMap.put(ROW, item.getRow());
                        rowMap.put(FONT_NAME, item.getFontName());
                        rowMap.put(FONT_SIZE, item.getFontSize());
                        rowMap.put(FONT_COLOR, item.getColor());
                        rowMap.put(CONTENT, item.getContent());
                        rowMap.put(SPACING, item.getSpacing());
                        rowMap.put(FONT_COUNT, item.getContent() != null ? item.getContent().length() : 0);
                        rowMap.put(LEFT, item.getLeft());
                        rowMap.put(TOP, item.getTop());
                        rowData.add(rowMap);
                    }
                    map.put("data", rowData);
                    data.add(map);
                    itemNo++;
                }
            }
        } else {
            return Collections.emptyList();
        }
        return data;
    }
    @Override
    public List<Map<String, Object>> selectSendRecordByEquipmentCode(String equipmentCode, int repeat) {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        if (CommonUtils.isNotEmpty(equipmentCode)) {
            // 查询发送记录
            ControlSolutionSendRecordItem logModel = new ControlSolutionSendRecordItem();
            logModel.setEquipmentCode(equipmentCode);
            List<ControlSolutionSendRecordItem> sendLogs = Collections.emptyList();
            if (repeat == 1) {
                sendLogs = controlSolutionItemLogDao.selectSendRecordByEquipmentCode(logModel);
            } else {
                sendLogs = controlSolutionItemLogDao.selectListByCondition(logModel);
            }
            if (JudgeNullUtil.iList(sendLogs)) {
                int itemNo = 0;
                for (ControlSolutionSendRecordItem log : sendLogs) {
                    BoardSolutionConfig boardSolutionConfig = JSON.parseObject(log.getConfigContent(),
                            BoardSolutionConfig.class);
                    String itemContent = boardSolutionConfig.getSendContent();
                    Map<String, Object> map = new HashMap<String, Object>();
                    List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                    map.put(SOLUTION_CODE, boardSolutionConfig.getSolutionCode());
                    map.put(BOARD_TEMPLATE_CODE, boardSolutionConfig.getBoardTemplateCode());
                    map.put(ITEM, "item" + itemNo);
                    map.put(ITEM_UUID, log.getCode() + "-" + CodeUtil.generateRandom(8));
                    map.put(PROGRAM_NAME, boardSolutionConfig.getName());
                    map.put(STAY_TIME,
                            (boardSolutionConfig.getStayTime() != null ? boardSolutionConfig.getStayTime() : 0));// 停留时间
                    if (boardSolutionConfig.getDisplayType() != null && boardSolutionConfig.getDisplayType() != 1) {
                        map.put(SPEED,
                                (boardSolutionConfig.getSpeed() != null ? boardSolutionConfig.getSpeed() : 0) / 50);// 出字速度
                    }
                    map.put(DISPLAY_TYPE, boardSolutionConfig.getDisplayType());
                    List<BoardSolutionConfig.Data> items = JSON.parseArray(itemContent, BoardSolutionConfig.Data.class);
                    for (BoardSolutionConfig.Data item : items) {
                        Map<String, Object> rowMap = new HashMap<String, Object>();
                        rowMap.put(ROW, item.getRow());
                        rowMap.put(FONT_NAME, item.getFontName());
                        rowMap.put(FONT_SIZE, item.getFontSize());
                        rowMap.put(FONT_COLOR, item.getColor());
                        rowMap.put(CONTENT, item.getContent());
                        rowMap.put(SPACING, item.getSpacing());
                        rowMap.put(FONT_COUNT, item.getContent() != null ? item.getContent().length() : 0);
                        rowMap.put(LEFT, item.getLeft());
                        rowMap.put(TOP, item.getTop());
                        rowData.add(rowMap);
                    }
                    map.put("data", rowData);
                    data.add(map);
                    itemNo++;
                }
            }
        } else {
            return Collections.emptyList();
        }
        return data;
    }
    @Override
    public int deleteSendRecordLog(ControlSolutionSendRecordItem model) {
        return controlSolutionItemLogDao.deleteByCondition(model);
    }
    @Override
    public int sendTextsToBoardUserForSolution(String equipmentCode, List<BoardSolutionConfig> configs) {
        EquipmentsAttr model = new EquipmentsAttr();
        long sendTime = System.currentTimeMillis();
        model.setEquipmentCode(equipmentCode);
        List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(model);
        if (configList != null && configList.size() > 0) {// 设置情报板基本参数
            BxClientModel bxModel = new BxClientModel();
            for (EquipmentsAttr attr : configList) {
                if (attr.getCustomTag() != null) {
                    if (attr.getCustomTag().equals(BxClientModel.IP)) {
                        bxModel.setIp(attr.getValue() != null ? attr.getValue() : "0");
                    } else if (attr.getCustomTag().equals(BxClientModel.PORT)) {
                        bxModel.setPort(Integer.parseInt(attr.getValue() != null ? attr.getValue() : "0"));
                    } else if (attr.getCustomTag().equals(BxClientModel.WIDHT)) {
                        bxModel.setWidth(Integer.parseInt(attr.getValue()));
                    } else if (attr.getCustomTag().equals(BxClientModel.HEIGHT)) {
                        bxModel.setHeight(Integer.parseInt(attr.getValue()));
                    }
                }
            }
            if (bxModel.getIp() != null && bxModel.getPort() != 0) {// 判断情报板ip和端口是否配置
                /*
                 * long sendTime = System.currentTimeMillis(); JSONArray texts =
                 * JSON.parseArray(config.getSendContent()); if (JudgeNullUtil.iList(texts)) {
                 */
                List<ControlSolutionSendRecordItem> sendLogs = new ArrayList<ControlSolutionSendRecordItem>();
                ControlSolutionSendRecordItem sendLog = null;
                StringBuffer content = new StringBuffer();
                for (int i = 0; i < configs.size(); i++) {
                    sendLog = new ControlSolutionSendRecordItem();
                    int displayType = configs.get(i).getDisplayType();
                    int stayTime;
                    if (configs.get(i).getStayTime() == null) {
                        stayTime = 0;
                    } else {
                        stayTime = configs.get(i).getStayTime();
                    }
                    content.append("item" + i);
                    content.append("=");
                    content.append(stayTime * 100);// 百分之1秒
                    content.append(",");
                    content.append(displayType);
                    content.append(",");
                    if (displayType != 0 && displayType != 1) {// 出字方式不等于0标识有速度
                        content.append(configs.get(i).getSpeed() * 50 + ",");
                    } else {
                        content.append("0,");
                    }
                    BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
                    boardSolutionConfig.setBoardTemplateCode(configs.get(i).getBoardTemplateCode());
                    boardSolutionConfig.setDisplayType(configs.get(i).getDisplayType());
                    boardSolutionConfig.setName(configs.get(i).getName());
                    boardSolutionConfig.setSendContent(configs.get(i).getSendContent());
                    boardSolutionConfig.setStayTime(configs.get(i).getStayTime());
                    boardSolutionConfigDao.updateByCondition(boardSolutionConfig);
                    List<BoardSolutionConfig.Data> rowDatas = JSON.parseArray(configs.get(i).getSendContent(),
                            BoardSolutionConfig.Data.class);
                    int size = 0;
                    boolean isSpac = false;
                    String spacing = "00";
                    String contentStr = "";
                    int count = 0;
                    for (BoardSolutionConfig.Data rowData : rowDatas) {
                        if (rowData.getContent().trim().length() == 0) {// 判断当行内容是否只有空格和没有填写内容
                            count++;
                            if (count == 1) {
                                content.append("\r\n");
                            }
                            continue;
                        }
                        size++;
                        if (rowData.getSpacing() != null && rowData.getSpacing() > 0) {// 是否有字体间隔
                            isSpac = true;
                            if (rowData.getSpacing() / 10 == 0) {// 如果字体间距小于10则进行补0操作
                                spacing = "0" + rowData.getSpacing();
                            } else {
                                spacing = String.valueOf(rowData.getSpacing());
                            }
                        }
                        String fontSize;
                        if (rowData.getFontSize() / 10 == 0) {// 如果字体小于10则进行补0操作
                            fontSize = "0" + rowData.getFontSize() + "0" + rowData.getFontSize();
                        } else {
                            fontSize = String.valueOf(rowData.getFontSize()) + String.valueOf(rowData.getFontSize());
                        }
                        String x, y;
                        if (rowData.getLeft() / 10 == 0) {// 如果距离左间距小于10则进行补0操作
                            x = "00" + rowData.getLeft();
                        } else if (rowData.getLeft() / 100 == 0) {// 如果距离左间距大于10小于100则进行补0操作
                            x = "0" + rowData.getLeft();
                        } else {
                            x = rowData.getLeft() + "";
                        }
                        if (rowData.getTop() / 10 == 0) {// 如果距离上间距小于10则进行补0操作
                            y = "00" + rowData.getTop();
                        } else if (rowData.getTop() / 100 == 0) {// 如果距离间距大于10小于100则进行补0操作
                            y = "0" + rowData.getTop();
                        } else {
                            y = rowData.getTop() + "";
                        }
                        bxModel.setFontColor(rowData.getColor());
                        if (!isSpac) {// 无间隔
                            content.append(MessageFormat.format(PLAY_CONTENT_TEMPLATE_NOSPAC, rowData.getFontName(),
                                    fontSize, x, y,
                                    bxModel.getFontColorRGB().getRed() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getRed(),
                                    bxModel.getFontColorRGB().getGreen() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getGreen(),
                                    bxModel.getFontColorRGB().getBlue() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getBlue(),
                                    "000", rowData.getContent()));
                        } else {// 有间隔
                            content.append(MessageFormat.format(PLAY_CONTENT_TEMPLATE, rowData.getFontName(), fontSize,
                                    x, y, spacing,
                                    bxModel.getFontColorRGB().getRed() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getRed(),
                                    bxModel.getFontColorRGB().getGreen() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getGreen(),
                                    bxModel.getFontColorRGB().getBlue() == 0 ? "000"
                                            : bxModel.getFontColorRGB().getBlue(),
                                    "000", rowData.getContent()));
                        }
                        if (size == rowDatas.size()) {
                            content.append("\r\n");
                        }
                        contentStr += rowData.getContent();
                    }
                    sendLog.setCode(CodeUtil.generateRandom(8));
                    sendLog.setEquipmentCode(equipmentCode);
                    sendLog.setSendContent(contentStr);
                    sendLog.setConfigContent(JSON.toJSONString(boardSolutionConfig));
                    sendLog.setSendTime(sendTime + "");
                    sendLogs.add(sendLog);
                }
                String rMsg = null;
                if (CommonUtils.isNotEmpty(content.toString())) {
                    try {
                        rMsg = new TcpClientUtil().sendContent(bxModel, new ByteModelBack().setText(FILE_NAME,
                                MessageFormat.format(PLAY_LIST_START, configs.size()) + content.toString()));
                        LOG.debug("rMsg:" + rMsg);
                        if (CommonUtils.isNotEmpty(rMsg)) {
                            if (rMsg.equals(CommonUtils.SUCCESS_MSG)) {// 发送成功
                                for (ControlSolutionSendRecordItem log : sendLogs) {
                                    log.setSendStatus(SEND_STATUS_SUCCESS);
                                    controlSolutionItemLogDao.insert(log);
                                }
                                return CommonUtils.SUCCESS_NUM;
                            } else {// 发送失败
                                for (ControlSolutionSendRecordItem log : sendLogs) {
                                    log.setSendStatus(SEND_STATUS_ERROR);
                                    controlSolutionItemLogDao.insert(log);
                                }
                                return CommonUtils.ERROR_NUM;
                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        LOG.error("sendTextsToBoardUserForSolution UnsupportedEncodingException Is Bad,Data:[" + bxModel
                                + "]", e);
                    } catch (SocketException e1) {
                        LOG.error("sendTextsToBoardUserForSolution SocketException Is Bad,Data:[" + bxModel + "]", e1);
                    }
                } else {
                    return CommonUtils.ERROR_NUM;
                }
            }
            // }
        }
        return CommonUtils.ERROR_NUM;
    }
    @Override
    public int sendTextsBoardNovaStar(BxClientModel bxModel, String equipmentCode) {
        if (CommonUtils.isNotEmpty(bxModel.getText())) {
            long sendTime = System.currentTimeMillis();
            JSONArray texts = JSON.parseArray(bxModel.getText());
            if (JudgeNullUtil.iList(texts)) {
                List<ControlSolutionSendRecordItem> sendLogs = new ArrayList<ControlSolutionSendRecordItem>();
                ControlSolutionSendRecordItem sendLog = null;
                StringBuffer content = new StringBuffer();
                for (int i = 0; i < texts.size(); i++) {
                    sendLog = new ControlSolutionSendRecordItem();
                    int displayType = texts.getJSONObject(i).getIntValue(DISPLAY_TYPE);
                    int stayTime = texts.getJSONObject(i).getIntValue(STAY_TIME);
                    String solutionCode = texts.getJSONObject(i).getString(SOLUTION_CODE);
                    String boardTemplateCode = texts.getJSONObject(i).getString(BOARD_TEMPLATE_CODE);
                    BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
                    boardSolutionConfig.setSolutionCode(solutionCode);
                    boardSolutionConfig.setName(texts.getJSONObject(i).getString(PROGRAM_NAME));
                    boardSolutionConfig.setBoardTemplateCode(boardTemplateCode);
                    boardSolutionConfig.setSendContent(texts.getJSONObject(i).getString("data"));
                    boardSolutionConfig.setDisplayType(displayType);
                    boardSolutionConfig.setStayTime(stayTime);
                    int speed = 0;
                    if (displayType != 0 && displayType != 1) {// 出字方式不等于0标识有速度
                        speed = texts.getJSONObject(i).getIntValue("speed") * 50;
                    }
                    boardSolutionConfigDao.updateByCondition(boardSolutionConfig);
                    List<BoardSolutionConfig.Data> rowDatas = JSON.parseArray(texts.getJSONObject(i).getString("data"),
                            BoardSolutionConfig.Data.class);
                    String contentStr = "";
                    for (BoardSolutionConfig.Data rowData : rowDatas) {
                        String fontSize;
                        if (rowData.getFontSize() / 10 == 0) {// 如果字体小于10则进行补0操作
                            fontSize = "0" + rowData.getFontSize() + "0" + rowData.getFontSize();
                        } else {
                            fontSize = String.valueOf(rowData.getFontSize()) + String.valueOf(rowData.getFontSize());
                        }
                        String x, y;
                        if (rowData.getLeft() / 10 == 0) {// 如果距离左间距小于10则进行补0操作
                            x = "00" + rowData.getLeft();
                        } else if (rowData.getLeft() / 100 == 0) {// 如果距离左间距大于10小于100则进行补0操作
                            x = "0" + rowData.getLeft();
                        } else {
                            x = rowData.getLeft() + "";
                        }
                        if (rowData.getTop() / 10 == 0) {// 如果距离上间距小于10则进行补0操作
                            y = "00" + rowData.getTop();
                        } else if (rowData.getTop() / 100 == 0) {// 如果距离间距大于10小于100则进行补0操作
                            y = "0" + rowData.getTop();
                        } else {
                            y = rowData.getTop() + "";
                        }
                        bxModel.setFontColor(rowData.getColor());
                        String fontName = rowData.getFontName();
                        int fontNameInt = 0;
                        if (fontName.equals("h")) {
                            fontNameInt = 1;
                        } else if (fontName.equals("k")) {
                            fontNameInt = 2;
                        } else if (fontName.equals("s")) {
                            fontNameInt = 3;
                        }
                        String fontColor = rowData.getColor();
                        int fontColorInt = 0;
                        if (fontColor.equals("red")) {
                            fontColorInt = 1;
                        } else if (fontColor.equals("green")) {
                            fontColorInt = 2;
                        } else if (fontColor.equals("yellow")) {
                            fontColorInt = 4;
                        }
                        String text = new NovaStarByteModel().getProgramData(i, stayTime, displayType, speed,
                                fontNameInt, x, y, fontSize, fontColorInt, rowData.getContent());
                        content.append(text);
                    }
                    sendLog.setCode(CodeUtil.generateRandom(8));
                    sendLog.setOperationUser(bxModel.getLoginName());
                    sendLog.setEquipmentCode(equipmentCode);
                    sendLog.setSendContent(contentStr);
                    sendLog.setConfigContent(JSON.toJSONString(boardSolutionConfig));
                    sendLog.setSendTime(sendTime + "");
                    sendLogs.add(sendLog);
                }
                if (CommonUtils.isNotEmpty(content.toString())) {
                    NovaStarTcpUtlis bxTcpUtlis = new NovaStarTcpUtlis();
                    NovaStarClientModel novaModel = new NovaStarClientModel();
                    novaModel.setIp(bxModel.getIp());
                    novaModel.setPort(bxModel.getPort());
                    novaModel.setText(content.toString());
                    boolean sendStatus = bxTcpUtlis.sendContent(novaModel);// 发送内容至诺瓦情报板
                    LOG.debug("sendStatus:" + sendStatus);
                    if (sendStatus) {
                        for (ControlSolutionSendRecordItem log : sendLogs) {
                            log.setSendStatus(SEND_STATUS_SUCCESS);
                            controlSolutionItemLogDao.insert(log);
                        }
                        return CommonUtils.SUCCESS_NUM;
                    } else {// 发送失败
                        LOG.error("发送内容至情报板返回错误");
                        for (ControlSolutionSendRecordItem log : sendLogs) {
                            log.setSendStatus(SEND_STATUS_ERROR);
                            controlSolutionItemLogDao.insert(log);
                        }
                        return CommonUtils.ERROR_NUM;
                    }
                } else {
                    LOG.error("发送内容为空,请检查");
                    return CommonUtils.ERROR_NUM;
                }
            }
        }
        return CommonUtils.ERROR_NUM;
    }
    @Override
    public Map<String, Object> selectBoardSanNova(String equipmentCode) {
        Map<String, Object> map = new HashMap<>();
        EquipmentsAttr equAttr = new EquipmentsAttr();
        equAttr.setEquipmentCode(equipmentCode);
        List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByCondition(equAttr);
        if (configList != null && configList.size() > 0) {
            NovaStarTcpUtlis bxTcpUtlis = new NovaStarTcpUtlis();
            NovaStarClientModel bxClientModel = new NovaStarClientModel();
            for (EquipmentsAttr attr : configList) {
                if (attr.getCustomTag() != null) {
                    if (attr.getCustomTag().equals(BxClientModel.IP)) {
                        bxClientModel.setIp(attr.getValue());
                    } else if (attr.getCustomTag().equals(BxClientModel.PORT)) {
                        bxClientModel.setPort(Integer.parseInt(attr.getValue()));
                    } else if (attr.getCustomTag().equals(BxClientModel.WIDHT)) {
                        bxClientModel.setWidth(Integer.parseInt(attr.getValue()));
                    } else if (attr.getCustomTag().equals(BxClientModel.HEIGHT)) {
                        bxClientModel.setHeight(Integer.parseInt(attr.getValue()));
                    }
                }
            }
            if (bxClientModel.getIp() != null && bxClientModel.getPort() != 0) {
                try {
                    String content = bxTcpUtlis.getContent(bxClientModel);// 获取当前播放内容
                    if (CommonUtils.isNotEmpty(content)) {
                        String[] contents = content.split("\r\n");
                        String[] params = contents[2].split("=")[1].split(",");
                        int stayTime = Integer.parseInt(params[0]);
                        int display = Integer.parseInt(params[1]);
                        int speed = Integer.parseInt(params[8]);
                        bxClientModel.setText(content);
                        List<Map<String, Object>> rowData = new ArrayList<Map<String, Object>>();
                        map.put(WIDTH, bxClientModel.getWidth());
                        map.put(HEIGHT, bxClientModel.getHeight());
                        map.put(STAY_TIME, stayTime);// 停留时间
                        map.put(DISPLAY_TYPE, display);
                        map.put(SPEED, speed);
                        int number = 1;
                        for (int i = 3; i < contents.length; i += 2) {
                            Map<String, Object> rowMap = new HashMap<String, Object>();
                            rowMap.put("row", number);
                            String[] rowTexts = contents[i].split("=")[1].split(",");
                            rowMap.put(LEFT, Integer.parseInt(rowTexts[0]));
                            rowMap.put(TOP, Integer.parseInt(rowTexts[1]));
                            rowMap.put(FONT_NAME, getFontNameDecodeToNova(rowTexts[2]));
                            rowMap.put(FONT_SIZE, Integer.parseInt(rowTexts[3].substring(2)));
                            rowMap.put(FONT_COLOR, getFontColorDecodeToNova(rowTexts[4]));
                            rowMap.put(CONTENT, rowTexts[7]);
                            rowMap.put(FONT_COUNT, rowTexts[7].length());
                            rowData.add(rowMap);
                            number++;
                        }
                        map.put("data", rowData);
                        map.put("code", CommonUtils.SUCCESS_NUM);
                    }
                } catch (Exception e) {
                    LOG.error("method:[selectBoardSanNova] is bad, data:  {},  e:  {}", bxClientModel, e);
                    map.put("code", CommonUtils.SYSTEM_EXCEPTION_CODE);
                }
            } else {
                map.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
            }
        } else {
            map.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
        }
        return map;
    }
    public String getFontNameDecodeToNova(String fontName) {
        switch (fontName) {
            case "1":
                return "黑体";
            case "2":
                return "楷体";
            case "3":
                return "宋体";
            default:
                return "宋体";
        }
    }
    public String getFontColorDecodeToNova(String fontColor) {
        switch (fontColor) {
            case "1":
                return "red";
            case "2":
                return "green";
            case "3":
                return "blue";
            case "4":
                return "yellow";
            default:
                return "red";
        }
    }
}
