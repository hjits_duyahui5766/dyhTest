package com.cloudinnov.logic;

import com.cloudinnov.model.AccidentWorkOrder;

public interface AccidentWorkOrderLogic extends IBaseLogic<AccidentWorkOrder> {
}
