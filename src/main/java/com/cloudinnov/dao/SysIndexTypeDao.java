package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.SysIndexType;

public interface SysIndexTypeDao  extends IBaseDao<SysIndexType>{
	public int insertInfo(SysIndexType sysIndexType);

	public int updateInfoByCondition(SysIndexType sysIndexType);
	
	/** 根据设备查询设备下的点位指标
	 * @Title: selectIndexTypeListByEquipmentCodes 
	 * @Description: TODO
	 * @param map
	 * @return
	 * @return: List<SysIndexType>
	 */
	public List<SysIndexType> selectIndexTypeListByEquipmentCodes(Map<String, Object> map);
}