package com.cloudinnov.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AlarmLog extends BaseModel{

    private String equipmentCode;
    
    private String solutionCode;
    
    private String solutionName;

	private String channelCode;

    private String faultCode;

    private Integer status;

    private String createTime;
    
    private String firstTime;
    
    private String workOrderCode;
    
    private String recoveryTime;

    private Integer count;

    @JsonIgnore
    String country;
    
    @JsonIgnore
    private String province;
    
    @JsonIgnore
    private String city; 
    
    @JsonIgnore
    private String key;
    
    private Integer type;
    
    private Integer level;
    
    private String description;
    
    private String handingSuggestion;

    private Companies company;
    
    private Equipments equipment;
    
    private Integer orderStatus;
    
    private String fault;
    
    private String triggerEquipNames;
    
    private String triggerConditions;
    
    private String triggerEquipPiles;
    
    private String triggerType;

    public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public String getSolutionName() {
		return solutionName;
	}

	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}
	
    public String getTriggerEquipPiles() {
		return triggerEquipPiles;
	}

	public void setTriggerEquipPiles(String triggerEquipPiles) {
		this.triggerEquipPiles = triggerEquipPiles;
	}

	public String getTriggerConditions() {
		return triggerConditions;
	}

	public void setTriggerConditions(String triggerConditions) {
		this.triggerConditions = triggerConditions;
	}

	public String getTriggerEquipNames() {
		return triggerEquipNames;
	}

	public void setTriggerEquipNames(String triggerEquipNames) {
		this.triggerEquipNames = triggerEquipNames;
	}

	public String getSolutionCode() {
		return solutionCode;
	}

	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}

	public String getFault() {
		return fault;
	}

	public void setFault(String fault) {
		this.fault = fault;
	}

	public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

	public String getFirstTime() {
		return firstTime;
	}

	public void setFirstTime(String firstTime) {
		this.firstTime = firstTime;
	}

	public String getWorkOrderCode() {
		return workOrderCode;
	}

	public void setWorkOrderCode(String workOrderCode) {
		this.workOrderCode = workOrderCode;
	}

	public String getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(String recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHandingSuggestion() {
		return handingSuggestion;
	}

	public void setHandingSuggestion(String handingSuggestion) {
		this.handingSuggestion = handingSuggestion;
	}

	public Companies getCompany() {
		return company;
	}

	public void setCompany(Companies company) {
		this.company = company;
	}

	public Equipments getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
    
    
}