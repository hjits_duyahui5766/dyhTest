package com.cloudinnov.model;

public class Environment {

	public final static String Light="lightId";
	private String lightId;
	private String currentTime;
	private String equipmentCode;
	private String categoryCode;
	public String getLightId() {
		return lightId;
	}
	public void setLightId(String lightId) {
		this.lightId = lightId;
	}
	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public static String getLight() {
		return Light;
	}
	
}
