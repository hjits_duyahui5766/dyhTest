package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.ProductionSchedule;


/**
 * @author nilixin
 * @date 2016年2月24日下午2:01:33
 * @email 
 * @remark 生产安排表service接口
 * @version
 */
public interface ProductionSchedulesLogic extends IBaseLogic<ProductionSchedule>{

	/** 查询生产调度表来查询生成调度用于产能报表
	* selectListStatByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionSchedules
	* @param @return    参数
	* @return List<ProductionSchedule>    返回类型 
	*/
	public List<ProductionSchedule> selectListStatByCondition(ProductionSchedule productionSchedules);
}
