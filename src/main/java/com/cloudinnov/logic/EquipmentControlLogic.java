package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.utils.control.ControlModel;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月13日上午11:31:37
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface EquipmentControlLogic {
	/**
	 * 根据设备控制命令控制设备
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @return 参数
	 * @return int 返回类型
	 */
	int sendContrlCommands(String command);
	/**
	 * 根据群控方案查询设备以及config
	 * @Description:匹配config中的点位与实际设备下的点位,如果存在则生成控制命令
	 * @param controlSolution
	 * @return 参数
	 * @return List<ControlModel> 返回类型
	 */
	List<ControlModel> generateControllerCommond(ControlSolution controlSolution);
	
	/**
	 * 根据动态预案匹配的设备生成bolomi命令
	 * @Description:匹配config中的点位与实际设备下的点位,如果存在则生成控制命令
	 * @param equipment
	 * @return 参数
	 * @return List<ControlModel> 返回类型
	 */
	List<ControlModel> generateControllerCommond(Equipments equipment);
}
