package com.cloudinnov.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @author chengning
 * @date 2016年2月27日下午3:48:07
 * @email ningcheng@cloudinnov.com
 * @remark 读取配置文件的工具类
 * @version
 */
public class PropertiesUtils {
	/**
	 * 获取属性文件的数据 根据key获取值
	 * 
	 * @param fileName
	 *            文件名　(注意：加载的是src下的文件,如果在某个包下．请把包名加上)
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public static String findPropertiesKey(String key) {
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(CommonUtils.class.getClassLoader().getResourceAsStream("config.properties"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

		return (String) properties.get(key);
	}
	
	/**
	 * 获取属性文件的数据 根据key获取值
	 * 
	 * @param fileName
	 *            文件名　(注意：加载的是src下的文件,如果在某个包下．请把包名加上)
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public static String findPropertiesKey(String name,String key) {
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(CommonUtils.class.getClassLoader().getResourceAsStream(name), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

		return (String) properties.get(key);
	}


	/**
	 * getProperties
	 * 
	 * @Description: 读取配置文件
	 * @param @return fileName 文件名　(注意：加载的是src下的文件,如果在某个包下．请把包名加上)
	 * @return Properties 返回类型
	 */
	public static Properties getProperties() {
		Properties prop = new Properties();
		String savePath = PropertiesUtils.class.getResource(
				"/config_" +CommonUtils.ENV+ ".properties").getPath();
		try {
			prop.load(new InputStreamReader(CommonUtils.class.getClassLoader().getResourceAsStream(savePath), "UTF-8"));
		} catch (Exception e) {
			return null;
		}
		return prop;
	}

	/**
	 * getjdbcProperties
	 * 
	 * @Description: 读取jdbc配置文件
	 * @param @return 参数
	 * @return Properties 返回类型
	 */
	public static Properties getjdbcProperties() {
		Properties prop = new Properties();
		String savePath = PropertiesUtils.class.getResource("/jdbc.properties")
				.getPath();
		// 以下方法读取属性文件会缓存问题
		// InputStream in = PropertiesUtils.class
		// .getResourceAsStream("/config.properties");
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(
					savePath));
			prop.load(in);
			in.close();
		} catch (Exception e) {
			return null;
		}
		return prop;
	}

	/**
	 * modifyProperties
	 * 
	 * @Description: 写入properties信息
	 * @param @param key 名称
	 * @param @param value 值
	 * @return void 返回类型
	 */
	public static void modifyProperties(String key, String value) {
		try {
			// 从输入流中读取属性列表（键和元素对）
			Properties prop = getProperties();
			prop.setProperty(key, value);
			String path = PropertiesUtils.class.getResource(
					"/config_" +CommonUtils.ENV+ ".properties").getPath();
			FileOutputStream outputFile = new FileOutputStream(path);
			prop.store(outputFile, "modify");
			outputFile.close();
			outputFile.flush();
		} catch (Exception e) {
		}
	}
}
