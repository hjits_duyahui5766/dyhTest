package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.CapacityStat;
import com.cloudinnov.model.ProductionLines;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年4月13日上午9:23:21
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface CapacityStatLogic extends IBaseLogic<CapacityStat> {

	/** 查询产能统计
	 * 可以选择equCode,lineCode,customerCode作为条件
	* selectFaultStatsByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsByCondition(Map<String, Object> map);
	
	/** 产能详情列表 分页
	* selectCapacityStatsDetailByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return Page<FaultStat>    返回类型 
	*/
	public Page<CapacityStat> selectCapacityStatsDetailByCondition(Map<String, Object> map);
	
	/** 产能小时查询
	* selectFaultStatsDetailByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return Page<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsHourByCondition(Map<String, Object> map);
	
	/** 产能按天查询列表分页
	* selectFaultStatsDayByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return Page<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsDayByCondition(Map<String, Object> map);
	

	/** 对连接产能的websocket所有客户端推送产能信息
	* energyPublish 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param     参数
	* @return void    返回类型 
	*/
	public void energyPublishAll();
	
	/** 根据客户获取产线数据(产能，开机时间等)
	* energyPublishByCustomer 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param params
	* @param @return    参数
	* @return List<ProductionLines>    返回类型 
	*/
	public void selectCapacityByProductLine(ProductionLines productionLine);
	
	public List<ProductionLines> energyPublishByCustomer(String[] params);
}
