package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.PredictionRuleLogDao;
import com.cloudinnov.dao.PredictionRuleLogDetailsDao;
import com.cloudinnov.logic.PredictionRuleLogLogic;
import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleLogDetails;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service("predictionRuleLogLogic")
public class PredictionRuleLogLogicImpl extends BaseLogicImpl<PredictionRuleLog> implements PredictionRuleLogLogic{

	@Autowired
	private PredictionRuleLogDao predictionRuleLogDao;
	@Autowired
	private PredictionRuleLogDetailsDao predictionRuleLogDetailsDao;
	
	@Override
	public String insertPredictionRuleLog(PredictionRuleLog predictionRuleLog) {
		List<PredictionRuleLogDetails> listPredictionRuleLogDetails = null;
		String predictionRuleLogDetailStr = predictionRuleLog.getPredictionRuleLogDetailStr();
		if (JudgeNullUtil.iStr(predictionRuleLogDetailStr)) {
			listPredictionRuleLogDetails = JSON.parseArray(predictionRuleLogDetailStr, PredictionRuleLogDetails.class);
		}
		//生成预案执行记录的唯一标识code
		String code = CodeUtil.predictionLogCode(PredictionRuleTableLogicImpl.digit);
		predictionRuleLog.setCode(code);
		int result = predictionRuleLogDao.insertPredictionRuleLog(predictionRuleLog);
		if (result == SUCCESS_NUM && JudgeNullUtil.iList(listPredictionRuleLogDetails)) {
			//插入预案执行记录详情表
			for (PredictionRuleLogDetails predictionRuleLogDetails : listPredictionRuleLogDetails) {
				predictionRuleLogDetails.setPredictionLogCode(code);
				predictionRuleLogDetailsDao.insertPredictionRuleLogDetails(predictionRuleLogDetails);
			}
		}
		return code;
	}

	@Override
	public Page<PredictionRuleLog> selectPredictionRuleLog(PredictionRuleLog predictionRuleLog, int index, int size) {
		PageHelper.startPage(index, size);
		List<PredictionRuleLog> predictionRuleLogs = predictionRuleLogDao.selectPredictionRuleLog(predictionRuleLog);
		return (Page<PredictionRuleLog>)predictionRuleLogs;
	}

	@Override
	public int updatePredictionRuleLog(PredictionRuleLog predictionRuleLog) {
	
		return predictionRuleLogDao.updatePredictionRuleLog(predictionRuleLog);
	}


}
