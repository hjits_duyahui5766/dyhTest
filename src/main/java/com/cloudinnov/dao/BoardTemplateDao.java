package com.cloudinnov.dao;

import com.cloudinnov.model.BoardTemplate;

public interface BoardTemplateDao extends IBaseDao<BoardTemplate>{
    
}