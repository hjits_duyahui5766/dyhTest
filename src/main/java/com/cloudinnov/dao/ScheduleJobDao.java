package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.task.model.ScheduleJob;

public interface ScheduleJobDao extends IBaseDao<ScheduleJob> {

	List<ScheduleJob> getAll(ScheduleJob model);

	List<ScheduleJob> selectListByJobGroup(String jobGroup);

}