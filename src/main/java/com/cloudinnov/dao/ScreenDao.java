package com.cloudinnov.dao;

import com.cloudinnov.model.Screen;

public interface ScreenDao extends IBaseDao<Screen> {
	
	int deleteAll();
    
}