package com.cloudinnov.dao;

import com.cloudinnov.model.AuthRole;

public interface AuthRoleDao extends IBaseDao<AuthRole> {
	public int insertInfo(AuthRole authRole);

	public int update(AuthRole authRole);
}