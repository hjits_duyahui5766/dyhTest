package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.SparepartPreventionCondition;

public interface SparepartPreventionConditionLogic extends IBaseLogic<SparepartPreventionCondition> {
	List<SparepartPreventionCondition> selectListByConfigId(Integer configId);
}
