package com.cloudinnov.logic;

import com.cloudinnov.model.InspectionWorkOrder;
import com.github.pagehelper.Page;

/**
 * @author guobo
 * @date 2016年12月6日 下午3:08:15
 * @email boguo@cloudinnov.com
 * @remark
 * @version
 */
public interface InspectionWorkOrderLogic extends IBaseLogic<InspectionWorkOrder> {
	Page<InspectionWorkOrder> search(int index, int size, InspectionWorkOrder iwo, String key);
}
