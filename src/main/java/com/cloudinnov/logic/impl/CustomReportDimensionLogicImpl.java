package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.CustomReportDimensionLogic;
import com.cloudinnov.model.CustomReportDimension;

@Service
public class CustomReportDimensionLogicImpl extends BaseLogicImpl<CustomReportDimension>
		implements CustomReportDimensionLogic {

}
