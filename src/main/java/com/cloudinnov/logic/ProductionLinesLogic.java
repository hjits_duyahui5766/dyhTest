package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Companies;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLineEquipments;
import com.cloudinnov.model.ProductionLines;

/**
 * @author guochao
 * @date 2016年2月17日下午2:01:33
 * @email chaoguo@cloudinnov.com
 * @remark 生产线service接口
 * @version
 */
/**
 * @author 程宁
 *
 */
public interface ProductionLinesLogic extends IBaseLogic<ProductionLines>{


	/**
	 * selectProductionLineTotalByCustomerCode
	 * 
	 * @Description: 根据客户Code查询该客户下的所有产线总数
	 * @param @param customerId
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int selectProductionLineTotalByCustomerCode(String comCode);
	
	public int selectProductionLineTotalByOemCode(Companies company);
	
	public int insertProductLineEquipment(ProductionLines productionLine);
	
	public int updateProductLineEquipment(ProductionLines productionLine);
	
	public List<Map<String, Object>> selectEquipmentByProLineCode(ProductionLineEquipments productionLineEqu);
	
	
	/**查询所有产线
	 * @param index
	 * @param size
	 * @param productionLine
	 * @return
	 */
	public List<ProductionLines> selectProductLineAll(int index,int size,ProductionLines productionLine);
	/**获取不属于该客户该产线下的设备列表 用于修改时使用
	 * @param productionLine
	 * @return
	 */
	public List<Equipments> selectProlineEqusNotExists(ProductionLines productionLine);
	
	/**获取产线下所有设备的状态(故障|停机)数量
	 * @param productionLine
	 * @return
	 */
	public int selectProlineStates(ProductionLines productionLine);
	
	public int saveOtherLanguage(ProductionLines productionLine);
	
	public int updateOtherLanguage(ProductionLines productionLine);
	
	/** 查询客户下的产线 返回产线code和name
	* selectProLineCodeNameByCustomerCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @return    参数
	* @return List<ProductionLines>    返回类型 
	*/
	public List<ProductionLines> selectProLineCodeNameByCustomerCode(String customerCodes,String language);
	
	
	/** 获取产线基本信息 产线code和name 客户code和name
	* selectProLineInfoByCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @return    参数
	* @return ProductionLines    返回类型 
	*/
	public List<ProductionLines> selectProLineInfoByCode(String customerCode, String language);
	
	/** 获取产线下的点位和值(安卓使用)
	* selectProLineInfoByCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @return    参数
	* @return ProductionLines    返回类型 
	*/
	public List<ProductionLines> selectProLinesByCustomerCode(ProductionLines productionLine);
	
	/** 获取产线下的设备
	* selectProLinesEquipmentByCustomerCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLines
	* @param @return    参数
	* @return List<ProductionLines>    返回类型 
	*/
	public List<ProductionLines> selectProLinesEquipmentByCustomerCode(ProductionLines productionLines);
	
	/** 获取产线基本信息 产线code和name 客户code和name
	* selectProLineInfoByCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionLine
	* @param @return    参数
	* @return ProductionLines    返回类型 
	*/
	public List<ProductionLines> selectProLineInfoByCode(ProductionLines productionLine);

}
	
