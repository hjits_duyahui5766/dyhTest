package com.cloudinnov.dao;

import com.cloudinnov.model.EquipmentInspectionWorkItemOption;

public interface EquipmentInspectionWorkItemOptionDao extends IBaseDao<EquipmentInspectionWorkItemOption>{
  
}