package com.cloudinnov.logic.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.BomCategoryLogic;
import com.cloudinnov.model.BomCategory;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年6月30日下午2:42:23
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Service
public class BomCategoryLogicImpl extends BaseLogicImpl<BomCategory> implements BomCategoryLogic {

	@Override
	public int save(BomCategory entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(BomCategory entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(BomCategory entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<BomCategory> selectList(BomCategory entity, boolean isOrderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<BomCategory> selectListPage(int index, int size, BomCategory entity, boolean isOrderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> selectListMap(Map<String, Object> condition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<BomCategory> selectListMapPage(int index, int size, Map<String, Object> condition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> selectMap(Map<String, Object> condition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BomCategory select(BomCategory entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int saveOtherLanguage(BomCategory bomCategory) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<BomCategory> addSelectList(BomCategory bomCategory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BomCategory> tableList(String[] codes, String language, String oemCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateOtherLanguage(BomCategory bomCategory) {
		// TODO Auto-generated method stub
		return 0;
	}

}
