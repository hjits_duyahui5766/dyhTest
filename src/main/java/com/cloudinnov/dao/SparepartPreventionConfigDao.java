package com.cloudinnov.dao;

import com.cloudinnov.model.SparepartPreventionConfig;

public interface SparepartPreventionConfigDao extends IBaseDao<SparepartPreventionConfig> {
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartPreventionConfig record);

    SparepartPreventionConfig selectByPrimaryKey(Integer id);

    int updateByCondition(SparepartPreventionConfig record);

    int updateByPrimaryKeyWithBLOBs(SparepartPreventionConfig record);

    int updateByPrimaryKey(SparepartPreventionConfig record);
    
    int lastInsertId();

	SparepartPreventionConfig selectBySparepartCode(SparepartPreventionConfig sparepartPreventionConfig);
}