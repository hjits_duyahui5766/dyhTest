package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.HistoryData;

public interface HistoryDataDao extends IBaseDao<HistoryData>{
    
    /** 
    * insertHistoryList 
    * @Description: 批量插入历史数据
    * @param @param map
    * @param @return    参数
    * @return int    返回类型 
    */
    int insertHistoryList(Map<String,Object> map);
    
    public int insert(Map<String,Object> map);
    
    public List<String> selectHistoryData(Map<String,Object> map);
}