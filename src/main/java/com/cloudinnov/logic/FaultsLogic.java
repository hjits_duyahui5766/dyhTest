package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.FaultStat;
import com.cloudinnov.model.Faults;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年2月18日下午2:43:39
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface FaultsLogic extends IBaseLogic<Faults> {
	public Map<String, Object> selectAlarmOrderInfoUseInsert(String channelCode, String faultCode);
	public Page<Faults> listByLibCode(int index, int size, String oemCode, String language, String level, String key,
			String libraryCode);
	/**
	 * 查看前1天的工单插入到故障统计表中
	 * @return
	 */
	public int insertFaultStat(long nowUnixTime);
	/**
	 * 查询工单统计 可以选择equCode,lineCode,customerCode作为条件 selectFaultStatsByCondition
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param map
	 * @param @return 参数
	 * @return List<FaultStat> 返回类型
	 */
	public List<FaultStat> selectFaultStatsByCondition(Map<String, Object> map);
	/**
	 * 工单详情列表 分页 selectFaultStatsDetailByCondition
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param map
	 * @param @return 参数
	 * @return Page<FaultStat> 返回类型
	 */
	public Page<FaultStat> selectFaultStatsDetailByCondition(Map<String, Object> map);
	/**
	 * 工单按天查询列表分页 selectFaultStatsDayByCondition
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param map
	 * @param @return 参数
	 * @return Page<FaultStat> 返回类型
	 */
	public Page<FaultStat> selectFaultStatsDayByCondition(Map<String, Object> map);
	public int saveOtherLanguage(Faults faults);
	public int updateOtherLanguage(Faults faults);
	/**
	 * 故障码导入 faultCodeImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @return 参数
	 * @return int 返回类型
	 */
	public int faultCodeImport(String data, Faults faults);
	public List<Faults> selectListByFaultCode(Faults faults);
}
