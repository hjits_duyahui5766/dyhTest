package com.cloudinnov.utils.control;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月11日下午12:30:15
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public class ControlModel {
	private String xid;
	private Object value;
	private Boolean result;
	private Integer delayTime;

	public String getXid() {
		return xid;
	}
	public void setXid(String xid) {
		this.xid = xid;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public Integer getDelayTime() {
		return delayTime;
	}
	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}
	@Override
	public String toString() {
		return "ControlModel [xid=" + xid + ", value=" + value + ", result=" + result + ", delayTime=" + delayTime
				+ "]";
	}
}
