package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SparepartPreventionConditionDao;
import com.cloudinnov.logic.SparepartPreventionConditionLogic;
import com.cloudinnov.model.SparepartPreventionCondition;

/**
 * @author liyijie
 * @date 2016年8月18日下午4:19:28
 * @email 37024760@qq.com
 * @remark
 * @version 
 */
@Service
public class SparepartPreventionConditionLogicImpl extends BaseLogicImpl<SparepartPreventionCondition> implements SparepartPreventionConditionLogic{
	
	@Autowired
	SparepartPreventionConditionDao sparepartPreventionConditionDao;
	@Override
	public List<SparepartPreventionCondition> selectListByConfigId(Integer configId) {
		return sparepartPreventionConditionDao.selectListByConfigId(configId);
		
	}

}
