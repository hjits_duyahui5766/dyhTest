package com.hjzk.serialport.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
 
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.utils.FireAlarmServer;
import com.cloudinnov.websocket.ControlSolutionDialogueWebsocket;
 
import com.hjzk.fire.AlarmInfo;
import com.hjzk.fire.FireHandler;
import com.hjzk.fire.FireOrder;
import com.hjzk.serialport.exception.NoSuchPort;
import com.hjzk.serialport.exception.NotASerialPort;
import com.hjzk.serialport.exception.PortInUse;
import com.hjzk.serialport.exception.SendDataToSerialPortFailure;
import com.hjzk.serialport.exception.SerialPortOutputStreamCloseFailure;
import com.hjzk.serialport.exception.SerialPortParameterFailure;

public class FireTaskWork {
	 private ApplicationContext context = null;
	 
	 @Autowired
	 private AmqpTemplate faultTemplate;
	 public  List<Integer> tmpAlarmList = new ArrayList<>();
	 
	 public void destroyData(){
		 
		 ScheduledExecutorService service = Executors  
	                .newSingleThreadScheduledExecutor();  
		 
		 service.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				 tmpAlarmList = new ArrayList<>();
			}
		}, 0, 10, TimeUnit.MINUTES);
		 System.err.println("-------------------------------------数据销毁----------------------------------------------");
	 }
	 
	public void startFireWork(){
		
		 new Thread(new Runnable() {
			 
			private AlarmInfo rightHandsAlarm;
			private AlarmInfo leftHandsAlarm;
			private AlarmInfo rightTemptureAlarm;
			private AlarmInfo leftTemptureAlarm;
			private AlarmInfo mountSmokeAlarm;
			private String alarmInfo;

			final FireHandler fireHandler = new FireHandler();
			@Override
			public void run() {
				try {
					rightHandsAlarm = fireHandler.getRightHandsAlarm();
					leftHandsAlarm = fireHandler.getLeftHandsAlarm();
					rightTemptureAlarm = fireHandler.getRightTemptureAlarm();
					leftTemptureAlarm = fireHandler.getLeftTemptureAlarm();
					mountSmokeAlarm = fireHandler.getMountSmokeAlarm();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if(rightHandsAlarm.isAlarm()||leftHandsAlarm.isAlarm()){
					 
					if(rightHandsAlarm.oneCodeList.size()>0){
						alarmInfo = "";
						for (int i = 0; i < rightHandsAlarm.oneCodeList.size(); i++) {
							
							alarmInfo = alarmInfo+rightHandsAlarm.oneCodeList.get(i)+"号手动报警 ";
						}
						for (int j = 0; j < rightHandsAlarm.oneCodeList.size(); j++) {
							 if(!tmpAlarmList.contains(rightHandsAlarm.getOneCodeList().get(j))){
							   tmpAlarmList.add(rightHandsAlarm.getOneCodeList().get(j));

								String msg="右洞"+rightHandsAlarm.getOneCodeList().get(j)+"号手动报警";
								FireCRTEvent fireEvent = new FireCRTEvent();
								fireEvent.setCategoryCode("SDBJAN");
								fireEvent.setDevice("火灾报警器");
								fireEvent.setDeviceId("rightHands");
								fireEvent.setEvent("手动报警按钮 ");
								fireEvent.setOccurrenceTime(System.currentTimeMillis());
								fireEvent.setPosition(msg);
								fireEvent.setSolutionCode("RDASDF"); 
								Object json = JSON.toJSON(fireEvent);
								FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(json));
								 
								 try {
									FireOrder.startAlarm();
								} catch (SendDataToSerialPortFailure | SerialPortOutputStreamCloseFailure
										| SerialPortParameterFailure | NotASerialPort | NoSuchPort | PortInUse
										| InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								 
							 }
						}
					}
					if(leftHandsAlarm.oneCodeList.size()>0){
						alarmInfo = "";
						for (int i = 0; i < leftHandsAlarm.oneCodeList.size(); i++) {
							 
							alarmInfo = alarmInfo+leftHandsAlarm.oneCodeList.get(i)+"号手动报警 ";
						}
						for (int j = 0; j < leftHandsAlarm.oneCodeList.size(); j++) {
							 if(!tmpAlarmList.contains(leftHandsAlarm.getOneCodeList().get(j))){
								 tmpAlarmList.add(leftHandsAlarm.getOneCodeList().get(j));
								 System.err.println("左洞"+leftHandsAlarm.getOneCodeList().get(j)+"号手动报警");
								 String msg="左洞"+leftHandsAlarm.getOneCodeList().get(j)+"号手动报警";
								FireCRTEvent fireEvent=new FireCRTEvent();
								fireEvent.setCategoryCode("SDBJAN");
								fireEvent.setDevice("火灾报警器");
								fireEvent.setDeviceId("leftHands");
								fireEvent.setEvent("手动报警按钮 ");
								fireEvent.setOccurrenceTime(System.currentTimeMillis());
								fireEvent.setPosition(msg);
								fireEvent.setSolutionCode("RDASDF"); 
								Object json = JSON.toJSON(fireEvent);
								FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(json));
									 
								 try {
										FireOrder.startAlarm();
									} catch (SendDataToSerialPortFailure | SerialPortOutputStreamCloseFailure
											| SerialPortParameterFailure | NotASerialPort | NoSuchPort | PortInUse
											| InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								  
							 }
						}
					}
					
				}else if(rightTemptureAlarm.isAlarm()||leftTemptureAlarm.isAlarm()){
					 if(rightTemptureAlarm.oneCodeList.size()>0){
						 for (int i = 0; i < rightTemptureAlarm.oneCodeList.size(); i++) {
							String msg="右洞"+rightTemptureAlarm.getOneCodeList().get(i)+"号感温光纤报警";
							FireCRTEvent fireEvent=new FireCRTEvent();
							fireEvent.setCategoryCode("SDBJAN");
							fireEvent.setDevice("火灾报警器");
							fireEvent.setDeviceId("rightTem");
							fireEvent.setEvent("感温光纤告警 ");
							fireEvent.setOccurrenceTime(System.currentTimeMillis());
							fireEvent.setPosition(msg);
							fireEvent.setSolutionCode("RDASDF"); 
							Object json = JSON.toJSON(fireEvent);
							FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(json));
								 
							 try {
									FireOrder.startAlarm();
								} catch (SendDataToSerialPortFailure | SerialPortOutputStreamCloseFailure
										| SerialPortParameterFailure | NotASerialPort | NoSuchPort | PortInUse
										| InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
							}
						}
						 
					 }else if(leftTemptureAlarm.oneCodeList.size()>0){
						 for (int i = 0; i <leftTemptureAlarm.oneCodeList.size(); i++) {
							String msg="左洞"+rightTemptureAlarm.getOneCodeList().get(i)+"号感温光纤报警";
							FireCRTEvent fireEvent=new FireCRTEvent();
							fireEvent.setCategoryCode("SDBJAN");
							fireEvent.setDevice("火灾报警器");
							fireEvent.setDeviceId("leftTem");
							fireEvent.setEvent("感温光纤告警 ");
							fireEvent.setOccurrenceTime(System.currentTimeMillis());
							fireEvent.setPosition(msg);
							fireEvent.setSolutionCode("RDASDF"); 
							Object json = JSON.toJSON(fireEvent);
							FireAlarmServer.fireTemplate.convertAndSend(JSON.toJSONString(json));
						}
					 } 
				}else if(mountSmokeAlarm.isAlarm()){
					if(mountSmokeAlarm.oneCodeList.size()>0){
						
					}
				}
			}
		}).start();
		
	}
	
	 public void SendMsg2Socket(String msg){
		 Iterator<Map.Entry<String, ControlSolutionDialogueWebsocket>> controlSolutionDialogueWebsocket = ControlSolutionDialogueWebsocket.webSocketMap
					.entrySet().iterator();
			while (controlSolutionDialogueWebsocket.hasNext()) {
				Map.Entry<String, ControlSolutionDialogueWebsocket> controlSolutionDialogueEntry = controlSolutionDialogueWebsocket
						.next();
				try {
					controlSolutionDialogueEntry.getValue().sendMessage(msg);
				} catch (IOException e) {
				}
			}
	 }
}
