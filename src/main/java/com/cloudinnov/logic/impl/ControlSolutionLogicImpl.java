package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cloudinnov.dao.BoardSolutionConfigDao;
import com.cloudinnov.dao.BoardTemplateDao;
import com.cloudinnov.dao.ControlSolutionConfigDao;
import com.cloudinnov.dao.ControlSolutionDao;
import com.cloudinnov.dao.ControlSolutionSendRecordItemDao;
import com.cloudinnov.dao.EquipmentPointsDao;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.dao.SectionDao;
import com.cloudinnov.dao.TriggerDao;
import com.cloudinnov.dao.mongo.EquipmentControlLogMongoDBDao;
import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.EquipmentControlLogic;
import com.cloudinnov.model.BoardSolutionConfig;
import com.cloudinnov.model.BoardTemplate;
import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.ControlSolutionConfig;
import com.cloudinnov.model.ControlSolutionExpre;
import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.FireCRTEvent;
import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.model.Section;
import com.cloudinnov.model.Trigger;
import com.cloudinnov.task.SchduleScanJob;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.RemoteControllerUtil;
import com.cloudinnov.utils.control.ControlModel;
import com.cloudinnov.utils.control.EquipmentControlLog;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service("controlSolutionLogic")
public class ControlSolutionLogicImpl extends BaseLogicImpl<ControlSolution> implements ControlSolutionLogic {
	private static final Logger logger = LoggerFactory.getLogger(ControlSolutionLogicImpl.class);
	private static final int SOLUTION_TYPE_BOARD = 1;
	private static final int SOLUTION_TYPE_OTHER = 2;
	/**
	 * 自动处理
	 * 
	 * @fieldName: AUTO_MODE
	 * @fieldType: int
	 * @Description: TODO
	 */
	private static final int AUTO_MODE = 1;
	/**
	 * 手动处理
	 * 
	 * @fieldName: MANUAL_MODE
	 * @fieldType: int
	 * @Description: TODO
	 */
	private static final int MANUAL_MODE = 2;
	private int digit = 5;
	@Autowired
	private ControlSolutionDao controlSolutionDao;
	@Autowired
	private ControlSolutionConfigDao controlSolutionConfigDao;
	@Autowired
	private TriggerDao tiggerDao;
	@Autowired
	private BoardSolutionConfigDao boardSolutionConfigDao;
	@Autowired
	private EquipmentsAttrDao equipmentsAttrDao;
	@Autowired
	private ControlSolutionSendRecordItemDao controlSolutionItemLogDao;
	@Autowired
	private EquipmentsDao equipmentsDao;
	@Autowired
	private BoardTemplateDao boardTemplateDao;
	@Autowired
	private EquipmentControlLogic equipmentControlLogic;
	@Autowired
	private EquipmentControlLogMongoDBDao equipmentControlLogMongoDBDao;
	@Autowired
	private EquipmentPointsDao equipmentPointsDao;
	@Autowired
	private SectionDao sectionDao;
	
	@Override
	public int save(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		String controlSolutionCode = CodeUtil.controlSolutionCode(digit);// 创建群控方案code
		controlSolution.setCode(controlSolutionCode);
		controlSolution.setStatus(CommonUtils.STATUS_NORMAL);
		List<ControlSolutionConfig> list = JSON.parseArray(controlSolution.getConSolConfig(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
		int result = controlSolutionDao.insert(controlSolution);
		if (result == 1) {
			for (ControlSolutionConfig conSolConfig : list) {
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setSolutionCode(controlSolutionCode);
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				if (conSolConfig.getCategoryCode() != null) {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getCategoryCode());
				} else {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				}
				controlSolutionConfig.setSendContent(conSolConfig.getSendContent());
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public Page<ControlSolution> selectListPage(int index, int size, ControlSolution controlSolution,
			boolean isOrderBy) {
		PageHelper.startPage(index, size);
		if (isOrderBy) {
			PageHelper.orderBy("cs.create_time desc");
		}
		List<ControlSolution> listByCondition = controlSolutionDao.selectListByCondition(controlSolution);

		// return (Page<ControlSolution>)
		// controlSolutionDao.selectListByCondition(controlSolution);
		return (Page<ControlSolution>) listByCondition;
	}

	@Override
	public int delete(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
		
		//新加的内容
		ControlSolutionExpre controlSolutionExpre = new ControlSolutionExpre();
		controlSolutionExpre.setSolutionCode(controlSolution.getCode());
		
		controlSolutionConfig.setSolutionCode(controlSolution.getCode());
		
		int result = controlSolutionDao.deleteByCondition(controlSolution);
		if (result >= 1) {
			int resultConfig = controlSolutionConfigDao.deleteByCondition(controlSolutionConfig);
			if (resultConfig >= 1) {
				controlSolutionDao.deleteExpreByCondition(controlSolutionExpre);
			}
		} else {
			result = 0;
		}
		return result;
	}

	@Override
	public int update(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
		int result = controlSolutionDao.updateByCondition(controlSolution);
		List<ControlSolutionConfig> list = JSON.parseArray(controlSolution.getConSolConfig(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		if (result == 1) {
			controlSolutionConfig.setSolutionCode(controlSolution.getCode());
			// 先删除设备控制配置表
			controlSolutionConfigDao.deleteByCondition(controlSolutionConfig);
			for (ControlSolutionConfig conSolConfig : list) {
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setSolutionCode(controlSolution.getCode());
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				if (conSolConfig.getCategoryCode() != null) {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getCategoryCode());
				} else {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				}
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public ControlSolution select(ControlSolution controlSolution) {
		ControlSolution solution = controlSolutionDao.selectEntityByCondition(controlSolution);
		if (solution != null) {
			ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
			controlSolutionConfig.setSolutionCode(solution.getCode());
			List<ControlSolutionConfig> selectListBySolution = controlSolutionConfigDao
					.selectListByCondition(controlSolutionConfig);
			List<ControlSolutionConfig.Config> configs;
			for (ControlSolutionConfig model : selectListBySolution) {
				configs = new ArrayList<>();
				String config = model.getConfig();
				if (CommonUtils.isNotEmpty(config)) {
					configs.addAll(JSON.parseArray(config, ControlSolutionConfig.Config.class));
				}
				HashMap<String, Object> map = new HashMap<String, Object>();
				String equipmentCode = model.getCode();// 获取设备编码
				List<EquipmentsAttr> attrList = equipmentsAttrDao.selectListByEquipmentCode(equipmentCode);// 根据设备编码查询设备属性
				for (EquipmentsAttr equipmentsAttr : attrList) {
					if (equipmentsAttr.getCustomTag() != null && equipmentsAttr.getCustomTag() != "") {
						map.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
					}
				}
				model.setConfigs(map);
				model.setTextConfig(configs);
			}
			solution.setControlSolutionConfig(selectListBySolution);
		}
		return solution;
	}

	@Override
	public Map<String, Object> selectControlSolutionMap(ControlSolution controlSulotion) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询情报版方案
		ControlSolution solution = controlSolutionDao.selectEntityByCondition(controlSulotion);
		// 查询设备控制配置表
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
		controlSolutionConfig.setSolutionCode(controlSulotion.getCode());
		List<ControlSolutionConfig> controlSolutionConfigInfo = controlSolutionConfigDao
				.selectListByCondition(controlSolutionConfig);
		for (ControlSolutionConfig item : controlSolutionConfigInfo) {
			Map<String, Object> mapConfigs = new HashMap<String, Object>();
			List<EquipmentsAttr> attrList = equipmentsAttrDao.selectListByEquipmentCode(item.getCode());// 根据设备编码查询设备属性
			for (EquipmentsAttr attrItem : attrList) {
				if (attrItem.getCustomTag() != null && attrItem.getCustomTag() != "") {
					mapConfigs.put(attrItem.getCustomTag(), attrItem.getValue());
				}
			}
			item.setConfigs(mapConfigs);
		}
		solution.setControlSolutionConfig(controlSolutionConfigInfo);
		// 根据触发器code查询触发器详情
		if (solution.getTiggerCode() != null && solution.getTiggerCode() != "") {
			Trigger trigger = new Trigger();
			trigger.setCode(solution.getTiggerCode());
			Trigger triggerInfo = tiggerDao.selectEntityByCondition(trigger);
			map.put("triggerInfo", triggerInfo);
		}
		map.put("solution", solution);
		return map;
	}

	@Override
	public Page<ControlSolution> selectInformationBoard(int index, int size, ControlSolution model) {
		PageHelper.startPage(index, size);
		// 查询情报板的信息
		Page<ControlSolution> informationBoard = controlSolutionDao.selectInformationBoard(model);
		return informationBoard;
	}

	@Override
	public List<ControlSolution> selectControlSolutionByEquiCode(ControlSolution controlSulotion) {
		return controlSolutionDao.selectControlSolutionByEquiCode(controlSulotion);
	}

	@Override
	public int updateControOnOff(String code, Integer onOff) {
		CommonUtils.ControlSolutionCacheMap = null;
		return controlSolutionDao.updateControOnOff(code, onOff);
	}

	@Override
	public List<ControlSolutionSendRecordItem> selectHistoryListByEquCateCode(String equCateCode) {
		List<ControlSolutionSendRecordItem> list = new ArrayList<>();
		BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
		boardSolutionConfig.setEquipmentClassify(equCateCode);
		List<BoardSolutionConfig> boardList = boardSolutionConfigDao.selectListByCondition(boardSolutionConfig);
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
		controlSolutionConfig.setEquipmentClassify(equCateCode);
		List<ControlSolutionConfig> controlList = controlSolutionConfigDao.selectListByEquCate(controlSolutionConfig);
		if (JudgeNullUtil.iList(boardList)) {
			ControlSolutionSendRecordItem controlSolutionSendRecordItem = null;
			for (BoardSolutionConfig boardModel : boardList) {
				controlSolutionSendRecordItem = new ControlSolutionSendRecordItem();
				controlSolutionSendRecordItem.setCode(boardModel.getSolutionCode());
				List<ControlSolutionSendRecordItem> recordList = controlSolutionItemLogDao
						.selectListByCondition(controlSolutionSendRecordItem);
				for (ControlSolutionSendRecordItem record : recordList) {
					record.setSolutionName(boardModel.getName());
					record.setSolutionCode(boardModel.getSolutionCode());
					record.setSolutionType(SOLUTION_TYPE_BOARD);
				}
				list.addAll(recordList);
			}
		}
		if (JudgeNullUtil.iList(controlList)) {
			ControlSolutionSendRecordItem controlSolutionSendRecordItem = null;
			for (ControlSolutionConfig contrlModel : controlList) {
				controlSolutionSendRecordItem = new ControlSolutionSendRecordItem();
				controlSolutionSendRecordItem.setCode(contrlModel.getSolutionCode());
				List<ControlSolutionSendRecordItem> recordList = controlSolutionItemLogDao
						.selectListByCondition(controlSolutionSendRecordItem);
				for (ControlSolutionSendRecordItem record : recordList) {
					record.setSolutionName(contrlModel.getName());
					record.setSolutionCode(contrlModel.getSolutionCode());
					record.setSolutionType(SOLUTION_TYPE_OTHER);
				}
				list.addAll(recordList);
			}
		}
		return list;
	}

	@Override
	public int saveBoardSolutionConfig(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		int returnCode = 0;
		String controlSolutionCode = CodeUtil.controlSolutionCode(digit);// 创建群控方案code
		controlSolution.setCode(controlSolutionCode);
		controlSolution.setStatus(CommonUtils.STATUS_NORMAL);
		controlSolution.setSolutionType(SOLUTION_TYPE_BOARD);
		if (CommonUtils.isNotEmpty(controlSolution.getEquipmentClassify())
				&& CommonUtils.isNotEmpty(controlSolution.getList())) {
			List<String> equCodes = JSON.parseArray(controlSolution.getEquipmentCode(), String.class);// 获取所有需要配置的设备
			List<BoardSolutionConfig> list = JSON.parseArray(controlSolution.getList(), BoardSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
			returnCode = controlSolutionDao.insert(controlSolution);
			if (returnCode == CommonUtils.SUCCESS_NUM) {
				BoardSolutionConfig boardSolutionConfig = null;
				// if (controlSolution.getType() == AUTO_MODE) {// 自动需要向配置表插入设备配置数据
				for (String equCode : equCodes) {
					for (BoardSolutionConfig conSolConfig : list) {
						boardSolutionConfig = new BoardSolutionConfig();// 创建情报板配置对象
						// 将json对象放入情报板控制配置对象中
						boardSolutionConfig.setSolutionCode(controlSolution.getCode());
						boardSolutionConfig.setEquipmentCode(equCode);
						boardSolutionConfig.setEquipmentClassify(controlSolution.getEquipmentClassify());
						List<BoardSolutionConfig.Data> rowDatas = JSON.parseArray(conSolConfig.getData(),
								BoardSolutionConfig.Data.class);
						boardSolutionConfig.setRowCount(rowDatas.size());
						boardSolutionConfig.setSpeed(conSolConfig.getSpeed());
						boardSolutionConfig.setStayTime(conSolConfig.getStayTime());
						boardSolutionConfig.setColor(conSolConfig.getColor());
						boardSolutionConfig.setDisplayType(conSolConfig.getDisplayType());
						boardSolutionConfig.setBoardTemplateCode(conSolConfig.getCode());
						boardSolutionConfig.setSendContent(conSolConfig.getData());
						boardSolutionConfig.setComment(controlSolution.getComment());
						returnCode = boardSolutionConfigDao.insert(boardSolutionConfig);
					}
				}

				/*
				 * } else if (controlSolution.getType() == MANUAL_MODE) { for
				 * (BoardSolutionConfig conSolConfig : list) { boardSolutionConfig = new
				 * BoardSolutionConfig();// 创建情报板配置对象 // 将json对象放入情报板控制配置对象中
				 * boardSolutionConfig.setSolutionCode(controlSolution.getCode());
				 * boardSolutionConfig.setEquipmentClassify(controlSolution.getEquipmentClassify
				 * ()); List<BoardSolutionConfig.Data> rowDatas =
				 * JSON.parseArray(conSolConfig.getData(), BoardSolutionConfig.Data.class);
				 * boardSolutionConfig.setRowCount(rowDatas.size());
				 * boardSolutionConfig.setSpeed(conSolConfig.getSpeed());
				 * boardSolutionConfig.setStayTime(conSolConfig.getStayTime());
				 * boardSolutionConfig.setColor(conSolConfig.getColor());
				 * boardSolutionConfig.setDisplayType(conSolConfig.getDisplayType());
				 * boardSolutionConfig.setBoardTemplateCode(conSolConfig.getCode());
				 * boardSolutionConfig.setSendContent(conSolConfig.getData());
				 * boardSolutionConfig.setComment(controlSolution.getComment()); returnCode =
				 * boardSolutionConfigDao.insert(boardSolutionConfig); } } else { returnCode =
				 * CommonUtils.ERROR_NUM; }
				 */
			}
		} else {
			returnCode = CommonUtils.ERROR_NUM;
		}
		return returnCode;
	}

	@Override
	public int saveControlSolutionConfig(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		String controlSolutionCode = CodeUtil.controlSolutionCode(digit);// 创建群控方案code
		controlSolution.setCode(controlSolutionCode);
		controlSolution.setStatus(CommonUtils.STATUS_NORMAL);
		controlSolution.setSolutionType(SOLUTION_TYPE_OTHER);
		List<ControlSolutionConfig> list = JSON.parseArray(controlSolution.getConSolConfig(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		ControlSolutionConfig controlSolutionConfig;// 创建设备控制配置对象
		int result = controlSolutionDao.insert(controlSolution);
		if (result == CommonUtils.SUCCESS_NUM) {
			for (ControlSolutionConfig conSolConfig : list) {
				controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setSolutionCode(controlSolutionCode);
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			return CommonUtils.SUCCESS_NUM;
		} else {
			return CommonUtils.ERROR_NUM;
		}
	}

	@Override
	public int saveControlSolutionExpression(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		// 触控和恢复表达式
		String expressArray = controlSolution.getExpressArray();
		String expressRecoverArray = controlSolution.getExpressRecoverArray();

		String controlSolutionCode = CodeUtil.controlSolutionCode(digit);// 创建群控方案code
		controlSolution.setCode(controlSolutionCode);
		controlSolution.setStatus(CommonUtils.STATUS_NORMAL);
		controlSolution.setSolutionType(SOLUTION_TYPE_OTHER);
		controlSolution.setIsSchedule(1);

		List<ControlSolutionConfig> listTriger = JSON.parseArray(controlSolution.getConSolConfig(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		List<ControlSolutionConfig> listRecover = JSON.parseArray(controlSolution.getConSolConfigRecover(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		ControlSolutionConfig controlSolutionConfig;// 创建设备控制配置对象
		ControlSolutionExpre controlSolutionExpre;// 创建表达式对象

		int result = controlSolutionDao.insertSchedule(controlSolution);
		if (result == CommonUtils.SUCCESS_NUM) {
			for (ControlSolutionConfig conSolConfig : listTriger) {
				controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setIsRecover(0);// 标记是触发
				controlSolutionConfig.setSolutionCode(controlSolutionCode);
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			for (ControlSolutionConfig conSolConfig : listRecover) {
				controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setIsRecover(1);// 标记是恢复
				controlSolutionConfig.setSolutionCode(controlSolutionCode);
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			if (CommonUtils.isNotEmpty(expressArray)) {
				JSONArray createArray = JSONArray.parseArray(expressArray);
				if (JudgeNullUtil.iList(createArray)) {
					controlSolutionExpre = new ControlSolutionExpre();
					StringBuffer pointCodes = new StringBuffer();
					StringBuffer conditions = new StringBuffer();
					StringBuffer names = new StringBuffer();

					String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);// 创建群控方案表达式code
					controlSolutionExpre.setSolutionCode(controlSolutionCode);
					controlSolutionExpre.setCode(expressionCode);
					controlSolutionExpre.setIsRecover(0);// 0是触发方案

					for (int i = 0; i < createArray.size(); i++) {
						String name = createArray.getJSONObject(i).getString("name");
						String pointCode = createArray.getJSONObject(i).getString("point_code");
						String condition = createArray.getJSONObject(i).getString("condition");
						names.append(name + ",");
						pointCodes.append(pointCode + ",");
						conditions.append(condition);
					}
					String pointCodeStr = pointCodes.substring(0, pointCodes.lastIndexOf(","));
					String namesStr = names.substring(0, names.lastIndexOf(","));
					controlSolutionExpre.setPointCode(pointCodeStr);
					controlSolutionExpre.setCondition(conditions.toString());
					controlSolutionExpre.setName(namesStr);

					controlSolutionExpre.setStatus(1);
					controlSolutionDao.insertExpre(controlSolutionExpre);
				}

				// 群控需求更改,以前代码注释
				// JSONArray createArray = JSONArray.parseArray(expressArray);
				// if (JudgeNullUtil.iList(createArray)) {
				// for (int i = 0; i < createArray.size(); i++) { // controlSolutionExpre = new
				// ControlSolutionExpre();
				// String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);//
				// 创建群控方案表达式code
				// controlSolutionExpre.setSolutionCode(controlSolutionCode);
				// controlSolutionExpre.setCode(expressionCode);
				// controlSolutionExpre.setIsRecover(0);//0是触发方案
				// String name = createArray.getJSONObject(i).getString("name");
				// String pointCode = createArray.getJSONObject(i).getString("point_code");
				// String condition = createArray.getJSONObject(i).getString("condition");
				// controlSolutionExpre.setName(name);
				// controlSolutionExpre.setPointCode(pointCode);
				// controlSolutionExpre.setCondition(condition);
				// controlSolutionExpre.setStatus(1);
				// controlSolutionDao.insertExpre(controlSolutionExpre);
				// }
				// }
			}

			if (CommonUtils.isNotEmpty(expressRecoverArray)) {
				JSONArray createArray = JSONArray.parseArray(expressRecoverArray);
				if (JudgeNullUtil.iList(createArray)) {
					controlSolutionExpre = new ControlSolutionExpre();
					StringBuffer pointCodes = new StringBuffer();
					StringBuffer conditions = new StringBuffer();
					StringBuffer names = new StringBuffer();

					String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);// 创建群控方案表达式code
					controlSolutionExpre.setSolutionCode(controlSolutionCode);
					controlSolutionExpre.setCode(expressionCode);
					controlSolutionExpre.setIsRecover(1);// 1是恢复方案

					for (int i = 0; i < createArray.size(); i++) {
						String pointCode = createArray.getJSONObject(i).getString("point_code");
						String name = createArray.getJSONObject(i).getString("name");
						String condition = createArray.getJSONObject(i).getString("condition");
						pointCodes.append(pointCode + ",");
						names.append(name + ",");
						conditions.append(condition);
					}
					String pointCodeStr = pointCodes.substring(0, pointCodes.lastIndexOf(","));
					String namesStr = names.substring(0, names.lastIndexOf(","));
					controlSolutionExpre.setPointCode(pointCodeStr);
					controlSolutionExpre.setCondition(conditions.toString());
					controlSolutionExpre.setName(namesStr);

					controlSolutionExpre.setStatus(1);
					controlSolutionDao.insertExpre(controlSolutionExpre);
				}

				// JSONArray createArray=JSONArray.parseArray(expressRecoverArray);
				// if (JudgeNullUtil.iList(createArray)) {
				// for (int i = 0; i < createArray.size(); i++) {
				// controlSolutionExpre = new ControlSolutionExpre();
				// String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);//
				// 创建群控方案表达式code
				// controlSolutionExpre.setSolutionCode(controlSolutionCode);
				// controlSolutionExpre.setCode(expressionCode);
				// controlSolutionExpre.setIsRecover(1);//1是恢复方案
				// String name = createArray.getJSONObject(i).getString("name");
				// String pointCode = createArray.getJSONObject(i).getString("point_code");
				// String condition = createArray.getJSONObject(i).getString("condition");
				// controlSolutionExpre.setName(name);
				// controlSolutionExpre.setPointCode(pointCode);
				// controlSolutionExpre.setCondition(condition);
				// controlSolutionExpre.setStatus(1);
				// controlSolutionDao.insertExpre(controlSolutionExpre);
				// }
				// }

			}
			return CommonUtils.SUCCESS_NUM;
		} else {
			return CommonUtils.ERROR_NUM;
		}
	}

	@Override
	public int updateSolutionConfigExpression(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		String expressArray = controlSolution.getExpressArray();
		String expressRecoverArray = controlSolution.getExpressRecoverArray();
		ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
		controlSolution.setIsSchedule(1);
		int result = controlSolutionDao.updateByCondition(controlSolution);
		List<ControlSolutionConfig> listTriger = JSON.parseArray(controlSolution.getConSolConfig(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		List<ControlSolutionConfig> listRecover = JSON.parseArray(controlSolution.getConSolConfigRecover(),
				ControlSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
		ControlSolutionExpre controlSolutionExpre = null;// 创建表达式对象
		ControlSolutionExpre controlSolutionExpreCondition = null;// 创建查询expression的条件
		if (result == 1) {
			controlSolutionConfig.setSolutionCode(controlSolution.getCode());
			// 先删除该控制方案下面所有的设备控制配置表
			controlSolutionConfigDao.deleteByCondition(controlSolutionConfig);
			for (ControlSolutionConfig conSolConfig : listTriger) {
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig = new ControlSolutionConfig();// 创建设备控制配置对象
				controlSolutionConfig.setIsRecover(0);// 标记是触发
				controlSolutionConfig.setSolutionCode(controlSolution.getCode());
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				if (conSolConfig.getCategoryCode() != null) {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getCategoryCode());
				} else {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				}
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			for (ControlSolutionConfig conSolConfig : listRecover) {
				controlSolutionConfig = new ControlSolutionConfig();
				// 将json中的数据的放入设备控制配配置对象中
				controlSolutionConfig.setIsRecover(1);// 标记是恢复
				controlSolutionConfig.setSolutionCode(controlSolution.getCode());
				controlSolutionConfig.setEquipmentCode(conSolConfig.getCode());
				controlSolutionConfig.setName(conSolConfig.getName());
				if (conSolConfig.getCategoryCode() != null) {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getCategoryCode());
				} else {
					controlSolutionConfig.setEquipmentClassify(conSolConfig.getEquipmentClassify());
				}
				controlSolutionConfig.setSendContent(conSolConfig.getConfig());
				controlSolutionConfig.setConfig(conSolConfig.getConfig());
				controlSolutionConfig.setComment(controlSolution.getComment());
				controlSolutionConfigDao.insert(controlSolutionConfig);
			}
			// 再设置该控制方案下面所有的表达式状态为9 无效
			int result1 = controlSolutionDao.deleteExpressionBySolutionCode(controlSolution.getCode());
			//先查询,判断是否有记录,有更新记录,没有插入记录
//			controlSolutionExpreCondition = new ControlSolutionExpre();
//			controlSolutionExpreCondition.setSolutionCode(controlSolution.getCode());
//			controlSolutionExpreCondition.setIsRecover(0);
//			ControlSolutionExpre solutionExpressionResult = controlSolutionDao.selectSolutionExpressionByRecover(controlSolutionExpreCondition); //查询记录
			if (CommonUtils.isNotEmpty(expressArray)) {
				JSONArray createArray = JSONArray.parseArray(expressArray);
				if (JudgeNullUtil.iList(createArray)) {
					controlSolutionExpre = new ControlSolutionExpre();
					StringBuffer pointCodes = new StringBuffer();
					StringBuffer conditions = new StringBuffer();
					StringBuffer names = new StringBuffer();

					controlSolutionExpre.setSolutionCode(controlSolution.getCode());
					String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);// 创建群控方案表达式code
//					String expressionCode = createArray.getJSONObject(0).getString("code");
					controlSolutionExpre.setCode(expressionCode);
					for (int i = 0; i < createArray.size(); i++) {
						String pointCode = createArray.getJSONObject(i).getString("point_code");
						String name = createArray.getJSONObject(i).getString("name");
						String condition = createArray.getJSONObject(i).getString("condition");

						pointCodes.append(pointCode + ",");
						names.append(name + ",");
						conditions.append(condition);
					}
					String pointCodeStr = pointCodes.substring(0, pointCodes.lastIndexOf(","));
					String namesStr = names.substring(0, names.lastIndexOf(","));
					controlSolutionExpre.setPointCode(pointCodeStr);
					controlSolutionExpre.setCondition(conditions.toString());
					controlSolutionExpre.setName(namesStr);

					controlSolutionExpre.setStatus(1);
					controlSolutionExpre.setIsRecover(0);// 0代表触发表达式
//					if (solutionExpressionResult != null) {
//						//有记录,更新
//						controlSolutionDao.updateExpre(controlSolutionExpre);
//					}else {
						//没有记录.插入
						controlSolutionDao.insertExpre(controlSolutionExpre);
//					}
				}
//				else { //触发更新的方案为空,判断数据库原先是否有记录:有,失效;没有,无操作
//					if (solutionExpressionResult != null) {
//						controlSolutionDao.deleteExpreByCondition(controlSolutionExpreCondition);
//					}
//				}
			}
//			solutionExpressionResult = null;
//			controlSolutionExpreCondition.setIsRecover(1);
//			solutionExpressionResult = controlSolutionDao.selectSolutionExpressionByRecover(controlSolutionExpreCondition); //查询记录
			if (CommonUtils.isNotEmpty(expressRecoverArray)) {
				JSONArray createArray = JSONArray.parseArray(expressRecoverArray);
				if (JudgeNullUtil.iList(createArray)) {
					controlSolutionExpre = new ControlSolutionExpre();
					StringBuffer pointCodes = new StringBuffer();
					StringBuffer conditions = new StringBuffer();
					StringBuffer names = new StringBuffer();

					controlSolutionExpre.setSolutionCode(controlSolution.getCode());
					String expressionCode = CodeUtil.controlSolutionExpressionCode(digit);// 创建群控方案表达式code
//					String expressionCode = createArray.getJSONObject(0).getString("code");
					controlSolutionExpre.setCode(expressionCode);
					for (int i = 0; i < createArray.size(); i++) {
						String pointCode = createArray.getJSONObject(i).getString("point_code");
						String name = createArray.getJSONObject(i).getString("name");
						String condition = createArray.getJSONObject(i).getString("condition");

						pointCodes.append(pointCode + ",");
						names.append(name + ",");
						conditions.append(condition);
					}
					String pointCodeStr = pointCodes.substring(0, pointCodes.lastIndexOf(","));
					String namesStr = names.substring(0, names.lastIndexOf(","));
					controlSolutionExpre.setPointCode(pointCodeStr);
					controlSolutionExpre.setCondition(conditions.toString());
					controlSolutionExpre.setName(namesStr);

					controlSolutionExpre.setStatus(1);
					controlSolutionExpre.setIsRecover(1);// 1代表恢复表达式
//					if (solutionExpressionResult != null) {
//						//有记录,更新
//						controlSolutionDao.updateExpre(controlSolutionExpre);
//					}else {
//						//没有记录.插入
						controlSolutionDao.insertExpre(controlSolutionExpre);
//					}
				}
//				else { //恢复更新的方案为空,判断数据库原先是否有记录:有,失效;没有,无操作
//					if (solutionExpressionResult != null) {
//						int condition = controlSolutionDao.deleteExpreByCondition(controlSolutionExpreCondition);
//						System.err.println("返回结果:" + condition);
//					}
//				}
			}
			return 1;
		} else {
			return 0;
		}
	}

//	@Override
//	public int updateControlSolutionExpression(ControlSolutionExpression controlSolutionExpression) {
//		CommonUtils.ControlSolutionCacheMap = null;
//		if (controlSolutionExpression != null) {
//			int status = 1;
//			List<ControlSolutionExpre> solutionExpres = controlSolutionExpression.getPoints();
//			for (ControlSolutionExpre solutionExpre : solutionExpres) {
//				status = controlSolutionDao.updateExpre(solutionExpre);
//				if (status == 0) {
//					return CommonUtils.ERROR_NUM;
//				}
//			}
//			return CommonUtils.SUCCESS_NUM;
//		} else {
//			return CommonUtils.ERROR_NUM;
//		}
//	}

	@Override
	public Map<String, Object> selectBoardSolution(ControlSolution controlSolution) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询情报版方案
		controlSolution.setSolutionType(SOLUTION_TYPE_BOARD);
		ControlSolution solution = controlSolutionDao.selectEntityByCondition(controlSolution);
		// if (solution!=null && solution.getType() == AUTO_MODE) {
		// 查询设备控制配置表
		BoardSolutionConfig model = new BoardSolutionConfig();
		model.setSolutionCode(solution.getCode());
		List<BoardSolutionConfig> boardSolutionConfigs = boardSolutionConfigDao.selectListBySolutionCode(model);
		Equipments equModel = null;
		Set<String> codes = new HashSet<String>();
		List<BoardSolutionConfig> newBoardSolutionConfigs = new ArrayList<>();
		List<Map<String, Object>> equMaps = new ArrayList<Map<String, Object>>();
		for (BoardSolutionConfig item : boardSolutionConfigs) {
			if (!codes.contains(item.getEquipmentCode())) {
				codes.add(item.getEquipmentCode());
				equModel = new Equipments();
				equModel.setCode(item.getEquipmentCode());
				equModel.setLanguage(CommonUtils.LOGIN_LANGUAGE);
				equModel = equipmentsDao.selectEntityByCondition(equModel);
				if (equModel != null) {
					Map<String, Object> equMap = new HashMap<String, Object>();
					Map<String, Object> config = new HashMap<String, Object>();
					List<EquipmentsAttr> attrList = equipmentsAttrDao
							.selectListByEquipmentCode(item.getEquipmentCode());// 根据设备编码查询设备属性
					for (EquipmentsAttr equipmentsAttr : attrList) {
						if (CommonUtils.isNotEmpty(equipmentsAttr.getCustomTag())) {
							config.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
						}
					}
					equMap.put("code", equModel.getCode());
					equMap.put("name", equModel.getName());
					equMap.put("configs", config);
					equMaps.add(equMap);
				}
			}
			if (!codes.contains(item.getBoardTemplateCode())) {
				item.setCode(item.getBoardTemplateCode());
				codes.add(item.getBoardTemplateCode());
				BoardTemplate boardTemplate = new BoardTemplate();
				boardTemplate.setLanguage(CommonUtils.LOGIN_LANGUAGE);
				boardTemplate.setCode(item.getBoardTemplateCode());
				boardTemplate = boardTemplateDao.selectEntityByCondition(boardTemplate);
				item.setName(boardTemplate.getName());
				item.setList(JSON.parseArray(item.getSendContent(), BoardSolutionConfig.Data.class));
				newBoardSolutionConfigs.add(item);
			}
			solution.setEquipmentClassify(item.getEquipmentClassify());
		}
		solution.setEquipments(equMaps);
		solution.setBoardSolutionConfig(newBoardSolutionConfigs);
		map.put("model", solution);
		return map;
	}

	@Override
	public int updateBoardSolutionConfig(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		int returnCode = 0;
		BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();// 创建情报板配置对象
		int result = controlSolutionDao.updateByCondition(controlSolution);
		if (result == BaseLogicImpl.SUCCESS_NUM) {
			if (CommonUtils.isNotEmpty(controlSolution.getEquipmentClassify())
					&& CommonUtils.isNotEmpty(controlSolution.getList())) {
				// 先删除情报板配置
				boardSolutionConfig.setSolutionCode(controlSolution.getCode());
				boardSolutionConfigDao.deleteByCondition(boardSolutionConfig);
				List<String> equCodes = JSON.parseArray(controlSolution.getEquipmentCode(), String.class);// 获取所有需要配置的设备
				List<BoardSolutionConfig> list = JSON.parseArray(controlSolution.getList(), BoardSolutionConfig.class);// 将复杂的JSON类型的数据转换成List
				// if (controlSolution.getType() == AUTO_MODE) {// 自动需要向配置表插入设备配置数据
				for (String equCode : equCodes) {
					for (BoardSolutionConfig conSolConfig : list) {
						boardSolutionConfig = new BoardSolutionConfig();// 创建情报板配置对象
						// 将json对象放入情报板控制配置对象中
						boardSolutionConfig.setSolutionCode(controlSolution.getCode());
						boardSolutionConfig.setEquipmentCode(equCode);
						boardSolutionConfig.setEquipmentClassify(controlSolution.getEquipmentClassify());
						List<BoardSolutionConfig.Data> rowDatas = JSON.parseArray(conSolConfig.getData(),
								BoardSolutionConfig.Data.class);
						boardSolutionConfig.setRowCount(rowDatas.size());
						boardSolutionConfig.setSpeed(conSolConfig.getSpeed());
						boardSolutionConfig.setStayTime(conSolConfig.getStayTime());
						boardSolutionConfig.setColor(conSolConfig.getColor());
						boardSolutionConfig.setDisplayType(conSolConfig.getDisplayType());
						boardSolutionConfig.setSendContent(conSolConfig.getData());
						boardSolutionConfig.setBoardTemplateCode(conSolConfig.getCode());
						boardSolutionConfig.setComment(controlSolution.getComment());
						returnCode = boardSolutionConfigDao.insert(boardSolutionConfig);
					}
				}
			}
		}
		return returnCode;
	}

	@Override
	public int deleteBoardSolutionConfig(ControlSolution controlSolution) {
		CommonUtils.ControlSolutionCacheMap = null;
		BoardSolutionConfig boardSolutionConfig = new BoardSolutionConfig();
		boardSolutionConfig.setSolutionCode(controlSolution.getCode());
		int result = controlSolutionDao.deleteByCondition(controlSolution);
		if (result == BaseLogicImpl.SUCCESS_NUM) {
			boardSolutionConfigDao.deleteByCondition(boardSolutionConfig);
		} else {
			result = BaseLogicImpl.ERROR_NUM;
		}
		return result;
	}

	/**
	 * 根据触发器编码查询群控方案
	 * 
	 * @param tiggerCode
	 * @return
	 */
	public List<ControlSolution> selectControlSolutions(String tiggerCode) {
		// TODO Auto-generated method stub
		return controlSolutionDao.selectControlSolutions(tiggerCode);
	}

	@Override
	public int sendControllerSolution(String code) {
		List<ControlSolution> list = controlSolutionDao.sendControllerSolution(code);
		int result = 0;
		List<List<ControlModel>> lists = new ArrayList<List<ControlModel>>();
		if (list != null && list.size() > 0) {
			for (ControlSolution controlSolution : list) {
				if (controlSolution.getSolutionType() != null
						&& controlSolution.getSolutionType() == CommonUtils.CONTROL_SOLUTION_DEVICE_TYPE) {
					List<ControlModel> data = equipmentControlLogic.generateControllerCommond(controlSolution);// 生成控制命令
					if (JudgeNullUtil.iList(data)) {
						lists.add(data);
					}
				}
			}
			if (JudgeNullUtil.iList(lists)) {
				List<ControlModel> controlModelList = new ArrayList<ControlModel>();
				for (List<ControlModel> point : lists) {
					for (ControlModel controlModel : point) {
						controlModelList.add(controlModel);
					}
				}
				EquipmentControlLog logs = RemoteControllerUtil.controller(controlModelList);// 发送控制指令到bolomi
				result = logs.getCode();
				equipmentControlLogMongoDBDao.save(logs);// 保存发送命令
			}
			result = 1;
		}
		return result;
	}

	@Override
	public int sendControllerSolutionRecover(String code, int isRecover) {
		List<ControlSolution> list = controlSolutionDao.sendControllerSolutionRecover(code, isRecover);
		int result = 0;
		List<List<ControlModel>> lists = new ArrayList<List<ControlModel>>();
		if (list != null && list.size() > 0) {
			for (ControlSolution controlSolution : list) {
				if (controlSolution.getSolutionType() != null
						&& controlSolution.getSolutionType() == CommonUtils.CONTROL_SOLUTION_DEVICE_TYPE) {
					List<ControlModel> data = equipmentControlLogic.generateControllerCommond(controlSolution);// 生成控制命令
					if (JudgeNullUtil.iList(data)) {
						lists.add(data);
					}
				}
			}
			if (JudgeNullUtil.iList(lists)) {
				List<ControlModel> controlModelList = new ArrayList<ControlModel>();
				for (List<ControlModel> point : lists) {
					for (ControlModel controlModel : point) {
						controlModelList.add(controlModel);
					}
				}
				EquipmentControlLog logs = RemoteControllerUtil.controller(controlModelList);// 发送控制指令到bolomi
				result = logs.getCode();
				equipmentControlLogMongoDBDao.save(logs);// 保存发送命令
			}
			result = 1;
		}
		return result;
	}

	@Override
	public List<ControlSolution> selectControlSolutionByFaultCode(String triggerCodes) {
		return controlSolutionDao.selectControlSolutionByFaultCode(triggerCodes);
	}

	// public List<ControlSolutionExpression> selectScheduleExpression(Integer
	// isRecover) {
	// return controlSolutionDao.selectScheduleExpression(isRecover);
	// }
	@Override
	public List<ControlSolutionExpre> selectScheduleExpression(Integer isRecover) {
		return controlSolutionDao.selectScheduleExpression(isRecover);
	}

	@Override
	public ControlSolution selectSolutionExpression(ControlSolution controlSolution, int isRecover) {
		ControlSolution solution = controlSolutionDao.selectEntityByCondition(controlSolution);
		if (solution != null) {
			ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
			controlSolutionConfig.setSolutionCode(solution.getCode());
			controlSolutionConfig.setIsRecover(isRecover);
			List<ControlSolutionConfig> selectListBySolution = controlSolutionConfigDao
					.selectListByConfigRecover(controlSolutionConfig);
			List<ControlSolutionConfig.Config> configs;
			for (ControlSolutionConfig model : selectListBySolution) {
				configs = new ArrayList<>();
				String config = model.getConfig();
				if (CommonUtils.isNotEmpty(config)) {
					configs.addAll(JSON.parseArray(config, ControlSolutionConfig.Config.class));
				}
				HashMap<String, Object> map = new HashMap<String, Object>();
				String equipmentCode = model.getCode();// 获取设备编码
				List<EquipmentsAttr> attrList = equipmentsAttrDao.selectListByEquipmentCode(equipmentCode);// 根据设备编码查询设备属性
				for (EquipmentsAttr equipmentsAttr : attrList) {
					if (equipmentsAttr.getCustomTag() != null && equipmentsAttr.getCustomTag() != "") {
						map.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
					}
				}
				model.setConfigs(map);
				model.setTextConfig(configs);
			}
			solution.setControlSolutionConfig(selectListBySolution);
			ControlSolutionExpre controlSolutionExpression = new ControlSolutionExpre();
			controlSolutionExpression = controlSolutionDao.selectSolutionExpression(controlSolution);
			if (controlSolutionExpression != null) {
				JSONArray listObject = new JSONArray();

				String code = controlSolutionExpression.getCode();
				String names = controlSolutionExpression.getName();
				String pointCodes = controlSolutionExpression.getPointCode();
				String conditions = controlSolutionExpression.getCondition();
				String solutionCode = controlSolutionExpression.getSolutionCode();

				// 对conditions表达式进行处理
				String conditionsString = getConditionsString(conditions);
				String[] conditionArray = conditionsString.split(",");

				// 对pointCodes的处理
				String[] pointCodeArray = pointCodes.split(",");
				// 对name的处理
				String[] nameArray = names.split(",");
				for (int i = 0; i < conditionArray.length; i++) {
					JSONObject map = new JSONObject();
					map.put("code", code);
					if (nameArray[i] != null) {
						map.put("name", nameArray[i]);
					}
					if (pointCodeArray[i] != null) {
						map.put("point_code", pointCodeArray[i]);
					}
					if (conditionArray[i] != null) {
						map.put("condition", conditionArray[i]);
					}
					map.put("solution_code", solutionCode);
					listObject.add(map);
				}
				String listObjectString = listObject.toJSONString();
				solution.setExpressArray(listObjectString);

				// 群控没改之前代码
				// List<ControlSolutionExpre> expreList =
				// controlSolutiionExpression.getPoints();
				// JSONArray listObject=new JSONArray();
				// for(ControlSolutionExpre solutionExpre : expreList) {
				//
				// String code = solutionExpre.getCode();
				// String name = solutionExpre.getName();
				// String pointCode = solutionExpre.getPointCode();
				// String condition = solutionExpre.getCondition();
				// String solutionCode = solutionExpre.getSolutionCode();
				// JSONObject map = new JSONObject();
				// map.put("code", code);
				// map.put("name", name);
				// map.put("point_code", pointCode);
				// map.put("condition", condition);
				// map.put("solution_code", solutionCode);
				// listObject.add(map);
				// }
				// String listObjectString = listObject.toJSONString();
				// solution.setExpressArray(listObjectString);

			}
		}
		return solution;
	}

	@Override
	public ControlSolution selectControlSolutionAllKind(ControlSolution controlSolution) {
		ControlSolution solution = controlSolutionDao.selectEntityByCondition(controlSolution);
		if (solution != null) {
			String equipmentSectionCode = "";
			ControlSolutionConfig controlSolutionConfig = new ControlSolutionConfig();
			controlSolutionConfig.setSolutionCode(solution.getCode());
			controlSolutionConfig.setIsRecover(0);// 触发
			List<ControlSolutionConfig> selectListBySolutionTriger = controlSolutionConfigDao
					.selectListByConfigRecover(controlSolutionConfig);
			List<ControlSolutionConfig.Config> configs;
			for (ControlSolutionConfig model : selectListBySolutionTriger) {
				if(equipmentSectionCode == "" && model.getEquipmentCode() != null && model.getEquipmentCode() != "") {
					equipmentSectionCode = model.getEquipmentCode();
				}
				configs = new ArrayList<>();
				if(model.getEquipmentClassify().equals("CDZSQ")){
					EquipmentPoints equipPoint = equipmentPointsDao.selectCDZSQIsLeftByEquipmentCode(model.getEquipmentCode());
					if(equipPoint != null && equipPoint.getIndexType() != null) {
						model.setSelectTrue(true);
					}else {
						model.setSelectTrue(false);
					}
				}
				String config = model.getConfig();
				if (CommonUtils.isNotEmpty(config)) {
					configs.addAll(JSON.parseArray(config, ControlSolutionConfig.Config.class));
				}

				HashMap<String, Object> map = new HashMap<String, Object>();
				String equipmentCode = model.getCode();// 获取设备编码
				List<EquipmentsAttr> attrList = equipmentsAttrDao.selectListByEquipmentCode(equipmentCode);// 根据设备编码查询设备属性
				for (EquipmentsAttr equipmentsAttr : attrList) {
					if (equipmentsAttr.getCustomTag() != null && equipmentsAttr.getCustomTag() != "") {
						map.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
					}
				}
				model.setConfigs(map);
				model.setTextConfig(configs);
			}
			solution.setControlSolutionConfig(selectListBySolutionTriger);
			controlSolutionConfig = new ControlSolutionConfig();
			controlSolutionConfig.setSolutionCode(solution.getCode());
			controlSolutionConfig.setIsRecover(1);// 恢复
			List<ControlSolutionConfig> selectListBySolutionRecover = controlSolutionConfigDao
					.selectListByConfigRecover(controlSolutionConfig);
			for (ControlSolutionConfig model : selectListBySolutionRecover) {
				if(equipmentSectionCode == "" && model.getEquipmentCode() != null && model.getEquipmentCode() != "") {
					equipmentSectionCode = model.getEquipmentCode();
				}
				configs = new ArrayList<>();
				if(model.getEquipmentClassify().equals("CDZSQ")){
					EquipmentPoints equipPoint = equipmentPointsDao.selectCDZSQIsLeftByEquipmentCode(model.getEquipmentCode());
					if(equipPoint != null && equipPoint.getIndexType() != null) {
						model.setSelectTrue(true);
					}else {
						model.setSelectTrue(false);
					}
				}
				String config = model.getConfig();
				if (CommonUtils.isNotEmpty(config)) {
					configs.addAll(JSON.parseArray(config, ControlSolutionConfig.Config.class));
				}
				HashMap<String, Object> map = new HashMap<String, Object>();
				String equipmentCode = model.getCode();// 获取设备编码
				List<EquipmentsAttr> attrList = equipmentsAttrDao.selectListByEquipmentCode(equipmentCode);// 根据设备编码查询设备属性
				for (EquipmentsAttr equipmentsAttr : attrList) {
					if (equipmentsAttr.getCustomTag() != null && equipmentsAttr.getCustomTag() != "") {
						map.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
					}
				}
				model.setConfigs(map);
				model.setTextConfig(configs);
			}
			solution.setControlSolutionConfigRecover(selectListBySolutionRecover);
			
			if (equipmentSectionCode != null && !"".equals(equipmentSectionCode)) {
				Section section = sectionDao.searchSectionInfo(equipmentSectionCode);
				if(solution.getTiggerCode() == null || solution.getTiggerCode() == "") {
					if(section != null && section.getCode() != null && section.getCode() != "") {
						solution.setTiggerCode(section.getCode());
					}
				}
			}
			
			ControlSolutionExpre controlSolutionExpre = new ControlSolutionExpre();
			controlSolutionExpre.setSolutionCode(solution.getCode());
			controlSolutionExpre.setIsRecover(0); // 0是触发的表达式
			controlSolutionExpre = controlSolutionDao.selectSolutionExpressionByRecover(controlSolutionExpre);
			if (controlSolutionExpre != null) {
				JSONArray listObject = new JSONArray();
				String code = controlSolutionExpre.getCode();
				String names = controlSolutionExpre.getName();
				String pointLocationName = null;
				String pointCode = null;
				String pointCodes = controlSolutionExpre.getPointCode();
				String conditions = controlSolutionExpre.getCondition();
				String solutionCode = controlSolutionExpre.getSolutionCode();
				Integer isRecover = controlSolutionExpre.getIsRecover();
				// 对conditions表达式进行处理
				String conditionsString = getConditionsString(conditions);
				String[] conditionArray = conditionsString.split(",");
				// 对pointCodes的处理
				String[] pointCodeArray = pointCodes.split(",");
				// 对name的处理
				String[] nameArray = names.split(",");
				for (int i = 0; i < conditionArray.length; i++) {
					JSONObject map = new JSONObject();
					map.put("code", code);
					if (nameArray[i] != null) {
						map.put("name", nameArray[i]);
					}
					if (pointCodeArray[i] != null) {
						pointCode = pointCodeArray[i];
						map.put("point_code", pointCodeArray[i]);
					}
					controlSolutionExpre = new ControlSolutionExpre();
					controlSolutionExpre.setPointCode(pointCode);
					controlSolutionExpre = controlSolutionDao.selectSolutionExprePointNameByRecover(controlSolutionExpre);
					if (controlSolutionExpre != null) {
						pointLocationName = controlSolutionExpre.getPointLocationName();
						map.put("pointLocationName", controlSolutionExpre.getPointLocationName());
					} else {
						map.put("pointLocationName", nameArray[i]);
					}
					if (conditionArray[i] != null) {
						map.put("condition", conditionArray[i]);
					}
					map.put("is_recover", isRecover);
					map.put("solution_code", solutionCode);
					listObject.add(map);
				}
				String listObjectString = listObject.toJSONString();
				solution.setExpressArray(listObjectString);
				
//				JSONArray listObject = new JSONArray();
//				String code = controlSolutionExpre.getCode();
//				String name = controlSolutionExpre.getName();
//				String pointCode = controlSolutionExpre.getPointCode();
//				String condition = controlSolutionExpre.getCondition();
//				String solutionCode = controlSolutionExpre.getSolutionCode();
//				
//				String pointLocationName = controlSolutionExpre.getPointLocationName();
//				Integer isRecover = controlSolutionExpre.getIsRecover();
//				JSONObject map = new JSONObject();
//				map.put("code", code);
//				map.put("name", name);
//				map.put("point_code", pointCode);
//				map.put("condition", condition);
//				map.put("solution_code", solutionCode);
//				map.put("is_recover", isRecover);
//				map.put("pointLocationName", pointLocationName);
//				
//				listObject.add(map);
//				String listObjectString = listObject.toJSONString();
//				solution.setExpressArray(listObjectString);
			}

			controlSolutionExpre = new ControlSolutionExpre();
			controlSolutionExpre.setSolutionCode(solution.getCode());
			controlSolutionExpre.setIsRecover(1); // 1是恢复的表达式
			controlSolutionExpre = controlSolutionDao.selectSolutionExpressionByRecover(controlSolutionExpre);
			if (controlSolutionExpre != null) {
				JSONArray listObject = new JSONArray();
				String code = controlSolutionExpre.getCode();
				String names = controlSolutionExpre.getName();
				String pointLocationName = null;
				String pointCode = null;
				String pointCodes = controlSolutionExpre.getPointCode();
				String conditions = controlSolutionExpre.getCondition();
				String solutionCode = controlSolutionExpre.getSolutionCode();
				Integer isRecover = controlSolutionExpre.getIsRecover();
				// 对conditions表达式进行处理
				String conditionsString = getConditionsString(conditions);
				String[] conditionArray = conditionsString.split(",");
				// 对pointCodes的处理
				String[] pointCodeArray = pointCodes.split(",");
				// 对name的处理
				String[] nameArray = names.split(",");
				for (int i = 0; i < conditionArray.length; i++) {
					JSONObject map = new JSONObject();
					map.put("code", code);
					if (nameArray[i] != null) {
						map.put("name", nameArray[i]);
					}
					if (pointCodeArray[i] != null) {
						pointCode = pointCodeArray[i];
						map.put("point_code", pointCodeArray[i]);
					}
					controlSolutionExpre = new ControlSolutionExpre();
					controlSolutionExpre.setPointCode(pointCode);
					controlSolutionExpre = controlSolutionDao.selectSolutionExprePointNameByRecover(controlSolutionExpre);
					if (controlSolutionExpre != null) {
						pointLocationName = controlSolutionExpre.getPointLocationName();
						map.put("pointLocationName", controlSolutionExpre.getPointLocationName());
					} else {
						map.put("pointLocationName", nameArray[i]);
					}
					if (conditionArray[i] != null) {
						map.put("condition", conditionArray[i]);
					}
					map.put("is_recover", isRecover);
					map.put("solution_code", solutionCode);
					listObject.add(map);
				}
				String listObjectString = listObject.toJSONString();
				solution.setExpressRecoverArray(listObjectString);
				
//				JSONArray listObject = new JSONArray();
//				String code = controlSolutionExpre.getCode();
//				String name = controlSolutionExpre.getName();
//				String pointCode = controlSolutionExpre.getPointCode();
//				String condition = controlSolutionExpre.getCondition();
//				String solutionCode = controlSolutionExpre.getSolutionCode();
//				Integer isRecover = controlSolutionExpre.getIsRecover();
//				String pointLocationName = controlSolutionExpre.getPointLocationName();
//				JSONObject map = new JSONObject();
//				map.put("code", code);
//				map.put("name", name);
//				map.put("point_code", pointCode);
//				map.put("condition", condition);
//				map.put("solution_code", solutionCode);
//				map.put("is_recover", isRecover);
//				map.put("pointLocationName", pointLocationName);
//				listObject.add(map);
//				String listObjectString = listObject.toJSONString();
//				solution.setExpressRecoverArray(listObjectString);
			}
		}
		return solution;
	}

	/**
	 * 
	 * @param conditions
	 *            形参:条件表达式
	 * @return 返回值是可以分割的字符串
	 */
	public String getConditionsString(String conditions) {
		StringBuffer conditionsBuffer = new StringBuffer();
		String stratWith = "";
		while (true) {
			// 判断字符串是开始的内容
			String substring = conditions.substring(0, 2);
			conditions = conditions.substring(2);
			// 以&&开头
			if ("&&".equals(substring)) { // 以&&开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else {
					conditionsBuffer.append(stratWith + conditions);
				}
			} else if ("||".equals(substring)) { // 以||开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else {
					conditionsBuffer.append(stratWith + conditions);
				}
			} else { // 不以&&或||开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				}  else {
					conditionsBuffer.append(stratWith + conditions);
				}
			}
			if ((conditions.length() - conditions.replace("&&", "").length()) / "&&".length() == 1
					&& !conditions.contains("||")) {
				conditionsBuffer.append(conditions);
				break;
			}
			if ((conditions.length() - conditions.replace("||", "").length()) / "||".length() == 1
					&& !conditions.contains("&&")) {
				conditionsBuffer.append(conditions);
				break;
			}
			if (!conditions.contains("||")&& !conditions.contains("&&")) {
				break;
			}
		}
		return conditionsBuffer.toString();
	}

	@Override
	public ControlSolutionExpre selectControlSolutionExpreByRecover(String solutionCode, int isRecover) {
		return controlSolutionDao.selectControlSolutionExpreByRecover(solutionCode, isRecover);
	}

	// 判断火灾异常等级后,要获取的数据
	public FireCRTEvent selectFireCRTEventByCode(@Param("code") String code, @Param("pointCode") String pointCode,
			@Param("isRecover") Integer isRecover) {
		return controlSolutionDao.selectFireCRTEventByCode(code, pointCode, isRecover);
	}
	
	@Override
	public Map<String, Object> selectPredictionBoard(ControlSolution controlSolution) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询情报板的信息
		List<ControlSolution> predictionBoards = controlSolutionDao.selectInformationBoard(controlSolution);
		for (ControlSolution solution : predictionBoards) {
			// 查询设备控制配置表
			BoardSolutionConfig model = new BoardSolutionConfig();
			model.setSolutionCode(solution.getCode());
			List<BoardSolutionConfig> boardSolutionConfigs = boardSolutionConfigDao.selectListBySolutionCode(model);
			Equipments equModel = null;
			Set<String> codes = new HashSet<String>();
			List<BoardSolutionConfig> newBoardSolutionConfigs = new ArrayList<>();
			List<Map<String, Object>> equMaps = new ArrayList<Map<String, Object>>();
			for (BoardSolutionConfig item : boardSolutionConfigs) {
				if (!codes.contains(item.getEquipmentCode())) {
					codes.add(item.getEquipmentCode());
					equModel = new Equipments();
					equModel.setCode(item.getEquipmentCode());
					equModel.setLanguage(CommonUtils.LOGIN_LANGUAGE);
					equModel = equipmentsDao.selectEntityByCondition(equModel);
					if (equModel != null) {
						Map<String, Object> equMap = new HashMap<String, Object>();
						Map<String, Object> config = new HashMap<String, Object>();
						List<EquipmentsAttr> attrList = equipmentsAttrDao
								.selectListByEquipmentCode(item.getEquipmentCode());// 根据设备编码查询设备属性
						for (EquipmentsAttr equipmentsAttr : attrList) {
							if (CommonUtils.isNotEmpty(equipmentsAttr.getCustomTag())) {
								config.put(equipmentsAttr.getCustomTag(), equipmentsAttr.getValue());
							}
						}
						equMap.put("code", equModel.getCode());
						equMap.put("name", equModel.getName());
						equMap.put("configs", config);
						equMaps.add(equMap);
					}
				}
				if (!codes.contains(item.getBoardTemplateCode())) {
					item.setCode(item.getBoardTemplateCode());
					codes.add(item.getBoardTemplateCode());
					BoardTemplate boardTemplate = new BoardTemplate();
					boardTemplate.setLanguage(CommonUtils.LOGIN_LANGUAGE);
					boardTemplate.setCode(item.getBoardTemplateCode());
					boardTemplate = boardTemplateDao.selectEntityByCondition(boardTemplate);
					item.setName(boardTemplate.getName());
					item.setList(JSON.parseArray(item.getSendContent(), BoardSolutionConfig.Data.class));
					newBoardSolutionConfigs.add(item);
				}
				solution.setEquipmentClassify(item.getEquipmentClassify());
			}
			solution.setEquipments(equMaps);
			solution.setBoardSolutionConfig(newBoardSolutionConfigs);
		}
		map.put("model", predictionBoards);
		return map;
	}
	
}