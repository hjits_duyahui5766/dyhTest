package com.cloudinnov.websocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cloudinnov.logic.AlarmWorkOrdersLogic;
import com.cloudinnov.utils.CommonUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 首页地图客户故障数据推送
 * @author chengning
 * @date 2016年8月5日下午5:46:26
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@ServerEndpoint("/homealarm/{code}/{type}/{language}")
public class HomeMapAlarmWebsocket {
	private static Logger logger = LoggerFactory.getLogger(HomeMapAlarmWebsocket.class);
	private static final String FAULT_REDIS_SPILT = "_";
	private String random;
	// 静态变量，用来记录当前在线连接数。
	private static int onlineCount = 0;
	// 用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
	public static ConcurrentMap<String, HomeMapAlarmWebsocket> webSocketMap = new ConcurrentHashMap<String, HomeMapAlarmWebsocket>();
	// 用来发送数据
	private Session session;
	@Autowired
	AlarmWorkOrdersLogic alarmWorkOrdersLogics;

	/**
	 * 打开连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@OnOpen
	public void onOpen(@PathParam("code") String code, @PathParam("type") String type,
			@PathParam("language") String language, Session session) throws JsonProcessingException, IOException {
		this.session = session;
		this.random = CommonUtils.getUUID();
		webSocketMap.put(code + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + type + FAULT_REDIS_SPILT + random,
				this);
		// 连接人数加1
		addOnlineCount();
		logger.debug("告警推送开始连接:" + code + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + type + FAULT_REDIS_SPILT
				+ random);
	}
	/**
	 * 收到客户端消息时触发 onMessage
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param select
	 * @param @param token
	 * @param @param message 参数
	 * @return void 返回类型
	 */
	@OnMessage
	public void onMessage(@PathParam("code") String code, @PathParam("type") String type,
			@PathParam("language") String language, String message) {
	}
	/**
	 * 异常时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnError
	public void onError(@PathParam("code") String code, @PathParam("type") String type,
			@PathParam("language") String language, Throwable throwable, Session session) {
		logger.error("告警推送异常:" + code + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + type + FAULT_REDIS_SPILT
				+ random);
		logger.error(throwable.getMessage(), throwable);
	}
	/**
	 * 关闭连接时触发
	 * @param relationId
	 * @param userCode
	 * @param session
	 */
	@OnClose
	public void onClose(@PathParam("code") String code, @PathParam("type") String type,
			@PathParam("language") String language, Session session) {
		webSocketMap
				.remove(code + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + type + FAULT_REDIS_SPILT + random);
		subOnlineCount(); // 在线数减1
		logger.debug("告警推送连接关闭:" + code + FAULT_REDIS_SPILT + language + FAULT_REDIS_SPILT + type + FAULT_REDIS_SPILT
				+ random);
	}
	/**
	 * 推送工单消息
	 * @param message
	 * @throws IOException
	 */
	public synchronized void sendMessage(String message) throws IOException {
		this.session.getBasicRemote().sendText(message);
	}
	public static synchronized int getOnlineCount() {
		return onlineCount;
	}
	public static synchronized void addOnlineCount() {
		HomeMapAlarmWebsocket.onlineCount++;
	}
	public static synchronized void subOnlineCount() {
		HomeMapAlarmWebsocket.onlineCount--;
	}
}
