package com.cloudinnov.utils.model;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;

public class ByteModel {
	public enum TypeEnum {
		/**
		 * 取可变信息标志的当前故障
		 * @fieldName: GET_FAULT
		 * @fieldType: Color
		 * @Description: TODO
		 */
		GET_FAULT('0', '1'),
		/**
		 * 设置可变信息标志的亮度调节方式
		 * @fieldName: SET_BRIGHT_MODEL
		 * @fieldType: Color
		 * @Description: TODO
		 */
		SET_BRIGHT_MODEL('0', '4'),
		/**
		 * 设置可变信息标志的当前显示亮度
		 * @fieldName: SET_LIGHT
		 * @fieldType: Color
		 * @Description: TODO
		 */
		SET_LIGHT('0', '5'),
		/**
		 * 取可变信息标志的当前亮度调节方式和显示亮度
		 * @fieldName: GET_LIGHT_AND_MODEL
		 * @fieldType: Color
		 * @Description: TODO
		 */
		GET_LIGHT_AND_MODEL('0', '6'),
		/**
		 * 向可变信息标志上载文件
		 * @fieldName: UPLOAD_DATA
		 * @fieldType: Color
		 * @Description: TODO
		 */
		UPLOAD_DATA('1', '0'),
		/**
		 * 取可变信息标志的当前显示内容
		 * @fieldName: GET_NOW_CONTENT
		 * @fieldType: Color
		 * @Description: TODO
		 */
		GET_NOW_CONTENT('9', '7'),
		/**
		 * 使可变信息标志显示预置的播放表
		 * @fieldName: PRESET_CONTENT
		 * @fieldType: Color
		 * @Description: TODO
		 */
		PRESET_CONTENT('9', '8');
		private char start;
		private char end;

		private TypeEnum(char start, char end) {
			this.start = start;
			this.end = end;
		}
		public char getStart() {
			return start;
		}
		public void setStart(char start) {
			this.start = start;
		}
		public char getEnd() {
			return end;
		}
		public void setEnd(char end) {
			this.end = end;
		}
	}

	public static byte[] data, form;
	public static final int START_COMMAND = 2;
	public static final int END_COMMAND = 3;
	/**
	 * 汉威
	 */
	public static final byte[] SIGN_ADDRESS = new byte[] { '0', '1' };
	public static final String CHARSET = "GB2312";
	private static final char FILE_NAME_END_IDENT = '+';
	public static final String FILE_NAME = "play.lst";
	public static final String PLAY_LIST_START = "[Playlist]\r\nItem_No=2\r\nitem0 = 0, 0, 0\r\n";
	public static final String PLAY_LIST_TEMPLATE = "item{0} = {1}, {2}, {3}, \\C{4}{5}\\f{6}{7}\\c{8}{9}{10}{11}\\S{12}{13}\r\n";
	public static final String PLAY_LIST_NOSTYLE_TEMPLATE = "item{0} = {1}, {2}, \\C{3}{4}\\f{5}{6}\\c{7}{8}{9}{10}\\S{11}{12}\r\n";
	private int index = 0;
	private int fromIndex = 0;

	private void head(int dataIndex, int formIndex) {
		data = new byte[dataIndex];
		form = new byte[formIndex];
		data[index++] = START_COMMAND;
		data[index++] = SIGN_ADDRESS[0];
		data[index++] = SIGN_ADDRESS[1];
		// 用于Crc校验
		form[fromIndex++] = SIGN_ADDRESS[0];
		form[fromIndex++] = SIGN_ADDRESS[1];
	}
	/**
	 * 获取故障 需要单独处理
	 * @Title: getFault
	 * @Description: TODO
	 * @return
	 * @return: byte[]
	 */
	public byte[] getFault() {
		head(8, 4);
		data[index++] = (byte) TypeEnum.GET_FAULT.getStart();
		data[index++] = (byte) TypeEnum.GET_FAULT.getEnd();
		form[fromIndex++] = (byte) TypeEnum.GET_FAULT.getStart();
		form[fromIndex++] = (byte) TypeEnum.GET_FAULT.getEnd();
		data[index++] = (byte) (crcTable(form) / 256 % 256);
		data[index++] = (byte) (crcTable(form) % 256);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] getNowContent() {
		head(8, 4);
		data[index++] = (byte) TypeEnum.GET_NOW_CONTENT.getStart();
		data[index++] = (byte) TypeEnum.GET_NOW_CONTENT.getEnd();
		form[fromIndex++] = (byte) TypeEnum.GET_NOW_CONTENT.getStart();
		form[fromIndex++] = (byte) TypeEnum.GET_NOW_CONTENT.getEnd();
		data[index++] = (byte) ((getCrc(form) >> 8) & 0xff);
		data[index++] = (byte) (getCrc(form) & 0xff);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] setBrightModel(char type) {
		head(6, 6);
		data[index++] = (byte) TypeEnum.SET_BRIGHT_MODEL.getStart();
		data[index++] = (byte) TypeEnum.SET_BRIGHT_MODEL.getEnd();
		form[fromIndex++] = (byte) TypeEnum.SET_BRIGHT_MODEL.getStart();
		form[fromIndex++] = (byte) TypeEnum.SET_BRIGHT_MODEL.getEnd();
		data[index++] = (byte) type;
		form[fromIndex++] = (byte) type;
		data[index++] = (byte) ((getCrc(form) >> 8) & 0xff);
		data[index++] = (byte) (getCrc(form) & 0xff);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] getLightModel() {
		head(8, 5);
		data[index++] = (byte) TypeEnum.GET_LIGHT_AND_MODEL.getStart();
		data[index++] = (byte) TypeEnum.GET_LIGHT_AND_MODEL.getEnd();
		form[fromIndex++] = (byte) TypeEnum.GET_LIGHT_AND_MODEL.getStart();
		form[fromIndex++] = (byte) TypeEnum.GET_LIGHT_AND_MODEL.getEnd();
		data[index++] = (byte) ((getCrc(form) >> 8) & 0xff);
		data[index++] = (byte) (getCrc(form) & 0xff);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] setPresetContent(String presetId) {
		head(11, 7);
		char[] strChar = presetId.toCharArray();
		data[index++] = (byte) TypeEnum.PRESET_CONTENT.getStart();
		data[index++] = (byte) TypeEnum.PRESET_CONTENT.getEnd();
		data[index++] = (byte) strChar[0];
		data[index++] = (byte) strChar[1];
		data[index++] = (byte) strChar[2];
		form[fromIndex++] = (byte) TypeEnum.PRESET_CONTENT.getStart();
		form[fromIndex++] = (byte) TypeEnum.PRESET_CONTENT.getEnd();
		form[fromIndex++] = (byte) strChar[0];
		form[fromIndex++] = (byte) strChar[1];
		form[fromIndex++] = (byte) strChar[2];
		data[index++] = (byte) ((getCrc(form) >> 8) & 0xff);
		data[index++] = (byte) (getCrc(form) & 0xff);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] setText(String fileName, String content) throws UnsupportedEncodingException {
		int fromLength = 9 + fileName.getBytes().length + content.getBytes(CHARSET).length;
		int dataLength = 13 + fileName.getBytes().length + content.getBytes(CHARSET).length;
		head(dataLength, fromLength);
		data[index++] = (byte) TypeEnum.UPLOAD_DATA.getStart();
		data[index++] = (byte) TypeEnum.UPLOAD_DATA.getEnd();
		form[fromIndex++] = (byte) TypeEnum.UPLOAD_DATA.getStart();
		form[fromIndex++] = (byte) TypeEnum.UPLOAD_DATA.getEnd();
		for (byte name : fileName.getBytes()) {// 文件名
			data[index++] = name;
			form[fromIndex++] = name;
		}
		data[index++] = FILE_NAME_END_IDENT;// 表明文件名的结束
		form[fromIndex++] = FILE_NAME_END_IDENT;// 表明文件名的结束
		// 文件指针偏移 4 字节十六进制数，先发高字节，后发低字节
		data[index++] = 0x00;
		form[fromIndex++] = 0x00;
		data[index++] = 0x00;
		form[fromIndex++] = 0x00;
		data[index++] = 0x00;
		form[fromIndex++] = 0x00;
		data[index++] = 0x00;
		form[fromIndex++] = 0x00;
		for (byte cont : content.getBytes(CHARSET)) {// 文件内容
			data[index++] = cont;
			form[fromIndex++] = cont;
		}
		data[index++] = (byte) ((getCrc(form) >> 8) & 0xff);
		data[index++] = (byte) (getCrc(form) & 0xff);
		data[index++] = END_COMMAND;
		return data;
	}
	public byte[] setText(BxClientModel bxModel) throws UnsupportedEncodingException {
		if (CommonUtils.isNotEmpty(bxModel.getText())) {
			JSONArray texts = JSON.parseArray(bxModel.getText());
			if (JudgeNullUtil.iList(texts)) {
				String content = null;
				int itemNoStart = 1;
				for (int i = 0; i < texts.size(); i++) {
					if (bxModel.getStyleIndex() == 0 || bxModel.getStyleIndex() == 1) {
						content = MessageFormat.format(PLAY_LIST_NOSTYLE_TEMPLATE, itemNoStart++, bxModel.getStayTime(),
								bxModel.getStyleIndex(), bxModel.getHorizontal(), bxModel.getVertical(),
								getFontName(bxModel.getFontName()), bxModel.getFontSize() + bxModel.getFontSize(),
								bxModel.getFontColorRGB().getRed(), bxModel.getFontColorRGB().getGreen(),
								bxModel.getFontColorRGB().getBlue(), bxModel.getFontColorRGB().getAlpha(),
								bxModel.getSpacing(), texts.getString(i));
					} else {
						content = MessageFormat.format(PLAY_LIST_TEMPLATE, itemNoStart++, bxModel.getStayTime(),
								bxModel.getStyleIndex(), bxModel.getSpeed(), bxModel.getHorizontal(),
								bxModel.getVertical(), getFontName(bxModel.getFontName()),
								bxModel.getFontSize() + bxModel.getFontSize(), bxModel.getFontColorRGB().getRed(),
								bxModel.getFontColorRGB().getGreen(), bxModel.getFontColorRGB().getBlue(),
								bxModel.getFontColorRGB().getAlpha(), bxModel.getSpacing(), texts.getString(i));
					}
				}
				if (CommonUtils.isNotEmpty(content)) {
					return setText(FILE_NAME, content);
				}
			}
		}
		return null;
	}
	public static int crcTable(byte[] bytes) {
		long c, treat, bcrc;
		int wcrc = 0;
		short i, j;
		for (i = 0; i < bytes.length; i++) {
			c = bytes[i];
			for (j = 0; j < 8; j++) {
				treat = (c & 0x80);
				c <<= 1;
				bcrc = (wcrc >> 8) & 0x80;
				wcrc <<= 1;
				if (treat != bcrc)
					wcrc ^= 0x1021;
			}
		}
		return wcrc;
	}

	static int[] crc_table = { 0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7, 0x8108, 0x9129, 0xA14A,
			0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF, 0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
			0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE, 0x2462, 0x3443, 0x0420, 0x1401, 0x64E6,
			0x74C7, 0x44A4, 0x5485, 0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D, 0x3653, 0x2672,
			0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4, 0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D,
			0xC7BC, 0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823, 0xC9CC, 0xD9ED, 0xE98E, 0xF9AF,
			0x8948, 0x9969, 0xA90A, 0xB92B, 0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12, 0xDBFD,
			0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A, 0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03,
			0x0C60, 0x1C41, 0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49, 0x7E97, 0x6EB6, 0x5ED5,
			0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70, 0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
			0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F, 0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004,
			0x4025, 0x7046, 0x6067, 0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E, 0x02B1, 0x1290,
			0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256, 0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C,
			0xC50D, 0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 0xA7DB, 0xB7FA, 0x8799, 0x97B8,
			0xE75F, 0xF77E, 0xC71D, 0xD73C, 0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634, 0xD94C,
			0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB, 0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1,
			0x3882, 0x28A3, 0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A, 0x4A75, 0x5A54, 0x6A37,
			0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92, 0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
			0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1, 0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B,
			0xBFBA, 0x8FD9, 0x9FF8, 0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0 };

	static short getCrc(byte[] buffer) {
		short crc = 0;
		for (int i = 0; i < buffer.length; i++) {
			crc = (short) (crc_table[((crc >> 8) ^ buffer[i]) & 0xFF] ^ (crc << 8));
		}
		return crc;
	}
	public String getFontName(String fontName) {
		switch (fontName) {
			case "宋体":
				return "s";
			case "楷体":
				return "k";
			case "黑体":
				return "h";
			default:
				return "s";
		}
	}
}
