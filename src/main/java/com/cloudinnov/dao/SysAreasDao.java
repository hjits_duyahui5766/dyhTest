package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.SysAreas;

public interface SysAreasDao extends IBaseDao<SysAreas> {

	public int insertInfo(SysAreas sa);

	public List<SysAreas> tableList(Map<String, Object> map);

	public List<SysAreas> phoneList(SysAreas sysAreas);
	
	public List<SysAreas> search(Map<String, Object> map);
}