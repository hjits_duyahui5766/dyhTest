package com.cloudinnov.logic;

import javax.servlet.http.HttpServletRequest;

import com.cloudinnov.model.SysLogs;

/**
 * @author chengning
 * @date 2016年2月18日下午4:02:51
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface SysLogsLogic extends IBaseLogic<SysLogs> {


	/** 记录登录用户信息
	* saveLoginLog 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param log
	* @param @param request
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int saveLoginLog(SysLogs log,HttpServletRequest request);
}
