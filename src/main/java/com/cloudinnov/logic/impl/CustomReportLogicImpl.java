package com.cloudinnov.logic.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.CustomReportDao;
import com.cloudinnov.logic.CustomReportLogic;
import com.cloudinnov.model.CustomReport;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service
public class CustomReportLogicImpl extends BaseLogicImpl<CustomReport> implements CustomReportLogic {
	
	public final static String REPORT_TYPE_LINE = "line";
	public final static String REPORT_TYPE_PIE = "pie";
	public final static String REPORT_TYPE_BAR = "bar";
	
	public static final String REPORT_TIME_TYPE_YEAR = "year";
	public static final String REPORT_TIME_TYPE_QUARTER = "quarter";
	public static final String REPORT_TIME_TYPE_MONTH = "month";
	public static final String REPORT_TIME_TYPE_WEEK = "week";
	public static final String REPORT_TIME_TYPE_DAY = "day";
	public static final String REPORT_TIME_TYPE_HOUR = "hour";
	public static final String REPORT_TIME_TYPE_MINUTE = "minute";
	public static final String REPORT_TIME_TYPE_SECOND = "second";
	
	@Autowired
	public CustomReportDao customReportDao;

	/**
	 * 报表添加
	 */
	public int save(CustomReport customReport) {
		customReport.setCode(CodeUtil.customerReportCode(5));
		int result = customReportDao.insert(customReport);
		if (result == CommonUtils.SUCCESS_NUM) {
			return customReportDao.insertInfo(customReport);
		} else {
			return CommonUtils.DEFAULT_NUM;
		}
	}

	/**
	 * 报表修改
	 */
	@Override
	public int update(CustomReport customReport) {
		int returnCode = customReportDao.updateByCondition(customReport);
		returnCode = customReportDao.updateInfoByCondition(customReport);
		return returnCode;
	}

	@Override
	public Page<CustomReport> search(PageModel page, Map<String, Object> params) {
		PageHelper.startPage(page.getIndex(), page.getSize());
		return (Page<CustomReport>) customReportDao.search(params);
	}
}
