package com.cloudinnov.model;

public class EquipmentsAttr extends BaseModel {
	public static final String IP = "ip";
	public static final String PORT = "port";
	public static final String WIDHT = "width";
	public static final String HEIGHT = "height";
	private Integer id;
	private String equipmentCode;
	private String customTag;
	private String name;
	private String value;
	private Integer status;
	private String createTime;
	private String categoryCode;// 分类code

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getCustomTag() {
		return customTag;
	}
	public void setCustomTag(String customTag) {
		this.customTag = customTag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	@Override
	public String toString() {
		return "EquipmentsAttr [id=" + id + ", equipmentCode=" + equipmentCode + ", customTag=" + customTag + ", name="
				+ name + ", value=" + value + ", status=" + status + ", createTime=" + createTime + ", categoryCode="
				+ categoryCode + "]";
	}
}
