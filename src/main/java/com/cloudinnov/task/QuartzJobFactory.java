package com.cloudinnov.task;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cloudinnov.task.model.ScheduleJob;


/** 计划任务执行处 无状态
 * @ClassName: QuartzJobFactory 
 * @Description: TODO
 * @author: ningmeng
 * @date: 2016年11月29日 上午11:38:12  
 */
public class QuartzJobFactory implements Job {
	public final Logger log = Logger.getLogger(this.getClass());

	public void execute(JobExecutionContext context) throws JobExecutionException {
		ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("scheduleJob");
		TaskUtils.invokMethod(scheduleJob);
	}
}