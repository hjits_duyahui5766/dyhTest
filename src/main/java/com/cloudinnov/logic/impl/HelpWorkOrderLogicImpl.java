package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.HelpWorkOrderDao;
import com.cloudinnov.logic.HelpWorkOrderLogic;
import com.cloudinnov.model.HelpWorkOrder;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author chengning
 * @date 2016年5月4日上午11:02:58
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Service("HelpWorkOrderLogic")
public class HelpWorkOrderLogicImpl extends BaseLogicImpl<HelpWorkOrder> implements HelpWorkOrderLogic {

	
	private int digit = 5;
	
	@Autowired
	private HelpWorkOrderDao helpWorkOrderDao;
	
	public int save(HelpWorkOrder helpAlarmWork) {
		helpAlarmWork.setCode(CodeUtil.flmCode(digit));
		helpAlarmWork.setOrderStatus(CommonUtils.ORDER_STATUS_NEW);
		int returnCode = helpWorkOrderDao.insert(helpAlarmWork);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = helpWorkOrderDao.insertInfo(helpAlarmWork);
		} else {
			returnCode = 0;
		}
		return returnCode;
	}
	

}
