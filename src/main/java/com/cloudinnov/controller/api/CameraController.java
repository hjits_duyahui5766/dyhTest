package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.CameraControlLogic;
import com.cloudinnov.logic.EquipmentsAttrLogic;
import com.cloudinnov.model.CameraModel;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.HikCameraData;
import com.cloudinnov.model.Screen;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.TcpClientUtil;

/**
 * 大屏控制
 * @ClassName: CameraController
 * @Description: TODO
 * @author: ningmeng
 * @date: 2017年1月21日 下午10:30:17
 */
@Controller
@RequestMapping("/webapi/camera")
public class CameraController extends BaseController {
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private EquipmentsAttrLogic equipmentsAttrLogic;
	@Autowired
	private CameraControlLogic cameraControlLogic;

	@RequestMapping(value = "/cameraStatus", method = RequestMethod.GET)
	@ResponseBody
	public void cameraStatus(HttpServletRequest request, HttpServletResponse response, EquipmentsAttr model)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<EquipmentsAttr> configList = equipmentsAttrLogic.selectList(model, false);
		if (configList != null && configList.size() > 0) {
			CameraModel cameraModel = new CameraModel();
			for (EquipmentsAttr attr : configList) {
				if (attr.getCustomTag() != null) {
					if (attr.getCustomTag().equals(CameraModel.DEVICE_CODE)) {
						cameraModel.setDeviceId(attr.getValue());
					} else if (attr.getCustomTag().equals(CameraModel.PORT)) {
						cameraModel.setPort(Integer.parseInt(attr.getValue()));
					} else if (attr.getCustomTag().equals(CameraModel.IP)) {
						cameraModel.setIp(attr.getValue());
					}
				}
			}
			if (CommonUtils.isNotEmpty(cameraModel.getDeviceId())) {
				try {
					int status = cameraControlLogic.cameraStatus(cameraModel);
					if (status == CommonUtils.SUCCESS_NUM) {
						returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
					} else if (status == TcpClientUtil.CLIENT_ERROR_CODE) {
						returnData = returnJsonAllRequest(request, response, map, TcpClientUtil.CLIENT_ERROR_CODE, "");
					} else if (status == TcpClientUtil.SYSTEM_ERROR_CODE) {
						returnData = returnJsonAllRequest(request, response, map, TcpClientUtil.SYSTEM_ERROR_CODE, "");
					} else if (status == TcpClientUtil.UNKNOWN_STATUS_ERROR) {
						returnData = returnJsonAllRequest(request, response, map,
								TcpClientUtil.UNKNOWN_STATUS_ERROR_CODE, "");
					} else if (status == TcpClientUtil.ERROR_CODE) {
						returnData = returnJsonAllRequest(request, response, map, ERROR, "");
					}
				} catch (Exception e) {
					map.put("data", e);
					returnData = returnJsonAllRequest(request, response, map, ERROR, "");
				}
			} else {
				map.put("data", "设备摄像头控制参数未设置!");
				returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
			}
		} else {
			map.put("data", "摄像头投屏参数未设置!");
			returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/control", method = RequestMethod.POST)
	@ResponseBody
	public void controlCamera(HttpServletRequest request, HttpServletResponse response, EquipmentsAttr model,
			CameraModel cameraModel) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<EquipmentsAttr> configList = equipmentsAttrLogic.selectList(model, false);
		if (configList != null && configList.size() > 0) {
			for (EquipmentsAttr attr : configList) {
				if (attr.getCustomTag() != null) {
					if (attr.getCustomTag().equals(CameraModel.CAMERA_ID)) {
						cameraModel.setCameraId(attr.getValue());
						break;
					}
				}
			}
			if (CommonUtils.isNotEmpty(cameraModel.getCameraId()) && cameraModel.getControlCommand() != null) {
				try {
					int status = cameraControlLogic.controllerCamera(cameraModel);
					if (status == CommonUtils.DEFAULT_NUM) {
						returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
					} else if (status == TcpClientUtil.CLIENT_ERROR_CODE) {
						returnData = returnJsonAllRequest(request, response, map, TcpClientUtil.CLIENT_ERROR_CODE, "");
					} else if (status == TcpClientUtil.SYSTEM_ERROR_CODE) {
						returnData = returnJsonAllRequest(request, response, map, TcpClientUtil.SYSTEM_ERROR_CODE, "");
					} else if (status == TcpClientUtil.UNKNOWN_STATUS_ERROR) {
						returnData = returnJsonAllRequest(request, response, map,
								TcpClientUtil.UNKNOWN_STATUS_ERROR_CODE, "");
					} else {
						returnData = returnJsonAllRequest(request, response, map, ERROR, "");
					}
				} catch (Exception e) {
					map.put("data", e);
					returnData = returnJsonAllRequest(request, response, map, ERROR, "");
				}
			} else {
				map.put("data", "摄像头控制参数未设置!");
				returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
			}
		} else {
			map.put("data", "摄像头投屏参数未设置!");
			returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/switchCamera", method = RequestMethod.POST)
	@ResponseBody
	public void switchCamera(HttpServletRequest request, HttpServletResponse response, EquipmentsAttr model,
			CameraModel cameraModel) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<EquipmentsAttr> configList = equipmentsAttrLogic.selectList(model, false);
		if (configList != null && configList.size() > 0) {
			for (EquipmentsAttr attr : configList) {
				if (attr.getCustomTag() != null) {
					if (attr.getCustomTag().equals(CameraModel.INDEX_CODE)) {
						cameraModel.setIndexCode(attr.getValue());
						break;
					}
				}
			}
			if (CommonUtils.isNotEmpty(cameraModel.getIndexCode()) && cameraModel.getScreenId() != null) {
				try {
					String status = cameraControlLogic.switchCamera(cameraModel);
					if (status.equals(CommonUtils.CAREMA_OK)) {
						Screen screen = new Screen();
						screen.setEquipmentCode(model.getEquipmentCode());
						screen.setIndex(cameraModel.getScreenId());
						screen.setStatus(CommonUtils.SUCCESS_NUM);
						int result = cameraControlLogic.updateSwitchCamera(screen);
						if (result == 1) {
							returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
						} else {
							returnData = returnJsonAllRequest(request, response, map, ERROR, "");
						}
					} else if (status.equals(CommonUtils.CAREMA_NOT_FOUND)) {
						returnData = returnJsonAllRequest(request, response, map, CommonUtils.CAREMA_NOT_FOUND_CODE,
								"");
					} else {
						returnData = returnJsonAllRequest(request, response, map, ERROR, "");
					}
				} catch (Exception e) {
					map.put("data", e);
					returnData = returnJsonAllRequest(request, response, map, ERROR, "");
				}
			} else {
				map.put("data", "摄像头投屏参数未设置!");
				returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
			}
		} else {
			map.put("data", "摄像头投屏参数未设置!");
			returnData = returnJsonAllRequest(request, response, map, CommonUtils.PARAMETER_EXCEPTION_CODE, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 查询所有屏幕列表
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectAllScreen", method = RequestMethod.GET)
	@ResponseBody
	public void selectAllScreen(Screen screen, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<Screen> screeninfo = cameraControlLogic.selectScreen(screen);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (screeninfo.size() > 0) {
			map.put("list", screeninfo);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/selectScreenIsCheck", method = RequestMethod.GET)
	@ResponseBody
	public void selectScreenIsCheck(Screen screen, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Screen model = cameraControlLogic.select(screen);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> isCheck = new HashMap<String, Object>();
		if (model != null) {
			isCheck.put("isCheck", 1);
			map.put("model", isCheck);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			isCheck.put("isCheck", 0);
			map.put("model", isCheck);
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 查询海康平台所有摄像头进行同步
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param screen
	 * @param @param request
	 * @param @param response
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/syncHikCamera", method = RequestMethod.GET)
	public void syncHikCamera(HikCameraData model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		cameraControlLogic.saveAllHikCamera(model);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	/**
	 * 查询大屏进行同步
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param screen
	 * @param @param request
	 * @param @param response
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/syncHikScreens", method = RequestMethod.GET)
	public void syncScreens(HttpServletRequest request, HttpServletResponse response) throws IOException {
		cameraControlLogic.saveAllScreensByTwallCode(null);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
}
