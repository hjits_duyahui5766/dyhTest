package com.cloudinnov.model;

import java.util.List;

public class WorkOrderLogs  extends BaseModel{

    private String workOrderCode;
    
    private String customerCode;

    private String userCode;

    private String submitTime;

    //操作：1-解决工单 ，2-未解决工单（提供意见），3-转派
    private Integer operation;
    
    private String targetCompany;
    
    private String targetUser;
    
    //处理内容
    private String content;
    
    //解决方案
    private String solution;
    
    private AuthUsers user;
    
    private Integer type;
    
    private String imagesJson;
    
    private List<Attach> images;
    
    private String targetUserName;
    
    private Integer msgType;
    
    private String[] urls;
    
    private Integer canAnswer;
    
	public String getImagesJson() {
		return imagesJson;
	}

	public void setImagesJson(String imagesJson) {
		this.imagesJson = imagesJson;
	}

	public AuthUsers getUser() {
		return user;
	}

	public void setUser(AuthUsers user) {
		this.user = user;
	}

	

	public String getWorkOrderCode() {
		return workOrderCode;
	}

	public void setWorkOrderCode(String workOrderCode) {
		this.workOrderCode = workOrderCode;
	}

	public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

	public Integer getOperation() {
		return operation;
	}

	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	public String getTargetCompany() {
		return targetCompany;
	}

	public void setTargetCompany(String targetCompany) {
		this.targetCompany = targetCompany;
	}

	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	
	public Integer getType() {
		return type;
	}

	public List<Attach> getImages() {
		return images;
	}

	public void setImages(List<Attach> images) {
		this.images = images;
	}

	public String getTargetUserName() {
		return targetUserName;
	}

	public void setTargetUserName(String targetUserName) {
		this.targetUserName = targetUserName;
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public String[] getUrls() {
		return urls;
	}

	public void setUrls(String[] urls) {
		this.urls = urls;
	}

	public Integer getCanAnswer() {
		return canAnswer;
	}

	public void setCanAnswer(Integer canAnswer) {
		this.canAnswer = canAnswer;
	}
	
	
}