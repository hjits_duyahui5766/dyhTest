package com.cloudinnov.dao;

import com.cloudinnov.model.SysLanguage;

public interface SysLanguageDao extends IBaseDao<SysLanguage> {
}