package com.cloudinnov.model;

import com.cloudinnov.utils.CommonUtils;

public class PointExport extends BaseModel {
	private String customerCode;
	private String equipmentCode;
	private String xid;
	private String name;
	private String type;
	private String typeName;
	private String config;
	private String bomCode;
	private String comment;
	private Integer range;
	private Integer viewType;
	private Integer isCrucial;
	private Integer isProductivity;
	private Equipments equipment;
	private Companies company;
	private String value;
	@SuppressWarnings("unused")
	private String unit;
	private String time;
	@SuppressWarnings("unused")
	private String min;
	@SuppressWarnings("unused")
	private String max;
	private String meaningZero;
	private String meaningOne;
	private String colourZero;
	private String colourOne;
	private String bomName;
	private String kind;
	private String newCode;

	public String getNewCode() {
		return newCode;
	}
	public void setNewCode(String newCode) {
		this.newCode = newCode;
	}
	public Integer getIsProductivity() {
		return isProductivity;
	}
	public String getKind() {
		if (getCode() != null) {
			return getCode().substring(0, 2);
		}
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public void setIsProductivity(Integer isProductivity) {
		this.isProductivity = isProductivity;
	}
	public String getBomName() {
		return bomName;
	}
	public void setBomName(String bomName) {
		this.bomName = bomName;
	}
	public String getMin() {
		if (getType().equals("mn")) {
			return getConfig().split("\\|")[2];
		}
		return null;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMax() {
		if (getType().equals("mn")) {
			return getConfig().split("\\|")[4];
		}
		return null;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getUnit() {
		if (getType().equals("mn")) {
			return getConfig().split("\\|")[0];
		}
		return null;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getIsCrucial() {
		return isCrucial;
	}
	public void setIsCrucial(Integer isCrucial) {
		this.isCrucial = isCrucial;
	}
	public Integer getRange() {
		return range;
	}
	public void setRange(Integer range) {
		this.range = range;
	}
	public Integer getViewType() {
		return viewType;
	}
	public void setViewType(Integer viewType) {
		this.viewType = viewType;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		if (type.equals(CommonUtils.POINT_TYPE_MN)) {
			this.typeName = CommonUtils.ANALOG_QUANTITY_MSG;
		} else if (type.equals(CommonUtils.POINT_TYPE_SZ)) {
			this.typeName = CommonUtils.DIGITAL_QUANTITY_MSG;
		}
		this.type = type;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public String getBomCode() {
		return bomCode;
	}
	public void setBomCode(String bomCode) {
		this.bomCode = bomCode;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Equipments getEquipment() {
		return equipment;
	}
	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}
	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public String getXid() {
		return xid;
	}
	public void setXid(String xid) {
		this.xid = xid;
	}
	public String getMeaningZero() {
		if (getType().equals("sz")) {
			return getConfig().split("\\|")[0];
		}
		return null;
	}
	public void setMeaningZero(String meaningZero) {
		this.meaningZero = meaningZero;
	}
	public String getMeaningOne() {
		if (getType().equals("sz")) {
			return getConfig().split("\\|")[2];
		}
		return null;
	}
	public void setMeaningOne(String meaningOne) {
		this.meaningOne = meaningOne;
	}
	public String getColourZero() {
		if (getType().equals("sz")) {
			return getConfig().split("\\|")[4];
		}
		return null;
	}
	public void setColourZero(String colourZero) {
		this.colourZero = colourZero;
	}
	public String getColourOne() {
		if (getType().equals("sz")) {
			return getConfig().split("\\|")[6];
		}
		return null;
	}
	public void setColourOne(String colourOne) {
		this.colourOne = colourOne;
	}
}
