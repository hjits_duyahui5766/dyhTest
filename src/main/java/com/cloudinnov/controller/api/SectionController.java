package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.SectionLogic;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.Section;
import com.cloudinnov.model.SectionPlanConfig;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.github.pagehelper.Page;

/**
 * @author GuoBo
 */
@Controller
@RequestMapping("/webapi/section")
public class SectionController extends BaseController {
	@Autowired
	private SectionLogic sectionLogic;
	@Autowired
	private EquipmentsLogic equipmentsLogic;

	/**
	 * 路段添加
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段保存")
	public void save(Section section, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = sectionLogic.save(section);
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 根据路段code获取相应路段信息
	 * @param section
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "获取需要更新的路段信息")
	public void select(Section section, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Section result = sectionLogic.select(section);
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 路段更新
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段更新")
	public void update(Section section, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = sectionLogic.update(section);
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 路段删除
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段删除")
	public void delete(Section section, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = sectionLogic.delete(section);
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 路段列表（带分页）
	 * @param index 页码
	 * @param size 列表需展示数据的数量
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段列表")
	public void listPage(@Valid PageModel page, BindingResult error, Section section, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Page<Section> sections = null;
		sections = sectionLogic.selectListPage(page.getIndex(), page.getSize(), section, true);
		if (sections != null) {
			map.put("list", sections);
			map.put("total", sections.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 路段列表（无分页）
	 * @param index
	 * @param size
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/listNoPage")
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段列表")
	public void listNoPage(Section section, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		List<Section> sections = sectionLogic.selectList(section, true);
		if (sections != null) {
			map.put("list", sections);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 路段查询
	 */
	@RequestMapping(value = "/search")
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段查询")
	public void search(@Valid PageModel page, BindingResult error, String country, String province, String type,
			String city, String key, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Page<Section> result;
		result = sectionLogic.search(page.getIndex(), page.getSize(), country, province, city, key, type);
		if (result != null) {
			map.put("list", result);
			map.put("total", result.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/searchSectionInfo")
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "路段查询")
	public void searchSectionInfo(String equipCode,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Section result;
		result = sectionLogic.searchSectionInfo(equipCode);
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 路段详情
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/equipmentList")
	@ResponseBody
	@SystemLog(module = "路段详情", methods = "根据路段code查询设备")
	public void equipmentList(Section section, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map = sectionLogic.selectEquLineAlarmCountBySectionCode(section.getCode());
		map.put("equSum", 0);
		map.put("equStartSum", 0);
		map.put("equDisConnectSum", 0);
		map.put("equFaultSum", 0);
		Section result = sectionLogic.select(section);// 查询路段信息
		String returnData = "";
		if (result != null) {
			Equipments equipments = new Equipments();
			equipments.setSectionCode(section.getCode());
			equipments.setLanguage(section.getLanguage());
			List<Equipments> sections = equipmentsLogic.selectEquListBySectionCode(equipments);// 根据路段code查询设备
			map.put("equSum", sections.size());
			for (Equipments equ : sections) {
				if (equ.getCurrentState() == CommonUtils.EQU_STATE_NORMAL) {
					map.put("equStartSum", (int) map.get("equStartSum") + 1);
				} else if (equ.getCurrentState() == CommonUtils.EQU_STATE_OFFLINE) {
					map.put("equStartSum", (int) map.get("equDisConnectSum") + 1);
				} else if (equ.getCurrentState() == CommonUtils.EQU_STATE_FAULT) {
					map.put("equStartSum", (int) map.get("equFaultSum") + 1);
				}
			}
			map.put("model", result);
			map.put("list", sections);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 查询设备分类以及设备分类下的设备
	 * @param equipments
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "selectEquiClassifyAndEquipments", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "大屏管理", methods = "查询设备分类以及设备分类下的设备")
	public void selectEquiClassifyAndEquipments(Equipments equipments, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<Equipments> listInfo = equipmentsLogic.selectEquiClassifyAndEquipments(equipments);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (listInfo.size() != 0) {
			map.put("list", listInfo);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 查询路段设备详情--情报板设备
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectInformationBoardInfo", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "路段详情", methods = "查询路段设备详情")
	public void selectInformationBoardInfo(Equipments equipments, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<Equipments> equipmentsInfo = equipmentsLogic.selectInfoQuipments(equipments, index, size);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (equipmentsInfo != null) {
			map.put("list", equipmentsInfo);
			map.put("total", equipmentsInfo.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 查询路段设备详情--非情报板设备
	 * @param equipments
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectNoBoardInfo", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "设备控制", methods = "查询路段下的设备")
	public void selectNoBoardInfo(Equipments equipments, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<Equipments> equipmentsInfo = equipmentsLogic.selectEqInfoBySectionCode(equipments, index, size);
		String returnData = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (equipmentsInfo != null) {
			map.put("list", equipmentsInfo);
			map.put("toal", equipmentsInfo.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 查询路段下的设备分类 selectEquimentClassifyInfo
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "selectEquimentClassifyInfo", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "查询路段下的设备分类")
	public void selectEquimentClassifyInfo(Section section, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		Equipments model = new Equipments();
		model.setSectionCode(section.getCode());
		List<Equipments> sectionEquipments = equipmentsLogic.selectEquListBySectionCode(model);
		if (JudgeNullUtil.iList(sectionEquipments)) {
			map.put("list", sectionEquipments);
		}
		List<Equipments> cateEquipments = equipmentsLogic.selectEquimentClassifyInfo(model);
		for (Equipments equipment : cateEquipments) {
			int disConnCSum = 0;
			int faultSum = 0;
			int startSum = 0;
			if (equipment.getCurrentState() == CommonUtils.EQU_STATE_NORMAL) {
				startSum += 1;// 3正常
			} else if (equipment.getCurrentState() == CommonUtils.EQU_STATE_OFFLINE) {
				disConnCSum += 1;// 0离线未连接数
			} else if (equipment.getCurrentState() == CommonUtils.EQU_STATE_FAULT) {
				faultSum += 1;// 2故障数
			}
			equipment.setEquStartSum(startSum);
			equipment.setEquDisConnectSum(disConnCSum);
			equipment.setEquFaultSum(faultSum);
			equipment.setEquSum(cateEquipments.size());
		}
		map.put("equimentsClassify", cateEquipments);
		String returnData = "";
		if (JudgeNullUtil.iList(sectionEquipments)) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 保存路段平码图配置 saveSectionPlanCongig
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/saveConfig", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "/路段管理", methods = "保存路段平面配置")
	public void saveSectionPlanCongig(SectionPlanConfig section, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		int result = sectionLogic.saveSectionPlanConfig(section);
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 获取路段平面配置列表 selectSectionPlanConfig
	 * @param page
	 * @param error
	 * @param sectionPlan
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "查询路段配置信息")
	public void selectSectionPlanConfig(@Valid PageModel page, BindingResult error, SectionPlanConfig sectionPlan,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		Page<SectionPlanConfig> list = sectionLogic.selectSectionPlanConfig(page, sectionPlan);
		if (list != null) {
			map.put("total", list.getTotal());
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 隧道保存绿化带信息
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "路段配置管理", methods = "保存隧道绿化带信息")
	public void insertSectionConfig(SectionPlanConfig section, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		int result = sectionLogic.insertSectionConfigHD(section);
		if (result == CommonUtils.STATUS_NORMAL) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 查询隧道绿化带信息
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/select/config", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "路段管理", methods = "查询隧道绿化带信息")
	public void selectSectionPlanConfigs(SectionPlanConfig section, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<SectionPlanConfig> list = sectionLogic.selectSectionConfig(section);
		if (list.size() > 0) {
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * 删除平面图配置
	 * @param section
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/config/delete", method = RequestMethod.POST)
	@SystemLog(module = "路段平面图管理", methods = "删除苦短平面图配置")
	public void deleteSectionConfig(SectionPlanConfig section, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = sectionLogic.deleteSectionConfig(section);
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
