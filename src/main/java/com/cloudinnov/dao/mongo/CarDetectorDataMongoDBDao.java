package com.cloudinnov.dao.mongo;

import org.springframework.stereotype.Repository;

import com.cloudinnov.model.CarDetectorData;

@Repository("carDetectorDataMongoDBDao")
public class CarDetectorDataMongoDBDao extends BaseMongoDAOImpl<CarDetectorData> {
	
}
