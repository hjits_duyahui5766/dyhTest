package com.cloudinnov.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PredictionRuleTable {

	private Integer id;
	private String accidentType; //事故类型
	private String accidentArea; //事故区域
	private String categoryCode; //设备类型
	private String sectionCode; //隧道编码
	private String areaId; //设备所在区域标识
	private String config; //控制设备的动作
	private Integer isRecover; //触发/恢复标识:0,触发;1,恢复
	
	private String configs; //控制设备的动作
	private List<Equipments> categoryConfig;
	private String categoryConfigs;
	private String predictionName;
	private String lane;

	public String getLane() {
		return lane;
	}
	public void setLane(String lane) {
		this.lane = lane;
	}
	public String getPredictionName() {
		return predictionName;
	}
	public void setPredictionName(String predictionName) {
		this.predictionName = predictionName;
	}
	public String getCategoryConfigs() {
		return categoryConfigs;
	}
	public void setCategoryConfigs(String categoryConfigs) {
		this.categoryConfigs = categoryConfigs;
	}
	public List<Equipments> getCategoryConfig() {
		return categoryConfig;
	}
	public void setCategoryConfig(List<Equipments> categoryConfig) {
		this.categoryConfig = categoryConfig;
	}
	public String getConfigs() {
		return configs;
	}
	public void setConfigs(String configs) {
		this.configs = configs;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public String getAccidentArea() {
		return accidentArea;
	}
	public void setAccidentArea(String accidentArea) {
		this.accidentArea = accidentArea;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public Integer getIsRecover() {
		return isRecover;
	}
	public void setIsRecover(Integer isRecover) {
		this.isRecover = isRecover;
	}
	
}
