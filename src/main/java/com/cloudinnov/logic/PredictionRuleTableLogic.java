package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.model.PredictionRuleTable;
import com.github.pagehelper.Page;

import scala.collection.generic.BitOperations.Int;

public interface PredictionRuleTableLogic extends IBaseLogic<PredictionRuleTable> {

	List<PredictionRuleTable> selectPredictions(PredictionRuleTable predictionRuleTable);

	Map<String, Object> achieveAndSendRecoverPredictionRules(PredictionRuleTable predictionRuleTable);

	List<String> getAccidentTypes();

	Map<String, Object> manTrigGetPredictionRules(String message);

	// 添加火灾预案紧急联系人
	int insertContacters(PredictionContacter predictionContacter);

	int updateContacters(PredictionContacter predictionContacter);

	int deleteContacters(PredictionContacter predictionContacter);

	// 查询火灾预案中的联系人
	Page<PredictionContacter> selectPredictionContacter(PredictionContacter predictionContacter, int index, int size);

	//添加触发预案方法
	int insertPredictionRule(PredictionRuleTable predictionRuleTable);
	
	//添加恢复预案方法
	int insertRecoverPredictionRule(PredictionRuleTable predictionRuleTable);
	
	//查询预案的方法
	Page<PredictionRuleTable> selectPredictionRule(PredictionRuleTable predictionRuleTable, int index, int size);
	
	//查询单条预案的方法
	PredictionRuleTable selectSinglePredictionRule(PredictionRuleTable predictionRuleTable);
	
	//更新触发预案的方法
	int updatePredictionRule(PredictionRuleTable predictionRuleTable);
	
	//更新触发预案的方法
	int updateRecoverPredictionRule(PredictionRuleTable predictionRuleTable);
	
	//删除预案方法
	int deletePredictionRule(PredictionRuleTable predictionRuleTable);
}
