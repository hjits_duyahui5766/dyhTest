package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AuthRightLogic;
import com.cloudinnov.model.AuthRight;
import com.cloudinnov.utils.CommonUtils;

/** 
 * @ClassName: AuthRightController 
 * @Description: 操作权限管理
 * @author: ningmeng
 * @date: 2016年11月14日 下午12:29:29  
 */
@Controller
@RequestMapping("/webapi/right")
public class AuthRightController extends BaseController {
	
	@Autowired
	private AuthRightLogic authRightLogic;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public void getList(AuthRight authRight, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		authRight.setCompanyCode(getUserInfo(request).get(COM_CODE));
		authRight.setPortal(CommonUtils.PORTAL_ADMIN_TYPE);
        List<AuthRight> list = authRightLogic.selectList(authRight, false);
        if(list !=null){
        	map.put("list", list);
        }
        String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
        response.getWriter().println(returnData);
    }
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "操作权限管理", methods = "操作权限信息保存")
    public void save(AuthRight authRight, HttpServletRequest request, HttpServletResponse response) throws IOException {

		authRight.setCompanyCode(getUserInfo(request).get(COM_CODE));
		authRight.setPortal(CommonUtils.PORTAL_ADMIN_TYPE);
        int result = authRightLogic.save(authRight);
        Map<String, Object> map = new HashMap<String, Object>();
        String returnData = returnJsonAllRequest(request, response, map, result, "");
        response.getWriter().println(returnData);
    }
	
	/**
	 * resourcesDelete
	 * @Description: 根据code删除一条资源记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "操作权限管理", methods = "信息删除")
	public void resourcesDelete(AuthRight authRight, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authRightLogic.delete(authRight);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resourcesUpdate
	 * @Description: 更新资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "操作权限管理", methods = "信息修改")
	public void resourcesUpdate(AuthRight authRight, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authRightLogic.update(authRight);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	
	/**
	 * resSaveOtherLanguage
	 * @Description: 资源增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "操作权限管理", methods = "修改其他语言")
	public void resSaveOtherLanguage(AuthRight authRight, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authRight.setLanguage(authRight.getOtherLanguage());
		int returnCode = authRightLogic.saveOtherLanguage(authRight);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "操作权限管理", methods = "修改其他语言")
	public void resUpdateOtherLanguage(AuthRight authRight, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authRight.setLanguage(authRight.getOtherLanguage());
		int returnCode = authRightLogic.updateOtherLanguage(authRight);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
}
