package com.cloudinnov.controller.api;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.sql.visitor.functions.If;
import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.dao.EquipmentsDao;
import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.LightDetectLogic;
import com.cloudinnov.logic.impl.CarDetectorLogicImpl;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.ExportData;
import com.cloudinnov.model.LightDetect;
import com.cloudinnov.task.SchduleScanJob;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.POIUtils;
import com.cloudinnov.utils.TcpClientUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Controller
@RequestMapping("/light")
public class LightDetectController extends BaseController {

	public static final String LightRedisKeyHead = "light:deviceId:";
	public static final String LightRedisKeyFoot = ":value";
	private static final String REALTIME_SPLITTER_LEVEL1 = ",";
	private static final int REALTIME_TIME_LEVEL1 = 0;
	private static final int REALTIME_TIME_OUT_SECOND = 60;
	private static final int REALTIME_TIME_OUT_MILL = 1000;

	@Autowired
	LightDetectLogic lightDetectLogic;
	@Autowired
	private JedisPool jedisPool;
	private Jedis redis;
	@Autowired
	ControlSolutionLogic controlSolutionLogin;
	@Autowired
	EquipmentsDao equipmentsDao;

	@RequestMapping("/test")
	@ResponseBody
	public String test() {
		System.err.println(11111);
		// List<ControlSolutionExpression> selectScheduleExpression =
		// controlSolutionLogin.selectScheduleExpression();
		SchduleScanJob a = new SchduleScanJob();
		a.execute();
		// return "aaa"+selectScheduleExpression.get(0).getSolutionCode();
		return null;
	}

	@RequestMapping("/lightInfo")
	@ResponseBody
	public String getLightDetectLoginInfo() {
		List<LightDetect> lightDetectIpInfo = lightDetectLogic.lightDetectIpInfo();
		String value = lightDetectIpInfo.get(0).getValue();
		return value;
	}

	@RequestMapping("/lightByCode")
	@ResponseBody
	public void getLightValue(HttpServletRequest request, HttpServletResponse response, String equipCode)
			throws JsonProcessingException {
		String returnData = "";
		boolean pingResult = false;
		try {
			redis = jedisPool.getResource();
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
			// 先通过equipCode的值查询数据库,获取该设备的ip和port
			List<EquipmentsAttr> EquipmentsAttrs = lightDetectLogic.selectLightByEquipCode(equipCode);
			String ip = "";
			for (EquipmentsAttr equipmentsAttr : EquipmentsAttrs) {
				if ("ip".equals(equipmentsAttr.getCustomTag())) {
					ip = equipmentsAttr.getValue();
				}
			}
			// 调用TcpClientUtil的ping(ip,port)方法,判断是否ping通,是:3;否:0
			pingResult = TcpClientUtil.ping(ip);
			// 查询redis中是否有数据
			String realTimeDataKey = LightRedisKeyHead + equipCode + LightRedisKeyFoot;
			String lightValue = redis.lindex(realTimeDataKey, 0);
			if (pingResult) {
				if (lightValue != null) {
					String[] split2 = lightValue.split(",");
					Map<String, Object> map = new HashMap<>();
					map.put("currentState", 3);
					map.put("value", split2[1]);
					returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
					response.getWriter().println(returnData);
				} else {
					Map<String, Object> map = new HashMap<>();
					map.put("currentState", 0);
					map.put("value", "0");
					returnData = returnJsonAllRequest(request, response, map, ERROR, "");
					response.getWriter().println(returnData);
				}
			} else {
				Map<String, Object> map = new HashMap<>();
				map.put("currentState", 0);
				map.put("value", "0");
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
				response.getWriter().println(returnData);
			}

			// Map<String, Object>map = new HashMap<>();
			// map.put("currentState", 3);
			// map.put("value", "10");
			// returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			// response.getWriter().println(returnData);
		} catch (JsonProcessingException e) {
			Map<String, Object> map = new HashMap<>();
			map.put("currentState", 0);
			map.put("value", "0");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		} catch (IOException e) {
			Map<String, Object> map = new HashMap<>();
			map.put("currentState", 0);
			map.put("value", "0");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		} catch (Exception e) {
			Map<String, Object> map = new HashMap<>();
			map.put("currentState", 0);
			map.put("value", "0");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		} finally {
			jedisPool.returnResource(redis);
		}
	}

	@RequestMapping("/equipEnvirData")
	@ResponseBody
	public void getEnvironmentDataByEquipCode(HttpServletRequest request, HttpServletResponse response,
			String equipCode) throws IOException {

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		Map<String, Object> environmentDataByEquipCode = lightDetectLogic.getEnvironmentDataByEquipCode(equipCode);
		String returnData = "";
		if (environmentDataByEquipCode != null) {
			returnData = returnJsonAllRequest(request, response, environmentDataByEquipCode, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, null, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping("/envirInfo")
	@ResponseBody
	public void getEnvironmentInfo(HttpServletRequest request, HttpServletResponse response, String categoryCode)
			throws IOException {

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		redis = jedisPool.getResource();
		Map<String, Object> map = null;
		List<Map<String, Object>> data = new ArrayList<>();
		try {
			List<Equipments> equipmentInfoByCategoryInfo = lightDetectLogic
					.getEquipmentInfoByCategoryInfo(categoryCode);
			for (Equipments equipment : equipmentInfoByCategoryInfo) {
				map = new HashMap<>();
				map.put("equipCode", equipment.getEquipmentCode());
				map.put("equipName", equipment.getName());
				map.put("pileNo", equipment.getPileNo());
				map.put("sectionName", equipment.getSectionName());
				map.put("direction", equipment.getDirection());
				String realTimeDataKey = LightRedisKeyHead + equipment.getEquipmentCode() + LightRedisKeyFoot;
				String value = redis.lindex(realTimeDataKey, 0);
				if (value != null) {
					value = value.replace(" ", "");
					String[] valueArray = value.split(REALTIME_SPLITTER_LEVEL1);
					/**
					 * 判断实时redis中的第一条数据时间和当前时间相差是否大于60s
					 */
					long timeout = 0L;
					if (valueArray[0].length() > 11) {
						timeout = (System.currentTimeMillis() - Long.parseLong(valueArray[0])) / REALTIME_TIME_OUT_MILL;
					} else {
						timeout = ((System.currentTimeMillis() / 1000) - Long.parseLong(valueArray[0]));
					}
					if (timeout < REALTIME_TIME_OUT_SECOND) {
						map.put("currentValue", valueArray[1]);
						map.put("currentState", 3);

					} else {
						map.put("currentValue", 0);
						map.put("currentState", 0);
					}
				} else {
					map.put("currentValue", 0);
					map.put("currentState", 0);
				}
				data.add(map);
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			jedisPool.returnResourceObject(redis);
		}
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put("data", data);
		returnData = returnJsonAllRequest(request, response, returnMap, SUCCESS, "");
		response.getWriter().println(returnData);

	}

	@RequestMapping(value = "/reportLight", method = RequestMethod.GET)
	public void reportLight(HttpServletRequest request, HttpServletResponse response, String equipCode,
			String startTime, String endTime) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipCode);
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		Map<String, Object> month = lightDetectLogic.selectLightReportDatafromMongoDB(params, "month");
		Map<String, Object> year = lightDetectLogic.selectLightReportDatafromMongoDB(params, "year");
		Map<String, Object> day = lightDetectLogic.selectLightReportDatafromMongoDB(params, "day");
		Map<String, Object> hour = lightDetectLogic.selectLightReportDatafromMongoDB(params, "hour");
		Map<String, Object> map = new HashMap<>();
		map.put("month", month);
		map.put("year", year);
		map.put("day", day);
		map.put("hour", hour);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/reportNo", method = RequestMethod.GET)
	public void reportNo2(HttpServletRequest request, HttpServletResponse response, String equipCode, String startTime,
			String endTime) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipCode);
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);

		Map<String, Object> month = lightDetectLogic.selectNoReportDatafromMongoDB(params, "month");
		Map<String, Object> year = lightDetectLogic.selectNoReportDatafromMongoDB(params, "year");
		Map<String, Object> day = lightDetectLogic.selectNoReportDatafromMongoDB(params, "day");
		Map<String, Object> hour = lightDetectLogic.selectNoReportDatafromMongoDB(params, "hour");
		Map<String, Object> map = new HashMap<>();
		map.put("month", month);
		map.put("year", year);
		map.put("day", day);
		map.put("hour", hour);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/reportCovi", method = RequestMethod.GET)
	public void reportCovi(HttpServletRequest request, HttpServletResponse response, String equipCode, String startTime,
			String endTime) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, String> params = new HashMap<String, String>();
		Map<String, Object> map = new HashMap<>();
		if (equipCode != null) {
			params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipCode);
			params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
			params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);

			Map<String, Object> month = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "month");
			Map<String, Object> year = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "year");
			Map<String, Object> day = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "day");
			Map<String, Object> hour = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "hour");

			map.put("month", month);
			map.put("year", year);
			map.put("day", day);
			map.put("hour", hour);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			map.put("object", "");
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	// 风速、风向； 光强
	@RequestMapping(value = "/reportFsfx", method = RequestMethod.GET)
	public void reportFsfx(HttpServletRequest request, HttpServletResponse response, String equipmentCode,
			String startTime, String endTime) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipmentCode);
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);

		Map<String, Object> month = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "month");
		Map<String, Object> year = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "year");
		Map<String, Object> day = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "day");
		Map<String, Object> hour = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "hour");
		Map<String, Object> map = new HashMap<>();
		map.put("month", month);
		map.put("year", year);
		map.put("day", day);
		map.put("hour", hour);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/reportIntensity", method = RequestMethod.GET)
	public void reportIntensity(HttpServletRequest request, HttpServletResponse response, String equipCode,
			String startTime, String endTime) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		String returnData = "";
		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipCode);
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);

		Map<String, Object> month = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "month");
		Map<String, Object> year = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "year");
		Map<String, Object> day = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "day");
		Map<String, Object> hour = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "hour");
		Map<String, Object> map = new HashMap<>();
		map.put("month", month);
		map.put("year", year);
		map.put("day", day);
		map.put("hour", hour);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/export", method = RequestMethod.GET)
	@SystemLog(module = "能见检测器", methods = "导出能见度检测器数据")
	public void export(HttpServletRequest request, HttpServletResponse response, String equipCode, String sectionName,
			String pileNo, String startTime, String endTime, String equipmentName) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, equipCode);
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		Map<String, Object> map = lightDetectLogic.selectLightReportDatafromMongoDB(params, "hour");
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(sectionName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(pileNo);
		ExportData exportData = new ExportData();
		Map<String, Object> models = (Map<String, Object>) map.get("model");
		List<Map<String, Object>> carData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> carDatas = new ArrayList<Map<String, Object>>();

		carDatas = (List<Map<String, Object>>) models.get("hour");
		carData = getRankMongoData(carDatas);
		
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : carData) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		List<Object> values = new ArrayList<Object>();
		for (Map<String, Object> map2 : carData) {
			Object value = map2.get("value");
			values.add(value);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(values)) {
			POIUtils.exportLightDataToExcel("能见度检测器数据", eqList, timesTreand, times, values, response);
		}
	}

	@RequestMapping(value = "/exportLight", method = RequestMethod.GET)
	@SystemLog(module = "能见检测器", methods = "导出能见度检测器数据")
	public void exportLight(HttpServletRequest request, HttpServletResponse response, Equipments model,
			String startTime, String endTime, String equipmentName, int type) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, model.getEquipmentCode());
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(model.getSectionName().getBytes("iso-8859-1"), "utf-8"));
		eqList.add(model.getPileNo());
		ExportData exportData = new ExportData();
		List<Map<String, Object>> lightData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> lightDatas = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		if (type == 1) {// 年
			Map<String, Object> year = lightDetectLogic.selectLightReportDatafromMongoDB(params, "year");
			Map<String, Object> models = (Map<String, Object>) year.get("model");
			lightDatas = (List<Map<String, Object>>) models.get("year");
			lightData = getRankMongoData(lightDatas);
		} else if (type == 2) {// 月
			Map<String, Object> month = lightDetectLogic.selectLightReportDatafromMongoDB(params, "month");
			Map<String, Object> models = (Map<String, Object>) month.get("model");
			lightDatas = (List<Map<String, Object>>) models.get("month");
			lightData = getRankMongoData(lightDatas);
		} else if (type == 3) {// 日
			Map<String, Object> day = lightDetectLogic.selectLightReportDatafromMongoDB(params, "day");
			Map<String, Object> models = (Map<String, Object>) day.get("model");
			lightDatas = (List<Map<String, Object>>) models.get("day");
			lightData = getRankMongoData(lightDatas);
		} else if (type == 4) {// 时
			Map<String, Object> hour = lightDetectLogic.selectLightReportDatafromMongoDB(params, "hour");
			Map<String, Object> models = (Map<String, Object>) hour.get("model");
			lightDatas = (List<Map<String, Object>>) models.get("hour");
			lightData = getRankMongoData(lightDatas);
		}
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : lightData) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		List<Object> values = new ArrayList<Object>();
		for (Map<String, Object> map2 : lightData) {
			Object value = map2.get("value");
			values.add(value);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(values)) {
			POIUtils.exportLightDataToExcel("能见度检测器数据", eqList, timesTreand, times, values, response);
		}
	}

	@RequestMapping(value = "/exportNo", method = RequestMethod.GET)
	@SystemLog(module = "No2管理", methods = "导出No2数据")
	public void exportNo(HttpServletRequest request, HttpServletResponse response, Equipments model, String startTime,
			String endTime, String equipmentName, int type) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, model.getEquipmentCode());
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(model.getSectionName().getBytes("iso-8859-1"), "utf-8"));
		eqList.add(model.getPileNo());
		ExportData exportData = new ExportData();
		List<Map<String, Object>> no2Data = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> no2Datas = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		if (type == 1) {// 年
			Map<String, Object> year = lightDetectLogic.selectNoReportDatafromMongoDB(params, "year");
			Map<String, Object> models = (Map<String, Object>) year.get("model");
			no2Datas = (List<Map<String, Object>>) models.get("year");
			no2Data = getRankMongoData(no2Datas);
		} else if (type == 2) {// 月
			Map<String, Object> month = lightDetectLogic.selectNoReportDatafromMongoDB(params, "month");
			Map<String, Object> models = (Map<String, Object>) month.get("model");
			no2Datas = (List<Map<String, Object>>) models.get("month");
			no2Data = getRankMongoData(no2Datas);
		} else if (type == 3) {// 日
			Map<String, Object> day = lightDetectLogic.selectNoReportDatafromMongoDB(params, "day");
			Map<String, Object> models = (Map<String, Object>) day.get("model");
			no2Datas = (List<Map<String, Object>>) models.get("day");
			no2Data = getRankMongoData(no2Datas);
		} else if (type == 4) {// 时
			Map<String, Object> hour = lightDetectLogic.selectNoReportDatafromMongoDB(params, "hour");
			Map<String, Object> models = (Map<String, Object>) hour.get("model");
			no2Datas = (List<Map<String, Object>>) models.get("hour");
			no2Data = getRankMongoData(no2Datas);
		}
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : no2Data) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		List<Object> values = new ArrayList<Object>();
		for (Map<String, Object> map2 : no2Data) {
			Object value = map2.get("No2Value");
			values.add(value);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(values)) {
			POIUtils.exportNo2Excel("No2检测器数据", eqList, timesTreand, times, values, response);
		}
	}

	@RequestMapping(value = "/exportCovi", method = RequestMethod.GET)
	@SystemLog(module = "Covi管理", methods = "导出Covi数据")
	public void exportCovi(HttpServletRequest request, HttpServletResponse response, Equipments model, String startTime,
			String endTime, String equipmentName, int type) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, model.getEquipmentCode());
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(model.getSectionName().getBytes("iso-8859-1"), "utf-8"));
		eqList.add(model.getPileNo());
		ExportData exportData = new ExportData();
		List<Map<String, Object>> coviDatas = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> coviData = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		if (type == 1) {// 年
			Map<String, Object> year = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "year");
			Map<String, Object> models = (Map<String, Object>) year.get("model");
			coviDatas = (List<Map<String, Object>>) models.get("year");
			coviData = getRankMongoData(coviDatas);
		} else if (type == 2) {// 月
			Map<String, Object> month = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "month");
			Map<String, Object> models = (Map<String, Object>) month.get("model");
			coviDatas = (List<Map<String, Object>>) models.get("month");
			coviData = getRankMongoData(coviDatas);
		} else if (type == 3) {// 日
			Map<String, Object> day = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "day");
			Map<String, Object> models = (Map<String, Object>) day.get("model");
			coviDatas = (List<Map<String, Object>>) models.get("day");
			coviData = getRankMongoData(coviDatas);
		} else if (type == 4) {// 时
			Map<String, Object> hour = lightDetectLogic.selectCOVIReportDatafromMongoDB(params, "hour");
			Map<String, Object> models = (Map<String, Object>) hour.get("model");
			coviDatas = (List<Map<String, Object>>) models.get("hour");
			coviData = getRankMongoData(coviDatas);
		}
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : coviData) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(coviData)) {
			POIUtils.exportCoviExcel("CoVi检测器数据", eqList, timesTreand, times, coviData, response);
		}
	}

	@RequestMapping(value = "/exportFsfx", method = RequestMethod.GET)
	@SystemLog(module = "Fsfx管理", methods = "导出Fsfx数据")
	public void exportFsfx(HttpServletRequest request, HttpServletResponse response, Equipments model, String startTime,
			String endTime, String equipmentName, int type) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

		// 修改字符编码
		String sectionName = request.getParameter("sectionName");
		String name = new String(sectionName.getBytes("iso-8859-1"), "utf-8");
		model.setSectionName(name);

		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, model.getEquipmentCode());
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(model.getSectionName().getBytes("iso-8859-1"), "utf-8"));
		eqList.add(model.getPileNo());
		ExportData exportData = new ExportData();
		List<Map<String, Object>> fsfxData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> fsfxDatas = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		if (type == 1) {// 年
			Map<String, Object> year = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "year");
			Map<String, Object> models = (Map<String, Object>) year.get("model");
			fsfxDatas = (List<Map<String, Object>>) models.get("year");
			fsfxData = getRankMongoData(fsfxDatas);
		} else if (type == 2) {// 月
			Map<String, Object> month = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "month");
			Map<String, Object> models = (Map<String, Object>) month.get("model");
			fsfxDatas = (List<Map<String, Object>>) models.get("month");
			fsfxData = getRankMongoData(fsfxDatas);
		} else if (type == 3) {// 日
			Map<String, Object> day = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "day");
			Map<String, Object> models = (Map<String, Object>) day.get("model");
			fsfxDatas = (List<Map<String, Object>>) models.get("day");
			fsfxData = getRankMongoData(fsfxDatas);
		} else if (type == 4) {// 时
			Map<String, Object> hour = lightDetectLogic.selectFsfxReportDatafromMongoDB(params, "hour");
			Map<String, Object> models = (Map<String, Object>) hour.get("model");
			fsfxDatas = (List<Map<String, Object>>) models.get("hour");
			fsfxData = getRankMongoData(fsfxDatas);
		}
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : fsfxData) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		List<Object> values = new ArrayList<Object>();
		for (Map<String, Object> map2 : fsfxData) {
			Object value = map2.get("fsValue");
			values.add(value);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(values)) {
			POIUtils.exportFsfxExcel("风速、风向检测器数据", eqList, timesTreand, times, values, response);
		}
	}

	@RequestMapping(value = "/exportIntensity", method = RequestMethod.GET)
	@SystemLog(module = "Intensity管理", methods = "导出Intensity数据")
	public void exportIntensity(HttpServletRequest request, HttpServletResponse response, Equipments model,
			String startTime, String endTime, String equipmentName, int type) throws IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

		Map<String, String> params = new HashMap<String, String>();
		params.put(CarDetectorLogicImpl.KEY_EQUCODE, model.getEquipmentCode());
		params.put(CarDetectorLogicImpl.KEY_START_TIME, startTime);
		params.put(CarDetectorLogicImpl.KEY_END_TIME, endTime);
		List<String> eqList = new ArrayList<String>();
		eqList.add(new String(equipmentName.getBytes("iso-8859-1"), "utf-8"));
		eqList.add(new String(model.getSectionName().getBytes("iso-8859-1"), "utf-8"));
		eqList.add(model.getPileNo());
		ExportData exportData = new ExportData();
		List<Map<String, Object>> intensityData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> intensityDatas = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		if (type == 1) {// 年
			Map<String, Object> year = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "year");
			Map<String, Object> models = (Map<String, Object>) year.get("model");
			intensityDatas = (List<Map<String, Object>>) models.get("year");
			intensityData = getRankMongoData(intensityDatas);
		} else if (type == 2) {// 月
			Map<String, Object> month = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "month");
			Map<String, Object> models = (Map<String, Object>) month.get("model");
			intensityDatas = (List<Map<String, Object>>) models.get("month");
			intensityData = getRankMongoData(intensityDatas);
		} else if (type == 3) {// 日
			Map<String, Object> day = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "day");
			Map<String, Object> models = (Map<String, Object>) day.get("model");
			intensityDatas = (List<Map<String, Object>>) models.get("day");
			intensityData = getRankMongoData(intensityDatas);
		} else if (type == 4) {// 时
			Map<String, Object> hour = lightDetectLogic.selectIntensityReportDatafromMongoDB(params, "hour");
			Map<String, Object> models = (Map<String, Object>) hour.get("model");
			intensityDatas = (List<Map<String, Object>>) models.get("hour");
			intensityData = getRankMongoData(intensityDatas);
		}
		List<String> times = new ArrayList<String>();
		for (Map<String, Object> map2 : intensityData) {
			String time = (String) map2.get("time");
			times.add(time);
		}
		String timesTreand = null;
		if (JudgeNullUtil.iList(times)) {
			if (times.size() == 1) {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(0);
			} else {
				timesTreand = "时间范围：" + times.get(0) + "到" + times.get(times.size() - 1);
			}
			exportData.setTime(times);
		}
		List<Object> values = new ArrayList<Object>();
		for (Map<String, Object> map2 : intensityData) {
			Object value = map2.get("GqValue");
			values.add(value);
		}
		if (JudgeNullUtil.iList(eqList) && JudgeNullUtil.iList(times) && JudgeNullUtil.iList(values)) {
			POIUtils.exportIntensityExcel("光强检测器数据", eqList, timesTreand, times, values, response);
		}
	}

	/**
	 * 把mongo的数据排序后返回
	 * @param mongoData 数据来源
	 * @return
	 */
	public List<Map<String, Object>> getRankMongoData(List<Map<String, Object>> mongoData){
		Collections.sort(mongoData, new Comparator<Map<String, Object>>() {
			public int compare(Map<String, Object> mongoData1, Map<String, Object> mongoData2) {
				String date1 = "";
				String date2 = "";
				date1 = mongoData1.get("time").toString();
				date2 = mongoData2.get("time").toString();
				return date1.compareTo(date2);
			}
		});
		return mongoData;
	}
}
