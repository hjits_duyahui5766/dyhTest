package com.cloudinnov.model;

import java.util.List;

public class ControlSolutionExpre {
	
	private Integer id;

	private String code;

	private String name;
	
	private String pointCode;
	
	private String condition;
	
	private String solutionCode;
	
	private Integer status;

	private String comment;
	
	private String createTime;
	
	private String lastUpdateTime;

	private Integer isRecover;
	
	private String pointLocationName;//关联到point_info表
	
	private Integer level;//告警级别
	
	private String pointType;//point点位数据类型:数字sz和模拟mn两类
	
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public String getPointType() {
		return pointType;
	}

	public void setPointType(String pointType) {
		this.pointType = pointType;
	}

	public String getPointLocationName() {
		return pointLocationName;
	}

	public void setPointLocationName(String pointLocationName) {
		this.pointLocationName = pointLocationName;
	}

	public Integer getIsRecover() {
		return isRecover;
	}

	public void setIsRecover(Integer isRecover) {
		this.isRecover = isRecover;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPointCode() {
		return pointCode;
	}

	public void setPointCode(String pointCode) {
		this.pointCode = pointCode;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}


	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getSolutionCode() {
		return solutionCode;
	}

	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}

 
}
