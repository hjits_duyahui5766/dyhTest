package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.SectionPlanConfig;

public interface SectionPlanConfigDao extends IBaseDao<SectionPlanConfig> {
	int delete(Integer id);
	List<SectionPlanConfig> selectSectionConfig(SectionPlanConfig section);
	/**
	 * 根据路段编码查询横道门
	 * @param sectionCode
	 * @return
	 */
	List<SectionPlanConfig> selectSectionConfig(String sectionCode);
	/**
	 * 删除路段平面图配置
	 * @param section
	 * @return
	 */
	int deleteSectionConfig(SectionPlanConfig section);
}
