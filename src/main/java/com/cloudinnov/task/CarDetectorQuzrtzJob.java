package com.cloudinnov.task;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.utils.support.spring.SpringUtils;

public class CarDetectorQuzrtzJob {
	
	private static final Logger logger = LoggerFactory.getLogger(AlarmRuleQuartzJob.class);
	
	private CarDetectorLogic carDetectorLogic = SpringUtils.getBean("carDetectorLogic");

	/**
	 * 车辆汇总推送
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param 参数
	 * @return void 返回类型
	 * @throws ParseException
	 */
	protected void execute() {
		
		carDetectorLogic.carTotalToWebsocket();
		
		logger.debug("[车辆汇总推送任务执行完毕]");
	}
}
