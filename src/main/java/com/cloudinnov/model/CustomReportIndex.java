package com.cloudinnov.model;

public class CustomReportIndex extends BaseModel{
	

    private String reportCode;

    private String indexCode;

    private Integer valueType;
	
	private Integer isUserCustomerReport;
	
    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

	public Integer getValueType() {
		return valueType;
	}

	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	public Integer getIsUserCustomerReport() {
		return isUserCustomerReport;
	}

	public void setIsUserCustomerReport(Integer isUserCustomerReport) {
		this.isUserCustomerReport = isUserCustomerReport;
	}
    
}