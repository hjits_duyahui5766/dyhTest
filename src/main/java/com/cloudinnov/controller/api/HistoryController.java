package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.HistoryDataLogic;

/**
 * @author chengning
 * @date 2016年3月15日上午10:38:54
 * @email ningcheng@cloudinnov.com
 * @remark 查询历史数据
 * @version
 */
@RequestMapping("/webapi/history")
@Controller
@Scope("prototype")
public class HistoryController extends BaseController {
	@Autowired
	private HistoryDataLogic historyDataLogic;

	@RequestMapping(value = "/list")
	@ResponseBody
	public void selectHistory(String language, String codes, String startTime, String endTime,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		param.put("language", language);
		List<List<String>> list = historyDataLogic.selectHistoryDatas(codes, startTime, endTime, param);
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/listByTSDB")
	@ResponseBody
	public void selectHistoryByTSDB(String language, String codes, String startTime, String endTime,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		param.put("language", language);
		List<List<String>> list = historyDataLogic.selectHistoryDatasByMongoDB(codes, startTime, endTime, param);
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
}
