package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.CapacityStat;

public interface CapacityStatDao extends IBaseDao<CapacityStat>{
	
	/** 工单详情统计 条件为equCode  lineCode  customerCode，默认可以不传
	* selectFaultStatsByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsDetailByCondition(Map<String, Object> map);
	
	/** 按小时统计  条件为lineCode  customerCode 默认可以不传 day必须传
	* selectFaultStatsDayByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	
	public List<CapacityStat> selectCapacityStatsHourByCondition(Map<String, Object> map);
	
	/** 按天统计  条件为equCode  lineCode  customerCode 默认可以不传 day必须传
	* selectFaultStatsDayByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsDayByCondition(Map<String, Object> map);
	/** 按周统计  条件为equCode  lineCode  customerCode 默认可以不传 startYear,endYear,startWeek,endWeek必须传
	* selectFaultStatsWeekByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsWeekByCondition(Map<String, Object> map);
	/** 按月统计  条件为equCode  lineCode  customerCode 默认可以不传 startYear,endYear,startMonth,endMonth必须传
	* selectFaultStatsMonthByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsMonthByCondition(Map<String, Object> map);
	/** 按年统计  条件为equCode  lineCode  customerCode 默认可以不传 
	* selectFaultStatsYearByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param map
	* @param @return    参数
	* @return List<FaultStat>    返回类型 
	*/
	public List<CapacityStat> selectCapacityStatsYearByCondition(Map<String, Object> map);
}