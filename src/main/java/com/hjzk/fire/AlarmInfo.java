package com.hjzk.fire;

import java.util.ArrayList;
import java.util.List;

public class AlarmInfo {
	
	private boolean isAlarm; //是否有警报
	public ArrayList<Integer> oneCodeList = new ArrayList<Integer>(); //存储报警位置的一次码
	
	public boolean isAlarm() {
		return isAlarm;
	}
	
	public void setAlarm(boolean isAlarm) {
		this.isAlarm = isAlarm;
	}

	public ArrayList<Integer> getOneCodeList() {
		return oneCodeList;
	}
	public void setOneCodeList(ArrayList<Integer> oneCodeList) {
		this.oneCodeList = oneCodeList;
	}
	
 
}
