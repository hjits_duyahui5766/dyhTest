package com.cloudinnov.model;

public class SectionPlanConfig extends BaseModel {
	private String sectionCode;
	private Integer basemap;
	private String location;
	private String top;
	private String left;
	private String config;

	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public Integer getBasemap() {
		return basemap;
	}
	public void setBasemap(Integer basemap) {
		this.basemap = basemap;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
