package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthUserDao;
import com.cloudinnov.dao.PreventionWorkOrderDao;
import com.cloudinnov.logic.PreventionWorkOrderLogic;
import com.cloudinnov.model.AuthUsers;
import com.cloudinnov.model.PreventionWorkOrder;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

@Service("PreventionWorkOrderLogic")
public class PreventionWorkOrderLogicImpl extends BaseLogicImpl<PreventionWorkOrder> implements PreventionWorkOrderLogic {
	
	@Autowired
	private PreventionWorkOrderDao preventionWorkOrderDao;
	
	@Autowired
	private AuthUserDao authUserDao;
	@Override
	public Page<PreventionWorkOrder> search(int index, int size, PreventionWorkOrder preventionWorkOrder,
			String key) {
		PageHelper.startPage(index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		if(preventionWorkOrder.getCustomerCode()!=null && !preventionWorkOrder.getCustomerCode().equals(""))
		{
			map.put("customerCode", preventionWorkOrder.getCustomerCode());
			map.put("customerCodes", preventionWorkOrder.getCustomerCode().split(","));
		}
		if(preventionWorkOrder.getProLineCode()!=null && !preventionWorkOrder.getProLineCode().equals(""))
		{
			map.put("proLineCode", preventionWorkOrder.getProLineCode());
			map.put("proLineCodes", preventionWorkOrder.getProLineCode().split(","));
		}
		if(preventionWorkOrder.getEquipmentCode()!=null && !preventionWorkOrder.getEquipmentCode().equals(""))
		{
			map.put("equipmentCode", preventionWorkOrder.getEquipmentCode());
			map.put("equipmentCodes", preventionWorkOrder.getEquipmentCode().split(","));
		}
		
		map.put("keyword", key);
		map.put("orderstatus", preventionWorkOrder.getOrderStatus());
		map.put("language", preventionWorkOrder.getLanguage());
		Page<PreventionWorkOrder> list = (Page<PreventionWorkOrder>)  preventionWorkOrderDao.search(map);
		
		return list;
	}
	
	public PreventionWorkOrder select(PreventionWorkOrder preventionWorkOrder){
		
		PreventionWorkOrder model = preventionWorkOrderDao.selectEntityByCondition(preventionWorkOrder);
		if (model != null) {
			if (model.getEquipment().getOems() != null) {
				List<AuthUsers> users = new ArrayList<AuthUsers>();
				String[] userCode = model.getEquipment().getOems().split(",");
				for (int i = 0; i < userCode.length; i++) {
					AuthUsers user = new AuthUsers();
					user.setCode(userCode[i]);
					user.setLanguage(preventionWorkOrder.getLanguage());
					users.add(authUserDao.selectEntityByCondition(user));
				}
				model.getEquipment().setOemPersons(users);
			}
			if (model.getEquipment().getCustomers()!= null) {
				List<AuthUsers> users = new ArrayList<AuthUsers>();
				String[] userCode = model.getEquipment().getCustomers().split(",");
				for (int i = 0; i < userCode.length; i++) {
					AuthUsers user = new AuthUsers();
					user.setCode(userCode[i]);
					user.setLanguage(preventionWorkOrder.getLanguage());
					users.add(authUserDao.selectEntityByCondition(user));
				}
				model.getEquipment().setCustomerPersons(users);
			}
		}
		return model;
		
	}
}
