package com.cloudinnov.model;

/**
 * 预案执行记录详情
 * @author duyah
 *
 */
public class PredictionRuleLogDetails {
	
	private String id;
	private String predictionLogCode; //预案执行记录的code
	private String executionTime;	//记录时间
	private String executionContent;	//记录详情
	private String pictureUrl;		//记录截图的url
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPredictionLogCode() {
		return predictionLogCode;
	}
	public void setPredictionLogCode(String predictionLogCode) {
		this.predictionLogCode = predictionLogCode;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	public String getExecutionContent() {
		return executionContent;
	}
	public void setExecutionContent(String executionContent) {
		this.executionContent = executionContent;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
}
