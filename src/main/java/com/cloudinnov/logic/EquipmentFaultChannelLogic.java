package com.cloudinnov.logic;

import com.cloudinnov.model.EquipmentFaultChannel;

/**
 * @author guochao
 * @date 2016年2月17日下午4:59:07
 * @email chaoguo@cloudinnov.com
 * @remark 设备故障通道serivce接口
 * @version
 */
public interface EquipmentFaultChannelLogic extends
		IBaseLogic<EquipmentFaultChannel> {
	public int saveOtherLanguage(EquipmentFaultChannel equipmentFaultChannel);
	
	public int updateOtherLanguage(EquipmentFaultChannel equipmentFaultChannel);

}
