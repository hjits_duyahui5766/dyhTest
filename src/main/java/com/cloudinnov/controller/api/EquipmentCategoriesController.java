package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentsCategoriesAttrLogic;
import com.cloudinnov.logic.EquipmentsCategoriesLogic;
import com.cloudinnov.model.EquipmentsCategories;
import com.cloudinnov.model.EquipmentsCategoryAttr;
import com.cloudinnov.model.TreeObject;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.TreeUtil;
import com.github.pagehelper.Page;

/**
 * @author nilixin
 * @date 2016年2月24日上午9:29:13
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/equcategory")
public class EquipmentCategoriesController extends BaseController {

	@Autowired
	private EquipmentsCategoriesLogic equipmentCategoriesLogic;
	
	@Autowired
	private EquipmentsCategoriesAttrLogic equipmentsCategoriesAttrLogic;

	/**
	 * save
	 * 
	 * @Description: 保存设备分类
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save")
	@ResponseBody
	@SystemLog(module = "设备分类", methods = "设备分类信息保存")
	public void save(EquipmentsCategories equipmentCategories, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		equipmentCategories.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = equipmentCategoriesLogic.save(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * saveOtherLanguage
	 * 
	 * @Description: 添加设备分类其他语言信息
	 * @param @param
	 *            equipmentCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "设备分类", methods = "添加其他语言")
	public void saveOtherLanguage(EquipmentsCategories equipmentCategories, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = null;
		equipmentCategories.setLanguage(equipmentCategories.getOtherLanguage());
		int result = equipmentCategoriesLogic.saveOtherLanguage(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备分类", methods = "修改其他语言")
	public void updateOtherLanguage(EquipmentsCategories equipmentCategories, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentCategories.setLanguage(equipmentCategories.getOtherLanguage());
		int result = equipmentCategoriesLogic.updateOtherLanguage(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据category的code 删除设备
	 * @param @param
	 *            equipmentCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemLog(module = "设备分类", methods = "设备分类删除")
	public void delete(EquipmentsCategories equCate, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		int result = equipmentCategoriesLogic.delete(equCate);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 更新设备分类
	 * @param @param
	 *            equipmentCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "设备分类", methods = "信息修改")
	public void update(EquipmentsCategories equipmentCategories, HttpServletRequest request,
			HttpServletResponse response, Model model)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		int result = equipmentCategoriesLogic.update(equipmentCategories);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 分页查找所有设备分类
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentsCategories ec, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		ec.setOemCode(getUserInfo(request).get(OEM_CODE));
		Page<EquipmentsCategories> equipmentCategory = equipmentCategoriesLogic.selectListPage(index, size, ec, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentCategory != null) {
			map.put("list", equipmentCategory);
			map.put("total", equipmentCategory.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找设备
	 * @param @param
	 *            equipmentBomCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentsCategories equipmentsCategories, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		if (equipmentsCategories.getOtherLanguage() != null && equipmentsCategories.getOtherLanguage() != "") {
			equipmentsCategories.setLanguage(equipmentsCategories.getOtherLanguage());
		}
		EquipmentsCategories result = equipmentCategoriesLogic.select(equipmentsCategories);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("model", result);
		if (result != null) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(EquipmentsCategories ec, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		ec.setOemCode(getUserInfo(request).get(OEM_CODE));
		List<EquipmentsCategories> equipmentCategory = equipmentCategoriesLogic.selectList(ec, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentCategory != null) {
			map.put("list", equipmentCategory);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

	/**
	 * addSelectList
	 * 
	 * @Description: 获取分类select数据 by guochao
	 * @param @param
	 *            equipmentsCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/addSelectList")
	@ResponseBody
	public void addSelectList(EquipmentsCategories equipmentsCategories, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(CommonUtils.isNotEmpty(equipmentsCategories.getCode())){
			EquipmentsCategoryAttr equipmentsCategoryAttr = new EquipmentsCategoryAttr();
			equipmentsCategoryAttr.setCategoryCode(equipmentsCategories.getCode());
			List<EquipmentsCategoryAttr> result = equipmentsCategoriesAttrLogic
					.selectByCategoryCode(equipmentsCategoryAttr);
			map.put("poropertyList", result);
		}
		String oemCode = (String) request.getAttribute("oemCode");
		equipmentsCategories.setOemCode(oemCode);
		List<EquipmentsCategories> result = equipmentCategoriesLogic.addSelectList(equipmentsCategories);
		String returnData = "";
		
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * treeList
	 * 
	 * @Description: 分类列表 by guochao
	 * @param @param
	 *            equipmentsCategories
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/treeList")
	@ResponseBody
	public void treeList(EquipmentsCategories equipmentsCategories, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentsCategories.setOemCode(getUserInfo(request).get(OEM_CODE));
		List<EquipmentsCategories> result = equipmentCategoriesLogic.addSelectList(equipmentsCategories);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setCode(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setText(result.get(i).getName());
				ts.setIndexId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setComment(result.get(i).getComment());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/tableList")
	@ResponseBody
	public void tableList(String[] codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String oemCode = (String) request.getAttribute("oemCode");
		List<EquipmentsCategories> result = equipmentCategoriesLogic.tableList(codes, language, oemCode);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
