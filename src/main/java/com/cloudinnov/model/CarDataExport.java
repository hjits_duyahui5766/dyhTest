package com.cloudinnov.model;

import java.util.List;

import org.wuwz.poi.ExportConfig;

/**
 * @author libo
 * @email boli@cloudinnov.com
 * @日期 2017年8月15日 下午4:16:30
 */
public class CarDataExport {
	/* 设备名称 */
	@ExportConfig(value = "设备名称", width = 100)
	private String equipmenCode;
	/* 所属路段 */
	@ExportConfig(value = "所属路段", width = 100)
	private String sectionName;
	/* 桩号 */
	@ExportConfig(value = "桩号", width = 100)
	private String pileNo;
	private List<ExportData> list;

	
	public List<ExportData> getList() {
		return list;
	}
	public void setList(List<ExportData> list) {
		this.list = list;
	}
	public String getEquipmenCode() {
		return equipmenCode;
	}
	public void setEquipmenCode(String equipmenCode) {
		this.equipmenCode = equipmenCode;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getPileNo() {
		return pileNo;
	}
	public void setPileNo(String pileNo) {
		this.pileNo = pileNo;
	}
}
