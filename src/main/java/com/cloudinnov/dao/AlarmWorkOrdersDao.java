package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.AlarmWorkOrders;
import com.cloudinnov.model.Companies;

/**
 * @author chengning
 * @date 2016年2月17日上午11:47:25
 * @email ningcheng@cloudinnov.com
 * @remark 工单管理
 * @version
 */

public interface AlarmWorkOrdersDao extends IBaseDao<AlarmWorkOrders> {

	public int updateCountByCondition(AlarmWorkOrders alarmWorkOrders);

	public int insertInfo(AlarmWorkOrders alarmWorkOrders);

	/**
	 * selectWorkOrdersTotalByCustomerId
	 * 
	 * @Description: 根据客户id查询该客户下的工单总数
	 * @param @param
	 *            customerId
	 * @param @return
	 *            参数
	 * @return int 返回类型
	 */
	public int selectWorkOrdersTotalByCustomerCode(String comCode);

	public List<AlarmWorkOrders> search(Map<String, Object> map);

	/**
	 * 查询代办工单，orderStatus为不等于条件
	 * 
	 * @Title: selectAgencyWorkOrderList
	 * @Description: TODO
	 * @param map
	 * @return
	 * @return: List<AlarmWorkOrders>
	 */
	public List<AlarmWorkOrders> selectAgencyWorkOrderList(Map<String, Object> map);

	public List<AlarmWorkOrders> selectOrderStatusByCondition(AlarmWorkOrders alarmWorkOrders);

	public int selectUndoneWorkOrdersTotalByOemCode(Companies company);

	/**
	 * selectFaultInfoByChannelAndFaultCode
	 * 
	 * @Description: 根据设备故障通道，故障码查询故障码翻译信息
	 * @param @param
	 *            channel
	 * @param @param
	 *            faultCode
	 * @param @param
	 *            language
	 * @param @return
	 *            参数
	 * @return Faults 返回类型
	 */
	public List<AlarmWorkOrders> selectFaultInfoByChannelAndFaultCode(String channel, String faultCode);

	/**
	 * 查询我的工单列表 selectAlarmWorksByUser
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            alarmWorkOrders
	 * @param @return
	 *            参数
	 * @return List<AlarmWorkOrders> 返回类型
	 */
	public List<AlarmWorkOrders> selectAlarmWorksByUser(AlarmWorkOrders alarmWorkOrders);

	/**
	 * 获取一段时间设备故障占比
	 * 
	 * @Title: selectEquipmentFaultProportion
	 * @Description: TODO
	 * @param map
	 *            startTime endTime oemCode
	 * @return 返回一段时间内设备故障数量
	 * @return: int
	 */
	public List<AlarmWorkOrders> selectEquipmentFaultProportion(Map<String, Object> map);

	public List<AlarmWorkOrders> searchAlarmAndHelp(Map<String, Object> map);

	/**
	 * 根据路段code获取工单数量
	 * 
	 * @param sectionCode
	 * @return
	 */
	public int selectWorkOrdersTotalBySectionCode(String sectionCode);

	public List<AlarmWorkOrders> selectUntreatedWorkOrder(@Param("categoryCode")String categoryCode, @Param("orderStatus")Integer orderStatus);
}
