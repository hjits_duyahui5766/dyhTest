package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.CustomReportTimeLogic;
import com.cloudinnov.model.CustomReportTime;

@Service
public class CustomReportTimeLogicImpl extends BaseLogicImpl<CustomReportTime> implements CustomReportTimeLogic {

}
