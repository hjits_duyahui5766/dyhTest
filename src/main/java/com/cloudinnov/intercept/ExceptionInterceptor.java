package com.cloudinnov.intercept;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import com.cloudinnov.exception.ParameterException;
import com.cloudinnov.exception.SystemException;
import com.cloudinnov.utils.CommonUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author guochao
 * @date 2016年2月26日下午5:55:51
 * @email chaoguo@cloudinnov.com
 * @remark 异常拦截器
 * @version
 */
public class ExceptionInterceptor implements HandlerExceptionResolver {

	private final Logger logger = LoggerFactory.getLogger(ExceptionInterceptor.class);

	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		Map<String, Object> model = new HashMap<String, Object>();
		// 是否异步请求
		if (request.getHeader("accept") != null && !(request.getHeader("accept").indexOf("application/json") > -1
				|| (request.getHeader("X-Requested-With") != null
						&& request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
			// 根据不同错误转向不同页面
			if (ex instanceof SystemException) {
				model.put("code", CommonUtils.SYSTEM_EXCEPTION_CODE);
				model.put("msg", CommonUtils.SYSTEM_EXCEPTION_MSG);
				logger.error("同步请求业务异常返回 1001 异常 " , ex);
			} else if (ex instanceof ParameterException) {
				model.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
				model.put("msg", CommonUtils.PARAMETER_EXCEPTION_MSG);
				logger.error("同步请求参数异常返回 1002, 异常 " , ex);
			} else {
				model.put("code", CommonUtils.OTHER_EXCEPTION_CODE);
				model.put("msg", CommonUtils.OTHER_EXCEPTION_MSG);
				logger.error("同步请求其他异常返回 1000,异常 " , ex);
			}
			ObjectMapper mapper = new ObjectMapper();
			PrintWriter writer;
			try {
				writer = response.getWriter();
				writer.write(mapper.writeValueAsString(model));
				writer.flush();
			} catch (IOException e) {
				model.put("code", CommonUtils.OTHER_EXCEPTION_CODE);
				model.put("msg", CommonUtils.OTHER_EXCEPTION_MSG);
				logger.error("同步请求异常返回页面WEB-INF/error");
				return new ModelAndView("WEB-INF/error", model);
			}
			return new ModelAndView();
		} else {
			try {
				if (ex instanceof SystemException) {
					model.put("code", CommonUtils.SYSTEM_EXCEPTION_CODE);
					model.put("msg", CommonUtils.SYSTEM_EXCEPTION_MSG);
					logger.error("同步请求业务异常返回 1001 异常 " , ex);
				} else if (ex instanceof ParameterException) {
					model.put("code", CommonUtils.PARAMETER_EXCEPTION_CODE);
					model.put("msg", CommonUtils.PARAMETER_EXCEPTION_MSG);
					logger.error("同步请求参数异常返回 1002, 异常 " , ex);
				} else {
					model.put("code", CommonUtils.OTHER_EXCEPTION_CODE);
					model.put("msg", CommonUtils.OTHER_EXCEPTION_MSG);
					logger.error("同步请求其他异常返回 1000,异常 " , ex);
				}
				ObjectMapper mapper = new ObjectMapper();
				PrintWriter writer;
				writer = response.getWriter();
				writer.write(mapper.writeValueAsString(model));
				writer.flush();
			} catch (IOException e) {
				model.put("code", CommonUtils.OTHER_EXCEPTION_CODE);
				model.put("msg", CommonUtils.OTHER_EXCEPTION_MSG);
				logger.error("异步请求异常返回页面WEB-INF/error");
				return new ModelAndView("WEB-INF/error", model);
			}
			return new ModelAndView();
		}
	}
}
