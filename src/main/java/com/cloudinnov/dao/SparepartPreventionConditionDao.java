package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.SparepartPreventionCondition;

public interface SparepartPreventionConditionDao extends IBaseDao<SparepartPreventionCondition> {
    int deleteByConfigId(Integer id);

    int insert(SparepartPreventionCondition record);//

    int insertSelective(SparepartPreventionCondition record);

    SparepartPreventionCondition selectByPrimaryKey(Integer id);

    int updateByCondition(SparepartPreventionCondition record);

    int updateByPrimaryKeyWithBLOBs(SparepartPreventionCondition record);

    int updateByPrimaryKey(SparepartPreventionCondition record);
    
    List<SparepartPreventionCondition> selectListByConfigId(Integer configId);
    
}