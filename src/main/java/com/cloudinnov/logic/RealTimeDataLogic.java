package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.RealTimeData;
import com.cloudinnov.model.RealTimeObject;
import com.cloudinnov.model.RealtimeList;

/**
 * @author chengning
 * @date 2016年2月23日下午2:29:02奥ba
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface RealTimeDataLogic extends IBaseLogic<RealTimeData> {
	/**
	 * insertRealtimeList
	 * @Description: 批量数据插入
	 * @param @param map
	 * @param @return 参数
	 * @return int 返回类型
	 */
	int insertRealtimeList(List<RealTimeData> list, String tableName);
	/**
	 * selectRealtimeDatas
	 * @Description: 根据多个code批量获取实时数据
	 * @param @param codes
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型 <code,value>
	 */
	List<RealTimeObject> selectRealtimeDatasByCodes(String[] codes, String language);
	/**
	 * selectRealtimeDatas
	 * @Description: 根据多个code批量获取实时数据
	 * @param @param codes
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型 <code,value>
	 */
	List<EquipmentPoints> selectRealtimeDatasByEquipment(EquipmentPoints equipmentPoint);
	/**
	 * 查询设备下的指标类型和值
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param equipmentPoint
	 * @return 参数
	 * @return List<EquipmentPoints> 返回类型
	 */
	Map<String, Object> selectRealtimeIndexTypeDataByEquipment(Equipments model);
	/**
	 * 查询路段下所有设备下的指标类型和值
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param sectionCode
	 * @return 参数
	 * @return List<Equipments> 返回类型
	 */
	List<Equipments> selectRealtimeIndexTypeDataBySectionCode(String sectionCode);
	/**
	 * selectRealtimeData
	 * @Description: 根据code批量实时数据
	 * @param @param code
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型 <code,value>
	 */
	Map<String, Object> selectRealtimeData(String code);
	/**
	 * 根据点位code获取点位名称 selectPointCodeName
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param map
	 * @param @return 参数
	 * @return Map<String,String> 返回类型
	 */
	Map<String, EquipmentPoints> selectPointCodeName(Map<String, Object> map);
	/**
	 * selectRealtimeData
	 * @Description: 根据单个code获取实时数据
	 * @param @param code
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型 <code,value>
	 */
	List<RealtimeList> selectRealtimeDataByPointCode(String code, int type);
	/**
	 * 根据指标类型获取累加值或者瞬时值
	 * @Title: selectRealtimeDataByIndexType
	 * @Description: TODO
	 * @param indexTypeCode
	 * @param valueType 1 累加值 2 瞬时值 type 1 地区 2 行业 3 客户 4 设备
	 * @return
	 * @return: String
	 */
	double selectPieReportTotalDataByCondition(Map<String, Object> map);
	Map<String, Object> selectReportDataByCondition(Map<String, Object> map);
	/**
	 * selectRealtimeDatas
	 * @Description: 根据多个code批量获取实时数据
	 * @param @param codes
	 * @param @return 参数
	 * @return Map<String,Object> 返回类型 <code,value>
	 */
	List<RealTimeObject> selectRealtimeDatasMobile(EquipmentPoints equipmentPoint);
}
