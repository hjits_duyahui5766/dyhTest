package com.cloudinnov.dao;

import com.cloudinnov.model.FaultWorkOrderWithBLOBs;

public interface FaultWorkOrderDao extends IBaseDao<FaultWorkOrderWithBLOBs>{
    
}