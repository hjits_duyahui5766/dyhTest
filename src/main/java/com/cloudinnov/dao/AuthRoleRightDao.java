package com.cloudinnov.dao;

import java.util.Map;

import com.cloudinnov.model.AuthRoleRight;

public interface AuthRoleRightDao extends IBaseDao<AuthRoleRight>{
	
	public int insertList(Map<String,Object> map);
}