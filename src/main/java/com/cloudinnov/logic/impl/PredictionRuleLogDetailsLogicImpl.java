package com.cloudinnov.logic.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.PredictionRuleLogDao;
import com.cloudinnov.dao.PredictionRuleLogDetailsDao;
import com.cloudinnov.dao.PredictionRuleTableDao;
import com.cloudinnov.dao.SectionDao;
import com.cloudinnov.logic.PredictionRuleLogDetailsLogic;
import com.cloudinnov.logic.PredictionRuleLogLogic;
import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleLogDetails;
import com.cloudinnov.model.Section;

@Service("predictionRuleLogDetailsLogic")
public class PredictionRuleLogDetailsLogicImpl extends BaseLogicImpl<PredictionRuleLogDetails> implements PredictionRuleLogDetailsLogic{

	@Autowired
	private PredictionRuleLogDetailsDao predictionRuleLogDetailsDao;
	@Autowired
	private PredictionRuleLogDao predictionRuleLogDao;
	@Autowired
	private PredictionRuleTableDao predictionRuleTableDao;
	
	@Override
	public int insertPredictionRuleLogDetails(PredictionRuleLogDetails PredictionRuleLogDetails) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String executionTime = sdf.format(new Date());
		PredictionRuleLogDetails.setExecutionTime(executionTime);
		return predictionRuleLogDetailsDao.insertPredictionRuleLogDetails(PredictionRuleLogDetails);
	}

	@Override
	public PredictionRuleLog selectPredictionRuleLogDetails(PredictionRuleLog predictionRuleLog) {
		//查询预案执行记录表
		PredictionRuleLog log = predictionRuleLogDao.selectPredictionRuleLogByCode(predictionRuleLog);
		//查询原执行记录详情
		List<PredictionRuleLogDetails> predictionRuleLogDetails = predictionRuleLogDetailsDao.selectPredictionRuleLogDetails(predictionRuleLog);
		log.setPredictionRuleLogDetails(predictionRuleLogDetails);
		
		//查询紧急联系人列表
		PredictionContacter predictionContacter = new PredictionContacter();
		if (log.getAccidentType().contains("火灾")) {
			predictionContacter.setType(1);
		}else if (log.getAccidentType().contains("交通事故")) {
			predictionContacter.setType(2);
		}else if (log.getAccidentType().contains("交通拥堵")) {
			predictionContacter.setType(3);
		}
		List<PredictionContacter> predictionContacters = predictionRuleTableDao.selectPredictionContacter(predictionContacter);
		log.setPredictionContacters(predictionContacters);
		
		return log;
	}
	
}
