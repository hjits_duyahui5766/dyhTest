package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleLogDetails;

public interface PredictionRuleLogDetailsDao extends IBaseDao<PredictionRuleLogDetails>{

	int insertPredictionRuleLogDetails(PredictionRuleLogDetails predictionRuleLogDetails);
	
	List<PredictionRuleLogDetails> selectPredictionRuleLogDetails(PredictionRuleLog predictionRuleLog);
	
	int deletePredictionRuleLogDetails(PredictionRuleLogDetails predictionRuleLogDetails);
}
