package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.RealTimeDataLogic;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.RealTimeObject;
import com.cloudinnov.model.RealtimeList;

/**
 * @author chengning
 * @date 2016年2月26日下午2:32:50
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/realtime")
public class RealtimeController extends BaseController {

	@Autowired
	private RealTimeDataLogic realTimeDataLogic;

	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(String language, String codes,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String[] pointCodes = codes.split(",");
		List<RealTimeObject> list = realTimeDataLogic.selectRealtimeDatasByCodes(pointCodes,language);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
			response.getWriter().print(returnData);
		}else{
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, ERROR,
					"");
			response.getWriter().print(returnData);
		}
	}
	
	@RequestMapping(value = "/m/getRealtimeByObjectCode")
	@ResponseBody
	public void selectRealtimeDatasMobile(EquipmentPoints equipmentPoints,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<RealTimeObject> list = realTimeDataLogic.selectRealtimeDatasMobile(equipmentPoints);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
			response.getWriter().print(returnData);
		}else{
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, ERROR,
					"");
			response.getWriter().print(returnData);
		}
	}
	
	@RequestMapping(value = "/equipmentrealdata")
	@ResponseBody
	public void equipmentRealData(EquipmentPoints equipmentPoint,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<EquipmentPoints> list = realTimeDataLogic.selectRealtimeDatasByEquipment(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
			response.getWriter().print(returnData);
		}else{
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, ERROR,
					"");
			response.getWriter().print(returnData);
		}
	}
	
	@RequestMapping(value = "/getValueByPointCode")
	@ResponseBody
	public void selectValueByPointCode(String language, String code, int type,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<RealtimeList> list = realTimeDataLogic.selectRealtimeDataByPointCode(code,type);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
			response.getWriter().print(returnData);
		}else{
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, ERROR,"");
			response.getWriter().print(returnData);
		}
	}
}
