package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cloudinnov.dao.SparepartPreventionConditionDao;
import com.cloudinnov.dao.SparepartPreventionConfigDao;
import com.cloudinnov.logic.SparepartPreventionConfigLogic;
import com.cloudinnov.model.SparepartPreventionCondition;
import com.cloudinnov.model.SparepartPreventionConfig;
import com.cloudinnov.utils.CommonUtils;

@Service
public class SparepartPreventionConfigLogicImpl extends BaseLogicImpl<SparepartPreventionConfig> implements SparepartPreventionConfigLogic {

	@Autowired
	private SparepartPreventionConfigDao sparepartPreventionConfigDao;
	@Autowired
	private SparepartPreventionConditionDao sparepartPreventionConditionDao;
	
	/*public int save(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition){
		sparepartPreventionConfig.setStatus(new Integer(1));
		String conditionPoint = sparepartPreventionConfig.getConditionPoint();
		if(conditionPoint!=null && conditionPoint.length()>0){
			conditionPoint = conditionPoint.substring(0,conditionPoint.length()-1);
		}
		
		int result = sparepartPreventionConfigDao.insert(sparepartPreventionConfig);
		int configId = sparepartPreventionConfig.getId();
		if(result == 1){
			if(sparepartPreventionCondition.getAlarmConditionItemListLength() == 0){
				return 1;
			}
			
			String[] formulas = sparepartPreventionCondition.getFormula().substring(0, sparepartPreventionCondition.getFormula().length()-1).split("&");
			String[] conditionWarningValues = sparepartPreventionCondition.getStringConditionWarningValue().substring(0, sparepartPreventionCondition.getStringConditionWarningValue().length()-1).split("&");
			String[] conditionWarningContents = sparepartPreventionCondition.getConditionWarningContent().substring(0, sparepartPreventionCondition.getConditionWarningContent().length()-1).split("&");
			if(formulas.length == conditionWarningValues.length && formulas.length == conditionWarningContents.length ){
				for(int i = 0 ; i < formulas.length ; i++){
					SparepartPreventionCondition spc =  new SparepartPreventionCondition();
					spc.setConfigId(configId);
					spc.setFormula(formulas[i]);
					spc.setConditionWarningValue(Integer.parseInt(conditionWarningValues[i]));
					spc.setConditionWarningContent(conditionWarningContents[i]);
					spc.setStatus(new Integer(1));
					int conditionResult = sparepartPreventionConditionDao.insert(spc);
					if(conditionResult != 1){
						return 0 ;
					}
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		return 1;
	}
	*/
	
	public int save(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition){
		String items = sparepartPreventionConfig.getItems();
		String conditionPoint = null;
		if(CommonUtils.isNotEmpty(items)){
			JSONArray arrayItems = JSON.parseArray(items);
			for(int i=0; i<arrayItems.size(); i++){
				JSONObject item = arrayItems.getJSONObject(i);
				JSONObject alarmCondition = item.getJSONObject("alarmCondition");
				String conditionCode = item.getString("conditionCode");
				conditionPoint += conditionCode + CommonUtils.SPLIT_VERTICAL_LINE +alarmCondition.getString("name") + CommonUtils.SPLIT_VERTICAL_LINE +alarmCondition.getString("unit") + CommonUtils.SPLIT_SEMICOLON;
			}
		}
		sparepartPreventionConfig.setConditionCode(conditionPoint);
		int result = sparepartPreventionConfigDao.insert(sparepartPreventionConfig);
		int configId = sparepartPreventionConfig.getId();
		if(result == CommonUtils.SUCCESS_NUM){
			JSONArray arrayItems = JSON.parseArray(items);
			SparepartPreventionCondition spc = null;
			for(int i=0; i<arrayItems.size(); i++){
				spc = new SparepartPreventionCondition();
				JSONObject item = arrayItems.getJSONObject(i);
				JSONObject alarmCondition = item.getJSONObject("alarmCondition");
				Integer alarmValue = item.getInteger("alarmValue");
				String alarmContent = item.getString("alarmContent");
				spc.setConfigId(configId);
				spc.setFormula(alarmCondition.getString("code")+ CommonUtils.SPLIT_COMMA + alarmCondition.getString("name")+ CommonUtils.SPLIT_COMMA + alarmCondition.getString("formulaOperator") +CommonUtils.SPLIT_COMMA + alarmCondition.getString("formulaValue"));
				spc.setConditionWarningValue(alarmValue);
				spc.setConditionWarningContent(alarmContent);
				spc.setStatus(new Integer(1));
				int conditionResult = sparepartPreventionConditionDao.insert(spc);
				if(conditionResult != 1){
					return CommonUtils.DEFAULT_NUM ;
				}
			}
		}else{
			return CommonUtils.DEFAULT_NUM ;
		}
		return CommonUtils.SUCCESS_NUM ;
	}

	@Override
	public SparepartPreventionConfig selectBySparepartCode(SparepartPreventionConfig sparepartPreventionConfig) {
		return sparepartPreventionConfigDao.selectBySparepartCode(sparepartPreventionConfig);
	}

	@Override
	public int update(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition) {

		String conditionPoint = sparepartPreventionConfig.getConditionPoint();
		if(conditionPoint!=null && conditionPoint.length()>0){
			conditionPoint = conditionPoint.substring(0,conditionPoint.length()-1);
		}

		sparepartPreventionConfigDao.updateByCondition(sparepartPreventionConfig);
		sparepartPreventionConditionDao.deleteByConfigId(sparepartPreventionConfig.getId());
			if(sparepartPreventionCondition.getAlarmConditionItemListLength() == 0){
				return 1;
			}
			
			String[] formulas = sparepartPreventionCondition.getFormula().substring(0, sparepartPreventionCondition.getFormula().length()-1).split("&");
			String[] conditionWarningValues = sparepartPreventionCondition.getStringConditionWarningValue().substring(0, sparepartPreventionCondition.getStringConditionWarningValue().length()-1).split("&");
			String[] conditionWarningContents = sparepartPreventionCondition.getConditionWarningContent().substring(0, sparepartPreventionCondition.getConditionWarningContent().length()-1).split("&");
			if(formulas.length == conditionWarningValues.length && formulas.length == conditionWarningContents.length ){
				for(int i = 0 ; i < formulas.length ; i++){
					SparepartPreventionCondition spc =  new SparepartPreventionCondition();
					spc.setConfigId(sparepartPreventionConfig.getId());
					spc.setFormula(formulas[i]);
					spc.setConditionWarningValue(Integer.parseInt(conditionWarningValues[i]));
					spc.setConditionWarningContent(conditionWarningContents[i]);
					spc.setStatus(new Integer(1));
					int conditionResult = sparepartPreventionConditionDao.insert(spc);
					if(conditionResult != 1){
						return 0 ;
					}
				}
			}else{
				return 0;
			}
		
		return 1;	
		}

	
}
