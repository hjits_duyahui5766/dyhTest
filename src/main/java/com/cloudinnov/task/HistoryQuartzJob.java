package com.cloudinnov.task;

import java.text.ParseException;
import java.util.List;

import org.quartz.SchedulerException;

import com.cloudinnov.logic.HistoryDataLogic;
import com.cloudinnov.utils.support.spring.SpringUtils;

/**
 * @author chengning
 * @date 2016年8月4日上午12:40:18
 * @email
 * @remark 把redis中按照每小时的key值（点位/历史数据）放入数据库。
 * @version
 */
public class HistoryQuartzJob {
	private HistoryDataLogic historyDataLogic = SpringUtils.getBean("historyDataLogic");

	/**
	 * 任务调度 去消费历史数据并且插入数据库 execute
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param 参数
	 * @return void 返回类型
	 * @throws ParseException
	 * @throws SchedulerException
	 */
	protected void execute() throws ParseException, SchedulerException {
		historyDataLogic.saveDataToMongoDBByRedis();
	}
	@SuppressWarnings("unused")
	private static String compressed(List<String> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i != list.size() - 1)
				sb.append(";");
		}
		return sb.toString();
	}
}
