package com.cloudinnov.model;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Equipments extends BaseModel {
	private static final long serialVersionUID = 1L;
	
	private String customerCode;// 客户code
	private String proLineCode;// 产线code
	private String name;
	private String pileNo;// 桩号
	private String serialNumber;
	private String categoryCode;// 分类code
	private String categoryName;// 分类名称
	private String brand;// 品牌
	private String model;
	private String placeOfOrigin;
	private String imageUrl;
	private String oemName;
	private String productionDate;
	private String supplierCode;
	private String supplierName;
	private String purchaseDate;
	private String guaranteeBeginDate;
	private Integer warrantyPeriod;
	private String oemPerson;
	private String supplierPerson;
	private String customerPerson;
	private String oemPersonName;
	private String customerName;
	private String customerPhone;
	private List<AuthUsers> oemPersons;
	private List<AuthUsers> customerPersons;
	private List<EquipmentPoints> points;
	private String oems;
	private String suppliers;
	private String customers;
	private String qrUrl;
	private String modelName;
	private String agentName;
	private String attrInfo;// 设备附加属性
	private String equipmentCode;// 设备code
	private String equipmentName;// 设备名称
	private String customTag;// 自定义标签
	private String attrName;// 属性名称
	private String value;// 属性值
	private String sectionCode;// 路段code
	private String type;// 路段类型
	private String sectionName;
	private Map<String, Object> configs;
	private List<EquipmentsAttr> equipmentsAttr;
	private Map<String, Object> indexType;
	@JsonIgnore
	@NotNull(message = "count参数必填")
	@Min(value = 1, message = "复制个数最小值为1")
	private Integer count;
	private Integer equStartSum = 0;
	private Integer equSum = 0;
	private Integer equFaultSum = 0;
	private Integer equDisConnectSum = 0;
	private Map<String, Object> map;
	private int parentId;
	private Integer grade;
	private String locations;
	private String queryKey;
	private String direction;// 设备所属路段方向
	private String nodeClass;// 节点层级
	private Integer ratedCurrent;//额定电流
	private String ammeterType;	//电表类型
	private String parentCode;	//电表的父级标识
	
	private List<Equipments> equipmensLeft;
	private List<Equipments> equipmensRight;
	
	//新添加字段
	private String areaId;
	private Integer HdCount;
	private List<ControlSolutionConfig.Config> config;
	private String command;
	private String accidentType;
	private String pileRange;  //桩号范围
	private Map<String, Object> boardConfig;  //情报板设备的群控详细信息
	private String solutionCode; //情报板设备匹配的群控code标识
	private List<String> lanes;  //车道指示器设备分类的所有车道数
	private String lane;	//选中车道指示器的所在车道数
	private String alias;	//设备的别名
	
	public List<String> getLanes() {
		return lanes;
	}
	public void setLanes(List<String> lanes) {
		this.lanes = lanes;
	}
	public String getLane() {
		return lane;
	}
	public void setLane(String lane) {
		this.lane = lane;
	}
	public String getSolutionCode() {
		return solutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}
	public Map<String, Object> getBoardConfig() {
		return boardConfig;
	}
	public void setBoardConfig(Map<String, Object> boardConfig) {
		this.boardConfig = boardConfig;
	}
	public String getPileRange() {
		return pileRange;
	}
	public void setPileRange(String pileRange) {
		this.pileRange = pileRange;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public List<Equipments> getEquipmensLeft() {
		return equipmensLeft;
	}
	public void setEquipmensLeft(List<Equipments> equipmensLeft) {
		this.equipmensLeft = equipmensLeft;
	}
	public List<Equipments> getEquipmensRight() {
		return equipmensRight;
	}
	public void setEquipmensRight(List<Equipments> equipmensRight) {
		this.equipmensRight = equipmensRight;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public Integer getHdCount() {
		return HdCount;
	}
	public void setHdCount(Integer hdCount) {
		HdCount = hdCount;
	}
	public List<ControlSolutionConfig.Config> getConfig() {
		return config;
	}
	public void setConfig(List<ControlSolutionConfig.Config> config) {
		this.config = config;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public Integer getRatedCurrent() {
		return ratedCurrent;
	}
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	public void setRatedCurrent(Integer ratedCurrent) {
		this.ratedCurrent = ratedCurrent;
	}
	public String getAmmeterType() {
		return ammeterType;
	}
	public void setAmmeterType(String ammeterType) {
		this.ammeterType = ammeterType;
	}
	public String getNodeClass() {
		return nodeClass;
	}
	public void setNodeClass(String nodeClass) {
		this.nodeClass = nodeClass;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getLocations() {
		return locations;
	}
	public void setLocations(String locations) {
		this.locations = locations;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getOemPersonName() {
		return oemPersonName;
	}
	public void setOemPersonName(String oemPersonName) {
		this.oemPersonName = oemPersonName;
	}
	public String getSupplierPersonName() {
		return supplierPersonName;
	}
	public void setSupplierPersonName(String supplierPersonName) {
		this.supplierPersonName = supplierPersonName;
	}
	public String getCustomerPersonName() {
		return customerPersonName;
	}
	public void setCustomerPersonName(String customerPersonName) {
		this.customerPersonName = customerPersonName;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	private String supplierPersonName;
	private String customerPersonName;
	private String country;
	private String province;
	private String city;
	private String location;
	private String comment;
	private String lineName;
	private Integer currentState = 0;
	private List<String> currentStates;
	private String state;
	private List<TreeObject> children;
	private List<Equipments> equipmensInfo;

	public Integer getEquStartSum() {
		return equStartSum;
	}
	public void setEquStartSum(Integer equStartSum) {
		this.equStartSum = equStartSum;
	}
	public List<Equipments> getEquipmensInfo() {
		return equipmensInfo;
	}
	public void setEquipmensInfo(List<Equipments> equipmensInfo) {
		this.equipmensInfo = equipmensInfo;
	}
	public List<TreeObject> getChildren() {
		return children;
	}
	public void setChildren(List<TreeObject> children) {
		this.children = children;
	}
	public List<String> getCurrentStates() {
		return currentStates;
	}
	public void setCurrentStates(List<String> currentStates) {
		this.currentStates = currentStates;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	private Companies company;

	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public Integer getCurrentState() {
		return currentState;
	}
	public void setCurrentState(Integer currentState) {
		this.currentState = currentState;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getPlaceOfOrigin() {
		return placeOfOrigin;
	}
	public void setPlaceOfOrigin(String placeOfOrigin) {
		this.placeOfOrigin = placeOfOrigin;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getOemName() {
		return oemName;
	}
	public void setOemName(String oemName) {
		this.oemName = oemName;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Integer getWarrantyPeriod() {
		return warrantyPeriod;
	}
	public void setWarrantyPeriod(Integer warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}
	public String getOemPerson() {
		return oemPerson;
	}
	public void setOemPerson(String oemPerson) {
		this.oemPerson = oemPerson;
	}
	public String getSupplierPerson() {
		return supplierPerson;
	}
	public void setSupplierPerson(String supplierPerson) {
		this.supplierPerson = supplierPerson;
	}
	public String getCustomerPerson() {
		return customerPerson;
	}
	public void setCustomerPerson(String customerPerson) {
		this.customerPerson = customerPerson;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(String productionDate) {
		this.productionDate = productionDate;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getGuaranteeBeginDate() {
		return guaranteeBeginDate;
	}
	public void setGuaranteeBeginDate(String guaranteeBeginDate) {
		this.guaranteeBeginDate = guaranteeBeginDate;
	}
	public String getProLineCode() {
		return proLineCode;
	}
	public void setProLineCode(String proLineCode) {
		this.proLineCode = proLineCode;
	}
	public List<AuthUsers> getOemPersons() {
		return oemPersons;
	}
	public void setOemPersons(List<AuthUsers> oemPersons) {
		this.oemPersons = oemPersons;
	}
	public List<AuthUsers> getCustomerPersons() {
		return customerPersons;
	}
	public void setCustomerPersons(List<AuthUsers> customerPersons) {
		this.customerPersons = customerPersons;
	}
	public List<EquipmentPoints> getPoints() {
		return points;
	}
	public void setPoints(List<EquipmentPoints> points) {
		this.points = points;
	}
	public String getQrUrl() {
		return qrUrl;
	}
	public void setQrUrl(String qrUrl) {
		this.qrUrl = qrUrl;
	}
	public String getOems() {
		return oems;
	}
	public void setOems(String oems) {
		this.oems = oems;
	}
	public String getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(String suppliers) {
		this.suppliers = suppliers;
	}
	public String getCustomers() {
		return customers;
	}
	public void setCustomers(String customers) {
		this.customers = customers;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getAttrInfo() {
		return attrInfo;
	}
	public void setAttrInfo(String attrInfo) {
		this.attrInfo = attrInfo;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getCustomTag() {
		return customTag;
	}
	public void setCustomTag(String customTag) {
		this.customTag = customTag;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public String getEquipmentName() {
		return equipmentName;
	}
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getEquSum() {
		return equSum;
	}
	public void setEquSum(Integer equSum) {
		this.equSum = equSum;
	}
	public Integer getEquFaultSum() {
		return equFaultSum;
	}
	public void setEquFaultSum(Integer equFaultSum) {
		this.equFaultSum = equFaultSum;
	}
	public Integer getEquDisConnectSum() {
		return equDisConnectSum;
	}
	public void setEquDisConnectSum(Integer equDisConnectSum) {
		this.equDisConnectSum = equDisConnectSum;
	}
	public String getPileNo() {
		return pileNo;
	}
	public void setPileNo(String pileNo) {
		this.pileNo = pileNo;
	}
	public Map<String, Object> getConfigs() {
		return configs;
	}
	public void setConfigs(Map<String, Object> configs) {
		this.configs = configs;
	}
	public List<EquipmentsAttr> getEquipmentsAttr() {
		return equipmentsAttr;
	}
	public void setEquipmentsAttr(List<EquipmentsAttr> equipmentsAttr) {
		this.equipmentsAttr = equipmentsAttr;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getQueryKey() {
		return queryKey;
	}
	public void setQueryKey(String queryKey) {
		this.queryKey = queryKey;
	}
	public Map<String, Object> getIndexType() {
		return indexType;
	}
	public void setIndexType(Map<String, Object> indexType) {
		this.indexType = indexType;
	}
    @Override
    public String toString() {
        return "{\"customerCode\":\"" + customerCode + "\",\"proLineCode\":\"" + proLineCode + "\",\"name\":\"" + name
                + "\",\"pileNo\":\"" + pileNo + "\",\"serialNumber\":\"" + serialNumber + "\",\"categoryCode\":\""
                + categoryCode + "\",\"categoryName\":\"" + categoryName + "\",\"brand\":\"" + brand + "\",\"model\":\""
                + model + "\",\"placeOfOrigin\":\"" + placeOfOrigin + "\",\"imageUrl\":\"" + imageUrl
                + "\",\"oemName\":\"" + oemName + "\",\"productionDate\":\"" + productionDate + "\",\"supplierCode\":\""
                + supplierCode + "\",\"supplierName\":\"" + supplierName + "\",\"purchaseDate\":\"" + purchaseDate
                + "\",\"guaranteeBeginDate\":\"" + guaranteeBeginDate + "\",\"warrantyPeriod\":\"" + warrantyPeriod
                + "\",\"oemPerson\":\"" + oemPerson + "\",\"supplierPerson\":\"" + supplierPerson
                + "\",\"customerPerson\":\"" + customerPerson + "\",\"oemPersonName\":\"" + oemPersonName
                + "\",\"customerName\":\"" + customerName + "\",\"customerPhone\":\"" + customerPhone
                + "\",\"oemPersons\":\"" + oemPersons + "\",\"customerPersons\":\"" + customerPersons
                + "\",\"points\":\"" + points + "\",\"oems\":\"" + oems + "\",\"suppliers\":\"" + suppliers
                + "\",\"customers\":\"" + customers + "\",\"qrUrl\":\"" + qrUrl + "\",\"modelName\":\"" + modelName
                + "\",\"agentName\":\"" + agentName + "\",\"attrInfo\":\"" + attrInfo + "\",\"equipmentCode\":\""
                + equipmentCode + "\",\"equipmentName\":\"" + equipmentName + "\",\"customTag\":\"" + customTag
                + "\",\"attrName\":\"" + attrName + "\",\"value\":\"" + value + "\",\"sectionCode\":\"" + sectionCode
                + "\",\"type\":\"" + type + "\",\"sectionName\":\"" + sectionName + "\",\"configs\":\"" + configs
                + "\",\"equipmentsAttr\":\"" + equipmentsAttr + "\",\"indexType\":\"" + indexType + "\",\"count\":\""
                + count + "\",\"equStartSum\":\"" + equStartSum + "\",\"equSum\":\"" + equSum + "\",\"equFaultSum\":\""
                + equFaultSum + "\",\"equDisConnectSum\":\"" + equDisConnectSum + "\",\"map\":\"" + map
                + "\",\"parentId\":\"" + parentId + "\",\"grade\":\"" + grade + "\",\"locations\":\"" + locations
                + "\",\"queryKey\":\"" + queryKey + "\",\"direction\":\"" + direction + "\",\"supplierPersonName\":\""
                + supplierPersonName + "\",\"customerPersonName\":\"" + customerPersonName + "\",\"country\":\""
                + country + "\",\"province\":\"" + province + "\",\"city\":\"" + city + "\",\"location\":\"" + location
                + "\",\"comment\":\"" + comment + "\",\"lineName\":\"" + lineName + "\",\"currentState\":\""
                + currentState + "\",\"currentStates\":\"" + currentStates + "\",\"state\":\"" + state
                + "\",\"children\":\"" + children + "\",\"equipmensInfo\":\"" + equipmensInfo + "\",\"company\":\""
                + company + "\"}  ";
    }
	
}
