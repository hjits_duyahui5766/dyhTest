package com.cloudinnov.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EquipmentInspectionWorkItem extends BaseModel {

	@JsonIgnore
	private String[] codes;
	
	private String configCode;

	private String customerCode;

	private String equipmentCode;

	private Integer selectType;

	@JsonIgnore
	private String selectItem;

	private List<SelectName> selectItems;
	
	private List<SelectContent> selectContents;

	private String selectedItem;

	private String remark;

	private Integer orderId;

	private String name;

	private String description;
	
	private String result;
	
	private String resultCode;
	
	private String[] files;

	public String getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public Integer getSelectType() {
		return selectType;
	}

	public void setSelectType(Integer selectType) {
		this.selectType = selectType;
	}

	public String getSelectItem() {
		return selectItem;
	}

	public void setSelectItem(String selectItem) {
		this.selectItem = selectItem;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	
	public List<SelectName> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<SelectName> selectItems) {
		this.selectItems = selectItems;
	}


	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}



	public static class SelectName {

		private Integer id;
		
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
		
	}
	
	@JsonIgnore
    public SelectName getSelectName() { 
        return new SelectName(); 
    } 
    
    public static class SelectContent {

    	private Integer id;
    	
		private String content;
		
		private boolean isChecked;
		
		
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public boolean isChecked() {
			return isChecked;
		}

		public void setChecked(boolean isChecked) {
			this.isChecked = isChecked;
		}	
	}
    
    @JsonIgnore
    public SelectContent getSelectContent(){
    	return new SelectContent();
    }

	public List<SelectContent> getSelectContents() {
		return selectContents;
	}

	public void setSelectContents(List<SelectContent> selectContents) {
		this.selectContents = selectContents;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String[] getFiles() {
		return files;
	}

	public void setFiles(String[] files) {
		this.files = files;
	}

	public String[] getCodes() {
		return codes;
	}

	public void setCodes(String[] codes) {
		this.codes = codes;
	}
    
	
}