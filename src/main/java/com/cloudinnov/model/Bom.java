package com.cloudinnov.model;

public class Bom extends BaseModel{
    private Integer id;
    
    private String code;

    private String oemCode;

    private String model;

    private String brand;

    private String category;

    private Integer status;

    private String createTime;

    private String lastUpdateTime;

    private String ciCode;
    
    private String ciProductUrl;
    
    private Integer ciTransStatus;
    
    private String language;
    
    private String name;
    
    private String image_url;
    
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOemCode() {
        return oemCode;
    }

    public void setOemCode(String oemCode) {
        this.oemCode = oemCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getCiCode() {
        return ciCode;
    }

    public void setCiCode(String ciCode) {
        this.ciCode = ciCode;
    }

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCiProductUrl() {
		return ciProductUrl;
	}

	public void setCiProductUrl(String ciProductUrl) {
		this.ciProductUrl = ciProductUrl;
	}

	public Integer getCiTransStatus() {
		return ciTransStatus;
	}

	public void setCiTransStatus(Integer ciTransStatus) {
		this.ciTransStatus = ciTransStatus;
	}
    
    
}