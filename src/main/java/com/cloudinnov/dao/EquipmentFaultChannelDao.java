package com.cloudinnov.dao;

import java.util.Map;

import com.cloudinnov.model.EquipmentFaultChannel;

/**
 * @author guochao
 * @date 2016年2月17日下午4:49:37
 * @email chaoguo@cloudinnov.com
 * @remark 设备故障通道dao接口
 * @version
 */
public interface EquipmentFaultChannelDao extends
		IBaseDao<EquipmentFaultChannel> {
	public int insertInfo(EquipmentFaultChannel efc);
	
	public int updateInfoByCondition(EquipmentFaultChannel efc);
	
	/** 批量插入
	 * @Title: insertBatch 
	 * @Description: TODO
	 * @param params
	 * @return
	 * @return: int
	 */
	public int insertBatch(Map<String, Object> params);
}
