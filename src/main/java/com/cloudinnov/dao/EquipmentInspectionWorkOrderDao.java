package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.EquipmentInspectionWorkOrder;

public interface EquipmentInspectionWorkOrderDao extends IBaseDao<EquipmentInspectionWorkOrder>{

	public List<EquipmentInspectionWorkOrder> search(Map<String, Object> map);
}