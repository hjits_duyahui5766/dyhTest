package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.EquipmentBoms;

/**
 * @author guochao
 * @date 2016年2月17日下午4:40:46
 * @email chaoguo@cloudinnov.com
 * @remark 设备bom service接口
 * @version
 */
public interface EquipmentBomsLogic extends IBaseLogic<EquipmentBoms> {

	public int saveOtherLanguage(EquipmentBoms equipmentBom);

	public int updateOtherLanguage(EquipmentBoms equipmentBom);
	
	public List<EquipmentBoms> listByCode(EquipmentBoms equipmentBom);
	
	public int bomImport(String data,EquipmentBoms equipmentBom);
}
