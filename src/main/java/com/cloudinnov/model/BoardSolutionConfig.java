package com.cloudinnov.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BoardSolutionConfig extends BaseModel {
	private Integer id;
	private String solutionCode;
	private String equipmentCode;
	private String equipmentClassify;
	private Integer rowCount;
	private String color;
	private Integer displayType;
	@JsonIgnore
	private String sendContent;
	private Integer status;
	private String comment;
	private String createTime;
	private String lastUpdateTime;
	private Integer solutionType;
	private String categoryCode;
	@JsonIgnore
	private String data;
	@JsonIgnore
	private List<Data> list;
	private Integer speed;
	private Integer stayTime;
	private String boardTemplateCode;
	private String boardTemplateName;
	@JsonIgnore
	private Map<String, Object> equipmentCodes;

	public Map<String, Object> getEquipmentCodes() {
		return equipmentCodes;
	}
	public void setEquipmentCodes(Map<String, Object> equipmentCodes) {
		this.equipmentCodes = equipmentCodes;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@JsonProperty("data")
	public List<Data> getList() {
		return list;
	}
	public void setList(List<Data> list) {
		this.list = list;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSolutionCode() {
		return solutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getEquipmentClassify() {
		return equipmentClassify;
	}
	public void setEquipmentClassify(String equipmentClassify) {
		this.equipmentClassify = equipmentClassify;
	}
	public Integer getRowCount() {
		return rowCount;
	}
	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Integer getDisplayType() {
		return displayType;
	}
	public void setDisplayType(Integer displayType) {
		this.displayType = displayType;
	}
	public String getSendContent() {
		return sendContent;
	}
	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Integer getSolutionType() {
		return solutionType;
	}
	public void setSolutionType(Integer solutionType) {
		this.solutionType = solutionType;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public Integer getSpeed() {
		return speed;
	}
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	public Integer getStayTime() {
		return stayTime;
	}
	public void setStayTime(Integer stayTime) {
		this.stayTime = stayTime;
	}
	public String getBoardTemplateCode() {
		return boardTemplateCode;
	}
	public void setBoardTemplateCode(String boardTemplateCode) {
		this.boardTemplateCode = boardTemplateCode;
	}
	public String getBoardTemplateName() {
		return boardTemplateName;
	}
	public void setBoardTemplateName(String boardTemplateName) {
		this.boardTemplateName = boardTemplateName;
	}

	public static class Data {
		private Integer spacing = 05;// 字间距
		/** 字体大小 */
		private int fontSize = 32;
		/** 字体名称 */
		private String fontName = "宋体";
		private Integer row;
		private String content;
		private String color;
		private Integer left;
		private Integer top;

		public int getFontSize() {
			return fontSize;
		}
		public void setFontSize(int fontSize) {
			this.fontSize = fontSize;
		}
		public String getFontName() {
			return fontName;
		}
		public void setFontName(String fontName) {
			this.fontName = fontName;
		}
		public Integer getSpacing() {
			return spacing;
		}
		public void setSpacing(Integer spacing) {
			this.spacing = spacing;
		}
		public Integer getRow() {
			return row;
		}
		public void setRow(Integer row) {
			this.row = row;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public Integer getLeft() {
			return left;
		}
		public void setLeft(Integer left) {
			this.left = left;
		}
		public Integer getTop() {
			return top;
		}
		public void setTop(Integer top) {
			this.top = top;
		}
		@Override
		public String toString() {
			return "Data [spacing=" + spacing + ", fontSize=" + fontSize + ", fontName=" + fontName + ", row=" + row
					+ ", content=" + content + ", color=" + color + ", left=" + left + ", top=" + top + "]";
		}
	}
}
