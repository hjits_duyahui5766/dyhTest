package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AttachLogic;
import com.cloudinnov.logic.ProductionLinesLogic;
import com.cloudinnov.model.Attach;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLineEquipments;
import com.cloudinnov.model.ProductionLines;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author nilixin
 * @date 2016年2月24日下午6:10:25
 * @email
 * @remark 生产线表 controller
 * @version
 */
@Controller
@RequestMapping("/webapi/productionLine")
public class ProductionLinesController extends BaseController {

	@Autowired
	private ProductionLinesLogic productionLinesLogic;
	@Autowired
	private AttachLogic attachLogic;

	private int digit = 5;

	private static final String ATTACH = "productionLine";
	private static final String IMAGE = "image";

	/**
	 * save
	 * 
	 * @Description: 保存生产线表
	 * @param @param
	 *            productionLine
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "生产线", methods = "生产线信息保存")
	public void save(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 添加生产线
		productionLine.setOemCode(getUserInfo(request).get(OEM_CODE));
		productionLine.setCode(CodeUtil.prolineCode(digit));
		int result = productionLinesLogic.insertProductLineEquipment(productionLine);

		// 保存图片
		Attach attach = new Attach();
		attach.setObjectCode(productionLine.getCode()); // 保存图片
		if(productionLine.getImage()!=null && !productionLine.getImage().equals("")){
			String urls = storeBase64(productionLine.getImage(), request);
			attach.setUrl(urls);
		}
		attach.setType(ATTACH);
		attach.setFileType(IMAGE);
		attachLogic.save(attach);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}

	}

	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "生产线", methods = "生产线添加其他语言")
	public void saveOtherLanguage(ProductionLines productionLine, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		productionLine.setLanguage(productionLine.getOtherLanguage());
		int result = productionLinesLogic.saveOtherLanguage(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "生产线", methods = "生产线添加其他语言")
	public void updateOtherLanguage(ProductionLines productionLine, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		productionLine.setLanguage(productionLine.getOtherLanguage());
		int result = productionLinesLogic.updateOtherLanguage(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据product的code 删除生产线表
	 * @param @param
	 *            productionLinesCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemLog(module = "生产线", methods = "生产线信息删除")
	public void delete(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productionLinesLogic.delete(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * update
	 * 
	 * @Description: 更新生产线表
	 * @param @param
	 *            productionLine
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "生产线", methods = "生产线信息修改")
	public void update(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productionLinesLogic.update(productionLine);
		result = productionLinesLogic.updateProductLineEquipment(productionLine);
		// 更新图片
		Attach attach = new Attach();
		if (productionLine.getImage() != null) {
			attach.setObjectCode(productionLine.getCode()); // 保存图片
			String urls = storeBase64(productionLine.getImage(), request);
			attach.setUrl(urls);
			attach.setType(ATTACH);
			attach.setFileType(IMAGE);
			attachLogic.update(attach);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		if (result >= DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * list
	 * 
	 * @Description: 查找所有生产线
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void listPage(int index, int size, ProductionLines productionLines, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String userType = getUserInfo(request).get(USER_TYPE);
		if (userType.equals(CommonUtils.ADMIN_TYPE)) {

		} else if(userType.equals(CommonUtils.INTEGRATOR_TYPE)){
			productionLines.setIntegratorCode(getUserInfo(request).get(INTEGRATOR_CODE));
		}else if (userType.equals(CommonUtils.OEM_TYPE)) {
			productionLines.setOemCode(getUserInfo(request).get(OEM_CODE));
		} else if (userType.equals(CommonUtils.AGENT_TYPE)) {
			productionLines.setAgentCode(getUserInfo(request).get(COM_CODE));
		}

		Page<ProductionLines> equipments = productionLinesLogic.selectListPage(index, size, productionLines, false);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipments != null) {
			map.put("list", equipments);
			map.put("total", equipments.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * 查询生产线列表 |无分页 获取产线以及产线下的关键点位
	 * 
	 * @param productionLines
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/listnopage")
	@ResponseBody
	public void list(ProductionLines productionLines, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		productionLines.setOemCode(getUserInfo(request).get(OEM_CODE));
		/**
		 * 关键点位
		 */
		productionLines.setIsCrucial(CommonUtils.ISCRUCIAL_POINT_YES);
		Map<String, Object> map = new HashMap<String, Object>();

		List<ProductionLines> equipments = productionLinesLogic.selectList(productionLines, false);
		if (equipments != null) {
			map.put("list", equipments);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找生产线表
	 * @param @param
	 *            productionLineCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (productionLine.getOtherLanguage() != null && productionLine.getOtherLanguage() != "") {
			productionLine.setLanguage(productionLine.getOtherLanguage());
		}
		ProductionLines result = productionLinesLogic.select(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			Attach attach = new Attach();
			attach.setType(ATTACH);
			attach.setObjectCode(productionLine.getCode());
			Attach image = attachLogic.select(attach);
			if (image != null) {
				result.setImage(image.getUrl());
			}
			map.put("model", result);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	@RequestMapping(value = "/selectProlineEqusNotExists")
	@ResponseBody
	public void selectProlineEqusNotExists(ProductionLines productionLine, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<Equipments> result = productionLinesLogic.selectProlineEqusNotExists(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	@RequestMapping(value = "/selectProlineState")
	@ResponseBody
	public void selectProlineState(ProductionLines productionLine, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		int result = productionLinesLogic.selectProlineStates(productionLine);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prostate", result);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	/**
	 * selectEquipmentTotalByCustomerId
	 * 
	 * @Description: 根据客户id 查询该客户下的所有产线总数
	 * @param @param
	 *            customerId
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/selectProductionLineTotalByCustomerId")
	@ResponseBody
	public void selectEquipmentTotalByCustomerId(String customerCode, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		int result = productionLinesLogic.selectProductionLineTotalByCustomerCode(customerCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	@RequestMapping(value = "/selectEquByProLine")
	@ResponseBody
	public void selectProLineEqu(ProductionLineEquipments productionLineEqu, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<Map<String, Object>> result = productionLinesLogic.selectEquipmentByProLineCode(productionLineEqu);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			map.put("total", result.size());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}

	}

	/**
	 * 查询客户下的生产线
	 * 
	 * @param productionLine
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/listbyquery")
	@ResponseBody
	public void listByQuery(String codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<ProductionLines> equipments = productionLinesLogic.selectProLineCodeNameByCustomerCode(codes, language);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipments != null) {
			map.put("list", equipments);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

}
