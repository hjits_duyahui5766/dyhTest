package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.ReportCar;

public interface EquipmentsAttrDao extends IBaseDao<EquipmentsAttr> {
	/**
	 * 批量插入
	 * @Title: insertBatch
	 * @Description: TODO
	 * @param params
	 * @return
	 * @return: int
	 */
	int insertBatch(Map<String, Object> params);
	List<EquipmentsAttr> selectListByEquipmentCode(@Param("equipmentCode") String equipmentCode);
	int deleteInfoByEquCode(String equipmentCode);
	/**
	 * 通过单一附加属性查询设备列表
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param customTag
	 * @param @return 参数
	 * @return List<EquipmentsAttr> 返回类型
	 */
	List<EquipmentsAttr> selectEquListByCustomTag(@Param("customTag") String customTag);
	/**
	 * 通过附加属性查询摄像头列表
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param customTag
	 * @param @return 参数
	 * @return List<EquipmentsAttr> 返回类型
	 */
	List<EquipmentsAttr> selectCameraListByCustomTag(@Param("customTag") String customTag);
	/**
	 * 根据设备附加属性key和value查询出设备编码
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @return 参数
	 * @return List<EquipmentsAttr> 返回类型
	 */
	List<EquipmentsAttr> selectEquipmentByCustomTagAndValue(EquipmentsAttr model);
	int updateBatchByCondition(Map<String, Object> map);
	
	/**
	 * 根据设备Code和custom_tag查询出设备custom_tag:value
	 * @param model
	 * @return 参数
	 * @return EquipmentsAttr 返回类型
	 */
	int saveCarDayData(ReportCar reportCar);
	
	EquipmentsAttr selectEquipmentByEquipmentCodeAndCustomTag(EquipmentsAttr model);
	
	List<ReportCar> selectTotalDayDataByMysql(@Param("equipmentCode")String equipmentCode,@Param("startTime")String startTime,@Param("endTime")String endTime);
	
	List<ReportCar> selectTotalMonthDataByMysql(@Param("equipmentCode")String equipmentCode,@Param("startTime")String startTime,@Param("endTime")String endTime);
	
	List<ReportCar> selectTotalYearDataByMysql(@Param("equipmentCode")String equipmentCode,@Param("startTime")String startTime,@Param("endTime")String endTime);
}
