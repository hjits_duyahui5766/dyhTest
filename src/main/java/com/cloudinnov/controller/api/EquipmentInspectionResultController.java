package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AttachLogic;
import com.cloudinnov.logic.EquipmentInspectionResultLogic;
import com.cloudinnov.model.Attach;
import com.cloudinnov.model.EquipmentInspectionResult;
import com.cloudinnov.model.EquipmentInspectionWorkItem;
import com.cloudinnov.model.SelectContent;
import com.cloudinnov.utils.CommonUtils;

@Controller
@RequestMapping("/webapi/inspection/result")
public class EquipmentInspectionResultController extends BaseController {

	@Autowired
	private EquipmentInspectionResultLogic equipmentInspectionResultLogic;
	
	@Autowired
	private AttachLogic attachLogic;

	/** 巡检工单项结果保存
	 * @Title: save 
	 * @Description: TODO
	 * @param equipmentInspectionResult
	 * @param request
	 * @param response
	 * @throws IOException
	 * @return: void
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "巡检管理", methods = "巡检结果保存")
	public void save(EquipmentInspectionResult equipmentInspectionResult, EquipmentInspectionWorkItem equipmentInspectionWorkItem ,HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = 0;
		String result = "";
		if(equipmentInspectionWorkItem.getSelectType() != 3){
			List<SelectContent> selectContents = JSON.parseArray(equipmentInspectionWorkItem.getResult(),SelectContent.class);
			for(SelectContent model : selectContents){
				if(equipmentInspectionWorkItem.getSelectType() == 1){
					if(model.isChecked()){
						result += model.getId()+CommonUtils.SPLIT_COMMA;
					}
				}else if(equipmentInspectionWorkItem.getSelectType() == 2){
					if(model.isChecked()){
						result = String.valueOf(model.getId());
					}
				}
			}
			equipmentInspectionResult.setResult(result);
		}
		
		EquipmentInspectionResult resultModel = equipmentInspectionResultLogic.select(equipmentInspectionResult);
		if(resultModel !=null){
			returnCode = equipmentInspectionResultLogic.update(equipmentInspectionResult);
			equipmentInspectionResult.setCode(resultModel.getCode());
		}else{
			returnCode = equipmentInspectionResultLogic.save(equipmentInspectionResult);
		}
		int msgType = Integer.parseInt(request.getParameter("msgType"));
		Attach attach = null;
		String[] fileUrls = null;
		if (msgType == CommonUtils.UPLOAD_TYPE_MSG_IMAGE) {
			fileUrls = uploadFile(request, CommonUtils.UPLOAD_TYPE_MSG_IMAGE_STR);
			attach = new Attach();
			attach.setFileType(CommonUtils.UPLOAD_TYPE_MSG_IMAGE_STR);
		}else if (msgType == CommonUtils.UPLOAD_TYPE_MSG_VOICE) {
			fileUrls = uploadFile(request, CommonUtils.UPLOAD_TYPE_MSG_VOICE_STR);
			attach = new Attach();
			attach.setFileType(CommonUtils.UPLOAD_TYPE_MSG_VOICE_STR);
		}else if (msgType == CommonUtils.UPLOAD_TYPE_MSG_VIDEO) {
			fileUrls = uploadFile(request, CommonUtils.UPLOAD_TYPE_MSG_VIDEO_STR);
			attach = new Attach();
			attach.setFileType(CommonUtils.UPLOAD_TYPE_MSG_VIDEO_STR);
		}
		if(fileUrls !=null && fileUrls.length>0){
			attach.setUrls(fileUrls);
			attach.setObjectCode(equipmentInspectionResult.getCode()); // 保存资源
			returnCode = attachLogic.saveInspectionImage(attach);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (returnCode == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "巡检管理", methods = "巡检结果删除")
	public void delete(EquipmentInspectionResult equipmentInspectionResult, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = equipmentInspectionResultLogic.delete(equipmentInspectionResult);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentInspectionResult equipmentInspectionResult, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		EquipmentInspectionResult result = equipmentInspectionResultLogic.select(equipmentInspectionResult);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			response.getWriter().print(returnJsonAllRequest(request, response, map, SUCCESS, ""));
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}
	
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "巡检管理", methods = "巡检结果更新")
	public void update(EquipmentInspectionResult equipmentInspectionResult, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = equipmentInspectionResultLogic.update(equipmentInspectionResult);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);

	}
}
