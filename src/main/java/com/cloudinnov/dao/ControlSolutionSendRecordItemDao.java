package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.ControlSolutionSendRecordItem;

public interface ControlSolutionSendRecordItemDao extends IBaseDao<ControlSolutionSendRecordItem> {
	/**
	 * 根据设备编码查询最后的发送记录
	 * @Title: selectLastSendRecordByEquipmentCode
	 * @Description: TODO
	 * @return
	 * @return: List<ControlSolutionSendRecordItem>
	 */
	List<ControlSolutionSendRecordItem> selectLastSendRecordByEquipmentCode(ControlSolutionSendRecordItem model);
	/**
	 * 根据设备编码查询发送记录(去重)
	 * @Title: selectSendRecordByEquipmentCode
	 * @Description: TODO
	 * @param model
	 * @return
	 * @return: List<ControlSolutionSendRecordItem>
	 */
	List<ControlSolutionSendRecordItem> selectSendRecordByEquipmentCode(ControlSolutionSendRecordItem model);
	/**
	 * 搜索情报板发送记录发送记录
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param map solutionCode 路段编码 cateCode设备分类编码 startTime 开始时间 endTime结束时间
	 * @return 参数
	 * @return List<ControlSolutionSendRecordItem> 返回类型
	 */
	List<ControlSolutionSendRecordItem> search(Map<String, Object> map);
	/**
	 * 根据设备编码和发送时间查询某个设备某个时间点发送的记录
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model equipmentCode 设备编码 sendTime发送时间戳
	 * @return 参数
	 * @return List<ControlSolutionSendRecordItem> 返回类型
	 */
	List<ControlSolutionSendRecordItem> selectListByEquCodeAndSendTime(ControlSolutionSendRecordItem model);
}
