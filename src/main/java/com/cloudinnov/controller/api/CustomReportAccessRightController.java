package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.CustomReportAccessRightLogic;
import com.cloudinnov.model.CustomReportAccessRight;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/customReportAccessRight")
public class CustomReportAccessRightController extends BaseController {
	@Autowired
	private CustomReportAccessRightLogic customReportAccessRightLogic;

	/**
	 * 报表权限配置-添加
	 * 
	 * @param customReportAccessRight
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表权限配置", methods = "添加")
	public void save(CustomReportAccessRight customReportAccessRight, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportAccessRightLogic.save(customReportAccessRight);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表权限配置-删除
	 * 
	 * @param customReportAccessRight
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表权限配置", methods = "删除")
	public void delete(CustomReportAccessRight customReportAccessRight, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportAccessRightLogic.delete(customReportAccessRight);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表权限配置-修改
	 * 
	 * @param customReportAccessRight
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "报表权限配置", methods = "修改")
	public void update(CustomReportAccessRight customReportAccessRight, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportAccessRightLogic.update(customReportAccessRight);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表权限配置-查询（分页）
	 * 
	 * @param customReportAccessRight
	 *            参数
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	@SystemLog(module = "报表权限配置", methods = "查询")
	public void listPage(CustomReportAccessRight customReportAccessRight, int index, int size,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Page<CustomReportAccessRight> customReportAccessRights = customReportAccessRightLogic.selectListPage(index,
				size, customReportAccessRight, false);
		Map<String, Object> map = new HashMap<String, Object>();
		if (customReportAccessRights != null) {
			map.put("list", customReportAccessRights);
			map.put("total", customReportAccessRights.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

}
