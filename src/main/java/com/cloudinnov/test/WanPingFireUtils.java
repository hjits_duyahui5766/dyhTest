package com.cloudinnov.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;


public class WanPingFireUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static boolean isConnect = false;
	private static Socket socket;
	private static String ip="10.141.181.253";
	private static int port=5000; 
	private static final String CODEDFORMAT = "gb2312";
	private static final int REVEIVEBUFFERSIZE = 1024 * 8;// 接收缓存8k
	
	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
		startConnectService();
	}

	public static void startConnectService() {

		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			System.err.println("连接成功");
		} else {
			connectAgain();

		}

	}

	public static void connectAgain() {
		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			System.err.println("连接成功");
		} else {
			connectAgain();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	public static class ThreadReader extends Thread {
		Socket socket;

		public ThreadReader(Socket socket) {
			this.socket = socket;
			this.setName("ThreadReader");
		}

		@Override
		public void run() {
 
			InputStream inputStream = null;
			InputStreamReader inputStreamReader = null;
			BufferedReader bufferedReader = null;
			
			
			try {
				inputStream = (InputStream) socket.getInputStream();// 获取一个输入流，接收服务端的信息
				inputStreamReader = new InputStreamReader(inputStream, CODEDFORMAT);// 包装成字符流，提高效率,gbk
				bufferedReader = new BufferedReader(inputStreamReader);// 缓冲区
				char[] rcchar = new char[REVEIVEBUFFERSIZE];
				// TCP长连接，十秒一个心跳数据包，连接断开会抛出异常退出接收线程
				while (true) {// 长连接读取CRT信息，连接断开会抛出异常
					int len = bufferedReader.read(rcchar);// 堵塞读取
					if (len == -1) {
						throw new Exception("socket连接出错");
					}
					String crtstrs = String.valueOf(rcchar, 0, len);
					System.err.println(crtstrs);
					parseData(crtstrs);
					 
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static class ThreadWriter extends Thread {
		Socket socket;

		public ThreadWriter(Socket socket) {
			this.socket = socket;
			this.setName("ThreadWriter");
		}

		@Override
		public void run() {
			try {
				while (true) {
					socket.sendUrgentData(0xff);
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				System.err.println("网络断线");
			}
		}
	}

	public static void getCurrentThread() {
		for (Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
			Thread thread = entry.getKey();

			StackTraceElement[] stackTraceElements = entry.getValue();

			if (thread.equals(Thread.currentThread())) {
				continue;
			}
			System.out.println("\n线程： " + thread.getName() + "\n");
			for (StackTraceElement element : stackTraceElements) {
				System.out.println("\t" + element + "\n");
			}
		}
	}
	
	public static void parseData(String receiveData) {
		try {
			String[] receiveArr = receiveData.split("\r\n");
			 
			if(receiveArr.length<4) {
				String pile=receiveArr[2];
				String[] alarmMsgArr=receiveArr[1].split("::");
				System.err.println(alarmMsgArr);
				String fireType=alarmMsgArr[0];
				String[] fireContentArr = alarmMsgArr[1].split(" ");
				System.err.println(fireContentArr);
				String time = fireContentArr[2];
				String date = fireContentArr[3];
				String controlPan = fireContentArr[4].split(":")[1];//盘号
				String controlCard = fireContentArr[6].split(":")[1];//卡号
				String controlDevice = fireContentArr[8].split(":")[1];//器件号
				System.err.println(fireType+"  "+time+"  "+date+"  "+controlPan+"  "+controlCard+"  "+controlDevice+" "+pile);
					
			}
		} catch (Exception e) {
			System.err.println("解析失败："+e);
		}
	}
	
}
