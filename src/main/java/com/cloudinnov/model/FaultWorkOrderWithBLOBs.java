package com.cloudinnov.model;

public class FaultWorkOrderWithBLOBs extends FaultWorkOrder {
    private String description;

    private String handingSuggestion;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHandingSuggestion() {
        return handingSuggestion;
    }

    public void setHandingSuggestion(String handingSuggestion) {
        this.handingSuggestion = handingSuggestion;
    }
}