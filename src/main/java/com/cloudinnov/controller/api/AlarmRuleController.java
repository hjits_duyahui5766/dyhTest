package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AlarmRuleLogic;
import com.cloudinnov.model.AlarmRule;
import com.cloudinnov.utils.CommonUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.pagehelper.Page;

/**
 * @author libo
 * @date 2016年11月25日 下午5:37:44
 * @email boli@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/alarmRule")
public class AlarmRuleController extends BaseController {

	@Autowired
	private AlarmRuleLogic alarmRuleLogic;

	/**
	 * 告警规则管理--保存 save
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws JsonProcessingException
	 *             IOException
	 *
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "告警规则管理", methods = "告警规则添加")
	public void save(AlarmRule alarmRule, HttpServletRequest request, HttpServletResponse response)
			throws JsonProcessingException, IOException {
		alarmRule.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = alarmRuleLogic.save(alarmRule);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 告警规则管理--删除 delete
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 *
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "告警规则管理", methods = "告警规则删除")
	public void delete(AlarmRule alarmRule, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = alarmRuleLogic.delete(alarmRule);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 告警规则管理--修改 update
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 *
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "告警规则管理", methods = "告警规则修改")
	public void update(AlarmRule alarmRule, HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		int resultCode = alarmRuleLogic.update(alarmRule);
		String returnData = "";

		Map<String, Object> map = new HashMap<String, Object>();

		if (resultCode == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}

		response.getWriter().print(returnData);
	}

	/**
	 * 告警规则管理--查询，分页 listPage
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 *
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "告警规则管理", methods = "告警规则列表带分页")
	public void listPage(AlarmRule alarmRule, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<AlarmRule> alarmRules = alarmRuleLogic.selectListPage(index, size, alarmRule, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (alarmRules != null) {
			map.put("list", alarmRules);
			map.put("total", alarmRules.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * 告警规则查询--无分页 selectList
	 * 
	 * @Description
	 * @param
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 *
	 */
	@RequestMapping(value = "/selectList", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "告警规则管理", methods = "告警规则查询无分页")
	public void selectList(AlarmRule alarmRule, HttpServletRequest request, HttpServletResponse response)
			throws JsonProcessingException, IOException {

		AlarmRule result = alarmRuleLogic.select(alarmRule);

		HashMap<String, Object> map = new HashMap<String, Object>();

		if (result != null) {
			map.put("list", result);
			response.getWriter().print(returnJsonAllRequest(request, response, map, SUCCESS, ""));
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}

	}
	
	/**
	 * 告警规则开关修改
	 * @param code
	 * @param onOff
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value="/updateAlarmRuleOnOff",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="告警规则管理",methods = "告警规则开关修改")
	public void updateAlarmRuleOnOff(String code,Integer onOff,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = alarmRuleLogic.updateAlarmRuleOnOff(code,onOff);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
}
