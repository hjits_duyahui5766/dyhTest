package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.SysIndustry;

public interface SysIndustryDao extends IBaseDao<SysIndustry>{
	
	public int insertInfo(SysIndustry sysIndustry);

	public int updateInfoByCondition(SysIndustry sysIndustry);
	
	public SysIndustry selectEntityByParentCode(SysIndustry sysIndustry);
	
	public List<SysIndustry> tableList(Map<String, Object> map);
	
	public List<SysIndustry> search(Map<String, Object> map);
	
	
}