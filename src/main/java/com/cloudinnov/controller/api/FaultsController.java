package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.FaultsLogic;
import com.cloudinnov.model.Faults;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年2月18日下午2:53:00
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/fault")
public class FaultsController extends BaseController {
	@Autowired
	private FaultsLogic faultsLogic;

	/**
	 * 新增故障
	 * @Description: 新故障
	 * @param @param monitorView
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "信息保存")
	public void save(Faults faults, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		faults.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = faultsLogic.save(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * delete
	 * @Description: 根据code删除一条监控视图
	 * @param @param faultCode
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "信息删除")
	public void delete(Faults faults, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = faultsLogic.delete(faults);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * update
	 * @Description: 查找特定监控视图
	 * @param @param monitorView
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "信息修改")
	public void update(Faults faults, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = faultsLogic.update(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * 查询所有故障（带分页）
	 * @Description: 故障列表
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障信息", methods = "故障列表")
	public void list(int index, int size, String language, String level, String key, String libraryCode,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Page<Faults> list = faultsLogic.listByLibCode(index, size, getUserInfo(request).get(OEM_CODE), language, level,
				key, libraryCode);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	/**
	 * select
	 * @Description: 根据code或id查询相应故障
	 * @param @param equipment
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(Faults faults, HttpServletRequest request, HttpServletResponse response, Model model)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (faults.getOtherLanguage() != null && faults.getOtherLanguage() != "") {
			faults.setLanguage(faults.getOtherLanguage());
		}
		Faults result = faultsLogic.select(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}
	@RequestMapping(value = "/faultCheck")
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "信息修改")
	public void faultCheck(Faults faults, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = faultsLogic.update(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "添加其他语言")
	public void saveOtherLanguage(Faults faults, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		// 获取oemCode
		faults.setLanguage(faults.getOtherLanguage());
		int result = faultsLogic.saveOtherLanguage(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障管理", methods = "更新其他语言")
	public void updateOtherLanguage(Faults faults, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		// 获取oemCode
		faults.setLanguage(faults.getOtherLanguage());
		int result = faultsLogic.updateOtherLanguage(faults);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * 故障码导入 faultCodeImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @param faultTranslationLibraries
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "故障码管理", methods = "故障码导入")
	public void faultCodeImport(String data, Faults faults, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		Map<String, Object> map = new HashMap<String, Object>();
		int returnCode = faultsLogic.faultCodeImport(data, faults);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
