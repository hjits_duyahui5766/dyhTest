package com.cloudinnov.utils;

import org.apache.commons.lang.RandomStringUtils;

import com.cloudinnov.utils.support.spring.SpringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author chenning
 * @date 2016年8月5日下午3:25:57
 * @email
 * @remark 生成code的工具类
 * @version
 */
public class CodeUtil {
	
	private static JedisPool Jpool = SpringUtils.getBean("jedisPool");
	private static Jedis redisClient;
	// 随机数模板
	private static String template = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
	private static String codeTemplate = "abcdefghjklmnpqrstuvwxyz123456789";

	/** 生成随机字符串加数字
	 * @Title: generateRandom 
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String generateRandom(int digit) {
		String generateCode = "";
		String[] data = new String[digit];
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			generateCode = generateCode + data[i];
		}
		return generateCode;
	}
	public static String customerCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String customerCode = "CU";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			customerCode = customerCode + data[i];
		}
		Long result = redisClient.sadd("customer", customerCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return customerCode;
	}

	public static String equipmentCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentCode = "EQ";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentCode = equipmentCode + data[i];
		}
		Long result = redisClient.sadd("equipment", equipmentCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentCode;
	}

	public static String faultWorkorderCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String faultWorkorderCode = "WO";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			faultWorkorderCode = faultWorkorderCode + data[i];
		}
		Long result = redisClient.sadd("faultWorkorder", faultWorkorderCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return faultWorkorderCode;
	}
	
	public static String accidentWorkOrderCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String accidentWorkOrderCode = "AWF";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			accidentWorkOrderCode = accidentWorkOrderCode + data[i];
		}
		Long result = redisClient.sadd("accidentWorkOrderCode", accidentWorkOrderCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return accidentWorkOrderCode;
	}

	public static String equipmentBomCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentBomCode = "WO";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentBomCode = equipmentBomCode + data[i];
		}
		Long result = redisClient.sadd("equipmentBom", equipmentBomCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentBomCode;
	}

	public static String pointCode(int digit, String type, String kind) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String pointCode = "";
		/*
		 * if(type.equals(CommonUtils.POINT_TYPE_MN)){
		 * if(kind.equals("dl")||kind.equals("DL")){ pointCode = "ADL"; }else
		 * if(kind.equals("dy")||kind.equals("DY")) { pointCode = "ADY"; }else
		 * if(kind.equals("cl")||kind.equals("CL")) { pointCode = "ACL"; }else
		 * if
		 * (kind.equals("qt")||kind.equals("QT")||kind.equals("")||kind==null) {
		 * pointCode = "AQT"; } }else if
		 * (type.equals(CommonUtils.POINT_TYPE_SZ)) { pointCode = "DSZ"; }
		 */
		if (type.equals(CommonUtils.POINT_TYPE_MN)) {
			pointCode = "A";
		} else if (type.equals(CommonUtils.POINT_TYPE_SZ)) {
			pointCode = "D";
		} else {
			return pointCode;
		}
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, codeTemplate);
			pointCode = pointCode + data[i];
		}
		Long result = redisClient.sadd("point", pointCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return pointCode;
	}

	public static String pointCode(int digit, String type) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String pointCode = "";
		if (type.equals(CommonUtils.POINT_TYPE_MN)) {
			pointCode = "A";
		} else if (type.equals(CommonUtils.POINT_TYPE_SZ)) {
			pointCode = "D";
		} else {
			return pointCode;
		}
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, codeTemplate);
			pointCode = pointCode + data[i];
		}
		Long result = redisClient.sadd("point", pointCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return pointCode;
	}

	public static String equipmentCateCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentCateCode = "EC";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentCateCode = equipmentCateCode + data[i];
		}
		Long result = redisClient.sadd("equipmentCate", equipmentCateCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentCateCode;
	}

	public static String equipmentCateAttrCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentCateAttrCode = "ECA";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentCateAttrCode = equipmentCateAttrCode + data[i];
		}
		Long result = redisClient.sadd("equipmentCate", equipmentCateAttrCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentCateAttrCode;
	}

	public static String faultTranslateLibraryCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String faultTranslateLibraryCode = "TL";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			faultTranslateLibraryCode = faultTranslateLibraryCode + data[i];
		}
		Long result = redisClient.sadd("faultTranslateLibrary", faultTranslateLibraryCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return faultTranslateLibraryCode;
	}

	public static String faultCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String faultCode = "FA";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			faultCode = faultCode + data[i];
		}
		Long result = redisClient.sadd("fault", faultCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return faultCode;
	}

	public static String equipmentFaultChannelCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentFaultChannelCode = "FC";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentFaultChannelCode = equipmentFaultChannelCode + data[i];
		}
		Long result = redisClient.sadd("equipmentFaultChannel", equipmentFaultChannelCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentFaultChannelCode;
	}

	public static String prolineCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String prolineCode = "PL";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			prolineCode = prolineCode + data[i];
		}
		Long result = redisClient.sadd("proline", prolineCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return prolineCode;
	}

	public static String monitorCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String monitorCode = "MO";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			monitorCode = monitorCode + data[i];
		}
		Long result = redisClient.sadd("monitor", monitorCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return monitorCode;
	}

	public static String userCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String userCode = "US";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			userCode = userCode + data[i];
		}
		Long result = redisClient.sadd("user", userCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return userCode;
	}

	public static String resCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String resCode = "RE";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			resCode = resCode + data[i];
		}
		Long result = redisClient.sadd("resources", resCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return resCode;
	}

	public static String roleCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String roleCode = "RO";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			roleCode = roleCode + data[i];
		}
		Long result = redisClient.sadd("role", roleCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return roleCode;
	}

	/**
	 * 原料code
	 * 
	 * @param digit
	 * @return
	 */
	public static String matCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String matCode = "MA";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			matCode = matCode + data[i];
		}
		Long result = redisClient.sadd("material", matCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return matCode;
	}

	/**
	 * 原料产物code
	 * 
	 * @param digit
	 * @return
	 */
	public static String mprCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String mprCode = "MP";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			mprCode = mprCode + data[i];
		}
		Long result = redisClient.sadd("materialProduction", mprCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return mprCode;
	}

	/**
	 * 工单code
	 * 
	 * @param digit
	 * @return
	 */
	public static String flmCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String flmCode = "FW";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			flmCode = flmCode + data[i];
		}
		Long result = redisClient.sadd("flm", flmCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return flmCode;
	}

	/**
	 * 求助工单code
	 * 
	 * @param digit
	 * @return
	 */
	public static String fhmCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String fhmCode = "FH";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			fhmCode = fhmCode + data[i];
		}
		Long result = redisClient.sadd("fhm", fhmCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return fhmCode;
	}

	/**
	 * 客户原料code
	 * 
	 * @param digit
	 * @return
	 */
	public static String cmCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String cmCode = "CM";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			cmCode = cmCode + data[i];
		}
		Long result = redisClient.sadd("cm", cmCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return cmCode;
	}

	/**
	 * 客户产物code
	 * 
	 * @param digit
	 * @return
	 */
	public static String cpCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String cpCode = "CP";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			cpCode = cpCode + data[i];
		}
		Long result = redisClient.sadd("cp", cpCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return cpCode;
	}

	public static String areaCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String areaCode = "AR";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			areaCode = areaCode + data[i];
		}
		Long result = redisClient.sadd("area", areaCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return areaCode;
	}

	public static String parameterCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String parameterCode = "PA";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			parameterCode = parameterCode + data[i];
		}
		Long result = redisClient.sadd("parameter", parameterCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return parameterCode;
	}

	// 巡检配置
	public static String inspectionConfigCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String inspectionConfigCode = "IC";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			inspectionConfigCode = inspectionConfigCode + data[i];
		}
		Long result = redisClient.sadd("inspectionConfig", inspectionConfigCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return inspectionConfigCode;
	}

	// 巡检工作项
	public static String inspectionWorkItemsCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String inspectionWorkItemCode = "IWI";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			inspectionWorkItemCode = inspectionWorkItemCode + data[i];
		}
		Long result = redisClient.sadd("inspectionWorkItem", inspectionWorkItemCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return inspectionWorkItemCode;
	}

	// 巡检工单
	public static String inspectionWorkOrderCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String inspectionWorkOrderCode = "IWO";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			inspectionWorkOrderCode = inspectionWorkOrderCode + data[i];
		}
		Long result = redisClient.sadd("inspectionWorkOrder", inspectionWorkOrderCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return inspectionWorkOrderCode;
	}

	// 巡检结果
	public static String inspectionResultCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String inspectionResultCode = "IRU";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			inspectionResultCode = inspectionResultCode + data[i];
		}
		Long result = redisClient.sadd("inspectionResult", inspectionResultCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return inspectionResultCode;
	}

	public static String equipmentModelCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String equipmentModelCode = "EM";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			equipmentModelCode = equipmentModelCode + data[i];
		}
		Long result = redisClient.sadd("equipmentModel", equipmentModelCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return equipmentModelCode;
	}

	public static String sysIndexTypeCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String sysIndexTypeCode = "SIT";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			sysIndexTypeCode = sysIndexTypeCode + data[i];
		}
		Long result = redisClient.sadd("sysIndexType", sysIndexTypeCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return sysIndexTypeCode;
	}

	public static String sysIndustryCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String sysIndustryCode = "SIN";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			sysIndustryCode = sysIndustryCode + data[i];
		}
		Long result = redisClient.sadd("sysIndustry", sysIndustryCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return sysIndustryCode;
	}

	public static String communicationDeviceCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String sysIndexTypeCode = "CDE";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			sysIndexTypeCode = sysIndexTypeCode + data[i];
		}
		Long result = redisClient.sadd("communicationDevice", sysIndexTypeCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return sysIndexTypeCode;
	}

	public static String customerReportCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String customerReportCode = "CRP";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			customerReportCode = customerReportCode + data[i];
		}
		Long result = redisClient.sadd("customerReportCode", customerReportCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return customerReportCode;
	}

	public static String authDeptCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String authDeptCode = "ADP";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			authDeptCode = authDeptCode + data[i];
		}
		Long result = redisClient.sadd("authDeptCode", authDeptCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return authDeptCode;
	}

	/**
	 * 模块编码生成
	 * 
	 * @Title: authModuleCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String authModuleCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String authModuleCode = "AMU";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			authModuleCode = authModuleCode + data[i];
		}
		Long result = redisClient.sadd("authModuleCode", authModuleCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return authModuleCode;
	}

	/**
	 * 权限编码生成
	 * 
	 * @Title: authRightCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String authRightCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String authRightCode = "ART";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			authRightCode = authRightCode + data[i];
		}
		Long result = redisClient.sadd("authRightCode", authRightCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return authRightCode;
	}

	
	/** 告警规则编码生成
	 * @Title: authAlarmRuleCode 
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String alarmRuleCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String alarmRuleCode = "AR";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			alarmRuleCode = alarmRuleCode + data[i];
		}
		Long result = redisClient.sadd("alarmRuleCode", alarmRuleCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return alarmRuleCode;
	}
	
	/** 告警规则点位配置编码生成
	 * @Title: authAlarmRuleCode 
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String alarmRulePointCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String alarmRulePointCode = "ARP";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			alarmRulePointCode = alarmRulePointCode + data[i];
		}
		Long result = redisClient.sadd("alarmRulePointCode", alarmRulePointCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return alarmRulePointCode;
	}
	
	/**
	 * 触发器编码生成
	 * 
	 * @Title: tiggerCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String tiggerCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String tiggerCode = "TG";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			tiggerCode = tiggerCode + data[i];
		}
		Long result = redisClient.sadd("tiggerCode", tiggerCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return tiggerCode;
	}

	/**
	 * 群控方案编码生成
	 * 
	 * @Title: tiggerCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String controlSolutionCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String controlSolutionCode = "CST";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			controlSolutionCode = controlSolutionCode + data[i];
		}
		Long result = redisClient.sadd("controlSolutionCode", controlSolutionCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return controlSolutionCode;
	}
	/**
	 * 群控方案表达式编码生成
	 * 
	 * @Title: tiggerCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String controlSolutionExpressionCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String controlSolutionExpressionCode = "CSE";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			controlSolutionExpressionCode = controlSolutionExpressionCode + data[i];
		}
		Long result = redisClient.sadd("controlSolutionExpressionCode", controlSolutionExpressionCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return controlSolutionExpressionCode;
	}
	/**
	 * 工单编码生成
	 * 
	 * @Title: accidentWorkOrderCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String workOrderCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String controlSolutionCode = "WK";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			controlSolutionCode = controlSolutionCode + data[i];
		}
		Long result = redisClient.sadd("controlSolutionCode", controlSolutionCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return controlSolutionCode;
	}
	
	/**
	 * 大屏控制编码
	 * 
	 * @Title: accidentWorkOrderCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String creeenController(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String controlSolutionCode = "CR";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			controlSolutionCode = controlSolutionCode + data[i];
		}
		Long result = redisClient.sadd("controlSolutionCode", controlSolutionCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return controlSolutionCode;
	}
	
	/**
	 * 情报板模板编码
	 * 
	 * @Title: accidentWorkOrderCode
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String boardTemplateCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String boardTemplateCode = "BT";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			boardTemplateCode = boardTemplateCode + data[i];
		}
		Long result = redisClient.sadd("boardTemplateCode", boardTemplateCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return boardTemplateCode;
	}
	
	/** 大屏编码
	 * @Title: screenCode 
	 * @Description: TODO
	 * @param digit
	 * @return
	 * @return: String
	 */
	public static String screenCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String screenCode = "SCR";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			screenCode = screenCode + data[i];
		}
		Long result = redisClient.sadd("screenCode", screenCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return screenCode;
	}
	
	/**
	 * 紧急联系人code
	 * @param digit
	 * @return
	 */
	public static String contacterCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String contacterCode = "CC";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			contacterCode = contacterCode + data[i];
		}
		Long result = redisClient.sadd("contacterCode", contacterCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return contacterCode;
	}
	
	/**
	 * 预案记录code
	 * @param digit
	 * @return
	 */
	public static String predictionLogCode(int digit) {
		redisClient = Jpool.getResource();
		String[] data = new String[digit];
		String predictionLogCode = "PLC";
		for (int i = 0; i < digit; i++) {
			data[i] = RandomStringUtils.random(1, template);
			predictionLogCode = predictionLogCode + data[i];
		}
		Long result = redisClient.sadd("predictionLogCode", predictionLogCode);
		Jpool.returnResource(redisClient);
		if (result == 0) {
			return "";
		}
		return predictionLogCode;
	}
	
}
