package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.CustomReportIndexLogic;
import com.cloudinnov.model.CustomReportIndex;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/customReportIndex")
public class CustomReportIndexController extends BaseController {

	@Autowired
	private CustomReportIndexLogic customReportIndexLogic;

	/**
	 * 报表指标配置-添加
	 * 
	 * @param customReportIndex
	 *            参数
	 * @param response
	 * @param request
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表指标配置", methods = "添加")
	public void save(CustomReportIndex customReportIndex, HttpServletResponse response, HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportIndexLogic.save(customReportIndex);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 
	 * @param customReportIndex
	 *            参数
	 * @param response
	 * @param request
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表指标配置", methods = "删除")
	public void delete(CustomReportIndex customReportIndex, HttpServletResponse response, HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportIndexLogic.delete(customReportIndex);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表指标配置-修改
	 * 
	 * @param customReportIndex
	 *            参数
	 * @param response
	 * @param request
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表指标配置", methods = "修改")
	public void update(CustomReportIndex customReportIndex, HttpServletResponse response, HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportIndexLogic.update(customReportIndex);
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表指标配置-查询
	 * 
	 * @param customReportIndex
	 *            参数
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表指标配置", methods = "查询")
	public void listPage(CustomReportIndex customReportIndex, int index, int size, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException {
		Page<CustomReportIndex> customReportIndexs = customReportIndexLogic.selectListPage(index, size,
				customReportIndex, false);
		Map<String, Object> map = new HashMap<String, Object>();
		if (customReportIndexs != null) {
			map.put("list", customReportIndexs);
			map.put("total", customReportIndexs.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}
}
