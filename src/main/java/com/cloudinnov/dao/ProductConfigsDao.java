package com.cloudinnov.dao;

import com.cloudinnov.model.ProductConfigs;

/**
 * @author nilixin
 * @date 2016年2月24日上午11:54:01
 * @email 
 * @remark 产物配置 接口
 * @version
 */
public interface ProductConfigsDao extends IBaseDao<ProductConfigs>{

	public int insertInfo(ProductConfigs productConfigs);
	
	public int updateInfoByCondition(ProductConfigs productConfigs);
}