package com.cloudinnov.logic;

import com.cloudinnov.model.AuthRight;

public interface AuthRightLogic extends IBaseLogic<AuthRight> {
	
	public int saveOtherLanguage(AuthRight authRight);
	
	public int updateOtherLanguage(AuthRight authRight);
}
