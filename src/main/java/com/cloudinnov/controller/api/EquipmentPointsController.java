package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentPointLogic;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月18日上午9:28:08
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/equipmentPoint")
public class EquipmentPointsController extends BaseController {
	@Autowired
	private EquipmentPointLogic equipmentPointLogic;

	/**
	 * save
	 * @Description: 保存设备点位
	 * @param @param equipmentPoint
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备点位", methods = "信息保存")
	public void save(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		equipmentPoint.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = equipmentPointLogic.save(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result == SUCCESS) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "设备点位", methods = "保存其他语言")
	public void saveOtherLanguage(EquipmentPoints equipmentPoint, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		equipmentPoint.setLanguage(equipmentPoint.getOtherLanguage());
		int result = equipmentPointLogic.saveOtherLanguage(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备点位", methods = "更新其他语言")
	public void updateOtherLanguage(EquipmentPoints equipmentPoint, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		equipmentPoint.setLanguage(equipmentPoint.getOtherLanguage());
		int result = equipmentPointLogic.updateOtherLanguage(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * delete
	 * @Description: 根据点位的code 删除设备点位
	 * @param @param equipmentPointCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备点位", methods = "信息删除")
	public void delete(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = equipmentPointLogic.delete(equipmentPoint);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * update
	 * @Description: 更新设备点位
	 * @param @param equipmentPoint
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "设备点位", methods = "信息修改")
	public void update(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = equipmentPointLogic.update(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	/**
	 * list
	 * @Description: 查找所有设备点位
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentPoints equipmentPoint, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		Page<EquipmentPoints> equipmentPoints = null;
		if (request.getParameter("equipmentType") != null
				&& request.getParameter("equipmentType").equals("productionLine")) {
			equipmentPoints = equipmentPointLogic.selectProductionLineListPage(index, size, equipmentPoint, true);
		} else {
			equipmentPoints = equipmentPointLogic.selectListPage(index, size, equipmentPoint, true);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentPoints != null) {
			map.put("list", equipmentPoints);
			map.put("total", equipmentPoints.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/listPointsNoPage")
	@ResponseBody
	public void listPointsNoPage(EquipmentPoints equipmentPoint, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<EquipmentPoints> equipmentPoints = equipmentPointLogic.selectList(equipmentPoint, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentPoints != null) {
			map.put("list", equipmentPoints);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/listNoPage")
	@ResponseBody
	public void listNoPage(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		List<EquipmentPoints> equipmentPoints = equipmentPointLogic.selectList(equipmentPoint, false);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentPoints != null) {
			for (EquipmentPoints point : equipmentPoints) {
				point.setViewType(point.getMonitorType());
				if (point.getType().equals(CommonUtils.POINT_TYPE_MN)) {
					if (point.getConfig() != null && point.getConfig().split("\\|").length >= 4) {
						point.setMin(point.getConfig().split("\\|")[2]);
						point.setMax(point.getConfig().split("\\|")[4]);
					}
				} else if (point.getType().equals(CommonUtils.POINT_TYPE_SZ)) {
					String minDesc = point.getConfig().split("\\|")[0];
					String maxDesc = point.getConfig().split("\\|")[2];
					point.setMinDesc(minDesc != null ? minDesc : "");
					point.setMaxDesc(maxDesc != null ? maxDesc : "");
					point.setUnit("");
					point.setViewType(CommonUtils.POINT_VIEW_TYPE_PANEL);
				}
			}
			map.put("list", equipmentPoints);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/monitorEqusPoints")
	@ResponseBody
	public void monitorEqusPoints(String equipmentCodes, String language, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String[] codes = equipmentCodes.split(",");
		List<EquipmentPoints> equipmentPoints = equipmentPointLogic.monitorEqusPoints(codes, language);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentPoints != null) {
			map.put("list", equipmentPoints);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * select
	 * @Description: 根据code查找设备点位
	 * @param @param equipmentPoint
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (equipmentPoint.getOtherLanguage() != null && equipmentPoint.getOtherLanguage() != "") {
			equipmentPoint.setLanguage(equipmentPoint.getOtherLanguage());
		}
		EquipmentPoints result = equipmentPointLogic.select(equipmentPoint);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(String codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (codes != null) {
			String[] codesTem = codes.split(",");
			List<EquipmentPoints> equipmentPoints = equipmentPointLogic.listByCode(codesTem, language);
			if (equipmentPoints != null) {
				for (EquipmentPoints point : equipmentPoints) {
					point.setViewType(point.getMonitorType());
					if (point.getType().equals(CommonUtils.POINT_TYPE_MN)) {
						if (point.getConfig() != null && point.getConfig().split("\\|").length >= 4) {
							point.setMin(point.getConfig().split("\\|")[2]);
							point.setMax(point.getConfig().split("\\|")[4]);
						}
					} else if (point.getType().equals(CommonUtils.POINT_TYPE_SZ)) {
						String minDesc = point.getConfig().split("\\|")[0];
						String maxDesc = point.getConfig().split("\\|")[2];
						point.setMinDesc(minDesc != null ? minDesc : "");
						point.setMaxDesc(maxDesc != null ? maxDesc : "");
						point.setUnit("");
						point.setViewType(CommonUtils.POINT_VIEW_TYPE_PANEL);
					}
				}
				map.put("list", equipmentPoints);
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			}
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 点位导入 pointImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @param equipmentPoints
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备点位导入")
	public void pointImport(String data, EquipmentPoints equipmentPoints, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		Map<String, Object> map = new HashMap<String, Object>();
		if (!CommonUtils.isNotEmpty(data)) {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		} else {
			int returnCode = equipmentPointLogic.pointImport(data, equipmentPoints);
			if (returnCode == CommonUtils.SUCCESS_NUM) {
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 点位导出 pointImport
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param data
	 * @param @param equipmentPoints
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/export")
	@ResponseBody
	@SystemLog(module = "设备管理", methods = "设备点位导出")
	public void pointExport(EquipmentPoints equipmentPoint, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		equipmentPointLogic.pointExport(equipmentPoint, response);
	}
}
