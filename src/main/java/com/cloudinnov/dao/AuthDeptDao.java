package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AuthDept;

public interface AuthDeptDao extends IBaseDao<AuthDept>{

    int insertInfo(AuthDept authDept);

    int updateInfoByCondition(AuthDept authDept);
    
    AuthDept selectEntityByparentId(AuthDept authDept);
    
    public List<AuthDept> tableList(Map<String, Object> map);
}