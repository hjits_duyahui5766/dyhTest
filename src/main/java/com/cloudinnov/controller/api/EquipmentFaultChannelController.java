package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentFaultChannelLogic;
import com.cloudinnov.model.EquipmentFaultChannel;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月18日上午9:04:11
 * @email chaoguo@cloudinnov.com
 * @remark 设备故障通道controller
 * @version
 */
@Controller
@RequestMapping("/webapi/equipmentFaultChannel")
public class EquipmentFaultChannelController extends BaseController {

	@Autowired
	private EquipmentFaultChannelLogic equipmentFaultChannelLogic;

	/**
	 * save
	 * 
	 * @Description: 保存设备故障通道
	 * @param @param equipmentFaultChannel
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备故障通道", methods = "信息保存")
	public void save(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		equipmentFaultChannel.setOemCode(getUserInfo(request).get(OEM_CODE));
		int result = equipmentFaultChannelLogic.save(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map,
				result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "设备故障通道", methods = "保存其他语言")
	public void saveOtherLanguage(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		equipmentFaultChannel.setLanguage(equipmentFaultChannel.getOtherLanguage());
		int result = equipmentFaultChannelLogic
				.saveOtherLanguage(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备故障通道", methods = "修改其他语言")
	public void updateOtherLanguage(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		equipmentFaultChannel.setLanguage(equipmentFaultChannel.getOtherLanguage());
		int result = equipmentFaultChannelLogic.updateOtherLanguage(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map,
				result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据故障通道的code 删除设备故障通道
	 * @param @param equipmentFaultChannelCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "设备故障通道", methods = "信息删除")
	public void delete(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws JsonGenerationException, JsonMappingException,
			IOException {
		int result = equipmentFaultChannelLogic.delete(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map,
				result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 更新设备故障通道
	 * @param @param equipmentFaultChannel
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "设备故障通道", methods = "信息修改")
	public void update(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws JsonGenerationException, JsonMappingException,
			IOException {
		int result = equipmentFaultChannelLogic.update(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map,
				result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 查找所有设备bom
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentFaultChannel efc,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Page<EquipmentFaultChannel> equipmentFaultChannel = equipmentFaultChannelLogic
				.selectListPage(index, size, efc, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentFaultChannel != null) {
			map.put("list", equipmentFaultChannel);
			map.put("total", equipmentFaultChannel.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找设备bom
	 * @param @param equipmentBomCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentFaultChannel equipmentFaultChannel,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (equipmentFaultChannel.getOtherLanguage() != null
				&&equipmentFaultChannel.getOtherLanguage() != "") {
			equipmentFaultChannel.setLanguage(equipmentFaultChannel.getOtherLanguage());
		}
		EquipmentFaultChannel result = equipmentFaultChannelLogic
				.select(equipmentFaultChannel);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
