package com.cloudinnov.model;

public class RealTimeData extends BaseModel{

    private String value;

    private long time;

    public RealTimeData(String value, long time) {
		this.value = value;
		this.time = time;
	}
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

    
}