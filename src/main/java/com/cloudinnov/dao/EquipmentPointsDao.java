package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.EquipmentPoints;

/**
 * @author guochao
 * @date 2016年2月17日下午5:15:58
 * @email chaoguo@cloudinnov.com
 * @remark 设备点位dao 接口
 * @version
 */
public interface EquipmentPointsDao extends IBaseDao<EquipmentPoints> {
	List<EquipmentPoints> selectSimulationAllPoint();
	public List<EquipmentPoints> listByCode(Map<String, Object> map);
	public int insertInfo(EquipmentPoints equipmentsPoints);
	public int updateInfoByCondition(EquipmentPoints equipmentsPoints);
	public List<EquipmentPoints> selectProductionLineListPage(EquipmentPoints equipmentsPoints);
	public List<EquipmentPoints> monitorEqusPoints(Map<String, Object> map);
	/**
	 * 查询所有产能点位 isProductivityPoint
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @return 参数
	 * @return List<EquipmentPoints> 返回类型
	 */
	public List<EquipmentPoints> selectProductivityPoint();
	/**
	 * 查询所有的设备状态点位 selectEquipmentStatePoints
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param equipmentsPoints
	 * @param @return 参数
	 * @return List<EquipmentPoints> 返回类型
	 */
	public List<EquipmentPoints> selectEquipmentStatePoints(EquipmentPoints equipmentsPoints);
	/**
	 * 根据指标类型获取点位列表
	 * @Title: selectListByIndexType
	 * @Description: TODO
	 * @param equipmentsPoints
	 * @return
	 * @return: List<EquipmentPoints>
	 */
	public List<EquipmentPoints> selectListByIndexType(EquipmentPoints equipmentsPoints);
	/**
	 * 批量插入
	 * @Title: insertBatch
	 * @Description: TODO
	 * @param params
	 * @return
	 * @return: int
	 */
	public int insertBatch(Map<String, Object> params);
	/**
	 * 根据点位编码查询点位设备数据
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param pointCode
	 * @return 参数
	 * @return EquipmentPoints 返回类型
	 */
	EquipmentPoints selectPointEquDataByPointCode(@Param("code") String pointCode);
	/**
	 * 根据设备编码查询设备控制点位信息
	 * @param string
	 * @return
	 */
	List<EquipmentPoints> selectPointsByEquCode(String equipmentCode);
	/**
	 * 根据设备编码查询设备反馈点位信息
	 * @param code
	 * @return
	 */
	List<EquipmentPoints> selectPointsByEquipmentCode(String code);
	/**
	 * @param equCode
	 * @return
	 */
	List<EquipmentPoints> selectPointsByEquipCode(String equCode);
	
	/** 
	* 根据设备编码和是否为反馈点查询设备下的点位列表 
	* @param equCode
	* @param isFeedback
	* @return    参数
	* @return List<EquipmentPoints>    返回类型 
	*/
	List<EquipmentPoints> selectPointsByEquCodeAndFeedback(@Param("equipmentCode") String equCode, @Param("isFeedback") int isFeedback);
	
	EquipmentPoints selectCDZSQIsLeftByEquipmentCode(@Param("equipCode") String equipCode);
}
