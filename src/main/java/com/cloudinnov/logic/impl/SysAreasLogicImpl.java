package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysAreasDao;
import com.cloudinnov.logic.SysAreasLogic;
import com.cloudinnov.model.SysAreas;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月18日下午4:07:12
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class SysAreasLogicImpl extends BaseLogicImpl<SysAreas> implements SysAreasLogic {

	private int digit = 5;
	@Autowired
	private SysAreasDao sysAreasDao;

	public int save(SysAreas sysAreas) {
		sysAreas.setCode(CodeUtil.areaCode(digit));
		sysAreas.setStatus(CommonUtils.STATUS_NORMAL);
		int result = sysAreasDao.insert(sysAreas);
		if (result == 1) {
			result = sysAreasDao.insertInfo(sysAreas);
		}
		return result;
	}

	@Override
	public List<SysAreas> tableList(String[] codes, String language) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("codes", codes);
		return sysAreasDao.tableList(map);
	}

	@Override
	public int saveOtherLanguage(SysAreas sysAreas) {
		return sysAreasDao.insertInfo(sysAreas);
	}

	@Override
	public int updateOtherLanguage(SysAreas sysAreas) {
		return sysAreasDao.updateByCondition(sysAreas);
	}

	@Override
	public List<SysAreas> phoneList(SysAreas sysAreas) {
		return sysAreasDao.phoneList(sysAreas);
	}

	@Override
	public List<SysAreas> search(SysAreas sysAreas) {
		Map<String, Object> map = new HashMap<>();
		map.put("language", sysAreas.getLanguage());
		if(sysAreas.getParentIds()!=null){
			map.put("parentIds", sysAreas.getParentIds().split(","));
		}
		return sysAreasDao.search(map);
	}

}
