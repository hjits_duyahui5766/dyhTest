package com.cloudinnov.model;

import java.util.List;
import java.util.Map;

public class ControlSolutionSendRecordItem extends BaseModel {
    private String solutionCode;
    private String solutionName;
    private String equipmentCode;
    private String equipmentName;
    private String pileNo;
    private String cateCode;
    private String cateName;
    private String sendContent;
    private Integer sendStatus = 1;
    private Integer solutionType;
    private String sendTime;
    private String configContent;
    private String operationUser;
    private Map<String, Object> config;
    private List<Map<String, Object>> programList;
    private String[] equipmentCodes;
    private String sectionCode;
    private String sectionName;

    public String getEquipmentCode() {
        return equipmentCode;
    }
    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }
    public String getSendContent() {
        return sendContent;
    }
    public void setSendContent(String sendContent) {
        this.sendContent = sendContent;
    }
    public Integer getSendStatus() {
        return sendStatus;
    }
    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }
    public String getSolutionCode() {
        return solutionCode;
    }
    public void setSolutionCode(String solutionCode) {
        this.solutionCode = solutionCode;
    }
    public String getSolutionName() {
        return solutionName;
    }
    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }
    public Integer getSolutionType() {
        return solutionType;
    }
    public void setSolutionType(Integer solutionType) {
        this.solutionType = solutionType;
    }
    public String getSendTime() {
        return sendTime;
    }
    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
    public String getConfigContent() {
        return configContent;
    }
    public void setConfigContent(String configContent) {
        this.configContent = configContent;
    }
    public String getOperationUser() {
        return operationUser;
    }
    public void setOperationUser(String operationUser) {
        this.operationUser = operationUser;
    }
    public String getEquipmentName() {
        return equipmentName;
    }
    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }
    public String getPileNo() {
        return pileNo;
    }
    public void setPileNo(String pileNo) {
        this.pileNo = pileNo;
    }
    public String getCateName() {
        return cateName;
    }
    public void setCateName(String cateName) {
        this.cateName = cateName;
    }
    public String getCateCode() {
        return cateCode;
    }
    public void setCateCode(String cateCode) {
        this.cateCode = cateCode;
    }
    public List<Map<String, Object>> getProgramList() {
        return programList;
    }
    public void setProgramList(List<Map<String, Object>> programList) {
        this.programList = programList;
    }
    public Map<String, Object> getConfig() {
        return config;
    }
    public void setConfig(Map<String, Object> config) {
        this.config = config;
    }
    public String[] getEquipmentCodes() {
        return equipmentCodes;
    }
    public void setEquipmentCodes(String[] equipmentCodes) {
        this.equipmentCodes = equipmentCodes;
    }
    public String getSectionCode() {
        return sectionCode;
    }
    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }
    public String getSectionName() {
        return sectionName;
    }
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
