package com.cloudinnov.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.model.CarDetectorData.CarDetectorDataChilren;
import com.cloudinnov.utils.CommonUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/applicationContext.xml" })
public class Junit {
	 
	@Autowired
	private CarDetectorLogic carDetectorLogic;
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	JedisPool jedisPool;
 
	@Test
	public  void redis() {
		 
	}
	
	 
}
