package com.cloudinnov.logic;

import com.cloudinnov.model.AccidentWorkOrder;
import com.cloudinnov.model.WorkOrder;
import com.github.pagehelper.Page;

public interface WorkOrderLogic extends IBaseLogic<WorkOrder> {

	int accidentWorkOrderSave(WorkOrder workOrder);

	Page<WorkOrder> selectWorkOrderList(WorkOrder workOrder, int index, int size);

	AccidentWorkOrder selectWorkOrderInfo(AccidentWorkOrder accidentWorkOrder);

	int deleteWorkOrder(WorkOrder workOrder);

	int updateWorkOrder(WorkOrder workOrder);

	Page<AccidentWorkOrder> selectAccidentWorkOrderInfo(AccidentWorkOrder accidentWorkOrder, int index, int size);


}
