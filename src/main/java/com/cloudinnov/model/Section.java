package com.cloudinnov.model;

import java.util.List;

public class Section extends BaseModel {
	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String name;
	private String province;
	private String city;
	private String address;
	private String location;
	private Integer leftLength;
	private Integer rightLength;
	private String leftStartPile;// 左隧道开始桩号
	private String leftEndPile;// 左隧道结束桩号
	private String rightStartPile;// 右隧道开始桩号
	private String rightEndPile;// 右隧道结束桩号
	private String startPile;
	private String endPile;
	private String sectionCode;
	private List<Equipments> sectionEquipments;//为了电表而设计
	
	public List<Equipments> getSectionEquipments() {
		return sectionEquipments;
	}
	public void setSectionEquipments(List<Equipments> sectionEquipments) {
		this.sectionEquipments = sectionEquipments;
	}
	public String getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}
	public Integer getLeftLength() {
		return leftLength;
	}
	public void setLeftLength(Integer leftLength) {
		this.leftLength = leftLength;
	}
	public Integer getRightLength() {
		return rightLength;
	}
	public void setRightLength(Integer rightLength) {
		this.rightLength = rightLength;
	}
	public String getStartPile() {
		return startPile;
	}
	public void setStartPile(String startPile) {
		this.startPile = startPile;
	}
	public String getEndPile() {
		return endPile;
	}
	public void setEndPile(String endPile) {
		this.endPile = endPile;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLeftStartPile() {
		return leftStartPile;
	}
	public void setLeftStartPile(String leftStartPile) {
		this.leftStartPile = leftStartPile;
	}
	public String getLeftEndPile() {
		return leftEndPile;
	}
	public void setLeftEndPile(String leftEndPile) {
		this.leftEndPile = leftEndPile;
	}
	public String getRightStartPile() {
		return rightStartPile;
	}
	public void setRightStartPile(String rightStartPile) {
		this.rightStartPile = rightStartPile;
	}
	public String getRightEndPile() {
		return rightEndPile;
	}
	public void setRightEndPile(String rightEndPile) {
		this.rightEndPile = rightEndPile;
	}
	@Override
	public String toString() {
		return "Section [type=" + type + ", name=" + name + ", province=" + province + ", city=" + city + ", address="
				+ address + ", location=" + location + ", leftLength=" + leftLength + ", rightLength=" + rightLength
				+ ", leftStartPile=" + leftStartPile + ", leftEndPile=" + leftEndPile + ", rightStartPile="
				+ rightStartPile + ", rightEndPile=" + rightEndPile + ", startPile=" + startPile + ", endPile="
				+ endPile + ", sectionCode=" + sectionCode + "]";
	}
}
