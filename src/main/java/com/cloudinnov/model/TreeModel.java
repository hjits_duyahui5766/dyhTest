package com.cloudinnov.model;

import java.util.List;

/**
 * @author guochao
 * @date 2016年3月21日上午9:48:00
 * @email chaoguo@cloudinnov.com
 * @remark 用于前台展示 属性可修改
 * @version
 */
public class TreeModel {

	private Integer indexId;
	
	private String code;
	
	private Integer parentId;
	
	private String parentCode;
	
	private String name;
	
	private String url;

	private String icon;
	
	private String description;
	
	private List<TreeModel> children;

	public Integer getIndexId() {
		return indexId;
	}

	public void setIndexId(Integer indexId) {
		this.indexId = indexId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public List<TreeModel> getChildren() {
		return children;
	}

	public void setChildren(List<TreeModel> children) {
		this.children = children;
	}
	
}
