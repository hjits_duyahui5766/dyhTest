package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.MonitorViews;

public interface MonitorViewsDao extends IBaseDao<MonitorViews> {
	
	public List<MonitorViews> search(Map<String, String> map);
	
	public int insertInfo(MonitorViews monitorViews);
	
	public int updateInfoByCondition(MonitorViews monitorViews);
}