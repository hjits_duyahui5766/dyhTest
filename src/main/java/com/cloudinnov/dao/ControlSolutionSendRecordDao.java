package com.cloudinnov.dao;

import com.cloudinnov.model.ControlSolutionSendRecord;

public interface ControlSolutionSendRecordDao extends IBaseDao<ControlSolutionSendRecord>{
  
}