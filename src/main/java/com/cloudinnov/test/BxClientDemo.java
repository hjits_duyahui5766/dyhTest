package com.cloudinnov.test;

import java.awt.Font;
import java.util.ArrayList;

import onbon.bx05.Bx5GEnv;
import onbon.bx05.Bx5GException;
import onbon.bx05.Bx5GScreen;
import onbon.bx05.Bx5GScreenClient;
import onbon.bx05.area.TextCaptionBxArea;
import onbon.bx05.area.page.TextBxPage;
import onbon.bx05.file.BxFileWriterListener;
import onbon.bx05.file.ProgramBxFile;
import onbon.bx05.utils.DisplayStyleFactory;
import onbon.bx05.utils.DisplayStyleFactory.DisplayStyle;
import onbon.bx05.utils.TextBinary;


public class BxClientDemo implements BxFileWriterListener<Bx5GScreen>{
	public static void main(String[] args) throws Exception {

        //
        // SDK 初始化
        //Bx5GEnv.initial("log.properties");
        Bx5GEnv.initial("log.properties", 30000);
        
        Bx5GScreenClient screen = new Bx5GScreenClient("MyScreenName");
        screen.connect("192.168.199.125", 5005);

        screen.ping();
        screen.checkControllerStatus();
        screen.checkFirmware();
        screen.checkMemVolumes();
        screen.syncTime();
        screen.readControllerId();
        screen.turnOn();
        screen.lock();
        screen.unlock();
        
        DisplayStyle[] styles = DisplayStyleFactory.getStyles().toArray(new DisplayStyle[0]);
        TextCaptionBxArea area = new TextCaptionBxArea(0, 0, 160, 32, screen.getProfile());

        ProgramBxFile p1 = new ProgramBxFile("P001", screen.getProfile());
        
        //p1.addPlayPeriodSetting(15, 59, 00, 16, 00, 00);

        TextBxPage page = new TextBxPage(new Font("宋体", Font.PLAIN, 32));
        page.setText("第一行文字111111111111");
        //page.newLine("第二行文字");
        page.setStayTime(0);
        page.setSpeed(5);
        // 调整特技方式
     		page.setDisplayStyle(styles[4]);
     		//page.setLineBreak(false);
        // 设置文本水平对齐方式
        page.setHorizontalAlignment(TextBinary.Alignment.NEAR);
        // 设置文本垂直居中方式
        page.setVerticalAlignment(TextBinary.Alignment.NEAR);
        area.addPage(page);

        p1.addArea(area);
        
        
        
       /* TextCaptionBxArea area2 = new TextCaptionBxArea(0, 0, 160, 32, screen.getProfile());

        ProgramBxFile p2 = new ProgramBxFile("P002", screen.getProfile());
        
        p2.addPlayPeriodSetting(16, 00, 03, 16, 01, 00);

        TextBxPage page2 = new TextBxPage(new Font("宋体", Font.PLAIN, 16));
        page2.newLine("第二行文字22222");
        //page.newLine("第二行文字");
       // page.setStayTime(200);
        // 设置文本水平对齐方式
        page2.setHorizontalAlignment(TextBinary.Alignment.NEAR);
        // 设置文本垂直居中方式
        page2.setVerticalAlignment(TextBinary.Alignment.NEAR);
        area2.addPage(page2);

        
        p2.addArea(area2);*/
        
        // 创建一个 arraylist
        ArrayList<ProgramBxFile> plist = new ArrayList<>();
        plist.add(p1);
        //plist.add(p2);
        screen.deletePrograms();
        
        
        screen.writePrograms(plist);

        screen.disconnect();
    }


    @Override
    public void fileWriting(Bx5GScreen bx6GScreen, String s, int i) {

    }

    @Override
    public void fileFinish(Bx5GScreen bx6GScreen, String s, int i) {

    }

    @Override
    public void progressChanged(Bx5GScreen bx6GScreen, String s, int i, int i1) {

    }

    @Override
    public void cancel(Bx5GScreen bx6GScreen, String s, Bx5GException e) {

    }

    @Override
    public void done(Bx5GScreen bx6GScreen) {

    }
}
