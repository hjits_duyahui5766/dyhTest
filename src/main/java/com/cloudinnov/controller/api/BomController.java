package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.BomLogic;
import com.cloudinnov.model.Bom;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.HttpUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年6月27日下午1:32:40
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Controller
@RequestMapping("/webapi/bom")
public class BomController extends BaseController{

	@Autowired
	private BomLogic bomLogic;
	
	/**
	 * save
	 * 
	 * @Description: 保存元件bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "信息保存")
	public void save(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		bom.setOemCode(getUserInfo(request).get(OEM_CODE));
		bom.setCiCode(getUserInfo(request).get(OEM_CODE)+"-"+CommonUtils.getUUID());
		int result = bomLogic.save(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage")
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "信息保存")
	public void saveOtherLanguage(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		bom.setLanguage(bom.getOtherLanguage());
		int result = bomLogic.saveOtherLanguage(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "添加其他语言")
	public void updateOtherLanguage(Bom bom, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		bom.setLanguage(bom.getOtherLanguage());
		int result = bomLogic.updateOtherLanguage(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据code 删除元件bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "信息删除")
	public void delete(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = bomLogic.delete(bom);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * update
	 * 
	 * @Description: 根据code或者id更新元件bom
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "元件信息修改")
	public void update(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = bomLogic.update(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 分页查找元件bom
	 * @param @param
	 *            index 当前页数
	 * @param @param
	 *            size 页面总数
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		bom.setOemCode(getUserInfo(request).get(OEM_CODE));
		Page<Bom> equipmentBoms = bomLogic.selectListPage(index, size, bom, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (equipmentBoms != null) {
			map.put("total", equipmentBoms.getTotal());
			map.put("list", equipmentBoms);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	@RequestMapping(value = "/listByCode")
	@ResponseBody
	public void listByCode(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		bom.setOemCode(getUserInfo(request).get(OEM_CODE));
		List<Bom> boms = bomLogic.listByCode(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (boms != null) {
			map.put("list", boms);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/**
	 * select
	 * 
	 * @Description: 根据code或者id 查找元件bom实体
	 * @param @param
	 *            equipmentBom
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(Bom bom, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (bom.getOtherLanguage() != null && bom.getOtherLanguage() != "") {
			bom.setLanguage(bom.getOtherLanguage());
		}
		Bom result = bomLogic.select(bom);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}

	/** bom导入
	* bomImport 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param data
	* @param @param equipmentPoints
	* @param @param request
	* @param @param response
	* @param @throws JsonGenerationException
	* @param @throws JsonMappingException
	* @param @throws IOException    参数
	* @return void    返回类型 
	*/
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "元件管理", methods = "元件Bom导入")
	public void bomImport(String data, Bom bom,
			HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String returnData = null;
		Map<String, Object> map = new HashMap<String, Object>();
		int returnCode = bomLogic.bomImport(data, bom);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,
					"");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	
	@RequestMapping(value = "/getoembrand")
	public void getOemBrand(Bom bom,HttpServletRequest request, HttpServletResponse response)
			throws IOException {	
		String returnData = null;
		Map<String, Object> map = new HashMap<String, Object>();
		String data = HttpUtil.doGet(CommonUtils.CI_PRODUCT_BOM +"test-oem-code", null,null);
		boolean isSuccess = false;
		if(data != null){
			ObjectMapper mapper = new ObjectMapper();  
			JsonNode node = mapper.readTree(data);
			if(node!=null&&node.size()>=0){
				int statusCode = node.get("code").asInt();
				boolean success = node.get("success").asBoolean();
				if(statusCode == 200 && success){
					map.put("list", node.get("dataList"));
					isSuccess = true;
				}
			}
		}
		if(isSuccess){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}		
		response.getWriter().print(returnData);
	}
	
}
