package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.AuthUserLogic;
import com.cloudinnov.logic.EquipmentInspectionWorkItemLogic;
import com.cloudinnov.logic.EquipmentInspectionWorkOrderLogic;
import com.cloudinnov.model.AuthUsers;
import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkItem;
import com.cloudinnov.model.EquipmentInspectionWorkOrder;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/inspection/workOrder")
public class EquipmentInspectionWorkOrderController extends BaseController {

	@Autowired
	private EquipmentInspectionWorkOrderLogic workOrderLogic;
	@Autowired
	private EquipmentInspectionWorkItemLogic workItemLogic;
	
	@Autowired
	private AuthUserLogic userLogic;

	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentInspectionWorkOrder eiwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<EquipmentInspectionWorkOrder> equipmentInspectionWorkOrders = workOrderLogic.list(index, size, eiwo);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentInspectionWorkOrders != null) {
			map.put("list", equipmentInspectionWorkOrders);
			map.put("total", equipmentInspectionWorkOrders.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/search")
	@ResponseBody
	public void serach(int index, int size, String key,EquipmentInspectionWorkOrder eiwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		Page<EquipmentInspectionWorkOrder> list = workOrderLogic.search(index, size, eiwo, key);
		
		map.put("alarmUntreateds", CommonUtils.DEFAULT_NUM);
		map.put("alarmHandles", CommonUtils.DEFAULT_NUM);
		map.put("alarmCloses", CommonUtils.DEFAULT_NUM);
		map.put("alarmSees", CommonUtils.DEFAULT_NUM);
		
		if(list.getResult()!=null && list.getResult().size()>0){
			for(int i = 0 ;i<list.size();i++){
				if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_NEW){
					map.put("alarmUntreateds", (Integer) map.get("alarmUntreateds") + 1);
				}else if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_HANDLE){
					map.put("alarmHandles", (Integer) map.get("alarmHandles") + 1);
				}else if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_CLOSE){
					map.put("alarmCloses", (Integer) map.get("alarmCloses") + 1);
				}/*else if(list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_READED){
					map.put("alarmSees", (Integer) map.get("alarmSees") + 1);
				}*/
			}
		}
		String returnData = "";	
		map.put("list", list);
		map.put("total", list.getTotal());
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	/**
	 * 根据configCode 查询所有工作项
	 * 
	 * @param eiwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/searchItemsByCode")
	@ResponseBody
	public void searchItemsByCode(int index, int size, EquipmentInspectionConfig entity, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<EquipmentInspectionWorkItem> items = workItemLogic.selectByConfigCode(index, size, entity);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (items != null) {
			map.put("list", items);
			map.put("total", items.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentInspectionWorkOrder eiwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		EquipmentInspectionWorkOrder equipmentInspectionWorkOrder = workOrderLogic.select(eiwo);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentInspectionWorkOrder != null) {
			map.put("model", equipmentInspectionWorkOrder);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 巡检工单转派
	 * 
	 * @param eiwo
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value = "/turnsend",method = RequestMethod.POST )
	@ResponseBody
	public void turnsend(EquipmentInspectionWorkOrder eiwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		int result = workOrderLogic.update(eiwo);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	/**
	 * 工单提醒
	 * 
	 * @param eiwo
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value = "/reminded")
	@ResponseBody
	public void reminded(EquipmentInspectionWorkOrder eiwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		
		eiwo.setPaddingBy(getUserInfo(request).get(USER_CODE));
		EquipmentInspectionWorkOrder model = workOrderLogic.select(eiwo);
		if(model!=null){
			AuthUsers user = new AuthUsers();
			user.setCode(model.getPaddingBy());
			user = userLogic.select(user);
			if(user!=null){
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			}else{
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
}
