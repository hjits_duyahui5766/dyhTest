package com.cloudinnov.logic.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.SysLogsDao;
import com.cloudinnov.logic.SysLogsLogic;
import com.cloudinnov.model.SysLogs;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author chengning
 * @date 2016年2月18日下午4:09:28
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class SysLogsLogicImpl extends BaseLogicImpl<SysLogs> implements SysLogsLogic {

	@Autowired
	private SysLogsDao sysLogDao;
	
	@Override
	public int saveLoginLog(SysLogs sysLogs, HttpServletRequest request) {
		sysLogs.setUserip(CommonUtils.toIpAddr(request));
		sysLogs.setCode(CommonUtils.getUUID());	
		sysLogs.setStatus(CommonUtils.STATUS_NORMAL);
		return sysLogDao.insert(sysLogs);
	}


}
