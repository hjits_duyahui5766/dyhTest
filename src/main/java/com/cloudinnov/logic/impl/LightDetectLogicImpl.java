package com.cloudinnov.logic.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.LightDetectDao;
import com.cloudinnov.logic.LightDetectLogic;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.LightDetect;
import com.cloudinnov.utils.CommonUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Service
public class LightDetectLogicImpl implements LightDetectLogic {

	static final Logger LOG = LoggerFactory.getLogger(CarDetectorLogicImpl.class);
	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static SimpleDateFormat sdfSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String KEY_EQUCODE = "equipCode";
	public static final String KEY_START_TIME = "startTime";
	public static final String KEY_END_TIME = "endTime";

	@Autowired
	LightDetectDao lightDetectDao;

	@Autowired
	private JedisPool jedisPool;

	@Autowired
	private MongoTemplate mongoTemplate;

	private final String KeyHeadString = "light:deviceId:";
	private final String KeyFootString = ":value";

	@Override
	public List<LightDetect> lightDetectIpInfo() {
		// TODO Auto-generated method stub
		return lightDetectDao.lightDetectIpInfo();
	}

	/**
	 * 获取能见度监测器的信息
	 */
	@Override
	public List<EquipmentsAttr> selectLightByEquipCode(String equipCode) {
		List<EquipmentsAttr> equipmentsAttrs = lightDetectDao.selectLightByEquipCode(equipCode);
		return equipmentsAttrs;
	}

	@Override
	public String getLightValue(String equipCode) {
		// TODO Auto-generated method stub
		Jedis redis = null;
		String lindex = null;
		try {
			redis = jedisPool.getResource();
			String key = KeyHeadString + equipCode + KeyFootString;// EQMG9BT
			lindex = redis.lindex(key, 0);

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			jedisPool.returnResource(redis);
		}
		return lindex;
	}

	/**
	 *  
	 */
	@Override
	public Map<String, Object> selectLightReportDatafromMongoDB(Map<String, String> map, String type) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 0 + 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", map.get(CarDetectorLogicImpl.KEY_EQUCODE)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = null;
		if (type.equals("hour")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, average:{\"$avg\":\"$value\"}}}";

		} else if (type.equals("day")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }}, average:{\"$avg\":\"$value\"}}}";

		} else if (type.equals("month")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, average:{\"$avg\":\"$value\"}}}";

		} else if (type.equals("year")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" }}, average:{\"$avg\":\"$value\"}}}";

		}
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("average", "$average");
		projectFields.put("year", "$_id.year");
		if (type.equals("month")) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type.equals("day")) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type.equals("hour")) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipCode", "$_id.equipCode");
		DBObject project = new BasicDBObject("$project", projectFields);

		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("collectLightData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			String year = "";
			String month = "";
			String day = "";
			String hour = "";
			int length = 0;
			if (dbObject.get("year") != null) {
				year = dbObject.get("year").toString();
				length = year.length();
				for (; length < 4; length++) {
					year = "0" + year;
				}
			}
			if (dbObject.get("month") != null) {
				month = dbObject.get("month").toString();
				length = month.length();
				for (; length < 2; length++) {
					month = "0" + month;
				}
			}
			if (dbObject.get("day") != null) {
				day = dbObject.get("day").toString();
				length = day.length();
				for (; length < 2; length++) {
					day = "0" + day;
				}
			}
			if (dbObject.get("hour") != null) {
				hour = dbObject.get("hour").toString();
				length = hour.length();
				for (; length < 2; length++) {
					hour = "0" + hour;
				}
			}

			if (type.equals("hour")) {
				mapData.put("time", year + "-" + month + "-" + day + " " + hour);
			} else if (type.equals("day")) {
				mapData.put("time", year + "-" + month + "-" + day);
			} else if (type.equals("month")) {
				mapData.put("time", year + "-" + month);
			} else if (type.equals("year")) {
				mapData.put("time", year);
			}
			int ans = 0;
			if (object.get("average") != null) {
				ans = convertNum(object.get("average").toString());
			}
			mapData.put("value", ans);
			if (!CommonUtils.isEmpty(dbObject.get("equipCode").toString())) {
				mapData.put("code", dbObject.get("equipCode"));
			}
			data.add(mapData);
		}
		Map<String, Object> allData = new HashMap<String, Object>();
		if (type.equals("hour")) {
			allData.put("hour", data);
		} else if (type.equals("day")) {
			allData.put("day", data);
		} else if (type.equals("month")) {
			allData.put("month", data);
		} else if (type.equals("year")) {
			allData.put("year", data);
		}

		Map<String, Object> modelData = new HashMap<String, Object>();
		modelData.put("model", allData);
		return modelData;

	}

	@Override
	public Map<String, Object> selectNoReportDatafromMongoDB(Map<String, String> map, String type) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 0 + 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", map.get(CarDetectorLogicImpl.KEY_EQUCODE)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = null;
		if (type.equals("hour")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, average:{\"$avg\":\"$No2Value\"}}}";

		} else if (type.equals("day")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }}, average:{\"$avg\":\"$No2Value\"}}}";

		} else if (type.equals("month")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, average:{\"$avg\":\"$No2Value\"}}}";

		} else if (type.equals("year")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" }}, average:{\"$avg\":\"$No2Value\"}}}";

		}
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("average", "$average");
		projectFields.put("year", "$_id.year");
		if (type.equals("month")) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type.equals("day")) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type.equals("hour")) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipCode", "$_id.equipCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("collectNo2Data").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			String year = "";
			String month = "";
			String day = "";
			String hour = "";
			int length = 0;
			if (dbObject.get("year") != null) {
				year = dbObject.get("year").toString();
				length = year.length();
				for (; length < 4; length++) {
					year = "0" + year;
				}
			}
			if (dbObject.get("month") != null) {
				month = dbObject.get("month").toString();
				length = month.length();
				for (; length < 2; length++) {
					month = "0" + month;
				}
			}
			if (dbObject.get("day") != null) {
				day = dbObject.get("day").toString();
				length = day.length();
				for (; length < 2; length++) {
					day = "0" + day;
				}
			}
			if (dbObject.get("hour") != null) {
				hour = dbObject.get("hour").toString();
				length = hour.length();
				for (; length < 2; length++) {
					hour = "0" + hour;
				}
			}

			if (type.equals("hour")) {
				mapData.put("time", year + "-" + month + "-" + day + " " + hour);
			} else if (type.equals("day")) {
				mapData.put("time", year + "-" + month + "-" + day);
			} else if (type.equals("month")) {
				mapData.put("time", year + "-" + month);
			} else if (type.equals("year")) {
				mapData.put("time", year);
			}

			int ans = 0;
			if (object.get("average") != null) {
				ans = (int) Double.parseDouble(object.get("average").toString());
			}
			mapData.put("No2Value", ans);
			if (!CommonUtils.isEmpty(dbObject.get("equipCode").toString())) {
				mapData.put("code", dbObject.get("equipCode"));
			}
			data.add(mapData);
		}
		Map<String, Object> allData = new HashMap<String, Object>();
		if (type.equals("hour")) {
			allData.put("hour", data);
		} else if (type.equals("day")) {
			allData.put("day", data);
		} else if (type.equals("month")) {
			allData.put("month", data);
		} else if (type.equals("year")) {
			allData.put("year", data);
		}

		Map<String, Object> modelData = new HashMap<String, Object>();
		modelData.put("model", allData);
		return modelData;

	}

	@Override
	public List<Equipments> getEquipmentInfoByCategoryInfo(String categoryCode) {
		// TODO Auto-generated method stub
		return lightDetectDao.getEquipmentInfoByCategoryInfo(categoryCode);
	}

	@Override
	public Map<String, Object> getEnvironmentDataByEquipCode(String equipCode) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 0 + 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", equipCode) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\",lightValue:\"$lightValue\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }},count:{\"$sum\":1}}}";
		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("lightDetectorData").aggregate(match, group);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;

		Iterable<DBObject> results = output.results();

		Map<String, Object> reportMap = new HashMap<>();
		reportMap.put("equipCode", equipCode);
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			mapData.put("time", dbObject.get("year").toString() + "-" + dbObject.get("month") + "-"
					+ dbObject.get("day") + " " + dbObject.get("hour"));
			mapData.put("value", dbObject.get("lightValue"));

			data.add(mapData);
		}
		reportMap.put("data", data);
		return reportMap;
	}

	@Override
	public Map<String, Object> selectCOVIReportDatafromMongoDB(Map<String, String> map, String type) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", map.get(CarDetectorLogicImpl.KEY_EQUCODE)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = null;
		if (type.equals("hour")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, coaverage:{\"$avg\":\"$coValue\"}, viaverage:{\"$avg\":\"$viValue\"}}}";

		} else if (type.equals("day")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }}, coaverage:{\"$avg\":\"$coValue\"}, viaverage:{\"$avg\":\"$viValue\"}}}";

		} else if (type.equals("month")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, coaverage:{\"$avg\":\"$coValue\"}, viaverage:{\"$avg\":\"$viValue\"}}}";

		} else if (type.equals("year")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" }}, coaverage:{\"$avg\":\"$coValue\"}, viaverage:{\"$avg\":\"$viValue\"}}}";

		}
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("coaverage", "$coaverage");
		projectFields.put("viaverage", "$viaverage");
		projectFields.put("year", "$_id.year");
		if (type.equals("month")) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type.equals("day")) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type.equals("hour")) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipCode", "$_id.equipCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("collectCoviData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			String year = "";
			String month = "";
			String day = "";
			String hour = "";

			int length = 0;

			if (dbObject.get("year") != null) {
				year = dbObject.get("year").toString();
				length = year.length();
				for (; length < 4; length++) {
					year = "0" + year;
				}
			}
			if (dbObject.get("month") != null) {
				month = dbObject.get("month").toString();
				length = month.length();
				for (; length < 2; length++) {
					month = "0" + month;
				}
			}
			if (dbObject.get("day") != null) {
				day = dbObject.get("day").toString();
				length = day.length();
				for (; length < 2; length++) {
					day = "0" + day;
				}
			}
			if (dbObject.get("hour") != null) {
				hour = dbObject.get("hour").toString();
				length = hour.length();
				for (; length < 2; length++) {
					hour = "0" + hour;
				}
			}
			if (type.equals("hour")) {
				//String factTime = addDateHour(year + "-" + month + "-" + day + " " + hour, -8);
				mapData.put("time", year + "-" + month + "-" + day + " " + hour);
			} else if (type.equals("day")) {
				mapData.put("time", year + "-" + month + "-" + day);
			} else if (type.equals("month")) {
				mapData.put("time", year + "-" + month);
			} else if (type.equals("year")) {
				mapData.put("time", year);
			}

			int ans = 0;
			if (object.get("coaverage") != null) {
				ans = convertNum(object.get("coaverage").toString());

			}
			mapData.put("coValue", ans);
			ans = 0;
			if (object.get("viaverage") != null) {
				ans = (int) Double.parseDouble(object.get("viaverage").toString());
			}
			mapData.put("viValue", ans);
			if (!CommonUtils.isEmpty(dbObject.get("equipCode").toString())) {
				mapData.put("code", dbObject.get("equipCode"));
			}
			data.add(mapData);
		}
		Map<String, Object> allData = new HashMap<String, Object>();
		if (type.equals("hour")) {
			allData.put("hour", data);
		} else if (type.equals("day")) {
			allData.put("day", data);
		} else if (type.equals("month")) {
			allData.put("month", data);
		} else if (type.equals("year")) {
			allData.put("year", data);
		}

		Map<String, Object> modelData = new HashMap<String, Object>();
		modelData.put("model", allData);
		return modelData;
	}

	public String addDateHour(String day, int hour) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		Date date = null;
		try {
			date = format.parse(day);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (date == null)
			return "";
		System.out.println("front:" + format.format(date)); // 显示输入的日期
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hour);// 24小时制
		date = cal.getTime();
		System.out.println("after:" + format.format(date)); // 显示更新后的日期
		cal = null;
		return format.format(date);
	}

	@Override
	public Map<String, Object> selectFsfxReportDatafromMongoDB(Map<String, String> map, String type) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", map.get(CarDetectorLogicImpl.KEY_EQUCODE)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = null;
		if (type.equals("hour")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, fsaverage:{\"$avg\":\"$WsData\"}}}";

		} else if (type.equals("day")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }}, fsaverage:{\"$avg\":\"$WsData\"}}}";

		} else if (type.equals("month")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, fsaverage:{\"$avg\":\"$WsData\"}}}";

		} else if (type.equals("year")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" }}, fsaverage:{\"$avg\":\"$WsData\"}}}";

		}
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("fsaverage", "$fsaverage");
		projectFields.put("year", "$_id.year");
		if (type.equals("month")) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type.equals("day")) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type.equals("hour")) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipCode", "$_id.equipCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("collectWsData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			String year = "";
			String month = "";
			String day = "";
			String hour = "";
			int length = 0;
			if (dbObject.get("year") != null) {
				year = dbObject.get("year").toString();
				length = year.length();
				for (; length < 4; length++) {
					year = "0" + year;
				}
			}
			if (dbObject.get("month") != null) {
				month = dbObject.get("month").toString();
				length = month.length();
				for (; length < 2; length++) {
					month = "0" + month;
				}
			}
			if (dbObject.get("day") != null) {
				day = dbObject.get("day").toString();
				length = day.length();
				for (; length < 2; length++) {
					day = "0" + day;
				}
			}
			if (dbObject.get("hour") != null) {
				hour = dbObject.get("hour").toString();
				length = hour.length();
				for (; length < 2; length++) {
					hour = "0" + hour;
				}
			}

			if (type.equals("hour")) {
				mapData.put("time", year + "-" + month + "-" + day + " " + hour);
			} else if (type.equals("day")) {
				mapData.put("time", year + "-" + month + "-" + day);
			} else if (type.equals("month")) {
				mapData.put("time", year + "-" + month);
			} else if (type.equals("year")) {
				mapData.put("time", year);
			}

			int ans = 0;
			if (object.get("fsaverage") != null) {
				ans = convertNum(object.get("fsaverage").toString());

			}
			mapData.put("fsValue", ans);
			if (!CommonUtils.isEmpty(dbObject.get("equipCode").toString())) {
				mapData.put("code", dbObject.get("equipCode"));
			}
			data.add(mapData);
		}
		Map<String, Object> allData = new HashMap<String, Object>();
		if (type.equals("hour")) {
			allData.put("hour", data);
		} else if (type.equals("day")) {
			allData.put("day", data);
		} else if (type.equals("month")) {
			allData.put("month", data);
		} else if (type.equals("year")) {
			allData.put("year", data);
		}
		Map<String, Object> modelData = new HashMap<String, Object>();
		modelData.put("model", allData);
		return modelData;
	}

	@Override
	public Map<String, Object> selectIntensityReportDatafromMongoDB(Map<String, String> map, String type) {
		// TODO Auto-generated method stub
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(new Date());
		oneDayStart.add(Calendar.DAY_OF_MONTH, -1);
		oneDayStart.set(Calendar.HOUR_OF_DAY, 8);
		oneDayStart.set(Calendar.MINUTE, 0);
		oneDayStart.set(Calendar.SECOND, 0);
		oneDayStart.set(Calendar.MILLISECOND, 0);
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(new Date());
		oneDayEnd.add(Calendar.DAY_OF_MONTH, 0);
		oneDayEnd.set(Calendar.HOUR_OF_DAY, 7);
		oneDayEnd.set(Calendar.MINUTE, 59);
		oneDayEnd.set(Calendar.SECOND, 59);
		oneDayEnd.set(Calendar.MILLISECOND, 999);
		// match
		BasicDBObject[] array = { new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
				new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
				new BasicDBObject("equipCode", map.get(CarDetectorLogicImpl.KEY_EQUCODE)) };
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		String groupStr = null;
		if (type.equals("hour")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, average:{\"$avg\":\"$GqData\"}}}";

		} else if (type.equals("day")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }}, average:{\"$avg\":\"$GqData\"}}}";

		} else if (type.equals("month")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, average:{\"$avg\":\"$GqData\"}}}";

		} else if (type.equals("year")) {
			groupStr = "{\"$group\":{_id:{equipCode:\"$equipCode\", year: { \"$year\": \"$utcTime\" }}, average:{\"$avg\":\"$GqData\"}}}";

		}
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("average", "$average");
		projectFields.put("year", "$_id.year");
		if (type.equals("month")) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type.equals("day")) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type.equals("hour")) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipCode", "$_id.equipCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		BasicDBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		AggregationOutput output = mongoTemplate.getCollection("collectGqData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> mapData = null;
		for (DBObject object : output.results()) {
			mapData = new HashMap<>();
			DBObject dbObject = (DBObject) object.get("_id");
			String year = "";
			String month = "";
			String day = "";
			String hour = "";
			int length = 0;
			if (dbObject.get("year") != null) {
				year = dbObject.get("year").toString();
				length = year.length();
				for (; length < 4; length++) {
					year = "0" + year;
				}
			}
			if (dbObject.get("month") != null) {
				month = dbObject.get("month").toString();
				length = month.length();
				for (; length < 2; length++) {
					month = "0" + month;
				}
			}
			if (dbObject.get("day") != null) {
				day = dbObject.get("day").toString();
				length = day.length();
				for (; length < 2; length++) {
					day = "0" + day;
				}
			}
			if (dbObject.get("hour") != null) {
				hour = dbObject.get("hour").toString();
				length = hour.length();
				for (; length < 2; length++) {
					hour = "0" + hour;
				}
			}

			if (type.equals("hour")) {
				mapData.put("time", year + "-" + month + "-" + day + " " + hour);
			} else if (type.equals("day")) {
				mapData.put("time", year + "-" + month + "-" + day);
			} else if (type.equals("month")) {
				mapData.put("time", year + "-" + month);
			} else if (type.equals("year")) {
				mapData.put("time", year);
			}

			int ans = 0;
			if (object.get("average") != null) {
				ans = convertNum(object.get("average").toString());
			}
			mapData.put("GqValue", ans);
			if (!CommonUtils.isEmpty(dbObject.get("equipCode").toString())) {
				mapData.put("code", dbObject.get("equipCode"));
			}
			data.add(mapData);
		}
		Map<String, Object> allData = new HashMap<String, Object>();
		if (type.equals("hour")) {
			allData.put("hour", data);
		} else if (type.equals("day")) {
			allData.put("day", data);
		} else if (type.equals("month")) {
			allData.put("month", data);
		} else if (type.equals("year")) {
			allData.put("year", data);
		}
		Map<String, Object> modelData = new HashMap<String, Object>();
		modelData.put("model", allData);
		return modelData;
	}

	public Integer convertNum(String inputNum) {
		return (int) Double.parseDouble(inputNum);
	}
}
