package com.cloudinnov.dao;

import com.cloudinnov.model.AuthRight;

public interface AuthRightDao extends IBaseDao<AuthRight>{
	int insertInfo(AuthRight authRight);

    int updateInfoByCondition(AuthRight authRight);
}