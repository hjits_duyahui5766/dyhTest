package com.cloudinnov.model;

public class AuthModule extends BaseModel{

    private Integer portal;

    private String code;

    public Integer getPortal() {
        return portal;
    }

    public void setPortal(Integer portal) {
        this.portal = portal;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}