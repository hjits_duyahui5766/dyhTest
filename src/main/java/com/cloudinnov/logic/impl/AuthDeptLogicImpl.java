package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AuthDeptDao;
import com.cloudinnov.dao.AuthDeptUserDao;
import com.cloudinnov.logic.AuthDeptLogic;
import com.cloudinnov.model.AuthDept;
import com.cloudinnov.model.AuthDeptUser;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * Created by chengning on 2016/11/11.
 */
@Service
public class AuthDeptLogicImpl extends BaseLogicImpl<AuthDept> implements AuthDeptLogic{

    int digit = 5;
    
    @Autowired
    private AuthDeptDao authDeptDao;
    
    @Autowired
    private AuthDeptUserDao authDeptUserDao;

    public int save(AuthDept dept) {
    	dept.setCode(CodeUtil.authDeptCode(digit));
    	if(dept.getParentId() == CommonUtils.RESOURCE_PARENT_ID){
    		dept.setParentId(CommonUtils.RESOURCE_PARENT_ID);
    		dept.setGrade(1);
		}else if(dept.getParentId() >= CommonUtils.RESOURCE_PARENT_ID){
			AuthDept parent = authDeptDao.selectEntityByparentId(dept);
			dept.setParentId(parent.getId());
			dept.setGrade(parent.getGrade() +1);
		}else{
			return 0;
		}
        int returnCode = authDeptDao.insert(dept);
        if (returnCode == CommonUtils.SUCCESS_NUM) {
            returnCode = authDeptDao.insertInfo(dept);
        } else {
            returnCode = 0;
        }
        return returnCode;
    }
    
    public int update(AuthDept dept) {
        int returnCode = authDeptDao.updateByCondition(dept);
        if (returnCode == CommonUtils.SUCCESS_NUM) {
            returnCode = authDeptDao.updateInfoByCondition(dept);
        } else {
            returnCode = 0;
        }
        return returnCode;
    }

	@Override
	public List<AuthDept> tableList(String[] codes, String language) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("codes", codes);
		return authDeptDao.tableList(map);
	}

	@Override
	public int saveOtherLanguage(AuthDept authDept) {
		int returnCode = authDeptDao.insertInfo(authDept);
		return returnCode;
	}

	@Override
	public int updateOtherLanguage(AuthDept authDept) {
		int returnCode = authDeptDao.updateInfoByCondition(authDept);
		return returnCode;
	}

	@Override
	public List<AuthDeptUser> selectDeptUserList(AuthDeptUser authDeptUser) {
		return authDeptUserDao.selectListByCondition(authDeptUser);
	}
}
