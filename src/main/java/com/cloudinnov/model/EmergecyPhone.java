package com.cloudinnov.model;

/**
 * @remark 分机编号：phoneid 状态：status 03----呼叫 01----通话 02---结束通话05---广播开 06---广播关 接收时间：currentTime
 */
public class EmergecyPhone {
	public final static String PHONE_ID = "phoneId";
	private String phoneId;
	private int status;
	private String currentTime;
	
	private String equipmentCode;
	private String categoryCode;
	

	public String getPhoneId() {
		return phoneId;
	}
	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	
}
