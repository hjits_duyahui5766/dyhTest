package com.cloudinnov.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.logic.JobTaskLogic;
import com.cloudinnov.logic.TriggerLogic;
import com.cloudinnov.model.ControlSolution;
import com.cloudinnov.model.Trigger;
import com.cloudinnov.task.model.ScheduleJob;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.support.spring.SpringUtils;

/**
 * 告警规则任务处理
 * @ClassName: AlarmRuleQuartzJob
 * @Description: TODO
 * @author: ningmeng
 * @date: 2016年12月1日 下午2:00:24
 */
public class TriggerQuartzJob {
	private static final Logger logger = LoggerFactory.getLogger(TriggerQuartzJob.class);
	private TriggerLogic triggerLogic = SpringUtils.getBean("tiggerLogic");
	private JobTaskLogic jobTaskLogic = SpringUtils.getBean("jobTaskLogic");
	private ControlSolutionLogic controlSolutionLogic = SpringUtils.getBean("controlSolutionLogic");

	/**
	 * 轮询所有触发器,查询触发器关联的群控方案,进行数据(目前只支持文本)发送
	 * @Title: sendText
	 * @Description: TODO
	 * @return: void
	 */
	protected void execute() {
		try {
			List<ScheduleJob> runJobs = jobTaskLogic.getRunningJob();
			for (ScheduleJob jobModel : runJobs) {
				if (jobModel.getJobGroup().equals(ScheduleJob.JOB_GROUP_TIGGER)) {// 触发器组
					jobModel.setEnv(CommonUtils.ENV);
					ScheduleJob model = jobTaskLogic.selectTask(jobModel);
					if (model != null) {
						List<ControlSolution> list = controlSolutionLogic.selectControlSolutions(model.getObjectCode());
						for (ControlSolution controlSolution : list) {
							if (controlSolution.getSolutionType() == CommonUtils.CONTROL_SOLUTION_BOARD_TYPE) {
								// 当前时间大于等于生效时间,并且当前时间小于等于失效时间
								if (System.currentTimeMillis() >= Long.parseLong(model.getEffectTime())
										&& System.currentTimeMillis() <= Long.parseLong(model.getExpireTime())) {
									model.setStartTime(System.currentTimeMillis());
									int returnCode = triggerLogic
											.controlBoardSolutionByTiggercode(model.getObjectCode(), model);
									if (returnCode == 1) {
										logger.debug("触发器" + model.getObjectCode() + "发送文本成功");
									} else if (returnCode == 0) {
										logger.debug("触发器" + model.getObjectCode() + "未添加文本");
									} else if (returnCode == -1) {
										logger.debug("触发器" + model.getObjectCode() + "发送文本失败");
									}
								} else {
									logger.debug("触发器" + model.getObjectCode() + "未在指定生效时间内");
								}
							} else if (controlSolution.getSolutionType() == CommonUtils.CONTROL_SOLUTION_DEVICE_TYPE) {
								// if (System.currentTimeMillis() >=
								// Long.parseLong(model.getEffectTime())
								// && System.currentTimeMillis() <=
								// Long.parseLong(model.getExpireTime())) {
								model.setStartTime(System.currentTimeMillis());
								int result = controllerEquipments(model.getObjectCode());
								if (result == 1) {
									logger.debug("触发器" + model.getObjectCode() + "设备控制成功");
								} else if (result == -1) {
									logger.debug("触发器" + model.getObjectCode() + "设备控制失败");
								} else if (result == 0) {
									logger.debug("触发器" + model.getObjectCode() + "未控制设备");
								}
								// } else {
								// logger.debug("触发器" + model.getObjectCode() + "未在指定生效时间内");
								// }
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected int controllerEquipments(String code) {
		Trigger tigger = new Trigger();
		tigger.setCode(code);
		Trigger triggerInfo = triggerLogic.select(tigger);
		int result = 0;
		if (triggerInfo != null) {
			result = triggerLogic.triggerControllerEquipments(tigger);
		}
		return result;
	}
}
