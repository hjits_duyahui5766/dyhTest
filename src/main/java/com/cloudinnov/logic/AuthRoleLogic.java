package com.cloudinnov.logic; 

import java.util.List;

import com.cloudinnov.model.AuthRole;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年3月16日下午2:28:03
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version 
 */
public interface AuthRoleLogic extends IBaseLogic<AuthRole>{

	public int saveOtherLanguage(AuthRole authRole);
	
	public int updateOtherLanguage(AuthRole authRole);
	
	Page<AuthRole> selectListPage(PageModel page,AuthRole entity,boolean isOrderBy);
	
	public List<AuthRole> selectList(AuthRole authRole,boolean isOrderBy);
}
