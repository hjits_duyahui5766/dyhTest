package com.cloudinnov.dao;

import com.cloudinnov.model.WorkOrder;

public interface WorkOrderDao extends IBaseDao<WorkOrder> {
    
}