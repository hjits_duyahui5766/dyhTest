package com.cloudinnov.model;

public class EquipmentsCategoryAttr extends BaseModel {
	private Integer id;

	private String categoryCode;

	private String code;

	private String customTag;

	private Integer isRequire;

	private Integer status;

	private String createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomTag() {
		return customTag;
	}

	public void setCustomTag(String customTag) {
		this.customTag = customTag;
	}

	public Integer getIsRequire() {
		return isRequire;
	}

	public void setIsRequire(Integer isRequire) {
		this.isRequire = isRequire;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}