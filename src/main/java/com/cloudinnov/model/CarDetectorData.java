package com.cloudinnov.model;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: CarDetectorData
 * @Description: TODO
 * @author: ningmeng
 * @date: 2017年1月17日 上午10:55:08
 */
public class CarDetectorData {
    public static final String DEVICE_ID = "deviceId";
    public static final String PERIOD = "period";
    public static final String LANE_TOTAL = "laneTotal";
    public static final String IP = "ip";
    private String equipmentCode;
    private String deviceId;
    private Integer period;
    private Integer laneTotal;
    private Long carTotal;
    private String laneData;
    private String summaryTime;
    private Date utcTime;
    private long timeMillis;
    private String ip;
    private Integer currentState;
    private List<CarDetectorDataChilren> data;
    private String euqipmentName;
    private String sectionName;
    private String pileNo;
    private Integer type;// 1年，2月，3日，4时
    private CarData carData;
    private Integer beyondLaneData;
    private Integer behindLaneData;
    
    private Integer firstLaneOneKind;
    private Integer firstLaneOneKindAvg;
    private Integer firstLaneTwoKind;
    private Integer firstLaneTwoKindAvg;
    private Integer firstLaneThreeKind;
    private Integer firstLaneThreeKindAvg;
    private Integer firstLaneFourKind;
    private Integer firstLaneFourKindAvg;
    private Integer firstLaneAverageSpeed;
    private Integer firstLaneOccuPancy;
    private Integer firstLaneFlow;
    
    private Integer secondLaneOneKind;
    private Integer secondLaneOneKindAvg;
    private Integer secondLaneTwoKind;
    private Integer secondLaneTwoKindAvg;
    private Integer secondLaneThreeKind;
    private Integer secondLaneThreeKindAvg;
    private Integer secondLaneFourKind;
    private Integer secondLaneFourKindAvg;
    private Integer secondLaneAverageSpeed;
    private Integer secondLaneOccuPancy;
    private Integer secondLaneFlow;
    
    private Integer thirdLaneOneKind;
    private Integer thirdLaneOneKindAvg;
    private Integer thirdLaneTwoKind;
    private Integer thirdLaneTwoKindAvg;
    private Integer thirdLaneThreeKind;
    private Integer thirdLaneThreeKindAvg;
    private Integer thirdLaneFourKind;
    private Integer thirdLaneFourKindAvg;
    private Integer thirdLaneAverageSpeed;
    private Integer thirdLaneOccuPancy;
    private Integer thirdLaneFlow;
    
    private Integer forthLaneOneKind;
    private Integer forthLaneOneKindAvg;
    private Integer forthLaneTwoKind;
    private Integer forthLaneTwoKindAvg;
    private Integer forthLaneThreeKind;
    private Integer forthLaneThreeKindAvg;
    private Integer forthLaneFourKind;
    private Integer forthLaneFourKindAvg;
    private Integer forthLaneAverageSpeed;
    private Integer forthLaneOccuPancy;
    private Integer forthLaneFlow;
    
    private Integer fifthLaneOneKind;
    private Integer fifthLaneTwoKind;
    private Integer fifthLaneThreeKind;
    private Integer fifthLaneFourKind;
    private Integer fifthLaneAverageSpeed;
    private Integer fifthLaneOccuPancy;
    private Integer fifthLaneFlow;
    
    private Integer sixthLaneOneKind;
    private Integer sixthLaneTwoKind;
    private Integer sixthLaneThreeKind;
    private Integer sixthLaneFourKind;
    private Integer sixthLaneAverageSpeed;
    private Integer sixthLaneOccuPancy;
    private Integer sixthLaneFlow;
    
    public Integer getSixthLaneAverageSpeed() {
		return sixthLaneAverageSpeed;
	}
	public void setSixthLaneAverageSpeed(Integer sixthLaneAverageSpeed) {
		this.sixthLaneAverageSpeed = sixthLaneAverageSpeed;
	}
	public Integer getSixthLaneOccuPancy() {
		return sixthLaneOccuPancy;
	}
	public void setSixthLaneOccuPancy(Integer sixthLaneOccuPancy) {
		this.sixthLaneOccuPancy = sixthLaneOccuPancy;
	}
	 
	public Integer getFirstLaneAverageSpeed() {
		return firstLaneAverageSpeed;
	}
	public void setFirstLaneAverageSpeed(Integer firstLaneAverageSpeed) {
		this.firstLaneAverageSpeed = firstLaneAverageSpeed;
	}
	public Integer getFirstLaneOccuPancy() {
		return firstLaneOccuPancy;
	}
	public void setFirstLaneOccuPancy(Integer firstLaneOccuPancy) {
		this.firstLaneOccuPancy = firstLaneOccuPancy;
	}
	 
	public Integer getSecondLaneAverageSpeed() {
		return secondLaneAverageSpeed;
	}
	public void setSecondLaneAverageSpeed(Integer secondLaneAverageSpeed) {
		this.secondLaneAverageSpeed = secondLaneAverageSpeed;
	}
	public Integer getSecondLaneOccuPancy() {
		return secondLaneOccuPancy;
	}
	public void setSecondLaneOccuPancy(Integer secondLaneOccuPancy) {
		this.secondLaneOccuPancy = secondLaneOccuPancy;
	}
	 
	public Integer getThirdLaneAverageSpeed() {
		return thirdLaneAverageSpeed;
	}
	public void setThirdLaneAverageSpeed(Integer thirdLaneAverageSpeed) {
		this.thirdLaneAverageSpeed = thirdLaneAverageSpeed;
	}
	public Integer getThirdLaneOccuPancy() {
		return thirdLaneOccuPancy;
	}
	public void setThirdLaneOccuPancy(Integer thirdLaneOccuPancy) {
		this.thirdLaneOccuPancy = thirdLaneOccuPancy;
	}
	 
	public Integer getForthLaneAverageSpeed() {
		return forthLaneAverageSpeed;
	}
	public void setForthLaneAverageSpeed(Integer forthLaneAverageSpeed) {
		this.forthLaneAverageSpeed = forthLaneAverageSpeed;
	}
	public Integer getForthLaneOccuPancy() {
		return forthLaneOccuPancy;
	}
	public void setForthLaneOccuPancy(Integer forthLaneOccuPancy) {
		this.forthLaneOccuPancy = forthLaneOccuPancy;
	}
	 
	public Integer getFifthLaneAverageSpeed() {
		return fifthLaneAverageSpeed;
	}
	public void setFifthLaneAverageSpeed(Integer fifthLaneAverageSpeed) {
		this.fifthLaneAverageSpeed = fifthLaneAverageSpeed;
	}
	public Integer getFifthLaneOccuPancy() {
		return fifthLaneOccuPancy;
	}
	public void setFifthLaneOccuPancy(Integer fifthLaneOccuPancy) {
		this.fifthLaneOccuPancy = fifthLaneOccuPancy;
	}
	 
	public Integer getFirstLaneFlow() {
		return firstLaneFlow;
	}
	public void setFirstLaneFlow(Integer firstLaneFlow) {
		this.firstLaneFlow = firstLaneFlow;
	}
	public Integer getSecondLaneFlow() {
		return secondLaneFlow;
	}
	public void setSecondLaneFlow(Integer secondLaneFlow) {
		this.secondLaneFlow = secondLaneFlow;
	}
	public Integer getThirdLaneFlow() {
		return thirdLaneFlow;
	}
	public void setThirdLaneFlow(Integer thirdLaneFlow) {
		this.thirdLaneFlow = thirdLaneFlow;
	}
	public Integer getForthLaneFlow() {
		return forthLaneFlow;
	}
	public void setForthLaneFlow(Integer forthLaneFlow) {
		this.forthLaneFlow = forthLaneFlow;
	}
	public Integer getFifthLaneFlow() {
		return fifthLaneFlow;
	}
	public void setFifthLaneFlow(Integer fifthLaneFlow) {
		this.fifthLaneFlow = fifthLaneFlow;
	}
	public Integer getSixthLaneFlow() {
		return sixthLaneFlow;
	}
	public void setSixthLaneFlow(Integer sixthLaneFlow) {
		this.sixthLaneFlow = sixthLaneFlow;
	}
	public Integer getFirstLaneOneKind() {
		return firstLaneOneKind;
	}
	public void setFirstLaneOneKind(Integer firstLaneOneKind) {
		this.firstLaneOneKind = firstLaneOneKind;
	}
	public Integer getFirstLaneTwoKind() {
		return firstLaneTwoKind;
	}
	public void setFirstLaneTwoKind(Integer firstLaneTwoKind) {
		this.firstLaneTwoKind = firstLaneTwoKind;
	}
	public Integer getFirstLaneThreeKind() {
		return firstLaneThreeKind;
	}
	public void setFirstLaneThreeKind(Integer firstLaneThreeKind) {
		this.firstLaneThreeKind = firstLaneThreeKind;
	}
	public Integer getFirstLaneFourKind() {
		return firstLaneFourKind;
	}
	public void setFirstLaneFourKind(Integer firstLaneFourKind) {
		this.firstLaneFourKind = firstLaneFourKind;
	}
	public Integer getSecondLaneOneKind() {
		return secondLaneOneKind;
	}
	public void setSecondLaneOneKind(Integer secondLaneOneKind) {
		this.secondLaneOneKind = secondLaneOneKind;
	}
	public Integer getSecondLaneTwoKind() {
		return secondLaneTwoKind;
	}
	public void setSecondLaneTwoKind(Integer secondLaneTwoKind) {
		this.secondLaneTwoKind = secondLaneTwoKind;
	}
	public Integer getSecondLaneThreeKind() {
		return secondLaneThreeKind;
	}
	public void setSecondLaneThreeKind(Integer secondLaneThreeKind) {
		this.secondLaneThreeKind = secondLaneThreeKind;
	}
	public Integer getSecondLaneFourKind() {
		return secondLaneFourKind;
	}
	public void setSecondLaneFourKind(Integer secondLaneFourKind) {
		this.secondLaneFourKind = secondLaneFourKind;
	}
	public Integer getThirdLaneOneKind() {
		return thirdLaneOneKind;
	}
	public void setThirdLaneOneKind(Integer thirdLaneOneKind) {
		this.thirdLaneOneKind = thirdLaneOneKind;
	}
	public Integer getThirdLaneTwoKind() {
		return thirdLaneTwoKind;
	}
	public void setThirdLaneTwoKind(Integer thirdLaneTwoKind) {
		this.thirdLaneTwoKind = thirdLaneTwoKind;
	}
	public Integer getThirdLaneThreeKind() {
		return thirdLaneThreeKind;
	}
	public void setThirdLaneThreeKind(Integer thirdLaneThreeKind) {
		this.thirdLaneThreeKind = thirdLaneThreeKind;
	}
	public Integer getThirdLaneFourKind() {
		return thirdLaneFourKind;
	}
	public void setThirdLaneFourKind(Integer thirdLaneFourKind) {
		this.thirdLaneFourKind = thirdLaneFourKind;
	}
	public Integer getForthLaneOneKind() {
		return forthLaneOneKind;
	}
	public void setForthLaneOneKind(Integer forthLaneOneKind) {
		this.forthLaneOneKind = forthLaneOneKind;
	}
	public Integer getForthLaneTwoKind() {
		return forthLaneTwoKind;
	}
	public void setForthLaneTwoKind(Integer forthLaneTwoKind) {
		this.forthLaneTwoKind = forthLaneTwoKind;
	}
	public Integer getForthLaneThreeKind() {
		return forthLaneThreeKind;
	}
	public void setForthLaneThreeKind(Integer forthLaneThreeKind) {
		this.forthLaneThreeKind = forthLaneThreeKind;
	}
	public Integer getForthLaneFourKind() {
		return forthLaneFourKind;
	}
	public void setForthLaneFourKind(Integer forthLaneFourKind) {
		this.forthLaneFourKind = forthLaneFourKind;
	}
	public Integer getFifthLaneOneKind() {
		return fifthLaneOneKind;
	}
	public void setFifthLaneOneKind(Integer fifthLaneOneKind) {
		this.fifthLaneOneKind = fifthLaneOneKind;
	}
	public Integer getFifthLaneTwoKind() {
		return fifthLaneTwoKind;
	}
	public void setFifthLaneTwoKind(Integer fifthLaneTwoKind) {
		this.fifthLaneTwoKind = fifthLaneTwoKind;
	}
	public Integer getFifthLaneThreeKind() {
		return fifthLaneThreeKind;
	}
	public void setFifthLaneThreeKind(Integer fifthLaneThreeKind) {
		this.fifthLaneThreeKind = fifthLaneThreeKind;
	}
	public Integer getFifthLaneFourKind() {
		return fifthLaneFourKind;
	}
	public void setFifthLaneFourKind(Integer fifthLaneFourKind) {
		this.fifthLaneFourKind = fifthLaneFourKind;
	}
	public Integer getSixthLaneOneKind() {
		return sixthLaneOneKind;
	}
	public void setSixthLaneOneKind(Integer sixthLaneOneKind) {
		this.sixthLaneOneKind = sixthLaneOneKind;
	}
	public Integer getSixthLaneTwoKind() {
		return sixthLaneTwoKind;
	}
	public void setSixthLaneTwoKind(Integer sixthLaneTwoKind) {
		this.sixthLaneTwoKind = sixthLaneTwoKind;
	}
	public Integer getSixthLaneThreeKind() {
		return sixthLaneThreeKind;
	}
	public void setSixthLaneThreeKind(Integer sixthLaneThreeKind) {
		this.sixthLaneThreeKind = sixthLaneThreeKind;
	}
	public Integer getSixthLaneFourKind() {
		return sixthLaneFourKind;
	}
	public void setSixthLaneFourKind(Integer sixthLaneFourKind) {
		this.sixthLaneFourKind = sixthLaneFourKind;
	}

	 
    

    public Integer getBeyondLaneData() {
		return beyondLaneData;
	}
	public void setBeyondLaneData(Integer beyondLaneData) {
		this.beyondLaneData = beyondLaneData;
	}
	public Integer getBehindLaneData() {
		return behindLaneData;
	}
	public void setBehindLaneData(Integer behindLaneData) {
		this.behindLaneData = behindLaneData;
	}

	public static class CarData implements Comparable<CarData> {
        private String time;
        private String value;

        public String getTime() {
            return time;
        }
        public void setTime(String time) {
            this.time = time;
        }
        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
        @Override
        public String toString() {
            return "CarData [time=" + time + ", value=" + value + ", getTime()=" + getTime() + ", getValue()="
                    + getValue() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                    + super.toString() + "]";
        }
        @Override
        public int compareTo(CarData carData) {
            return this.getTime().compareTo(carData.getTime());
        }
    }

    public CarData getCarData() {
        return carData;
    }
    public void setCarData(CarData carData) {
        this.carData = carData;
    }
    public String getEuqipmentName() {
        return euqipmentName;
    }
    public void setEuqipmentName(String euqipmentName) {
        this.euqipmentName = euqipmentName;
    }
    public String getSectionName() {
        return sectionName;
    }
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    public String getPileNo() {
        return pileNo;
    }
    public void setPileNo(String pileNo) {
        this.pileNo = pileNo;
    }
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    public String getEquipmentCode() {
        return equipmentCode;
    }
    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public Integer getPeriod() {
        return period;
    }
    public void setPeriod(Integer period) {
        this.period = period;
    }
    public Integer getLaneTotal() {
        return laneTotal;
    }
    public void setLaneTotal(Integer laneTotal) {
        this.laneTotal = laneTotal;
    }
    public Long getCarTotal() {
        return carTotal;
    }
    public void setCarTotal(Long carTotal) {
        this.carTotal = carTotal;
    }
    public String getLaneData() {
        return laneData;
    }
    public void setLaneData(String laneData) {
        this.laneData = laneData;
    }
    public String getSummaryTime() {
        return summaryTime;
    }
    public void setSummaryTime(String summaryTime) {
        this.summaryTime = summaryTime;
    }
    public Date getUtcTime() {
        return utcTime;
    }
    public void setUtcTime(Date utcTime) {
        this.utcTime = utcTime;
    }
    public long getTimeMillis() {
        return timeMillis;
    }
    public void setTimeMillis(long timeMillis) {
        this.timeMillis = timeMillis;
    }
    public List<CarDetectorDataChilren> getData() {
        return data;
    }
    public void setData(List<CarDetectorDataChilren> data) {
        this.data = data;
    }
    public Integer getCurrentState() {
        return currentState;
    }
    public void setCurrentState(Integer currentState) {
        this.currentState = currentState;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public class CarDetectorDataChilren {
        private String deviceId;
        private long lineId;
        private long lineAvgVelocuty;
        private long lineFlow;
        private long lineOccRate;
        private long lineOneClassflow;
        private long lineTwoClassflow;
        private long lineThreeClassflow;
        private long lineFourClassflow;

        public String getDeviceId() {
            return deviceId;
        }
        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }
        public long getLineId() {
            return lineId;
        }
        public void setLineId(long lineId) {
            this.lineId = lineId;
        }
        public long getLineAvgVelocuty() {
            return lineAvgVelocuty;
        }
        public void setLineAvgVelocuty(long lineAvgVelocuty) {
            this.lineAvgVelocuty = lineAvgVelocuty;
        }
        public long getLineFlow() {
            return lineFlow;
        }
        public void setLineFlow(long lineFlow) {
            this.lineFlow = lineFlow;
        }
        public long getLineOccRate() {
            return lineOccRate;
        }
        public void setLineOccRate(long lineOccRate) {
            this.lineOccRate = lineOccRate;
        }
        public long getLineOneClassflow() {
            return lineOneClassflow;
        }
        public void setLineOneClassflow(long lineOneClassflow) {
            this.lineOneClassflow = lineOneClassflow;
        }
        public long getLineTwoClassflow() {
            return lineTwoClassflow;
        }
        public void setLineTwoClassflow(long lineTwoClassflow) {
            this.lineTwoClassflow = lineTwoClassflow;
        }
        public long getLineThreeClassflow() {
            return lineThreeClassflow;
        }
        public void setLineThreeClassflow(long lineThreeClassflow) {
            this.lineThreeClassflow = lineThreeClassflow;
        }
        public long getLineFourClassflow() {
            return lineFourClassflow;
        }
        public void setLineFourClassflow(long lineFourClassflow) {
            this.lineFourClassflow = lineFourClassflow;
        }
        
    }

    public Integer getFirstLaneOneKindAvg() {
		return firstLaneOneKindAvg;
	}
	public void setFirstLaneOneKindAvg(Integer firstLaneOneKindAvg) {
		this.firstLaneOneKindAvg = firstLaneOneKindAvg;
	}
	public Integer getFirstLaneTwoKindAvg() {
		return firstLaneTwoKindAvg;
	}
	public void setFirstLaneTwoKindAvg(Integer firstLaneTwoKindAvg) {
		this.firstLaneTwoKindAvg = firstLaneTwoKindAvg;
	}
	public Integer getFirstLaneThreeKindAvg() {
		return firstLaneThreeKindAvg;
	}
	public void setFirstLaneThreeKindAvg(Integer firstLaneThreeKindAvg) {
		this.firstLaneThreeKindAvg = firstLaneThreeKindAvg;
	}
	public Integer getFirstLaneFourKindAvg() {
		return firstLaneFourKindAvg;
	}
	public void setFirstLaneFourKindAvg(Integer firstLaneFourKindAvg) {
		this.firstLaneFourKindAvg = firstLaneFourKindAvg;
	}
	public Integer getSecondLaneOneKindAvg() {
		return secondLaneOneKindAvg;
	}
	public void setSecondLaneOneKindAvg(Integer secondLaneOneKindAvg) {
		this.secondLaneOneKindAvg = secondLaneOneKindAvg;
	}
	public Integer getSecondLaneTwoKindAvg() {
		return secondLaneTwoKindAvg;
	}
	public void setSecondLaneTwoKindAvg(Integer secondLaneTwoKindAvg) {
		this.secondLaneTwoKindAvg = secondLaneTwoKindAvg;
	}
	public Integer getSecondLaneThreeKindAvg() {
		return secondLaneThreeKindAvg;
	}
	public void setSecondLaneThreeKindAvg(Integer secondLaneThreeKindAvg) {
		this.secondLaneThreeKindAvg = secondLaneThreeKindAvg;
	}
	public Integer getSecondLaneFourKindAvg() {
		return secondLaneFourKindAvg;
	}
	public void setSecondLaneFourKindAvg(Integer secondLaneFourKindAvg) {
		this.secondLaneFourKindAvg = secondLaneFourKindAvg;
	}
	public Integer getThirdLaneOneKindAvg() {
		return thirdLaneOneKindAvg;
	}
	public void setThirdLaneOneKindAvg(Integer thirdLaneOneKindAvg) {
		this.thirdLaneOneKindAvg = thirdLaneOneKindAvg;
	}
	public Integer getThirdLaneTwoKindAvg() {
		return thirdLaneTwoKindAvg;
	}
	public void setThirdLaneTwoKindAvg(Integer thirdLaneTwoKindAvg) {
		this.thirdLaneTwoKindAvg = thirdLaneTwoKindAvg;
	}
	public Integer getThirdLaneThreeKindAvg() {
		return thirdLaneThreeKindAvg;
	}
	public void setThirdLaneThreeKindAvg(Integer thirdLaneThreeKindAvg) {
		this.thirdLaneThreeKindAvg = thirdLaneThreeKindAvg;
	}
	public Integer getThirdLaneFourKindAvg() {
		return thirdLaneFourKindAvg;
	}
	public void setThirdLaneFourKindAvg(Integer thirdLaneFourKindAvg) {
		this.thirdLaneFourKindAvg = thirdLaneFourKindAvg;
	}
	public Integer getForthLaneOneKindAvg() {
		return forthLaneOneKindAvg;
	}
	public void setForthLaneOneKindAvg(Integer forthLaneOneKindAvg) {
		this.forthLaneOneKindAvg = forthLaneOneKindAvg;
	}
	public Integer getForthLaneTwoKindAvg() {
		return forthLaneTwoKindAvg;
	}
	public void setForthLaneTwoKindAvg(Integer forthLaneTwoKindAvg) {
		this.forthLaneTwoKindAvg = forthLaneTwoKindAvg;
	}
	public Integer getForthLaneThreeKindAvg() {
		return forthLaneThreeKindAvg;
	}
	public void setForthLaneThreeKindAvg(Integer forthLaneThreeKindAvg) {
		this.forthLaneThreeKindAvg = forthLaneThreeKindAvg;
	}
	public Integer getForthLaneFourKindAvg() {
		return forthLaneFourKindAvg;
	}
	public void setForthLaneFourKindAvg(Integer forthLaneFourKindAvg) {
		this.forthLaneFourKindAvg = forthLaneFourKindAvg;
	}
	
	@Override
    public String toString() {
        return "CarDetectorData [equipmentCode=" + equipmentCode + ", deviceId=" + deviceId + ", period=" + period
                + ", laneTotal=" + laneTotal + ", carTotal=" + carTotal + ", laneData=" + laneData + ", summaryTime="
                + summaryTime + ", utcTime=" + utcTime + ", timeMillis=" + timeMillis + ", currentState=" + currentState
                + ", data=" + data + "]";
    }
}
