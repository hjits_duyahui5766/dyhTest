package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.RealTimeData;

public interface RealTimeDataDao extends IBaseDao<RealTimeData>{
 
    /** 
    * insertRealtimeList 
    * @Description: 批量插入数据
    * @param @param map
    * @param @return    参数
    * @return int    返回类型 
    */
    int insertRealtimeList(Map<String, Object> map);
    
    List<RealTimeData> selectHistoryData(Map<String, Object> map);
}