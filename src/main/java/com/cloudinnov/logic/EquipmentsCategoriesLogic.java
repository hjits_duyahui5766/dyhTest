package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.EquipmentsCategories;

/**
 * @author guochao
 * @date 2016年2月17日下午5:56:14
 * @email chaoguo@cloudinnov.com
 * @remark 设备分类service接口
 * @version
 */
public interface EquipmentsCategoriesLogic extends IBaseLogic<EquipmentsCategories> {
	public int saveOtherLanguage(EquipmentsCategories equipmentsCategories);

	public List<EquipmentsCategories> addSelectList(EquipmentsCategories equipmentsCategories);

	public List<EquipmentsCategories> tableList(String[] codes, String language, String oemCode);

	public int updateOtherLanguage(EquipmentsCategories equipmentsCategories);
}
