package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentFaultChannelDao;
import com.cloudinnov.logic.EquipmentFaultChannelLogic;
import com.cloudinnov.model.EquipmentFaultChannel;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月17日下午5:00:15
 * @email chaoguo@cloudinnov.com
 * @remark 设备故障通道service实现
 * @version
 */
@Service
public class EquipmentFaultChannelLogicImpl extends BaseLogicImpl<EquipmentFaultChannel>
		implements EquipmentFaultChannelLogic {

	private int digit = 5;
	@Autowired
	private EquipmentFaultChannelDao equipmentFaultChannelDao;

	public int save(EquipmentFaultChannel equipmentFaultChannel) {
		equipmentFaultChannel.setStatus(1);
		String code = CodeUtil.equipmentFaultChannelCode(digit);
		equipmentFaultChannel.setCode(code);
		equipmentFaultChannel.setCreateTime(String.valueOf(System.currentTimeMillis()));
		equipmentFaultChannel.setStatus(CommonUtils.STATUS_NORMAL);
		int result = equipmentFaultChannelDao.insert(equipmentFaultChannel);
		if (result == 1) {
			result = equipmentFaultChannelDao.insertInfo(equipmentFaultChannel);
		}
		return result;
	}

	@Override
	public int update(EquipmentFaultChannel equipmentFaultChannel) {
		int returnCode = equipmentFaultChannelDao.updateByCondition(equipmentFaultChannel);
		returnCode = equipmentFaultChannelDao.updateInfoByCondition(equipmentFaultChannel);
		return returnCode;
	}

	@Override
	public int saveOtherLanguage(EquipmentFaultChannel equipmentFaultChannel) {
		return equipmentFaultChannelDao.insertInfo(equipmentFaultChannel);
	}

	@Override
	public int updateOtherLanguage(EquipmentFaultChannel equipmentFaultChannel) {
		return equipmentFaultChannelDao.updateInfoByCondition(equipmentFaultChannel);
	}

}
