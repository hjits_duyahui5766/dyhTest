package com.cloudinnov.test;

import org.apache.commons.lang.RandomStringUtils;

import com.cloudinnov.logic.ControlSolutionLogic;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.support.spring.SpringUtils;

public class Test1 {

	public static void main(String[] args) {
		String[] data = new String[5];
		String controlSolutionCode = "CR";
		for (int i = 0; i < 5; i++) {
			data[i] = RandomStringUtils.random(1, "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789");
			controlSolutionCode = controlSolutionCode + data[i];
		}
		
		System.out.println(controlSolutionCode);
	}
}
