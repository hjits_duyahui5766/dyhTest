package com.cloudinnov.model;

public class EquipmentInspectionWorkOrder extends BaseModel{

    private String customerCode;
    
    private String customerName;

    private String configCode;
    
    private String equipmentCode;
    
    private String equipmentName;

    private String title;

    private String inspectionDate;

    private Integer orderStatus;

    private String paddingBy;

    private String comment;

    private String content;
    
    private String paddingByName;
    
    private String equipmentImage;
    
    private Integer period;

    private String proLineCode;
    
    public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaddingBy() {
        return paddingBy;
    }

    public void setPaddingBy(String paddingBy) {
        this.paddingBy = paddingBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

	public String getPaddingByName() {
		return paddingByName;
	}

	public void setPaddingByName(String paddingByName) {
		this.paddingByName = paddingByName;
	}

	public String getEquipmentImage() {
		return equipmentImage;
	}

	public void setEquipmentImage(String equipmentImage) {
		this.equipmentImage = equipmentImage;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getProLineCode() {
		return proLineCode;
	}

	public void setProLineCode(String proLineCode) {
		this.proLineCode = proLineCode;
	}
    
    
}