package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.AuthRoleUserLogic;
import com.cloudinnov.model.AuthRoleUser;

@Service
public class AuthRoleUserLogicImpl extends BaseLogicImpl<AuthRoleUser>
		implements AuthRoleUserLogic {

}
