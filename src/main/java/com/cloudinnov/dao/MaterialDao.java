package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.Material;
import com.cloudinnov.model.ProductionLines;

/** 原料管理
 * @author chengning
 * @date 2016年3月31日上午11:12:48
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface MaterialDao extends IBaseDao<Material>{
  
	public int insertInfo(Material material);
	
	public int updateInfoByCondition(Material material);
	
	public List<Material> selectMaterialByProlineCode(ProductionLines ProductionLine);
}