package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AttachDao;
import com.cloudinnov.logic.AttachLogic;
import com.cloudinnov.model.Attach;

/**附件Logic
 * @author chengning
 * @date 2016年4月14日下午2:25:31
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
@Service
public class AttachLogicImpl extends BaseLogicImpl<Attach> implements AttachLogic {

	public static final String ATTACH_USER_TYPE = "user";
	
	public static final String ATTACH_EQUIPMENT_TYPE = "equipment";
	
	public static final String ATTACH_WORKORDER_TYPE = "worderorder";
	
	public static final String ATTACH_INSPECTION_TYPE = "inspection";
	
	public static final String ATTACH_FILETYPE_IMAGE = "image";
	
	public static final String ATTACH_FILETYPE_FILE = "file";
	
	@Autowired
	private AttachDao attachDao;

	@Override
	public int save(Attach attach) {
		int returnCode = attachDao.insert(attach);
		return returnCode;
	}

	@Override
	public int saveWorderOrderImage(Attach attach){
		if(attach.getFileType()==null){
			attach.setFileType(ATTACH_FILETYPE_IMAGE);
		}
		attach.setType(ATTACH_WORKORDER_TYPE);
		int returnCode = 0;
		for(String url: attach.getUrls()){
			attach.setUrl(url);
			returnCode = save(attach);
		}
		return returnCode;		
	}
	
	@Override
	public int saveInspectionImage(Attach attach){
		if(attach.getFileType()==null){
			attach.setFileType(ATTACH_FILETYPE_IMAGE);
		}
		attach.setType(ATTACH_INSPECTION_TYPE);
		int returnCode = 0;
		for(String url: attach.getUrls()){
			attach.setUrl(url);
			returnCode = save(attach);
		}
		return returnCode;		
	}

	@Override
	public int update(Attach attach) {
		int returnCode = 0;
		Attach image = attachDao.selectEntityByCondition(attach);
		if(image!=null){
			returnCode = attachDao.updateByCondition(attach);
		}else{
			returnCode = save(attach);
		}
		return returnCode;
	}
	
	
	
}
