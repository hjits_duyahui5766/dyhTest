package com.cloudinnov.model;

/**
 * @author guochao
 * @date 2016年3月16日上午9:02:54
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public class RealTime {
	
	private String code;
	private String value;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "RealTime [code=" + code + ", value=" + value + "]";
	}
}
