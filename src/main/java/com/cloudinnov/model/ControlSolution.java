package com.cloudinnov.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ControlSolution extends BaseModel {
	@NotNull(message = "编码参数必填")
	@NotEmpty(message = "编码参数不能为空")
	private String code;
	private String name;
	private String tiggerCode;
	private Integer effectiveLength;
	private Integer level;
	private Integer type;
	private Integer status;
	private String comment;
	private Integer solutionType;
	private Integer isSchedule;
	private String sendContent;
	private String createTime;
	private String lastUpdateTime;
	private String conSolConfig;// 设备控制配置
	private String conSolConfigRecover;// 设备控制恢复配置
	@JsonIgnore
	private String data;// 设备控制配置
	@JsonIgnore
	private String list;
	private String tiggerName;
	@JsonIgnore
	private List<ControlSolutionConfig> controlSolutionConfig;
	private List<ControlSolutionConfig> controlSolutionConfigRecover;
	@JsonIgnore
	private List<BoardSolutionConfig> boardSolutionConfig;
	private String equipmentClassify;
	private String equipmentCode;
	@JsonIgnore
	private String[] equipmentCodes;
	private List<Map<String, Object>> equipments;
	private Integer onOff;// 方案开关
	private String expressArray; //群控触发条件
	private String expressRecoverArray; //群控恢复条件
	@JsonIgnore
	private String config;

	public Integer getIsSchedule() {
		return isSchedule;
	}
	public void setIsSchedule(Integer isSchedule) {
		this.isSchedule = isSchedule;
	}
	public String getExpressRecoverArray() {
		return expressRecoverArray;
	}
	public void setExpressRecoverArray(String expressRecoverArray) {
		this.expressRecoverArray = expressRecoverArray;
	}
	public String getConSolConfigRecover() {
		return conSolConfigRecover;
	}
	public void setConSolConfigRecover(String conSolConfigRecover) {
		this.conSolConfigRecover = conSolConfigRecover;
	}
	public List<ControlSolutionConfig> getControlSolutionConfigRecover() {
		return controlSolutionConfigRecover;
	}
	public void setControlSolutionConfigRecover(List<ControlSolutionConfig> controlSolutionConfigRecover) {
		this.controlSolutionConfigRecover = controlSolutionConfigRecover;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public Integer getOnOff() {
		return onOff;
	}
	public void setOnOff(Integer onOff) {
		this.onOff = onOff;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getEquipmentClassify() {
		return equipmentClassify;
	}
	public void setEquipmentClassify(String equipmentClassify) {
		this.equipmentClassify = equipmentClassify;
	}
	public String getTiggerName() {
		return tiggerName;
	}
	public void setTiggerName(String tiggerName) {
		this.tiggerName = tiggerName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTiggerCode() {
		return tiggerCode;
	}
	public void setTiggerCode(String tiggerCode) {
		this.tiggerCode = tiggerCode;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getSolutionType() {
		return solutionType;
	}
	public void setSolutionType(Integer solutionType) {
		this.solutionType = solutionType;
	}
	public String getSendContent() {
		return sendContent;
	}
	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getConSolConfig() {
		return conSolConfig;
	}
	public void setConSolConfig(String conSolConfig) {
		this.conSolConfig = conSolConfig;
	}
	@JsonProperty("data")
	public List<ControlSolutionConfig> getControlSolutionConfig() {
		return controlSolutionConfig;
	}
	public void setControlSolutionConfig(List<ControlSolutionConfig> controlSolutionConfig) {
		this.controlSolutionConfig = controlSolutionConfig;
	}
	@JsonProperty("list")
	public List<BoardSolutionConfig> getBoardSolutionConfig() {
		return boardSolutionConfig;
	}
	public void setBoardSolutionConfig(List<BoardSolutionConfig> boardSolutionConfig) {
		this.boardSolutionConfig = boardSolutionConfig;
	}
	public Integer getEffectiveLength() {
		return effectiveLength;
	}
	public void setEffectiveLength(Integer effectiveLength) {
		this.effectiveLength = effectiveLength;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public String[] getEquipmentCodes() {
		return equipmentCodes;
	}
	public void setEquipmentCodes(String[] equipmentCodes) {
		this.equipmentCodes = equipmentCodes;
	}
	@JsonProperty("equipmentCodes")
	public List<Map<String, Object>> getEquipments() {
		return equipments;
	}
	public void setEquipments(List<Map<String, Object>> equipments) {
		this.equipments = equipments;
	}
	public String getExpressArray() {
		return expressArray;
	}
	public void setExpressArray(String expressArray) {
		this.expressArray = expressArray;
	}
	
}
