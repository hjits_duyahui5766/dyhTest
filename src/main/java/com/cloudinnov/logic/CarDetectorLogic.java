package com.cloudinnov.logic;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cloudinnov.model.CarDetectorData;

public interface CarDetectorLogic extends IBaseLogic<CarDetectorData> {
	
	/**
	 * 查询车辆汇总
	 * @Title: carTotalToWebsocket
	 * @Description: TODO
	 * @return: void
	 */
	void carTotalToWebsocket();
	void saveRealTimeDataToMongoDB(CarDetectorData carDetectorData);
	/**
	 * 查询车检仪报表
	 * @Title: selectReportDataToMongoDB
	 * @Description: TODO
	 * @param map 可选参数(equCode 设备编码 startTime 开始时间 endTime 结束时间)
	 * @return
	 * @return: List<CarDetectorData>
	 */
	Map<String, Object> selectReportDataToMongoDB(int type, Map<String, String> map);
	/**
	 * @Title: selectDataByGroupAndCondition
	 * @Description: TODO
	 * @param type 1 无维度查询全部数据 2 有设备维度无时间维度查询设备的全部数据 3 有设备有时间维度查询设备规定时间的数据 4 无设备有时间维度查询设备规定时间的数据
	 * @param equCode
	 * @param startTime
	 * @param endTime
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> selectDataByGroupAndCondition(int type, String equCode, Date startTime, Date endTime);
	List<Map<String, Object>> selectTotalDataByEquAndTime(int type, String equCode, Date startTime, Date endTime);
	/**
	 * 根据设备编码查询每日和每周的车检仪器数据
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param equCode
	 * @return 参数
	 * @return Map<String,Object> 返回类型
	 */
	Map<String, Object> selectDataByEquCodeToRedis(String equCode);
	
	 
}
