package com.cloudinnov.dao;

import com.cloudinnov.model.EquipmentsCategoryAttr;

public interface EquipmentsCategoryAttrDao extends IBaseDao<EquipmentsCategoryAttr> {
	public int deleteByPrimaryKey(Integer id);

	public int insert(EquipmentsCategoryAttr record);

	public int insertSelective(EquipmentsCategoryAttr record);

	EquipmentsCategoryAttr selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(EquipmentsCategoryAttr record);

	public int updateByPrimaryKey(EquipmentsCategoryAttr record);

	public int insertInfo(EquipmentsCategoryAttr equipmentsCategoryAttr);
}