package com.cloudinnov.logic;

import com.cloudinnov.model.AuthRoleUser;

/**
 * 根据设备ID查询解决方案，查询解决方案下的设备列表
 * 
 */
public interface ControlSolutionConfigLogic extends IBaseLogic<AuthRoleUser>{

}
