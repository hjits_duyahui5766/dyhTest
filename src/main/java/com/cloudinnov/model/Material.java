package com.cloudinnov.model;


public class Material  extends BaseModel{

	private String name;
	
	private String comment;
	
    private Long referenceValue;

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getReferenceValue() {
        return referenceValue;
    }

    public void setReferenceValue(Long referenceValue) {
        this.referenceValue = referenceValue;
    }

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
    

}