package com.cloudinnov.logic;

import com.cloudinnov.model.AuthModule;

public interface AuthModuleLogic extends IBaseLogic<AuthModule> {
	
	public int saveOtherLanguage(AuthModule authModule);
	
	public int updateOtherLanguage(AuthModule authModule);
}
