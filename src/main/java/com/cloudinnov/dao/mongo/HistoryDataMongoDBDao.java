package com.cloudinnov.dao.mongo;

import org.springframework.stereotype.Repository;
import com.cloudinnov.model.HistoryDataToMongoDB;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年3月30日上午10:56:02
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Repository("historyDataMongoDBDao")
public class HistoryDataMongoDBDao extends BaseMongoDAOImpl<HistoryDataToMongoDB> {
}
