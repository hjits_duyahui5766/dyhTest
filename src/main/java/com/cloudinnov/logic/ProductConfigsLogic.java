package com.cloudinnov.logic;

import com.cloudinnov.model.ProductConfigs;

/**
 * @author nilixin
 * @date 2016年2月23日下午4:40:46
 * @email 
 * @remark 产物配置接口
 * @version
 */
public interface ProductConfigsLogic extends IBaseLogic<ProductConfigs>{

	public int saveOtherLanguage(ProductConfigs productConfig);
	
	public int updateOtherLanguage(ProductConfigs productConfig);
}
