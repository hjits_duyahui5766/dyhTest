package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AlarmWorkOrdersLogic;
import com.cloudinnov.logic.CompaniesLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.MonitorViewsLogic;
import com.cloudinnov.logic.ProductionLinesLogic;
import com.cloudinnov.model.Companies;
import com.cloudinnov.model.MonitorViews;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月18日下午12:28:40
 * @email chaoguo@cloudinnov.com
 * @remark 监控试图controller
 * @version
 */
@Controller
@RequestMapping("/webapi/monitorViews")
public class MonitorViewsController extends BaseController {

	@Autowired
	private MonitorViewsLogic monitorViewsLogic;

	@Autowired
	private CompaniesLogic companiesLogic;
	@Autowired
	private ProductionLinesLogic productionLinesLogic;
	@Autowired
	private AlarmWorkOrdersLogic alarmWorkOrdersLogic;
	@Autowired
	private EquipmentsLogic equipmentsLogic;

	/**
	 * save
	 * 
	 * @Description: 新建一条监控视图
	 * @param @param
	 *            monitorView
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            session
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "监控管理", methods = "监控视图保存")
	public void save(MonitorViews monitor, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		monitor.setUserCode(getUserInfo(request).get(USER_CODE));
		monitor.setCustomerCode(getUserInfo(request).get(OEM_CODE));

		int result = monitorViewsLogic.save(monitor);

		Map<String, Object> map = new HashMap<String, Object>();

		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据code删除一条监控视图
	 * @param @param
	 *            monitorViewCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            session
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemLog(module = "监控管理", methods = "监控视图删除")
	public void delete(MonitorViews monitorView, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws IOException {
		int result = monitorViewsLogic.delete(monitorView);

		Map<String, Object> map = new HashMap<String, Object>();

		if (result == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * update
	 * 
	 * @Description: 更新监控视图 code字段必须传
	 * @param @param
	 *            monitorView
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "监控视图", methods = "监控视图信息修改")
	public void update(MonitorViews monitorView, HttpServletRequest request, HttpServletResponse response, Model model)
			throws IOException {
		int result = monitorViewsLogic.update(monitorView);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * list
	 * 
	 * @Description: 分页查询监控视图列表
	 * @param @param
	 *            index
	 * @param @param
	 *            size
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, MonitorViews monitor, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		if (getUserInfo(request).get(USER_TYPE) != null
				&& getUserInfo(request).get(USER_TYPE).equals(CommonUtils.OEM_TYPE)) {
			monitor.setOemCode(getUserInfo(request).get(OEM_CODE));
		}
		if (getUserInfo(request).get(USER_TYPE) != null
				&& getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)) {
			monitor.setCustomerCode(getUserInfo(request).get(COM_CODE));
		}
		List<MonitorViews> monitorView = monitorViewsLogic.selectList(monitor, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (monitorView != null) {
			map.put("list", monitorView);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * select
	 * 
	 * @Description: 查找特定监控视图
	 * @param @param
	 *            monitorView
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            session
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(MonitorViews monitorView, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		MonitorViews result = monitorViewsLogic.select(monitorView);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseBody
	public void search(String country, String province, String city, String key, String userCode, String type,
			String language, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		List<MonitorViews> list = monitorViewsLogic.search(key, userCode, type);
		if (list != null) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}

	}

	@RequestMapping(value = "/selectcustomerlist")
	@ResponseBody
	public void selectCompanyMonitor(String language, String customerName, HttpServletRequest request, String key,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("language", language);
		map.put("customerName", customerName);
		String userType = getUserInfo(request).get(USER_TYPE);
		if (userType.equals(CommonUtils.ADMIN_TYPE)) {

		} else if (userType.equals(CommonUtils.INTEGRATOR_TYPE)) {
			map.put("integratorCode", getUserInfo(request).get(INTEGRATOR_CODE));
		} else if (userType.equals(CommonUtils.OEM_TYPE)) {
			map.put("oemCode", getUserInfo(request).get(OEM_CODE));
		} else if (userType.equals(CommonUtils.AGENT_TYPE)) {
			map.put("agentCode", getUserInfo(request).get(COM_CODE));
		}

		List<Map<String, String>> list = monitorViewsLogic.selectCompanysMonitors(map, key);
		if (list != null) {
			map.put("list", list);
		}
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping("/statistics")
	@ResponseBody
	public void statistics(String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String userType = getUserInfo(request).get(USER_TYPE);
		Companies company = new Companies();
		Map<String, Object> map = new HashMap<String, Object>();
		if (userType.equals(CommonUtils.ADMIN_TYPE)) {

		} else if (userType.equals(CommonUtils.INTEGRATOR_TYPE)) {
			company.setIntegratorCode(getUserInfo(request).get(INTEGRATOR_CODE));
		} else if (userType.equals(CommonUtils.OEM_TYPE)) {
			company.setOemCode(getUserInfo(request).get(OEM_CODE));
		} else if (userType.equals(CommonUtils.AGENT_TYPE)) {
			company.setAgentCode(getUserInfo(request).get(COM_CODE));
		}
		company.setLanguage(language);
		int customersTotal = companiesLogic.selectCompaniesTotalByOemCode(company);
		int productLinesTotal = productionLinesLogic.selectProductionLineTotalByOemCode(company);
		int workOrdersTotal = alarmWorkOrdersLogic.selectUndoneWorkOrdersTotalByOemCode(company);
		int equipmentTotal = equipmentsLogic.selectEquipmentListByOemCode(company);
		map.put("customersTotal", customersTotal);
		map.put("productLinesTotal", productLinesTotal);
		map.put("workOrdersTotal", workOrdersTotal);
		map.put("equipmentTotal", equipmentTotal);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
}
