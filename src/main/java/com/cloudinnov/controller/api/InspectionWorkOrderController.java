package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AuthUserLogic;
import com.cloudinnov.logic.EquipmentInspectionWorkItemLogic;
import com.cloudinnov.logic.InspectionWorkOrderLogic;
import com.cloudinnov.model.AuthUsers;
import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.EquipmentInspectionWorkItem;
import com.cloudinnov.model.InspectionWorkOrder;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author guobo
 * @date 2016年12月6日 下午2:57:28
 * @email boguo@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/inspectionWorkOrder")
public class InspectionWorkOrderController extends BaseController {

	@Autowired
	private InspectionWorkOrderLogic workOrderLogic;

	@Autowired
	private EquipmentInspectionWorkItemLogic workItemLogic;

	@Autowired
	private AuthUserLogic userLogic;

	/**
	 * 巡检工单列表--带分页
	 * 
	 * @param index
	 * @param size
	 * @param iwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "巡检工单管理", methods = "巡检工单列表")
	public void list(int index, int size, InspectionWorkOrder iwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		Page<InspectionWorkOrder> inspectionWorkOrders = workOrderLogic.selectListPage(index, size, iwo, true);

		if (inspectionWorkOrders != null) {
			map.put("list", inspectionWorkOrders);
			map.put("total", inspectionWorkOrders.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 巡检工单搜索
	 * 
	 * @param index
	 * @param size
	 * @param key
	 * @param eiwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "巡检工单管理", methods = "巡检工单搜索")
	public void search(int index, int size, String key, InspectionWorkOrder iwo, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		Page<InspectionWorkOrder> list = workOrderLogic.search(index, size, iwo, key);
		map.put("alarmUntreateds", CommonUtils.DEFAULT_NUM);// 未处理的
		map.put("alarmHandles", CommonUtils.DEFAULT_NUM);// 处理中的
		map.put("alarmCloses", CommonUtils.DEFAULT_NUM);// 已处理的
		map.put("alarmSees", CommonUtils.DEFAULT_NUM);

		if (list.getResult() != null && list.getResult().size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_NEW) {
					map.put("alarmUntreateds", (Integer) map.get("alarmUntreateds") + 1);
				} else if (list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_HANDLE) {
					map.put("alarmHandles", (Integer) map.get("alarmHandles") + 1);
				} else if (list.getResult().get(i).getOrderStatus() == CommonUtils.ORDER_STATUS_CLOSE) {
					map.put("alarmCloses", (Integer) map.get("alarmCloses") + 1);
				}
			}
		}
		String returnData = "";
		map.put("list", list);
		map.put("total", list.getTotal());
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

	/**
	 * 根据configCode 查询所有工作项
	 * 
	 * @param index
	 * @param size
	 * @param entity
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/searchItemsByCode", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog()
	public void searchItemsByCode(int index, int size, EquipmentInspectionConfig entity, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<EquipmentInspectionWorkItem> items = workItemLogic.selectByConfigCode(index, size, entity);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (items != null) {
			map.put("list", items);
			map.put("total", items.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 巡检工单查询--单条数据
	 * 
	 * @param iwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "巡检工单管理", methods = "巡检工单查询--单条数据")
	public void select(InspectionWorkOrder iwo, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		InspectionWorkOrder inspectionWorkOrder = workOrderLogic.select(iwo);
		String returnData = "";
		if (inspectionWorkOrder != null) {
			map.put("model", inspectionWorkOrder);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 巡检工单转派
	 * 
	 * @param eiwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/turnsend", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "巡检工单管理", methods = "巡检工单转派")
	public void turnsend(InspectionWorkOrder iwo, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = workOrderLogic.update(iwo);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == CommonUtils.SUCCESS_NUM) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 工单提醒
	 * 
	 * @param iwo
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/reminded")
	@ResponseBody
	public void reminded(InspectionWorkOrder iwo, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		iwo.setPaddingBy(getUserInfo(request).get(USER_CODE));
		InspectionWorkOrder model = workOrderLogic.select(iwo);
		if (model != null) {
			AuthUsers user = new AuthUsers();
			user.setCode(model.getPaddingBy());
			user = userLogic.select(user);
			if (user != null) {
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
}
