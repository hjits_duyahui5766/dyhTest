package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.CompaniesDao;
import com.cloudinnov.dao.MonitorViewsDao;
import com.cloudinnov.logic.MonitorViewsLogic;
import com.cloudinnov.model.MonitorViews;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年2月18日下午1:31:03
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class MonitorViewsLogicImpl extends BaseLogicImpl<MonitorViews> implements MonitorViewsLogic {

	private int digit = 5;
	@Autowired
	private MonitorViewsDao monitorViewDao;

	@Autowired
	private CompaniesDao companyDao;

	public int save(MonitorViews monitorView) {
		monitorView.setCode(CodeUtil.monitorCode(digit));
		int returnCode = monitorViewDao.insert(monitorView);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = monitorViewDao.insertInfo(monitorView);
		}
		return returnCode;
	}

	public int update(MonitorViews monitorView) {
		int returnCode = monitorViewDao.updateByCondition(monitorView);
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			returnCode = monitorViewDao.updateInfoByCondition(monitorView);
		}
		return returnCode;
	}

	@Override
	public List<MonitorViews> search(String key, String userCode, String type) {
		// PageHelper.startPage(index, size);
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", key);
		map.put("type", type);
		map.put("userCode", userCode);
		return monitorViewDao.search(map);
	}

	@Override
	public List<Map<String, String>> selectCompanysMonitors(Map<String, Object> map, String key) {
		map.put("name", key);
		return companyDao.selectCompanysMonitors(map);
	}
}
