package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.MonitorViews;

/**
 * @author guochao
 * @date 2016年2月18日下午12:27:25
 * @email chaoguo@cloudinnov.com
 * @remark 业务层接口
 * @version
 */
public interface MonitorViewsLogic extends IBaseLogic<MonitorViews> {
	public List<MonitorViews> search(String key, String userCode, String type);

	public List<Map<String, String>> selectCompanysMonitors(Map<String, Object> map, String key);
}
