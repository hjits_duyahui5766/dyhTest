package com.cloudinnov.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.ProductionLineEquipmentsDao;
import com.cloudinnov.logic.ProductionLineEquipmentsLogic;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLineEquipments;
import com.cloudinnov.model.ProductionLines;

/**
 * @author nilixin
 * @date 2016年2月17日上午11:50:41
 * @email
 * @remark
 * @version
 */
@Service
public class ProductionLineEquipmentsLogicImpl extends BaseLogicImpl<ProductionLineEquipments> implements
		ProductionLineEquipmentsLogic {

	@Autowired
	private ProductionLineEquipmentsDao productionLineEquipmentsDao;

	public int save(ProductionLineEquipments productionLineEquipments) {
		productionLineEquipments.setStatus(1);
		return productionLineEquipmentsDao.insert(productionLineEquipments);
	}
	
	@Override
	public List<Equipments> selectEquCodeNameByProLineCode(
			String prolineCodes,String language) {
		if(prolineCodes == null){
			return null;
		}
		String[] codes = prolineCodes.split(",");
		ProductionLines productionLine = null;
		List<Equipments> list = new ArrayList<Equipments>();
		for(int i = 0;i<codes.length;i++){
			productionLine = new ProductionLines();
			productionLine.setCode(codes[i]);
			productionLine.setLanguage(language);
			List<Equipments> result = productionLineEquipmentsDao.selectEquCodeNameByProLineCode(productionLine);
			list.addAll(result);
		}
		return list;
	}
}
