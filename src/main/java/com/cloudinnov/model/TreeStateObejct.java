package com.cloudinnov.model;

/**
 * @author guochao
 * @date 2016年3月22日下午4:50:59
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
public class TreeStateObejct {
	private boolean opened;
	private boolean selected;

	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
