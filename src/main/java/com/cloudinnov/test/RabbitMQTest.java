package com.cloudinnov.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/applicationContext.xml", "classpath*:/kafka.xml",
		"classpath*:/spring-mongodb.xml" })
public class RabbitMQTest {
	@Autowired
	private AmqpTemplate fireTemplate;

	@Test
	public void testMongodb() {
		// TODO First, create an emergency call entity class, the fire alarm Server receive data, to
		// the emergency telephone entity class assignment, and then stored in RabbitMQ
		fireTemplate.convertAndSend("火灾报警数据");
	}
}
