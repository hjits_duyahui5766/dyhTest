package com.cloudinnov.dao.mongo;

import org.springframework.stereotype.Repository;

import com.cloudinnov.model.FireCRTEvent;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月7日下午3:41:29
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Repository("fireCRTEventMongoDBDao")
public class FireCRTEventMongoDBDao extends BaseMongoDAOImpl<FireCRTEvent> {
	
}
