package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.LightDetect;

public interface LightDetectLogic {

	List<LightDetect> lightDetectIpInfo();
	
	//获取能见度指示器的ip和port
	List<EquipmentsAttr> selectLightByEquipCode(String equipCode);
	
	public String getLightValue(String equipCode);
	
	public Map<String, Object> selectLightReportDatafromMongoDB(Map<String, String> map,String type); 
	
	public List<Equipments>getEquipmentInfoByCategoryInfo(String categoryCode);
	
	public Map<String, Object> getEnvironmentDataByEquipCode(String equipCode);
	
	public Map<String, Object> selectNoReportDatafromMongoDB(Map<String, String> map, String type);
	
	public Map<String, Object> selectCOVIReportDatafromMongoDB(Map<String, String> map, String type);

	public Map<String, Object> selectFsfxReportDatafromMongoDB(Map<String, String> map, String type);
	
	public Map<String, Object> selectIntensityReportDatafromMongoDB(Map<String, String> map, String type);
}
