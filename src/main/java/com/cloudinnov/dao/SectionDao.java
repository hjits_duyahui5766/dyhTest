package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Section;

public interface SectionDao extends IBaseDao<Section> {
	public int deleteByPrimaryKey(Integer id);

	public int insert(Section record);

	public int insertInfo(Section section);

	public List<Section> search(Map<String, String> map);
	
	public Section searchSectionInfo(String equipCode);

	public int insertSelective(Section record);

	public Section selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(Section record);

	public int updateByPrimaryKey(Section record);
	
	public List<Section> selectSectionEquipmentPoint( );
	
	public List<Section> selectSectionEquipmentNodeClassPoint( );
	
	public Section selectSectionByCode(String code);
	
}