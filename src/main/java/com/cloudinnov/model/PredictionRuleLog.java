package com.cloudinnov.model;

import java.util.List;

/*
 * 预案执行记录表
 */
public class PredictionRuleLog {
	private Integer id;
	private String code;	//预案记录的code
	private String predictionName;	//预案的名称
	private String accidentType;	//事故类型
	private String accidentArea;	//事故发生区域
	private String triggerType;		//预案触发的类型:手动和自动
	private String triggerTime;		//预案触发的时间
	private String alarmEquipment;		//预案触发的设备名称
	private List<PredictionContacter> predictionContacters;		//预案触发时的紧急联系人
	private List<PredictionRuleLogDetails> predictionRuleLogDetails;		//预案记录的详细情况
	private String predictionRuleLogDetailStr;		//预案记录的详细情况的string值
	private String comment;	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPredictionName() {
		return predictionName;
	}
	public void setPredictionName(String predictionName) {
		this.predictionName = predictionName;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public String getAccidentArea() {
		return accidentArea;
	}
	public void setAccidentArea(String accidentArea) {
		this.accidentArea = accidentArea;
	}
	public String getTriggerType() {
		return triggerType;
	}
	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}
	public String getTriggerTime() {
		return triggerTime;
	}
	public void setTriggerTime(String triggerTime) {
		this.triggerTime = triggerTime;
	}
	public String getAlarmEquipment() {
		return alarmEquipment;
	}
	public void setAlarmEquipment(String alarmEquipment) {
		this.alarmEquipment = alarmEquipment;
	}
	public List<PredictionContacter> getPredictionContacters() {
		return predictionContacters;
	}
	public void setPredictionContacters(List<PredictionContacter> predictionContacters) {
		this.predictionContacters = predictionContacters;
	}
	public List<PredictionRuleLogDetails> getPredictionRuleLogDetails() {
		return predictionRuleLogDetails;
	}
	public void setPredictionRuleLogDetails(List<PredictionRuleLogDetails> predictionRuleLogDetails) {
		this.predictionRuleLogDetails = predictionRuleLogDetails;
	}
	public String getPredictionRuleLogDetailStr() {
		return predictionRuleLogDetailStr;
	}
	public void setPredictionRuleLogDetailStr(String predictionRuleLogDetailStr) {
		this.predictionRuleLogDetailStr = predictionRuleLogDetailStr;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
