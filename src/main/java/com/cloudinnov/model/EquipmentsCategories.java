package com.cloudinnov.model;

public class EquipmentsCategories extends BaseModel {

	private String oemCode;

	private String name;

	private int parentId;

	private int grade;

	private String poropertyList;

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	private String comment;

	public String getOemCode() {
		return oemCode;
	}

	public void setOemCode(String oemCode) {
		this.oemCode = oemCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPoropertyList() {
		return poropertyList;
	}

	public void setPoropertyList(String poropertyList) {
		this.poropertyList = poropertyList;
	}

}