
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes
#pragma warning(disable:4996)


#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//#include <afxcontrolbars.h>     // MFC support for ribbons and control bars
//#include "Platform.h"
#include "tmcp_interface_sdk.h"
#include <map>
using namespace std;


#define WM_PTZ				WM_USER + 100
#define WM_STOPPLAY			WM_USER + 101
#define WM_FOCUS			WM_USER + 102
#define WM_CAPPIC			WM_USER + 103
#define WM_STARTPLAY		WM_USER + 104
#define WM_QUERYRECORD		WM_USER + 105
#define WM_PLAYRECORD		WM_USER + 106
#define WM_RECORDMENU		WM_USER + 107
#define PLAY_START			10001 //开始播放 
#define PLAY_PAUSE			10002 //暂停播放
#define PLAY_FAST			10003 //加快播放速度
#define PLAY_SLOW			10004 //减慢播放速度
#define PLAY_OFFSET			10005 //播放定位


#define ALARM_CODE_VIDEO_LOSE				10000
#define ALARM_CODE_VIDEO_MOVE				10002
#define ALARM_CODE_VIDEO_COVER				10001
#define ALARM_CODE_ACROSS_LINE				203
#define ALARM_CODE_ENTER_REGION				204
#define ALARM_CODE_EXIT_REGION				205
#define ALARM_CODE_ALARM_INTRUTION			206
#define ALARM_CODE_TAKE_LEAVE				207
#define ALARM_CODE_LOITER					208
#define ALARM_CODE_PARKING					209
#define ALARM_CODE_SPEED_DETECT				210
#define ALARM_CODE_AGGREGATION				211
#define ALARM_CODE_ASSAULT					212
#define ALARM_CODE_PEOPLE_NUMBER			213
#define ALARM_CODE_BLOCK					214
#define ALARM_CODE_APPROACH_PARKING			215
#define ALARM_CODE_REGRESS					216
#define ALARM_CODE_PASSER					217
#define ALARM_CODE_THROW					218
#define ALARM_CODE_SMOKE					219
#define ALARM_CODE_OPTICAL_FIBER			220
#define ALARM_CODE_SENSOR_ALARM				400
#define ALARM_CODE_BOX_ALARM				600
#define ALARM_CODE_VRM						222
using namespace Platform;
// 控制中心的属性名
// namespace	Cell
// {
// 	const char* const ControlUnitID = "ControlUnitID";			// 控制中心ID
// 	const char* const ControlUnitName = "ControlUnitName";		// 控制中心名称
// 	const char* const UpControlUnitID = "UpControlUnitID";		// 上级控制中心ID
// }
// 
// //区域的属性名
// namespace  Region
// {
// 	const char* const region_id = "region_id";					// 区域ID
// 	const char* const region_name = "region_name";				// 区域名称
// 	const char* const region_high = "region_high";				// 上级区域ID
// 	const char* const region_level = "region_level";			// 区域等级
// 	const char* const region_cellLsh = "region_cellLsh";		// 区域所属控制中心ID
// }
// 
// 
// // 设备的属性名
// namespace  Device
// {
// 	const char* const device_id = "device_id";					// 设备ID
// 	const char* const device_name = "device_name";				// 设备名称
// 	const char* const device_type = "device_type";				// 设备类型
// 	const char* const device_state = "device_state";			// 设备状态 0-在线 1-断线
// 	const char* const device_talk = "device_talk";				// 对讲通道数
// 	const char* const device_chan =  "device_chan";				// 通道个数
// 	const char* const ip_address = "ip_address";				// 设备IP地址
// 	const char* const device_port = "device_port";				// 设备端口
// 	const char* const cell_id = "cell_id";						// 所属区域ID
// 	const char* const device_alarmin = "device_alarmin";		// 报警输入个数
// 	const char* const device_alarmout = "device_alarmout";		// 报警输出个数
// }
// 
// // 摄像头设备的属性名
// namespace  Camera
// {
// 	const char* const category_id = "category_id";			// 类别ID（0-枪机/1-半球/2-快球/3-云台）
// 	const char* const device_id = "device_id";				// 摄像头ID
// 	const char* const device_name = "device_name";			// 摄像头名称
// 	const char* const device_type = "device_type";			// 设备类型，摄像头固定为0x3
// 	const char* const inport = "inport";					// 通道号
// 	const char* const ConnectType = "ConnectType";			// 连接类型（0-TCP/1-UDP/2-MCAST/3-RTP）
// 	const char* const controlunit_id = "controlunit_id";	// 所属控制中心ID
// 	const char* const parent_device_id = "parent_device_id";// 父设备ID，即编码器ID
// 	const char* const region_id = "region_id";				// 所属区域ID
// }
// 
// // 报警输入的属性名
// namespace  Alarmin
// {
// 	const char* const IO_id = "IO_id";								// IO信息
// 	const char* const device_id = "device_id";						// IO ID
// 	const char* const device_name = "device_name";					// IO名称
// 	const char* const device_type = "device_type";					// 设备类型，报警输入固定为0x4
// 	const char* const iochanindex = "iochanindex";					// 通道号
// 	const char* const parent_device_id = "parent_device_id";		// 父设备ID
// 	const char* const region_id = "region_id";						// 所属区域ID
// }
// 
// // 报警输出的属性名
// namespace  Alarmout
// {
// 	const char* const IO_id = "IO_id";								// IO信息
// 	const char* const device_id = "device_id";						// IO ID
// 	const char* const device_name = "device_name";					// IO名称
// 	const char* const device_type = "device_type";					// 设备类型，报警输出固定为0x5
// 	const char* const iochanindex = "iochanindex";					// 通道号
// 	const char* const parent_device_id = "parent_device_id";		// 父设备ID
// 	const char* const region_id = "region_id";						// 所属区域ID
// }
// 
// // 录像文件的属性名
// namespace  RecordFile
// {
// 	const char* const starttime = "starttime";					// 开始时间
// 	const char* const endtime = "endtime";						// 结束时间
// 	const char* const type = "type";							// 存储类型
// 	const char* const filename = "filename";					// 文件名
// 	const char* const size = "size";							// 文件大小（字节）
// }

typedef struct CRUISEPOINT
{
	BYTE PresetNum; //预置点
	BYTE Dwell;  //停留时间
	BYTE Speed;  //速度
	BYTE Reserve; //保留
	CRUISEPOINT()
	{
		PresetNum = 0; //预置点
		Dwell     = 0; //停留时间
		Speed     = 0; //速度
		Reserve   = 0; //保留
	}
}CRUISEPOINT, *LPCRUISEPOINT;

typedef struct NET_MULTI_SDK_CRUISE_RET{
	CRUISEPOINT struCruisePoint[32];   //最大支持32个巡航点
	NET_MULTI_SDK_CRUISE_RET()
	{
		memset(&struCruisePoint, 0, sizeof(CRUISEPOINT));
	}
}NET_MULTI_SDK_CRUISE_RET, *LPNET_MULTI_SDK_CRUISE_RET;


#ifdef _DEBUG
#pragma comment(lib, "tmcp_interface_sdk.lib")
#else
#pragma comment(lib, "tmcp_interface_sdk.lib")
#endif // _DEBUG


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


