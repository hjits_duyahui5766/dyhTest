package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.AlarmWorkOrdersLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.ProductionLinesLogic;
import com.cloudinnov.model.AlarmWorkOrders;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLines;

/**
 * @author chengning
 * @date 2016年3月16日下午4:00:14
 * @email ningcheng@cloudinnov.com
 * @remark 我的工作台
 * @version
 */
@Controller
@RequestMapping("/webapi/operPlatform")
public class OperPlatformController extends BaseController {

	@Autowired
	private AlarmWorkOrdersLogic alarmWorkOrdersLogic;

	@Autowired
	private EquipmentsLogic equipmentsLogic;

	@Autowired
	private ProductionLinesLogic productionLinesLogic;

	/**
	 * monitorCenter
	 * 
	 * @Description: 我的监控中心
	 * @param @param
	 *            productionLines
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping("/monitorCenter")
	@ResponseBody
	public void monitorCenter(ProductionLines productionLines, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		productionLines.setCustomerCode(getUserInfo(request).get(COM_CODE));
		Map<String, List<?>> map = new HashMap<String, List<?>>();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> alarmMap = new HashMap<String, Object>();
		alarmMap.put("name", "总产量");
		alarmMap.put("unit", "吨");
		alarmMap.put("value", "3345");
		list.add(alarmMap);
		
		alarmMap = new HashMap<String, Object>();
		alarmMap.put("name", "总运行时长");
		alarmMap.put("unit", "小时");
		alarmMap.put("value", "45");
		list.add(alarmMap);

		alarmMap = new HashMap<String, Object>();
		alarmMap.put("name", "总耗电量");
		alarmMap.put("unit", "千瓦/小时");
		alarmMap.put("value", "4500");
		list.add(alarmMap);

		map.put("summary", list);

		List<ProductionLines> lines = productionLinesLogic.selectProLinesByCustomerCode(productionLines);

		map.put("productionline", lines);
		dataMap.put("data", map);
		String returnData = returnJsonAllRequest(request, response, dataMap, SUCCESS, null);
		response.getWriter().print(returnData);
	}
	
	/**
	 * monitorCenterBasicData
	 * 
	 * @Description: 我的监控中心基础数据,返回客户下的所有产线基础数据
	 * @param @param
	 *            productionLines
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping("/monitorCenterBasic")
	@ResponseBody
	public void monitorCenterBasicData(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		productionLine.setCustomerCode(getUserInfo(request).get(COM_CODE));
		List<ProductionLines> lines = productionLinesLogic.selectProLineInfoByCode(productionLine);
		
		map.put("list", lines);		
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		response.getWriter().print(returnData);
	}
	
	/**
	 * monitorCenterDetail
	 * 
	 * @Description: 我的监控中心,通过产线code来获取该产线的详细信息，包含产线信息，以及产线下的设备，点位等数据
	 * @param @param
	 *            productionLines
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping("/monitorCenterDetail")
	@ResponseBody
	public void monitorCenterDetail(ProductionLines productionLine, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		productionLine.setCustomerCode(getUserInfo(request).get(COM_CODE));
		List<ProductionLines> lines = productionLinesLogic.selectProLinesByCustomerCode(productionLine);
		map.put("model", lines);		
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		response.getWriter().print(returnData);
	}
	/**
	 * 
	 * 
	 * @Description: 我的工作台 select
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @throws
	 *            IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping("/select")
	@ResponseBody
	public void select(HttpServletRequest request, HttpServletResponse response, AlarmWorkOrders model)
			throws IOException {
		model.setCustomerCode(getUserInfo(request).get(COM_CODE));
		model.setPaddingBy(getUserInfo(request).get(USER_CODE));
		Map<String, Object> map = new HashMap<String, Object>();
		/**
		 * 查询我的工单总数
		 */
		Map<String, Object> alarmMap = alarmWorkOrdersLogic.selecAlarmsByUserCode(model);
		map.put("summary", alarmMap);
		/**
		 * 查询我的设备列表
		 */
		Equipments equipment = new Equipments();
		equipment.setCustomerPerson(getUserInfo(request).get(USER_CODE));
		equipment.setLanguage(model.getLanguage());
		List<Equipments> equipments = equipmentsLogic.selectEquipmentsByUser(equipment);
		map.put("equipments", equipments);
		map.put("equipmentsTotal", equipments.size());
		AlarmWorkOrders alarm = new AlarmWorkOrders();
		alarm.setCustomerCode(getUserInfo(request).get(COM_CODE));
		alarm.setPaddingBy(getUserInfo(request).get(USER_CODE));
		alarm.setLanguage(model.getLanguage());
		List<AlarmWorkOrders> alarms = alarmWorkOrdersLogic.selectAlarmWorksByUser(alarm);
		map.put("alarms", alarms);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		response.getWriter().print(returnData);

	}
}
