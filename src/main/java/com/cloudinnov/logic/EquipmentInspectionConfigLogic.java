package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.ProductionLineEquipments;
import com.github.pagehelper.Page;

public interface EquipmentInspectionConfigLogic extends IBaseLogic<EquipmentInspectionConfig> {
	public List<EquipmentInspectionConfig> quartzList();

	public Page<EquipmentInspectionConfig> search(int index, int size, String country, String province, String city,
			String key, String oemCode, String integratorCode, String type, String language,
			ProductionLineEquipments ple);
	
	public EquipmentInspectionConfig moblieSelect(EquipmentInspectionConfig entity);
	
	public Map<String, Object> getConfigStateByEquipmentCode(EquipmentInspectionConfig entity);
	
	public List<EquipmentInspectionConfig> getConfigListByEquipmentCode(EquipmentInspectionConfig entity);
	
}
