package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.SparepartPreventionConditionLogic;
import com.cloudinnov.logic.SparepartPreventionConfigLogic;
import com.cloudinnov.model.SparepartPreventionCondition;
import com.cloudinnov.model.SparepartPreventionConfig;

@Controller
@RequestMapping("webapi/prevention")
public class SparepartPreventionConfigController extends BaseController {
	
	@Autowired
	private SparepartPreventionConfigLogic sparepartPreventionConfigLogic;
	@Autowired
	private SparepartPreventionConditionLogic sparepartPreventionConditionLogic;
	
	/*@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "预防性维护", methods = "维护条件保存")
	public void save(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition,HttpServletRequest request, 
			HttpServletResponse response,HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		int result = sparepartPreventionConfigLogic.save(sparepartPreventionConfig,sparepartPreventionCondition);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}*/
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "预防性维护", methods = "维护条件保存")
	public void save(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition,HttpServletRequest request, 
			HttpServletResponse response,HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		int result = sparepartPreventionConfigLogic.save(sparepartPreventionConfig,sparepartPreventionCondition);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@ResponseBody
	public void select(SparepartPreventionConfig sparepartPreventionConfig,HttpServletRequest request, 
			HttpServletResponse response,HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		SparepartPreventionConfig result = sparepartPreventionConfigLogic.selectBySparepartCode(sparepartPreventionConfig);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;

		if (result != null) {
			map.put("model", result);
			int configId = result.getId();
			List<SparepartPreventionCondition> list = sparepartPreventionConditionLogic.selectListByConfigId(configId);
			if(list!=null){
				map.put("conditionList", list);
			}
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "预防性维护", methods = "维护条件修改")
	public void update(SparepartPreventionConfig sparepartPreventionConfig,SparepartPreventionCondition sparepartPreventionCondition,HttpServletRequest request, 
			HttpServletResponse response,HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
		int result = sparepartPreventionConfigLogic.update(sparepartPreventionConfig,sparepartPreventionCondition);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
}
