package com.cloudinnov.model;

public class CustomReportTime extends BaseModel{

    private String reportCode;

    private String beginTime;

    private String endTime;

    private String summaryUnit;

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSummaryUnit() {
        return summaryUnit;
    }

    public void setSummaryUnit(String summaryUnit) {
        this.summaryUnit = summaryUnit;
    }

}