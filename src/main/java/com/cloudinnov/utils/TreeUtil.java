package com.cloudinnov.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cloudinnov.model.TreeObject;

/**
 * @author guochao
 * @date 2016年3月21日上午11:46:34
 * @email chaoguo@cloudinnov.com
 * @remark 把一个list集合,里面的bean含有 parentId 转为树形式
 * @version
 */
public class TreeUtil {

	/**
	 * getChildTreeObjects
	 * 
	 * @Description: 根据父节点的ID获取所有子节点
	 * @param @param list
	 * @param @param praentId
	 * @param @return 参数
	 * @return List<TreeObject> 返回类型
	 */
	public List<TreeObject> getChildTreeObjects(List<TreeObject> list,
			int praentId) {
		List<TreeObject> returnList = new ArrayList<TreeObject>();
		for (Iterator<TreeObject> iterator = list.iterator(); iterator
				.hasNext();) {
			TreeObject t = (TreeObject) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId().equals(praentId)) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}

	/**
	 * recursionFn
	 * 
	 * @Description: 把一个list集合,里面的bean含有 parentId 转为树形式
	 * @param @param list
	 * @param @param t 参数
	 * @return void 返回类型
	 */
	private void recursionFn(List<TreeObject> list, TreeObject t) {
		List<TreeObject> childList = getChildList(list, t);// 得到子节点列表
		t.setChildren(childList);
		for (TreeObject tChild : childList) {
			if (hasChild(list, tChild)) {// 判断是否有子节点
				// returnList.add(TreeObject);
				Iterator<TreeObject> it = childList.iterator();
				while (it.hasNext()) {
					TreeObject n = (TreeObject) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	// 得到子节点列表
	private List<TreeObject> getChildList(List<TreeObject> list, TreeObject t) {

		List<TreeObject> tlist = new ArrayList<TreeObject>();
		Iterator<TreeObject> it = list.iterator();
		while (it.hasNext()) {
			TreeObject n = (TreeObject) it.next();
			if (n.getParentId().equals(t.getSelectId())) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	List<TreeObject> returnList = new ArrayList<TreeObject>();

	/**
	 * getChildTreeObjects
	 * 
	 * @Description: 根据父节点的ID获取所有子节点
	 * @param @param list
	 * @param @param typeId 父节点id
	 * @param @param prefix 前缀
	 * @param @return 参数
	 * @return List<TreeObject> 返回类型
	 */
	public List<TreeObject> getChildTreeObjects(List<TreeObject> list,
			int typeId, String prefix) {
		if (list == null)
			return null;
		for (Iterator<TreeObject> iterator = list.iterator(); iterator
				.hasNext();) {
			TreeObject node = (TreeObject) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (node.getParentId().equals(typeId)) {
				recursionFn(list, node, prefix);
			}
			// 二、遍历所有的父节点下的所有子节点
			/*
			 * if (node.getParentId()==0) { recursionFn(list, node); }
			 */
		}
		return returnList;
	}

	private void recursionFn(List<TreeObject> list, TreeObject node, String p) {
		List<TreeObject> childList = getChildList(list, node);// 得到子节点列表
		if (hasChild(list, node)) {// 判断是否有子节点
			returnList.add(node);
			Iterator<TreeObject> it = childList.iterator();

			while (it.hasNext()) {
				TreeObject n = (TreeObject) it.next();
				n.setName(p + n.getName());
				recursionFn(list, n, p + p);
			}
		} else {
			returnList.add(node);
		}
	}

	// 判断是否有子节点
	private boolean hasChild(List<TreeObject> list, TreeObject t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}

}
