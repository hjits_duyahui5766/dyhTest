package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.CustomReportDimensionLogic;
import com.cloudinnov.model.CustomReportDimension;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;

/**
 * 
 * @author GuoBo
 *
 */
@Controller
@RequestMapping("/webapi/customReportDimension")
public class CustomReportDimensionController extends BaseController {
	@Autowired
	private CustomReportDimensionLogic customReportDimensionLogic;

	/**
	 * 报表维度配置-添加
	 * 
	 * @param customReportDimension
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/save")
	@ResponseBody
	@SystemLog(module = "报表维度配置", methods = "添加")
	public void save(CustomReportDimension customReportDimension, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		int result = customReportDimensionLogic.save(customReportDimension);
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表维度配置-删除
	 * 
	 * @param customReportDimension
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表维度配置", methods = "删除")
	public void delete(CustomReportDimension customReportDimension, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportDimensionLogic.delete(customReportDimension);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表维度配置-修改
	 * 
	 * @param customReportDimension
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表维度配置", methods = "修改")
	public void update(CustomReportDimension customReportDimension, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportDimensionLogic.update(customReportDimension);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");

		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表维度配置-查询
	 * 
	 * @param customReportDimension
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	@SystemLog(module = "报表维度配置", methods = "查询")
	public void listPage(CustomReportDimension customReportDimension, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		Page<CustomReportDimension> customReportDimensions = customReportDimensionLogic.selectListPage(index, size,
				customReportDimension, false);
		Map<String, Object> map = new HashMap<String, Object>();
		if (customReportDimensions != null) {
			map.put("list", customReportDimensions);
			map.put("total", customReportDimensions.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

}
