package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.CustomReportTimeLogic;
import com.cloudinnov.model.CustomReportTime;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/customReportTime")
public class CustomReportTimeController extends BaseController {
	@Autowired
	private CustomReportTimeLogic customReportTimeLogic;

	/**
	 * 报表时间配置-添加
	 * 
	 * @param customReportTime
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表时间配置", methods = "添加")
	public void save(CustomReportTime customReportTime, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportTimeLogic.save(customReportTime);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表时间配置-删除
	 * 
	 * @param customReportTime
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "报表时间配置", methods = "删除")
	public void delete(CustomReportTime customReportTime, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportTimeLogic.delete(customReportTime);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表时间配置-修改
	 * 
	 * @param customReportTime
	 *            参数
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "报表时间配置", methods = "修改")
	public void update(CustomReportTime customReportTime, HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		int result = customReportTimeLogic.update(customReportTime);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == 1) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	/**
	 * 报表时间配置-查询（分页）
	 * 
	 * @param customReportTime
	 *            参数
	 * @param index
	 * @param size
	 * @param request
	 * @param response
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void listPage(CustomReportTime customReportTime, int index, int size, HttpServletRequest request,
			HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		Page<CustomReportTime> customReportTimes = customReportTimeLogic.selectListPage(index, size, customReportTime,
				false);
		Map<String, Object> map = new HashMap<String, Object>();
		if (customReportTimes != null) {
			map.put("list", customReportTimes);
			map.put("total", customReportTimes.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

}
