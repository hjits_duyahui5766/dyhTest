package com.cloudinnov.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;
 

public class WPCarClientUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static boolean isConnect = false;
	private static Socket socket;
	private static String ip="192.168.22.234";
	private static int port = 965;
	private static final String CODEDFORMAT = "gb2312";
	private static final int REVEIVEBUFFERSIZE = 1024 * 8;// 接收缓存8k
	
	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
		startConnectService();
	}

	public static void startConnectService() {

		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			// TODO: handle exception
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			System.err.println("连接成功");
		} else {
			connectAgain();

		}

	}

	public static void connectAgain() {
		try {
			socket = new Socket(ip, port);
			isConnect = true;

		} catch (Exception e) {
			// TODO: handle exception
			isConnect = false;
		}
		if (isConnect) {
			// new ThreadWriter(socket).start();
			new ThreadReader(socket).start();
			System.err.println("连接成功");
		} else {
			connectAgain();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static class ThreadReader extends Thread {
		Socket socket;

		public ThreadReader(Socket socket) {
			this.socket = socket;
			this.setName("ThreadReader");
		}

		@Override
		public void run() {
 
			InputStream inputStream = null;
			InputStreamReader inputStreamReader = null;
			BufferedReader bufferedReader = null;
			
			
			try {
				inputStream = (InputStream) socket.getInputStream();// 获取一个输入流，接收服务端的信息
				inputStreamReader = new InputStreamReader(inputStream, CODEDFORMAT);// 包装成字符流，提高效率,gbk
				bufferedReader = new BufferedReader(inputStreamReader);// 缓冲区
				char[] rcchar = new char[REVEIVEBUFFERSIZE];
				// TCP长连接，十秒一个心跳数据包，连接断开会抛出异常退出接收线程
				while (true) {// 长连接读取CRT信息，连接断开会抛出异常
					int len = bufferedReader.read(rcchar);// 堵塞读取
					if (len == -1) {
						throw new Exception("socket连接出错");
					}
					String crtstrs = String.valueOf(rcchar, 0, len);
					System.err.println(crtstrs);
					 
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static class ThreadWriter extends Thread {
		Socket socket;

		public ThreadWriter(Socket socket) {
			this.socket = socket;
			this.setName("ThreadWriter");
		}

		@Override
		public void run() {
			try {
				while (true) {
					socket.sendUrgentData(0xff);
					Thread.sleep(2000);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.err.println("网络断线");
			}
		}
	}

	public static void getCurrentThread() {
		for (Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
			Thread thread = entry.getKey();

			StackTraceElement[] stackTraceElements = entry.getValue();

			if (thread.equals(Thread.currentThread())) {
				continue;
			}

			System.out.println("\n线程： " + thread.getName() + "\n");
			for (StackTraceElement element : stackTraceElements) {
				System.out.println("\t" + element + "\n");
			}
		}
	}
	
	public static void parseCarData(String result) {
		 
		try {
			String[] dataArr = result.split(",");
			String deviceId=dataArr[0];
			String period=dataArr[1];
			String recoderId=dataArr[2];
			String time=dataArr[3];
			
			String firstLanelOneKind=dataArr[4];
			String firstLanelOneKindAvg=dataArr[5];
			String firstLanelTwoKind=dataArr[6];
			String firstLanelTwoKindAvg=dataArr[7];
			String firstLanelThreeKind=dataArr[8];
			String firstLanelThreeKindAvg=dataArr[9];
			String firstLanelFourKind=dataArr[10];
			String firstLanelFourKindAvg=dataArr[11];
			
			String secondLanelOneKind=dataArr[12];
			String secondLanelOneKindAvg=dataArr[13];
			String secondLanelTwoKind=dataArr[14];
			String secondLanelTwoKindAvg=dataArr[15];
			String secondLanelThreeKind=dataArr[16];
			String secondLanelThreeKindAvg=dataArr[17];
			String secondLanelFourKind=dataArr[18];
			String secondLanelFourKindAvg=dataArr[19];
			
			String thirdLanelOneKind=dataArr[20];
			String thirdLanelOneKindAvg=dataArr[21];
			String thirdLanelTwoKind=dataArr[22];
			String thirdLanelTwoKindAvg=dataArr[23];
			String thirdLanelThreeKind=dataArr[24];
			String thirdLanelThreeKindAvg=dataArr[25];
			String thirdLanelFourKind=dataArr[26];
			String thirdLanelFourKindAvg=dataArr[27];
			
			String fourthLanelOneKind=dataArr[28];
			String fourthLanelOneKindAvg=dataArr[29];
			String fourthLanelTwoKind=dataArr[30];
			String fourthLanelTwoKindAvg=dataArr[31];
			String fourthLanelThreeKind=dataArr[32];
			String fourthLanelThreeKindAvg=dataArr[33];
			String fourthLanelFourKind=dataArr[34];
			String fourthLanelFourKindAvg=dataArr[35];
			
			String firstLanelOccu=dataArr[36];
			String secondLaneOccu=dataArr[37];
			String thirdLaneOccu=dataArr[38];
			String fourthLaneOccu=dataArr[39];
			System.err.println("解析成功");
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("解析失败");
		}
		 
	}

}
