package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.EquipmentInspectionConfig;

public interface EquipmentInspectionConfigDao extends IBaseDao<EquipmentInspectionConfig> {
	int insert(EquipmentInspectionConfig record);

	List<EquipmentInspectionConfig> quartzList(String condition);

	public List<EquipmentInspectionConfig> search(Map<String, Object> map);
}