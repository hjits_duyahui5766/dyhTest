package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthUsers;

public interface AuthResourceDao extends IBaseDao<AuthResource> {
	
	
	public int insertInfo(AuthResource authResource);
	
	public int updateInfoByCondition(AuthResource authResource);

	public List<AuthResource> selectResourceByUser(AuthUsers au);

	public List<AuthResource> selectTreeList(String roleCode,String language);
	
	public List<AuthResource> tableList(Map<String, Object> map);
	
	public List<AuthResource> selectSencondResourceByUser(Map<String, Object> map);
}