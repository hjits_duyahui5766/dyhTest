package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.ProductionSchedule;

/**
 * @author nilixin
 * @date 2016年2月17日下午1:54:43
 * @email 
 * @remark 
 * @version
 */
public interface ProductionScheduleDao extends IBaseDao<ProductionSchedule>{
	
	public ProductionSchedule selectByCode(ProductionSchedule productionSchedules);
	
	/** 查询生产调度表来查询生成调度用于产能报表
	* selectListStatByCondition 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param productionSchedules
	* @param @return    参数
	* @return List<ProductionSchedule>    返回类型 
	*/
	public List<ProductionSchedule> selectListStatByCondition(ProductionSchedule productionSchedules);
	
	 
}
