package com.cloudinnov;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.mongodb.BasicDBObject;

public class TestMethod {

	public static void main(String[] args) {

		final Base64.Decoder decoder = Base64.getDecoder();
		final Base64.Encoder encoder = Base64.getEncoder();
		final String text = "covi";
		byte[] textByte = null;

		try {
			textByte = text.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 编码
		final String encodedText = encoder.encodeToString(textByte);
		System.out.println("编码: " + encodedText);
		// 解码
		try {
			System.out.println("解码: " + new String(decoder.decode(encodedText), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// 1526981217484
		// 1527219809

		// time=1528300805000
		// time=1528387199000
		//
		// time=1528214405000,
		// time=1528300799000,

		// 测试redis时间和当前时间的差值
		long redisValue = 1528387199L;
		long currentTimeMillis = System.currentTimeMillis();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = sdf.format(redisValue * 1000);

		System.out.println("format : " + format);
		System.out.println("当前时间: " + currentTimeMillis);
		System.out.println("时间差: " + (currentTimeMillis / 1000l - redisValue));

		// 获取上个月的第一天
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		System.out.println("上个月第一天：" + format1.format(calendar.getTime()));

		// 获取上个月的最后一天
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar1 = Calendar.getInstance();
		int month = calendar1.get(Calendar.MONTH);
		calendar1.set(Calendar.MONTH, month - 1);
		calendar1.set(Calendar.DAY_OF_MONTH, calendar1.getActualMaximum(Calendar.DAY_OF_MONTH));
		System.out.println("上个月最后一天：" + sf.format(calendar1.getTime()));
	}

	/**
	 * 获取昨天的23:59:59点时间的方法
	 * 
	 * @return
	 * @throws Exception
	 */
	@Test
	public void getBeforeLastMonthdate() throws Exception {
		SimpleDateFormat sdfSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		String endTime = sdfSecond.format(calendar.getTime());
		Date stopTime = sdfSecond.parse(endTime);
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(stopTime);
		System.out.println("endTime: " + oneDayStart.getTime());
	}

	@Test
	public void subStrings() {
		String conditions = "$A21zeu7h=100||$Ac4tkkyf=100&&$Ay9jqzly=100";
		StringBuffer conditionsBuffer = new StringBuffer();
		while (true) {
			if (!conditions.startsWith("&&") && !conditions.startsWith("||")) {
				if (conditions.indexOf("&&") >= 9 && conditions.indexOf("&&") <= 22) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(condition + ",");
				} else if (conditions.indexOf("||") >= 9 && conditions.indexOf("||") <= 22) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(condition + ",");
				}
			} else if (conditions.startsWith("&&")) {
				conditions = conditions.substring(2);
				if (conditions.indexOf("&&") >= 9 && conditions.indexOf("&&") <= 25) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append("&&" + condition + ",");
				} else if (conditions.indexOf("||") >= 9 && conditions.indexOf("||") <= 25) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append("&&" + condition + ",");
				}
			} else if (conditions.startsWith("||")) {
				conditions = conditions.substring(2);
				if (conditions.indexOf("&&") >= 9 && conditions.indexOf("&&") <= 25) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append("||" + condition + ",");
				} else if (conditions.indexOf("||") >= 9 && conditions.indexOf("||") <= 25) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append("||" + condition + ",");
				}
			}
			if ((conditions.length() - conditions.replace("&&", "").length()) / "&&".length() == 1
					&& !conditions.contains("||")) {
				break;
			}
			if ((conditions.length() - conditions.replace("||", "").length()) / "||".length() == 1
					&& !conditions.contains("&&")) {
				break;
			}
		}
		conditionsBuffer.append(conditions);
		System.err.println(conditionsBuffer.toString());
	}

	@Test
	public void subStringConditions() {
		String conditions = "&&$Ac4tkkyf<=30";
		StringBuffer conditionsBuffer = new StringBuffer();
		String stratWith = "";
		while (true) {
			// 判断字符串是开始的内容
			String substring = conditions.substring(0, 2);
			conditions = conditions.substring(2);
			// 以&&开头
			if ("&&".equals(substring)) { // 以&&开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else {
					conditionsBuffer.append(stratWith + conditions);
				}
			} else if ("||".equals(substring)) { // 以||开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else {
					conditionsBuffer.append(stratWith + conditions);
				}
			} else { // 不以&&或||开头
				stratWith = substring;
				if (conditions.contains("&&") && conditions.contains("||")) {
					if (conditions.indexOf("&&") < conditions.indexOf("||")) {
						String condition = conditions.substring(0, conditions.indexOf("&&"));
						conditions = conditions.substring(conditions.indexOf("&&"));
						conditionsBuffer.append(stratWith + condition + ",");
					} else {
						String condition = conditions.substring(0, conditions.indexOf("||"));
						conditions = conditions.substring(conditions.indexOf("||"));
						conditionsBuffer.append(stratWith + condition + ",");
					}
				} else if (conditions.contains("&&") && !conditions.contains("||")) {
					String condition = conditions.substring(0, conditions.indexOf("&&"));
					conditions = conditions.substring(conditions.indexOf("&&"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else if (conditions.contains("||") && !conditions.contains("&&")) {
					String condition = conditions.substring(0, conditions.indexOf("||"));
					conditions = conditions.substring(conditions.indexOf("||"));
					conditionsBuffer.append(stratWith + condition + ",");
				} else {
					conditionsBuffer.append(stratWith + conditions);
				}
			}
			if ((conditions.length() - conditions.replace("&&", "").length()) / "&&".length() == 1
					&& !conditions.contains("||")) {
				conditionsBuffer.append(conditions);
				break;
			}
			if ((conditions.length() - conditions.replace("||", "").length()) / "||".length() == 1
					&& !conditions.contains("&&")) {
				conditionsBuffer.append(conditions);
				break;
			}
			if (!conditions.contains("||") && !conditions.contains("&&")) {
				break;
			}
		}
		System.err.println(conditionsBuffer.toString());
	}

	/**
	 * 去除所有的'&&'和'||'以及'(',')',拆分字符串
	 */
	@Test
	public void subStringTest() {
		String conditions = "($Ay9jqzly<=30&&$Ac4tkkyf<=30)&&($Ay9jqzly>=88||$Ac4tkkyf>=88)";
		conditions = conditions.replaceAll("&&", ",");
		conditions = conditions.replaceAll("\\|", ",");
		conditions = conditions.replaceAll("\\(", ",");
		conditions = conditions.replaceAll("\\)", ",");
		// 去除多个','号存在的情况,只剩下一个
		while (conditions.contains(",,")) {
			conditions = conditions.replaceAll(",,", ",");
		}
		// 去除掉条件中以','开始或以','结尾
		if (conditions.startsWith(",")) {
			conditions = conditions.substring(1);
		}
		if (conditions.endsWith(",")) {
			conditions = conditions.substring(0, conditions.lastIndexOf(","));
		}
		String[] conditionArray = conditions.split(",");
		for (String condition : conditionArray) {
			System.err.println(condition);
		}
	}

	@Test
	public void getMongoDateTime() throws ParseException {
		SimpleDateFormat sdfSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 5);
		String endTime = sdfSecond.format(calendar.getTime());
		Date stopTime = sdfSecond.parse(endTime);

		Calendar oneDayStart = Calendar.getInstance();
		Calendar oneDayEnd = Calendar.getInstance();

		Date startDate = new Date(1528300805000L);
		Date endDate = new Date(1528387199000L);
		oneDayStart.setTime(startDate);
		oneDayEnd.setTime(endDate);

		Date time1 = oneDayStart.getTime();
		Date time2 = oneDayEnd.getTime();
		String string1 = time1.toString();
		String string2 = time2.toString();

		BasicDBObject basicDBObject1 = new BasicDBObject("$gte", string1);
		BasicDBObject basicDBObject2 = new BasicDBObject("$lte", string2);
		BasicDBObject basicDBObject3 = new BasicDBObject("utcTime", basicDBObject1);
		BasicDBObject basicDBObject4 = new BasicDBObject("utcTime", basicDBObject2);
		BasicDBObject[] array = { basicDBObject3, basicDBObject4 };
		System.err.println(array.toString());
	}

	@Test
	public void getMaxAndMinTime() throws ParseException {
		List<String> times = new ArrayList<String>();
		times.add("2018-06-12 09");
		times.add("2018-06-12 08");
		times.add("2018-06-12 07");
		times.add("2018-06-12 06");
		times.add("2018-06-12 05");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH");
		Map<String, String> timeResult = new HashMap<>();
		String maxTime = "";
		String minTime = "";
		for (int i = 0; i < times.size(); i++) {
			if (i == 0) {
				maxTime = times.get(i);
				minTime = times.get(i);
			} else {
				Date standardMaxTime = sdf.parse(maxTime);
				long standardMaxTimeL = standardMaxTime.getTime();
				Date standardMinTime = sdf.parse(minTime);
				long standardMinTimeL = standardMinTime.getTime();

				Date maxDate = sdf.parse(times.get(i));
				long maxTimeL = maxDate.getTime();
				Date minDate = sdf.parse(times.get(i));
				long minTimeL = minDate.getTime();
				if (maxTimeL >= standardMaxTimeL) {
					maxTime = times.get(i);
				}
				if (minTimeL <= standardMinTimeL) {
					minTime = times.get(i);
				}
			}
		}
		timeResult.put("maxTime", maxTime);
		timeResult.put("minTime", minTime);
	}

	/**
	 * 集合排序
	 */
	@Test
	public void rankNum() throws Exception {
		List<Map<Integer, Object>> list = new ArrayList<>();
		List<Map<Integer, Object>> rankList = new ArrayList<>();
		Map<Integer, Object> map1 = new HashMap<>();
		Map<Integer, Object> map2 = new HashMap<>();
		Map<Integer, Object> map3 = new HashMap<>();
		Map<Integer, Object> map4 = new HashMap<>();
		Map<Integer, Object> map5 = new HashMap<>();
		Map<Integer, Object> map6 = new HashMap<>();
		map1.put(1, "2018-05-03");
		map2.put(1, "2018-05-01");
		map3.put(1, "2018-05-06");
		map4.put(1, "2018-05-05");
		map5.put(1, "2018-05-02");
		map6.put(1, "2018-05-04");
		list.add(map1);
		list.add(map2);
		list.add(map3);
		list.add(map4);
		list.add(map5);
		list.add(map6);

		Collections.sort(list, new Comparator<Map<Integer, Object>>() {
			public int compare(Map<Integer, Object> o1, Map<Integer, Object> o2) {
				String date1 = "";
				String date2 = "";
				date1 = o1.get(1).toString();
				date2 = o2.get(1).toString();
				return date1.compareTo(date2);
			}
		});
		
		for (Map<Integer, Object> result : list) {
			for (Entry<Integer, Object> entry : result.entrySet()) {
				System.err.println(entry.getKey() + " " + entry.getValue());
			}
		}

	}

	@Test
	public void judgeMethod() {
		String aa = "a+b";
		String[] split = aa.split("\\+");
		System.err.println(split[0] + "  " + split[1]);
	}
}



