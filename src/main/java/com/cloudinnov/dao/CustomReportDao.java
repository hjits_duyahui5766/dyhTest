package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.CustomReport;

public interface CustomReportDao extends IBaseDao<CustomReport> {
	public int deleteByPrimaryKey(Integer id);

	public int insertInfo(CustomReport record);

	public int insertSelective(CustomReport record);

	public CustomReport selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(CustomReport record);

	public int updateByPrimaryKey(CustomReport record);

	public int updateInfoByCondition(CustomReport record);
	
	public List<CustomReport> search(Map<String, Object> map);
}