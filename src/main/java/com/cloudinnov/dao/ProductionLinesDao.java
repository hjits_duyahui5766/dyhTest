package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.Companies;
import com.cloudinnov.model.EquipmentPoints;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLines;

/**
 * @author guochao
 * @date 2016年2月17日下午1:54:43
 * @email chaoguo@cloudinnov.com
 * @remark 生产线dao接口
 * @version
 */
public interface ProductionLinesDao extends IBaseDao<ProductionLines> {

	/**
	 * selectProductionLineTotalByCustomerId
	 * 
	 * @Description: 根据客户id 获取该客户下的所有产线数量
	 * @param @param
	 *            customerId
	 * @param @return
	 *            参数
	 * @return int 返回类型
	 */
	public int selectProductionLineTotalByCustomerCode(String comCode);

	public int selectProductionLineTotalByOemCode(Companies company);

	public List<ProductionLines> selectListByCondition(ProductionLines productionLine);

	public int insertInfo(ProductionLines productionLine);

	/**
	 * 获取不属于该客户该产线下的设备列表 用于修改时使用
	 * 
	 * @param productionLine
	 * @return
	 */
	public List<Equipments> selectProlineEqusNotExists(ProductionLines productionLine);

	/**
	 * 获取产线下所有设备的状态(故障|停机)数量
	 * 
	 * @param productionLine
	 * @return
	 */
	public int selectProlineStates(ProductionLines productionLine);

	public int updateInfoByCondition(ProductionLines productionLine);

	public List<ProductionLines> selectProLineCodeNameByCustomerCode(ProductionLines productionLine);

	/**
	 * 获取产线下的设备 selectEquipmentsByProductCode
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            productionLine
	 * @param @return
	 *            参数
	 * @return List<Equipments> 返回类型
	 */
	public List<Equipments> selectEquipmentsByProductCode(ProductionLines productionLine);

	/**
	 * 获取产线下的点位 selectEquipmentsByProductCode
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            productionLine
	 * @param @return
	 *            参数
	 * @return List<Equipments> 返回类型
	 */
	public List<EquipmentPoints> selectPointsByProductCode(ProductionLines productionLine);

	public List<ProductionLines> selectProLineInfoByCode(ProductionLines productionLine);

	/**
	 * 根据路段code获取产线数量
	 * 
	 * @param sectionCode
	 * @return
	 */
	public int selectProductionLineTotalBySectionCode(String sectionCode);
}
