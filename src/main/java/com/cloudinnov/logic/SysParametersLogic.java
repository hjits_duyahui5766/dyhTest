package com.cloudinnov.logic;

import com.cloudinnov.model.SysParameters;

/**
 * @author chengning
 * @date 2016年2月18日下午4:06:31
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface SysParametersLogic extends IBaseLogic<SysParameters>{

}
