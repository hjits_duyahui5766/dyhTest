package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.PredictionRuleLog;

public interface PredictionRuleLogDao extends IBaseDao<PredictionRuleLog>{

	int insertPredictionRuleLog(PredictionRuleLog predictionRuleLog);
	
	List<PredictionRuleLog> selectPredictionRuleLog(PredictionRuleLog predictionRuleLog);
	
	PredictionRuleLog selectPredictionRuleLogByCode(PredictionRuleLog predictionRuleLog);
	
	int updatePredictionRuleLog(PredictionRuleLog predictionRuleLog);
	
	int deletePredictionRuleLog(PredictionRuleLog predictionRuleLog);
}
