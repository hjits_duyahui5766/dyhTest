package com.cloudinnov.model;

import java.util.List;

public class WorkOrder extends BaseModel{
	
	public static final int ALARM_WORK_ORDER = 1;
	public static final int INSPECTION_WORK_ORDER = 2;
	public static final int MAINTAIN_WORK_ORDER = 3;
	public static final int RESPLACE_WORK_ORDER = 4;
	public static final int ACCIDENT_WORK_ORDER = 5;
	
    private String code;

    private String sectionCode;//路段编码

    private String title;

    private Integer type;

    private Integer level;

    private Integer orderStatus;

    private String paddingBy;

    private Integer status;

    private String comment;

    private String createTime;

    private String lastUpdateTime;

    private String content;

    private String accidentWorkOrder;
   
    private String faultWorkOrder;
    
    private List<FaultWorkOrderWithBLOBs> faultWorkOrderList;
    
	public List<FaultWorkOrderWithBLOBs> getFaultWorkOrderList() {
		return faultWorkOrderList;
	}

	public void setFaultWorkOrderList(List<FaultWorkOrderWithBLOBs> faultWorkOrderList) {
		this.faultWorkOrderList = faultWorkOrderList;
	}

	public String getFaultWorkOrder() {
		return faultWorkOrder;
	}

	public void setFaultWorkOrder(String faultWorkOrder) {
		this.faultWorkOrder = faultWorkOrder;
	}

	public String getAccidentWorkOrder() {
		return accidentWorkOrder;
	}

	public void setAccidentWorkOrder(String accidentWorkOrder) {
		this.accidentWorkOrder = accidentWorkOrder;
	}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

  
    public String getSectionCode() {
		return sectionCode;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaddingBy() {
        return paddingBy;
    }

    public void setPaddingBy(String paddingBy) {
        this.paddingBy = paddingBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}