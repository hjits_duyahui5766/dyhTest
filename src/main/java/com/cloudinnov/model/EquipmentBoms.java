package com.cloudinnov.model;

public class EquipmentBoms extends BaseModel{

    private String customerCode;

    private String equipmentCode;

    private String name;

    private String serialNumber;

    private String bomCode;

    private String position;

    private String brand;

    private String imageUrl;

    private Integer installAmount;

    private Integer stockAmount;

    private String comment;
    
    private String oemBomCode;
    
    private String model;
    
    private Bom bom;
    
    

    public Bom getBom() {
		return bom;
	}

	public void setBom(Bom bom) {
		this.bom = bom;
	}

	public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBomCode() {
		return bomCode;
	}

	public void setBomCode(String bomCode) {
		this.bomCode = bomCode;
	}

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getInstallAmount() {
        return installAmount;
    }

    public void setInstallAmount(Integer installAmount) {
        this.installAmount = installAmount;
    }

    public Integer getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(Integer stockAmount) {
        this.stockAmount = stockAmount;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

	public String getOemBomCode() {
		return oemBomCode;
	}

	public void setOemBomCode(String oemBomCode) {
		this.oemBomCode = oemBomCode;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
    
}