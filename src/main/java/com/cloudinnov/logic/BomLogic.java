package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.Bom;

/**
 * @author chengning
 * @date 2016年6月27日下午1:30:07
 * @email ningcheng@cloudinnov.com
 * @remark 原件管理Logic
 * @version 
 */
public interface BomLogic extends IBaseLogic<Bom>{
	
	public int saveOtherLanguage(Bom bom);

	public int updateOtherLanguage(Bom bom);
	
	public List<Bom> listByCode(Bom bom);
	
	public int bomImport(String data,Bom bom);

}
