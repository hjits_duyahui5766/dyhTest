package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.AlarmLog;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;

public interface AlarmLogLogic extends IBaseLogic<AlarmLog>{
	
	public Page<AlarmLog> search(PageModel page, AlarmLog model);

	public Page<AlarmLog> selectAlarm(Integer size, Integer index, AlarmLog model);
	
	public List<AlarmLog> selectFireAlarmList();
	
	public Page<AlarmLog> selectFireAlarmListPage(Integer size, Integer index);
}
