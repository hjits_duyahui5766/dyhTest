package com.cloudinnov.logic.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.EquipmentsAttrDao;
import com.cloudinnov.dao.mongo.CarDetectorDataMongoDBDao;
import com.cloudinnov.logic.CarDetectorLogic;
import com.cloudinnov.model.CarDetectorData;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.ReportCar;
import com.cloudinnov.utils.CarDetectorServer;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.JudgeNullUtil;
import com.cloudinnov.utils.TcpClientUtil;
import com.cloudinnov.websocket.CarDetectorWebsocket;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisException;

@Service("carDetectorLogic")
public class CarDetectorLogicImpl extends BaseLogicImpl<CarDetectorData> implements CarDetectorLogic {
	
	static final Logger LOG = LoggerFactory.getLogger(CarDetectorLogicImpl.class);
	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static SimpleDateFormat sdfSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String KEY_EQUCODE = "equCode";
	public static final String KEY_START_TIME = "startTime";
	public static final String KEY_END_TIME = "endTime";
	
	@Autowired
	private JedisPool jedisPool;
	@Autowired
	private EquipmentsAttrDao equipmentsAttrDao;
	@Autowired  
	private CarDetectorDataMongoDBDao carDetectorDataMongoDBDao;
	@Autowired
	private MongoTemplate mongoTemplate;

	/**
	 * redis 查询实时数据
	 */
	@Override
	public CarDetectorData select(CarDetectorData model) {
		List<Map<String, Object>> result = selectTotalDataByEquAndTime(0, model.getEquipmentCode(), null, null);
		if (JudgeNullUtil.iList(result)) {
			for (Map<String, Object> map : result) {
				model.setSummaryTime(System.currentTimeMillis() + "");
				model.setCarTotal(map.get("value") != null ? Long.parseLong(map.get("value") + "") : 0);
				break;
			}
		}
		List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByEquipmentCode(model.getEquipmentCode());
		if (configList != null && configList.size() > 0) {
			for (EquipmentsAttr attr : configList) {
				if (attr.getCustomTag() != null) {
					if (attr.getCustomTag().equals(CarDetectorData.DEVICE_ID)) {
						model.setDeviceId(attr.getValue());
					} else if (attr.getCustomTag().equals(CarDetectorData.PERIOD)) {
						model.setPeriod(attr.getValue() != null ? Integer.parseInt(attr.getValue()) : 0);
					} else if (attr.getCustomTag().equals(CarDetectorData.LANE_TOTAL)) {
						model.setLaneTotal(attr.getValue() != null ? Integer.parseInt(attr.getValue()) : 0);
					} else if (attr.getCustomTag().equals(CarDetectorData.IP)) {
						model.setIp(attr.getValue());
					}
				}
			}
		}
		if (CommonUtils.isNotEmpty(model.getIp())) {
			try {
				boolean isConnect = TcpClientUtil.ping(model.getIp());
				if (isConnect) {
					model.setCurrentState(CommonUtils.EQU_STATE_NORMAL);
				} else {
					//model.setCurrentState(CommonUtils.EQU_STATE_OFFLINE);
					model.setCurrentState(3);
					 
				} 
				/*if(model.getCarTotal()>0||isConnect){
					model.setCurrentState(CommonUtils.EQU_STATE_NORMAL);
				}else{
					model.setCurrentState(CommonUtils.EQU_STATE_OFFLINE);
				}*/
			} catch (Exception e) {
				//model.setCurrentState(CommonUtils.EQU_STATE_OFFLINE);
				model.setCurrentState(3);
			}
		}
		return model;
	}
	/**
	 * websocket 设备状态
	 */
	@Override
	public void carTotalToWebsocket() {
		Jedis redis = null;
		try {
			redis = jedisPool.getResource();
			Map<String, Object> map = new HashMap<String, Object>();
			String traffics = redis.get(CarDetectorServer.TOTAL_CAR + CarDetectorServer.CAL_VALUE);
			int carTotal = Integer
					.parseInt(traffics != null ? traffics.split(CarDetectorServer.SPLITTER_LEVEL0)[0] : "0");
			map.put("traffics", (float) carTotal / 10000);
			map.put("roadsLengths", 173);
			map.put("mileages", 0);
			// 遍历所有连接客户工单WebSocket 推送对话
			Iterator<Map.Entry<String, CarDetectorWebsocket>> carTotalEntries = CarDetectorWebsocket.webSocketMap
					.entrySet().iterator();
			while (carTotalEntries.hasNext()) {
				Map.Entry<String, CarDetectorWebsocket> alarmDialogueWebsocketEntry = carTotalEntries.next();
				try {
					alarmDialogueWebsocketEntry.getValue().sendMessage(JSON.toJSONString(map));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} finally {
			jedisPool.returnResource(redis);
		}
	}
	/**
	 * 车检器数据存储
	 */
	@Override
	public void saveRealTimeDataToMongoDB(CarDetectorData carDetectorData) {
		carDetectorDataMongoDBDao.save(carDetectorData);
	}
	
	/**
	 * 
	 */
	@Override
	public Map<String, Object> selectReportDataToMongoDB(int type, Map<String, String> map) {
		Map<String, Object> data = null;
		String equCode = map.get(KEY_EQUCODE);
		String startTime = map.get(KEY_START_TIME);
		String endTime = map.get(KEY_END_TIME);
		if (CommonUtils.isEmpty(equCode) && CommonUtils.isEmpty(startTime) && CommonUtils.isEmpty(endTime)) {// 无维度查询全部数据
			data = new HashMap<>();
			data.put("model", selectDataByGroupAndCondition(type, null, null, null));
		} else if (CommonUtils.isNotEmpty(equCode) && CommonUtils.isEmpty(startTime) && CommonUtils.isEmpty(endTime)) {// 有设备维度无时间维度查询设备的全部数据
			data = new HashMap<>();
			data.put("model", selectDataByGroupAndCondition(type, equCode, null, null));
		} else if (CommonUtils.isNotEmpty(equCode) && CommonUtils.isNotEmpty(startTime)
				&& CommonUtils.isNotEmpty(endTime)) {// 有设备有时间维度查询设备规定时间的数据
			data = new HashMap<>();
			Date startTimeStamp = null;
			Date endTimeStamp = null;
			try {
				startTimeStamp = sdfSecond.parse(startTime);
				endTimeStamp = sdfSecond.parse(endTime);
			} catch (ParseException e) {
				data = Collections.emptyMap();
			}
			data.put("model", selectDataByGroupAndCondition(type, equCode, startTimeStamp, endTimeStamp));
		} else if (CommonUtils.isEmpty(equCode) && CommonUtils.isNotEmpty(startTime)
				&& CommonUtils.isNotEmpty(endTime)) {// 无设备有时间维度查询设备规定时间的数据
			Date startTimeStamp = null;
			Date endTimeStamp = null;
			try {
				startTimeStamp = sdfSecond.parse(startTime);
				endTimeStamp = sdfSecond.parse(endTime);
			} catch (ParseException e) {
				data = Collections.emptyMap();
			}
			data = new HashMap<>();
			data.put("model", selectDataByGroupAndCondition(type, equCode, startTimeStamp, endTimeStamp));
		}
		return data;
	}
	
	/**
	 * @Title: selectDataByGroupAndCondition
	 * @Description: TODO
	 * @param type 1 无维度查询全部数据 2 有设备维度无时间维度查询设备的全部数据 3 有设备有时间维度查询设备规定时间的数据 4 无设备有时间维度查询设备规定时间的数据
	 * @param equCode
	 * @param startTime
	 * @param endTime
	 * @return
	 * @return: Map<String,Object>
	 */
	@Override
	public Map<String, Object> selectDataByGroupAndCondition(int type, String equCode, Date startTime, Date endTime) {
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(type == 1) {
			data.put("year", selectTotalDataByMysql(1, equCode, startTime, endTime));
		} else if(type == 2) {
			data.put("month", selectTotalDataByMysql(2, equCode, startTime, endTime));
		} else if(type == 3) {
			data.put("day", selectTotalDataByMysql(3, equCode, startTime, endTime));
		} else if(type == 4) {
			data.put("hour", selectTotalDataByTime(4, equCode, startTime, endTime));
		} else if(type == 0) {
			data.put("year", selectTotalDataByMysql(1, equCode, startTime, endTime));
		
			data.put("month", selectTotalDataByMysql(2, equCode, startTime, endTime));
		
			data.put("day", selectTotalDataByMysql(3, equCode, startTime, endTime));

			data.put("hour", selectTotalDataByTime(4, equCode, startTime, endTime));
		}
		return data;
	}
	
	public List<Map<String, Object>> selectTotalDataByMysql(int type, String equCode, Date startTime, Date endTime) {
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> map = null;
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(startTime != null ? startTime : new Date());
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(endTime != null ? endTime : new Date());
		if (startTime == null) {
			oneDayStart.add(Calendar.YEAR, 0);
			oneDayStart.set(Calendar.MONTH, 0);
			oneDayStart.set(Calendar.DAY_OF_MONTH, 1);
			oneDayStart.set(Calendar.HOUR_OF_DAY, 8);
			oneDayStart.set(Calendar.MINUTE, 0);
			oneDayStart.set(Calendar.SECOND, 0);
			oneDayStart.set(Calendar.MILLISECOND, 0);
			
			oneDayEnd.add(Calendar.YEAR, 1);
			oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
			oneDayEnd.set(Calendar.MONTH, 0);
			oneDayEnd.set(Calendar.HOUR_OF_DAY, 8);
			oneDayEnd.set(Calendar.MINUTE, 0);
			oneDayEnd.set(Calendar.SECOND, 0);
			oneDayEnd.set(Calendar.MILLISECOND, 0);
		}
		SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd");
		String dayStart = formatTime.format(oneDayStart.getTime());
		String dayEnd = formatTime.format(oneDayEnd.getTime());
		List<ReportCar> reportCarList = new ArrayList<>();
		if(type == 1) {
			reportCarList = equipmentsAttrDao.selectTotalYearDataByMysql(equCode, dayStart, dayEnd);
		} else if(type == 2) {
			reportCarList = equipmentsAttrDao.selectTotalMonthDataByMysql(equCode, dayStart, dayEnd);	
		} else if(type == 3) {
			reportCarList = equipmentsAttrDao.selectTotalDayDataByMysql(equCode, dayStart, dayEnd);
		}
		for(ReportCar reportCar : reportCarList) {
			map = new HashMap<>();
			map.put("time", reportCar.getUtcTime());
			
			map.put("firstLaneOneKind", reportCar.getFirstLaneOneKind());
			map.put("firstLaneTwoKind", reportCar.getFirstLaneTwoKind());
			map.put("firstLaneThreeKind", reportCar.getFirstLaneThreeKind());
			map.put("firstLaneFourKind", reportCar.getFirstLaneFourKind());
			map.put("firstLaneAverageSpeed", reportCar.getFirstLaneAverageSpeed());
			map.put("firstLaneOccuPancy", reportCar.getFirstLaneOccuPancy());
			map.put("firstLaneFlow", reportCar.getFirstLaneFlow());
			
			map.put("secondLaneOneKind", reportCar.getSecondLaneOneKind());
			map.put("secondLaneTwoKind", reportCar.getSecondLaneTwoKind());
			map.put("secondLaneThreeKind", reportCar.getSecondLaneThreeKind());
			map.put("secondLaneFourKind", reportCar.getSecondLaneFourKind());
			map.put("secondLaneAverageSpeed", reportCar.getSecondLaneAverageSpeed());
			map.put("secondLaneOccuPancy", reportCar.getSecondLaneOccuPancy());
			map.put("secondLaneFlow", reportCar.getSecondLaneFlow());
			
			map.put("thirdLaneOneKind", reportCar.getThirdLaneOneKind());
			map.put("thirdLaneTwoKind", reportCar.getThirdLaneTwoKind());
			map.put("thirdLaneThreeKind", reportCar.getThirdLaneThreeKind());
			map.put("thirdLaneFourKind", reportCar.getThirdLaneFourKind());
			map.put("thirdLaneAverageSpeed", reportCar.getThirdLaneAverageSpeed());
			map.put("thirdLaneOccuPancy", reportCar.getThirdLaneOccuPancy());
			map.put("thirdLaneFlow", reportCar.getThirdLaneFlow());
			
			map.put("forthLaneOneKind", reportCar.getForthLaneOneKind());
			map.put("forthLaneTwoKind", reportCar.getForthLaneTwoKind());
			map.put("forthLaneThreeKind", reportCar.getForthLaneThreeKind());
			map.put("forthLaneFourKind", reportCar.getForthLaneFourKind());
			map.put("forthLaneAverageSpeed", reportCar.getForthLaneAverageSpeed());
			map.put("forthLaneOccuPancy", reportCar.getForthLaneOccuPancy());
			map.put("forthLaneFlow", reportCar.getForthLaneFlow());
			
			map.put("fifthLaneOneKind", reportCar.getFifthLaneOneKind());
			map.put("fifthLaneTwoKind", reportCar.getFifthLaneTwoKind());
			map.put("fifthLaneThreeKind", reportCar.getFifthLaneThreeKind());
			map.put("fifthLaneFourKind", reportCar.getFifthLaneFourKind());
			map.put("fifthLaneAverageSpeed", reportCar.getFifthLaneAverageSpeed());
			map.put("fifthLaneOccuPancy", reportCar.getFifthLaneOccuPancy());
			map.put("fifthLaneFlow", reportCar.getFifthLaneFlow());
			
			map.put("sixthLaneOneKind", reportCar.getSixthLaneOneKind());
			map.put("sixthLaneTwoKind", reportCar.getSixthLaneTwoKind());
			map.put("sixthLaneThreeKind", reportCar.getSixthLaneThreeKind());
			map.put("sixthLaneFourKind", reportCar.getSixthLaneFourKind());
			map.put("sixthLaneAverageSpeed", reportCar.getSixthLaneAverageSpeed());
			map.put("sixthLaneOccuPancy", reportCar.getSixthLaneOccuPancy());
			map.put("sixthLaneFlow", reportCar.getSixthLaneFlow());
			
			map.put("value", reportCar.getCarTotal());
			Integer allCount=reportCar.getCarTotal();
			int chengdu=allCount*5/9;
			int chongqing=allCount-chengdu;
			map.put("chengdu", chengdu);
			map.put("chongqing", chongqing);
			if (CommonUtils.isNotEmpty(equCode)) {
				map.put("code", equCode);
			}
			data.add(map);
		}
		return data;
	}
	
	@SuppressWarnings("deprecation")
	private List<Map<String, Object>> selectTotalDataByTime(int type, String equCode, Date startTime, Date endTime) {
     	String groupStr = "";
		if (CommonUtils.isNotEmpty(equCode)) {
			if (type == 1) {// 年
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+ "}}";
			} else if (type == 2) {// 月
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			} else if (type == 3) {// 日
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" },month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }  }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+ "}}";
			} else if (type == 4) {// 小时
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			}
		} else {
			if (type == 1) {// 年
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"
						+"}}";
			} else if (type == 2) {// 月
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+"}}";
			} else if (type == 3) {// 日
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" },month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }  }, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+ "}}";
			} else if (type == 4) {// 小时
				groupStr = "{\"$group\":{_id:{year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, total:{\"$sum\": \"$carTotal\"},"
						+ "firstLaneOneKind:{\"$sum\": \"$firstLaneOneKind\"}, secondLaneOneKind:{\"$sum\": \"$secondLaneOneKind\"}, thirdLaneOneKind:{\"$sum\": \"$thirdLaneOneKind\"}, forthLaneOneKind:{\"$sum\": \"$forthLaneOneKind\"}, fifthLaneOneKind:{\"$sum\": \"$fifthLaneOneKind\"}, sixthLaneOneKind:{\"$sum\": \"$sixthLaneOneKind\"},"
						+ "firstLaneTwoKind:{\"$sum\": \"$firstLaneTwoKind\"}, secondLaneTwoKind:{\"$sum\": \"$secondLaneTwoKind\"}, thirdLaneTwoKind:{\"$sum\": \"$thirdLaneTwoKind\"}, forthLaneTwoKind:{\"$sum\": \"$forthLaneTwoKind\"}, fifthLaneTwoKind:{\"$sum\": \"$fifthLaneTwoKind\"}, sixthLaneTwoKind:{\"$sum\": \"$sixthLaneTwoKind\"},"
						+ "firstLaneThreeKind:{\"$sum\": \"$firstLaneThreeKind\"}, secondLaneThreeKind:{\"$sum\": \"$secondLaneThreeKind\"}, thirdLaneThreeKind:{\"$sum\": \"$thirdLaneThreeKind\"}, forthLaneThreeKind:{\"$sum\": \"$forthLaneThreeKind\"}, fifthLaneThreeKind:{\"$sum\": \"$fifthLaneThreeKind\"}, sixthLaneThreeKind:{\"$sum\": \"$sixthLaneThreeKind\"},"
						+ "firstLaneFourKind:{\"$sum\": \"$firstLaneFourKind\"}, secondLaneFourKind:{\"$sum\": \"$secondLaneFourKind\"}, thirdLaneFourKind:{\"$sum\": \"$thirdLaneFourKind\"}, forthLaneFourKind:{\"$sum\": \"$forthLaneFourKind\"}, fifthLaneFourKind:{\"$sum\": \"$fifthLaneFourKind\"}, sixthLaneFourKind:{\"$sum\": \"$sixthLaneFourKind\"},"
						+ "firstLaneAverageSpeed:{\"$avg\": \"$firstLaneAverageSpeed\"},secondLaneAverageSpeed:{\"$avg\": \"$secondLaneAverageSpeed\"},thirdLaneAverageSpeed:{\"$avg\": \"$thirdLaneAverageSpeed\"},forthLaneAverageSpeed:{\"$avg\": \"$forthLaneAverageSpeed\"}, fifthLaneAverageSpeed:{\"$avg\": \"$fifthLaneAverageSpeed\"}, sixthLaneAverageSpeed:{\"$avg\": \"$sixthLaneAverageSpeed\"},"
						+ "firstLaneOccuPancy:{\"$avg\": \"$firstLaneOccuPancy\"},secondLaneOccuPancy:{\"$avg\": \"$secondLaneOccuPancy\"},thirdLaneOccuPancy:{\"$avg\": \"$thirdLaneOccuPancy\"},forthLaneOccuPancy:{\"$avg\": \"$forthLaneOccuPancy\"},fifthLaneOccuPancy:{\"$avg\": \"$fifthLaneOccuPancy\"},sixthLaneOccuPancy:{\"$avg\": \"$sixthLaneOccuPancy\"},"
						+ "firstLaneFlow:{\"$sum\": \"$firstLaneFlow\"},secondLaneFlow:{\"$sum\": \"$secondLaneFlow\"},thirdLaneFlow:{\"$sum\": \"$thirdLaneFlow\"},forthLaneFlow:{\"$sum\": \"$forthLaneFlow\"},fifthLaneFlow:{\"$sum\": \"$fifthLaneFlow\"},sixthLaneFlow:{\"$sum\": \"$sixthLaneFlow\"}"				
						+"}}";
			}
		}
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(startTime != null ? startTime : new Date());
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(endTime != null ? endTime : new Date());
		if (startTime == null) {
			if (type == 1 || type == 2) {// 月
				oneDayStart.add(Calendar.YEAR, 0);
				oneDayStart.set(Calendar.MONTH, 0);
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.YEAR, 1);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.set(Calendar.MONTH, 0);
			} else if (type == 3) {// 日
				oneDayStart.add(Calendar.MONTH, 0);
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.MONTH, 1);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
			} else if (type == 4) {// 小时
				oneDayStart.add(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.DAY_OF_MONTH, 1);
			}
			oneDayStart.set(Calendar.HOUR_OF_DAY, 8);
			oneDayStart.set(Calendar.MINUTE, 0);
			oneDayStart.set(Calendar.SECOND, 0);
			oneDayStart.set(Calendar.MILLISECOND, 0);
			oneDayEnd.set(Calendar.HOUR_OF_DAY, 8);
			oneDayEnd.set(Calendar.MINUTE, 0);
			oneDayEnd.set(Calendar.SECOND, 0);
			oneDayEnd.set(Calendar.MILLISECOND, 0);
		}
		BasicDBObject[] array = null;
		if (CommonUtils.isNotEmpty(equCode)) {
			EquipmentsAttr model = new EquipmentsAttr();
			model.setEquipmentCode(equCode);
			model.setCustomTag(CarDetectorData.DEVICE_ID);
			model = equipmentsAttrDao.selectEntityByCondition(model);
			if (model == null) {
				return Collections.emptyList();
			}
			String deviceId = model.getValue();
			array = new BasicDBObject[] {
					new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
					new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
					new BasicDBObject("deviceId", deviceId) };
		} else {
			array = new BasicDBObject[] {
					new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
					new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())) };
		}
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		DBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		/* Reshape Group Result */
		DBObject projectFields = new BasicDBObject();
		projectFields.put("total", "$total");
		projectFields.put("year", "$_id.year");
		projectFields.put("firstLaneOneKind", "$firstLaneOneKind");
		projectFields.put("firstLaneTwoKind", "$firstLaneTwoKind");
		projectFields.put("firstLaneThreeKind", "$firstLaneThreeKind");
		projectFields.put("firstLaneFourKind", "$firstLaneFourKind");
		projectFields.put("firstLaneAverageSpeed", "$firstLaneAverageSpeed");
		projectFields.put("firstLaneOccuPancy", "$firstLaneOccuPancy");
		projectFields.put("firstLaneFlow", "$firstLaneFlow");
		
		projectFields.put("secondLaneOneKind", "$secondLaneOneKind");
		projectFields.put("secondLaneTwoKind", "$secondLaneTwoKind");
		projectFields.put("secondLaneThreeKind", "$secondLaneThreeKind");
		projectFields.put("secondLaneFourKind", "$secondLaneFourKind");
		projectFields.put("secondLaneAverageSpeed", "$secondLaneAverageSpeed");
		projectFields.put("secondLaneOccuPancy", "$secondLaneOccuPancy");
		projectFields.put("secondLaneFlow", "$secondLaneFlow");
		
		projectFields.put("thirdLaneOneKind", "$thirdLaneOneKind");
		projectFields.put("thirdLaneTwoKind", "$thirdLaneTwoKind");
		projectFields.put("thirdLaneThreeKind", "$thirdLaneThreeKind");
		projectFields.put("thirdLaneFourKind", "$thirdLaneFourKind");
		projectFields.put("thirdLaneAverageSpeed", "$thirdLaneAverageSpeed");
		projectFields.put("thirdLaneOccuPancy", "$thirdLaneOccuPancy");
		projectFields.put("thirdLaneFlow", "$thirdLaneFlow");
		
		projectFields.put("forthLaneOneKind", "$forthLaneOneKind");
		projectFields.put("forthLaneTwoKind", "$forthLaneTwoKind");
		projectFields.put("forthLaneThreeKind", "$forthLaneThreeKind");
		projectFields.put("forthLaneFourKind", "$forthLaneFourKind");
		projectFields.put("forthLaneAverageSpeed", "$forthLaneAverageSpeed");
		projectFields.put("forthLaneOccuPancy", "$forthLaneOccuPancy");
		projectFields.put("forthLaneFlow", "$forthLaneFlow");
		
		projectFields.put("fifthLaneOneKind", "$fifthLaneOneKind");
		projectFields.put("fifthLaneTwoKind", "$fifthLaneTwoKind");
		projectFields.put("fifthLaneThreeKind", "$fifthLaneThreeKind");
		projectFields.put("fifthLaneFourKind", "$fifthLaneFourKind");
		projectFields.put("fifthLaneAverageSpeed", "$fifthLaneAverageSpeed");
		projectFields.put("fifthLaneOccuPancy", "$fifthLaneOccuPancy");
		projectFields.put("fifthLaneFlow", "$fifthLaneFlow");
		
		projectFields.put("sixthLaneOneKind", "$sixthLaneOneKind");
		projectFields.put("sixthLaneTwoKind", "$sixthLaneTwoKind");
		projectFields.put("sixthLaneThreeKind", "$sixthLaneThreeKind");
		projectFields.put("sixthLaneFourKind", "$sixthLaneFourKind");
		projectFields.put("sixthLaneAverageSpeed", "$sixthLaneAverageSpeed");
		projectFields.put("sixthLaneOccuPancy", "$sixthLaneOccuPancy");
		projectFields.put("sixthLaneFlow", "$sixthLaneFlow");
		if (type == 2) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type == 3) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type == 4) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipmentCode", "$_id.equipmentCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		/* 查看Group结果 */
		AggregationOutput output = mongoTemplate.getCollection("carDetectorData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> map = null;
		int firstLaneOneKind = 0;
		int firstLaneTwoKind = 0;
		int firstLaneThreeKind = 0;
		int firstLaneFourKind = 0;
		int firstLaneAverageSpeed = 0;
		int firstLaneOccuPancy = 0;
		int firstLaneFlow = 0;
		
		int secondLaneOneKind = 0;
		int secondLaneTwoKind = 0;
		int secondLaneThreeKind = 0;
		int secondLaneFourKind = 0;
		int secondLaneAverageSpeed = 0;
		int secondLaneOccuPancy = 0;
		int secondLaneFlow = 0;
		
		int thirdLaneOneKind = 0;
		int thirdLaneTwoKind = 0;
		int thirdLaneThreeKind = 0;
		int thirdLaneFourKind = 0;
		int thirdLaneAverageSpeed = 0;
		int thirdLaneOccuPancy = 0;
		int thirdLaneFlow = 0;
		
		int forthLaneOneKind = 0;
		int forthLaneTwoKind = 0;
		int forthLaneThreeKind = 0;
		int forthLaneFourKind = 0;
		int forthLaneAverageSpeed = 0;
		int forthLaneOccuPancy = 0;
		int forthLaneFlow = 0;
		
		int fifthLaneOneKind = 0;
		int fifthLaneTwoKind = 0;
		int fifthLaneThreeKind = 0;
		int fifthLaneFourKind = 0;
		int fifthLaneAverageSpeed = 0;
		int fifthLaneOccuPancy = 0;
		int fifthLaneFlow = 0;
		
		int sixthLaneOneKind = 0;
		int sixthLaneTwoKind = 0;
		int sixthLaneThreeKind = 0;
		int sixthLaneFourKind = 0;
		int sixthLaneAverageSpeed = 0;
		int sixthLaneOccuPancy = 0;
		int sixthLaneFlow = 0;
		
		for (DBObject dbObject : output.results()) {
			map = new HashMap<>();
			if (type == 1) {
				map.put("time", dbObject.get("year").toString());
			} else if (type == 2) {// 月
				map.put("time", dbObject.get("year").toString() + "-" + dbObject.get("month"));
			} else if (type == 3) {// 日
				String month = dbObject.get("month") != null ? String.valueOf(dbObject.get("month")) : "";
				if (month.length() == 1) {
					month = "0" + month;
				}
				String day = dbObject.get("day") != null ? String.valueOf(dbObject.get("day")) : "";
				if (day.length() == 1) {
					day = "0" + day;
				}

				map.put("time", dbObject.get("year").toString() + "-" + month + "-" + day);
			} else if (type == 4) {// 小时
				String month = dbObject.get("month") != null ? String.valueOf(dbObject.get("month")) : "";
				if (month.length() == 1) {
					month = "0" + month;
				}
				String day = dbObject.get("day") != null ? String.valueOf(dbObject.get("day")) : "";
				if (day.length() == 1) {
					day = "0" + day;
				}
				int hour = Integer.parseInt(dbObject.get("hour").toString());
				// if (hour / 10 == 0) {
				hour += 8;
				// }
				String hourStr = hour + "";
				if (hourStr.length() == 1) {
					hourStr = "0" + hourStr;
				}
				map.put("time", dbObject.get("year").toString() + "-" + month + "-" + day + " " + hourStr);
			}
			String temp = "";
			temp = dbObject.get("firstLaneOneKind").toString();
			firstLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneTwoKind").toString();
			firstLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneThreeKind").toString();
			firstLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("firstLaneFourKind").toString();
			firstLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("firstLaneAverageSpeed")!=null) {
				temp = dbObject.get("firstLaneAverageSpeed").toString();
				firstLaneAverageSpeed = (int)Double.parseDouble(temp);	
			}else {
				firstLaneOneKind = 0;
			}
			if(dbObject.get("firstLaneOccuPancy")!=null) {
				temp = dbObject.get("firstLaneOccuPancy").toString();
				firstLaneOccuPancy = (int)Double.parseDouble(temp);	
			}else {
				firstLaneOccuPancy = 0;
			}
			temp = dbObject.get("firstLaneFlow").toString();
			firstLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("secondLaneOneKind").toString();
			secondLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneTwoKind").toString();
			secondLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneThreeKind").toString();
			secondLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("secondLaneFourKind").toString();
			secondLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("secondLaneAverageSpeed")!=null) {
				temp = dbObject.get("secondLaneAverageSpeed").toString();
				secondLaneAverageSpeed = (int)Double.parseDouble(temp);	
			}else {
				secondLaneAverageSpeed = 0;
			}
			if(dbObject.get("secondLaneOccuPancy")!=null) {
				temp = dbObject.get("secondLaneOccuPancy").toString();
				secondLaneOccuPancy = (int)Double.parseDouble(temp);	
			}else {
				secondLaneOccuPancy = 0;
			}
			temp = dbObject.get("secondLaneFlow").toString();
			secondLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("thirdLaneOneKind").toString();
			thirdLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneTwoKind").toString();
			thirdLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneThreeKind").toString();
			thirdLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("thirdLaneFourKind").toString();
			thirdLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("thirdLaneAverageSpeed")!=null) {
				temp = dbObject.get("thirdLaneAverageSpeed").toString();
				thirdLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				thirdLaneAverageSpeed = 0;
			}
			if(dbObject.get("thirdLaneOccuPancy")!=null) {
				temp = dbObject.get("thirdLaneOccuPancy").toString();
				thirdLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				thirdLaneOccuPancy = 0;
			}
			temp = dbObject.get("thirdLaneFlow").toString();
			thirdLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("forthLaneOneKind").toString();
			forthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneTwoKind").toString();
			forthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneThreeKind").toString();
			forthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("forthLaneFourKind").toString();
			forthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("forthLaneAverageSpeed")!=null) {
				temp = dbObject.get("forthLaneAverageSpeed").toString();
				forthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				forthLaneAverageSpeed = 0;
			}
			if(dbObject.get("forthLaneOccuPancy")!=null) {
				temp = dbObject.get("forthLaneOccuPancy").toString();
				forthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				forthLaneOccuPancy = 0;
			}
			temp = dbObject.get("forthLaneFlow").toString();
			forthLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("fifthLaneOneKind").toString();
			fifthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneTwoKind").toString();
			fifthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneThreeKind").toString();
			fifthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("fifthLaneFourKind").toString();
			fifthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("fifthLaneAverageSpeed")!=null) {
				temp = dbObject.get("fifthLaneAverageSpeed").toString();
				fifthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				fifthLaneAverageSpeed = 0;
			}
			if(dbObject.get("fifthLaneOccuPancy")!=null) {
				temp = dbObject.get("fifthLaneOccuPancy").toString();
				fifthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				fifthLaneOccuPancy = 0;
			}
			temp = dbObject.get("fifthLaneFlow").toString();
			fifthLaneFlow = Integer.parseInt(temp);
			
			temp = dbObject.get("sixthLaneOneKind").toString();
			sixthLaneOneKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneTwoKind").toString();
			sixthLaneTwoKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneThreeKind").toString();
			sixthLaneThreeKind = Integer.parseInt(temp);
			temp = dbObject.get("sixthLaneFourKind").toString();
			sixthLaneFourKind = Integer.parseInt(temp);
			if(dbObject.get("sixthLaneAverageSpeed")!=null) {
				temp = dbObject.get("sixthLaneAverageSpeed").toString();
				sixthLaneAverageSpeed = (int)Double.parseDouble(temp);
			}else {
				sixthLaneAverageSpeed = 0;
			}
			if(dbObject.get("sixthLaneOccuPancy")!=null) {
				temp = dbObject.get("sixthLaneOccuPancy").toString();
				sixthLaneOccuPancy = (int)Double.parseDouble(temp);
			}else {
				sixthLaneOccuPancy = 0;
			}
			temp = dbObject.get("sixthLaneFlow").toString();
			sixthLaneFlow = Integer.parseInt(temp);
			
			map.put("firstLaneOneKind", firstLaneOneKind);
			map.put("firstLaneTwoKind", firstLaneTwoKind);
			map.put("firstLaneThreeKind", firstLaneThreeKind);
			map.put("firstLaneFourKind", firstLaneFourKind);
			map.put("firstLaneAverageSpeed", firstLaneAverageSpeed);
			map.put("firstLaneOccuPancy", firstLaneOccuPancy);
			map.put("firstLaneFlow", firstLaneFlow);

			map.put("secondLaneOneKind", secondLaneOneKind);
			map.put("secondLaneTwoKind", secondLaneTwoKind);
			map.put("secondLaneThreeKind", secondLaneThreeKind);
			map.put("secondLaneFourKind", secondLaneFourKind);
			map.put("secondLaneAverageSpeed", secondLaneAverageSpeed);
			map.put("secondLaneOccuPancy", secondLaneOccuPancy);
			map.put("secondLaneFlow", secondLaneFlow);

			map.put("thirdLaneOneKind", thirdLaneOneKind);
			map.put("thirdLaneTwoKind", thirdLaneTwoKind);
			map.put("thirdLaneThreeKind", thirdLaneThreeKind);
			map.put("thirdLaneFourKind", thirdLaneFourKind);
			map.put("thirdLaneAverageSpeed", thirdLaneAverageSpeed);
			map.put("thirdLaneOccuPancy", thirdLaneOccuPancy);
			map.put("thirdLaneFlow", thirdLaneFlow);
			
			map.put("forthLaneOneKind", forthLaneOneKind);
			map.put("forthLaneTwoKind", forthLaneTwoKind);
			map.put("forthLaneThreeKind", forthLaneThreeKind);
			map.put("forthLaneFourKind", forthLaneFourKind);
			map.put("forthLaneAverageSpeed", forthLaneAverageSpeed);
			map.put("forthLaneOccuPancy", forthLaneOccuPancy);
			map.put("forthLaneFlow", forthLaneFlow);
			
			map.put("fifthLaneOneKind", fifthLaneOneKind);
			map.put("fifthLaneTwoKind", fifthLaneTwoKind);
			map.put("fifthLaneThreeKind", fifthLaneThreeKind);
			map.put("fifthLaneFourKind", fifthLaneFourKind);
			map.put("fifthLaneAverageSpeed", fifthLaneAverageSpeed);
			map.put("fifthLaneOccuPancy", fifthLaneOccuPancy);
			map.put("fifthLaneFlow", fifthLaneFlow);
			
			map.put("sixthLaneOneKind", sixthLaneOneKind);
			map.put("sixthLaneTwoKind", sixthLaneTwoKind);
			map.put("sixthLaneThreeKind", sixthLaneThreeKind);
			map.put("sixthLaneFourKind", sixthLaneFourKind);
			map.put("sixthLaneAverageSpeed", sixthLaneAverageSpeed);
			map.put("sixthLaneOccuPancy", sixthLaneOccuPancy);
			map.put("sixthLaneFlow", sixthLaneFlow);	
			
			map.put("value", dbObject.get("total"));
			String valueString = dbObject.get("total").toString();
			Integer allCount=Integer.parseInt(valueString);
			int chengdu=allCount*5/9;
			int chongqing=allCount-chengdu;
			map.put("chengdu", chengdu);
			map.put("chongqing", chongqing);
			if (CommonUtils.isNotEmpty(equCode)) {
				map.put("code", dbObject.get("equipmentCode"));
			}
			data.add(map);
		}
		return data;
	}
	
	@Override
	public List<Map<String, Object>> selectTotalDataByEquAndTime(int type, String equCode, Date startTime,
			Date endTime) {
		return selectEquTotalDataByTime(type, equCode, startTime, endTime);
	}
	
	private List<Map<String, Object>> selectEquTotalDataByTime(int type, String equCode, Date startTime, Date endTime) {
		String groupStr = "";
		if (CommonUtils.isNotEmpty(equCode) && startTime != null && endTime != null) {
			if (type == 1) {// 年
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"}}}";
			} else if (type == 2) {// 月
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } }, total:{\"$sum\": \"$carTotal\"}}}";
			} else if (type == 3) {// 日
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" },month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }  }, total:{\"$sum\": \"$carTotal\"}}}";
			} else if (type == 4) {// 小时
				groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\", year: { \"$year\": \"$utcTime\" } ,month: { \"$month\": \"$utcTime\" } ,day: { \"$dayOfMonth\": \"$utcTime\" }, hour: { \"$hour\": \"$utcTime\" }}, total:{\"$sum\": \"$carTotal\"}}}";
			}
		} else if (CommonUtils.isNotEmpty(equCode)) {
			groupStr = "{\"$group\":{_id:{equipmentCode:\"$equipmentCode\"}, total:{\"$sum\": \"$carTotal\"}}}";
		} else {
			return Collections.emptyList();
		}
		Calendar oneDayStart = Calendar.getInstance();
		oneDayStart.setTime(startTime != null ? startTime : new Date());
		Calendar oneDayEnd = Calendar.getInstance();
		oneDayEnd.setTime(endTime != null ? endTime : new Date());
		if (startTime == null) {
			if (type == 1 || type == 2) {// 月
				oneDayStart.add(Calendar.YEAR, 0);
				oneDayEnd.add(Calendar.YEAR, 1);
				oneDayStart.set(Calendar.MONTH, 0);
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.set(Calendar.MONTH, 0);
			} else if (type == 3) {// 日
				oneDayStart.add(Calendar.MONTH, 0);  
				oneDayStart.set(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.MONTH, 1);
				oneDayEnd.set(Calendar.DAY_OF_MONTH, 0);
			} else if (type == 4) {// 小时
				oneDayStart.add(Calendar.DAY_OF_MONTH, 0);
				oneDayEnd.add(Calendar.DAY_OF_MONTH, 1);
			}
			oneDayStart.set(Calendar.HOUR_OF_DAY, 0);
			oneDayStart.set(Calendar.MINUTE, 0);
			oneDayStart.set(Calendar.SECOND, 0);
			oneDayStart.set(Calendar.MILLISECOND, 0);
			oneDayEnd.set(Calendar.HOUR_OF_DAY, 0);
			oneDayEnd.set(Calendar.MINUTE, 0);
			oneDayEnd.set(Calendar.SECOND, 0);
			oneDayEnd.set(Calendar.MILLISECOND, 0);
		}
		BasicDBObject[] array = null;
		EquipmentsAttr model = new EquipmentsAttr();
		model.setEquipmentCode(equCode);
		model.setCustomTag(CarDetectorData.DEVICE_ID);
		model = equipmentsAttrDao.selectEntityByCondition(model);
		if (model == null) {
			return Collections.emptyList();
		}
		String deviceId = model.getValue();
		DBObject projectFields = new BasicDBObject();
		if (CommonUtils.isNotEmpty(deviceId) && startTime != null && endTime != null) {
			projectFields.put("year", "$_id.year");
			array = new BasicDBObject[] {
					new BasicDBObject("utcTime", new BasicDBObject("$gte", oneDayStart.getTime())),
					new BasicDBObject("utcTime", new BasicDBObject("$lte", oneDayEnd.getTime())),
					new BasicDBObject("deviceId", deviceId) };
		} else if (CommonUtils.isNotEmpty(deviceId)) {
			array = new BasicDBObject[] { new BasicDBObject("deviceId", deviceId) };
		} else {
			return Collections.emptyList();
		}
		BasicDBObject cond = new BasicDBObject();
		cond.put("$and", array);
		DBObject match = new BasicDBObject("$match", cond);
		DBObject group = JSON.parseObject(groupStr, BasicDBObject.class);
		projectFields.put("total", "$total");
		if (type == 2) {// 月
			projectFields.put("month", "$_id.month");
		} else if (type == 3) {// 日
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
		} else if (type == 4) {// 小时
			projectFields.put("month", "$_id.month");
			projectFields.put("day", "$_id.day");
			projectFields.put("hour", "$_id.hour");
		}
		projectFields.put("equipmentCode", "$_id.equipmentCode");
		DBObject project = new BasicDBObject("$project", projectFields);
		/* 查看Group结果 */
		@SuppressWarnings("deprecation")
		AggregationOutput output = mongoTemplate.getCollection("carDetectorData").aggregate(match, group, project);
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> map = null;
		for (DBObject dbObject : output.results()) {
			map = new HashMap<>();
			if (CommonUtils.isNotEmpty(equCode) && startTime != null && endTime != null) {
				if (type == 1) {
					map.put("time", dbObject.get("year").toString());
				} else if (type == 2) {// 月
					map.put("time", dbObject.get("year").toString() + "-" + dbObject.get("month"));
				} else if (type == 3) {// 日
					map.put("time",
							dbObject.get("year").toString() + "-" + dbObject.get("month") + "-" + dbObject.get("day"));
				} else if (type == 4) {// 小时
					int hour = Integer.parseInt(dbObject.get("hour").toString());
					// if (hour / 10 == 0) {
					hour += 8;
					// }
					map.put("time", dbObject.get("year").toString() + "-" + dbObject.get("month") + "-"
							+ dbObject.get("day") + " " + hour);
				}
			}
			map.put("value", dbObject.get("total"));
			//总数量获取成都 重庆方向数量
			Integer allCount=Integer.parseInt((String) dbObject.get("total"));
			int chengdu=allCount*5/9;
			int chongqing=allCount-chengdu;
			map.put("chengdu", chengdu);
			map.put("chongqing", chongqing);
			//
			
			if (CommonUtils.isNotEmpty(equCode)) {
				map.put("code", dbObject.get("equipmentCode"));
			}
			data.add(map);
		}
		return data;
	}
	
 
	 
	
	/**
	 * request redis请求车检器数据
	 */
	@Override
	public Map<String, Object> selectDataByEquCodeToRedis(String equCode) {
		Map<String, Object> map = new HashMap<>();
		if (CommonUtils.isNotEmpty(equCode)) {
			List<EquipmentsAttr> configList = equipmentsAttrDao.selectListByEquipmentCode(equCode);
			if (configList != null && configList.size() > 0) {
				for (EquipmentsAttr attr : configList) {
					if (attr.getCustomTag() != null) {
						if (attr.getCustomTag().equals(CarDetectorData.DEVICE_ID)) {
							map.put(CarDetectorData.DEVICE_ID, attr.getValue());
						} else if (attr.getCustomTag().equals(CarDetectorData.PERIOD)) {
							map.put(CarDetectorData.PERIOD,
									attr.getValue() != null ? Integer.parseInt(attr.getValue()) : 0);
						} else if (attr.getCustomTag().equals(CarDetectorData.LANE_TOTAL)) {
							map.put(CarDetectorData.LANE_TOTAL,
									attr.getValue() != null ? Integer.parseInt(attr.getValue()) : 0);
						} else if (attr.getCustomTag().equals(CarDetectorData.IP)) {
							map.put(CarDetectorData.IP, attr.getValue());
						}
					}
				}
			}
			String deviceId = String.valueOf(map.get(CarDetectorData.DEVICE_ID));
			if (CommonUtils.isNotEmpty(deviceId)) {
				long timeMillis=System.currentTimeMillis();
				long hourValue = selectDataToRedis(deviceId, timeMillis - 3600000,
						timeMillis);
				long beyondHourValue=selectCarLaneDataToRedis(deviceId, timeMillis - 3600000, timeMillis,0);
				long behindHourValue=selectCarLaneDataToRedis(deviceId, timeMillis - 3600000,timeMillis, 1);
				map.put("hourValue", hourValue);
				map.put("beyondHourValue", beyondHourValue);
				map.put("behindHourValue", behindHourValue);
				long dayValue = selectDataToRedis(deviceId, timeMillis - 86400000,
						timeMillis);
				long beyondDayValue = selectCarLaneDataToRedis(deviceId, timeMillis - 86400000, timeMillis,0);
				long hehindDayValue=selectCarLaneDataToRedis(deviceId, timeMillis - 86400000, timeMillis, 1);
				map.put("beyondDayValue", beyondDayValue);
				map.put("hehindDayValue", hehindDayValue);
				map.put("dayValue", dayValue);
				
				long weekValue = selectDataToRedis(deviceId, timeMillis - 604800000,timeMillis);
				map.put("weekValue", weekValue);
				long beyondWeekValue=selectCarLaneDataToRedis(deviceId, timeMillis - 604800000, timeMillis, 0);
				long behindWeekValue=selectCarLaneDataToRedis(deviceId, timeMillis - 604800000, timeMillis, 1);
				map.put("beyondWeekValue", beyondWeekValue);
				map.put("behindWeekValue", behindWeekValue);
				long monthValue = selectDataToRedis(deviceId, timeMillis - 2592000000L,timeMillis);
				long beyondMonthValue=selectCarLaneDataToRedis(deviceId, timeMillis - 2592000000L, timeMillis, 0);
				long behindMonthValue=selectCarLaneDataToRedis(deviceId, timeMillis - 2592000000L, timeMillis, 1);
				map.put("beyondMonthValue", beyondMonthValue);
				map.put("behindMonthValue", behindMonthValue);
				map.put("monthValue", monthValue);
			}
			if (CommonUtils.isNotEmpty(String.valueOf(map.get(CarDetectorData.IP)))) {
				try {
					boolean isConnect = TcpClientUtil.ping(CarDetectorData.IP);
					if (isConnect) {
						map.put("currentState", CommonUtils.EQU_STATE_NORMAL);
					} else {
						map.put("currentState", CommonUtils.EQU_STATE_OFFLINE);
					}
				} catch (Exception e) {
					map.put("currentState", CommonUtils.EQU_STATE_OFFLINE);
				}
			}
		}
		map.put("laneTotal", 6);
		map.put("summaryTime", System.currentTimeMillis());
		return map;
	}
	/**
	 * 根据开始时间和结束时间及设备ID查询值
	 * @param deviceId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private long selectDataToRedis(String deviceId, long startTime, long endTime) {
		long value = 0;
		Jedis redis = null;
		try {
			redis = jedisPool.getResource();
			String keyName = CarDetectorServer.CAL_NAME + deviceId + CarDetectorServer.CAL_VALUE;
			List<String> values = redis.lrange(keyName, 0, -1);
			if (JudgeNullUtil.iList(values)) {
				for (String redisVlue : values) {
					long time = Long.parseLong(redisVlue.split(CarDetectorServer.SPLITTER_LEVEL0)[1]);
					if (time >= startTime && time <= endTime) {
						value += Long.parseLong(redisVlue.split(CarDetectorServer.SPLITTER_LEVEL0)[0]);
					}
				}
			}
		} catch (JedisException e) {
			LOG.error("method is selectDataToRedis, data is : [deviceId:{}, startTime:{}, endTime:{} ], error :{}",
					deviceId, startTime, endTime, e);
		} finally {
			jedisPool.returnResource(redis);
		}
		return value;
	}
	
	private long selectCarLaneDataToRedis(String deviceId,long startTime,long endTime,Integer flag){
		if(flag==null){
			flag=0;
		}
		long value = 0;
		Jedis redis = null;
		try {
			redis = jedisPool.getResource();
			String keyName = CarDetectorServer.CAL_NAME + deviceId + CarDetectorServer.CAL_VALUE;
			List<String> values = redis.lrange(keyName, 0, -1);
			if (JudgeNullUtil.iList(values)) {
				for (String redisVlue : values) {
					long time = Long.parseLong(redisVlue.split(CarDetectorServer.SPLITTER_LEVEL0)[1]);
					if (time >= startTime && time <= endTime) {
						if(flag==0){
							value += Long.parseLong(redisVlue.split(CarDetectorServer.SPLITTER_LEVEL0)[2]);
						}else{
							value += Long.parseLong(redisVlue.split(CarDetectorServer.SPLITTER_LEVEL0)[3]);
						}
						 
					}
				}
			}
		} catch (JedisException e) {
			LOG.error("method is selectDataToRedis, data is : [deviceId:{}, startTime:{}, endTime:{} ], error :{}",
					deviceId, startTime, endTime, e);
		} finally {
			jedisPool.returnResource(redis);
		}
		return value;
	}
	
	
}
