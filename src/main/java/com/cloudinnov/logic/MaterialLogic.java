package com.cloudinnov.logic; 

import java.util.List;

import com.cloudinnov.model.Material;

/**
 * @author chengning
 * @date 2016年3月31日下午12:10:22
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface MaterialLogic extends IBaseLogic<Material> {

	public int saveOtherLanguage(Material material);

	public int updateOtherLanguage(Material material);
	
	/** 获取产线下的原料列表
	* selectMaterialByProlineCode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param ProductionLine
	* @param @return    参数
	* @return List<Material>    返回类型 
	*/
	public List<Material> selectMaterialByProlineCode(String codes,String langauge);
}
