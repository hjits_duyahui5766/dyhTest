package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.EquipmentsCategoryAttr;

public interface EquipmentsCategoriesAttrLogic extends IBaseLogic<EquipmentsCategoryAttr> {

	public List<EquipmentsCategoryAttr> selectByCategoryCode(EquipmentsCategoryAttr equipmentsCategoryAttr);

}
