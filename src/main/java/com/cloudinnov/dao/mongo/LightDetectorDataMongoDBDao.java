package com.cloudinnov.dao.mongo;

import org.springframework.stereotype.Repository;

import com.cloudinnov.model.LightDetect;

@Repository("lightDetectorDataMongoDBDao")
public class LightDetectorDataMongoDBDao extends BaseMongoDAOImpl<LightDetect> {

}
