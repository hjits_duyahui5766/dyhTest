package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.AlarmUserLogic;
import com.cloudinnov.model.AlarmWorkUser;

@Service
public class AlarmWorkUserLogicImpl extends BaseLogicImpl<AlarmWorkUser> implements AlarmUserLogic {
}
