package com.cloudinnov.model;

public class MonitorViews extends BaseModel {

	private String customerCode;

	private String userCode;

	private String name;

	private String type;

	private String config;

	private Integer permission;

	private String comment;

	public String getCompareCustomers() {
		return compareCustomers;
	}

	public void setCompareCustomers(String compareCustomers) {
		this.compareCustomers = compareCustomers;
	}

	private String compareCustomers;

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}