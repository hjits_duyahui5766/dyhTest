package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.ProductionSchedulesLogic;
import com.cloudinnov.model.ProductionSchedule;
import com.github.pagehelper.Page;

/**
 * 生产安排表
 * 
 * @author nilixin
 * @date 2016年2月24日下午6:10:25
 * @email
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/productionSchedules")
public class ProductionSchedulesController extends BaseController {

	@Autowired
	private ProductionSchedulesLogic productionSchedulesLogic;

	/**
	 * save
	 * 
	 * @Description: 保存生产安排表
	 * @param @param
	 *            productionSchedul
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/save")
	@ResponseBody
	@SystemLog(module = "生产安排表", methods = "生产安排表信息保存")
	public void save(ProductionSchedule productionSchedul, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		productionSchedul.setCustomerCode(getUserInfo(request).get(COM_CODE));
		int result = productionSchedulesLogic.save(productionSchedul);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * delete
	 * 
	 * @Description: 根据product的code 删除生产安排表
	 * @param @param
	 *            productionSchedulesCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemLog(module = "生产安排表", methods = "生产安排表信息删除")
	public void delete(ProductionSchedule productionSchedul, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productionSchedulesLogic.delete(productionSchedul);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * update
	 * 
	 * @Description: 更新生产安排表
	 * @param @param
	 *            productionLine
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@SystemLog(module = "生产安排表", methods = "生产安排表信息修改")
	public void update(ProductionSchedule productionSchedul, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = productionSchedulesLogic.update(productionSchedul);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result == DEFAULT_SUCCESS_SIZE) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * list
	 * 
	 * @Description: 查找所有生产安排表
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, ProductionSchedule proSchedules, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		proSchedules.setCustomerCode(getUserInfo(request).get(COM_CODE));
		Page<ProductionSchedule> equipments = productionSchedulesLogic.selectListPage(index, size, proSchedules, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipments != null) {
			map.put("list", equipments);
			map.put("total", equipments.getTotal());
			String returuData = returnJsonAllRequest(request, response, map, SUCCESS, null);
			response.getWriter().println(returuData);
		} else {
			String returuData = returnJsonAllRequest(request, response, map, ERROR, null);
			response.getWriter().println(returuData);
		}
	}

	/**
	 * select
	 * 
	 * @Description: 根据code查找生产安排表
	 * @param @param
	 *            productionLineCode
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            JsonGenerationException
	 * @param @throws
	 *            JsonMappingException
	 * @param @throws
	 *            IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(ProductionSchedule productionSchedul, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ProductionSchedule result = productionSchedulesLogic.select(productionSchedul);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			String returuData = returnJsonAllRequest(request, response, map, SUCCESS, null);
			response.getWriter().println(returuData);
		} else {
			String returuData = returnJsonAllRequest(request, response, map, ERROR, null);
			response.getWriter().println(returuData);
		}
	}

}
