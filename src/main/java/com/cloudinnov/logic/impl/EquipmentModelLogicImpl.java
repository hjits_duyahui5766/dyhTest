package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.EquipmentModelDao;
import com.cloudinnov.logic.EquipmentModelLogic;
import com.cloudinnov.model.EquipmentModel;
import com.cloudinnov.utils.CodeUtil;
import com.cloudinnov.utils.CommonUtils;

@Service
public class EquipmentModelLogicImpl extends BaseLogicImpl<EquipmentModel> implements EquipmentModelLogic {

	private int digit = 5;
	@Autowired
	private EquipmentModelDao equipmentModelDao;

	@Override
	public int save(EquipmentModel entity) {
		String code = CodeUtil.equipmentModelCode(digit);
		entity.setCode(code);
		entity.setStatus(CommonUtils.STATUS_NORMAL);
		int result = equipmentModelDao.insert(entity);
		if (result == 1) {
			return equipmentModelDao.insertInfo(entity);
		} else {
			return 0;
		}
	}

	public int update(EquipmentModel entity) {
		int returnCode = equipmentModelDao.updateByCondition(entity);
		returnCode = equipmentModelDao.updateInfoByCondition(entity);
		return returnCode;
	}

	public int saveOtherLanguage(EquipmentModel equipmentModel) {
		return equipmentModelDao.insertInfo(equipmentModel);
	}

	public int updateOtherLanguage(EquipmentModel equipmentModel) {
		return equipmentModelDao.updateInfoByCondition(equipmentModel);
	}

	public List<EquipmentModel> listByCode(EquipmentModel equipmentModel) {
		return equipmentModelDao.selectListByCondition(equipmentModel);
	}

	@Override
	public List<EquipmentModel> selectByCateCode(EquipmentModel equipmentModel) {
		return equipmentModelDao.selectListByCondition(equipmentModel);
	}

}
