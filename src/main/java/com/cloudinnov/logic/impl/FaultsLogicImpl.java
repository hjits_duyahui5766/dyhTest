package com.cloudinnov.logic.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.FaultsDao;
import com.cloudinnov.logic.FaultsLogic;
import com.cloudinnov.model.FaultStat;
import com.cloudinnov.model.Faults;
import com.cloudinnov.utils.CodeUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author chengning
 * @date 2016年2月18日下午2:44:43
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service("faultsLogic")
public class FaultsLogicImpl extends BaseLogicImpl<Faults> implements FaultsLogic {
	private static final Logger logger = LoggerFactory.getLogger(FaultsLogicImpl.class);
	private int digit = 5;
	public static final String QUERY_TYPE = "type";
	public static final int QUERY_TYPE_DETAIL = 1;
	public static final int QUERY_TYPE_DAY = 2;
	public static final int QUERY_TYPE_WEEK = 3;
	public static final int QUERY_TYPE_MONTH = 4;
	public static final int QUERY_TYPE_YEAR = 5;
	@Autowired
	private FaultsDao faultsDao;

	public int save(Faults faults) {
		String code = CodeUtil.faultCode(digit);
		faults.setCode(code);
		int returnCode = faultsDao.insert(faults);
		if (returnCode == 1) {
			returnCode = faultsDao.insertInfo(faults);
		}
		return returnCode;
	}
	public int update(Faults faults) {
		int returnCode = faultsDao.updateByCondition(faults);
		if (returnCode == 1) {
			returnCode = faultsDao.updateInfoByCondition(faults);
		}
		return returnCode;
	}
	@Override
	public Map<String, Object> selectAlarmOrderInfoUseInsert(String channelCode, String faultCode) {
		return faultsDao.selectAlarmOrderInfoUseInsert(channelCode, faultCode);
	}
	@Override
	public Page<Faults> listByLibCode(int index, int size, String oemCode, String language, String level, String key,
			String libraryCode) {
		PageHelper.startPage(index, size);
		Map<String, Object> map = new HashMap<String, Object>();
		// map.put("libraryCode", libraryCode);
		map.put("oemCode", oemCode);
		map.put("language", language);
		map.put("level", level);
		map.put("key", key);
		map.put("libraryCode", libraryCode);
		return (Page<Faults>) faultsDao.listByLibCode(map);
	}
	@Override
	public int insertFaultStat(long nowUnixTime) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", nowUnixTime - 3600000);
		map.put("end", nowUnixTime);
		// map.put("end", nowUnixTime - 86400000);
		return faultsDao.insertFaultStat(map);
	}
	@Override
	public List<FaultStat> selectFaultStatsByCondition(Map<String, Object> map) {
		List<FaultStat> list = new ArrayList<FaultStat>();
		if (map.get(QUERY_TYPE).equals(QUERY_TYPE_WEEK)) {
			list = faultsDao.selectFaultStatsWeekByCondition(map);
		} else if (map.get(QUERY_TYPE).equals(QUERY_TYPE_MONTH)) {
			list = faultsDao.selectFaultStatsMonthByCondition(map);
		} else if (map.get(QUERY_TYPE).equals(QUERY_TYPE_YEAR)) {
			list = faultsDao.selectFaultStatsYearByCondition(map);
		} else if (map.get(QUERY_TYPE).equals(QUERY_TYPE_DAY)) {
			list = faultsDao.selectFaultStatsDayByCondition(map);
		}
		return list;
	}
	@Override
	public Page<FaultStat> selectFaultStatsDetailByCondition(Map<String, Object> map) {
		if (map.get("index") == null || map.get("size") == null) {
			return null;
		}
		PageHelper.startPage(Integer.parseInt(String.valueOf(map.get("index"))),
				Integer.parseInt(String.valueOf(map.get("size"))));
		return (Page<FaultStat>) faultsDao.selectFaultStatsDetailByCondition(map);
	}
	@Override
	public Page<FaultStat> selectFaultStatsDayByCondition(Map<String, Object> map) {
		PageHelper.startPage(Integer.parseInt(String.valueOf(map.get("index"))),
				Integer.parseInt(String.valueOf(map.get("size"))));
		return (Page<FaultStat>) faultsDao.selectFaultStatsDayByCondition(map);
	}
	@Override
	public int saveOtherLanguage(Faults faults) {
		return faultsDao.insertInfo(faults);
	}
	@Override
	public int updateOtherLanguage(Faults faults) {
		return faultsDao.updateInfoByCondition(faults);
	}
	@Override
	public int faultCodeImport(String data, Faults faults) {
		int returnCode = 0;
		ObjectMapper mapper = new ObjectMapper();
		Faults fault;
		try {
			JsonNode node = mapper.readTree(data);
			for (int i = 0; i < node.size(); i++) {
				fault = new Faults();
				fault.setFaultCode(node.get(i).get("故障值").asText());
				fault.setDescription(node.get(i).get("故障描述").asText());
				fault.setHandlingSuggestion(node.get(i).get("处理意见").asText());
				fault.setOemCode(faults.getOemCode());
				fault.setLibraryCode(faults.getLibraryCode());
				fault.setCode(CodeUtil.faultCode(5));
				fault.setLanguage(faults.getLanguage());
				fault.setStatus(faults.getStatus());
				fault.setCreateTime(faults.getCreateTime());
				returnCode = faultsDao.insert(fault);
				returnCode = faultsDao.insertInfo(fault);
			}
		} catch (IOException e) {
			logger.error("点位导入异常 \r{}", e);
		}
		return returnCode;
	}
	@Override
	public List<Faults> selectListByFaultCode(Faults faults) {
		return faultsDao.selectListByFaultCode(faults);
	}
}
