package com.cloudinnov.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BoardTemplate extends BaseModel{

    private String code;

    private String name;

    private String categoryCode;
    
    private String categoryName;

    private Integer rows;

    @JsonIgnore
    private String rowsConfig;

    private Integer status;

    private String comment;

    private String createTime;

    private String lastUpdateTime;
    
    @JsonIgnore
    private String data;
    
    @JsonIgnore
    private List<Data> list;

    @JsonProperty("data")
    public List<Data> getList() {
		return list;
	}

	public void setList(List<Data> list) {
		this.list = list;
	}


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getRowsConfig() {
        return rowsConfig;
    }

    public void setRowsConfig(String rowsConfig) {
        this.rowsConfig = rowsConfig;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	
	public static class Data {
		private Integer spacing = 05;//字间距
		
		/**字体大小*/
    	private int fontSize = 32;
    	
    	/**字体名称*/
    	private String fontName = "宋体";
    	
    	private Integer row;
    	
    	public int getFontSize() {
    		return fontSize;
    	}

    	public void setFontSize(int fontSize) {
    		this.fontSize = fontSize;
    	}

    	public String getFontName() {
    		return fontName;
    	}

    	public void setFontName(String fontName) {
    		this.fontName = fontName;
    	}
		

		public Integer getSpacing() {
			return spacing;
		}

		public void setSpacing(Integer spacing) {
			this.spacing = spacing;
		}

		public Integer getRow() {
			return row;
		}

		public void setRow(Integer row) {
			this.row = row;
		}
	}
    
}