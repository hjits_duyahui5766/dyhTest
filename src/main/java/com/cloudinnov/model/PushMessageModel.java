package com.cloudinnov.model;

public class PushMessageModel extends BaseModel {
	
	private Integer msgType ;
	
	private String title;

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
