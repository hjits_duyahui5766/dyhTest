package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.SysDictsLogic;
import com.cloudinnov.model.SysDicts;

/**
 * @author chengning
 * @date 2016年2月18日下午4:08:49
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class SysDictsLogicImpl extends BaseLogicImpl<SysDicts> implements SysDictsLogic {

}
