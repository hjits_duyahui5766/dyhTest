package com.cloudinnov.dao;

import com.cloudinnov.model.AuthModule;

public interface AuthModuleDao extends IBaseDao<AuthModule>{
	int insertInfo(AuthModule authModule);

    int updateInfoByCondition(AuthModule authModule);
}