package com.cloudinnov.model;

public class HelpWorkOrder extends BaseModel {
	private String customerCode;
	private String equipmentCode;
	private String sponsor;
	private String title;
	private Integer orderStatus;
	private String paddingBy;
	private String comment;
	private String content;
	private Companies company;
	private Equipments equipment;

	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public Equipments getEquipment() {
		return equipment;
	}
	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPaddingBy() {
		return paddingBy;
	}
	public void setPaddingBy(String paddingBy) {
		this.paddingBy = paddingBy;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
