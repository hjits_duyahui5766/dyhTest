package com.cloudinnov.logic.impl;

import org.springframework.stereotype.Service;

import com.cloudinnov.logic.AuthRoleResourceLogic;
import com.cloudinnov.model.AuthRoleResource;

/**
 * @author guochao
 * @date 2016年3月22日下午3:14:14
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Service
public class AuthRoleResourceLogicImpl extends BaseLogicImpl<AuthRoleResource>
		implements AuthRoleResourceLogic {
}
