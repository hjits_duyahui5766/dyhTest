package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AuthResourcesLogic;
import com.cloudinnov.logic.AuthRoleLogic;
import com.cloudinnov.logic.AuthRoleUserLogic;
import com.cloudinnov.logic.SysAreasLogic;
import com.cloudinnov.logic.SysDictsLogic;
import com.cloudinnov.logic.SysIndexTypeLogic;
import com.cloudinnov.logic.SysIndustryLogic;
import com.cloudinnov.logic.SysLanguageLogic;
import com.cloudinnov.logic.SysLogsLogic;
import com.cloudinnov.logic.SysParametersLogic;
import com.cloudinnov.model.AreaObject;
import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthRole;
import com.cloudinnov.model.AuthRoleUser;
import com.cloudinnov.model.AuthUsers;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.model.SysAreas;
import com.cloudinnov.model.SysDicts;
import com.cloudinnov.model.SysIndexType;
import com.cloudinnov.model.SysIndustry;
import com.cloudinnov.model.SysLanguage;
import com.cloudinnov.model.SysLogs;
import com.cloudinnov.model.SysParameters;
import com.cloudinnov.model.TreeObject;
import com.cloudinnov.model.TreeStateObejct;
import com.cloudinnov.utils.AreasUtil;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.TreeUtil;
import com.github.pagehelper.Page;

/**
 * 系统controller用于系统设置一类的控制
 * 
 * @author alsm
 *
 */
/**
 * @author guochao
 * @date 2016年2月18日下午6:32:32
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/system")
public class SystemController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);
	@Autowired
	private AuthResourcesLogic authResourcesLogic;
	@Autowired
	private AuthRoleLogic authRoleLogic;
	@Autowired
	private SysAreasLogic sysAreasLogic;
	@Autowired
	private SysDictsLogic sysDictsLogic;
	@Autowired
	private SysParametersLogic sysParametersLogic;
	@Autowired
	private SysLogsLogic sysLogsLogic;
	@Autowired
	private SysLanguageLogic sysLanguageLogic;
	@Autowired
	private AuthResourcesLogic authResourceLogic;
	@Autowired
	private AuthRoleUserLogic authRoleUserLogic;
	@Autowired
	private SysIndexTypeLogic sysIndexTypeLogic;
	@Autowired
	private SysIndustryLogic sysIndustryLogic;

	/**
	 * resourcesList
	 * @Description: 资源列表
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/resources/list")
	@ResponseBody
	public void resourcesList(int index, int size, AuthResource authResource, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<AuthResource> list = authResourcesLogic.selectListPage(index, size, authResource, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/resources/secondLevelList")
	@ResponseBody
	public void secondLevelList(int parentId, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {
			Map<String, Object> paramap = new HashMap<String, Object>();
			paramap.put("language", language);
			paramap.put("companyCode", getUserInfo(request).get(COM_CODE));
			paramap.put("code", getUserInfo(request).get(USER_CODE));
			paramap.put("parentId", parentId);
			List<AuthResource> resources = authResourceLogic.selectSencondResourceByUser(paramap);
			Map<String, Object> map = new HashMap<String, Object>();
			String returnData = "";
			if (resources != null) {
				map.put("list", resources);
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
			response.getWriter().print(returnData);
		} catch (Exception e) {
			logger.error("资源二级菜单信息列表异常 \r {}", e);
		}
	}
	@RequestMapping(value = "/resources/treeList")
	@ResponseBody
	public void treeList(AuthResource authResource, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<AuthResource> result = authResourcesLogic.selectList(authResource, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			TreeObject treeModel = null;
			for (int i = 0; i < result.size(); i++) {
				treeModel = new TreeObject();
				treeModel.setIndexId(result.get(i).getId());
				treeModel.setSelectId(result.get(i).getId());
				treeModel.setParentId(result.get(i).getParentId());
				treeModel.setCode(result.get(i).getCode());
				treeModel.setName(result.get(i).getName());
				treeModel.setText(result.get(i).getName());
				treeModel.setIcon(result.get(i).getIcon());
				treeModel.setUrl(result.get(i).getUrl());
				treeModel.setGrade(result.get(i).getGrade());
				treeModel.setId(result.get(i).getCode());
				treeModel.setOrderId(result.get(i).getOrderId());
				treeModel.setIsCommon(result.get(i).getIsCommon());
				list.add(treeModel);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, CommonUtils.RESOURCE_PARENT_ID);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/resources/tableList")
	@ResponseBody
	public void resourcesTableList(String[] codes, String language, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<AuthResource> result = authResourcesLogic.tableList(codes, language);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/resources/treeListSelect")
	@ResponseBody
	public void treeListSelect(String language, String roleCode, String type, HttpServletRequest request, int portal,
			HttpServletResponse response) throws IOException {
		AuthResource authResource = new AuthResource();
		authResource.setType(type);
		authResource.setPortal(portal);
		authResource.setLanguage(language);
		List<AuthResource> result = authResourcesLogic.selectList(authResource, false);
		List<AuthResource> selectedResult = authResourcesLogic.selectTreeList(roleCode, language);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setText(result.get(i).getName());
				for (int j = 0; j < selectedResult.size(); j++) {
					if (result.get(i).getCode().equals(selectedResult.get(j).getCode())) {
						TreeStateObejct tso = new TreeStateObejct();
						tso.setOpened(true);
						tso.setSelected(true);
						ts.setState(tso);
					}
				}
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/resources/addSelectList", method = RequestMethod.GET)
	@ResponseBody
	public void addSelectList(AuthResource authResources, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		authResources.setType(getUserInfo(request).get(USER_TYPE));
		List<AuthResource> result = authResourcesLogic.selectList(authResources, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * resourcesSave
	 * @Description: 新增资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/resources/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源信息保存")
	public void resourcesSave(AuthResource authResources, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		authResources.setType(CommonUtils.OEM_TYPE);
		authResources.setPortal(CommonUtils.PORTAL_OEM_TYPE);
		authResources.setOemCode(getUserInfo(request).get(OEM_CODE));
		int returnCode = authResourcesLogic.save(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	/**
	 * resourcesDelete
	 * @Description: 根据code删除一条资源记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/resources/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源信息删除")
	public void resourcesDelete(AuthResource authResources, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authResourcesLogic.delete(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resourcesUpdate
	 * @Description: 更新资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/resources/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源信息修改")
	public void resourcesUpdate(AuthResource authResources, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authResourcesLogic.update(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resSaveOtherLanguage
	 * @Description: 资源增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/resources/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源信息修改其他语言")
	public void resSaveOtherLanguage(AuthResource authResources, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authResources.setLanguage(authResources.getOtherLanguage());
		int returnCode = authResourcesLogic.saveOtherLanguage(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/resources/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源信息修改其他语言")
	public void resUpdateOtherLanguage(AuthResource authResources, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authResources.setLanguage(authResources.getOtherLanguage());
		int returnCode = authResourcesLogic.updateOtherLanguage(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resourcesSelect
	 * @Description: 查找一条资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/resources/select")
	@ResponseBody
	public void resourcesSelect(AuthResource authResources, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (authResources.getOtherLanguage() != null && authResources.getOtherLanguage() != "") {
			authResources.setLanguage(authResources.getOtherLanguage());
		}
		AuthResource result = authResourcesLogic.select(authResources);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/list")
	@ResponseBody
	public void roleList(PageModel page, AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String companyCode = authRole.getCompanyCode();
		if (companyCode == null || companyCode == "") {
			companyCode = getUserInfo(request).get(COM_CODE);
		}
		authRole.setCompanyCode(companyCode);
		Page<AuthRole> list = authRoleLogic.selectListPage(page, authRole, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/selectList")
	@ResponseBody
	public void selectList(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String companyCode = authRole.getCompanyCode();
		if (companyCode == null || companyCode == "") {
			companyCode = getUserInfo(request).get(COM_CODE);
		}
		authRole.setCompanyCode(companyCode);
		List<AuthRole> list = authRoleLogic.selectList(authRole, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * treeList
	 * @Description: 分类列表 by guochao
	 * @param @param equipmentsCategories
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/role/treeList")
	@ResponseBody
	public void treeList(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String companyCode = authRole.getCompanyCode();
		if (companyCode == null || companyCode == "") {
			companyCode = getUserInfo(request).get(COM_CODE);
		}
		authRole.setCompanyCode(companyCode);
		List<AuthRole> result = authRoleLogic.selectList(authRole, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setIndexId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setText(result.get(i).getName());
				list.add(ts);
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/selectUserRole")
	@ResponseBody
	public void selectUserRole(AuthUsers authUser, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		AuthRoleUser aru = new AuthRoleUser();
		aru.setUserCode(authUser.getCode());
		aru.setCompanyCode(authUser.getCompanyCode());
		AuthRoleUser model = authRoleUserLogic.select(aru);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (model != null) {
			map.put("model", model);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/selectUserRoleList")
	@ResponseBody
	public void selectUserRoleList(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 获取客户下的所有角色
		List<AuthRole> result = authRoleLogic.selectList(authRole, true);
		// 获取该用户已经勾选的角色
		AuthRoleUser aru = new AuthRoleUser();
		aru.setUserCode(authRole.getCode());
		aru.setCompanyCode(authRole.getCompanyCode());
		List<AuthRoleUser> selectedResult = authRoleUserLogic.selectList(aru, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setText(result.get(i).getName());
				for (int j = 0; j < selectedResult.size(); j++) {
					if (result.get(i).getCode().equals(selectedResult.get(j).getRoleCode())) {
						TreeStateObejct tso = new TreeStateObejct();
						tso.setOpened(true);
						tso.setSelected(true);
						ts.setState(tso);
					}
				}
				list.add(ts);
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "权限信息保存")
	public void roleSave(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String companyCode = authRole.getCompanyCode();
		if (CommonUtils.isEmpty(companyCode)) {
			companyCode = getUserInfo(request).get(COM_CODE);
		}
		authRole.setOemCode(getUserInfo(request).get(OEM_CODE));
		authRole.setCompanyCode(companyCode);
		int returnCode = authRoleLogic.save(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "角色信息修改其他语言")
	public void roleSaveOtherLanguage(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		authRole.setLanguage(authRole.getOtherLanguage());
		int returnCode = authRoleLogic.saveOtherLanguage(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/role/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "角色信息修改其他语言")
	public void roleUpdateOtherLanguage(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		authRole.setLanguage(authRole.getOtherLanguage());
		int returnCode = authRoleLogic.updateOtherLanguage(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/role/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "权限信息删除")
	public void roleDelete(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authRoleLogic.delete(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/role/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "权限信息修改")
	public void roleUpdate(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authRoleLogic.update(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	// 未修改返回类型
	@RequestMapping(value = "/role/select")
	@ResponseBody
	public void roleSelect(AuthRole authRole, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (authRole.getOtherLanguage() != null && authRole.getOtherLanguage() != "") {
			authRole.setLanguage(authRole.getOtherLanguage());
		}
		AuthRole result = authRoleLogic.select(authRole);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/list")
	@ResponseBody
	public void areasList(SysAreas sa, HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.AGENT_TYPE)) {
			sa.setAgentCode(getUserInfo(request).get(COM_CODE));
		}
		List<SysAreas> result = sysAreasLogic.selectList(sa, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setText(result.get(i).getName());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/phoneList")
	@ResponseBody
	public void phoneList(SysAreas sa, HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<SysAreas> result = sysAreasLogic.phoneList(sa);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/searchList")
	@ResponseBody
	public void searchList(SysAreas sa, HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<SysAreas> result = sysAreasLogic.selectList(sa, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<AreaObject> list = new ArrayList<AreaObject>();
			for (int i = 0; i < result.size(); i++) {
				AreaObject ts = new AreaObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setName(result.get(i).getName());
				ts.setId(result.get(i).getId());
				list.add(ts);
			}
			AreasUtil areaUtil = new AreasUtil();
			Object ns = areaUtil.returnAreaData(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/search ", method = RequestMethod.POST)
	@ResponseBody
	public void areasSearch(SysAreas sa, HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<SysAreas> result = sysAreasLogic.search(sa);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<AreaObject> list = new ArrayList<AreaObject>();
			for (int i = 0; i < result.size(); i++) {
				AreaObject ts = new AreaObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setName(result.get(i).getName());
				ts.setId(result.get(i).getId());
				list.add(ts);
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/getListByParentIds ", method = RequestMethod.POST)
	@ResponseBody
	public void getListByParentIds(SysAreas sa, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysAreas> result = sysAreasLogic.search(sa);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<AreaObject> list = new ArrayList<AreaObject>();
			for (int i = 0; i < result.size(); i++) {
				AreaObject ts = new AreaObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setName(result.get(i).getName());
				ts.setId(result.get(i).getId());
				list.add(ts);
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/search", method = RequestMethod.POST)
	@ResponseBody
	public void areasSearchList(SysAreas sa, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysAreas> result = sysAreasLogic.search(sa);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<AreaObject> list = new ArrayList<AreaObject>();
			for (int i = 0; i < result.size(); i++) {
				AreaObject ts = new AreaObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setName(result.get(i).getName());
				ts.setId(result.get(i).getId());
				list.add(ts);
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/addSelectList")
	@ResponseBody
	public void addSelectList(SysAreas sa, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysAreas> result = sysAreasLogic.selectList(sa, true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/tableList")
	@ResponseBody
	public void tableList(String[] codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysAreas> result = sysAreasLogic.tableList(codes, language);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/save", method = RequestMethod.POST)
	@ResponseBody
	public void areasSave(SysAreas sysArea, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysArea.setOemCode(getUserInfo(request).get(OEM_CODE));
		int returnCode = sysAreasLogic.save(sysArea);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "区域信息修改其他语言")
	public void roleSaveOtherLanguage(SysAreas sysArea, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysArea.setLanguage(sysArea.getOtherLanguage());
		int returnCode = sysAreasLogic.saveOtherLanguage(sysArea);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/areas/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "区域信息修改其他语言")
	public void roleUpdateOtherLanguage(SysAreas sysArea, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysArea.setLanguage(sysArea.getOtherLanguage());
		int returnCode = sysAreasLogic.updateOtherLanguage(sysArea);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/areas/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "区域信息删除")
	public void areasDelete(SysAreas sa, HttpServletRequest request, HttpServletResponse response) throws IOException {
		int returnCode = sysAreasLogic.delete(sa);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "区域信息修改")
	public void areasUpdate(SysAreas sysArea, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysAreasLogic.update(sysArea);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/areas/select", method = RequestMethod.GET)
	@ResponseBody
	// 查询实体未修改
	public void areasSelect(SysAreas sysArea, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (sysArea.getOtherLanguage() != null && sysArea.getOtherLanguage() != "") {
			sysArea.setLanguage(sysArea.getOtherLanguage());
		}
		SysAreas result = sysAreasLogic.select(sysArea);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/dicts/list", method = RequestMethod.GET)
	@ResponseBody
	public void dictsList(int index, int size, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Page<SysDicts> list = sysDictsLogic.selectListPage(index, size, null, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/dicts/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "数据字典信息保存")
	public void dictsSave(SysDicts sysDict, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysDict.setOemCode(getUserInfo(request).get(OEM_CODE));
		int returnCode = sysDictsLogic.save(sysDict);
		String returnData = returnJsonAllRequest(request, response, null, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/dicts/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "数据字典信息删除")
	public void dictsDelete(String dictCode, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysDictsLogic.delete(null);
		String returnData = returnJsonAllRequest(request, response, null, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/dicts/update")
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "数据字典信息修改")
	public void dictsUpdate(SysDicts sysDict, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysDictsLogic.update(sysDict);
		String returnData = returnJsonAllRequest(request, response, null, returnCode, "");
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/dicts/select", method = RequestMethod.GET)
	@ResponseBody
	// 查询实体未修改
	public void dictsSelect(SysDicts sysDict, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		SysDicts result = sysDictsLogic.select(sysDict);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/parameters/list", method = RequestMethod.GET)
	@ResponseBody
	public void parametersList(int index, int size, SysParameters sp, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<SysParameters> list = sysParametersLogic.selectListPage(index, size, sp, true);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/parameters/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统参数信息保存")
	public void parametersSave(SysParameters sysParameters, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysParameters.setOemCode(getUserInfo(request).get(OEM_CODE));
		int returnCode = sysParametersLogic.save(sysParameters);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (returnCode == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/parameters/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统参数信息删除")
	public void parametersDelete(SysParameters sysParameter, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = sysParametersLogic.delete(sysParameter);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/parameters/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统参数信息修改")
	public void parametersUpdate(SysParameters sysParameters, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = sysParametersLogic.update(sysParameters);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/parameters/select", method = RequestMethod.GET)
	@ResponseBody
	public void parametersSelect(SysParameters sysParameters, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		SysParameters result = sysParametersLogic.select(sysParameters);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			response.getWriter().print(returnJsonAllRequest(request, response, map, SUCCESS, ""));
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}
	@RequestMapping(value = "/logs/list", method = RequestMethod.GET)
	@ResponseBody
	public void logsList(int index, int size, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		SysLogs syslog = new SysLogs();
		syslog.setOemCode(getUserInfo(request).get(OEM_CODE));
		Map<String, Object> map = new HashMap<String, Object>();
		Page<SysLogs> list = sysLogsLogic.selectListPage(index, size, syslog, true);
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			map.put("total", list.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/logs/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统日志信息保存")
	public String logsSave(SysLogs sysLogs, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysLogs.setOemCode(getUserInfo(request).get(OEM_CODE));
		sysLogsLogic.save(sysLogs);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(sysLogs);
	}
	@RequestMapping(value = "/logs/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统日志信息删除")
	public String logsDelete(@PathVariable String logsCode, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sysLogsLogic.delete(null);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();
		map.put("status", "1");
		return mapper.writeValueAsString(map);
	}
	@RequestMapping(value = "/logs/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "系统日志信息修改")
	public String logsUpdate(SysLogs sysLogs, HttpServletRequest request, HttpServletResponse response, Model model)
			throws IOException {
		sysLogsLogic.update(sysLogs);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();
		map.put("status", "1");
		return mapper.writeValueAsString(map);
	}
	@RequestMapping(value = "/language/list", method = RequestMethod.GET)
	@ResponseBody
	public void languageList(SysLanguage sl, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		sl.setStatus(CommonUtils.STATUS_NORMAL);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysLanguage> list = sysLanguageLogic.selectList(sl, false);
		String returnData = "";
		if (list != null) {
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/resources/listnopage", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "资源门户信息列表")
	public void gateWayList(SysDicts dict, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysDicts> sysDicts = sysDictsLogic.selectList(dict, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (sysDicts != null) {
			map.put("list", sysDicts);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}
	/**
	 * industryList
	 * @Description: 行业列表
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/getListByParentIds", method = RequestMethod.POST)
	@ResponseBody
	public void industrySearchList(PageModel page, SysIndustry sysIndustry, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (page != null && page.getIndex() != null) {
			Page<SysIndustry> list = sysIndustryLogic.selectListPage(page.getIndex(), page.getSize(), sysIndustry,
					true);
			if (list != null) {
				map.put("list", list);
				map.put("total", list.getTotal());
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		} else {
			List<SysIndustry> list = sysIndustryLogic.search(sysIndustry);
			if (list != null) {
				map.put("list", list);
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		}
		response.getWriter().print(returnData);
	}
	/**
	 * industryList
	 * @Description: 行业列表
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/list", method = RequestMethod.POST)
	@ResponseBody
	public void industryList(PageModel page, SysIndustry sysIndustry, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (page != null && page.getIndex() != null) {
			Page<SysIndustry> list = sysIndustryLogic.selectListPage(page.getIndex(), page.getSize(), sysIndustry,
					true);
			if (list != null) {
				map.put("list", list);
				map.put("total", list.getTotal());
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		} else {
			List<SysIndustry> list = sysIndustryLogic.selectList(sysIndustry, true);
			if (list != null) {
				map.put("list", list);
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/industry/treeList")
	@ResponseBody
	public void industryTreeList(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysIndustry> result = sysIndustryLogic.selectList(sysIndustry, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setCode(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setIndexId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setText(result.get(i).getName());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/industry/addSelectList", method = RequestMethod.GET)
	@ResponseBody
	public void industryAddSelectList(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<SysIndustry> result = sysIndustryLogic.selectList(sysIndustry, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, 0, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * industrySave
	 * @Description: 新增行业记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "行业信息保存")
	public void industrySave(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndustryLogic.save(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	/**
	 * industryDelete
	 * @Description: 根据code删除一条行业记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "行业信息删除")
	public void industryDelete(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndustryLogic.delete(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * industryUpdate
	 * @Description: 更新行业记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "行业信息修改")
	public void industryUpdate(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndustryLogic.update(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * industrySaveOtherLanguage
	 * @Description: 增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/industry/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "行业信息修改其他语言")
	public void industrySaveOtherLanguage(SysIndustry sysIndustry, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		sysIndustry.setLanguage(sysIndustry.getOtherLanguage());
		int returnCode = sysIndustryLogic.saveOtherLanguage(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/industry/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "行业信息修改其他语言")
	public void industryUpdateOtherLanguage(SysIndustry sysIndustry, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		sysIndustry.setLanguage(sysIndustry.getOtherLanguage());
		int returnCode = sysIndustryLogic.updateOtherLanguage(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * industrySelect
	 * @Description: 查找一条行业记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/industry/select")
	@ResponseBody
	public void industrySelect(SysIndustry sysIndustry, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (sysIndustry.getOtherLanguage() != null && sysIndustry.getOtherLanguage() != "") {
			sysIndustry.setLanguage(sysIndustry.getOtherLanguage());
		}
		SysIndustry result = sysIndustryLogic.select(sysIndustry);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	@RequestMapping(value = "/industry/tableList")
	@ResponseBody
	public void industryTableList(String[] codes, String language, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<SysIndustry> result = sysIndustryLogic.tableList(codes, language);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * indexTypeList
	 * @Description: 指标列表
	 * @param @param request
	 * @param @param response
	 * @param @param session
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/indexType/list")
	@ResponseBody
	public void indexTypeList(PageModel page, SysIndexType sysIndexType, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && page.getIndex() != null) {
			Page<SysIndexType> list = sysIndexTypeLogic.selectListPage(page.getIndex(), page.getSize(), sysIndexType,
					true);
			if (list != null) {
				map.put("list", list);
				map.put("total", list.getTotal());
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		} else {
			List<SysIndexType> list = sysIndexTypeLogic.selectList(sysIndexType, true);
			if (list != null) {
				map.put("list", list);
				returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			} else {
				returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			}
		}
		response.getWriter().print(returnData);
	}
	/**
	 * indexTypeSave
	 * @Description: 新增指标记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/indexType/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "指标信息保存")
	public void indexTypeSave(SysIndexType sysIndexType, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndexTypeLogic.save(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, returnCode, "");
		response.getWriter().print(returnData);
	}
	/**
	 * indexTypeDelete
	 * @Description: 根据code删除一条指标记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/indexType/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "指标信息删除")
	public void indexTypeDelete(SysIndexType sysIndexType, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndexTypeLogic.delete(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * indexTypeUpdate
	 * @Description: 更新指标记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/indexType/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "指标信息修改")
	public void indexTypeUpdate(SysIndexType sysIndexType, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = sysIndexTypeLogic.update(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * indexTypeSaveOtherLanguage
	 * @Description: 指标增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/indexType/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "指标信息保存其他语言")
	public void indexTypeSaveOtherLanguage(SysIndexType sysIndexType, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		sysIndexType.setLanguage(sysIndexType.getOtherLanguage());
		int returnCode = sysIndexTypeLogic.saveOtherLanguage(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/indexType/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "系统管理", methods = "指标信息修改其他语言")
	public void indexTypeUpdateOtherLanguage(SysIndexType sysIndexType, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		sysIndexType.setLanguage(sysIndexType.getOtherLanguage());
		int returnCode = sysIndexTypeLogic.updateOtherLanguage(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * indexTypeSelect
	 * @Description: 查找一条指标记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/indexType/select")
	@ResponseBody
	public void indexTypeSelect(SysIndexType sysIndexType, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (sysIndexType.getOtherLanguage() != null && sysIndexType.getOtherLanguage() != "") {
			sysIndexType.setLanguage(sysIndexType.getOtherLanguage());
		}
		SysIndexType result = sysIndexTypeLogic.select(sysIndexType);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = "";
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	/**
	 * 根据设备编码获取设备下的点位指标类型
	 * @Title: selectIndexTypeListByEquipmentCodes
	 * @Description: TODO
	 * @param equipment
	 * @param request
	 * @param response
	 * @throws IOException
	 * @return: void
	 */
	@RequestMapping(value = "/indexType/getIndexTypeListByEquipmentCodes", method = RequestMethod.GET)
	@ResponseBody
	public void selectIndexTypeListByEquipmentCodes(SysIndexType indexType, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<SysIndexType> list = sysIndexTypeLogic.selectIndexTypeListByEquipmentCodes(indexType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null && list.size() > 0) {
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, null);
			response.getWriter().print(returnData);
		}
	}
}
