package com.cloudinnov.model;

import com.cloudinnov.utils.CommonUtils;

public class AlarmWorkOrders extends BaseModel {

	private String customerCode;

	private String sectionCode;

	private String equipmentCode;

	private String faultChannelCode;

	private String faultCode;

	private String firstTime;

	private Integer count;

	private String paddingBy;

	private String comment;

	private String description;

	private String handingSuggestion;

	private Integer orderStatus;

	private String orderStatusName;

	private Equipments equipment;

	private Companies company;

	private ProductionLines productline;

	private String oemPerson;

	private String supplierPerson;

	private String customerPerson;

	private Integer faultCount;

	// private Integer ignoreStatus;

	private Integer level;// 故障级别

	private Integer type;// 故障类型

	private Integer isCreateOrder;// 是否生成工单

	private Integer isReceiveOrder;// 1未接单(只有工单所关联的设备责任人包含当前登陆用户才可接单) 2 已接单

	// 解决方案
	private String solution;

	private String emGroupId;

	public ProductionLines getProductline() {
		return productline;
	}

	public void setProductline(ProductionLines productline) {
		this.productline = productline;
	}

	public String getOemPerson() {
		return oemPerson;
	}

	public void setOemPerson(String oemPerson) {
		this.oemPerson = oemPerson;
	}

	public String getSupplierPerson() {
		return supplierPerson;
	}

	public void setSupplierPerson(String supplierPerson) {
		this.supplierPerson = supplierPerson;
	}

	public String getCustomerPerson() {
		return customerPerson;
	}

	public void setCustomerPerson(String customerPerson) {
		this.customerPerson = customerPerson;
	}

	public Equipments getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipments equipment) {
		this.equipment = equipment;
	}

	public Companies getCompany() {
		return company;
	}

	public void setCompany(Companies company) {
		this.company = company;
	}

	public String getOrderStatusName() {
		return orderStatusName;
	}

	public void setOrderStatusName(String orderStatusName) {

		this.orderStatusName = orderStatusName;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {

		if (orderStatus == CommonUtils.ORDER_STATUS_CLOSE) {

			this.orderStatusName = CommonUtils.ORDER_STATUS_CLOSE_MSG;

		} else if (orderStatus == CommonUtils.ORDER_STATUS_HANDLE) {

			this.orderStatusName = CommonUtils.ORDER_STATUS_HANDLE_MSG;

		} else if (orderStatus == CommonUtils.ORDER_STATUS_NEW) {

			this.orderStatusName = CommonUtils.ORDER_STATUS_NEW_MSG;
		} /*
			 * else if (orderStatus == CommonUtils.ORDER_STATUS_READED) {
			 * 
			 * this.orderStatusName = CommonUtils.ORDER_STATUS_READED_MSG; }
			 */
		this.orderStatus = orderStatus;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getFaultChannelCode() {
		return faultChannelCode;
	}

	public void setFaultChannelCode(String faultChannelCode) {
		this.faultChannelCode = faultChannelCode;
	}

	public String getFaultCode() {
		return faultCode;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getPaddingBy() {
		return paddingBy;
	}

	public void setPaddingBy(String paddingBy) {
		this.paddingBy = paddingBy;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHandingSuggestion() {
		return handingSuggestion;
	}

	public void setHandingSuggestion(String handingSuggestion) {
		this.handingSuggestion = handingSuggestion;
	}

	public String getFirstTime() {
		return firstTime;
	}

	public void setFirstTime(String firstTime) {
		this.firstTime = firstTime;
	}

	public Integer getFaultCount() {
		return faultCount;
	}

	public void setFaultCount(Integer faultCount) {
		this.faultCount = faultCount;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsCreateOrder() {
		return isCreateOrder;
	}

	public void setIsCreateOrder(Integer isCreateOrder) {
		this.isCreateOrder = isCreateOrder;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public Integer getIsReceiveOrder() {
		return isReceiveOrder;
	}

	public void setIsReceiveOrder(Integer isReceiveOrder) {
		this.isReceiveOrder = isReceiveOrder;
	}

	public String getEmGroupId() {
		return emGroupId;
	}

	public void setEmGroupId(String emGroupId) {
		this.emGroupId = emGroupId;
	}

	public String getSectionCode() {
		return sectionCode;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

}