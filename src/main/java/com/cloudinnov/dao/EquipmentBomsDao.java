package com.cloudinnov.dao;

import java.util.Map;

import com.cloudinnov.model.EquipmentBoms;

/**
 * @author guochao
 * @date 2016年2月17日下午4:36:20
 * @email chaoguo@cloudinnov.com
 * @remark 设备bom Dao接口
 * @version
 */
public interface EquipmentBomsDao extends IBaseDao<EquipmentBoms> {
	public int insertInfo(EquipmentBoms equipmentsBom);
	
	public int updateInfoByCondition(EquipmentBoms equipmentsBom);
	
	/** 批量插入
	 * @Title: insertBatch 
	 * @Description: TODO
	 * @param params
	 * @return
	 * @return: int
	 */
	public int insertBatch(Map<String, Object> params);
}
