package com.cloudinnov.logic;

import com.cloudinnov.model.EquipmentInspectionWorkOrder;
import com.github.pagehelper.Page;

public interface EquipmentInspectionWorkOrderLogic extends IBaseLogic<EquipmentInspectionWorkOrder>{

	Page<EquipmentInspectionWorkOrder> list(int index, int size, EquipmentInspectionWorkOrder eiwo);
	
	Page<EquipmentInspectionWorkOrder> search(int index, int size, EquipmentInspectionWorkOrder eiwo,String key);

}
