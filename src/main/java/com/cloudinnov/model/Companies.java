package com.cloudinnov.model; 

import java.util.List;

/**
 * @author chengning
 * @date 2016年3月14日上午9:58:58
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public class Companies extends BaseModel{

	private String type;

    private String name;

    private String shortName;

    private String logo;

    private String homepage;

    private String contact;

    private String telephone;

    private String country;

    private String province;

    private String city;
    
    private List<String> countrys;

    private List<String> provinces;

    private List<String> citys;

    private String address;

    private String fax;

    private String serviceTelephone;

    private String email;
    
    private String oemCode;
    
    private String integratorCode;

    private String agentCode;

    private String location;

    private String comment;

    private List<?> list;
    
    private Integer currentState;
    
    private Integer isError;
    
    private List<Equipments> errorEquipments;
    
    private String industryCode;
    
    private List<String> industryCodes;
    
	public String getIntegratorCode() {
		return integratorCode;
	}

	public void setIntegratorCode(String integratorCode) {
		this.integratorCode = integratorCode;
	}

	public Integer getCurrentState() {
		return currentState;
	}

	public void setCurrentState(Integer currentState) {
		this.currentState = currentState;
	}

	

	public Integer getIsError() {
		return isError;
	}

	public void setIsError(Integer isError) {
		this.isError = isError;
	}

	public List<?> getList() {
		return list;
	}

	public void setList(List<?> list) {
		this.list = list;
	}

	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getServiceTelephone() {
        return serviceTelephone;
    }

    public void setServiceTelephone(String serviceTelephone) {
        this.serviceTelephone = serviceTelephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOemCode() {
        return oemCode;
    }

    public void setOemCode(String oemCode) {
        this.oemCode = oemCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

	public List<Equipments> getErrorEquipments() {
		return errorEquipments;
	}

	public void setErrorEquipments(List<Equipments> errorEquipments) {
		this.errorEquipments = errorEquipments;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public List<String> getCountrys() {
		return countrys;
	}

	public void setCountrys(List<String> countrys) {
		this.countrys = countrys;
	}

	public List<String> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<String> provinces) {
		this.provinces = provinces;
	}

	public List<String> getCitys() {
		return citys;
	}

	public void setCitys(List<String> citys) {
		this.citys = citys;
	}

	public List<String> getIndustryCodes() {
		return industryCodes;
	}

	public void setIndustryCodes(List<String> industryCodes) {
		this.industryCodes = industryCodes;
	}

	
    
}
