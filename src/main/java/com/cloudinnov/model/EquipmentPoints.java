package com.cloudinnov.model;

import com.cloudinnov.utils.CommonUtils;

public class EquipmentPoints extends BaseModel {
    private String customerCode;
    private String equipmentCode;
    private String sectionCode;
    private String name;
    private String type;
    private String typeName;
    private String config;
    private String bomCode;
    private String comment;
    private Integer range;
    private Integer viewType;
    private Integer isCrucial;
    private Integer isProductivity;
    private String relationEquipment;
    private Equipments equipment;
    private Companies company;
    private Object value;
    private Integer orderId;
    private String indexType;
    private SysIndexType sysIndexType;
    private Integer dataType;
    private Integer digits;
    private String chartType;
    private Integer monitorType;
    private String minDesc;
    private String maxDesc;
    private Integer valueType;
    private Integer isUserCustomerReport;
    private String xid;
    private Integer isFeedback;// 点位类型 1 控制 2 反馈

    public Integer getIsFeedback() {
        return isFeedback;
    }
    public void setIsFeedback(Integer isFeedback) {
        this.isFeedback = isFeedback;
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    public long getTime() {
        return time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    public Object getMin() {
        return min;
    }
    public void setMin(Object min) {
        this.min = min;
    }
    public Object getMax() {
        return max;
    }
    public void setMax(Object max) {
        this.max = max;
    }

    private String unit;
    private long time = -999;
    private Object min;
    private Object max;
    private String bomName;
    private String kind;
    private String newCode;

    public String getNewCode() {
        return newCode;
    }
    public void setNewCode(String newCode) {
        this.newCode = newCode;
    }
    public Integer getIsProductivity() {
        return isProductivity;
    }
    public String getKind() {
        return kind;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public void setIsProductivity(Integer isProductivity) {
        this.isProductivity = isProductivity;
    }
    public String getBomName() {
        return bomName;
    }
    public void setBomName(String bomName) {
        this.bomName = bomName;
    }
    public Integer getIsCrucial() {
        return isCrucial;
    }
    public void setIsCrucial(Integer isCrucial) {
        this.isCrucial = isCrucial;
    }
    public Integer getRange() {
        return range;
    }
    public void setRange(Integer range) {
        this.range = range;
    }
    public Integer getViewType() {
        return viewType;
    }
    public void setViewType(Integer viewType) {
        this.viewType = viewType;
    }
    public String getCustomerCode() {
        return customerCode;
    }
    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }
    public String getEquipmentCode() {
        return equipmentCode;
    }
    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        if (type.equals(CommonUtils.POINT_TYPE_MN)) {
            this.typeName = CommonUtils.ANALOG_QUANTITY_MSG;
        } else if (type.equals(CommonUtils.POINT_TYPE_SZ)) {
            this.typeName = CommonUtils.DIGITAL_QUANTITY_MSG;
        }
        this.type = type;
    }
    public String getConfig() {
        return config;
    }
    public void setConfig(String config) {
        this.config = config;
    }
    public String getBomCode() {
        return bomCode;
    }
    public void setBomCode(String bomCode) {
        this.bomCode = bomCode;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getTypeName() {
        return typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public Equipments getEquipment() {
        return equipment;
    }
    public void setEquipment(Equipments equipment) {
        this.equipment = equipment;
    }
    public Companies getCompany() {
        return company;
    }
    public void setCompany(Companies company) {
        this.company = company;
    }
    public String getRelationEquipment() {
        return relationEquipment;
    }
    public void setRelationEquipment(String relationEquipment) {
        this.relationEquipment = relationEquipment;
    }
    public Integer getOrderId() {
        return orderId;
    }
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
    public String getIndexType() {
        return indexType;
    }
    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }
    public SysIndexType getSysIndexType() {
        return sysIndexType;
    }
    public void setSysIndexType(SysIndexType sysIndexType) {
        this.sysIndexType = sysIndexType;
    }
    public Integer getDataType() {
        return dataType;
    }
    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }
    public Integer getDigits() {
        return digits;
    }
    public void setDigits(Integer digits) {
        this.digits = digits;
    }
    public String getChartType() {
        return chartType;
    }
    public void setChartType(String chartType) {
        this.chartType = chartType;
    }
    public Integer getMonitorType() {
        return monitorType;
    }
    public void setMonitorType(Integer monitorType) {
        this.monitorType = monitorType;
    }
    public String getMinDesc() {
        return minDesc;
    }
    public void setMinDesc(String minDesc) {
        this.minDesc = minDesc;
    }
    public String getMaxDesc() {
        return maxDesc;
    }
    public void setMaxDesc(String maxDesc) {
        this.maxDesc = maxDesc;
    }
    public Integer getValueType() {
        return valueType;
    }
    public void setValueType(Integer valueType) {
        this.valueType = valueType;
    }
    public Integer getIsUserCustomerReport() {
        return isUserCustomerReport;
    }
    public void setIsUserCustomerReport(Integer isUserCustomerReport) {
        this.isUserCustomerReport = isUserCustomerReport;
    }
    public String getSectionCode() {
        return sectionCode;
    }
    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }
    public String getXid() {
        return xid;
    }
    public void setXid(String xid) {
        this.xid = xid;
    }
    @Override
    public String toString() {
        return "EquipmentPoints [customerCode=" + customerCode + ", equipmentCode=" + equipmentCode + ", sectionCode="
                + sectionCode + ", name=" + name + ", type=" + type + ", typeName=" + typeName + ", config=" + config
                + ", bomCode=" + bomCode + ", comment=" + comment + ", range=" + range + ", viewType=" + viewType
                + ", isCrucial=" + isCrucial + ", isProductivity=" + isProductivity + ", relationEquipment="
                + relationEquipment + ", equipment=" + equipment + ", company=" + company + ", value=" + value
                + ", orderId=" + orderId + ", indexType=" + indexType + ", sysIndexType=" + sysIndexType + ", dataType="
                + dataType + ", digits=" + digits + ", chartType=" + chartType + ", monitorType=" + monitorType
                + ", minDesc=" + minDesc + ", maxDesc=" + maxDesc + ", valueType=" + valueType
                + ", isUserCustomerReport=" + isUserCustomerReport + ", xid=" + xid + ", unit=" + unit + ", time="
                + time + ", min=" + min + ", max=" + max + ", bomName=" + bomName + ", kind=" + kind + ", newCode="
                + newCode + "]";
    }
}
