package com.cloudinnov.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cloudinnov.dao.FaultWorkOrderDao;
import com.cloudinnov.dao.WorkOrderDao;
import com.cloudinnov.logic.FaultWorkOrderLogic;
import com.cloudinnov.model.FaultWorkOrder;
import com.cloudinnov.model.FaultWorkOrderWithBLOBs;
import com.cloudinnov.model.WorkOrder;
import com.cloudinnov.utils.CodeUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author chengning
 * @date 2016年2月17日上午11:50:41
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 工单Logic实现类 继承BaseLogicImpl 实现工单Logic接口
 */
@Service
public class FaultWorkOrderLogicImpl extends BaseLogicImpl<FaultWorkOrder> implements FaultWorkOrderLogic {
	
	@Autowired
	private WorkOrderDao workOrderDao;
	
	private FaultWorkOrderDao faultWorkOrderDao;
	
	@Override
	public int saveFaultWorkOrder(WorkOrder workOrder) {
		
		String code = CodeUtil.workOrderCode(5);
		
		workOrder.setCode(code);
		workOrder.setType(2);
		
		
		FaultWorkOrderWithBLOBs faultWorkOrder = new FaultWorkOrderWithBLOBs();
		
		faultWorkOrder.setCode(code);
		
		//将json数据转化成list
		List<FaultWorkOrder> list = JSON.parseArray(workOrder.getFaultWorkOrder(),FaultWorkOrder.class);
		
		int result = workOrderDao.insert(workOrder);
		
		if(result == 1){
			
			for (FaultWorkOrder faultWorkOrder2 : list) {
				
				faultWorkOrder.setEquipmentCode(faultWorkOrder.getEquipmentCode());
				faultWorkOrder.setFaultChannelCode(faultWorkOrder2.getFaultChannelCode());
				faultWorkOrder.setFaultCode(faultWorkOrder2.getFaultCode());
				faultWorkOrder.setDescription(faultWorkOrder.getDescription());
				faultWorkOrder.setHandingSuggestion(faultWorkOrder.getDescription());
				faultWorkOrder.setFirstTime(faultWorkOrder.getHandingSuggestion());
				faultWorkOrder.setCount(faultWorkOrder.getCount());
				
				faultWorkOrderDao.insert(faultWorkOrder);
				
			}
			return 1;
		}else{
			return 0;
		}
	}
	
	@Override
	public int updateFaultWorkOrder(WorkOrder workOrder) {
		
		FaultWorkOrderWithBLOBs faultWorkOrder = new FaultWorkOrderWithBLOBs();
		
		faultWorkOrder.setCode(workOrder.getCode());
		
		//将json数据转化成list
		List<FaultWorkOrder> list = JSON.parseArray(workOrder.getFaultWorkOrder(),FaultWorkOrder.class);
		
		int result = workOrderDao.updateByCondition(workOrder);
		
		if(result == 1){
			
			faultWorkOrderDao.deleteByCondition(faultWorkOrder);
			
			for (FaultWorkOrder faultWorkOrder2 : list) {
				
				faultWorkOrder.setEquipmentCode(faultWorkOrder.getEquipmentCode());
				faultWorkOrder.setFaultChannelCode(faultWorkOrder2.getFaultChannelCode());
				faultWorkOrder.setFaultCode(faultWorkOrder2.getFaultCode());
				faultWorkOrder.setDescription(faultWorkOrder.getDescription());
				faultWorkOrder.setHandingSuggestion(faultWorkOrder.getDescription());
				faultWorkOrder.setFirstTime(faultWorkOrder.getHandingSuggestion());
				faultWorkOrder.setCount(faultWorkOrder.getCount());
				
				faultWorkOrderDao.insert(faultWorkOrder);
			}
			return 1;
		}else{
			return 0;
		}
	}
	
	@Override
	public int deleteFaultWorkOrder(WorkOrder workOrder) {
		
		int result = workOrderDao.deleteByCondition(workOrder);
		
		FaultWorkOrderWithBLOBs faultWorkOrder = new FaultWorkOrderWithBLOBs();
		
		faultWorkOrder.setCode(workOrder.getCode());
		
		if(result ==1){
			faultWorkOrderDao.deleteByCondition(faultWorkOrder);
			return 1;
		}else{
			return 0;
		}
	}
	
	@Override
	public Page<WorkOrder> selectFaultWorkOrder(WorkOrder workOrder,int index, int size) {
		
		PageHelper.startPage(index,size);
		
		List<WorkOrder> list = workOrderDao.selectListByCondition(workOrder);
		
		FaultWorkOrderWithBLOBs faultWorkOrder = new FaultWorkOrderWithBLOBs();
		
		for (WorkOrder workOrder2 : list) {
		
			faultWorkOrder.setCode(workOrder2.getCode());
			
			List<FaultWorkOrderWithBLOBs> faultInfo = faultWorkOrderDao.selectListByCondition(faultWorkOrder);
			
			WorkOrder order = new WorkOrder();
			
			order.setFaultWorkOrderList(faultInfo);
			
		}
		
		return (Page<WorkOrder>) list;
	}
	
	@Override
	public WorkOrder selectFaultWorkOrderByCode(WorkOrder workOrder) {
		
		WorkOrder workOrderinfo = workOrderDao.selectEntityByCondition(workOrder);
		
		FaultWorkOrderWithBLOBs faultWorkOrder = new FaultWorkOrderWithBLOBs();
		
		faultWorkOrder.setCode(workOrder.getCode());
		
		List<FaultWorkOrderWithBLOBs> list = faultWorkOrderDao.selectListByCondition(faultWorkOrder);
		
		workOrderinfo.setFaultWorkOrderList(list);
		
		return null;
	}
	
}
