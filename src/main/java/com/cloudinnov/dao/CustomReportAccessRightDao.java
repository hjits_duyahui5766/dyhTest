package com.cloudinnov.dao;

import com.cloudinnov.model.CustomReportAccessRight;

public interface CustomReportAccessRightDao extends IBaseDao<CustomReportAccessRight> {
	int deleteByPrimaryKey(Integer id);

	int insert(CustomReportAccessRight record);

	int insertSelective(CustomReportAccessRight record);

	CustomReportAccessRight selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CustomReportAccessRight record);

	int updateByPrimaryKey(CustomReportAccessRight record);
}