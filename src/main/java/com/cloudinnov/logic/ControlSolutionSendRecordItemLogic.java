package com.cloudinnov.logic;

import java.util.Map;

import com.cloudinnov.model.ControlSolutionSendRecordItem;
import com.cloudinnov.model.PageModel;
import com.github.pagehelper.Page;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年6月20日上午10:54:38
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface ControlSolutionSendRecordItemLogic extends IBaseLogic<ControlSolutionSendRecordItem> {
	/**
	 * 搜索情报板发送记录发送记录
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param map solutionCode 路段编码 cateCode设备分类编码 startTime 开始时间 endTime结束时间
	 * @return 参数
	 * @return List<ControlSolutionSendRecordItem> 返回类型
	 */
	Page<ControlSolutionSendRecordItem> search(PageModel page, Map<String, Object> map);
	/**
	 * 根据设备编码和发送时间查询某个设备某个时间点发送的记录
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param equipmentCode 设备编码
	 * @param sendTime 发送时间
	 * @return 参数
	 * @return ControlSolutionSendRecordItem 返回类型
	 */
	ControlSolutionSendRecordItem selectListByEquCodeAndSendTime(String equipmentCode, String sendTime);
}
