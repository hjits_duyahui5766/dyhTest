package com.cloudinnov.dao;

import java.util.List;

import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.EquipmentsAttr;
import com.cloudinnov.model.LightDetect;

public interface LightDetectDao {
	
	List<LightDetect> lightDetectIpInfo();
	
	List<EquipmentsAttr> selectLightByEquipCode(String equipCode);
	
	List<Equipments> getEquipmentInfoByCategoryInfo(String categoryCode);

}
