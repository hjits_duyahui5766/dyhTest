package com.cloudinnov.logic;

import com.cloudinnov.model.FaultWorkOrder;
import com.cloudinnov.model.WorkOrder;
import com.github.pagehelper.Page;

public interface FaultWorkOrderLogic extends IBaseLogic<FaultWorkOrder>{

	int saveFaultWorkOrder(WorkOrder workOrder);

	int updateFaultWorkOrder(WorkOrder workOrder);

	int deleteFaultWorkOrder(WorkOrder workOrder);

	Page<WorkOrder> selectFaultWorkOrder(WorkOrder workOrder, int index, int size);

	WorkOrder selectFaultWorkOrderByCode(WorkOrder workOrder);

	
}
