package com.cloudinnov.model;

public class CameraModel {
	public static final String CAMERA_ID = "cameraId";
	public static final String INDEX_CODE = "indexCode";
	public static final String DEVICE_CODE = "deviceId";
	public static final String MATRIX_CODE = "matrixCode";// 矩阵编号
	public static final String IP = "ip";
	public static final String PORT = "port";
	public static final String CAMERA_ID_NAME = "摄像头ID";
	public static final String INDEX_CODE_NAME = "索引编号";
	public static final String DEVICE_CODE_NAME = "设备ID";
	public static final String MATRIX_CODE_NAME = "矩阵编号";// 矩阵编号
	public static final String IP_NAME = "设备IP";
	public static final String PORT_NAME = "端口";
	public static final int CAMERA_CONTROL_CODE = 1001;
	public static final int CAMERA_GETSTATUS_CODE = 1002;
	private String cameraId;
	private Integer controlCommand;
	private Integer action;
	private Integer screenId;
	private String indexCode;
	private String deviceId;
	private String matrixCode;
	private String ip;
	private Integer port;

	public Integer getControlCommand() {
		return controlCommand;
	}
	public void setControlCommand(Integer controlCommand) {
		this.controlCommand = controlCommand;
	}
	public Integer getAction() {
		return action;
	}
	public void setAction(Integer action) {
		this.action = action;
	}
	public String getCameraId() {
		return cameraId;
	}
	public void setCameraId(String cameraId) {
		this.cameraId = cameraId;
	}
	public Integer getScreenId() {
		return screenId;
	}
	public void setScreenId(Integer screenId) {
		this.screenId = screenId;
	}
	public String getIndexCode() {
		return indexCode;
	}
	public void setIndexCode(String indexCode) {
		this.indexCode = indexCode;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getMatrixCode() {
		return matrixCode;
	}
	public void setMatrixCode(String matrixCode) {
		this.matrixCode = matrixCode;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	@Override
	public String toString() {
		return "CameraModel [cameraId=" + cameraId + ", controlCommand=" + controlCommand + ", action=" + action
				+ ", screenId=" + screenId + ", indexCode=" + indexCode + ", deviceId=" + deviceId + ", matrixCode="
				+ matrixCode + ", ip=" + ip + ", port=" + port + "]";
	}
}
