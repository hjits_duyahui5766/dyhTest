package com.cloudinnov.model;

/**
 * @author libo
 * @Email boli@cloudinnov.com
 * @version 2017年3月1日 下午3:47:03
 */
public class Remote extends BaseModel {
	private String xid;
	private String pointValue;

	
	
	public String getXid() {
		return xid;
	}
	public void setXid(String xid) {
		this.xid = xid;
	}
	public String getPointValue() {
		return pointValue;
	}
	public void setPointValue(String pointValue) {
		this.pointValue = pointValue;
	}
}
