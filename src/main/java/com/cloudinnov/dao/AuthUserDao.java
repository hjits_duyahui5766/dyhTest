package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.AuthResource;
import com.cloudinnov.model.AuthUsers;

/**
 * @author chengning
 * @date 2016年2月17日下午6:48:58
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface AuthUserDao extends IBaseDao<AuthUsers> {

	/**
	 * 用户登录
	 * 
	 * @Description: 根据用户名密码查询用户信息
	 * @param @param
	 *            user 参数
	 * @return void 返回类型
	 */
	public AuthUsers selectByNamePwd(AuthUsers user);

	public AuthUsers selectByNameOrPhone(AuthUsers user);

	public int insertInfo(AuthUsers user);

	public int updateInfoByCondition(AuthUsers user);

	/**
	 * 查询用户资源
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            codes
	 * @param @return
	 *            参数
	 * @return List<AuthResources> 返回类型
	 */
	public List<AuthResource> selectResourcesByCode(Map<Object, Object> codes);

	/**
	 * selectOemUserList
	 * 
	 * @Description: 查找oem联系人
	 * @param @param
	 *            companyCode
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	public List<AuthUsers> selectOemUserList(Map<String, Object> map);

	/**
	 * selectCustomerUserList
	 * 
	 * @Description: 查找客户联系人
	 * @param @param
	 *            companyCode
	 * @param @return
	 *            参数
	 * @return List<AuthUsers> 返回类型
	 */
	public List<AuthUsers> selectCustomerUserList(Map<String, Object> map);

	/**
	 * insertUserRole
	 * 
	 * @Description: 添加用户角色
	 * @param @param
	 *            user
	 * @param @return
	 *            参数
	 * @return int 返回类型
	 */
	public int insertUserRole(AuthUsers user);

	public int updateUserInfo(AuthUsers user);
	
	public int selectByCondition(AuthUsers user);
	
	/** 通过手机号查询出所对应的区号+手机号
	* selectAreaAndPhone 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    参数
	* @return List<AuthUsers>    返回类型 
	*/
	public List<AuthUsers> selectAreaAndPhone(String phone);
	
	public int resetPassword(AuthUsers user);

}
