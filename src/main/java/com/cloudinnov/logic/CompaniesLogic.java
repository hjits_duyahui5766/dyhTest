package com.cloudinnov.logic;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Companies;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLines;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年2月17日下午2:12:43
 * @email ningcheng@cloudinnov.com
 * @remark 客户管理Service
 * @version
 */
public interface CompaniesLogic extends IBaseLogic<Companies> {

	/**
	 * selectCompaniesCodeAndName
	 * 
	 * @Description: 获取oem下的客户信息
	 * @param @param
	 *            oemCode
	 * @param @return
	 *            参数
	 * @return List<Companies> 返回类型
	 */
	public List<Companies> selectCompanyCodeAndName(Map<String, Object> map);

	public List<Companies> selectCompanyByOemCode(Map<String, Object> map);

	public Page<Companies> search(int index, int size, String country, String province, String city, String key,
			String oemCode,String integratorCode, String type, String language);

	public List<Companies> searchNoPage(String country, String province, String city, String key, String oemCode,
			String type, String language);

	public Map<String, Object> selectEquLineAlarmCountByCompanyCode(String comCode);

	public int selectCompaniesTotalByOemCode(Companies company);

	/**
	 * 获取客户下的设备状态
	 * 
	 * @param equipments
	 * @return
	 */
	public Map<String, Object> selectCustomerEquipmentState(Equipments equipments);

	/**
	 * 获取客户下的产线列表
	 * 
	 * @param ProductionLines
	 * @return
	 */
	public List<ProductionLines> selectCustomerProLines(ProductionLines productionLines);

	public int saveOtherLanguage(Companies company);

	public int updateOtherLanguage(Companies company);

	/**
	 * 获取oem或agent下的客户列表 selectCompanyByOemCode
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param
	 *            company
	 * @param @return
	 *            参数
	 * @return List<Companies> 返回类型
	 */
	public List<Companies> selectCompanyByOemCode(Companies company);

	public List<Companies> listByCodes(String codes, String language, String oemCode);
}
