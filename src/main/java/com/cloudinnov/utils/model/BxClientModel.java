package com.cloudinnov.utils.model;

import java.awt.Color;
import java.util.Arrays;

import com.cloudinnov.model.BaseModel;
import com.cloudinnov.utils.CommonUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;

import onbon.bx05.message.common.ErrorType;

public class BxClientModel extends BaseModel {
	public static final String IP = "ip";
	public static final String PORT = "port";
	public static final String WIDHT = "width";
	public static final String HEIGHT = "height";
	public static final String PROGRAM_NAME = "P00";
	/** 情报板ip */
	private String ip;
	/** 情报板端口 */
	private int port;
	/** 情报板别名(不可重复) */
	private String alias = CommonUtils.getUUID();
	/** 程序名称(不可重复) */
	private String programName = "P001";;
	/** 设置单行文本 */
	private String text;
	/** x坐标,从0开始 */
	private int x;
	/** y坐标,从0开始 */
	private int y;
	/** 情报板宽度 */
	private int width;
	/** 情报板高度 */
	private int height;
	/** 是否校准时间 */
	private boolean syncTime;
	/** 连接是否成功 */
	private boolean isOK;
	/** 边框样式 */
	private int styleIndex;
	/** 字体大小 */
	private int fontSize = 32;
	/** 字体名称 */
	private String fontName = "宋体";
	/** 字体颜色 */
	private String fontColor = "red";
	/** 是否需要换行 */
	private boolean isNewLine;
	/** 如需要换行,则把每行文本放入数组 */
	private String[] texts;
	/** 调整停留时间, 单位 10ms */
	private int stayTime = 10;
	private ErrorType error;
	private long startTime;
	private long endTime;
	private long[] startTimes;
	private long[] endTimes;
	private long duration;// 间隔时间/s
	private long effectiveLength;// 播放时间
	private int speed = 5;
	private String horizontal = "000";
	private String vertical = "000";
	private String spacing = "05";// 字间距
	private String loginName;

	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public static String getWidht() {
		return WIDHT;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public boolean isSyncTime() {
		return syncTime;
	}
	public void setSyncTime(boolean syncTime) {
		this.syncTime = syncTime;
	}
	public boolean isOK() {
		return isOK;
	}
	public void setOK(boolean isOK) {
		this.isOK = isOK;
	}
	public int getStyleIndex() {
		return styleIndex;
	}
	public void setStyleIndex(int styleIndex) {
		this.styleIndex = styleIndex;
	}
	public int getFontSize() {
		return fontSize;
	}
	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	public String getFontName() {
		return fontName;
	}
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	public boolean isNewLine() {
		return isNewLine;
	}
	public void setNewLine(boolean isNewLine) {
		this.isNewLine = isNewLine;
	}
	public String[] getTexts() {
		return texts;
	}
	public void setTexts(String[] texts) {
		this.texts = texts;
	}
	public int getStayTime() {
		return stayTime;
	}
	public void setStayTime(int stayTime) {
		this.stayTime = stayTime;
	}
	public ErrorType getError() {
		return error;
	}
	public void setError(ErrorType error) {
		this.error = error;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	public long[] getStartTimes() {
		return startTimes;
	}
	public void setStartTimes(long[] startTimes) {
		this.startTimes = startTimes;
	}
	public long[] getEndTimes() {
		return endTimes;
	}
	public void setEndTimes(long[] endTimes) {
		this.endTimes = endTimes;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public long getEffectiveLength() {
		return effectiveLength;
	}
	public void setEffectiveLength(long effectiveLength) {
		this.effectiveLength = effectiveLength;
	}
	public String getFontColor() {
		return fontColor;
	}
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public String getHorizontal() {
		return horizontal;
	}
	public void setHorizontal(String horizontal) {
		this.horizontal = horizontal;
	}
	public String getVertical() {
		return vertical;
	}
	public void setVertical(String vertical) {
		this.vertical = vertical;
	}
	public String getSpacing() {
		return spacing;
	}
	public void setSpacing(String spacing) {
		this.spacing = spacing;
	}
	@JsonIgnore
	public Color getFontColorRGB() {
		if (CommonUtils.isNotEmpty(fontColor) && fontColor.equals("red")) {
			return Color.red;
		} else if (CommonUtils.isNotEmpty(fontColor) && fontColor.equals("green")) {
			return Color.green;
		} else if (CommonUtils.isNotEmpty(fontColor) && fontColor.equals("yellow")) {
			return Color.yellow;
		} else {
			return Color.red;
		}
	}
	@Override
	public String toString() {
		return "BxClientModel [ip=" + ip + ", port=" + port + ", alias=" + alias + ", programName=" + programName
				+ ", text=" + text + ", x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + ", syncTime="
				+ syncTime + ", isOK=" + isOK + ", styleIndex=" + styleIndex + ", fontSize=" + fontSize + ", fontName="
				+ fontName + ", fontColor=" + fontColor + ", isNewLine=" + isNewLine + ", texts="
				+ Arrays.toString(texts) + ", stayTime=" + stayTime + ", error=" + error + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", startTimes=" + Arrays.toString(startTimes) + ", endTimes="
				+ Arrays.toString(endTimes) + ", duration=" + duration + ", effectiveLength=" + effectiveLength
				+ ", speed=" + speed + ", horizontal=" + horizontal + ", vertical=" + vertical + ", spacing=" + spacing
				+ ", getIp()=" + getIp() + ", getPort()=" + getPort() + ", getAlias()=" + getAlias()
				+ ", getProgramName()=" + getProgramName() + ", getText()=" + getText() + ", getX()=" + getX()
				+ ", getY()=" + getY() + ", getWidth()=" + getWidth() + ", getHeight()=" + getHeight()
				+ ", isSyncTime()=" + isSyncTime() + ", isOK()=" + isOK() + ", getStyleIndex()=" + getStyleIndex()
				+ ", getFontSize()=" + getFontSize() + ", getFontName()=" + getFontName() + ", isNewLine()="
				+ isNewLine() + ", getTexts()=" + Arrays.toString(getTexts()) + ", getStayTime()=" + getStayTime()
				+ ", getError()=" + getError() + ", getStartTime()=" + getStartTime() + ", getEndTime()=" + getEndTime()
				+ ", getStartTimes()=" + Arrays.toString(getStartTimes()) + ", getEndTimes()="
				+ Arrays.toString(getEndTimes()) + ", getDuration()=" + getDuration() + ", getEffectiveLength()="
				+ getEffectiveLength() + ", getFontColor()=" + getFontColor() + ", getSpeed()=" + getSpeed()
				+ ", getHorizontal()=" + getHorizontal() + ", getVertical()=" + getVertical() + ", getSpacing()="
				+ getSpacing() + ", getFontColorRGB()=" + getFontColorRGB() + ", getAgentCode()=" + getAgentCode()
				+ ", getLanguages()=" + getLanguages() + ", getOtherLanguage()=" + getOtherLanguage()
				+ ", getOemCode()=" + getOemCode() + ", getLanguage()=" + getLanguage() + ", getId()=" + getId()
				+ ", getCode()=" + getCode() + ", getStatusName()=" + getStatusName() + ", getCreateTime()="
				+ getCreateTime() + ", getStatus()=" + getStatus() + ", getLastUpdateTime()=" + getLastUpdateTime()
				+ ", getUserType()=" + getUserType() + ", getCompanyCode()=" + getCompanyCode()
				+ ", getIntegratorCode()=" + getIntegratorCode() + ", getName()=" + getName() + ", getComment()="
				+ getComment() + ", getCustomerCode()=" + getCustomerCode() + ", getCustomerName()=" + getCustomerName()
				+ ", getEquipmentCode()=" + getEquipmentCode() + ", getEquipmentName()=" + getEquipmentName()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
}
