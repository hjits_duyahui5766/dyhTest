package com.cloudinnov.logic.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.cloudinnov.dao.IBaseDao;
import com.cloudinnov.logic.IBaseLogic;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * @author chengning
 * @date 2016年3月4日下午6:40:43
 * @email ningcheng@cloudinnov.com
 * @remark BaseLogicImpl 定义Service的通用方法实现
 * @version
 */
/**
 * @author chengning
 * @date 2016年3月8日下午4:15:47
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public abstract class BaseLogicImpl<T>  implements IBaseLogic<T>{
	
	public static final int DEFAULT_NUM = 0;
	public static final int SUCCESS_NUM = 1;
	public static final String SUCCESS_MSG = "success";
	public static final int ERROR_NUM = 0;
	public static final String ERROR_MSG = "error";

	/**
	 * @Fields iBaseDao :注入BaseDao Spring4.x支持泛型注入方式
	 */
	@Autowired
	protected IBaseDao<T> iBaseDao;

	/** 
	* save 
	* @Description: 保存通用方法 
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int save(T entity) {
		return iBaseDao.insert(entity);
	}

	/** 
	* delete 
	* @Description: 删除通用方法
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int delete(T entity) {
		return iBaseDao.deleteByCondition(entity);
	}

	/** 
	* update 
	* @Description: 修改通用方法
	* @param @param entity
	* @param @return    参数
	* @return int    返回类型 
	*/
	public int update(T entity) {
		return iBaseDao.updateByCondition(entity);
	}


	/** 
	* selectList 
	* @Description: 查询单个表返回List 
	* @param @param entity
	* @param @param isOrderBy 是否按时间排序
	* @param @return    参数
	* @return List<T>    返回类型 
	*/
	public List<T> selectList(T entity,boolean isOrderBy) {
		if(isOrderBy){
			PageHelper.orderBy("create_time desc");
		}	
		
		return iBaseDao.selectListByCondition(entity);
	}
	
	
	/** 
	* selectListPage 
	* @Description: 查询单个表返回list 带分页
	* @param @param index
	* @param @param size
	* @param @param entity
	* @param @param isOrderBy 是否按时间排序
	* @param @return    参数
	* @return Page<T>    返回类型 
	*/
	public Page<T> selectListPage(int index, int size, T entity,boolean isOrderBy) {
		PageHelper.startPage(index, size);
		if(isOrderBy){
			PageHelper.orderBy("create_time desc");
		}	
		return (Page<T>)iBaseDao.selectListByCondition(entity);
	}

	/** 
	* selectListMap 
	* @Description: 查询多个表返回List<Map> 
	* @param @param condition
	* @param @return    参数
	* @return List<Map<String,Object>>    返回类型 
	*/
	public List<Map<String, Object>> selectListMap(Map<String, Object> condition) {
		return iBaseDao.selectListMapByCondition(condition);
	}
	
	/** 
	* selectListMap 
	* @Description: 查询多个表返回List<Map>  带分页
	* @param @param index
	* @param @param size
	* @param @param condition
	* @param @return    参数
	* @return List<Map<String,Object>>    返回类型 
	*/
	@SuppressWarnings("unchecked")
	public Page<T> selectListMapPage(int index, int size,Map<String, Object> condition) {
		PageHelper.startPage(index, size);
		return (Page<T>)iBaseDao.selectListMapByCondition(condition);

	}
	/** 
	* selectMap 
	* @Description: 查询多个表返回一条记录 
	* @param @param condition
	* @param @return    参数
	* @return Map<String,Object>    返回类型 
	*/
	public Map<String, Object> selectMap(Map<String, Object> condition) {
		return iBaseDao.selectMapByCondition(condition);
	}

	public T select(T entity){
		return iBaseDao.selectEntityByCondition(entity);
	}

	/** 查询名称是否存在
	 * @param name name
	 * @param tableName 表名称
	 * @return
	 */
	public int selectByName(String name,String tableName){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("name", name);
		params.put("table", tableName);
		return iBaseDao.findByName(params);
		
	}
}
