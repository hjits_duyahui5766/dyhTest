package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Companies;

public interface CompaniesDao extends IBaseDao<Companies> {

	/**
	 * selectCompaniesCodeAndName
	 * 
	 * @Description: 获取oem下的客户信息 code-name
	 * @param @param
	 *            oemCode
	 * @param @return
	 *            参数
	 * @return List<Companies> 返回类型
	 */
	public List<Companies> selectCompanyCodeAndName(Map<String, Object> map);

	public List<Companies> selectCompanyCodeAndNameByOem(Companies company);

	public List<Companies> search(Map<String, String> map);

	public int insertInfo(Companies company);

	public int updateInfoByCondition(Companies company);

	public int selectCompaniesTotalByOemCode(Companies company);

	/**
	 * 获取监控中心用户基础数据 selectComMonitors
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @return
	 *            参数
	 * @return Map<String,String> 返回类型
	 */
	public List<Map<String, String>> selectCompanysMonitors(Map<String, Object> map);

	public List<Companies> listByCodes(Map<String, Object> map);

}