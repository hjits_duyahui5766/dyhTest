package com.cloudinnov.dao;

import com.cloudinnov.model.HelpWorkOrder;

public interface HelpWorkOrderDao extends IBaseDao<HelpWorkOrder>{
    
	public int insertInfo(HelpWorkOrder helpWorkOrder);

	public int updateInfoByCondition(HelpWorkOrder helpWorkOrder);
}