package com.cloudinnov.logic;

import com.cloudinnov.model.PredictionRuleLog;
import com.cloudinnov.model.PredictionRuleLogDetails;

public interface PredictionRuleLogDetailsLogic extends IBaseLogic<PredictionRuleLogDetails>{
	
	int insertPredictionRuleLogDetails(PredictionRuleLogDetails predictionRuleLogDetails);
	
	PredictionRuleLog selectPredictionRuleLogDetails(PredictionRuleLog PredictionRuleLog);
	
}
