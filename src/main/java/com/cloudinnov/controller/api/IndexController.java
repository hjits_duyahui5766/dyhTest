package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.logic.AlarmWorkOrdersLogic;
import com.cloudinnov.logic.CompaniesLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.ProductionLinesLogic;
import com.cloudinnov.model.Companies;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.utils.CommonUtils;

/**
 * @author guochao
 * @date 2016年3月17日上午10:01:59
 * @email chaoguo@cloudinnov.com
 * @remark 首页数据获取
 * @version
 */

@Controller
@RequestMapping("/webapi/index")
public class IndexController extends BaseController {
	
	@Autowired
	private CompaniesLogic companiesLogic;
	@Autowired
	private ProductionLinesLogic productionLinesLogic;
	@Autowired
	private AlarmWorkOrdersLogic alarmWorkOrdersLogic;
	@Autowired
	private EquipmentsLogic equipmentsLogic;

	@RequestMapping("/statistics")
	@ResponseBody
	public void statistics(Companies company, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String userType = getUserInfo(request).get(USER_TYPE);
		String oemCode = getUserInfo(request).get(OEM_CODE);
		String integratorCode = getUserInfo(request).get(INTEGRATOR_CODE);
		Map<String, Object> map = new HashMap<String, Object>();

		if (userType.equals(CommonUtils.OEM_TYPE)) {
			company.setOemCode(oemCode);
		}
		if (userType.equals(CommonUtils.AGENT_TYPE)) {
			company.setAgentCode(getUserInfo(request).get(COM_CODE));
		}
		if(userType.equals(CommonUtils.INTEGRATOR_TYPE)){
			company.setIntegratorCode(integratorCode);
		}

		int customersTotal = companiesLogic.selectCompaniesTotalByOemCode(company);
		int productLinesTotal = productionLinesLogic.selectProductionLineTotalByOemCode(company);
		int workOrdersTotal = alarmWorkOrdersLogic.selectUndoneWorkOrdersTotalByOemCode(company);
		company.setCurrentState(CommonUtils.EQU_STATE_FAULT);
		int errorEquipmentTotal = equipmentsLogic.selectEquipmentStates(company);

		map.put("customersTotal", customersTotal);
		map.put("productLinesTotal", productLinesTotal);
		map.put("workOrdersTotal", workOrdersTotal);
		map.put("errorEquipmentTotal", errorEquipmentTotal);

		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/mainMonitor", method = RequestMethod.GET)
	@ResponseBody
	public void selectMainMonitor(Equipments equipment, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<>();
		List<Equipments> equCountList = equipmentsLogic.selectMainMonitor(equipment);
		map.put("equSum", 0);
		for(Equipments equ : equCountList){
			map.put("equSum", (int)map.get("equSum") + equ.getEquSum());
		}
		map.put("equFaultSum", 8);
		map.put("equDisConnectSum", 6);
		map.put("equCateList", equCountList);
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
}
