package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.EquipmentInspectionConfigLogic;
import com.cloudinnov.model.EquipmentInspectionConfig;
import com.cloudinnov.model.ProductionLineEquipments;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/webapi/inspection/config")
public class EquipmentInspectionConfigController extends BaseController {

	@Autowired
	private EquipmentInspectionConfigLogic inspectionLogic;

	@RequestMapping(value = "/list")
	@ResponseBody
	public void list(int index, int size, EquipmentInspectionConfig eic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Page<EquipmentInspectionConfig> equipmentInspectionConfig = inspectionLogic.selectListPage(index, size, eic,
				true);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (equipmentInspectionConfig != null) {
			map.put("list", equipmentInspectionConfig);
			map.put("total", equipmentInspectionConfig.getTotal());
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// OEM
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.OEM_TYPE)) {
			eic.setCustomerCode(getUserInfo(request).get(OEM_CODE));
		}
		int result = inspectionLogic.save(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = null;
		if (result == DEFAULT_SUCCESS_SIZE) {
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, null);
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, null);
		}
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "巡检管理", methods = "巡检信息删除")
	public void delete(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = inspectionLogic.delete(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		EquipmentInspectionConfig result = inspectionLogic.select(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			response.getWriter().print(returnJsonAllRequest(request, response, map, SUCCESS, ""));
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}
	
	@RequestMapping(value = "/m/select")
	@ResponseBody
	public void moblieSelect(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		EquipmentInspectionConfig result = inspectionLogic.moblieSelect(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			response.getWriter().print(returnJsonAllRequest(request, response, map, SUCCESS, ""));
		} else {
			response.getWriter().print(returnJsonAllRequest(request, response, map, ERROR, ""));
		}
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public void update(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int result = inspectionLogic.update(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);

	}

	/**
	 * 设备巡检配置查询
	 * 
	 * @param index
	 * @param size
	 * @param country
	 * @param province
	 * @param type
	 * @param city
	 * @param key
	 * @param language
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseBody
	public void search(int index, int size, String country, String province, String type, String city, String key,
			String language, ProductionLineEquipments ple, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String oemCode = getUserInfo(request).get(OEM_CODE);
		String integratorCode = getUserInfo(request).get(INTEGRATOR_CODE);
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.ADMIN_TYPE)) {
			oemCode = "";
			integratorCode = "";
		}
		Page<EquipmentInspectionConfig> result = inspectionLogic.search(index, size, country, province, city, key,
				oemCode, integratorCode, type, language, ple);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null && result.size() > 0) {
			map.put("list", result);
		}
		map.put("total", result.getTotal());
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	/** 根据设备编码获取巡检配置当前状态
	 * @Title: getConfigStateByEquipmentCode 
	 * @Description: TODO
	 * @param eic
	 * @param request
	 * @param response
	 * @throws IOException
	 * @return: void
	 */
	@RequestMapping(value = "/selectConfigStateByEquipmentCode", method = RequestMethod.GET)
	@ResponseBody
	public void getConfigStateByEquipmentCode(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		eic.setCustomerCode(getUserInfo(request).get(COM_CODE));
		Map<String, Object> result = inspectionLogic.getConfigStateByEquipmentCode(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("model", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		}else{
			 returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/selectConfigListByEquipmentCode", method = RequestMethod.GET)
	@ResponseBody
	public void getConfigListByEquipmentCode(EquipmentInspectionConfig eic, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		eic.setCustomerCode(getUserInfo(request).get(COM_CODE));
		List<EquipmentInspectionConfig> result = inspectionLogic.getConfigListByEquipmentCode(eic);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", result);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}

}
