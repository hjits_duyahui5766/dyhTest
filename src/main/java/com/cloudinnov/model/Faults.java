package com.cloudinnov.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Faults extends BaseModel {

	private String oemCode;

	private String libraryCode;

	private String faultCode;

	private String comment;

	private String description;

	private String handlingSuggestion;

	private String faultName;

	// 分类
	private Integer type;
	// 是否产生工单
	private Integer isCreateOrder;
	// 故障级别
	private Integer level;
	
	@JsonIgnore
	private Long happenTime;

	public String getOemCode() {
		return oemCode;
	}

	public void setOemCode(String oemCode) {
		this.oemCode = oemCode;
	}

	public String getLibraryCode() {
		return libraryCode;
	}

	public void setLibraryCode(String libraryCode) {
		this.libraryCode = libraryCode;
	}

	public String getFaultCode() {
		return faultCode;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHandlingSuggestion() {
		return handlingSuggestion;
	}

	public void setHandlingSuggestion(String handlingSuggestion) {
		this.handlingSuggestion = handlingSuggestion;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsCreateOrder() {
		return isCreateOrder;
	}

	public void setIsCreateOrder(Integer isCreateOrder) {
		this.isCreateOrder = isCreateOrder;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getFaultName() {
		return faultName;
	}

	public void setFaultName(String faultName) {
		this.faultName = faultName;
	}

	@JsonProperty("firstTime")
	public Long getHappenTime() {
		return happenTime;
	}
	@JsonProperty("firstTime")
	public void setHappenTime(Long happenTime) {
		this.happenTime = happenTime;
	}


	
}