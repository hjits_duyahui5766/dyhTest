package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.WorkOrderLogic;
import com.cloudinnov.model.AccidentWorkOrder;
import com.cloudinnov.model.WorkOrder;
import com.github.pagehelper.Page;

/**
 * 工单管理
 * @author libo
 * @date 2016年12月5日 下午5:10:08
 * @email boli@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/workOrder")
public class WorkOrderController extends BaseController{

	@Autowired
	private WorkOrderLogic workOrderLogic;
	
	/**
	 * 事故工单添加
	 * accidentWorkOrderSave
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/workOrderSave",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "工单管理",methods = "事故工单添加")
	public void workOrderSave (WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = workOrderLogic.accidentWorkOrderSave(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		
		response.getWriter().print(returnData);
	}
	
	/**
	 * 删除工单--事故工单的删除
	 * deleteWorkOrder
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="工单管理",methods = "删除工单")
	public void deleteWorkOrder(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = workOrderLogic.deleteWorkOrder(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
		
	}
	
	/**
	 * 工单修改--事故工单修改
	 * updateWorkOrder
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/updateWorkOrder",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "工单管理",methods = "工单修改")
	public void updateWorkOrder(WorkOrder workOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		int result = workOrderLogic.updateWorkOrder(workOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(result == 1){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	
	/**
	 * 工单查询--分页
	 * selectWorkOrder
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/selectWorkOrder",method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module="工单管理",methods="工单查询--分页")
	public void selectWorkOrder(WorkOrder workOrder,int index,int size,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		Page<WorkOrder> workOrderInfo =  workOrderLogic.selectWorkOrderList(workOrder,index,size);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(workOrderInfo != null){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map,ERROR,"" );
		}
		response.getWriter().print(returnData);
		
	}
	
	/**
	 * 查询事故工单--单条
	 * selectWorkOrderInfo
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/selectWorkOrderInfo",method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module="工单管理",methods="查询事故工单")
	public void selectWorkOrderInfo(AccidentWorkOrder accidentWorkOrder,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		AccidentWorkOrder accidentWorkOrderInfo = workOrderLogic.selectWorkOrderInfo(accidentWorkOrder);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(accidentWorkOrderInfo != null){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	/**
	 * 查询事故工单--分页
	 * selectAccidentWorkOrder
	 * @Description
	 * @param
	 * @return
	 * @throws IOException 
	 *
	 */
	@RequestMapping(value="/selectAccidentWorkOrder", method = RequestMethod.GET)
	@ResponseBody
	@SystemLog(module="工单管理",methods = "查询事故工单--分页")
	public void selectAccidentWorkOrder(AccidentWorkOrder accidentWorkOrder,int index,int size,HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		
		Page<AccidentWorkOrder> accidentWorkOrderInfo = workOrderLogic.selectAccidentWorkOrderInfo(accidentWorkOrder,index,size);
		
		String returnData = "";
		
		HashMap<String, Object> map = new HashMap<String,Object>();
		
		if(accidentWorkOrderInfo != null){
			returnData = returnJsonAllRequest(request, response, map, SUCCESS,"");
		}else{
			returnData = returnJsonAllRequest(request, response, map, ERROR,"");
		}
		response.getWriter().print(returnData);
	}
	
	
	
	
}
