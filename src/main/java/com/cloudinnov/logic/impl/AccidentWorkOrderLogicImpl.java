package com.cloudinnov.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudinnov.dao.AccidentWorkOrderDao;
import com.cloudinnov.logic.AccidentWorkOrderLogic;
import com.cloudinnov.model.AccidentWorkOrder;
import com.cloudinnov.utils.CodeUtil;

@Service
public class AccidentWorkOrderLogicImpl extends BaseLogicImpl<AccidentWorkOrder> implements AccidentWorkOrderLogic {
	
	@Autowired
	private AccidentWorkOrderDao accidentWorkOrderDao;
	
	@Override
	public int save(AccidentWorkOrder entity) {
		entity.setCode(CodeUtil.accidentWorkOrderCode(4));
		accidentWorkOrderDao.insert(entity);
		return 0;
	}
	@Override
	public int delete(AccidentWorkOrder entity) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int update(AccidentWorkOrder entity) {
		// TODO Auto-generated method stub
		return 0;
	}
}
