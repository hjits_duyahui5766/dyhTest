package com.cloudinnov.task;

import java.text.ParseException;

import com.cloudinnov.logic.EquipmentPointLogic;
import com.cloudinnov.utils.support.spring.SpringUtils;

/**
 * 设备运行状态后台任务类
 * @author chengning
 * @date 2016年6月27日下午4:34:19
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public class EquipmentStateQuartzJob {
	public static long EQU_STATE_RUNTIME = 0L;
	private EquipmentPointLogic equipmentPointLogic = SpringUtils.getBean("equipmentPointsLogic");

	/**
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param 参数
	 * @return void 返回类型
	 * @throws ParseException
	 */
	protected void execute() {
		equipmentPointLogic.checkEquipmentsState(null);
	}
}
