package com.cloudinnov.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SparepartPreventionConfig extends BaseModel{
    private Integer id;

    private String customerCode;

    private String equipmentCode;

    private String sparepartCode;

    private Integer type;

    private String pointCode;

    private String conditionPoint;

    private Integer warningValue;

    private Integer status;

    private String createTime;

    private String lastUpdateTime;

    private String warningContent; 
    
    private int isHadWorkOrder;
    
    @JsonIgnore
    private String conditionCode;
    @JsonIgnore
    private String alarmCondition;
    @JsonIgnore
    private String alarmValue;
    @JsonIgnore
    private String alarmContent;
    
    private String items;
    
    
	public int getIsHadWorkOrder() {
		return isHadWorkOrder;
	}

	public void setIsHadWorkOrder(int isHadWorkOrder) {
		this.isHadWorkOrder = isHadWorkOrder;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getSparepartCode() {
        return sparepartCode;
    }

    public void setSparepartCode(String sparepartCode) {
        this.sparepartCode = sparepartCode;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getConditionPoint() {
        return conditionPoint;
    }

    public void setConditionPoint(String conditionPoint) {
        this.conditionPoint = conditionPoint;
    }

    public Integer getWarningValue() {
        return warningValue;
    }

    public void setWarningValue(Integer warningValue) {
        this.warningValue = warningValue;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getWarningContent() {
        return warningContent;
    }

    public void setWarningContent(String warningContent) {
        this.warningContent = warningContent;
    }

	public String getConditionCode() {
		return conditionCode;
	}

	public void setConditionCode(String conditionCode) {
		this.conditionCode = conditionCode;
	}

	public String getAlarmCondition() {
		return alarmCondition;
	}

	public void setAlarmCondition(String alarmCondition) {
		this.alarmCondition = alarmCondition;
	}

	public String getAlarmValue() {
		return alarmValue;
	}

	public void setAlarmValue(String alarmValue) {
		this.alarmValue = alarmValue;
	}

	public String getAlarmContent() {
		return alarmContent;
	}

	public void setAlarmContent(String alarmContent) {
		this.alarmContent = alarmContent;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}
	
}