package com.cloudinnov.logic;

import com.cloudinnov.model.AlarmRule;

/** 告警记录逻辑类
 * @ClassName: AlarmRuleLogic 
 * @Description: TODO
 * @author: ningmeng
 * @date: 2016年12月2日 下午4:51:52  
 */
public interface AlarmRuleLogic extends IBaseLogic<AlarmRule> {

	int updateAlarmRuleOnOff(String code, Integer onOff);

}
