package com.cloudinnov.logic;

import com.cloudinnov.model.Attach;

/**附件Logic
 * @author chengning
 * @date 2016年4月14日下午2:24:14
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public interface AttachLogic extends IBaseLogic<Attach> {

	public int save(Attach attach);
	
	public int saveWorderOrderImage(Attach attach);
	
	public int saveInspectionImage(Attach attach);
}
