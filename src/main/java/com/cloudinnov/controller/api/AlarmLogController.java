package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AlarmLogLogic;
import com.cloudinnov.model.AlarmLog;
import com.cloudinnov.model.PageModel;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author chengning
 * @date 2016年11月24日16:25:40
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
@Controller
@RequestMapping("/webapi/alarmLog")
public class AlarmLogController extends BaseController {
	@Autowired
	private AlarmLogLogic alarmLogLogic;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseBody
	public void search(@Valid PageModel page, BindingResult error, AlarmLog model, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 校验没有通过
		if (error.hasErrors()) {
			String errorMsg = getErrors(error);
			response.getWriter().println(errorMsg);
			return;
		}
		String returnData = "";
		Map<String, Object> map = new HashMap<>();
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)) {
			model.setCustomerCode(getUserInfo(request).get(COM_CODE));
		}
		Page<AlarmLog> list = alarmLogLogic.search(page, model);
		if (list != null && list.size() > 0) {
			map.put("total", list.getTotal());
		}
		map.put("list", list);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public void selectList(AlarmLog model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<>();
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)) {
			model.setCustomerCode(getUserInfo(request).get(COM_CODE));
		}
		List<AlarmLog> list = alarmLogLogic.selectList(model, false);
		map.put("list", list);
		map.put("closeSum", 0);
		map.put("newSum", 0);
		map.put("handlingSum", 0);
		if(list !=null && !list.isEmpty()){
			for(AlarmLog log : list){
				if(log!=null && log.getOrderStatus() == CommonUtils.ORDER_STATUS_CLOSE){
					map.put("closeSum", (int)map.get("closeSum") +1);
				}else if(log!=null && log.getOrderStatus() == CommonUtils.ORDER_STATUS_HANDLE){
					map.put("handlingSum", (int)map.get("handlingSum") +1);
				}else{
					map.put("newSum", (int)map.get("newSum") +1);
				}
			}
		}
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/fireAlarmList", method = RequestMethod.GET)
	@ResponseBody
	public void fireAlarmList(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<>();
		List<AlarmLog> list = alarmLogLogic.selectFireAlarmList();
		//显示前5条数据
		List<AlarmLog> listFive = new ArrayList<AlarmLog>();
		for(int i=0; i<list.size(); i++) {
			if(i<5) {
				listFive.add(list.get(i));
			} else {
				break;
			}
		}
		int listFiveLength = listFive.size();
		AlarmLog alarmLogEmpty = new AlarmLog();
		alarmLogEmpty.setCreateTime("    ");
		alarmLogEmpty.setSolutionName("   ");
		for(int i=listFiveLength; i<5; i++) {
			listFive.add(alarmLogEmpty);
		}
		map.put("list", listFive);
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	@RequestMapping(value = "/fireAlarmListPage", method = RequestMethod.GET)
	@ResponseBody
	public void fireAlarmListPage(int size, int index, AlarmLog model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String returnData = "";
		Map<String, Object> map = new HashMap<>();
		Page<AlarmLog> list = alarmLogLogic.selectFireAlarmListPage(size,index);
		if (list != null && list.size() > 0) {
			map.put("list", list);
		}
		map.put("total", list.getTotal());
		returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);
	}
	
	/**
	 * 查询告警记录
	 * @param page
	 * @param error
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/selectAlarm",method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module="告警记录管理",methods = "查询告警记录")
	public void selectAlarm(int size,int index,AlarmLog model, HttpServletRequest request,
			HttpServletResponse response) throws IOException{
			String returnData = "";
			Map<String, Object> map = new HashMap<>();
			if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.CUSTORER_TYPE)) {
				model.setCustomerCode(getUserInfo(request).get(COM_CODE));
			}
			Page<AlarmLog> list = alarmLogLogic.selectAlarm(size,index, model);
			if (list != null && list.size() > 0) {
				map.put("total", list.getTotal());
			}
			map.put("list", list);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		
	}
	
}
