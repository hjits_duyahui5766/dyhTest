package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.SysIndustry;

public interface SysIndustryLogic extends IBaseLogic<SysIndustry> {
	
	public int saveOtherLanguage(SysIndustry sysIndustry) ;
	
	public int updateOtherLanguage(SysIndustry sysIndustry);
	
	public List<SysIndustry> tableList(String[] codes, String language);
	
	public List<SysIndustry> search(SysIndustry sysIndustry);
}
