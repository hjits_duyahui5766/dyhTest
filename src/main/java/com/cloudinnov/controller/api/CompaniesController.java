package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.CompaniesLogic;
import com.cloudinnov.logic.EquipmentsLogic;
import com.cloudinnov.logic.SectionLogic;
import com.cloudinnov.model.Companies;
import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.Section;
import com.cloudinnov.utils.CommonUtils;
import com.github.pagehelper.Page;

/**
 * @author guochao
 * @date 2016年2月24日下午1:30:08
 * @email chaoguo@cloudinnov.com
 * @remark
 * @version
 */
/**
 * @author 郭超
 *
 */
@Controller
@RequestMapping("/webapi/company")
public class CompaniesController extends BaseController {
	@Autowired
	private CompaniesLogic companiesLogic;

	@Autowired
	private EquipmentsLogic equipmentsLogic;
	
	@Autowired
	private SectionLogic sectionLogic;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "客户管理", methods = "客户信息保存")
	public void save(Companies company, HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 客户图片处理
		String logo = company.getLogo();
		if (logo != null || logo != "") {
			logo = storeBase64(logo, request);
		}
		company.setLogo(logo);

		// 获取oemCode
		if (company.getType().equals(CommonUtils.CUSTORER_TYPE)) {
			company.setOemCode(getUserInfo(request).get(OEM_CODE));
		}
		
		// 处理代理商
		String userType = getUserInfo(request).get(USER_TYPE);
		
		if(userType.equals(CommonUtils.OEM_TYPE)){
			company.setOemCode(getUserInfo(request).get(OEM_CODE));
		}
		
		if (userType.equals(CommonUtils.AGENT_TYPE)) {
			company.setAgentCode(getUserInfo(request).get(COM_CODE));
		}
		// 处理集成商
		if (userType.equals(CommonUtils.INTEGRATOR_TYPE)) {
			company.setIntegratorCode(getUserInfo(request).get(INTEGRATOR_CODE));
		}
		int result = companiesLogic.save(company);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "客户管理", methods = "客户添加其他语言")
	public void saveOtherLanguage(Companies company, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 客户图片处理
		String logo = company.getLogo();
		if (logo != null || logo != "") {
			logo = storeBase64(logo, request);
		}
		company.setLogo(logo);

		// 获取oemCode
		company.setLanguage(company.getOtherLanguage());
		int result = companiesLogic.saveOtherLanguage(company);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "客户管理", methods = "客户修改其他语言")
	public void updateOtherLanguage(Companies company, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 获取oemCode
		company.setLanguage(company.getOtherLanguage());
		int result = companiesLogic.updateOtherLanguage(company);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "客户管理", methods = "客户信息修改")
	public void update(Companies company, HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (company.getLogo() != "") {
			// 客户图片处理
			String logo = company.getLogo();
			logo = storeBase64(logo, request);
			company.setLogo(logo);
			// 客户图片处理
		}
		int result = companiesLogic.update(company);
		Map<String, Object> map = new HashMap<String, Object>();
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * delete
	 * 
	 * @Description: 根据code或者id删除客户
	 * @param @param
	 *            company
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * 
	 * @param @throws
	 *            IOException 参数
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "客户管理", methods = "客户信息删除")
	public void delete(Companies company, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int result = companiesLogic.delete(company);
		String returnData = returnJsonAllRequest(request, response, map, result, "");
		response.getWriter().println(returnData);
	}

	/**
	 * list
	 * 
	 * @Description: 客户信息列表
	 * @param @param
	 *            index 当前页数
	 * @param @param
	 *            size 每页数量
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * 
	 * @param @throws
	 *            IOException 参数
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public void listPage(int index, int size, Companies com, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询oem下的代理商和客户
		if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.OEM_TYPE)) {
			com.setOemCode(getUserInfo(request).get(OEM_CODE));
			// 查询代理商下的客户
		} else if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.AGENT_TYPE)) {
			com.setAgentCode(getUserInfo(request).get(COM_CODE));
		} else if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.ADMIN_TYPE)) {

		} else if (getUserInfo(request).get(USER_TYPE).equals(CommonUtils.INTEGRATOR_TYPE)) {
			com.setIntegratorCode(getUserInfo(request).get(INTEGRATOR_CODE));
		} else {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
			return;
		}
		Page<Companies> companies = companiesLogic.selectListPage(index, size, com, true);

		if (companies != null) {
			map.put("list", companies);
			map.put("total", companies.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * select
	 * 
	 * @Description: 根据code或者id 查询客户
	 * @param @param
	 *            company
	 * @param @param
	 *            request
	 * @param @param
	 *            response
	 * @param @param
	 *            model
	 * @param @return
	 * @param @throws
	 *            IOException 参数
	 * @return JSONPObject 返回类型
	 */
	@RequestMapping(value = "/select")
	@ResponseBody
	public void select(Companies company, HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (company.getOtherLanguage() != null && company.getOtherLanguage() != "") {
			company.setLanguage(company.getOtherLanguage());
		}
		Companies result = companiesLogic.select(company);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map = companiesLogic.selectEquLineAlarmCountByCompanyCode(company.getCode());
			/**
			 * 暂时new一个对象 最好改为map参数，可以不依赖实体类
			 */
			Equipments equipment = new Equipments();
			equipment.setLanguage(company.getLanguage());
			equipment.setCustomerCode(company.getCode());
			List<Equipments> list = equipmentsLogic.listByCustomer(equipment);
			map.put("model", result);
			map.put("list", list);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * @param index
	 *            页数
	 * @param size
	 *            每页大小
	 * @param country
	 *            国家
	 * @param province
	 *            省份
	 * @param type
	 *            类型
	 * @param city
	 *            城市
	 * @param key
	 *            关键字(名称、简称)
	 * @param language
	 *            语言
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseBody
	public void search(int index, int size, String country, String province, String type, String city, String key,
			String language, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String userType = getUserInfo(request).get(USER_TYPE);
		String oemCode = getUserInfo(request).get(OEM_CODE);
		String integratorCode = getUserInfo(request).get(INTEGRATOR_CODE);

		if (userType != null && userType.equals(CommonUtils.ADMIN_TYPE)) {
			oemCode = "";
			integratorCode = "";
			Page<Companies> result = companiesLogic.search(index, size, country, province, city, key, oemCode,
					integratorCode, type, language);
			Map<String, Object> map = new HashMap<String, Object>();
			if (result != null && result.size() > 0) {
				map.put("list", result);
			}
			map.put("total", result.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			Page<Companies> result = companiesLogic.search(index, size, country, province, city, key, oemCode,
					integratorCode, type, language);
			Map<String, Object> map = new HashMap<String, Object>();
			if (result != null && result.size() > 0) {
				map.put("list", result);
			}
			map.put("total", result.getTotal());
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * 
	 * 搜索不分页
	 * 
	 * @param country
	 * @param province
	 * @param type
	 * @param city
	 * @param key
	 * @param language
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/searchNoPage", method = RequestMethod.POST)
	@ResponseBody
	public void searchNoPage(String country, String province, String type, String city, String key, String language,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String oemCode = null;
		if(type != null && type.equals(CommonUtils.OEM_TYPE)){
			oemCode = getUserInfo(request).get(OEM_CODE);
		}
		
		List<Companies> result = companiesLogic.searchNoPage(country, province, city, key, oemCode, type, language);
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null && result.size() > 0) {
			map.put("list", result);
		}
		String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		response.getWriter().println(returnData);

	}

	/**
	 * 设备列表查询路段列表 无分页 （可以优化处理为一个方法）
	 * 
	 * @param com
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/listnopage")
	@ResponseBody
	public void list(Section section, HttpServletRequest request, HttpServletResponse response) throws IOException {

		section.setOemCode(getUserInfo(request).get(OEM_CODE));

		String userType = getUserInfo(request).get(USER_TYPE);

		if (!userType.equals(CommonUtils.OEM_TYPE)) {
			section.setType(userType);
		}
		if (userType.equals(CommonUtils.AGENT_TYPE)) {
			section.setAgentCode(getUserInfo(request).get(COM_CODE));
		}
		
		List<Section> sections = sectionLogic.selectList(section, true);
		Map<String, Object> map = new HashMap<String, Object>();
		if (sections != null) {
			map.put("list", sections);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * 
	 * 根据公司codes查询公司列表
	 * 
	 * codes格式 (A,B,C,D)
	 * 
	 * @param codes
	 * @param language
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/listByCodes")
	@ResponseBody
	public void listByCodes(String codes, String language, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String oemCode = getUserInfo(request).get(OEM_CODE);
		List<Companies> companies = companiesLogic.listByCodes(codes, language, oemCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if (companies != null) {
			map.put("list", companies);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

	/**
	 * 获取oem或代理商下的客户
	 * 
	 * @param com
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/listbyquery" ,method = RequestMethod.POST)
	@ResponseBody
	public void listByQuery(Companies com, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String userType = getUserInfo(request).get(USER_TYPE);
		if (userType.equals(CommonUtils.OEM_TYPE)) {
			com.setOemCode(getUserInfo(request).get(OEM_CODE));
		} else if (userType.equals(CommonUtils.AGENT_TYPE)) {
			com.setAgentCode(getUserInfo(request).get(COM_CODE));
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
			return;
		}
		List<Companies> companies = companiesLogic.selectCompanyByOemCode(com);
		if (companies != null) {
			map.put("list", companies);
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().println(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().println(returnData);
		}
	}

}
