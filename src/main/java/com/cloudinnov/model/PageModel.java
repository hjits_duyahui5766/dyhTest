package com.cloudinnov.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author chengning
 * @date 2016年6月24日下午5:14:18
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version 
 */
public class PageModel {

	@NotNull(message = "index参数必填")
	@Min(value = 0 , message = "当前页最小值为0")
	private Integer index;
	
	@NotNull(message = "size参数必填")
	@Min(value = 1 , message = "每页显示的条数最小值为1")
	private Integer size;
	
	

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
	
}
