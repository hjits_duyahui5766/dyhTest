package com.cloudinnov.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.PredictionContacter;
import com.cloudinnov.model.PredictionRuleTable;

import scala.annotation.meta.param;

public interface PredictionRuleTableDao extends IBaseDao<PredictionRuleTable>{

	public List<PredictionRuleTable> selectPredictionsRule(PredictionRuleTable predictionRuleTable);
	
	List<String> getAccidentTypes();
	
	int insertContacters(PredictionContacter predictionContacter);
	
	int updateContacters(PredictionContacter predictionContacter);

	int deleteContacters(PredictionContacter predictionContacter);
	
	List<PredictionContacter> selectPredictionContacter(PredictionContacter predictionContacter);
	
	int insertPredictionRule(PredictionRuleTable predictionRuleTable);
	
	//判断添加的预案是否存在
	List<PredictionRuleTable> judgeIsExistPredictionRule(PredictionRuleTable predictionRuleTable);
	
	List<PredictionRuleTable> selectPredictionRule(PredictionRuleTable predictionRuleTable);
	
	List<PredictionRuleTable> selectSinglePredictionRule(PredictionRuleTable predictionRuleTable);
	
	int deletePredictionRule(PredictionRuleTable predictionRuleTable);
}
