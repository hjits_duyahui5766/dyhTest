package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.model.BomCategory;

/**
 * @author chengning
 * @date 2016年6月27日下午1:25:23
 * @email ningcheng@cloudinnov.com
 * @remark 原件分类Logic
 * @version 
 */
public interface BomCategoryLogic extends IBaseLogic<BomCategory>{

	public int saveOtherLanguage(BomCategory bomCategory);

	public List<BomCategory> addSelectList(BomCategory bomCategory);

	public List<BomCategory> tableList(String[] codes, String language, String oemCode);

	public int updateOtherLanguage(BomCategory bomCategory);
}
