package com.cloudinnov.model;

/**
 * 海康摄像头数据对接类
 * @author chengning
 * @date 2017年3月2日下午2:26:53
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version Copyright: Copyright (c) 2011 Company:中科云创科技有限公司
 */
public class HikCameraData {
	private Integer cameraId;
	private String indexCode;
	private String name;
	private String matrixCode;
	private Integer deviceId;
	private String networkAddr;
	private Integer networkPort;
	private String controlName;

	public Integer getCameraId() {
		return cameraId;
	}
	public void setCameraId(Integer cameraId) {
		this.cameraId = cameraId;
	}
	public String getIndexCode() {
		return indexCode;
	}
	public void setIndexCode(String indexCode) {
		this.indexCode = indexCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMatrixCode() {
		return matrixCode;
	}
	public void setMatrixCode(String matrixCode) {
		this.matrixCode = matrixCode;
	}
	public Integer getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	public String getNetworkAddr() {
		return networkAddr;
	}
	public void setNetworkAddr(String networkAddr) {
		this.networkAddr = networkAddr;
	}
	public Integer getNetworkPort() {
		return networkPort;
	}
	public void setNetworkPort(Integer networkPort) {
		this.networkPort = networkPort;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	@Override
	public String toString() {
		return "HikCameraData [cameraId=" + cameraId + ", indexCode=" + indexCode + ", name=" + name + ", matrixCode="
				+ matrixCode + ", deviceId=" + deviceId + ", networkAddr=" + networkAddr + ", networkPort="
				+ networkPort + ", controlName=" + controlName + "]";
	}
}
