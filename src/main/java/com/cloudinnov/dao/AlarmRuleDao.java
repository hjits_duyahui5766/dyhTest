package com.cloudinnov.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cloudinnov.model.AlarmRule;

public interface AlarmRuleDao extends IBaseDao<AlarmRule> {
	
	List<AlarmRule> selectAll();

	int updateAlarmRuleOnOff(@Param("code")String code, @Param("onOff")Integer onOff);
    
}