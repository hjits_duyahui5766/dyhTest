package com.cloudinnov.utils;

class CameraUtil {
	
	static {
		System.loadLibrary("msvcr120d");
		System.loadLibrary("mfc120ud");
		//
		System.loadLibrary("msvcp120d");
		//System.loadLibrary("tmcp_interface_sdk");
		System.loadLibrary("test");
	}
	
	public static void main(String[] args) {
		CameraUtil camer = new CameraUtil();
		
		System.out.println(camer.Plat_LoginCMS("192.168.1.31", "admin", "hik12345+", "8011", 0));
		
	}
	
	/** 初始化资源接口
	 * @Title: Plat_Init 
	 * @Description: TODO
	 * @return
	 * @return: int
	 */
	public native int Plat_Init();
	
	/** 释放资源接口
	 * @Title: Plat_Free 
	 * @Description: TODO
	 * @return
	 * @return: int
	 */
	public native int Plat_Free();

	/** 用户登录接口
	 * @Title: Plat_LoginCMS 
	 * @Description: TODO
	 * @return 成功后返回回话句柄
	 * @return: int
	 */
	public native int Plat_LoginCMS(String cscmsURL, String csUserName, String csPSW, String csPort, int iLoginType);
	
	/** 退出登录接口
	 * @Title: Plat_LogoutCMS 
	 * @Description: TODO
	 * @param iUserhandle 回话句柄
	 * @return
	 * @return: int
	 */
	public native int Plat_LogoutCMS(int iUserhandle);
	
	/** 获取错误信息接口
	 * @Title: Plat_GetLastError 
	 * @Description: TODO
	 * @return
	 * @return: int
	 */
	public native int Plat_GetLastError();
	
	/** 资源查询接口
	 * @Title: Plat_QueryResource 
	 * @Description: TODO
	 * @param iResourceType 设备类型
	 *	CELL 		= 0x00,			中心
	 *	REGION		= 0x01,			区域
	 *	DEVICE	 	= 0x02,       	设备 
	 *	CAMERA 	= 0x03,       	摄像头
	 *	ALARMIN 	= 0x04,			报警输入
	 *	ALARMOUT	= 0x05			报警输出
	 * @param iUserHandle Plat_LoginCMS 返回的句柄
	 * @return
	 * @return: int
	 * 查询成功后使用 MoveNext()， GetValueStr()， GetValueInt()逐条获取设备信息
	 */
	public native int Plat_QueryResource(int iResourceType, int iUserHandle);
	
	/** 查询指定监控点某一时间段内的录像文件信息
	 * @Title: Plat_QueryRecordFile 
	 * @Description: TODO
	 * @param csCameraIdx 监控点编码
	 * @param lStartTime
	 * @param lEndtime
	 * @param csQueryCondition 0为IPSAN  1为设备  2为PCNVR
	 * @param iUserHandle Plat_LoginCMS 返回的句柄
	 * @return 
	 * @return: int
	 * 查询成功后使用 MoveNext()， GetValueStr()， GetValueInt()逐条获取录像信息(录像片段的 开始时间 结束时间 录像URL) 
	 */
	public native int Plat_QueryRecordFile(String csCameraIdx, long lStartTime, long lEndtime, String csQueryCondition, int iUserHandle);
	
	/** 向后移动数据访问游标
	 * @Title: Plat_MoveNext 
	 * @Description: TODO
	 * @param iUserHandle
	 * @return
	 * @return: int
	 */
	public native int Plat_MoveNext(int iUserHandle);
	
	/** 获取查询信息的字符串属性
	 * @Title: Plat_GetValueStr 
	 * @Description: TODO
	 * @param propertyName 属性名称
	 * @param iUserHandle Plat_LoginCMS Plat_LoginCMS 返回的句柄
	 * @return
	 * @return: String
	 */
	public native String  Plat_GetValueStr(String propertyName, int iUserHandle);
	
	/** 获取查询信息的整形属性
	 * @Title: Plat_GetValueInt 
	 * @Description: TODO
	 * @param propertyName 属性名称
	 * @param iUserHandle Plat_LoginCMS 返回的句柄
	 * @return
	 * @return: int
	 */
	public native int Plat_GetValueInt(String propertyName, int iUserHandle);
	
	/** 查询实时流视频的URL 
	 * @Title: Plat_QueryRealStreamURL 
	 * @Description: TODO
	 * @param csCameraIdx 监控点编码
	 * @param iUserHandle Plat_LoginCMS 返回的句柄
	 * @return
	 * @return: byte[]
	 * 
	 */
	public native byte[] Plat_QueryRealStreamURL(String csCameraIdx, int iUserHandle);
	
	/** 停止视频播放
	 * @Title: Plat_StopVideo 
	 * @Description: TODO
	 * @param hStream Plat_PlayVideo返回的句柄
	 * @return
	 * @return: int
	 */
	public native int Plat_StopVideo(int hStream);
	
	/** 云台控制接口
	 * @Title: Plat_ControlCamera 
	 * @Description: TODO
	 * @param csCameraIdx 监控点编码
	 * @param dwPTZCommand 控制命令
	 * @param param1 1：开始动作 0：停止动作，当dwPTZCommand为8,9,39时，param1为预置位序号，最多255,具体和球机有关,当dwPTZCommand为37,38时，param1为巡航轨迹编号
	 * @param param2 云台速度或巡航速度或巡航点停顿时间（1-7）
	 * @param param3
	 * @param param4
	 * @param iUserHandle
	 * @return
	 * @return: int
	 */
	public native int Plat_ControlCamera(String csCameraIdx, int dwPTZCommand, int param1, int param2, int param3, int param4, int iUserHandle);
	
	/** 录像文件下载
	 * @Title: Plat_StartDownLoad 
	 * @Description: TODO
	 * @param csURL 录像文件的URL
	 * @param csSaveFilePath 本地文件保存全路径
	 * @param lStartTime 下载录像的开始时间，为0则按录像默认开始时间 
	 * @param lEndtime 下载录像的结束时间，为0则按录像默认结束时间
	 * @param ullMAXFileSize 分包的大小，如 256*1024*1024,分包为256MB
	 * @param iUserHandle Plat_LoginCMS 返回的句柄
	 * @return
	 * @return: int
	 */
	public native int Plat_StartDownLoad(String csURL, String csSaveFilePath, long lStartTime, long lEndtime, int ullMAXFileSize, int iUserHandle);
	
	/** 停止文件下载
	 * @Title: Plat_StopDownLoad 
	 * @Description: TODO
	 * @param iUserHandle Plat_StartDownLoad返回的句柄
	 * @return
	 * @return: int
	 */
	public native int Plat_StopDownLoad(int iUserHandle);

}
