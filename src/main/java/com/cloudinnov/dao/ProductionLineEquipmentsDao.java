package com.cloudinnov.dao;

import java.util.List;
import java.util.Map;

import com.cloudinnov.model.Equipments;
import com.cloudinnov.model.ProductionLineEquipments;
import com.cloudinnov.model.ProductionLines;

/**
 * @author guochao
 * @date 2016年2月17日下午7:05:34
 * @email chaoguo@cloudinnov.com
 * @remark 生产线设备dao接口
 * @version
 */
public interface ProductionLineEquipmentsDao extends IBaseDao<ProductionLineEquipments>{

	public List<Map<String, Object>> selectEquipmentByProLineCode(ProductionLineEquipments productionLineEqu);
	
	public List<Equipments> selectEquCodeNameByProLineCode(ProductionLines productionLine);

}
