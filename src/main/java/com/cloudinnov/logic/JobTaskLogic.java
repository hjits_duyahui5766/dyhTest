package com.cloudinnov.logic;

import java.util.List;

import com.cloudinnov.task.model.ScheduleJob;

/**
 * Description: <br/>
 * Copyright (c),2011-2017 <br/>
 * This program is protected by copyright Cloudinnov; <br/>
 * Program Name: <br/>
 * Dte:
 * @author chengning
 * @date 2017年4月8日下午3:53:25
 * @email ningcheng@cloudinnov.com
 * @remark
 * @version
 */
public interface JobTaskLogic {
	int addTask(ScheduleJob job);
	void changeStatus(Long jobId, String cmd);
	void addJob(ScheduleJob job);
	/**
	 * 查询任务
	 * @return
	 */
	List<ScheduleJob> getRunningJob();
	ScheduleJob selectTask(ScheduleJob jobModel);
	List<ScheduleJob> getAllTask();
	void updateCron(Long jobId, String cron);
}
