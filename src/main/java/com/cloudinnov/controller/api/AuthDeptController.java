package com.cloudinnov.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinnov.aop.SystemLog;
import com.cloudinnov.logic.AuthDeptLogic;
import com.cloudinnov.model.AuthDept;
import com.cloudinnov.model.AuthDeptUser;
import com.cloudinnov.model.TreeObject;
import com.cloudinnov.model.TreeStateObejct;
import com.cloudinnov.utils.CommonUtils;
import com.cloudinnov.utils.TreeUtil;

/**
 * Created by chengning on 2016/11/11.
 */
@Controller
@RequestMapping("/webapi/dept")
public class AuthDeptController extends BaseController{

    @Autowired
    private AuthDeptLogic authDeptLogic;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @SystemLog(module = "部门管理", methods = "部门信息保存")
    public void save(AuthDept authDept, HttpServletRequest request, HttpServletResponse response) throws IOException {

        authDept.setCompanyCode(getUserInfo(request).get(COM_CODE));
        authDept.setPortal(CommonUtils.PORTAL_ADMIN_TYPE);
        int result = authDeptLogic.save(authDept);
        Map<String, Object> map = new HashMap<String, Object>();
        String returnData = returnJsonAllRequest(request, response, map, result, "");
        response.getWriter().println(returnData);
    }
    
    @RequestMapping(value = "/treeList")
	@ResponseBody
	public void treeList(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<AuthDept> result = authDeptLogic.selectList(authDept, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			TreeObject treeModel = null;
			for (int i = 0; i < result.size(); i++) {
				treeModel = new TreeObject();
				treeModel.setIndexId(result.get(i).getId());
				treeModel.setParentId(result.get(i).getParentId());
				treeModel.setSelectId(result.get(i).getId());
				treeModel.setCode(result.get(i).getCode());
				treeModel.setName(result.get(i).getName());
				treeModel.setComment(result.get(i).getComment());
				treeModel.setGrade(result.get(i).getGrade());
				list.add(treeModel);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, CommonUtils.RESOURCE_PARENT_ID);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
    
    @RequestMapping(value = "/treeSelectIdList")
   	@ResponseBody
   	public void treeSelectIdList(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
   			throws IOException {
   		List<AuthDept> result = authDeptLogic.selectList(authDept, false);
   		String returnData = "";
   		Map<String, Object> map = new HashMap<String, Object>();
   		if (result != null) {
   			List<TreeObject> list = new ArrayList<TreeObject>();
   			TreeObject treeModel = null;
   			for (int i = 0; i < result.size(); i++) {
   				treeModel = new TreeObject();
   				treeModel.setId(result.get(i).getCode());
   				treeModel.setSelectId(result.get(i).getId());
   				treeModel.setParentId(result.get(i).getParentId());
   				treeModel.setCode(result.get(i).getCode());
   				treeModel.setText(result.get(i).getName());
   				treeModel.setComment(result.get(i).getComment());
   				treeModel.setGrade(result.get(i).getGrade());
   				list.add(treeModel);
   			}
   			TreeUtil treeUtil = new TreeUtil();
   			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, CommonUtils.RESOURCE_PARENT_ID);
   			map.put("list", ns);
   			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
   		} else {
   			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
   		}
   		response.getWriter().print(returnData);
   	}
    
	@RequestMapping(value = "/tableList")
	@ResponseBody
	public void resourcesTableList(String[] codes, String language, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<AuthDept> result = authDeptLogic.tableList(codes, language);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			map.put("list", result);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	
	@RequestMapping(value = "/addSelectList", method = RequestMethod.GET)
	@ResponseBody
	public void addSelectList(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<AuthDept> result = authDeptLogic.selectList(authDept, false);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setSelectId(result.get(i).getId());
				ts.setName(result.get(i).getName());
				ts.setCode(result.get(i).getCode());
				ts.setGrade(result.get(i).getGrade());
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, CommonUtils.RESOURCE_PARENT_ID, "　");
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
	
	/**
	 * resourcesDelete
	 * @Description: 根据code删除一条资源记录
	 * @param @param authResourceCode
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "部门管理", methods = "信息删除")
	public void resourcesDelete(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authDeptLogic.delete(authDept);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	/**
	 * resourcesUpdate
	 * @Description: 更新资源记录
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @param model
	 * @param @return
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return String 返回类型
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "部门管理", methods = "信息修改")
	public void resourcesUpdate(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int returnCode = authDeptLogic.update(authDept);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	
	/**
	 * resSaveOtherLanguage
	 * @Description: 资源增加其他语言
	 * @param @param authResources
	 * @param @param request
	 * @param @param response
	 * @param @throws JsonGenerationException
	 * @param @throws JsonMappingException
	 * @param @throws IOException 参数
	 * @return void 返回类型
	 */
	@RequestMapping(value = "/saveOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "部门管理", methods = "修改其他语言")
	public void resSaveOtherLanguage(AuthDept authDept, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authDept.setLanguage(authDept.getOtherLanguage());
		int returnCode = authDeptLogic.saveOtherLanguage(authDept);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	@RequestMapping(value = "/updateOtherLanguage", method = RequestMethod.POST)
	@ResponseBody
	@SystemLog(module = "部门管理", methods = "修改其他语言")
	public void resUpdateOtherLanguage(AuthDept authDept, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		authDept.setLanguage(authDept.getOtherLanguage());
		int returnCode = authDeptLogic.updateOtherLanguage(authDept);
		Map<String, Object> map = new HashMap<String, Object>();
		if (returnCode == CommonUtils.SUCCESS_NUM) {
			String returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
			response.getWriter().print(returnData);
		} else {
			String returnData = returnJsonAllRequest(request, response, map, ERROR, "");
			response.getWriter().print(returnData);
		}
	}
	
	@RequestMapping(value = "/selectDeptUserList")
	@ResponseBody
	public void selectUserRoleList(AuthDept authDept, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		authDept.setPortal(CommonUtils.PORTAL_OEM_TYPE);
		authDept.setCompanyCode(getUserInfo(request).get(COM_CODE));
		// 获取客户下的所有部门
		List<AuthDept> result = authDeptLogic.selectList(authDept, true);
		// 获取该用户已经勾选的部门
		AuthDeptUser deptUser = new AuthDeptUser();
		deptUser.setUserCode(authDept.getCode());
		deptUser.setCompanyCode(authDept.getCompanyCode());
		List<AuthDeptUser> selectedResult = authDeptLogic.selectDeptUserList(deptUser);
		String returnData = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (result != null) {
			List<TreeObject> list = new ArrayList<TreeObject>();
			for (int i = 0; i < result.size(); i++) {
				TreeObject ts = new TreeObject();
				ts.setParentId(result.get(i).getParentId());
				ts.setId(result.get(i).getCode());
				ts.setSelectId(result.get(i).getId());
				ts.setText(result.get(i).getName());
				ts.setSelectId(result.get(i).getId());
				for (int j = 0; j < selectedResult.size(); j++) {
					if (result.get(i).getCode().equals(selectedResult.get(j).getDeptCode())) {
						TreeStateObejct tso = new TreeStateObejct();
						tso.setOpened(true);
						tso.setSelected(true);
						ts.setState(tso);
					}
				}
				list.add(ts);
			}
			TreeUtil treeUtil = new TreeUtil();
			List<TreeObject> ns = treeUtil.getChildTreeObjects(list, CommonUtils.RESOURCE_PARENT_ID);
			map.put("list", ns);
			returnData = returnJsonAllRequest(request, response, map, SUCCESS, "");
		} else {
			returnData = returnJsonAllRequest(request, response, map, ERROR, "");
		}
		response.getWriter().print(returnData);
	}
}
